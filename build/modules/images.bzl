#
# Image Definitions
#

load("@rules_graalvm//graalvm:defs.bzl", "native_image")

load("@rules_pkg//pkg:mappings.bzl", "pkg_files", "pkg_attributes")
load("@rules_pkg//pkg:tar.bzl", "pkg_tar")
load("@rules_oci//oci:defs.bzl", "oci_image", "oci_load")

def aoc_native_binary(name, main_class, **kwargs):
    """Compile a `java` library into a native binary"""

    native_image(
        name = name,
        main_class = main_class,
        # "-H:+DeleteLocalSymbols" doesn't seem to play nicely with the hermetic C++ compiler :(
        extra_args = [ "-H:-DeleteLocalSymbols" ],
        static_zlib = "@zlib//:zlib",
        **kwargs,
    )

def aoc_bench_container(
    name,
    image_tags,
    native_exe = ":native",
    base = "@distroless",
    entrypoint = [ "/opt/advent-of-code/benchmarker" ],
    **kwargs,
):
    """Create a container image containing a native binary"""

    pkg_files(
        name = "%s_exe" % name,
        srcs = [native_exe],
        prefix = "/opt/advent-of-code/",
        attributes = pkg_attributes(mode = "0755"),
        renames = {
            native_exe: "benchmarker"
        },
    )

    pkg_tar(
        name = "%s_tar" % name,
        srcs = [":%s_exe" % name],
    )

    oci_image(
        name = name,
        base = base,
        tars = [":%s_tar" % name],
        workdir = "/work",
        entrypoint = entrypoint,
        **kwargs,
    )

    oci_load(
        name = "%s_load" % name,
        image = ":%s" % name,
        repo_tags = image_tags,
    )