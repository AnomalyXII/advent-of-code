package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 24: Lobby Layout.
 */
@Solution(year = 2020, day = 24, title = "Lobby Layout")
public class Day24 {

    private static final Pattern DIRECTIONS = Pattern.compile("ne|e|se|sw|w|nw");

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Your raft makes it to the tropical island; it turns out that the small crab was an excellent navigator.
     * You make your way to the resort.
     * <p>
     * As you enter the lobby, you discover a small problem: the floor is being renovated.
     * You can't even reach the check-in desk until they've finished installing the <i>new tile floor</i>.
     * <p>
     * The tiles are all <i>hexagonal</i>; they need to be arranged in a <a href="https://en.wikipedia.org/wiki/Hexagonal_tiling">hex grid</a> with a very specific color pattern.
     * Not in the mood to wait, you offer to help figure out the pattern.
     * <p>
     * The tiles are all <i>white</i> on one side and <i>black</i> on the other.
     * They start with the white side facing up.
     * The lobby is large enough to fit whatever pattern might need to appear there.
     * <p>
     * A member of the renovation crew gives you a <i>list of the tiles that need to be flipped over</i> (your puzzle input).
     * Each line in the list identifies a single tile that needs to be flipped by giving a series of steps starting from a <i>reference tile</i> in the very center of the room.
     * (Every line starts from the same reference tile.)
     * <p>
     * Because the tiles are hexagonal, every tile has <i>six neighbors</i>: east, southeast, southwest, west, northwest, and northeast.
     * These directions are given in your list, respectively, as <code>e</code>, <code>se</code>, <code>sw</code>, <code>w</code>, <code>nw</code>, and <code>ne</code>.
     * A tile is identified by a series of these directions with <i>no delimiters</i>; for example, <code>esenee</code> identifies the tile you land on if you start at the reference tile and then move one tile east, one tile southeast, one tile northeast, and one tile east.
     * <p>
     * Each time a tile is identified, it flips from white to black or from black to white.
     * Tiles might be flipped more than once.
     * For example, a line like <code>esew</code> flips a tile immediately adjacent to the reference tile, and a line like <code>nwwswee</code> flips the reference tile itself.
     * <p>
     * Here is a larger example:
     * <pre>
     * sesenwnenenewseeswwswswwnenewsewsw
     * neeenesenwnwwswnenewnwwsewnenwseswesw
     * seswneswswsenwwnwse
     * nwnwneseeswswnenewneswwnewseswneseene
     * swweswneswnenwsewnwneneseenw
     * eesenwseswswnenwswnwnwsewwnwsene
     * sewnenenenesenwsewnenwwwse
     * wenwwweseeeweswwwnwwe
     * wsweesenenewnwwnwsenewsenwwsesesenwne
     * neeswseenwwswnwswswnw
     * nenwswwsewswnenenewsenwsenwnesesenew
     * enewnwewneswsewnwswenweswnenwsenwsw
     * sweneswneswneneenwnewenewwneswswnese
     * swwesenesewenwneswnwwneseswwne
     * enesenwswwswneneswsenwnewswseenwsese
     * wnwnesenesenenwwnenwsewesewsesesew
     * nenewswnwewswnenesenwnesewesw
     * eneswnwswnwsenenwnwnwwseeswneewsenese
     * neswnwewnwnwseenwseesewsenwsweewe
     * wseweeenwnesenwwwswnew
     * </pre>
     * <p>
     * In the above example, 10 tiles are flipped once (to black), and 5 more are flipped twice (to black, then back to white).
     * After all of these instructions have been followed, a total of <i><code>10</code></i> tiles are <i>black</i>.
     * <p>
     * Go through the renovation crew's list and determine which tiles they need to flip.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return After all of the instructions have been followed, <i>how many tiles are left with the black side up</i>?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Map<Tile, Boolean> tiles = new HashMap<>();
        context.consume(line -> {
            final Tile e = calculateTileCoordinate(line);
            tiles.put(e, !tiles.getOrDefault(e, false));
        });

        return countBlackTiles(tiles);
    }

    /**
     * The tile floor in the lobby is meant to be a living art exhibit.
     * Every day, the tiles are all flipped according to the following rules:
     * <ul>
     * <li> Any <i>black</i> tile with <i>zero</i> or <i>more than 2</i> black tiles immediately adjacent to it is flipped to <i>white</i>. </li>
     * <li> Any <i>white</i> tile with <i>exactly 2</i> black tiles immediately adjacent to it is flipped to <i>black</i>. </li>
     * </ul>
     * <p>
     * Here, <i>tiles immediately adjacent</i> means the six tiles directly touching the tile in question.
     * <p>
     * The rules are applied <i>simultaneously</i> to every tile; put another way, it is first determined which tiles need to be flipped, then they are all flipped at the same time.
     * <p>
     * In the above example, the number of black tiles that are facing up after the given number of days has passed is as follows:
     * <pre>
     * Day 1: 15
     * Day 2: 12
     * Day 3: 25
     * Day 4: 14
     * Day 5: 23
     * Day 6: 28
     * Day 7: 41
     * Day 8: 37
     * Day 9: 49
     * Day 10: 37
     *
     * Day 20: 132
     * Day 30: 259
     * Day 40: 406
     * Day 50: 566
     * Day 60: 788
     * Day 70: 1106
     * Day 80: 1373
     * Day 90: 1844
     * Day 100: 2208
     * </pre>
     * <p>
     * After executing this process a total of 100 times, there would be <i><code>2208</code></i> black tiles facing up.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many tiles will be black after 100 days?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Map<Tile, Boolean> tiles = new HashMap<>();
        context.consume(line -> {
            final Tile e = calculateTileCoordinate(line);
            tiles.put(e, !tiles.getOrDefault(e, false));
        });

        for (int i = 0; i < 100; i++) {
            // Create a Set of _just_ black tiles:
            final Set<Tile> blackTiles = streamBlackTiles(tiles)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toSet());

            // Since we don't care about white tiles that don't have a border with
            //   a black tile, we can just ignore anything not immediately
            //   surrounding a black tile?
            final Set<Tile> interestingTiles = blackTiles.stream()
                    .flatMap(tile -> Stream.of(
                            tile, // Centre,
                            tile.calc(Direction.NORTH_EAST),
                            tile.calc(Direction.EAST),
                            tile.calc(Direction.SOUTH_EAST),
                            tile.calc(Direction.SOUTH_WEST),
                            tile.calc(Direction.WEST),
                            tile.calc(Direction.NORTH_WEST)
                    ))
                    .peek(tile -> tiles.computeIfAbsent(tile, x -> false)) // Sneaky sneaky
                    .collect(Collectors.toSet());

            final Map<Tile, Boolean> updatedTiles = new HashMap<>();
            interestingTiles.forEach(tile -> {
                final boolean wasBlack = tiles.get(tile); // Tile should exist by now, so we can NPE if it doesn't!
                final long surroundingBlackTiles = Arrays.stream(Direction.values())
                        .filter(d -> tiles.getOrDefault(tile.calc(d), false))
                        .count();

                final boolean isNowBlack
                        // Any black tile with zero or more than 2 black tiles immediately adjacent to it is flipped to white.
                        = (wasBlack && !(surroundingBlackTiles == 0 || surroundingBlackTiles > 2))
                        // Any white tile with exactly 2 black tiles immediately adjacent to it is flipped to black.
                        || (!wasBlack && surroundingBlackTiles == 2);

                if (wasBlack != isNowBlack) {
                    updatedTiles.put(tile, isNowBlack);
                }
            });

            tiles.putAll(updatedTiles);
        }

        return countBlackTiles(tiles);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Calculate the coordinate of a tile, relative to the centre (0, 0).
     */
    private static Tile calculateTileCoordinate(final String line) {
        final Matcher m = DIRECTIONS.matcher(line);

        int north = 0;
        int east = 0;
        while (m.find()) {
            final Direction d = Direction.fromShortStr(m.group(0));
            north = d.modifyNorth(north);
            east = d.modifyEast(east);
        }

        return new Tile(north, east);
    }

    /*
     * Stream all the black tiles.
     */
    private static Stream<Map.Entry<Tile, Boolean>> streamBlackTiles(final Map<Tile, Boolean> tiles) {
        return tiles.entrySet().stream()
                .filter(Map.Entry::getValue);
    }

    /*
     * Count all the black tiles.
     */
    private static long countBlackTiles(final Map<Tile, Boolean> tiles) {
        return streamBlackTiles(tiles).count();
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A direction that a hexagon can border another.
     */
    private enum Direction {

        NORTH_EAST("ne", 1, 1),
        EAST("e", 0, 2),
        SOUTH_EAST("se", -1, 1),
        SOUTH_WEST("sw", -1, -1),
        WEST("w", 0, -2),
        NORTH_WEST("nw", 1, -1),

        // end of constants
        ;

        private final String str;
        private final int northMod;
        private final int eastMod;

        // Constructors

        Direction(final String str, final int northMod, final int eastMod) {
            this.str = str;
            this.northMod = northMod;
            this.eastMod = eastMod;
        }

        // Helper Methods

        public int modifyNorth(final int north) {
            return north + northMod;
        }

        public int modifyEast(final int east) {
            return east + eastMod;
        }

        /*
         * Look up the Direction based on the short string.
         */
        static Direction fromShortStr(final String str) {
            return Stream.of(values())
                    .filter(d -> d.str.equals(str))
                    .findAny()
                    .orElseThrow();
        }
    }

    /*
     * A tile, represented as the coordinate of the tile relative to the
     * centre tile (0,0).
     */
    private record Tile(int north, int east) {

        // Helper Methods

        Tile calc(final Direction direction) {
            return new Tile(direction.modifyNorth(north), direction.modifyEast(east));
        }

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final Tile tile = (Tile) o;
            return north == tile.north && east == tile.east;
        }

        @Override
        public int hashCode() {
            return Objects.hash(north, east);
        }

    }

}
