package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 19: Monster Messages.
 */
@Solution(year = 2020, day = 19, title = "Monster Messages")
public class Day19 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You land in an airport surrounded by dense forest.
     * As you walk to your high-speed train, the Elves at the Mythical Information Bureau contact you again.
     * They think their satellite has collected an image of a <i>sea monster</i>!
     * Unfortunately, the connection to the satellite is having problems, and many of the messages sent back from the satellite have been corrupted.
     * <p>
     * They sent you a list of <i>the rules valid messages should obey</i> and a list of <i>received messages</i> they've collected so far (your puzzle input).
     * <p>
     * The <i>rules for valid messages</i> (the top part of your puzzle input) are numbered and build upon each other.
     * For example:
     * <pre>
     * 0: 1 2
     * 1: "a"
     * 2: 1 3 | 3 1
     * 3: "b"
     * </pre>
     * <p>
     * Some rules, like <code>3: "b"</code>, simply match a single character (in this case, <code>b</code>).
     * <p>
     * The remaining rules list the sub-rules that must be followed; for example, the rule <code>0: 1 2</code> means that to match rule <code>0</code>, the text being checked must match rule <code>1</code>, and the text after the part that matched rule <code>1</code> must then match rule <code>2</code>.
     * <p>
     * Some of the rules have multiple lists of sub-rules separated by a pipe (<code>|</code>).
     * This means that <i>at least one</i> list of sub-rules must match.
     * (The ones that match might be different each time the rule is encountered.)
     * For example, the rule <code>2: 1 3 | 3 1</code> means that to match rule <code>2</code>, the text being checked must match rule <code>1</code> followed by rule <code>3</code> or it must match rule <code>3</code> followed by rule <code>1</code>.
     * <p>
     * Fortunately, there are no loops in the rules, so the list of possible matches will be finite.
     * Since rule <code>1</code> matches <code>a</code> and rule <code>3</code> matches <code>b</code>, rule <code>2</code> matches either <code>ab</code> or <code>ba</code>.
     * Therefore, rule <code>0</code> matches <code>aab</code> or <code>aba</code>.
     * <p>
     * Here's a more interesting example:
     * <pre>
     * 0: 4 1 5
     * 1: 2 3 | 3 2
     * 2: 4 4 | 5 5
     * 3: 4 5 | 5 4
     * 4: "a"
     * 5: "b"
     * </pre>
     * <p>
     * Here, because rule <code>4</code> matches <code>a</code> and rule <code>5</code> matches <code>b</code>, rule <code>2</code> matches two letters that are the same (<code>aa</code> or <code>bb</code>), and rule <code>3</code> matches two letters that are different (<code>ab</code> or <code>ba</code>).
     * <p>
     * Since rule <code>1</code> matches rules <code>2</code> and <code>3</code> once each in either order, it must match two pairs of letters, one pair with matching letters and one pair with different letters.
     * This leaves eight possibilities: <code>aaab</code>, <code>aaba</code>, <code>bbab</code>, <code>bbba</code>, <code>abaa</code>, <code>abbb</code>, <code>baaa</code>, or <code>babb</code>.
     * <p>
     * Rule <code>0</code>, therefore, matches <code>a</code> (rule <code>4</code>), then any of the eight options from rule <code>1</code>, then <code>b</code> (rule <code>5</code>): <code>aaaabb</code>, <code>aaabab</code>, <code>abbabb</code>, <code>abbbab</code>, <code>aabaab</code>, <code>aabbbb</code>, <code>abaaab</code>, or <code>ababbb</code>.
     * <p>
     * The <i>received messages</i> (the bottom part of your puzzle input) need to be checked against the rules so you can determine which are valid and which are corrupted.
     * Including the rules and the messages together, this might look like:
     * <pre>
     * 0: 4 1 5
     * 1: 2 3 | 3 2
     * 2: 4 4 | 5 5
     * 3: 4 5 | 5 4
     * 4: "a"
     * 5: "b"
     *
     * ababbb
     * bababa
     * abbbab
     * aaabbb
     * aaaabbb
     * </pre>
     * <p>
     * Your goal is to <i>determine the number of messages that completely match rule <code>0</code></i>.
     * In the above example, <code>ababbb</code> and <code>abbbab</code> match, but <code>bababa</code>, <code>aaabbb</code>, and <code>aaaabbb</code> do not, producing the answer <i><code>2</code></i>.
     * The whole message must match all of rule 0; there can't be extra unmatched characters in the message.
     * (For example, <code>aaaabbb</code> might appear to match rule <code>0</code> above, but it has an extra unmatched <code>b</code> on the end.)
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many messages completely match rule <code>0</code>?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<String> rules = new ArrayList<>();
        final List<String> messages = new ArrayList<>();

        context.consume(line -> {
            if (!line.isBlank()) {
                if (looksLikeRule(line)) {
                    rules.add(line.replace(":", " -> "));
                } else {
                    messages.add(line.replaceAll("(.)", "$1 "));
                }
            }
        });

        final EarleyParser parser = EarleyParser.compile(rules.stream().sorted().toArray(String[]::new));
        return messages.stream()
                .filter(message -> isValid(parser, message))
                .count();
    }

    /**
     * As you look over the list of messages, you realize your matching rules aren't quite right.
     * To fix them, completely replace rules <code>8: 42</code> and <code>11: 42 31</code> with the following:
     * <pre>
     * 8: 42 | 42 8
     * 11: 42 31 | 42 11 31
     * </pre>
     * <p>
     * This small change has a big impact: now, the rules <i>do</i> contain loops, and the list of messages they could hypothetically match is infinite.
     * You'll need to determine how these changes affect which messages are valid.
     * <p>
     * Fortunately, many of the rules are unaffected by this change; it might help to start by looking at which rules always match the same set of values and how <i>those</i> rules (especially rules <code>42</code> and <code>31</code>) are used by the new versions of rules <code>8</code> and <code>11</code>.
     * <p>
     * (Remember, you only need to handle the rules you have; building a solution that could handle any hypothetical combination of rules would be <a href="https://en.wikipedia.org/wiki/Formal_grammar">significantly more difficult</a>.)
     * <p>
     * For example:
     * <pre>
     * 42: 9 14 | 10 1
     * 9: 14 27 | 1 26
     * 10: 23 14 | 28 1
     * 1: "a"
     * 11: 42 31
     * 5: 1 14 | 15 1
     * 19: 14 1 | 14 14
     * 12: 24 14 | 19 1
     * 16: 15 1 | 14 14
     * 31: 14 17 | 1 13
     * 6: 14 14 | 1 14
     * 2: 1 24 | 14 4
     * 0: 8 11
     * 13: 14 3 | 1 12
     * 15: 1 | 14
     * 17: 14 2 | 1 7
     * 23: 25 1 | 22 14
     * 28: 16 1
     * 4: 1 1
     * 20: 14 14 | 1 15
     * 3: 5 14 | 16 1
     * 27: 1 6 | 14 18
     * 14: "b"
     * 21: 14 1 | 1 14
     * 25: 1 1 | 1 14
     * 22: 14 14
     * 8: 42
     * 26: 14 22 | 1 20
     * 18: 15 15
     * 7: 14 5 | 1 21
     * 24: 14 1
     *
     * abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
     * bbabbbbaabaabba
     * babbbbaabbbbbabbbbbbaabaaabaaa
     * aaabbbbbbaaaabaababaabababbabaaabbababababaaa
     * bbbbbbbaaaabbbbaaabbabaaa
     * bbbababbbbaaaaaaaabbababaaababaabab
     * ababaaaaaabaaab
     * ababaaaaabbbaba
     * baabbaaaabbaaaababbaababb
     * abbbbabbbbaaaababbbbbbaaaababb
     * aaaaabbaabaaaaababaa
     * aaaabbaaaabbaaa
     * aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
     * babaaabbbaaabaababbaabababaaab
     * aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba
     * </pre>
     * <p>
     * Without updating rules <code>8</code> and <code>11</code>, these rules only match three messages: <code>bbabbbbaabaabba</code>, <code>ababaaaaaabaaab</code>, and <code>ababaaaaabbbaba</code>.
     * <p>
     * However, after updating rules <code>8</code> and <code>11</code>, a total of <i><code>12</code></i> messages match:
     * <ul>
     * <li> <code>bbabbbbaabaabba</code> </li>
     * <li> <code>babbbbaabbbbbabbbbbbaabaaabaaa</code> </li>
     * <li> <code>aaabbbbbbaaaabaababaabababbabaaabbababababaaa</code> </li>
     * <li> <code>bbbbbbbaaaabbbbaaabbabaaa</code> </li>
     * <li> <code>bbbababbbbaaaaaaaabbababaaababaabab</code> </li>
     * <li> <code>ababaaaaaabaaab</code> </li>
     * <li> <code>ababaaaaabbbaba</code> </li>
     * <li> <code>baabbaaaabbaaaababbaababb</code> </li>
     * <li> <code>abbbbabbbbaaaababbbbbbaaaababb</code> </li>
     * <li> <code>aaaaabbaabaaaaababaa</code> </li>
     * <li> <code>aaaabbaabbaaaaaaabbbabbbaaabbaabaaa</code> </li>
     * <li> <code>aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba</code> </li>
     * </ul>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return After updating rules <code>8</code> and <code>11</code>, how many messages completely match rule <code>0</code>?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<String> rules = new ArrayList<>();
        final List<String> messages = new ArrayList<>();

        context.consume(line -> {
            if (!line.isBlank()) {
                if (looksLikeRule(line)) {
                    if (line.startsWith("8:")) {
                        rules.add("8 -> 42 | 42 8");
                    } else if (line.startsWith("11:")) {
                        rules.add("11 -> 42 31 | 42 11 31");
                    } else {
                        rules.add(line.replace(":", " -> "));
                    }
                } else {
                    messages.add(line.replaceAll("(.)", "$1 "));
                }
            }
        });

        final EarleyParser parser = EarleyParser.compile(rules.stream().sorted().toArray(String[]::new));
        return messages.stream()
                .filter(message -> isValid(parser, message))
                .count();
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /**
     * Check if the given message is valid for the specified rule.
     *
     * @param message the message to validate
     * @param rules   an indexed array of rules that may be referenced
     * @return {@literal true} if the message is valid; {@literal false} otherwise
     */
    static boolean isValid(final String message, final String[] rules) {
        final EarleyParser parser = EarleyParser.compile(rules);
        return isValid(parser, message);
    }

    /**
     * Check if the given message is valid for the specified rule.
     *
     * @param message the message to validate
     * @param root    the rule to validate against
     * @param refs    an indexed array of rules that may be referenced
     * @return {@literal true} if the message is valid; {@literal false} otherwise
     */
    static boolean isValid(final String message, final String root, final String[] refs) {
        final String[] rules = new String[refs.length + 1];
        rules[0] = "root -> " + root;
        IntStream.range(0, refs.length)
                .forEach(i -> rules[i + 1] = i + " -> " + refs[i]);

        return isValid(message, rules);
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    private static final Pattern RULE_PATTERN = Pattern.compile("^([0-9]+):(.+)$");

    /*
     * Check if the given input looks like a rule.
     */
    private static boolean looksLikeRule(final String line) {
        return RULE_PATTERN.matcher(line).matches();
    }

    /*
     * Check if the given message is valid for the rules; specifically,
     * checking whether the rule is valid for the first rule in the provided
     * array of rules.
     */
    private static boolean isValid(final EarleyParser parser, final String message) {
        return parser.matches(message);
    }

    /*
     * An implementation of the Earley parsing algorithm.
     */
    private record EarleyParser(String root, Map<String, Production[]> productions) {

        // Helper Methods

        /*
         * Check if a given String matches the grammar.
         */
        public boolean matches(final String sentence) {
            // Format the input into the correct shape
            final String[] inputs = sentence.split(" +");
            return matches(inputs);
        }

        /*
         * Check if a given String matches the grammar.
         */
        public boolean matches(final String[] inputs) {
            // INIT(words)
            // S <- CREATE-ARRAY(LENGTH(words) + 1)
            //    for k <- from 0 to LENGTH(words) do
            final StateSet[] s = IntStream.rangeClosed(0, inputs.length)
                    // S[k] <- EMPTY-ORDERED-SET
                    .mapToObj(i -> new StateSet())
                    .toArray(StateSet[]::new);

            // ADD-TO-SET((γ -> •S, 0), S[0])
            s[0].add(new State(new Production("_", new String[]{root}), 0, 0));

            // for k <- from 0 to LENGTH(words) do
            IntStream.rangeClosed(0, inputs.length)
                    .forEachOrdered(k -> {
                        // for each state in S[k] do  // S[k] can expand during this loop
                        s[k].forEachFlux(state -> {
                            // if not FINISHED(state) then
                            if (!state.isFinished()) {
                                // if NEXT-ELEMENT-OF(state) is a nonterminal then
                                if (state.nextTokenIsNonTerminal()) {
                                    // PREDICTOR(state, k, grammar)         // non-terminal
                                    predictor(s, state, k, productions);
                                } else { // else do
                                    // SCANNER(state, k, words)             // terminal
                                    scanner(s, state, k, inputs);
                                }
                            } else { // else do
                                // COMPLETER(state, k)
                                completer(s, state, k);
                            }
                            // end
                        });
                        // end
                    });
            return s[inputs.length].containsCompletedProduction(root);
        }

        /*
         * Perform the predictor process, as defined by the Earley parsing
         * algorithm.
         */
        // procedure PREDICTOR((A → α•Bβ, j), k, grammar)
        private static void predictor(
                final StateSet[] s,
                final State state,
                final int k,
                final Map<String, Production[]> productions
        ) {
            final String token = state.nextToken();
            final Production[] productionsForNonTerminal = productions.get(token);

            // for each (B → γ) in GRAMMAR-RULES-FOR(B, grammar) do
            Stream.of(productionsForNonTerminal)
                    .forEach(p -> {
                        // ADD-TO-SET((B → •γ, k), S[k])
                        final State newState = new State(p, 0, k);
                        s[k].add(newState);
                    });
            // end
        }

        /*
         * Perform the scanner process, as defined by the Earley parsing
         * algorithm.
         */
        // procedure SCANNER((A → α•aβ, j), k, words)
        private static void scanner(final StateSet[] s, final State state, final int k, final String[] inputs) {
            final String input = k < inputs.length ? inputs[k] : null;
            final String token = state.nextToken();
            assert token != null : "Token was null!";

            // if a ⊂ PARTS-OF-SPEECH(words[k]) then
            if (terminalMatchesWord(input, token)) {
                // ADD-TO-SET((A → αa•β, j), S[k+1])
                final State newState = new State(state.production, state.position + 1, state.origin);
                s[k + 1].add(newState);
            }
            // end
        }

        /*
         * Perform the completer process, as defined by the Earley parsing
         * algorithm.
         */
        // procedure COMPLETER((B → γ•, x), k)
        private static void completer(final StateSet[] s, final State state, final int k) {
            // for each (A → α•Bβ, j) in S[x] do
            s[state.origin].forEachFlux(_state -> {
                if (state.production.id.equals(_state.nextToken())) {
                    // ADD-TO-SET((A → αB•β, j), S[k])
                    final State newState = new State(_state.production, _state.position + 1, _state.origin);
                    s[k].add(newState);
                }
            });
            // end
        }

        /*
         * Check if the given literal (non-terminal) token matches the given
         * input word.
         *
         * Since this is dealing with literals, we can simply strip the \"\"s off
         * the token and do a direct `String` comparison?
         */
        private static boolean terminalMatchesWord(final String input, final String token) {
            return token.substring(1, token.length() - 1).equals(input);
        }

        /*
         * Compile the given grammar rules into a format that can be used by this
         * Earley parser.
         *
         * Rules should be in the form: `A -> x y | "z"`.
         */
        private static EarleyParser compile(final String[] rules) {
            String root = null;
            final Map<String, Production[]> productions = new TreeMap<>();
            for (final String rule : rules) {
                final String[] parts = rule.split(" *-> *");
                final String id = parts[0];
                if (root == null) {
                    // For simplicity, assume the root rule is the first one defined...
                    root = id;
                }

                final String pattern = parts[1];
                final Production[] disjunctions = Stream.of(pattern.split(" *[|] *"))
                        .map(disjunction -> new Production(id, disjunction.split(" +")))
                        .toArray(Production[]::new);

                productions.put(id, disjunctions);
            }

            return new EarleyParser(root, productions);
        }

    }

    /*
     * A production, as defined by the Earley parsing algorithm.
     */
    private record Production(String id, String[] tokens) {

        // To String

        @Override
        public String toString() {
            return "(" + id + " -> " + String.join(" ", tokens) + ")";
        }

    }

    /*
     * A state in the Earley parsing process.
     *
     * In practice, this is a tuple of:
     *   - a production;
     *   - the current position within that production to match against;
     *   - the original offset that this production matched from.
     */
    private static final class State {

        // Private Members

        private final Production production;
        private final int position;
        private final int origin;

        private final String rep;

        // Constructors

        State(final Production production, final int position, final int origin) {
            this.production = production;
            this.position = position;
            this.origin = origin;

            final StringBuilder repBuilder = new StringBuilder(production.id);
            repBuilder.append(" -> ");
            for (int i = 0; i <= production.tokens.length; i++) {
                if (i == position) {
                    repBuilder.append("•");
                }
                if (i < production.tokens.length) {
                    repBuilder.append(production.tokens[i]);
                }
            }
            repBuilder.append(", ");
            repBuilder.append(origin);
            this.rep = repBuilder.toString();
        }

        // Helper Methods

        /*
         * Check if this production has finished.
         */
        public boolean isFinished() {
            return position >= production.tokens.length;
        }

        /*
         * Get the next token in the production.
         */
        public String nextToken() {
            return position < production.tokens.length ? production.tokens[position] : null;
        }

        /*
         * Check if the next token in the production is non-terminal.
         */
        public boolean nextTokenIsNonTerminal() {
            final String next = nextToken();
            assert next != null : "Next token was null!";

            return !(next.startsWith("\"") && next.endsWith("\""));
        }

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final State state = (State) o;
            return position == state.position
                    && origin == state.origin
                    && production.equals(state.production);
        }

        @Override
        public int hashCode() {
            return Objects.hash(production, position, origin);
        }

        // To String

        @Override
        public String toString() {
            return "((" + rep + "))";
        }

    }

    /*
     * A set of states.
     */
    private static final class StateSet extends LinkedHashSet<State> {

        // Helper Methods

        /*
         * Check if this StateSet contains a completed production with the
         * specified LHS>
         */
        boolean containsCompletedProduction(final String id) {
            return stream()
                    .filter(state -> state.production.id.equals(id))
                    .anyMatch(State::isFinished);
        }

        /*
         * Iterate over this StateSet, taking into account any changes that
         * have occurred during the current iteration.
         */
        void forEachFlux(final Consumer<State> consumer) {
            for (int i = 0; i < size(); i++) {
                // THIS IS REALLY HACKY...
                // Java doesn't really like iterating over a changing collection
                //    and we can't directly index the elements in a Set, so...
                //    ... yeah, this is a monstrosity but oh well!
                final List<State> copy = new ArrayList<>(this);
                final State state = copy.get(i);
                consumer.accept(state);
            }
        }

    }

}
