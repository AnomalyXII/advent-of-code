package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;
import net.anomalyxii.aoc.utils.geometry.Grid.MutableGrid;
import net.anomalyxii.aoc.utils.geometry.Velocity;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BooleanSupplier;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 11: Seating System.
 */
@Solution(year = 2020, day = 11, title = "Seating System")
public class Day11 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Your plane lands with plenty of time to spare.
     * The final leg of your journey is a ferry that goes directly to the tropical island where you can finally start your vacation.
     * As you reach the waiting area to board the ferry, you realize you're so early, nobody else has even arrived yet!
     * <p>
     * By modeling the process people use to choose (or abandon) their seat in the waiting area, you're pretty sure you can predict the best place to sit.
     * You make a quick map of the seat layout (your puzzle input).
     * <p>
     * The seat layout fits neatly on a grid.
     * Each position is either floor (<code>.</code>), an empty seat (<code>L</code>), or an occupied seat (<code>#</code>).
     * For example, the initial seat layout might look like this:
     * <pre>
     * L.LL.LL.LL
     * LLLLLLL.LL
     * L.L.L..L..
     * LLLL.LL.LL
     * L.LL.LL.LL
     * L.LLLLL.LL
     * ..L.L.....
     * LLLLLLLLLL
     * L.LLLLLL.L
     * L.LLLLL.LL
     * </pre>
     * <p>
     * Now, you just need to model the people who will be arriving shortly.
     * Fortunately, people are entirely predictable and always follow a simple set of rules.
     * All decisions are based on the <i>number of occupied seats</i> adjacent to a given seat (one of the eight positions immediately up, down, left, right, or diagonal from the seat).
     * The following rules are applied to every seat simultaneously:
     * <ul>
     * <li> If a seat is <i>empty</i> (<code>L</code>) and there are <i>no</i> occupied seats adjacent to it, the seat becomes <i>occupied</i>. </li>
     * <li> If a seat is <i>occupied</i> (<code>#</code>) and <i>four or more</i> seats adjacent to it are also occupied, the seat becomes <i>empty</i>. </li>
     * <li> Otherwise, the seat's state does not change. </li>
     * </ul>
     * Floor (<code>.</code>) never changes; seats don't move, and nobody sits on the floor.
     * <p>
     * After one round of these rules, every seat in the example layout becomes occupied:
     * <pre>
     * #.##.##.##
     * #######.##
     * #.#.#..#..
     * ####.##.##
     * #.##.##.##
     * #.#####.##
     * ..#.#.....
     * ##########
     * #.######.#
     * #.#####.##
     * </pre>
     * <p>
     * After a second round, the seats with four or more occupied adjacent seats become empty again:
     * <pre>
     * #.LL.L#.##
     * #LLLLLL.L#
     * L.L.L..L..
     * #LLL.LL.L#
     * #.LL.LL.LL
     * #.LLLL#.##
     * ..L.L.....
     * #LLLLLLLL#
     * #.LLLLLL.L
     * #.#LLLL.##
     * </pre>
     * <p>
     * This process continues for three more rounds:
     * <pre>
     * #.##.L#.##
     * #L###LL.L#
     * L.#.#..#..
     * #L##.##.L#
     * #.##.LL.LL
     * #.###L#.##
     * ..#.#.....
     * #L######L#
     * #.LL###L.L
     * #.#L###.##
     * </pre>
     * <pre>
     * #.#L.L#.##
     * #LLL#LL.L#
     * L.L.L..#..
     * #LLL.##.L#
     * #.LL.LL.LL
     * #.LL#L#.##
     * ..L.L.....
     * #L#LLLL#L#
     * #.LLLLLL.L
     * #.#L#L#.##
     * </pre>
     * <pre>
     * #.#L.L#.##
     * #LLL#LL.L#
     * L.#.L..#..
     * #L##.##.L#
     * #.#L.LL.LL
     * #.#L#L#.##
     * ..L.L.....
     * #L#L##L#L#
     * #.LLLLLL.L
     * #.#L#L#.##
     * </pre>
     * <p>
     * At this point, something interesting happens: the chaos stabilizes and further applications of these rules cause no seats to change state!
     * Once people stop moving around, you count <i><code>37</code></i> occupied seats.
     * <p>
     * Simulate your seating area by applying the seating rules repeatedly until no seats change state.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many seats end up occupied?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final MutableGrid seatingPlan = context.readMutableGrid(c -> c);

        final AtomicLong occupiedSeats = new AtomicLong();
        whileSeatsAreInFlux(() -> {
            seatingPlan.forEachMatching(
                    point -> !isEmptySpace(seatingPlan, point),
                    point -> occupiedSeats.addAndGet(countOccupiedSeats(seatingPlan, point, 1, 4))
            );

            // Update the placeholder positions
            return finaliseUpdatedSeatingPlan(seatingPlan);
        });

        return occupiedSeats.longValue();
    }

    /**
     * As soon as people start to arrive, you realize your mistake.
     * People don't just care about adjacent seats - they care about <i>the first seat they can see</i> in each of those eight directions!
     * <p>
     * Now, instead of considering just the eight immediately adjacent seats, consider the <i>first seat</i> in each of those eight directions.
     * For example, the empty seat below would see <i>eight</i> occupied seats:
     * <pre>
     * .......#.
     * ...#.....
     * .#.......
     * .........
     * ..#L....#
     * ....#....
     * .........
     * #........
     * ...#.....
     * </pre>
     * <p>
     * The leftmost empty seat below would only see <i>one</i> empty seat, but cannot see any of the occupied ones:
     * <pre>
     * .............
     * .L.L.#.#.#.#.
     * .............
     * </pre>
     * <p>
     * The empty seat below would see <i>no</i> occupied seats:
     * <pre>
     * .##.##.
     * #.#.#.#
     * ##...##
     * ...L...
     * ##...##
     * #.#.#.#
     * .##.##.
     * </pre>
     * <p>
     * Also, people seem to be more tolerant than you expected: it now takes <i>five or more</i> visible occupied seats for an occupied seat to become empty (rather than <i>four or more</i> from the previous rules).
     * The other rules still apply: empty seats that see no occupied seats become occupied, seats matching no rule don't change, and floor never changes.
     * <p>
     * Given the same starting layout as above, these new rules cause the seating area to shift around as follows:
     * <pre>
     * L.LL.LL.LL
     * LLLLLLL.LL
     * L.L.L..L..
     * LLLL.LL.LL
     * L.LL.LL.LL
     * L.LLLLL.LL
     * ..L.L.....
     * LLLLLLLLLL
     * L.LLLLLL.L
     * L.LLLLL.LL
     * </pre>
     * <pre>
     * #.##.##.##
     * #######.##
     * #.#.#..#..
     * ####.##.##
     * #.##.##.##
     * #.#####.##
     * ..#.#.....
     * ##########
     * #.######.#
     * #.#####.##
     * </pre>
     * <pre>
     * #.LL.LL.L#
     * #LLLLLL.LL
     * L.L.L..L..
     * LLLL.LL.LL
     * L.LL.LL.LL
     * L.LLLLL.LL
     * ..L.L.....
     * LLLLLLLLL#
     * #.LLLLLL.L
     * #.LLLLL.L#
     * </pre>
     * <pre>
     * #.L#.##.L#
     * #L#####.LL
     * L.#.#..#..
     * ##L#.##.##
     * #.##.#L.##
     * #.#####.#L
     * ..#.#.....
     * LLL####LL#
     * #.L#####.L
     * #.L####.L#
     * </pre>
     * <pre>
     * #.L#.L#.L#
     * #LLLLLL.LL
     * L.L.L..#..
     * ##LL.LL.L#
     * L.LL.LL.L#
     * #.LLLLL.LL
     * ..L.L.....
     * LLLLLLLLL#
     * #.LLLLL#.L
     * #.L#LL#.L#
     * </pre>
     * <pre>
     * #.L#.L#.L#
     * #LLLLLL.LL
     * L.L.L..#..
     * ##L#.#L.L#
     * L.L#.#L.L#
     * #.L####.LL
     * ..#.#.....
     * LLL###LLL#
     * #.LLLLL#.L
     * #.L#LL#.L#
     * </pre>
     * <pre>
     * #.L#.L#.L#
     * #LLLLLL.LL
     * L.L.L..#..
     * ##L#.#L.L#
     * L.L#.LL.L#
     * #.LLLL#.LL
     * ..#.L.....
     * LLL###LLL#
     * #.LLLLL#.L
     * #.L#LL#.L#
     * </pre>
     * <p>
     * Again, at this point, people stop shifting around and the seating area reaches equilibrium.
     * Once this occurs, you count <i><code>26</code></i> occupied seats.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Given the new visibility method and the rule change for occupied seats becoming empty, once equilibrium is reached, <i>how many seats end up occupied</i>?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final MutableGrid seatingPlan = context.readMutableGrid(c -> c);

        final AtomicLong occupiedSeats = new AtomicLong(0);
        whileSeatsAreInFlux(() -> {
            seatingPlan.forEachMatching(
                    point -> !isEmptySpace(seatingPlan, point),
                    point -> occupiedSeats.addAndGet(countOccupiedSeats(seatingPlan, point, Integer.MAX_VALUE, 5))
            );

            // Update the placeholder positions
            return finaliseUpdatedSeatingPlan(seatingPlan);
        });

        return occupiedSeats.longValue();
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Repeat an action until the seat occupation stops changing.
     */
    private static void whileSeatsAreInFlux(final BooleanSupplier action) {
        boolean shouldContinue;
        do {
            // Run...
            shouldContinue = action.getAsBoolean();
        } while (shouldContinue);
    }

    /*
     * Count how many seats are occupied (and mark them as such).
     */
    private int countOccupiedSeats(
            final MutableGrid seatingPlan,
            final Coordinate coordinate,
            final int limit,
            final int antisocialness
    ) {
        final long surroundingSeats = Velocity.directions()
                .filter(direction -> isFirstSeatInDirectionOccupied(seatingPlan, coordinate, direction, limit))
                .count();

        return maybeUpdateCurrentSeat(seatingPlan, coordinate, surroundingSeats, antisocialness);
    }

    /*
     * Update the seating plan if anyone will occupy or leave a seat.
     */
    private static int maybeUpdateCurrentSeat(
            final MutableGrid seatingPlan,
            final Coordinate coordinate,
            final long surroundingSeats,
            final int antisocialness
    ) {
        final boolean currentSeatOccupied = isOccupiedSeat(seatingPlan, coordinate);
        if (!currentSeatOccupied && surroundingSeats == 0) {
            seatingPlan.set(coordinate, '~');
            return 1;
        }

        if (currentSeatOccupied && surroundingSeats >= antisocialness) {
            seatingPlan.set(coordinate, 'l');
            return -1;
        }

        return 0;
    }

    /*
     * Update the seating plan with any changed seats.
     */
    private static boolean finaliseUpdatedSeatingPlan(final MutableGrid seatingPlan) {
        final boolean[] updated = new boolean[1];
        seatingPlan.forEach(point -> {
            if (seatingPlan.get(point) == 'l') {
                seatingPlan.set(point, 'L');
                updated[0] = true;
            } else if (seatingPlan.get(point) == '~') {
                seatingPlan.set(point, '#');
                updated[0] = true;
            }
        });
        return updated[0];
    }

    /*
     * Check if the first seat in a given direction is occupied.
     */
    private static boolean isFirstSeatInDirectionOccupied(
            final Grid seatingPlan,
            final Coordinate coordinate,
            final Velocity dir,
            final int limit
    ) {
        Coordinate nextCoordinate = coordinate.adjustBy(dir);
        for (int l = 0; l < limit && seatingPlan.contains(nextCoordinate); l++, nextCoordinate = nextCoordinate.adjustBy(dir)) {
            if (isEmptySpace(seatingPlan, nextCoordinate)) continue;
            return isOccupiedSeat(seatingPlan, nextCoordinate);
        }

        return false;
    }

    /*
     * Check if the given coordinate is an empty space.
     */
    private static boolean isEmptySpace(final Grid seatingPlan, final Coordinate coordinate) {
        return !seatingPlan.contains(coordinate) || seatingPlan.get(coordinate) == '.';
    }

    /*
     * Check if the given coordinate is an occupied seat.
     */
    private static boolean isOccupiedSeat(final Grid seatingPlan, final Coordinate coordinate) {
        return seatingPlan.contains(coordinate)
                && (seatingPlan.get(coordinate) == '#' || seatingPlan.get(coordinate) == 'l');
    }

}
