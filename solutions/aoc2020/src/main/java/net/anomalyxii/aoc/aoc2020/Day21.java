package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 21: Allergen Assessment.
 */
@Solution(year = 2020, day = 21, title = "Allergen Assessment")
public class Day21 {

    private static final Pattern INGREDIENTS_PATTERN = Pattern.compile("([a-z ]+)(?: [(]contains ([a-z, ]+)[)])?");

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You reach the train's last stop and the closest you can get to your vacation island without getting wet.
     * There aren't even any boats here, but nothing can stop you now: you build a raft.
     * You just need a few days' worth of food for your journey.
     * <p>
     * You don't speak the local language, so you can't read any ingredients lists.
     * However, sometimes, allergens are listed in a language you <i>do</i> understand.
     * You should be able to use this information to determine which ingredient contains which allergen and work out which foods are safe to take with you on your trip.
     * <p>
     * You start by compiling a list of foods (your puzzle input), one food per line.
     * Each line includes that food's <i>ingredients list</i> followed by some or all of the allergens the food contains.
     * <p>
     * Each allergen is found in exactly one ingredient.
     * Each ingredient contains zero or one allergen.
     * <i>Allergens aren't always marked</i>; when they're listed (as in <code>(contains nuts, shellfish)</code> after an ingredients list), the ingredient that contains each listed allergen will be <i>somewhere in the corresponding ingredients list</i>.
     * However, even if an allergen isn't listed, the ingredient that contains that allergen could still be present: maybe they forgot to label it, or maybe it was labeled in a language you don't know.
     * <p>
     * For example, consider the following list of foods:
     * <pre>
     * mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
     * trh fvjkl sbzzf mxmxvkd (contains dairy)
     * sqjhc fvjkl (contains soy)
     * sqjhc mxmxvkd sbzzf (contains fish)
     * </pre>
     * <p>
     * The first food in the list has four ingredients (written in a language you don't understand): <code>mxmxvkd</code>, <code>kfcds</code>, <code>sqjhc</code>, and <code>nhms</code>.
     * While the food might contain other allergens, a few allergens the food definitely contains are listed afterward: <code>dairy</code> and <code>fish</code>.
     * <p>
     * The first step is to determine which ingredients <i>can't possibly</i> contain any of the allergens in any food in your list.
     * In the above example, none of the ingredients <code>kfcds</code>, <code>nhms</code>, <code>sbzzf</code>, or <code>trh</code> can contain an allergen.
     * Counting the number of times any of these ingredients appear in any ingredients list produces <i><code>5</code></i>: they all appear once each except <code>sbzzf</code>, which appears twice.
     * <p>
     * Determine which ingredients cannot possibly contain any of the allergens in your list.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many times do any of those ingredients appear?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Map<String, Long> allIngredients = new HashMap<>();
        final Map<String, Set<String>> allergensToPossibleIngredients = new HashMap<>();

        context.consume(line -> {
            final Matcher m = INGREDIENTS_PATTERN.matcher(line);
            if (!m.matches()) {
                throw new IllegalArgumentException("Invalid ingredient list: '" + line + "'");
            }

            final String ingredientList = m.group(1);
            final List<String> ingredients = asList(ingredientList.split(" +"));
            ingredients.forEach(ingredient -> {
                final long count = allIngredients.getOrDefault(ingredient, 0L);
                allIngredients.put(ingredient, count + 1);
            });

            maybeFilterAllergens(m, allergensToPossibleIngredients, ingredients);
        });

        return allIngredients.entrySet().stream()
                .filter(entry -> allergensToPossibleIngredients.values().stream()
                        .noneMatch(allergens -> allergens.contains(entry.getKey())))
                .mapToLong(Map.Entry::getValue)
                .sum();
    }

    /**
     * Now that you've isolated the inert ingredients, you should have enough information to figure out which ingredient contains which allergen.
     * <p>
     * In the above example:
     * <ul>
     * <li> <code>mxmxvkd</code> contains <code>dairy</code>. </li>
     * <li> <code>sqjhc</code> contains <code>fish</code>. </li>
     * <li> <code>fvjkl</code> contains <code>soy</code>. </li>
     * </ul>
     * <p>
     * Arrange the ingredients <i>alphabetically by their allergen</i> and separate them by commas to produce your <i>canonical dangerous ingredient list</i>.
     * (There should <i>not be any spaces</i> in your canonical dangerous ingredient list.)
     * In the above example, this would be <i><code>mxmxvkd,sqjhc,fvjkl</code></i>.
     * <p>
     * Time to stock your raft with supplies.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is your canonical dangerous ingredient list?
     */
    @Part(part = II)
    public String calculateAnswerForPart2(final SolutionContext context) {
        final Map<String, Set<String>> allergensToPossibleIngredients = new HashMap<>();

        context.consume(line -> {
            final Matcher m = INGREDIENTS_PATTERN.matcher(line);
            if (!m.matches()) {
                throw new IllegalArgumentException("Invalid ingredient list: '" + line + "'");
            }

            final String ingredientList = m.group(1);
            final List<String> ingredients1 = asList(ingredientList.split(" +"));

            maybeFilterAllergens(m, allergensToPossibleIngredients, ingredients1);
        });

        final Map<String, String> ingredientToAllergenMap = matchAllergenToIngredient(allergensToPossibleIngredients);
        return ingredientToAllergenMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey)
                .collect(Collectors.joining(","));
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Maybe filter the list of Allergens to the ones that may apply to each
     * ingredient.
     */
    private static void maybeFilterAllergens(
            final Matcher m,
            final Map<String, Set<String>> allergensToPossibleIngredients,
            final List<String> ingredients
    ) {

        if (m.groupCount() < 2) return;

        final String allergenList = m.group(2);
        final String[] allergens = allergenList.split(", +");

        Arrays.stream(allergens)
                .forEach(allergen -> allergensToPossibleIngredients
                        .computeIfAbsent(allergen, x -> new HashSet<>(ingredients))
                        .retainAll(ingredients));
    }

    /*
     * Process the `Map` of allergens to ingredients and determine exactly
     * which ingredient contains each allergen.
     */
    private static Map<String, String> matchAllergenToIngredient(final Map<String, Set<String>> allergensToPossibleIngredients) {
        final Map<String, String> ingredientToAllergenMap = new HashMap<>();
        while (!allergensToPossibleIngredients.isEmpty()) {
            final Set<String> allergens = new HashSet<>(allergensToPossibleIngredients.keySet());
            allergens.forEach((allergen) -> {
                final Set<String> ingredients = allergensToPossibleIngredients.get(allergen);
                ingredients.removeAll(ingredientToAllergenMap.keySet());

                if (ingredients.isEmpty())
                    throw new IllegalStateException("No possible ingredients for '" + allergen + "'?");


                if (ingredients.size() == 1) {
                    final String ingredient = ingredients.iterator().next();
                    allergensToPossibleIngredients.remove(allergen);
                    ingredientToAllergenMap.put(ingredient, allergen);
                }
            });
        }
        return ingredientToAllergenMap;
    }

}
