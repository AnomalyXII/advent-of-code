package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;
import net.anomalyxii.aoc.utils.geometry.Velocity;

import java.util.concurrent.atomic.AtomicLong;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 3: Toboggan Trajectory.
 */
@Solution(year = 2020, day = 3, title = "Toboggan Trajectory")
public class Day3 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * With the toboggan login problems resolved, you set off toward the airport.
     * While travel by toboggan might be easy, it's certainly not safe: there's very minimal steering and the area is covered in trees.
     * You'll need to see which angles will take you near the fewest trees.
     * <p>
     * Due to the local geology, trees in this area only grow on exact integer coordinates in a grid.
     * You make a map (your puzzle input) of the open squares (<code>.</code>) and trees (<code>#</code>) you can see.
     * For example:
     * <pre>
     * ..##.......
     * #...#...#..
     * .#....#..#.
     * ..#.#...#.#
     * .#...##..#.
     * ..#.##.....
     * .#.#.#....#
     * .#........#
     * #.##...#...
     * #...##....#
     * .#..#...#.#
     * </pre>
     * <p>
     * These aren't the only trees, though; due to something you read about once involving arboreal genetics and biome stability, the same pattern repeats to the right many times:
     * <pre>
     * ..##.........##.........##.........##.........##.........##.......  --->
     * #...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
     * .#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
     * ..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
     * .#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
     * ..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....  --->
     * .#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
     * .#........#.#........#.#........#.#........#.#........#.#........#
     * #.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...
     * #...##....##...##....##...##....##...##....##...##....##...##....#
     * .#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#  --->
     * </pre>
     * <p>
     * You start on the open square (<code>.</code>) in the top-left corner and need to reach the bottom (below the bottom-most row on your map).
     * <p>
     * The toboggan can only follow a few specific slopes (you opted for a cheaper model that prefers rational numbers); start by <i>counting all the trees</i> you would encounter for the slope <i>right 3, down 1</i>:
     * <p>
     * From your starting position at the top-left, check the position that is right 3 and down 1.
     * Then, check the position that is right 3 and down 1 from there, and so on until you go past the bottom of the map.
     * <p>
     * The locations you'd check in the above example are marked here with <code>O</code> where there was an open square and <code>X</code> where there was a tree:
     * <pre>
     * ..##.........##.........##.........##.........##.........##.......  --->
     * #..O#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
     * .#....X..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
     * ..#.#...#O#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
     * .#...##..#..X...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
     * ..#.##.......#.X#.......#.##.......#.##.......#.##.......#.##.....  --->
     * .#.#.#....#.#.#.#.O..#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
     * .#........#.#........X.#........#.#........#.#........#.#........#
     * #.##...#...#.##...#...#.X#...#...#.##...#...#.##...#...#.##...#...
     * #...##....##...##....##...#X....##...##....##...##....##...##....#
     * .#..#...#.#.#..#...#.#.#..#...X.#.#..#...#.#.#..#...#.#.#..#...#.#  --->
     * </pre>
     * <p>
     * In this example, traversing the map using this slope would cause you to encounter <code>7</code> trees.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Starting at the top-left corner of your map and following a slope of right 3 and down 1, <i>how many trees would you encounter?</i>
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return countTreesOnRoute(Grid.parseHorizontallyInfinite(context.stream()), 3, 1);
    }

    /**
     * Time to check the rest of the slopes - you need to minimize the probability of a sudden arboreal stop, after all.
     * <p>
     * Determine the number of trees you would encounter if, for each of the following slopes, you start at the top-left corner and traverse the map all the way to the bottom:
     * <ul>
     * <li> Right 1, down 1. </li>
     * <li> Right 3, down 1. (This is the slope you already checked.) </li>
     * <li> Right 5, down 1. </li>
     * <li> Right 7, down 1. </li>
     * <li> Right 1, down 2. </li>
     * </ul>
     * In the above example, these slopes would find <code>2</code>, <code>7</code>, <code>3</code>, <code>4</code>, and <code>2</code> tree(s) respectively; multiplied together, these produce the answer <i><code>366</code></i>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you multiply together the number of trees encountered on each of the listed slopes?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Grid map = Grid.parseHorizontallyInfinite(context.stream());
        return countTreesOnRoute(map, 1, 1)
                * countTreesOnRoute(map, 3, 1)
                * countTreesOnRoute(map, 5, 1)
                * countTreesOnRoute(map, 7, 1)
                * countTreesOnRoute(map, 1, 2);
    }

    // ****************************************
    // Helper Members
    // ****************************************

    /**
     * Count the number of trees that would be encountered using a given
     * route through a map.
     *
     * @param map       the map
     * @param xDistance the horizontal distance to travel
     * @param yDistance the vertical distance to travel
     * @return the count of trees encountered on the route
     */
    static long countTreesOnRoute(final Grid map, final int xDistance, final int yDistance) {
        final AtomicLong count = new AtomicLong(0);
        map.forEachIntervalMatching(
                new Velocity(xDistance, yDistance),
                point -> isTreeAtCoordinate(map, point),
                point -> count.incrementAndGet()
        );
        return count.longValue();
    }

    /**
     * Check if there is a tree at the given coordinate on the map.
     *
     * @param map        the map
     * @param coordinate the {@link Coordinate} to check
     * @return {@literal true} if the coordinate is a tree; {@literal false} otherwise
     */
    static boolean isTreeAtCoordinate(final Grid map, final Coordinate coordinate) {
        return map.get(coordinate) == '#';
    }

}
