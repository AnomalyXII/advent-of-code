package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.function.ToLongFunction;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 18: Operation Order.
 */
@Solution(year = 2020, day = 18, title = "Operation Order")
public class Day18 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * As you look out the window and notice a heavily-forested continent slowly appear over the horizon, you are interrupted by the child sitting next to you.
     * They're curious if you could help them with their math homework.
     * <p>
     * Unfortunately, it seems like this "math" <a href="https://www.youtube.com/watch?v=3QtRK7Y2pPU&amp;t=15">follows different rules</a> than you remember.
     * <p>
     * The homework (your puzzle input) consists of a series of expressions that consist of addition (<code>+</code>), multiplication (<code>*</code>), and parentheses (<code>(...)</code>).
     * Just like normal math, parentheses indicate that the expression inside must be evaluated before it can be used by the surrounding expression.
     * Addition still finds the sum of the numbers on both sides of the operator, and multiplication still finds the product.
     * <p>
     * However, the rules of <i>operator precedence</i> have changed.
     * Rather than evaluating multiplication before addition, the operators have the same precedence, and are evaluated left-to-right regardless of the order in which they appear.
     * <p>
     * For example, the steps to evaluate the expression <code>1 + 2 * 3 + 4 * 5 + 6</code> are as follows:
     * <pre>
     * 1 + 2 * 3 + 4 * 5 + 6
     *   3   * 3 + 4 * 5 + 6
     *       9   + 4 * 5 + 6
     *          13   * 5 + 6
     *              65   + 6
     *                  71
     * </pre>
     * <p>
     * Parentheses can override this order; for example, here is what happens if parentheses are added to form <code>1 + (2 * 3) + (4 * (5 + 6))</code>:
     * <pre>
     * 1 + (2 * 3) + (4 * (5 + 6))
     * 1 +    6    + (4 * (5 + 6))
     *      7      + (4 * (5 + 6))
     *      7      + (4 *   11   )
     *      7      +     44
     *             51
     * </pre>
     * <p>
     * Here are a few more examples:
     * <ul>
     * <li> <code>2 * 3 + (4 * 5)</code> becomes <i><code>26</code></i>. </li>
     * <li> <code>5 + (8 * 3 + 9 + 3 * 4 * 3)</code> becomes <i><code>437</code></i>. </li>
     * <li> <code>5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))</code> becomes <i><code>12240</code></i>. </li>
     * <li> <code>((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2</code> becomes <i><code>13632</code></i>. </li>
     * </ul>
     * <p>
     * Before you can help with the homework, you need to understand it yourself.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Evaluate the expression on each line of the homework; what is the sum of the resulting values?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .mapToLong(Day18::evaluate)
                .sum();
    }

    /**
     * You manage to answer the child's questions and they finish part 1 of their homework, but get stuck when they reach the next section: <i>advanced</i> math.
     * <p>
     * Now, addition and multiplication have <i>different</i> precedence levels, but they're not the ones you're familiar with.
     * Instead, addition is evaluated <i>before</i> multiplication.
     * <p>
     * For example, the steps to evaluate the expression <code>1 + 2 * 3 + 4 * 5 + 6</code> are now as follows:
     * <pre>
     * 1 + 2 * 3 + 4 * 5 + 6
     *   3   * 3 + 4 * 5 + 6
     *   3   *   7   * 5 + 6
     *   3   *   7   *  11
     *      21       *  11
     *          231
     * </pre>
     * <p>
     * Here are the other examples from above:
     * <ul>
     * <li> <code>1 + (2 * 3) + (4 * (5 + 6))</code> still becomes <i><code>51</code></i>. </li>
     * <li> <code>2 * 3 + (4 * 5)</code> becomes <i><code>46</code></i>. </li>
     * <li> <code>5 + (8 * 3 + 9 + 3 * 4 * 3)</code> becomes <i><code>1445</code></i>. </li>
     * <li> <code>5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4)</code>) becomes <i><code>669060</code></i>. </li>
     * <li> <code>((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2</code> becomes <i><code>23340</code></i>. </li>
     * </ul>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you add up the results of evaluating the homework problems using these new rules?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return context.stream()
                .mapToLong(Day18::evaluateAdv)
                .sum();
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /**
     * Evaluate a simple maths equation.
     *
     * @param equation the equation to evaluate
     * @return the result
     */
    static long evaluate(final String equation) {
        final ArrayDeque<Character> chars = new ArrayDeque<>();
        for (final char c : equation.toCharArray()) {
            chars.add(c);
        }
        return evaluate(chars);
    }

    /**
     * Evaluate an advanced maths equation.
     *
     * @param equation the equation to evaluate
     * @return the result
     */
    static long evaluateAdv(final String equation) {
        final ArrayDeque<Character> chars = new ArrayDeque<>();
        for (final char c : equation.toCharArray()) {
            chars.add(c);
        }
        return evaluateAdv(chars);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Evaluate a simple maths equation.
     */
    private static long evaluate(final Deque<Character> equation) {
        return parse(equation, Day18::evaluateStack);
    }

    /*
     * Evaluate an advanced maths equation.
     */
    private static long evaluateAdv(final Deque<Character> equation) {
        return parse(equation, Day18::evaluateStackAdv);
    }

    /*
     * Parse the maths equation and evaluate sub-expressions.
     */
    private static long parse(final Deque<Character> equation, final ToLongFunction<List<String>> evaluator) {
        StringBuilder token = null;

        final List<String> stack = new ArrayList<>();
        Character c;
        while ((c = equation.pollFirst()) != null) {
            if (c == ')') {
                if (token != null) {
                    stack.add(token.toString());
                }

                return evaluator.applyAsLong(stack);
            }
            if (c == '(') {
                final long sub = parse(equation, evaluator);
                stack.add(Long.toString(sub));
                continue;
            }

            if (c == ' ') {
                if (token != null) {
                    stack.add(token.toString());
                    token = null;
                }
                continue;
            }
            if (c == '+' || c == '*') {
                if (token != null) {
                    stack.add(token.toString());
                    token = null;
                }

                stack.add(Character.toString(c));
                continue;
            }

            if (token == null) token = new StringBuilder();
            token.append(c);
        }

        if (token != null) {
            stack.add(token.toString());
        }

        return evaluator.applyAsLong(stack);
    }

    /*
     * Evaluate a stack of tokens in a simple way.
     */
    private static long evaluateStack(final List<String> stack) {
        long result = Long.parseLong(stack.getFirst());
        for (int i = 1; i < stack.size() - 1; i += 2) {
            final String token = stack.get(i);
            if (!"*".equalsIgnoreCase(token) && !"+".equalsIgnoreCase(token)) {
                throw new IllegalStateException("Expected '+' or '*' but found '" + token + "'");
            }

            final String next = stack.get(i + 1);
            final long right = Long.parseLong(next);

            if ("*".equalsIgnoreCase(token)) {
                result *= right;
            } else {
                result += right;
            }
        }

        return result;
    }

    /*
     * Evaluate a stack of tokens in an advanced way.
     */
    private static long evaluateStackAdv(final List<String> stack) {
        final List<String> newStack = new ArrayList<>();
        for (int i = 0; i < stack.size(); i++) {
            final String token = stack.get(i);

            if ("+".equalsIgnoreCase(token)) {
                final String prev = newStack.removeLast();
                final String next = stack.get(++i);

                final long left = Long.parseLong(prev);
                final long right = Long.parseLong(next);
                final long result = left + right;
                newStack.add(Long.toString(result));
                continue;
            }

            newStack.add(token);
        }

        return evaluateStack(newStack);
    }

}
