package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.NoChallenge;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 25: Combo Breaker.
 */
@Solution(year = 2020, day = 25, title = "Combo Breaker")
public class Day25 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You finally reach the check-in desk.
     * Unfortunately, their registration systems are currently offline, and they cannot check you in.
     * Noticing the look on your face, they quickly add that tech support is already on the way!
     * They even created all the room keys this morning; you can take yours now and give them your room deposit once the registration system comes back online.
     * <p>
     * The room key is a small <a href="https://en.wikipedia.org/wiki/Radio-frequency_identification">RFID</a> card.
     * Your room is on the 25th floor and the elevators are also temporarily out of service, so it takes what little energy you have left to even climb the stairs and navigate the halls.
     * You finally reach the door to your room, swipe your card, and - <i>beep</i> - the light turns red.
     * <p>
     * Examining the card more closely, you discover a phone number for tech support.
     * <p>
     * "Hello! How can we help you today?"
     * You explain the situation.
     * <p>
     * "Well, it sounds like the card isn't sending the right command to unlock the door.
     * If you go back to the check-in desk, surely someone there can reset it for you."
     * Still catching your breath, you describe the status of the elevator and the exact number of stairs you just had to climb.
     * <p>
     * "I see! Well, your only other option would be to reverse-engineer the cryptographic handshake the card does with the door and then inject your own commands into the data stream, but that's definitely impossible."
     * You thank them for their time.
     * <p>
     * Unfortunately for the door, you know a thing or two about cryptographic handshakes.
     * <p>
     * The handshake used by the card and the door involves an operation that <i>transforms</i> a <i>subject number</i>.
     * To transform a subject number, start with the value <code>1</code>.
     * Then, a number of times called the <i>loop size</i>, perform the following steps:
     * <ul>
     * <li> Set the value to itself multiplied by the <i>subject number</i>. </li>
     * <li> Set the value to the remainder after dividing the value by <i><code>20201227</code></i>. </li>
     * </ul>
     * <p>
     * The card always uses a specific, secret <i>loop size</i> when it transforms a subject number.
     * The door always uses a different, secret loop size.
     * <p>
     * The cryptographic handshake works like this:
     * <ul>
     * <li>
     * The <i>card</i> transforms the subject number of <code>7</code> according to the <i>card's</i> secret loop size.
     * The result is called the <i>card's public key</i>.
     * </li>
     * <li>
     * The <i>door</i> transforms the subject number of <code>7</code> according to the <i>door's</i> secret loop size.
     * The result is called the <i>door's public key</i>.
     * </li>
     * <li>
     * The card and door use the wireless RFID signal to transmit the two public keys (your puzzle input) to the other device.
     * Now, the <i>card</i> has the <i>door's</i> public key, and the <i>door</i> has the <i>card's</i> public key.
     * Because you can eavesdrop on the signal, you have both public keys, but neither device's loop size.
     * </li>
     * <li>
     * The <i>card</i> transforms the subject number of <i>the door's public key</i> according to the <i>card's</i> loop size.
     * The result is the <i>encryption key</i>.
     * </li>
     * <li>
     * The <i>door</i> transforms the subject number of <i>the card's public key</i> according to the <i>door's</i> loop size.
     * The result is the same <i>encryption key</i> as the <i>card</i> calculated.
     * </li>
     * </ul>
     * <p>
     * If you can use the two public keys to determine each device's loop size, you will have enough information to calculate the secret <i>encryption key</i> that the card and door use to communicate; this would let you send the <code>unlock</code> command directly to the door!
     * <p>
     * For example, suppose you know that the card's public key is <code>5764801</code>.
     * With a little trial and error, you can work out that the card's loop size must be <i><code>8</code></i>, because transforming the initial subject number of <code>7</code> with a loop size of <code>8</code> produces <code>5764801</code>.
     * <p>
     * Then, suppose you know that the door's public key is <code>17807724</code>.
     * By the same process, you can determine that the door's loop size is <i><code>11</code></i>, because transforming the initial subject number of <code>7</code> with a loop size of <code>11</code> produces <code>17807724</code>.
     * <p>
     * At this point, you can use either device's loop size with the other device's public key to calculate the <i>encryption key</i>.
     * Transforming the subject number of <code>17807724</code> (the door's public key) with a loop size of <code>8</code> (the card's loop size) produces the encryption key, <i><code>14897079</code></i>.
     * (Transforming the subject number of <code>5764801</code> (the card's public key) with a loop size of <code>11</code> (the door's loop size) produces the same encryption key: <i><code>14897079</code></i>.)
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What encryption key is the handshake trying to establish?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final long[] keys = context.stream()
                .mapToLong(Long::parseLong)
                .toArray();

        final long cardPublicKey = keys[0];
        final long doorPublicKey = keys[1];

        final int cardLoopSize = bruteForceLoopSize(cardPublicKey);
        final int doorLoopSize = bruteForceLoopSize(doorPublicKey);

        final long encryptionKey1 = transform(doorPublicKey, cardLoopSize);
        final long encryptionKey2 = transform(cardPublicKey, doorLoopSize);
        assert encryptionKey1 == encryptionKey2;

        return encryptionKey1;
    }

    /**
     * The light turns green and the door unlocks.
     * As you collapse onto the bed in your room, your pager goes off!
     * <p>
     * "It's an emergency!" the Elf calling you explains.
     * "The <a href="https://en.wikipedia.org/wiki/Soft_serve">soft serve</a> machine in the cafeteria on sub-basement 7 just failed and you're the only one that knows how to fix it!
     * We've already dispatched a reindeer to your location to pick you up."
     * <p>
     * You hear the sound of hooves landing on your balcony.
     * <p>
     * The reindeer carefully explores the contents of your room while you figure out how you're going to pay the <b>50 stars</b> you owe the resort before you leave.
     * Noticing that you look concerned, the reindeer wanders over to you; you see that it's carrying a small pouch.
     * <p>
     * "Sorry for the trouble," a note in the pouch reads.
     * Sitting at the bottom of the pouch is a gold coin with a little picture of a starfish on it.
     * <p>
     * Looks like you only needed <b>49 stars</b> after all.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return -
     */
    @Part(part = II)
    public NoChallenge calculateAnswerForPart2(final SolutionContext context) {
        return NoChallenge.NO_CHALLENGE;
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Brute force the loop size by transforming the `publicKey` until a
     * match is found.
     */
    private static int bruteForceLoopSize(final long publicKey) {
        long value = 1;
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            if (value == publicKey)
                return i;

            value = transform(7, value);
        }

        throw new IllegalStateException("Failed to brute force the key");
    }

    /*
     * Transform the given `publicKey`.
     */
    private static long transform(final long subject, final int loopSize) {
        long value = 1;
        for (int i = 0; i < loopSize; i++) {
            value = transform(subject, value);
        }
        return value;
    }

    /*
     * Transform the given subject/value.
     */
    private static long transform(final long subject, final long value) {
        return (value * subject) % 20201227;
    }

}
