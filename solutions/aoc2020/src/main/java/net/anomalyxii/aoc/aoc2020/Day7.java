package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 7: Handy Haversacks.
 */
@Solution(year = 2020, day = 7, title = "Handy Haversacks")
public class Day7 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You land at the regional airport in time for your next flight.
     * In fact, it looks like you'll even have time to grab some food: all flights are currently delayed due to <i>issues in luggage processing.</i>
     * <p>
     * Due to recent aviation regulations, many rules (your puzzle input) are being enforced about bags and their contents; bags must be color-coded and must contain specific quantities of other color-coded bags.
     * Apparently, nobody responsible for these regulations considered how long they would take to enforce!
     * <p>
     * For example, consider the following rules:
     * <pre>
     * light red bags contain 1 bright white bag, 2 muted yellow bags.
     * dark orange bags contain 3 bright white bags, 4 muted yellow bags.
     * bright white bags contain 1 shiny gold bag.
     * muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
     * shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
     * dark olive bags contain 3 faded blue bags, 4 dotted black bags.
     * vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
     * faded blue bags contain no other bags.
     * dotted black bags contain no other bags.
     * </pre>
     * <p>
     * These rules specify the required contents for 9 bag types.
     * In this example, every <code>faded blue</code> bag is empty, every <code>vibrant plum</code> bag contains 11 bags (5 <code>faded blue</code> and 6 <code>dotted black</code>), and so on.
     * <p>
     * You have a <i><code>shiny gold</code> bag</i>.
     * If you wanted to carry it in at least one other bag, how many different bag colors would be valid for the outermost bag?
     * (In other words: how many colors can, eventually, contain at least one <code>shiny gold</code> bag?)
     * <p>
     * In the above rules, the following options would be available to you:
     * <ul>
     * <li> A <code>bright white</code> bag, which can hold your <code>shiny gold</code> bag directly. </li>
     * <li> A <code>muted yellow</code> bag, which can hold your <code>shiny gold</code> bag directly, plus some other bags. </li>
     * <li> A <code>dark orange</code> bag, which can hold <code>bright white</code> and <code>muted yellow</code> bags, either of which could then hold your <code>shiny gold</code> bag. </li>
     * <li> A <code>light red</code> bag, which can hold <code>bright white</code> and <code>muted yellow</code> bags, either of which could then hold your <code>shiny gold</code> bag. </li>
     * </ul>
     * So, in this example, the number of bag colors that can eventually contain at least one <code>shiny gold</code> bag is <i><code>4</code></i>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many bag colors can eventually contain at least one <code>shiny gold</code> bag? (The list of rules is quite long; make sure you get all of it.)
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Map<String, Boolean> memo = new HashMap<>();
        final Map<String, BagSpec> bagSpecsByName = new HashMap<>();

        final List<BagSpec> bagSpecs = context.process(Day7::parseBagSpec);
        bagSpecs.forEach(spec -> bagSpecsByName.put(spec.myColour, spec));

        return bagSpecs.stream()
                .filter(spec -> hasPathToShinyGoldBag(spec, bagSpecsByName, memo))
                .count();
    }

    /**
     * It's getting pretty expensive to fly these days - not because of ticket prices, but because of the ridiculous number of bags you need to buy!
     * <p>
     * Consider again your <code>shiny gold</code> bag and the rules from the above example:
     * <ul>
     * <li> <code>faded blue</code> bags contain <code>0</code> other bags. </li>
     * <li> <code>dotted black</code> bags contain <code>0</code> other bags. </li>
     * <li> <code>vibrant plum</code> bags contain <code>11</code> other bags: 5 <code>faded blue</code> bags and 6 <code>dotted black</code> bags. </li>
     * <li> <code>dark olive</code> bags contain <code>7</code> other bags: 3 <code>faded blue</code> bags and 4 <code>dotted black</code> bags. </li>
     * </ul>
     * So, a single <code>shiny gold</code> bag must contain 1 <code>dark olive</code> bag (and the 7 bags within it) plus 2 <code>vibrant plum</code> bags (and the 11 bags within each of those): <code>1 + 1*7 + 2 + 2*11</code> = <i><code>32</code></i> bags!
     * <p>
     * Of course, the actual rules have a small chance of going several levels deeper than this example; be sure to count all of the bags, even if the nesting becomes topologically impractical!
     * <p>
     * Here's another example:
     * <pre>
     * shiny gold bags contain 2 dark red bags.
     * dark red bags contain 2 dark orange bags.
     * dark orange bags contain 2 dark yellow bags.
     * dark yellow bags contain 2 dark green bags.
     * dark green bags contain 2 dark blue bags.
     * dark blue bags contain 2 dark violet bags.
     * dark violet bags contain no other bags.
     * </pre>
     * <p>
     * In this example, a single <code>shiny gold</code> bag must contain <i><code>126</code></i> other bags.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many individual bags are required inside your single <code>shiny gold</code> bag?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Map<String, BagSpec> bagSpecsByName = new HashMap<>();

        final List<BagSpec> bagSpecs = context.process(Day7::parseBagSpec);
        bagSpecs.forEach(spec -> bagSpecsByName.put(spec.myColour, spec));

        return countNestedBags(bagSpecsByName.get("shiny gold"), bagSpecsByName);
    }

    // ****************************************
    // Helper Members
    // ****************************************

    private static final Pattern SPECIFICATION_PATTERN = Pattern.compile("([a-z ]+) bags contain ([0-9a-z, ]+)[.]", Pattern.CASE_INSENSITIVE);
    private static final Pattern CONTAINER_PATTERN = Pattern.compile("([0-9]+) ([a-z ]+) bags?,?", Pattern.CASE_INSENSITIVE);

    /**
     * Parse the given bag specification into a {@link BagSpec}.
     *
     * @param spec the specification
     * @return the {@link BagSpec}
     * @throws IllegalArgumentException if the specification is invalid
     */
    static BagSpec parseBagSpec(final String spec) {
        final Matcher m;
        if (!(m = SPECIFICATION_PATTERN.matcher(spec)).matches()) {
            throw new IllegalArgumentException("Invalid bag specification: '" + spec + "'");
        }

        final String myColour = m.group(1);
        final Map<String, Integer> childBags = parseChildBagSpec(m);
        return new BagSpec(myColour, childBags);
    }

    // ****************************************
    // Private Helper Members
    // ****************************************

    /*
     * Parse the list of child (contained) bag specifications.
     */
    private static Map<String, Integer> parseChildBagSpec(final Matcher m) {
        final String childBagsSpec = m.group(2);
        final Map<String, Integer> childBags = new HashMap<>();
        final Matcher c = CONTAINER_PATTERN.matcher(childBagsSpec);
        while (c.find()) {
            childBags.put(c.group(2), Integer.parseInt(c.group(1)));
        }
        return childBags;
    }

    /*
     * Determine whether this `BagSpec` has a path to containing a
     * "shiny gold" bag.
     */
    private static boolean hasPathToShinyGoldBag(
            final BagSpec spec,
            final Map<String, BagSpec> allSpecs,
            final Map<String, Boolean> memo
    ) {
        if (memo.containsKey(spec.myColour)) {
            return memo.get(spec.myColour);
        }

        for (final Map.Entry<String, Integer> entry : spec.childBags.entrySet()) {
            final String ref = entry.getKey();
            final Integer count = entry.getValue();
            if (count > 0) {
                final boolean result = "shiny gold".equalsIgnoreCase(ref)
                        || hasPathToShinyGoldBag(allSpecs.get(ref), allSpecs, memo);
                if (result) {
                    memo.put(spec.myColour, true);
                    return true;
                }
            }
        }

        memo.put(spec.myColour, false);
        return false;
    }

    /*
     * Count the number of bags contained inside a given colour.
     */
    private static long countNestedBags(final BagSpec spec, final Map<String, BagSpec> allBags) {
        long result = 0;
        for (final Map.Entry<String, Integer> entry : spec.childBags.entrySet()) {
            final String ref = entry.getKey();
            final Integer count = entry.getValue();

            result += (count * (1 + countNestedBags(allBags.get(ref), allBags)));
        }
        return result;
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /**
     * Holds the specification of a bag.
     */
    static class BagSpec {

        // Members

        final String myColour;
        final Map<String, Integer> childBags = new HashMap<>();

        // Constructors

        BagSpec(final String myColour, final Map<String, Integer> childBags) {
            this.myColour = requireNonNull(myColour, "My colour must not be null!");
            this.childBags.putAll(requireNonNull(childBags, "Child bags must not be null!"));
        }

    }

}
