package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.List;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 5: Binary Boarding.
 */
@Solution(year = 2020, day = 5, title = "Binary Boarding")
public class Day5 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You board your plane only to discover a new problem: you dropped your boarding pass!
     * You aren't sure which seat is yours, and all of the flight attendants are busy with the flood of people that suddenly made it through passport control.
     * <p>
     * You write a quick program to use your phone's camera to scan all of the nearby boarding passes (your puzzle input); perhaps you can find your seat through process of elimination.
     * <p>
     * Instead of <a href="https://www.youtube.com/watch?v=oAHbLRjF0vo">zones or groups</a>, this airline uses <i>binary space partitioning</i> to seat people.
     * A seat might be specified like <code>FBFBBFFRLR</code>, where <code>F</code> means "front", <code>B</code> means "back", <code>L</code> means "left", and <code>R</code> means "right".
     * <p>
     * The first 7 characters will either be <code>F</code> or <code>B</code>; these specify exactly one of the <i>128 rows</i> on the plane (numbered <code>0</code> through <code>127</code>).
     * Each letter tells you which half of a region the given seat is in.
     * Start with the whole list of rows; the first letter indicates whether the seat is in the <i>front</i> (<code>0</code> through <code>63</code>) or the <i>back</i> (<code>64</code> through <code>127</code>).
     * The next letter indicates which half of that region the seat is in, and so on until you're left with exactly one row.
     * <p>
     * For example, consider just the first seven characters of <code>FBFBBFFRLR</code>:
     * <ul>
     * <li> Start by considering the whole range, rows <code>0</code> through <code>127</code>. </li>
     * <li> <code>F</code> means to take the <i>lower half</i>, keeping rows <code>0</code> through <code>63</code>. </li>
     * <li> <code>B</code> means to take the <i>upper half</i>, keeping rows <code>32</code> through <code>63</code>. </li>
     * <li> <code>F</code> means to take the <i>lower half</i>, keeping rows <code>32</code> through <code>47</code>. </li>
     * <li> <code>B</code> means to take the <i>upper half</i>, keeping rows <code>40</code> through <code>47</code>. </li>
     * <li> <code>B</code> keeps rows <code>44</code> through <code>47</code>. </li>
     * <li> <code>F</code> keeps rows <code>44</code> through <code>45</code>. </li>
     * <li> The final <code>F</code> keeps the lower of the two, <i>row <code>44</code></i>. </li>
     * </ul>
     * The last three characters will be either <code>L</code> or <code>R</code>; these specify exactly one of the <i>8 columns</i> of seats on the plane (numbered <code>0</code> through <code>7</code>).
     * The same process as above proceeds again, this time with only three steps.
     * <code>L</code> means to keep the <i>lower half</i>, while <code>R</code> means to keep the <i>upper half</i>.
     * <p>
     * For example, consider just the last 3 characters of <code>FBFBBFFRLR</code>:
     * <ul>
     * <li> Start by considering the whole range, columns <code>0</code> through <code>7</code>. </li>
     * <li> <code>R</code> means to take the <i>upper half</i>, keeping columns <code>4</code> through <code>7</code>. </li>
     * <li> <code>L</code> means to take the <i>lower half</i>, keeping columns <code>4</code> through <code>5</code>. </li>
     * <li> The final <code>R</code> keeps the upper of the two, <i>column <code>5</code></i>. </li>
     * </ul>
     * So, decoding <code>FBFBBFFRLR</code> reveals that it is the seat at <i>row <code>44</code>, column <code>5</code></i>.
     * <p>
     * Every seat also has a unique <i>seat ID</i>: multiply the row by 8, then add the column.
     * In this example, the seat has ID <code>44 * 8 + 5 = <i>357</i></code>.
     * <p>
     * Here are some other boarding passes:
     * <ul>
     * <li> <code>BFFFBBFRRR</code>: row <code>70</code>, column <code>7</code>, seat ID <code>567</code>. </li>
     * <li> <code>FFFBBBFRRR</code>: row <code>14</code>, column <code>7</code>, seat ID <code>119</code>. </li>
     * <li> <code>BBFFBBFRLL</code>: row <code>102</code>, column <code>4</code>, seat ID <code>820</code>. </li>
     * </ul>
     * As a sanity check, look through your list of boarding passes.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the highest seat ID on a boarding pass?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .mapToLong(Day5::calculateSeatId)
                .max()
                .orElse(0);
    }

    /**
     * <i>Ding!</i>
     * The "fasten seat belt" signs have turned on.
     * Time to find your seat.
     * <p>
     * It's a completely full flight, so your seat should be the only missing boarding pass in your list.
     * However, there's a catch: some of the seats at the very front and back of the plane don't exist on this aircraft, so they'll be missing from your list as well.
     * <p>
     * Your seat wasn't at the very front or back, though; the seats with IDs +1 and -1 from yours will be in your list.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the ID of your seat?
     * @throws IllegalStateException if more than one seat was found to be missing
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<Long> seats = context.stream()
                .map(Day5::calculateSeatId)
                .sorted()
                .toList();

        long candidateForMySeat = -1;
        for (int i = 1; i < seats.size(); i++) {
            final long prevSeatId = seats.get(i - 1);
            final long thisSeatId = seats.get(i);

            if (prevSeatId != (thisSeatId - 1)) {
                if (candidateForMySeat >= 0) {
                    throw new IllegalStateException("Ambiguous missing seat!");
                }

                candidateForMySeat = thisSeatId - 1;
            }
        }

        return candidateForMySeat;
    }

    // ****************************************
    // Helper Members
    // ****************************************

    /**
     * Calculate the ID of a seat, given the boarding pass coordinates.
     *
     * @param pass the boarding pass
     * @return the seat ID
     */
    static long calculateSeatId(final String pass) {
        if (!pass.matches("[FB]{7}[LR]{3}")) {
            throw new IllegalArgumentException("Invalid boarding pass: " + pass);
        }

        int seat = 0;
        for (final char c : pass.toCharArray()) {
            seat = (seat << 1) + ((c == 'B' || c == 'R') ? 1 : 0);
        }

        return seat;
    }

}
