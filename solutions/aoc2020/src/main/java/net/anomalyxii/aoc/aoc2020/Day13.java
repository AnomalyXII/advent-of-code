package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 13: Shuttle Search.
 */
@Solution(year = 2020, day = 13, title = "Shuttle Search")
public class Day13 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Your ferry can make it safely to a nearby port, but it won't get much further.
     * When you call to book another ship, you discover that no ships embark from that port to your vacation island.
     * You'll need to get from the port to the nearest airport.
     * <p>
     * Fortunately, a shuttle bus service is available to bring you from the sea port to the airport!
     * Each bus has an ID number that also indicates <i>how often the bus leaves for the airport</i>.
     * <p>
     * Bus schedules are defined based on a <i>timestamp</i> that measures the <i>number of minutes</i> since some fixed reference point in the past.
     * At timestamp 0, every bus simultaneously departed from the sea port.
     * After that, each bus travels to the airport, then various other locations, and finally returns to the sea port to repeat its journey forever.
     * <p>
     * The time this loop takes a particular bus is also its ID number: the bus with ID <code>5</code> departs from the sea port at timestamps <code>0</code>, <code>5</code>, <code>10</code>, <code>15</code>, and so on.
     * The bus with ID <code>11</code> departs at <code>0</code>, <code>11</code>, <code>22</code>, <code>33</code>, and so on.
     * If you are there when the bus departs, you can ride that bus to the airport!
     * <p>
     * Your notes (your puzzle input) consist of two lines.
     * The first line is your estimate of the <i>earliest timestamp you could depart on a bus</i>.
     * The second line lists the bus IDs that are in service according to the shuttle company; entries that show <code>x</code> must be out of service, so you decide to ignore them.
     * <p>
     * To save time once you arrive, your goal is to figure out the <i>earliest bus you can take to the airport</i>.
     * (There will be exactly one such bus.)
     * <p>
     * For example, suppose you have the following notes:
     * <pre>
     * 939
     * 7,13,x,x,59,x,31,19
     * </pre>
     * <p>
     * Here, the earliest timestamp you could depart is <code>939</code>, and the bus IDs in service are <code>7</code>, <code>13</code>, <code>59</code>, <code>31</code>, and <code>19</code>.
     * Near timestamp 939, these bus IDs depart at the times marked <code>D</code>:
     * <pre>
     * time   bus 7   bus 13  bus 59  bus 31  bus 19
     * 929      .       .       .       .       .
     * 930      .       .       .       D       .
     * 931      D       .       .       .       D
     * 932      .       .       .       .       .
     * 933      .       .       .       .       .
     * 934      .       .       .       .       .
     * 935      .       .       .       .       .
     * 936      .       D       .       .       .
     * 937      .       .       .       .       .
     * 938      D       .       .       .       .
     * 939      .       .       .       .       .
     * 940      .       .       .       .       .
     * 941      .       .       .       .       .
     * 942      .       .       .       .       .
     * 943      .       .       .       .       .
     * 944      .       .       D       .       .
     * 945      D       .       .       .       .
     * 946      .       .       .       .       .
     * 947      .       .       .       .       .
     * 948      .       .       .       .       .
     * 949      .       D       .       .       .
     * </pre>
     * <p>
     * The earliest bus you could take is bus ID <code>59</code>.
     * It doesn't depart until timestamp <code>944</code>, so you would need to wait <code>944 - 939 = 5</code> minutes before it departs.
     * Multiplying the bus ID by the number of minutes you'd need to wait gives <i><code>295</code></i>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the ID of the earliest bus you can take to the airport multiplied by the number of minutes you'll need to wait for that bus?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<String> lines = context.read();

        final long arrivalTime = Long.parseLong(lines.getFirst());
        final int[] busIds = Arrays.stream(lines.get(1).split(","))
                .filter(id -> !"x".equalsIgnoreCase(id))
                .mapToInt(Integer::parseInt)
                .toArray();

        long id = -1;
        long remainingMinutes = Long.MAX_VALUE;

        for (final int busId : busIds) {
            final long lastDepartureTime = (arrivalTime / busId) * busId;
            final long nextDepartureTime = lastDepartureTime + busId;

            final long timeToWait = nextDepartureTime - arrivalTime;
            if (timeToWait < remainingMinutes) {
                id = busId;
                remainingMinutes = timeToWait;
            }
        }

        return id * remainingMinutes;
    }

    /**
     * The shuttle company is running a contest: one gold coin for anyone that can find the earliest timestamp such that the first bus ID departs at that time and each subsequent listed bus ID departs at that subsequent minute.
     * (The first line in your input is no longer relevant.)
     * <p>
     * For example, suppose you have the same list of bus IDs as above:
     * <pre>
     * 7,13,x,x,59,x,31,19
     * </pre>
     * <p>
     * An <code>x</code> in the schedule means there are no constraints on what bus IDs must depart at that time.
     * <p>
     * This means you are looking for the earliest timestamp (called t) such that:
     * <p>
     * - Bus ID <code>7</code> departs at timestamp <code>t</code>.
     * - Bus ID <code>13</code> departs one minute after timestamp <code>t</code>.
     * - There are no requirements or restrictions on departures at two or three minutes after timestamp <code>t</code>.
     * - Bus ID <code>59</code> departs four minutes after timestamp <code>t</code>.
     * - There are no requirements or restrictions on departures at five minutes after timestamp <code>t</code>.
     * - Bus ID <code>31</code> departs six minutes after timestamp <code>t</code>.
     * - Bus ID <code>19</code> departs seven minutes after timestamp <code>t</code>.
     * <p>
     * The only bus departures that matter are the listed bus IDs at their specific offsets from <code>t</code>.
     * Those bus IDs can depart at other times, and other bus IDs can depart at those times.
     * For example, in the list above, because bus ID <code>19</code> must depart seven minutes after the timestamp at which bus ID <code>7</code> departs, bus ID <code>7</code> will always also be departing with bus ID <code>19</code> at seven minutes after timestamp <code>t</code>.
     * <p>
     * In this example, the earliest timestamp at which this occurs is <i><code>1068781</code></i>:
     * <pre>
     * time     bus 7   bus 13  bus 59  bus 31  bus 19
     * 1068773    .       .       .       .       .
     * 1068774    D       .       .       .       .
     * 1068775    .       .       .       .       .
     * 1068776    .       .       .       .       .
     * 1068777    .       .       .       .       .
     * 1068778    .       .       .       .       .
     * 1068779    .       .       .       .       .
     * 1068780    .       .       .       .       .
     * 1068781    D       .       .       .       .
     * 1068782    .       D       .       .       .
     * 1068783    .       .       .       .       .
     * 1068784    .       .       .       .       .
     * 1068785    .       .       D       .       .
     * 1068786    .       .       .       .       .
     * 1068787    .       .       .       D       .
     * 1068788    D       .       .       .       D
     * 1068789    .       .       .       .       .
     * 1068790    .       .       .       .       .
     * 1068791    .       .       .       .       .
     * 1068792    .       .       .       .       .
     * 1068793    .       .       .       .       .
     * 1068794    .       .       .       .       .
     * 1068795    D       D       .       .       .
     * 1068796    .       .       .       .       .
     * 1068797    .       .       .       .       .
     * </pre>
     * <p>
     * In the above example, bus ID <code>7</code> departs at timestamp <code>1068788</code> (seven minutes after <code>t</code>).
     * This is fine; the only requirement on that minute is that bus ID <code>19</code> departs then, and it does.
     * <p>
     * Here are some other examples:
     * <ul>
     * <li> The earliest timestamp that matches the list <code>17,x,13,19</code> is <i><code>3417</code></i>. </li>
     * <li> <code>67,7,59,61</code> first occurs at timestamp <i><code>754018</code></i>. </li>
     * <li> <code>67,x,7,59,61</code> first occurs at timestamp <i><code>779210</code></i>. </li>
     * <li> <code>67,7,x,59,61</code> first occurs at timestamp <i><code>1261476</code></i>. </li>
     * <li> <code>1789,37,47,1889</code> first occurs at timestamp <i><code>1202161486</code></i>. </li>
     * </ul>
     * However, with so many bus IDs in your list, surely the actual earliest timestamp will be larger than <code>100000000000000</code>!
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the earliest timestamp such that all of the listed bus IDs depart at offsets matching their positions in the list?
     * @throws IllegalStateException if no solution can be found
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final int[] busIds = Arrays.stream(context.read().get(1).split(","))
                .mapToInt(id -> "x".equalsIgnoreCase(id)
                        ? -1
                        : Integer.parseInt(id))
                .toArray();

        final int[] offsets = IntStream.range(0, busIds.length)
                .filter(i -> busIds[i] > 0)
                .toArray();

        final int[] filteredBusIds = IntStream.of(offsets)
                .map(i -> busIds[i])
                .toArray();


        long stepSize = filteredBusIds[0];

        outer:
        for (long time = stepSize; time < Long.MAX_VALUE; time += stepSize) {
            for (int i = 0; i < offsets.length; i++) {
                if ((time + offsets[i]) % filteredBusIds[i] != 0) {
                    final long newMultiplier = Arrays.stream(filteredBusIds, 0, i)
                            .reduce(1, (x, y) -> x * y);

                    if (newMultiplier > stepSize) {
                        stepSize = newMultiplier;
                    }

                    continue outer;
                }
            }
            return time;
        }

        throw new IllegalStateException("Did not find a solution :(");
    }

}
