package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 17: Conway Cubes.
 */
@Solution(year = 2020, day = 17, title = "Conway Cubes")
public class Day17 {

    /*
     * The number of rounds to simulate.
     */
    private static final int NUMBER_OF_ROUNDS = 6;

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * As your flight slowly drifts through the sky, the Elves at the Mythical Information Bureau at the North Pole contact you.
     * They'd like some help debugging a malfunctioning experimental energy source aboard one of their super-secret imaging satellites.
     * <p>
     * The experimental energy source is based on cutting-edge technology: a set of Conway Cubes contained in a pocket dimension!
     * When you hear it's having problems, you can't help but agree to take a look.
     * <p>
     * The pocket dimension contains an infinite 3-dimensional grid.
     * At every integer 3-dimensional coordinate (<code>x,y,z</code>), there exists a single cube which is either <i>active</i> or <i>inactive</i>.
     * <p>
     * In the initial state of the pocket dimension, almost all cubes start <i>inactive</i>.
     * The only exception to this is a small flat region of cubes (your puzzle input); the cubes in this region start in the specified <i>active</i> (<code>#</code>) or <i>inactive</i> (<code>.</code>) state.
     * <p>
     * The energy source then proceeds to boot up by executing six <i>cycles</i>.
     * <p>
     * Each cube only ever considers its <i>neighbors</i>: any of the 26 other cubes where any of their coordinates differ by at most <code>1</code>.
     * For example, given the cube at <code>x=1,y=2,z=3</code>, its neighbors include the cube at <code>x=2,y=2,z=2</code>, the cube at <code>x=0,y=2,z=3</code>, and so on.
     * <p>
     * During a cycle, <i>all</i> cubes <i>simultaneously</i> change their state according to the following rules:
     * <ul>
     * <li>
     * If a cube is <i>active</i> and <i>exactly <code>2</code> or <code>3</code></i> of its neighbors are also active, the cube remains <i>active</i>.
     * Otherwise, the cube becomes <i>inactive</i>.
     * </li>
     * <li>
     * If a cube is <i>inactive</i> but <i>exactly <code>3</code></i> of its neighbors are active, the cube becomes <i>active</i>.
     * Otherwise, the cube remains <i>inactive</i>.
     * </li>
     * </ul>
     * <p>
     * The engineers responsible for this experimental energy source would like you to simulate the pocket dimension and determine what the configuration of cubes should be at the end of the six-cycle boot process.
     * <p>
     * For example, consider the following initial state:
     * <pre>
     * .#.
     * ..#
     * ###
     * </pre>
     * <p>
     * Even though the pocket dimension is 3-dimensional, this initial state represents a small 2-dimensional slice of it.
     * (In particular, this initial state defines a 3x3x1 region of the 3-dimensional space.)
     * <p>
     * Simulating a few cycles from this initial state produces the following configurations, where the result of each cycle is shown layer-by-layer at each given z coordinate (and the frame of view follows the active cells in each cycle):
     * <pre>
     * Before any cycles:
     *
     * z=0
     * .#.
     * ..#
     * ###
     *
     *
     * After 1 cycle:
     *
     * z=-1
     * #..
     * ..#
     * .#.
     *
     * z=0
     * #.#
     * .##
     * .#.
     *
     * z=1
     * #..
     * ..#
     * .#.
     *
     *
     * After 2 cycles:
     *
     * z=-2
     * .....
     * .....
     * ..#..
     * .....
     * .....
     *
     * z=-1
     * ..#..
     * .#..#
     * ....#
     * .#...
     * .....
     *
     * z=0
     * ##...
     * ##...
     * #....
     * ....#
     * .###.
     *
     * z=1
     * ..#..
     * .#..#
     * ....#
     * .#...
     * .....
     *
     * z=2
     * .....
     * .....
     * ..#..
     * .....
     * .....
     *
     *
     * After 3 cycles:
     *
     * z=-2
     * .......
     * .......
     * ..##...
     * ..###..
     * .......
     * .......
     * .......
     *
     * z=-1
     * ..#....
     * ...#...
     * #......
     * .....##
     * .#...#.
     * ..#.#..
     * ...#...
     *
     * z=0
     * ...#...
     * .......
     * #......
     * .......
     * .....##
     * .##.#..
     * ...#...
     *
     * z=1
     * ..#....
     * ...#...
     * #......
     * .....##
     * .#...#.
     * ..#.#..
     * ...#...
     *
     * z=2
     * .......
     * .......
     * ..##...
     * ..###..
     * .......
     * .......
     * .......
     * </pre>
     * <p>
     * After the full six-cycle boot process completes, <i><code>112</code></i> cubes are left in the <i>active</i> state.
     * <p>
     * Starting with your given initial configuration, simulate six cycles.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many cubes are left in the active state after the sixth cycle?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final char[][] baseLayer = context.stream()
                .map(String::toCharArray)
                .toArray(char[][]::new);

        return run3dSimulation(NUMBER_OF_ROUNDS, baseLayer);
    }

    /**
     * For some reason, your simulated results don't match what the experimental energy source engineers expected.
     * Apparently, the pocket dimension actually has <i>four spatial dimensions</i>, not three.
     * <p>
     * The pocket dimension contains an infinite 4-dimensional grid.
     * At every integer 4-dimensional coordinate (x,y,z,w), there exists a single cube (really, a <i>hypercube</i>) which is still either <i>active</i> or <i>inactive</i>.
     * <p>
     * Each cube only ever considers its <i>neighbors</i>: any of the 80 other cubes where any of their coordinates differ by at most <code>1</code>.
     * For example, given the cube at <code>x=1,y=2,z=3,w=4</code>, its neighbors include the cube at <code>x=2,y=2,z=3,w=3</code>, the cube at <code>x=0,y=2,z=3,w=4</code>, and so on.
     * <p>
     * The initial state of the pocket dimension still consists of a small flat region of cubes.
     * Furthermore, the same rules for cycle updating still apply: during each cycle, consider the <i>number of active neighbors</i> of each cube.
     * <p>
     * For example, consider the same initial state as in the example above.
     * Even though the pocket dimension is 4-dimensional, this initial state represents a small 2-dimensional slice of it.
     * (In particular, this initial state defines a 3x3x1x1 region of the 4-dimensional space.)
     * <p>
     * Simulating a few cycles from this initial state produces the following configurations, where the result of each cycle is shown layer-by-layer at each given <code>z</code> and <code>w</code> coordinate:
     * <pre>
     * Before any cycles:
     *
     * z=0, w=0
     * .#.
     * ..#
     * ###
     *
     *
     * After 1 cycle:
     *
     * z=-1, w=-1
     * #..
     * ..#
     * .#.
     *
     * z=0, w=-1
     * #..
     * ..#
     * .#.
     *
     * z=1, w=-1
     * #..
     * ..#
     * .#.
     *
     * z=-1, w=0
     * #..
     * ..#
     * .#.
     *
     * z=0, w=0
     * #.#
     * .##
     * .#.
     *
     * z=1, w=0
     * #..
     * ..#
     * .#.
     *
     * z=-1, w=1
     * #..
     * ..#
     * .#.
     *
     * z=0, w=1
     * #..
     * ..#
     * .#.
     *
     * z=1, w=1
     * #..
     * ..#
     * .#.
     *
     *
     * After 2 cycles:
     *
     * z=-2, w=-2
     * .....
     * .....
     * ..#..
     * .....
     * .....
     *
     * z=-1, w=-2
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=0, w=-2
     * ###..
     * ##.##
     * #...#
     * .#..#
     * .###.
     *
     * z=1, w=-2
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=2, w=-2
     * .....
     * .....
     * ..#..
     * .....
     * .....
     *
     * z=-2, w=-1
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=-1, w=-1
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=0, w=-1
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=1, w=-1
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=2, w=-1
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=-2, w=0
     * ###..
     * ##.##
     * #...#
     * .#..#
     * .###.
     *
     * z=-1, w=0
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=0, w=0
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=1, w=0
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=2, w=0
     * ###..
     * ##.##
     * #...#
     * .#..#
     * .###.
     *
     * z=-2, w=1
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=-1, w=1
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=0, w=1
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=1, w=1
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=2, w=1
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=-2, w=2
     * .....
     * .....
     * ..#..
     * .....
     * .....
     *
     * z=-1, w=2
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=0, w=2
     * ###..
     * ##.##
     * #...#
     * .#..#
     * .###.
     *
     * z=1, w=2
     * .....
     * .....
     * .....
     * .....
     * .....
     *
     * z=2, w=2
     * .....
     * .....
     * ..#..
     * .....
     * .....
     * </pre>
     * <p>
     * After the full six-cycle boot process completes, <i><code>848</code></i> cubes are left in the <i>active</i> state.
     * <p>
     * Starting with your given initial configuration, simulate six cycles in a 4-dimensional space.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many cubes are left in the active state after the sixth cycle?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final char[][] baseLayer = context.stream()
                .map(String::toCharArray)
                .toArray(char[][]::new);

        return run4dSimulation(NUMBER_OF_ROUNDS, baseLayer);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Run the simulation against a 3-dimensional space.
     */
    private long run3dSimulation(final int rounds, final char[][] baseLayer) {
        char[][][] allLayers = new char[][][]{baseLayer};

        final AtomicLong activeCubes = new AtomicLong(0);

        // Pre-count active cubes...
        forEachCubeInLayer(baseLayer, (x, y, c) -> activeCubes.addAndGet(c == '#' ? 1 : 0));

        for (int round = 0; round < rounds; round++) {
            final char[][][] mutatedLayers = copyAndGrowLayer(allLayers);

            for (int z = 0; z < mutatedLayers.length; z++) {
                // Pre-grow each layer, for safety...
                mutatedLayers[z] = copyAndGrowLayer(mutatedLayers[z]);
            }

            updateEachCubeInLayer(mutatedLayers, (x, y, z, c) -> {
                final int surroundingCubes = countSurroundingCubes(mutatedLayers, x, y, z);

                if (c == '.' && (surroundingCubes == 3)) {
                    // Activating
                    activeCubes.addAndGet(computeCountModifier(z));
                    return '!';
                }

                if (c == '#' && (surroundingCubes < 2 || surroundingCubes > 3)) {
                    // Deactivating
                    activeCubes.addAndGet(-computeCountModifier(z));
                    return '~';
                }

                return c;
            });

            updateEachCubeInLayer(mutatedLayers, (x, y, z, c) -> {
                if (c == '~') return '.';
                if (c == '!') return '#';
                return c;
            });

            allLayers = mutatedLayers;
        }

        return activeCubes.longValue();
    }

    /*
     * Run the simulation against a 4-dimensional space.
     */
    private long run4dSimulation(final int rounds, final char[][] baseLayer) {
        char[][][][] allLayers = new char[][][][]{new char[][][]{baseLayer}};

        final AtomicLong activeCubes = new AtomicLong(0);

        // Pre-count active cubes...
        forEachCubeInLayer(baseLayer, (x, y, c) -> activeCubes.addAndGet(c == '#' ? 1 : 0));

        for (int round = 0; round < rounds; round++) {
            final char[][][][] mutatedLayers = copyAndGrowLayer(allLayers);

            for (int w = 0; w < mutatedLayers.length; w++) {
                mutatedLayers[w] = copyAndGrowLayer(mutatedLayers[w]);
                for (int z = 0; z < mutatedLayers[w].length; z++) {
                    // Pre-grow each layer, for safety...
                    mutatedLayers[w][z] = copyAndGrowLayer(mutatedLayers[w][z]);
                }
            }

            updateEachCubeInLayer(mutatedLayers, (x, y, z, w, c) -> {
                final int surroundingCubes = countSurroundingCubes(mutatedLayers, x, y, z, w);

                if (c == '.' && (surroundingCubes == 3)) {
                    // Activating...
                    activeCubes.addAndGet(computeCountModifier(z, w));
                    return '!';
                }

                if (c == '#' && (surroundingCubes < 2 || surroundingCubes > 3)) {
                    // Deactivating...
                    activeCubes.addAndGet(-computeCountModifier(z, w));
                    return '~';
                }

                return c;
            });

            updateEachCubeInLayer(mutatedLayers, (x, y, z, w, c) -> {
                if (c == '~') return '.';
                if (c == '!') return '#';
                return c;
            });


            allLayers = mutatedLayers;
        }

        return activeCubes.longValue();
    }

    /*
     * Compute the number of times a cube is reflected in 4D space, based on
     * the 'w' and 'z' coordinates.
     */
    private static int computeCountModifier(final int z, final int w) {
        return computeCountModifier(z) * computeCountModifier(w);
    }

    /*
     * Compute the number of times a cube is reflected in 3D space, based on
     * the 'z' coordinate.
     */
    private static int computeCountModifier(final int z) {
        return z == 0 ? 1 : 2;
    }

    /*
     * Transform each cube in a 4D space.
     */
    private static void updateEachCubeInLayer(final char[][][][] layers, final CubeFunction4d function) {
        forEachCubeInLayer(layers, (w) -> forEachCubeInLayer(layers[w], (x, y, z, c) -> layers[w][z][y][x] = function.apply(x, y, z, w, c)));
    }

    /*
     * Transform each cube in a 3D space.
     */
    private static void updateEachCubeInLayer(final char[][][] layers, final CubeFunction3d function) {
        forEachCubeInLayer(layers, (z) -> forEachCubeInLayer(layers[z], (x, y, c) -> layers[z][y][x] = function.apply(x, y, z, c)));
    }

    /*
     * Process each cube in a 4D space.
     */
    private static void forEachCubeInLayer(final char[][][][] layers, final LayerConsumer4d consumer) {
        IntStream.range(0, layers.length)
                .forEach(consumer::accept);
    }

    /*
     * Process each 2D layer in a 3D space.
     */
    private static void forEachCubeInLayer(final char[][][] layers, final LayerConsumer3d consumer) {
        IntStream.range(0, layers.length)
                .forEach(consumer::accept);
    }

    /*
     * Process each cube in a 3D space.
     */
    private static void forEachCubeInLayer(final char[][][] layers, final CubeConsumer3d consumer) {
        forEachCubeInLayer(layers, (z) -> forEachCubeInLayer(layers[z], (x, y, c) -> consumer.accept(x, y, z, c)));
    }

    /*
     * Process each cube in a 2D space.
     */
    private static void forEachCubeInLayer(final char[][] layer, final CubeConsumer consumer) {
        for (int y = 0; y < layer.length; y++) {
            for (int x = 0; x < layer[y].length; x++) {
                consumer.accept(x, y, layer[y][x]);
            }
        }
    }

    /*
     * Get the 3D layer for a given 'w' coordinate in 4D space.
     */
    private static char[][][] getLayerAtDepth(final char[][][][] layers, final int w) {
        final int idx = w < 0 ? -w : w; // Mirror z...
        return idx < layers.length ? layers[idx] : null;
    }

    /*
     * Get the 2D layer for a given 'z' coordinate in 3D space.
     */
    private static char[][] getLayerAtDepth(final char[][][] layers, final int z) {
        final int idx = z < 0 ? -z : z; // Mirror z...
        return idx < layers.length ? layers[idx] : null;
    }

    /*
     * Count the number of active cubes surrounding a point in 4D space.
     */
    private int countSurroundingCubes(final char[][][][] layer, final int x, final int y, final int z, final int w) {
        int surroundingCubes = 0;
        for (int dw = -1; dw <= 1; dw++) {
            for (int dz = -1; dz <= 1; dz++) {
                for (int dy = -1; dy <= 1; dy++) {
                    for (int dx = -1; dx <= 1; dx++) {
                        if ((dx | dy | dz | dw) != 0 && isCubeActive(layer, x + dx, y + dy, z + dz, w + dw)) {
                            ++surroundingCubes;
                        }
                    }
                }
            }
        }
        return surroundingCubes;
    }

    /*
     * Count the number of active cubes surrounding a point in 3D space.
     */
    private int countSurroundingCubes(final char[][][] layers, final int x, final int y, final int z) {
        int surroundingCubes = 0;
        for (int dz = -1; dz <= 1; dz++) {
            for (int dy = -1; dy <= 1; dy++) {
                for (int dx = -1; dx <= 1; dx++) {
                    if ((dx | dy | dz) != 0 && isCubeActive(layers, x + dx, y + dy, z + dz)) {
                        ++surroundingCubes;
                    }
                }
            }
        }
        return surroundingCubes;
    }

    /*
     * Check if the cube at a given 4d coordinate is active.
     */
    private static boolean isCubeActive(final char[][][][] layers, final int x, final int y, final int z, final int w) {
        final char[][][] layer = getLayerAtDepth(layers, w);
        return layer != null && isCubeActive(layer, x, y, z);
    }

    /*
     * Check if the cube at a given 3d coordinate is active.
     */
    private static boolean isCubeActive(final char[][][] layers, final int x, final int y, final int z) {
        final char[][] layer = getLayerAtDepth(layers, z);
        return layer != null
                && isCubeActive(layer, x, y);
    }

    /*
     * Check if the cube at a given 2d coordinate is active.
     */
    private static boolean isCubeActive(final char[][] layer, final int x, final int y) {
        return (y >= 0 && y < layer.length)
                && (x >= 0 && x < layer[y].length)
                && (layer[y][x] == '#' || layer[y][x] == '~');
    }

    /*
     * Copy and grow a 4D layer.
     */
    private char[][][][] copyAndGrowLayer(final char[][][][] allLayers) {
        final char[][][][] mutatedLayers = new char[allLayers.length + 1][][][];
        System.arraycopy(allLayers, 0, mutatedLayers, 0, allLayers.length);

        final char[][][] lastLayer = allLayers[allLayers.length - 1];
        mutatedLayers[allLayers.length] = newLayer(lastLayer[0][0].length, lastLayer[0].length, lastLayer.length);
        return mutatedLayers;
    }

    /*
     * Copy and grow a 3D layer.
     */
    private char[][][] copyAndGrowLayer(final char[][][] layers) {
        final char[][][] mutatedLayers = new char[layers.length + 1][][];
        System.arraycopy(layers, 0, mutatedLayers, 0, layers.length);

        final char[][] lastLayer = layers[layers.length - 1];
        mutatedLayers[layers.length] = newLayer(lastLayer[0].length, lastLayer.length);
        return mutatedLayers;
    }

    /*
     * Copy and grow a 2D layer.
     */
    private char[][] copyAndGrowLayer(final char[][] layer) {
        final int height = layer.length;
        final int width = layer[0].length;

        final char[][] mutatedLayer = new char[height + 2][];
        for (int y = 0; y < mutatedLayer.length; y++) {
            mutatedLayer[y] = new char[width + 2];
            Arrays.fill(mutatedLayer[y], '.');
            if (y >= 1 && y <= height) {
                System.arraycopy(layer[y - 1], 0, mutatedLayer[y], 1, width);
            }
        }

        return mutatedLayer;
    }

    /*
     * Create a new 3D layer.
     */
    private char[][][] newLayer(final int width, final int height, final int depth) {
        final char[][][] layer = new char[depth][][];
        for (int i = 0; i < depth; i++) {
            layer[i] = newLayer(width, height);
        }
        return layer;
    }

    /*
     * Create a new 2D layer.
     */
    private char[][] newLayer(final int width, final int height) {
        final char[][] layer = new char[height][];
        for (int y = 0; y < height; y++) {
            layer[y] = new char[width];
            Arrays.fill(layer[y], '.');
        }
        return layer;
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Consume a 2D slice of 4d space.
     */
    @FunctionalInterface
    private interface LayerConsumer4d {
        void accept(int z);
    }

    /*
     * Consume a 2D slice of 3d space.
     */
    @FunctionalInterface
    private interface LayerConsumer3d {
        void accept(int z);
    }

    /*
     * Transform a point within a 4D slice of space.
     */
    @FunctionalInterface
    private interface CubeFunction4d {
        char apply(int x, int y, int z, int w, char c);
    }

    /*
     * Transform a point within a 3D slice of space.
     */
    @FunctionalInterface
    private interface CubeFunction3d {
        char apply(int x, int y, int z, char c);
    }

    /*
     * Consume a 3D slice of space.
     */
    @FunctionalInterface
    private interface CubeConsumer3d {
        void accept(int x, int y, int z, char c);
    }

    /*
     * Consume a point within a 2D slice of space.
     */
    @FunctionalInterface
    private interface CubeConsumer {
        void accept(int x, int y, char c);
    }

}
