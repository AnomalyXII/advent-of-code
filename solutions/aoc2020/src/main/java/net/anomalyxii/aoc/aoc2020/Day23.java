package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 23: Crab Cups.
 */
@Solution(year = 2020, day = 23, title = "Crab Cups")
public class Day23 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The small crab challenges <i>you</i> to a game!
     * The crab is going to mix up some cups, and you have to predict where they'll end up.
     * <p>
     * The cups will be arranged in a circle and labeled <i>clockwise</i> (your puzzle input).
     * For example, if your labeling were <code>32415</code>, there would be five cups in the circle; going clockwise around the circle from the first cup, the cups would be labeled <code>3</code>, <code>2</code>, <code>4</code>, <code>1</code>, <code>5</code>, and then back to <code>3</code> again.
     * <p>
     * Before the crab starts, it will designate the first cup in your list as the <i>current cup</i>.
     * The crab is then going to do <i>100 moves</i>.
     * <p>
     * Each <i>move</i>, the crab does the following actions:
     * <ul>
     * <li>
     * The crab picks up the three cups that are immediately clockwise of the current cup.
     * They are removed from the circle; cup spacing is adjusted as necessary to maintain the circle.
     * </li>
     * <li>
     * The crab selects a destination cup: the cup with a label equal to the current cup's label minus one.
     * If this would select one of the cups that was just picked up, the crab will keep subtracting one until it finds a cup that wasn't just picked up.
     * If at any point in this process the value goes below the lowest value on any cup's label, it wraps around to the highest value on any cup's label instead.
     * </li>
     * <li>
     * The crab places the cups it just picked up so that they are immediately clockwise of the destination cup.
     * They keep the same order as when they were picked up.
     * </li>
     * <li> The crab selects a new current cup: the cup which is immediately clockwise of the current cup. </li>
     * </ul>
     * <p>
     * For example, suppose your cup labeling were <code>389125467</code>.
     * If the crab were to do merely 10 moves, the following changes would occur:
     * <pre>
     * -- move 1 --
     * cups: (3) 8  9  1  2  5  4  6  7
     * pick up: 8, 9, 1
     * destination: 2
     *
     * -- move 2 --
     * cups:  3 (2) 8  9  1  5  4  6  7
     * pick up: 8, 9, 1
     * destination: 7
     *
     * -- move 3 --
     * cups:  3  2 (5) 4  6  7  8  9  1
     * pick up: 4, 6, 7
     * destination: 3
     *
     * -- move 4 --
     * cups:  7  2  5 (8) 9  1  3  4  6
     * pick up: 9, 1, 3
     * destination: 7
     *
     * -- move 5 --
     * cups:  3  2  5  8 (4) 6  7  9  1
     * pick up: 6, 7, 9
     * destination: 3
     *
     * -- move 6 --
     * cups:  9  2  5  8  4 (1) 3  6  7
     * pick up: 3, 6, 7
     * destination: 9
     *
     * -- move 7 --
     * cups:  7  2  5  8  4  1 (9) 3  6
     * pick up: 3, 6, 7
     * destination: 8
     *
     * -- move 8 --
     * cups:  8  3  6  7  4  1  9 (2) 5
     * pick up: 5, 8, 3
     * destination: 1
     *
     * -- move 9 --
     * cups:  7  4  1  5  8  3  9  2 (6)
     * pick up: 7, 4, 1
     * destination: 5
     *
     * -- move 10 --
     * cups: (5) 7  4  1  8  3  9  2  6
     * pick up: 7, 4, 1
     * destination: 3
     *
     * -- final --
     * cups:  5 (8) 3  7  4  1  9  2  6
     * </pre>
     * <p>
     * In the above example, the cups' values are the labels as they appear moving clockwise around the circle; the <i>current cup</i> is marked with <code>( )</code>.
     * <p>
     * After the crab is done, what order will the cups be in?
     * <i>Starting after the cup labeled <code>1</code></i>, collect the other cups' labels clockwise into a single string with no extra characters; each number except <code>1</code> should appear exactly once.
     * In the above example, after 10 moves, the cups clockwise from <code>1</code> are labeled <code>9</code>, <code>2</code>, <code>6</code>, <code>5</code>, and so on, producing <code>92658374</code>.
     * If the crab were to complete all 100 moves, the order after cup <code>1</code> would be <code>67384529</code>.
     * <p>
     * Using your labeling, simulate 100 moves.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What are the labels on the cups after cup 1?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Cups cups = Cups.createFrom(context.stream()
                                                  .flatMapToInt(String::chars)
                                                  .map(i -> i - '0')
                                                  .toArray());

        playCupGame(cups, 100);

        final Cup cup1 = cups.getNodeWithValue(1);

        long result = 0;
        Cup next = cup1.next;
        do {
            result = (result * 10) + next.value;
            next = next.next;
        } while (next != cup1);

        return result;
    }

    /**
     * Due to what you can only assume is a mistranslation (you're not exactly fluent in Crab), you are quite surprised when the crab starts arranging <i>many</i> cups in a circle on your raft - <i>one million</i> (<code>1000000</code>) in total.
     * <p>
     * Your labeling is still correct for the first few cups; after that, the remaining cups are just numbered in an increasing fashion starting from the number after the highest number in your list and proceeding one by one until one million is reached.
     * (For example, if your labeling were <code>54321</code>, the cups would be numbered <code>5</code>, <code>4</code>, <code>3</code>, <code>2</code>, <code>1</code>, and then start counting up from 6 until one million is reached.)
     * In this way, every number from one through one million is used exactly once.
     * <p>
     * After discovering where you made the mistake in translating Crab Numbers, you realize the small crab isn't going to do merely 100 moves; the crab is going to do <i>ten million</i> (<code>10000000</code>) moves!
     * <p>
     * The crab is going to hide your <b>stars</b> - one each - under the <i>two cups that will end up immediately clockwise of cup <code>1</code></i>.
     * You can have them if you predict what the labels on those cups will be when the crab is finished.
     * <p>
     * In the above example (<code>389125467</code>), this would be 934001 and then <code>159792</code>; multiplying these together produces <i><code>149245887792</code></i>.
     * <p>
     * Determine which two cups will end up immediately clockwise of cup <code>1</code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you multiply their labels together?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final int[] labelledCups = context.stream()
                .flatMapToInt(String::chars)
                .map(i -> i - '0')
                .toArray();
        final int maxLabelledCup = IntStream.of(labelledCups).max().orElseThrow();
        final int[] startingCups = IntStream.concat(
                IntStream.of(labelledCups),
                IntStream.rangeClosed(maxLabelledCup + 1, 1_000_000)
        ).toArray();

        final Cups cups = Cups.createFrom(startingCups);

        playCupGame(cups, 10_000_000);

        final Cup cup1 = cups.getNodeWithValue(1);
        final Cup next1 = cup1.next;
        final Cup next2 = next1.next;
        return ((long) next1.value * (long) next2.value);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Play the cups game.
     */
    private static void playCupGame(final Cups cups, final int rounds) {
        Cup currentCup = cups.root;

        for (int round = 0; round < rounds; round++) {
            final Cup next1 = currentCup.next;
            final Cup next2 = next1.next;
            final Cup next3 = next2.next;

            currentCup.setNext(next3.next);

            int destination = currentCup.value;
            do {
                destination = --destination < cups.min()
                        ? cups.max()
                        : destination;
            } while (destination == next1.value || destination == next2.value || destination == next3.value);

            final Cup insertAfter = cups.getNodeWithValue(destination);
            final Cup insertBefore = insertAfter.next;

            insertAfter.setNext(next1);
            next3.setNext(insertBefore);
            currentCup = currentCup.next;
        }
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * All the cups.
     */
    private record Cups(Cup root, Cup[] lookupByValue) {

        // Helper Methods

        /*
         * Get the minimum cup value.
         */
        public int min() {
            return 1;
        }

        /*
         * Get the maximum cup value.
         */
        public int max() {
            return lookupByValue.length;
        }

        /*
         * Get the Cup with the given value.
         */
        public Cup getNodeWithValue(final int i) {
            return lookupByValue[i - 1];
        }

        // Static Helper Methods

        /*
         * Create a new set of Cups.
         */
        public static Cups createFrom(final int[] cups) {
            final Cup tmp = new Cup(0);
            final Cup last = IntStream.of(cups)
                    .mapToObj(Cup::new)
                    .reduce(tmp, (result, next) -> {
                        result.next = next;
                        return next;
                    });

            final Cup root = tmp.next;
            last.next = root;

            final Cup[] lookup = new Cup[cups.length];

            Cup current = root;
            do {
                lookup[current.value - 1] = current;
                current = current.next;
            } while (current != root);

            return new Cups(root, lookup);
        }

    }

    /*
     * One cup.
     */
    private static final class Cup {

        // Private Members

        private final int value;
        private Cup next;

        // Constructors

        Cup(final int value) {
            this.value = value;
        }

        // Helper Methods

        /*
         * Set the next cup, implicitly making the reverse link.
         */
        public void setNext(final Cup other) {
            this.next = other;
        }

    }

}
