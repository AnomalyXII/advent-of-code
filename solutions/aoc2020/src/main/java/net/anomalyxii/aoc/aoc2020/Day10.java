package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 10: Adapter Array.
 */
@Solution(year = 2020, day = 10, title = "Adapter Array")
public class Day10 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Patched into the aircraft's data port, you discover weather forecasts of a massive tropical storm.
     * Before you can figure out whether it will impact your vacation plans, however, your device suddenly turns off!
     * <p>
     * Its battery is dead.
     * <p>
     * You'll need to plug it in.
     * There's only one problem: the charging outlet near your seat produces the wrong number of <i>jolts</i>.
     * Always prepared, you make a list of all of the joltage adapters in your bag.
     * <p>
     * Each of your joltage adapters is rated for a specific <i>output joltage</i> (your puzzle input).
     * Any given adapter can take an input 1, 2, or 3 jolts lower than its rating and still produce its rated output joltage.
     * <p>
     * In addition, your device has a built-in joltage adapter rated for <i><code>3</code> jolts higher</i> than the highest-rated adapter in your bag.
     * (If your adapter list were <code>3</code>, <code>9</code>, and <code>6</code>, your device's built-in adapter would be rated for <code>12</code> jolts.)
     * <p>
     * Treat the charging outlet near your seat as having an effective joltage rating of <code>0</code>.
     * <p>
     * Since you have some time to kill, you might as well test all of your adapters.
     * Wouldn't want to get to your resort and realize you can't even charge your device!
     * <p>
     * If you <i>use every adapter in your bag</i> at once, what is the distribution of joltage differences between the charging outlet, the adapters, and your device?
     * <p>
     * For example, suppose that in your bag, you have adapters with the following joltage ratings:
     * <pre>
     * 16
     * 10
     * 15
     * 5
     * 1
     * 11
     * 7
     * 19
     * 6
     * 12
     * 4
     * </pre>
     * <p>
     * With these adapters, your device's built-in joltage adapter would be rated for <code>19 + 3 = <i>22</i></code> jolts, 3 higher than the highest-rated adapter.
     * <p>
     * Because adapters can only connect to a source 1-3 jolts lower than its rating, in order to use every adapter, you'd need to choose them like this:
     * <ul>
     * <li>
     * The charging outlet has an effective rating of 0 jolts, so the only adapters that could connect to it directly would need to have a joltage rating of <code>1</code>, <code>2</code>, or <code>3</code> jolts.
     * Of these, only one you have is an adapter rated <code>1</code> jolt (difference of <i><code>1</code></i>).
     * </li>
     * <li> From your <code>1</code>-jolt rated adapter, the only choice is your <code>4</code>-jolt rated adapter (difference of <i><code>3</code></i>). </li>
     * <li>
     * From the <code>4</code>-jolt rated adapter, the adapters rated <code>5</code>, <code>6</code>, or <code>7</code> are valid choices.
     * However, in order to not skip any adapters, you have to pick the adapter rated <code>5</code> jolts (difference of <i><code>1</code></i>).
     * </li>
     * <li> Similarly, the next choices would need to be the adapter rated <code>6</code> and then the adapter rated <code>7</code> (with difference of <code>1</code> and <code>1</code>). </li>
     * <li> The only adapter that works with the <code>7</code>-jolt rated adapter is the one rated <code>10</code> jolts (difference of <i><code>3</code></i>). </li>
     * <li> From <code>10</code>, the choices are <code>11</code> or <code>12</code>; choose <code>11</code> (difference of <i><code>1</code></i>) and then <code>12</code> (difference of <i><code>1</code></i>). </li>
     * <li> After <code>12</code>, only valid adapter has a rating of <code>15</code> (difference of <i><code>3</code></i>), then <code>16</code> (difference of <i><code>1</code></i>), then <code>19</code> (difference of <i><code>3</code></i>). </li>
     * <li> Finally, your device's built-in adapter is always 3 higher than the highest adapter, so its rating is <code>22</code> jolts (always a difference of <i><code>3</code></i>). </li>
     * </ul>
     * In this example, when using every adapter, there are <code>7</code> differences of 1 jolt and <code>5</code> differences of 3 jolts.
     * <p>
     * Here is a larger example:
     * <pre>
     * 28
     * 33
     * 18
     * 42
     * 31
     * 14
     * 46
     * 20
     * 48
     * 47
     * 24
     * 23
     * 49
     * 45
     * 19
     * 38
     * 39
     * 11
     * 1
     * 32
     * 25
     * 35
     * 8
     * 17
     * 7
     * 9
     * 4
     * 2
     * 34
     * 10
     * 3
     * </pre>
     * <p>
     * In this larger example, in a chain that uses all of the adapters, there are <i><code>22</code></i> differences of 1 jolt and <i><code>10</code></i> differences of 3 jolts.
     * <p>
     * Find a chain that uses all of your adapters to connect the charging outlet to your device's built-in adapter and count the joltage differences between the charging outlet, the adapters, and your device.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the number of 1-jolt differences multiplied by the number of 3-jolt differences?
     * @throws IllegalStateException if any adapters have a joltage difference less than {@literal 0} or greater than {@literal 3}
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<Integer> adaptors = context.stream()
                .map(Integer::parseInt)
                .sorted()
                .toList();

        if (adaptors.isEmpty()) {
            return 0L;
        }

        long oneStep = 0;
        long threeStep = 0;

        int prev = 0;
        for (final int adaptor : adaptors) {
            final int difference = adaptor - prev;

            switch (difference) {
                case 1:
                    ++oneStep;
                    break;
                case 2:
                    // Don't need to care?
                    break;
                case 3:
                    ++threeStep;
                    break;
                default:
                    throw new IllegalStateException("Invalid difference in adaptor joltages: [" + adaptor + ", " + adaptor + " => " + difference + " ]");
            }

            prev = adaptor;
        }

        return oneStep * (threeStep + 1);
    }

    /**
     * To completely determine whether you have enough adapters, you'll need to figure out how many different ways they can be arranged.
     * Every arrangement needs to connect the charging outlet to your device.
     * The previous rules about when adapters can successfully connect still apply.
     * <p>
     * The first example above (the one that starts with <code>16</code>, <code>10</code>, <code>15</code>) supports the following arrangements:
     * <pre>
     * (0), 1, 4, 5, 6, 7, 10, 11, 12, 15, 16, 19, (22)
     * (0), 1, 4, 5, 6, 7, 10, 12, 15, 16, 19, (22)
     * (0), 1, 4, 5, 7, 10, 11, 12, 15, 16, 19, (22)
     * (0), 1, 4, 5, 7, 10, 12, 15, 16, 19, (22)
     * (0), 1, 4, 6, 7, 10, 11, 12, 15, 16, 19, (22)
     * (0), 1, 4, 6, 7, 10, 12, 15, 16, 19, (22)
     * (0), 1, 4, 7, 10, 11, 12, 15, 16, 19, (22)
     * (0), 1, 4, 7, 10, 12, 15, 16, 19, (22)
     * </pre>
     * <p>
     * (The charging outlet and your device's built-in adapter are shown in parentheses.)
     * Given the adapters from the first example, the total number of arrangements that connect the charging outlet to your device is 8.
     * <p>
     * The second example above (the one that starts with <code>28</code>, <code>33</code>, <code>18</code>) has many arrangements.
     * Here are a few:
     * <pre>
     * (0), 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31, 32, 33, 34, 35, 38, 39, 42, 45, 46, 47, 48, 49, (52)
     * (0), 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31, 32, 33, 34, 35, 38, 39, 42, 45, 46, 47, 49, (52)
     * (0), 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31, 32, 33, 34, 35, 38, 39, 42, 45, 46, 48, 49, (52)
     * (0), 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31, 32, 33, 34, 35, 38, 39, 42, 45, 46, 49, (52)
     * (0), 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31, 32, 33, 34, 35, 38, 39, 42, 45, 47, 48, 49, (52)
     * (0), 3, 4, 7, 10, 11, 14, 17, 20, 23, 25, 28, 31, 34, 35, 38, 39, 42, 45, 46, 48, 49, (52)
     * (0), 3, 4, 7, 10, 11, 14, 17, 20, 23, 25, 28, 31, 34, 35, 38, 39, 42, 45, 46, 49, (52)
     * (0), 3, 4, 7, 10, 11, 14, 17, 20, 23, 25, 28, 31, 34, 35, 38, 39, 42, 45, 47, 48, 49, (52)
     * (0), 3, 4, 7, 10, 11, 14, 17, 20, 23, 25, 28, 31, 34, 35, 38, 39, 42, 45, 47, 49, (52)
     * (0), 3, 4, 7, 10, 11, 14, 17, 20, 23, 25, 28, 31, 34, 35, 38, 39, 42, 45, 48, 49, (52)
     * </pre>
     * <p>
     * In total, this set of adapters can connect the charging outlet to your device in <i><code>19208</code></i> distinct arrangements.
     * <p>
     * You glance back down at your bag and try to remember why you brought so many adapters; there must be <i>more than a trillion</i> valid ways to arrange them!
     * Surely, there must be an efficient way to count the arrangements.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the total number of distinct ways you can arrange the adapters to connect the charging outlet to your device?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<Integer> adaptors = context.stream()
                .map(Integer::parseInt)
                .sorted()
                .collect(Collectors.toList());

        if (adaptors.isEmpty()) {
            return 0L;
        }

        adaptors.add(0, 0);

        final long[] memo = new long[adaptors.size()];
        Arrays.fill(memo, 0);
        memo[0] = 1;

        for (int i = 0; i < adaptors.size(); i++) {
            final int adaptor = adaptors.get(i);

            if (i < adaptors.size() - 1) {
                final int next = adaptors.get(i + 1);
                final int diff = next - adaptor;
                if (diff <= 3) {
                    memo[i + 1] += memo[i];
                }
            }

            if (i < adaptors.size() - 2) {
                final int next = adaptors.get(i + 2);
                final int diff = next - adaptor;
                if (diff <= 3) {
                    memo[i + 2] += memo[i];
                }
            }

            if (i < adaptors.size() - 3) {
                final int next = adaptors.get(i + 3);
                final int diff = next - adaptor;
                if (diff <= 3) {
                    memo[i + 3] += memo[i];
                }
            }
        }

        return memo[adaptors.size() - 1];
    }

}
