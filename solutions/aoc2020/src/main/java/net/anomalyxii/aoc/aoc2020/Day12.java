package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 12: Rain Risk.
 */
@Solution(year = 2020, day = 12, title = "Rain Risk")
public class Day12 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Your ferry made decent progress toward the island, but the storm came in faster than anyone expected.
     * The ferry needs to take <i>evasive actions</i>!
     * <p>
     * Unfortunately, the ship's navigation computer seems to be malfunctioning; rather than giving a route directly to safety, it produced extremely circuitous instructions.
     * When the captain uses the <a href="https://en.wikipedia.org/wiki/Public_address_system">PA</a> system to ask if anyone can help, you quickly volunteer.
     * <p>
     * The navigation instructions (your puzzle input) consists of a sequence of single-character <i>actions</i> paired with integer input <i>values</i>.
     * After staring at them for a few minutes, you work out what they probably mean:
     * <ul>
     * <li> Action <i><code>N</code></i> means to move <i>north</i> by the given value. </li>
     * <li> Action <i><code>S</code></i> means to move <i>south</i> by the given value. </li>
     * <li> Action <i><code>E</code></i> means to move <i>east</i> by the given value. </li>
     * <li> Action <i><code>W</code></i> means to move <i>west</i> by the given value. </li>
     * <li> Action <i><code>L</code></i> means to turn <i>left</i> the given number of degrees. </li>
     * <li> Action <i><code>R</code></i> means to turn <i>right</i> the given number of degrees. </li>
     * <li> Action <i><code>F</code></i> means to move <i>forward</i> by the given value in the direction the ship is currently facing. </li>
     * </ul>
     * The ship starts by facing <i>east</i>.
     * Only the <code>L</code> and <code>R</code> actions change the direction the ship is facing.
     * (That is, if the ship is facing east and the next instruction is <code>N10</code>, the ship would move north 10 units, but would still move east if the following action were <code>F</code>.)
     * <p>
     * For example:
     * <pre>
     * F10
     * N3
     * F7
     * R90
     * F11
     * </pre>
     * <p>
     * These instructions would be handled as follows:
     * <ul>
     * <li> <code>F10</code> would move the ship 10 units east (because the ship starts by facing east) to east 10, north 0. </li>
     * <li> <code>N3</code> would move the ship 3 units north to <i>east 10, north 3</i>. </li>
     * <li> <code>F7</code> would move the ship another 7 units east (because the ship is still facing east) to <i>east 17, north 3</i>. </li>
     * <li> <code>R90</code> would cause the ship to turn right by 90 degrees and face <i>south</i>; it remains at <i>east 17, north 3</i>. </li>
     * <li> <code>F11</code> would move the ship 11 units south to <i>east 17, south 8</i>. </li>
     * </ul>
     * At the end of these instructions, the ship's <a href="https://en.wikipedia.org/wiki/Manhattan_distance">Manhattan distance</a> (sum of the absolute values of its east/west position and its north/south position) from its starting position is <code>17 + 8</code> = <i><code>25</code></i>.
     * <p>
     * Figure out where the navigation instructions lead.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the Manhattan distance between that location and the ship's starting position?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Ship ship = new Ship();

        context.consume(inst -> {
            final char command = inst.charAt(0);
            final long delta = Long.parseLong(inst.substring(1));

            ship.process(command, delta);
        });

        return ship.info.calculateManhattanDistance();
    }

    /**
     * Before you can give the destination to the captain, you realize that the actual action meanings were printed on the back of the instructions the whole time.
     * <p>
     * Almost all of the actions indicate how to move a <i>waypoint</i> which is relative to the ship's position:
     * <ul>
     * <li> Action <i><code>N</code></i> means to move the waypoint <i>north</i> by the given value. </li>
     * <li> Action <i><code>S</code></i> means to move the waypoint <i>south</i> by the given value. </li>
     * <li> Action <i><code>E</code></i> means to move the waypoint <i>east</i> by the given value. </li>
     * <li> Action <i><code>W</code></i> means to move the waypoint <i>west</i> by the given value. </li>
     * <li> Action <i><code>L</code></i> means to rotate the waypoint around the ship <i>left</i> (<i>counter-clockwise</i>) the given number of degrees. </li>
     * <li> Action <i><code>R</code></i> means to rotate the waypoint around the ship <i>right</i> (<i>clockwise</i>) the given number of degrees. </li>
     * <li> Action <i><code>F</code></i> means to move <i>forward</i> to the waypoint a number of times equal to the given value. </li>
     * </ul>
     * The waypoint starts <i>10 units east and 1 unit north</i> relative to the ship.
     * The waypoint is relative to the ship; that is, if the ship moves, the waypoint moves with it.
     * <p>
     * For example, using the same instructions as above:
     * <ul>
     * <li>
     * <code>F10</code> moves the ship to the waypoint 10 times (a total of <i>100 units east and 10 units north</i>), leaving the ship at <i>east 100, north 10</i>.
     * The waypoint stays 10 units east and 1 unit north of the ship.
     * </li>
     * <li>
     * <code>N3</code> moves the waypoint 3 units north to <i>10 units east and 4 units north of the ship</i>.
     * The ship remains at <i>east 100, north 10</i>.
     * </li>
     * <li>
     * <code>F7</code> moves the ship to the waypoint 7 times (a total of <i>70 units east and 28 units north</i>), leaving the ship at <i>east 170, north 38</i>.
     * The waypoint stays 10 units east and 4 units north of the ship.
     * </li>
     * <li>
     * <code>R90</code> rotates the waypoint around the ship clockwise 90 degrees, moving it to <i>4 units east and 10 units south of the ship</i>.
     * The ship remains at <i>east 170, north 38</i>.
     * </li>
     * <li>
     * <code>F11</code> moves the ship to the waypoint 11 times (a total of <i>44 units east and 110 units south</i>), leaving the ship at <i>east 214, south 72</i>.
     * The waypoint stays 4 units east and 10 units south of the ship.
     * </li>
     * </ul>
     * <p>
     * After these operations, the ship's Manhattan distance from its starting position is <code>214 + 72</code> = <i><code>286</code></i>.
     * <p>
     * Figure out where the navigation instructions actually lead.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the Manhattan distance between that location and the ship's starting position?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Waypoint waypoint = new Waypoint();

        context.consume(inst -> {
            final char command = inst.charAt(0);
            final long delta = Long.parseLong(inst.substring(1));

            waypoint.process(command, delta);
        });

        return waypoint.ship.calculateManhattanDistance();
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Represents a ship that moves relative to the positional information.
     */
    private static final class Ship {

        // Private Members

        private final PositionalInfo info = new PositionalInfo(0, 0, 90);

        // Helper Methods

        /*
         * Process the command given to the ship's navigation.
         */
        void process(final char command, final long delta) {
            switch (command) {
                case 'L', 'R' -> info.rotate(command, (int) delta);
                case 'N', 'E', 'S', 'W' -> info.move(command, delta);
                case 'F' -> info.move(normaliseForwardDirection(info.angle), delta);
                default -> throw new IllegalArgumentException("Invalid command instruction: '" + command + "'");
            }
        }

        // Private Helper Methods

        /*
         * Normalise a direction based on an angle.
         */
        private static char normaliseForwardDirection(final int angle) {
            if (angle == 0) return 'N';
            if (angle == 90) return 'E';
            if (angle == 180) return 'S';
            if (angle == 270) return 'W';

            throw new IllegalArgumentException("Unable to normalise angle: " + angle);
        }
    }

    /*
     * Represents an anchor point relative to a ship.
     */
    private static final class Waypoint {

        // Private Members

        private final PositionalInfo ship = new PositionalInfo(0, 0, 0);
        private final PositionalInfo info = new PositionalInfo(1, 10, 0);

        // Helper Methods

        /*
         * Process the command given to the ship's navigation.
         */
        void process(final char command, final long delta) {
            switch (command) {
                case 'L', 'R' -> info.pivot(command, (int) delta);
                case 'N', 'E', 'S', 'W' -> info.move(command, delta);
                case 'F' -> {
                    final long distanceNorth = info.latitude;
                    ship.move('N', distanceNorth * delta);
                    final long distanceEast = info.longitude;
                    ship.move('E', distanceEast * delta);
                }
                default -> throw new IllegalArgumentException("Invalid command instruction: '" + command + "'");
            }
        }
    }

    /*
     * Holds the position information of the ship or waypoint.
     */
    private static final class PositionalInfo {

        // Private Members

        private long latitude;
        private long longitude;
        private int angle;

        // Constructors

        PositionalInfo(final long latitude, final long longitude, final int angle) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.angle = angle;
        }

        // Helper Methods

        /*
         * Move the position, relative to the current position.
         */
        void move(final char direction, final long distance) {
            switch (direction) {
                case 'N' -> moveNorth(distance);
                case 'E' -> moveEast(distance);
                case 'S' -> moveSouth(distance);
                case 'W' -> moveWest(distance);
                default -> throw new IllegalArgumentException("Invalid direction to move: '" + direction + "'");
            }
        }

        /*
         * Rotate the angle, relative to the current position.
         */
        void rotate(final char direction, final int delta) {
            switch (direction) {
                case 'L' -> rotateLeft(delta);
                case 'R' -> rotateRight(delta);
                default -> throw new IllegalArgumentException("Invalid rotation direction: '" + direction + "'");
            }
        }

        /*
         * Pivot the position, relative to (0, 0).
         */
        void pivot(final char direction, final int delta) {
            switch (direction) {
                case 'L' -> pivotLeft(delta);
                case 'R' -> pivotRight(delta);
                default -> throw new IllegalArgumentException("Invalid pivot direction: '" + direction + "'");
            }
        }

        /*
         * Move the positional information due north a given distance.
         */
        void moveNorth(final long distance) {
            latitude += distance;
        }

        /*
         * Move the positional information due south a given distance.
         */
        void moveSouth(final long distance) {
            latitude -= distance;
        }

        /*
         * Move the positional information due east a given distance.
         */
        void moveEast(final long distance) {
            longitude += distance;
        }

        /*
         * Move the positional information due west a given distance.
         */
        void moveWest(final long distance) {
            longitude -= distance;
        }

        /*
         * Change the angle of the position.
         */
        void rotateLeft(final int distance) {
            angle = ((angle - distance) + 360) % 360;
        }

        /*
         * Change the angle of the position.
         */
        void rotateRight(final int distance) {
            angle = ((angle + distance) + 360) % 360;
        }

        /*
         * Pivot the position by a given amount.
         */
        void pivotLeft(final int distance) {
            if (distance % 90 != 0) {
                throw new IllegalArgumentException("Cannot pivot by " + distance + " degrees");
            }

            int remaining = distance;
            while (remaining > 0) {
                final long x = latitude;
                latitude = longitude;
                longitude = -x;
                remaining -= 90;
            }
        }

        /*
         * Pivot the position by a given amount.
         */
        void pivotRight(final int distance) {
            if (distance % 90 != 0) {
                throw new IllegalArgumentException("Cannot pivot by " + distance + " degrees");
            }

            int remaining = distance;
            while (remaining > 0) {
                final long x = latitude;
                final long y = longitude;

                latitude = -y;
                longitude = x;

                remaining -= 90;
            }
        }

        /*
         * Calculate the manhattan distance of |lat| + |long|.
         */
        public long calculateManhattanDistance() {
            return Math.abs(latitude) + Math.abs(longitude);
        }
    }

}
