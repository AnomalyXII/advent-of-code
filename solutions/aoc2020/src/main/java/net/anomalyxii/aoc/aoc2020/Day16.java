package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 16: Ticket Translation.
 */
@Solution(year = 2020, day = 16, title = "Ticket Translation")
public class Day16 {

    /*
     * Default field filter: any fields that start with 'departure'.
     */
    private static final Predicate<String> DEFAULT_FIELD_FILTER = line -> line.startsWith("departure");

    // ****************************************
    // Private Members
    // ****************************************

    private final Predicate<String> relevantFieldFilter;

    // ****************************************
    // Constructors
    // ****************************************

    public Day16() {
        this(DEFAULT_FIELD_FILTER);
    }

    Day16(final Predicate<String> relevantFieldFilter) {
        this.relevantFieldFilter = relevantFieldFilter;
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * As you're walking to yet another connecting flight, you realize that one of the legs of your re-routed trip coming up is on a high-speed train.
     * However, the train ticket you were given is in a language you don't understand.
     * You should probably figure out what it says before you get to the train station after the next flight.
     * <p>
     * Unfortunately, you can't actually <i>read</i> the words on the ticket.
     * You can, however, read the numbers, and so you figure out <i>the fields these tickets must have</i> and <i>the valid ranges</i> for values in those fields.
     * <p>
     * You collect the <i>rules for ticket fields</i>, the <i>numbers on your ticket</i>, and the <i>numbers on other nearby tickets</i> for the same train service (via the airport security cameras) together into a single document you can reference (your puzzle input).
     * <p>
     * The <i>rules for ticket fields</i> specify a list of fields that exist <i>somewhere</i> on the ticket and the <i>valid ranges of values</i> for each field.
     * For example, a rule like <code>class: 1-3 or 5-7</code> means that one of the fields in every ticket is named class and can be any value in the ranges <code>1-3</code> or <code>5-7</code> (inclusive, such that <code>3</code> and <code>5</code> are both valid in this field, but <code>4</code> is not).
     * <p>
     * Each ticket is represented by a single line of comma-separated values.
     * The values are the numbers on the ticket in the order they appear; every ticket has the same format.
     * For example, consider this ticket:
     * <pre>
     * .--------------------------------------------------------.
     * | ????: 101    ?????: 102   ??????????: 103     ???: 104 |
     * |                                                        |
     * | ??: 301  ??: 302             ???????: 303      ??????? |
     * | ??: 401  ??: 402           ???? ????: 403    ????????? |
     * '--------------------------------------------------------'
     * </pre>
     * <p>
     * Here, <code>?</code> represents text in a language you don't understand.
     * This ticket might be represented as <code>101,102,103,104,301,302,303,401,402,403</code>; of course, the actual train tickets you're looking at are <i>much</i> more complicated.
     * In any case, you've extracted just the numbers in such a way that the first number is always the same specific field, the second number is always a different specific field, and so on - you just don't know what each position actually means!
     * <p>
     * Start by determining which tickets are <i>completely invalid</i>; these are tickets that contain values which <i>aren't valid for any field</i>.
     * Ignore <i>your</i> ticket for now.
     * <p>
     * For example, suppose you have the following notes:
     * <pre>
     * class: 1-3 or 5-7
     * row: 6-11 or 33-44
     * seat: 13-40 or 45-50
     *
     * your ticket:
     * 7,1,14
     *
     * nearby tickets:
     * 7,3,47
     * 40,4,50
     * 55,2,20
     * 38,6,12
     * </pre>
     * <p>
     * It doesn't matter which position corresponds to which field; you can identify invalid <i>nearby tickets</i> by considering only whether tickets contain <i>values that are not valid for any field</i>.
     * In this example, the values on the first <i>nearby ticket</i> are all valid for at least one field.
     * This is not true of the other three <i>nearby tickets</i>: the values <code>4</code>, <code>55</code>, and <code>12</code> are are not valid for any field.
     * Adding together all of the invalid values produces your <i>ticket scanning error rate</i>: <code>4 + 55 + 12</code> = <i><code>71</code></i>.
     * <p>
     * Consider the validity of the nearby tickets you scanned.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is your ticket scanning error rate?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Map<String, Set<Integer>> rules = new HashMap<>();

        final AtomicLong errorRate = new AtomicLong(0);
        forEachLineOfNotes(
                context,
                rule -> addRule(rules, rule),
                ticket -> {}, // Ignored, for now
                ticket -> errorRate.addAndGet(validateTicket(rules, ticket))
        );

        return errorRate.longValue();
    }

    /**
     * Now that you've identified which tickets contain invalid values, <i>discard those tickets entirely</i>.
     * Use the remaining valid tickets to determine which field is which.
     * <p>
     * Using the valid ranges for each field, determine what order the fields appear on the tickets.
     * The order is consistent between all tickets: if <code>seat</code> is the third field, it is the third field on every ticket, including <i>your ticket</i>.
     * <p>
     * For example, suppose you have the following notes:
     * <pre>
     * class: 0-1 or 4-19
     * row: 0-5 or 8-19
     * seat: 0-13 or 16-19
     *
     * your ticket:
     * 11,12,13
     *
     * nearby tickets:
     * 3,9,18
     * 15,1,5
     * 5,14,9
     * </pre>
     * <p>
     * Based on the <i>nearby tickets</i> in the above example: the first position must be <code>row</code>, the second position must be <code>class</code>, and the third position must be <code>seat</code>; you can conclude that in <i>your ticket</i>, <code>class</code> is <code>12</code>, <code>row</code> is <code>11</code>, and <code>seat</code> is <code>13</code>.
     * <p>
     * Once you work out which field is which, look for the six fields on <i>your ticket</i> that start with the word <code>departure</code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you multiply those six values together?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Map<String, Set<Integer>> rules = new HashMap<>();

        final int[][] myTicket = new int[1][];
        final List<int[]> validTickets = new ArrayList<>();

        forEachLineOfNotes(
                context,
                rule -> addRule(rules, rule),
                ticket -> myTicket[0] = ticket,
                ticket -> {
                    if (validateTicket(rules, ticket) == 0) {
                        validTickets.add(ticket);
                    }
                }
        );

        final Map<String, Integer> fieldMappings = resolveFieldMappings(rules, validTickets, myTicket[0]);
        return fieldMappings.entrySet().stream()
                .filter(entry -> relevantFieldFilter.test(entry.getKey()))
                .mapToLong(entry -> myTicket[0][entry.getValue()])
                .reduce(1L, (result, val) -> result * val);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Process each line of notes, performing an action on each non-empty
     * line depending on which section is currently being parsed.
     */
    private void forEachLineOfNotes(
            final SolutionContext context,
            final Consumer<String> onRule,
            final Consumer<int[]> onMyTicket,
            final Consumer<int[]> onNearbyTicket
    ) {
        final Phase[] phase = {Phase.RULES};
        context.consume(line -> {
            if (line.isEmpty() || line.isBlank()) {
                return;
            }

            if ("your ticket:".equalsIgnoreCase(line)) {
                phase[0] = Phase.MY_TICKET;
                return;
            }

            if ("nearby tickets:".equalsIgnoreCase(line)) {
                phase[0] = Phase.NEARBY_TICKETS;
                return;
            }

            switch (phase[0]) {
                case RULES -> onRule.accept(line);
                case MY_TICKET -> onMyTicket.accept(parseTicket(line));
                case NEARBY_TICKETS -> onNearbyTicket.accept(parseTicket(line));
                default -> throw new AssertionError("Invalid phase: " + phase[0]);
            }
        });
    }

    /*
     * Add a rule to the rule catalogue.
     */
    private static void addRule(final Map<String, Set<Integer>> rules, final String line) {
        final String[] parts = line.split(": *", 2);
        if (rules.containsKey(parts[0])) {
            throw new IllegalStateException("Duplicate rule: '" + parts[0] + "'");
        }

        rules.put(parts[0], parseRule(parts[1]));
    }

    /*
     * Parse and expand a rule.
     */
    private static Set<Integer> parseRule(final String rule) {
        final String[] parts = rule.split(" or ");
        return Stream.of(parts)
                .map(part -> part.split("-", 2))
                //.map(lowHigh -> new int[]{lowHigh[0], Integer.parseInt(lowHigh[1]))})
                .map(lowHigh -> IntStream.rangeClosed(Integer.parseInt(lowHigh[0]), Integer.parseInt(lowHigh[1])))
                .flatMap(IntStream::boxed)
                .collect(Collectors.toSet());
    }

    /*
     * Parse a ticket.
     */
    private int[] parseTicket(final String line) {
        return Stream.of(line.split(", *"))
                .mapToInt(Integer::parseInt)
                .toArray();
    }

    /*
     * Use a `List` of valid tickets to resolve each rule to exactly one
     * position within a ticket.
     */
    private static Map<String, Integer> resolveFieldMappings(
            final Map<String, Set<Integer>> rules,
            final List<int[]> validTickets,
            final int[] myTicket
    ) {
        final int ticketLength = myTicket.length;

        final Map<String, Set<Integer>> candidateFieldMappings = buildCandidateFieldMappings(rules, validTickets, ticketLength);
        final Map<String, Integer> resolvedFieldMappings = buildFinalisedFieldMappings(candidateFieldMappings);

        // Validate against my ticket!
        resolvedFieldMappings.forEach((key, field) -> {
            final Set<Integer> rule = rules.get(key);
            if (!rule.contains(myTicket[field])) {
                throw new IllegalStateException("My ticket is not valid according to the resolved rule mappings!");
            }
        });

        return resolvedFieldMappings;
    }

    /*
     * Construct a mapping of rules to field indexes for which the field on
     * every valid ticket holds true to that rule.
     */
    private static Map<String, Set<Integer>> buildCandidateFieldMappings(
            final Map<String, Set<Integer>> rules,
            final List<int[]> validTickets,
            final int ticketLength
    ) {
        final Map<String, Set<Integer>> candidateFieldMappings = new HashMap<>();
        rules.forEach((key, rule) -> candidateFieldMappings.put(key, computeDefaultFieldMappings(ticketLength)));
        validTickets.forEach(ticket -> {
            assert ticket.length == ticketLength;
            IntStream.range(0, ticketLength)
                    .forEach(field -> filterCandidateFieldPositions(rules, candidateFieldMappings, ticket, field));
        });
        return candidateFieldMappings;
    }

    /*
     * Resolve each rule down to a single field mapping.
     */
    private static Map<String, Integer> buildFinalisedFieldMappings(final Map<String, Set<Integer>> candidateFieldMappings) {
        final Map<String, Integer> resolvedFieldMappings = new HashMap<>();
        while (!candidateFieldMappings.isEmpty()) {
            final boolean[] changed = {false};
            // Pass through trying to find fields that only have one candidate!
            candidateFieldMappings.entrySet().stream()
                    .peek(entry -> {
                        if (entry.getValue().isEmpty()) {
                            throw new IllegalStateException("No possible fields were found for rule '" + entry.getKey() + "'");
                        }
                    })
                    .filter(entry -> entry.getValue().size() == 1)
                    .forEach(entry -> {
                        changed[0] = true;
                        resolvedFieldMappings.put(entry.getKey(), entry.getValue().iterator().next());
                    });

            resolvedFieldMappings.forEach((resolvedKey, resolvedField) -> {
                candidateFieldMappings.remove(resolvedKey);
                candidateFieldMappings.forEach((key, fields) -> fields.remove(resolvedField));
            });

            if (!changed[0]) {
                throw new IllegalStateException("Unable to resolve all rules: " + candidateFieldMappings.size() + " rules remaining");
            }
        }
        return resolvedFieldMappings;
    }

    /*
     * Filter the candidate field positions for every rule based on whether
     * the field in the supplied ticket is valid.
     */
    private static void filterCandidateFieldPositions(
            final Map<String, Set<Integer>> rules,
            final Map<String, Set<Integer>> candidateFieldMappings,
            final int[] ticket,
            final int field
    ) {
        rules.forEach((key, rule) -> {
            if (!rule.contains(ticket[field])) {
                candidateFieldMappings.get(key).remove(field);
            }
        });
    }

    /*
     * Create a `Set` of all field positions for a given ticket size.
     */
    private static Set<Integer> computeDefaultFieldMappings(final int size) {
        return IntStream.range(0, size)
                .boxed()
                .collect(Collectors.toSet());
    }

    /*
     * Validate whether a given ticket is valid for the provided rules.
     */
    private static long validateTicket(final Map<String, Set<Integer>> rules, final int[] ticketFields) {
        return validateTicket(rules, Arrays.stream(ticketFields));
    }

    /*
     * Validate whether a given ticket is valid for the provided rules.
     */
    private static long validateTicket(final Map<String, Set<Integer>> rules, final IntStream ticketFields) {
        return ticketFields
                .filter(field -> rules.values().stream().noneMatch(rule -> rule.contains(field)))
                .sum();
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Phases of the input.
     */
    private enum Phase {

        RULES,
        MY_TICKET,
        NEARBY_TICKETS,

        // End of constants
        ;

    }

}
