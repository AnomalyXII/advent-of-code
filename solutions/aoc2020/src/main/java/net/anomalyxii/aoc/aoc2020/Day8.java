package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 8: Handheld Halting.
 */
@Solution(year = 2020, day = 8, title = "Handheld Halting")
public class Day8 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Your flight to the major airline hub reaches cruising altitude without incident.
     * While you consider checking the in-flight menu for one of those drinks that come with a little umbrella, you are interrupted by the kid sitting next to you.
     * <p>
     * Their <a href="https://en.wikipedia.org/wiki/Handheld_game_console">handheld game console</a> won't turn on!
     * They ask if you can take a look.
     * <p>
     * You narrow the problem down to a strange <i>infinite loop</i> in the boot code (your puzzle input) of the device.
     * You should be able to fix it, but first you need to be able to run the code in isolation.
     * <p>
     * The boot code is represented as a text file with one <i>instruction</i> per line of text.
     * Each instruction consists of an <i>operation</i> (<code>acc</code>, <code>jmp</code>, or <code>nop</code>) and an <i>argument</i> (a signed number like <code>+4</code> or <code>-20</code>).
     * <ul>
     * <li>
     * <code>acc</code> increases or decreases a single global value called the <i>accumulator</i> by the value given in the argument.
     * For example, <code>acc +7</code> would increase the accumulator by 7.
     * The accumulator starts at <code>0</code>.
     * After an <code>acc</code> instruction, the instruction immediately below it is executed next.
     * </li>
     * <li>
     * <code>jmp</code> <i>jumps</i> to a new instruction relative to itself.
     * The next instruction to execute is found using the argument as an offset from the jmp instruction; for example,
     * <code>jmp +2</code> would skip the next instruction,
     * <code>jmp +1</code> would continue to the instruction immediately below it, and
     * <code>jmp -20</code> would cause the instruction 20 lines above to be executed next.
     * </li>
     * <li>
     * <code>nop</code> stands for <i>No OPeration</i> - it does nothing.
     * The instruction immediately below it is executed next.
     * </li>
     * </ul>
     * <p>
     * For example, consider the following program:
     * <pre>
     * nop +0
     * acc +1
     * jmp +4
     * acc +3
     * jmp -3
     * acc -99
     * acc +1
     * jmp -4
     * acc +6
     * </pre>
     * <p>
     * These instructions are visited in this order:
     * <pre>
     * nop +0  | 1
     * acc +1  | 2, 8(!)
     * jmp +4  | 3
     * acc +3  | 6
     * jmp -3  | 7
     * acc -99 |
     * acc +1  | 4
     * jmp -4  | 5
     * acc +6  |
     * </pre>
     * <p>
     * First, the <code>nop +0</code> does nothing.
     * Then, the accumulator is increased from 0 to 1 (<code>acc +1</code>) and <code>jmp +4</code> sets the next instruction to the other <code>acc +1</code> near the bottom.
     * After it increases the accumulator from 1 to 2, <code>jmp -4</code> executes, setting the next instruction to the only <code>acc +3</code>.
     * It sets the accumulator to 5, and <code>jmp -3</code> causes the program to continue back at the first acc +1.
     * <p>
     * This is an <i>infinite loop</i>: with this sequence of jumps, the program will run forever.
     * The moment the program tries to run any instruction a second time, you know it will never terminate.
     * <p>
     * Immediately <i>before</i> the program would run an instruction a second time, the value in the accumulator is 5.
     * <p>
     * Run your copy of the boot code.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Immediately before any instruction is executed a second time, what value is in the accumulator?
     * @throws IllegalStateException if no infinite loop is detected
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final AtomicLong accumulator = new AtomicLong(0);
        final List<String> instructions = new ArrayList<>(context.read());

        try {
            runCode(accumulator, instructions);
        } catch (final InfiniteLoopException e) {
            return accumulator.get();
        }

        throw new IllegalStateException("Expected an infinite loop!");
    }

    /**
     * After some careful analysis, you believe that <i>exactly one instruction is corrupted</i>.
     * <p>
     * Somewhere in the program, <i>either</i> a <code>jmp</code> is supposed to be a <code>nop</code>, <i>or</i> a <code>nop</code> is supposed to be a <code>jmp</code>.
     * (No <code>acc</code> instructions were harmed in the corruption of this boot code.)
     * <p>
     * The program is supposed to terminate by <i>attempting to execute an instruction immediately after the last instruction in the file</i>.
     * By changing exactly one <code>jmp</code> or <code>nop</code>, you can repair the boot code and make it terminate correctly.
     * <p>
     * For example, consider the same program from above:
     * <pre>
     * nop +0
     * acc +1
     * jmp +4
     * acc +3
     * jmp -3
     * acc -99
     * acc +1
     * jmp -4
     * acc +6
     * </pre>
     * <p>
     * If you change the first instruction from <code>nop +0</code> to <code>jmp +0</code>, it would create a single-instruction infinite loop, never leaving that instruction.
     * If you change almost any of the <code>jmp</code> instructions, the program will still eventually find another <code>jmp</code> instruction and loop forever.
     * <p>
     * However, if you change the second-to-last instruction (from <code>jmp -4</code> to <code>nop -4</code>), the program terminates!
     * The instructions are visited in this order:
     * <pre>
     * nop +0  | 1
     * acc +1  | 2
     * jmp +4  | 3
     * acc +3  |
     * jmp -3  |
     * acc -99 |
     * acc +1  | 4
     * nop -4  | 5
     * acc +6  | 6
     * </pre>
     * <p>
     * After the last instruction (<code>acc +6</code>), the program terminates by attempting to run the instruction below the last instruction in the file.
     * With this change, after the program terminates, the accumulator contains the value <i><code>8</code></i> (<code>acc +1</code>, <code>acc +1</code>, <code>acc +6</code>).
     * <p>
     * Fix the program so that it terminates normally by changing exactly one <code>jmp</code> (to <code>nop</code>) or <code>nop</code> (to <code>jmp</code>).
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the value of the accumulator after the program terminates?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<String> instructions = new ArrayList<>(context.read());
        return runCodeAndAttemptToPatch(instructions);
    }

    // ****************************************
    // Private Helper Members
    // ****************************************

    /*
     * Run the code, starting at the first instruction.
     */
    private void runCode(final AtomicLong accumulator, final List<String> instructions) {
        runCode(0, accumulator, instructions);
    }

    /*
     * Run the code, starting at the given instruction.
     */
    private void runCode(final int start, final AtomicLong accumulator, final List<String> instructions) {
        for (int i = start; i < instructions.size(); ) {
            final String inst = instructions.get(i);
            if ("-".equalsIgnoreCase(inst)) {
                throw new InfiniteLoopException();
            }

            final String cmd = inst.substring(0, 3);
            final String valStr = inst.substring(4);

            i = process(accumulator, instructions, i, cmd, valStr);
        }
    }

    /*
     * Run the code; however, before executing each instruction, attempt to
     * patch a `nop` to a `jmp` or a `jmp` to a `nop` and run the patched
     * code to see if it would complete.
     */
    private long runCodeAndAttemptToPatch(final List<String> instructions) {
        final AtomicLong accumulator = new AtomicLong(0);
        for (int i = 0; i < instructions.size(); ) {
            final String inst = instructions.get(i);
            if ("-".equalsIgnoreCase(inst)) {
                throw new InfiniteLoopException();
            }

            final String cmd = inst.substring(0, 3);
            final String valStr = inst.substring(4);

            // First, attempt to patch the code from this position...
            switch (cmd) {
                case "nop", "jmp" -> {
                    final String newInst = "nop".equals(cmd)
                            ? inst.replaceFirst("nop", "jmp")
                            : inst.replaceFirst("jmp", "nop");
                    final AtomicLong newAccumulator = new AtomicLong(accumulator.longValue());
                    final List<String> newInstructions = new ArrayList<>(instructions);
                    newInstructions.set(i, newInst);
                    try {
                        runCode(i, newAccumulator, newInstructions);
                        return newAccumulator.longValue();
                    } catch (final InfiniteLoopException e) {
                        // patch failed...
                    }
                }
                default -> {
                    // Can't patch...
                }
            }

            i = process(accumulator, instructions, i, cmd, valStr);
        }

        return accumulator.longValue();
    }

    /*
     * Process a given command and return the index of the next instruction.
     */
    private int process(
            final AtomicLong accumulator,
            final List<String> instructions,
            final int i,
            final String cmd,
            final String valStr
    ) {
        instructions.set(i, "-");

        int next = i;
        switch (cmd) {
            case "nop" -> ++next;
            case "acc" -> {
                accumulator.addAndGet(Long.parseLong(valStr));
                ++next;
            }
            case "jmp" -> next += Integer.parseInt(valStr);
            default -> throw new IllegalStateException("Invalid command: '" + cmd + "'");
        }
        return next;
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * Thrown if the code detects an infinite loop.
     */
    private static final class InfiniteLoopException extends RuntimeException {
    }

}
