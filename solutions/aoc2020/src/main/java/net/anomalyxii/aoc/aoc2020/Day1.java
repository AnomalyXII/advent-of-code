package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 1: Report Repair.
 */
@Solution(year = 2020, day = 1, title = "Report Repair")
public class Day1 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * After saving Christmas <a href="/events">five years in a row</a>, you've decided to take a vacation at a nice resort on a tropical island.
     * Surely, Christmas will go on without you.
     * <p>
     * The tropical island has its own currency and is entirely cash-only.
     * The gold coins used there have a little picture of a starfish; the locals just call them <b>stars</b>.
     * None of the currency exchanges seem to have heard of them, but somehow, you'll need to find fifty of these coins by the time you arrive so you can pay the deposit on your room.
     * <p>
     * To save your vacation, you need to get all <b>fifty stars</b> by December 25th.
     * <p>
     * Collect stars by solving puzzles.
     * Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first.
     * Each puzzle grants <b>one star</b>.
     * Good luck!
     * <p>
     * Before you leave, the Elves in accounting just need you to fix your <i>expense report</i> (your puzzle input); apparently, something isn't quite adding up.
     * <p>
     * Specifically, they need you to <i>find the two entries that sum to <code>2020</code></i> and then multiply those two numbers together.
     * <p>
     * For example, suppose your expense report contained the following:
     * <pre>
     * 1721
     * 979
     * 366
     * 299
     * 675
     * 1456
     * </pre>
     * <p>
     * In this list, the two entries that sum to <code>2020</code> are <code>1721</code> and <code>299</code>.
     * Multiplying them together produces <code>1721 * 299 = 514579</code>, so the correct answer is <i><code>514579</code></i>.
     * <p>
     * Of course, your expense report is much larger.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Find the two entries that sum to <code>2020</code>; what do you get if you multiply them together?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return findProductOf2020Pairs(context.process(Integer::parseInt));
    }

    /**
     * The Elves in accounting are thankful for your help; one of them even offers you a starfish coin they had left over from a past vacation.
     * They offer you a second one if you can find <i>three</i> numbers in your expense report that meet the same criteria.
     * <p>
     * Using the above example again, the three entries that sum to <code>2020</code> are <code>979</code>, <code>366</code>, and <code>675</code>.
     * Multiplying them together produces the answer, <i><code>241861950</code></i>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return In your expense report, what is the product of the three entries that sum to <code>2020</code>?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return findProductOf2020Triples(context.process(Integer::parseInt));
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /**
     * Find a pair of numbers in the given {@link List} that sum to
     * {@literal 2020} and return the product of these numbers.
     *
     * @param numbers the numbers
     * @return the product of any two numbers from the specified {@link List} that sum to 2020
     */
    static long findProductOf2020Pairs(final int... numbers) {
        return findProductOf2020Pairs(IntStream.of(numbers).boxed().collect(Collectors.toList()));
    }

    /**
     * Find a pair of numbers in the given {@link List} that sum to
     * {@literal 2020} and return the product of these numbers.
     *
     * @param numbers the {@link List} of numbers
     * @return the product of any two numbers from the specified {@link List} that sum to 2020
     */
    static long findProductOf2020Pairs(final List<Integer> numbers) {
        final List<Integer> sorted = new ArrayList<>(numbers);
        sorted.sort(Integer::compareTo);

        for (int i = 0; i < sorted.size(); i++) {
            final int x = sorted.get(i);
            final int y = 2020 - x;

            if (sorted.contains(y)) {
                return (long) x * y;
            }
        }

        throw new IllegalStateException("No valid 2020 pairs were found!");
    }

    /**
     * Find a triple of numbers in the given {@link List} that sum to
     * {@literal 2020} and return the product of these numbers.
     *
     * @param numbers the numbers
     * @return the product of any three numbers from the specified {@link List} that sum to 2020
     */
    static long findProductOf2020Triples(final int... numbers) {
        return findProductOf2020Triples(IntStream.of(numbers).boxed().collect(Collectors.toList()));
    }

    /**
     * Find a triple of numbers in the given {@link List} that sum to
     * {@literal 2020} and return the product of these numbers.
     *
     * @param numbers the {@link List} of numbers
     * @return the product of any three numbers from the specified {@link List} that sum to 2020
     */
    static long findProductOf2020Triples(final List<Integer> numbers) {
        final List<Integer> sorted = new ArrayList<>(numbers);
        sorted.sort(Integer::compareTo);

        for (int i = 0; i < sorted.size(); i++) {
            final int x = sorted.get(i);

            for (int j = i + 1; j < sorted.size(); j++) {
                final int y = sorted.get(j);

                final int z = 2020 - (x + y);
                if (sorted.contains(z)) {
                    return (long) x * y * z;
                }
            }
        }

        throw new IllegalStateException("No valid 2020 triples were found!");
    }

}
