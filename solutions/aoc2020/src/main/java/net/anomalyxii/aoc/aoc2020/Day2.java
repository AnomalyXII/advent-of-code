package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.function.BiFunction;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 2: Password Philosophy.
 */
@Solution(year = 2020, day = 2, title = "Password Philosophy")
public class Day2 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Your flight departs in a few days from the coastal airport; the easiest way down to the coast from here is via <a href="https://en.wikipedia.org/wiki/Toboggan">toboggan</a>.
     * <p>
     * The shopkeeper at the North Pole Toboggan Rental Shop is having a bad day.
     * "Something's wrong with our computers; we can't log in!"
     * You ask if you can take a look.
     * <p>
     * Their password database seems to be a little corrupted: some of the passwords wouldn't have been allowed by the Official Toboggan Corporate Policy that was in effect when they were chosen.
     * <p>
     * To try to debug the problem, they have created a list (your puzzle input) of <i>passwords</i> (according to the corrupted database) and <i>the corporate policy when that password was set</i>.
     * <p>
     * For example, suppose you have the following list:
     * <pre>
     * 1-3 a: abcde
     * 1-3 b: cdefg
     * 2-9 c: ccccccccc
     * </pre>
     * <p>
     * Each line gives the password policy and then the password.
     * The password policy indicates the lowest and highest number of times a given letter must appear for the password to be valid.
     * For example, <code>1-3 a</code> means that the password must contain a at least 1 time and at most 3 times.
     * <p>
     * In the above example, <i><code>2</code></i> passwords are valid.
     * The middle password, <code>cdefg</code>, is not; it contains no instances of <code>b</code>, but needs at least <code>1</code>.
     * The first and third passwords are valid: they contain one <code>a</code> or nine <code>c</code>, both within the limits of their respective policies.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many passwords are valid according to their policies?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .filter(Day2::isPasswordValidForOldCompany)
                .count();
    }

    /**
     * While it appears you validated the passwords correctly, they don't seem to be what the Official Toboggan Corporate Authentication System is expecting.
     * <p>
     * The shopkeeper suddenly realizes that he just accidentally explained the password policy rules from his old job at the sled rental place down the street!
     * The Official Toboggan Corporate Policy actually works a little differently.
     * <p>
     * Each policy actually describes <i>two positions in the password</i>, where <code>1</code> means the first character, <code>2</code> means the second character, and so on.
     * (Be careful; Toboggan Corporate Policies have no concept of "index zero"!)
     * <i>Exactly one of these positions</i> must contain the given letter.
     * Other occurrences of the letter are irrelevant for the purposes of policy enforcement.
     * <p>
     * Given the same example list from above:
     * <ul>
     * <li> <code>1-3 a: abcde </code> is <i>valid</i>: position <code>1</code> contains a and position <code>3</code> does not. </li>
     * <li> <code>1-3 b: cdefg </code> is <i>invalid</i>: neither position <code>1</code> nor position <code>3</code> contains <code>b</code>. </li>
     * <li> <code>2-9 c: ccccccccc </code> is <i>invalid</i>: both position <code>2</code> and position <code>9</code> contain <code>c</code>. </li>
     * </ul>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return <i>How many passwords are valid</i> according to the new interpretation of the policies?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return context.stream()
                .filter(Day2::isPasswordValidForNewCompany)
                .count();
    }

    // ****************************************
    // Helper Members
    // ****************************************

    /**
     * Check if a given password is acceptable according to attached password
     * policy.
     *
     * @param passwordAndPolicy the password and associated policy
     * @return {@literal true} if the password is valid; {@literal false} otherwise
     */
    static boolean isPasswordValidForOldCompany(final String passwordAndPolicy) {
        return isPasswordValid(passwordAndPolicy, Day2::isPasswordValidForOldCompany);
    }

    /**
     * Check if a given password is acceptable according to attached password
     * policy.
     *
     * @param passwordAndPolicy the password and associated policy
     * @return {@literal true} if the password is valid; {@literal false} otherwise
     */
    static boolean isPasswordValidForNewCompany(final String passwordAndPolicy) {
        return isPasswordValid(passwordAndPolicy, Day2::isPasswordValidForNewCompany);
    }

    /**
     * Check if a given password is acceptable according to the password
     * policy.
     *
     * @param chr      the character of the password policy
     * @param min      the minimum number of occurrences of the policy character
     * @param max      the maximum number of occurrences of the policy character
     * @param password the password to test
     * @return {@literal true} if the password is valid according to the policy; {@literal false} otherwise
     */
    static boolean isPasswordValidForOldCompany(
            final char chr,
            final int min,
            final int max,
            final String password
    ) {
        if (password == null) {
            return false;
        }

        int count = 0;
        final char[] chars = password.toCharArray();

        for (final char c : chars) {
            if (c == chr) {
                count++;
            }

            if (count > max) {
                return false;
            }
        }

        return count >= min;
    }

    /**
     * Check if a given password is acceptable according to the password
     * policy.
     *
     * @param chr      the character of the password policy
     * @param first    the first acceptable position of the policy character
     * @param second   the second acceptable position of the policy character
     * @param password the password to test
     * @return {@literal true} if the password is valid according to the policy; {@literal false} otherwise
     */
    static boolean isPasswordValidForNewCompany(
            final char chr,
            final int first,
            final int second,
            final String password
    ) {
        if (password == null) {
            return false;
        }

        final char chrAtPosition1 = password.length() >= first ? password.charAt(first - 1) : '\0';
        final char chrAtPosition2 = password.length() >= second ? password.charAt(second - 1) : '\0';

        return (chrAtPosition1 == chr || chrAtPosition2 == chr) && (chrAtPosition1 != chrAtPosition2);
    }

    // ****************************************
    // Private Helper Members
    // ****************************************

    /*
     * Split the given password and policy, and validate against the
     * specified `PolicyInterpretation`.
     */
    private static boolean isPasswordValid(
            final String passwordAndPolicy,
            final PolicyInterpretation interpretation
    ) {
        return splitPolicyAndValidatePassword(
                passwordAndPolicy, (policy, password) -> {
                    final String[] policyParts = policy.split(" ", 2);
                    final String policyRange = policyParts[0];
                    final String[] policyRangeParts = policyRange.split("-", 2);

                    final char policyChar = policyParts[1].charAt(0);
                    final int policyFirstPosition = Integer.parseInt(policyRangeParts[0]);
                    final int policySecondPosition = Integer.parseInt(policyRangeParts[1]);

                    return interpretation.validate(policyChar, policyFirstPosition, policySecondPosition, password);
                }
        );
    }

    /*
     * Split the password and policy on the first occurrence of ': '.
     */
    private static boolean splitPolicyAndValidatePassword(
            final String passwordAndPolicy,
            final BiFunction<String, String, Boolean> validator
    ) {
        final String[] parts = passwordAndPolicy.split(": ", 2);

        final String policy = parts[0];
        final String password = parts[1];

        return validator.apply(policy, password);
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /**
     * Validate a given password.
     */
    @FunctionalInterface
    private interface PolicyInterpretation {

        boolean validate(char chr, int first, int second, String password);

    }

}
