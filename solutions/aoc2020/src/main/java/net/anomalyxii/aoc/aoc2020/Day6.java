package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 6: Custom Customs.
 */
@Solution(year = 2020, day = 6, title = "Custom Customs")
public class Day6 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * As your flight approaches the regional airport where you'll switch to a much larger plane, <a href="https://en.wikipedia.org/wiki/Customs_declaration">customs declaration forms</a> are distributed to the passengers.
     * <p>
     * The form asks a series of 26 yes-or-no questions marked <code>a</code> through <code>z</code>.
     * All you need to do is identify the questions for which <i>anyone in your group</i> answers "yes".
     * Since your group is just you, this doesn't take very long.
     * <p>
     * However, the person sitting next to you seems to be experiencing a language barrier and asks if you can help.
     * For each of the people in their group, you write down the questions for which they answer "yes", one per line.
     * For example:
     * <pre>
     * abcx
     * abcy
     * abcz
     * </pre>
     * <p>
     * In this group, there are <code>6</code> questions to which anyone answered "yes": <code>a</code>, <code>b</code>, <code>c</code>, <code>x</code>, <code>y</code>, and <code>z</code>.
     * (Duplicate answers to the same question don't count extra; each question counts at most once.)
     * <p>
     * Another group asks for your help, then another, and eventually you've collected answers from every group on the plane (your puzzle input).
     * Each group's answers are separated by a blank line, and within each group, each person's answers are on a single line. For example:
     * <pre>
     * abc
     *
     * a
     * b
     * c
     *
     * ab
     * ac
     *
     * a
     * a
     * a
     * a
     *
     * b
     * </pre>
     * <p>
     * This list represents answers from five groups:
     * <ul>
     * <li> The first group contains one person who answered "yes" to <code>6</code> questions: <code>a</code>, <code>b</code>, and <code>c</code>. </li>
     * <li> The second group contains three people; combined, they answered "yes" to <code>3</code> questions: <code>a</code>, <code>b</code>, and <code>c</code>. </li>
     * <li> The third group contains two people; combined, they answered "yes" to <code>3</code> questions: <code>a</code>, <code>b</code>, and <code>c</code>. </li>
     * <li> The fourth group contains four people; combined, they answered "yes" to only <code>1</code> question, <code>a</code>. </li>
     * <li> The last group contains one person who answered "yes" to only <code>1</code> question, <code>b</code>. </li>
     * </ul>
     * In this example, the sum of these counts is <code>3 + 3 + 3 + 1 + 1</code> = <code><i>11</i></code>.
     * <p>
     * For each group, count the number of questions to which anyone answered "yes".
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of those counts?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.streamBatches()
                .map(group -> toCharSet(group.stream().flatMapToInt(String::chars)))
                .mapToLong(Set::size).sum();
    }

    /**
     * As you finish the last group's customs declaration, you notice that you misread one word in the instructions:
     * <p>
     * You don't need to identify the questions to which <i>anyone</i> answered "yes"; you need to identify the questions to which <i>everyone</i> answered "yes"!
     * <p>
     * Using the same example as above:
     * <pre>
     * abc
     *
     * a
     * b
     * c
     *
     * ab
     * ac
     *
     * a
     * a
     * a
     * a
     *
     * b
     * </pre>
     * <p>
     * This list represents answers from five groups:
     * <ul>
     * <li> In the first group, everyone (all 1 person) answered "yes" to <code>3</code> questions: <code>a</code>, <code>b</code>, and <code>c</code>. </li>
     * <li> In the second group, there is <i>no</i> question to which everyone answered "yes". </li>
     * <li> In the third group, everyone answered yes to only <code>1</code> question, <code>a</code>. Since some people did not answer "yes" to <code>b</code> or <code>c</code>, they don't count. </li>
     * <li> In the fourth group, everyone answered yes to only <code>1</code> question, <code>a</code>. </li>
     * <li> In the fifth group, everyone (all 1 person) answered "yes" to <code>1</code> question, <code>b</code>. </li>
     * </ul>
     * In this example, the sum of these counts is <code>3 + 0 + 1 + 1 + 1</code> = <code><i>6</i></code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return For each group, count the number of questions to which everyone answered "yes". What is the sum of those counts?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Set<Character> allQuestions = toCharSet("abcdefghijklmnopqrstuvwxyz".chars());
        return context.streamBatches()
                .map(group -> {
                    final Set<Character> remainingQuestions = new HashSet<>(allQuestions);
                    group.forEach(answers -> remainingQuestions.retainAll(toCharSet(answers.chars())));
                    return remainingQuestions;
                })
                .mapToLong(Set::size)
                .sum();
    }

    // ****************************************
    // Private Helper Members
    // ****************************************

    /*
     * Convert the given stream of ASCII to a Set of boxed Characters.
     */
    private static Set<Character> toCharSet(final IntStream asciiStream) {
        return asciiStream
                .mapToObj(ascii -> Character.toLowerCase((char) ascii))
                .collect(Collectors.toSet());
    }

}
