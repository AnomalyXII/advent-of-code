package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.Arrays;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 15: Rambunctious Recitation.
 */
@Solution(year = 2020, day = 15, title = "Rambunctious Recitation")
public class Day15 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You catch the airport shuttle and try to book a new flight to your vacation island.
     * Due to the storm, all direct flights have been cancelled, but a route is available to get around the storm.
     * You take it.
     * <p>
     * While you wait for your flight, you decide to check in with the Elves back at the North Pole.
     * They're playing a <i>memory game</i> and are ever so excited to explain the rules!
     * <p>
     * In this game, the players take turns saying <i>numbers</i>.
     * They begin by taking turns reading from a list of <i>starting numbers</i> (your puzzle input).
     * Then, each turn consists of considering the <i>most recently spoken number</i>:
     * <ul>
     * <li> If that was the <i>first</i> time the number has been spoken, the current player says <code>0</code>. </li>
     * <li> Otherwise, the number had been spoken before; the current player announces how many turns apart the number is from when it was previously spoken.</li>
     * </ul>
     * <p>
     * So, after the starting numbers, each turn results in that player speaking aloud either <code>0</code> (if the last number is new) or an <i>age</i> (if the last number is a repeat).
     * <p>
     * For example, suppose the starting numbers are <code>0,3,6</code>:
     * <ul>
     * <li> <i>Turn 1</i>: The <code>1</code>st number spoken is a starting number, <i><code>0</code></i>. </li>
     * <li> <i>Turn 2</i>: The <code>2</code>nd number spoken is a starting number, <i><code>3</code></i>. </li>
     * <li> <i>Turn 3</i>: The <code>3</code>rd number spoken is a starting number, <i><code>6</code></i>. </li>
     * <li>
     * <i>Turn 4</i>: Now, consider the last number spoken, <code>6</code>.
     * Since that was the first time the number had been spoken, the <code>4</code>th number spoken is <i><code>0</code></i>.
     * </li>
     * <li>
     * <i>Turn 5</i>: Next, again consider the last number spoken, <code>0</code>.
     * Since it <i>had</i> been spoken before, the next number to speak is the difference between the turn number when it was last spoken (the previous turn, <code>4</code>) and the turn number of the time it was most recently spoken before then (turn <code>1</code>).
     * Thus, the 5th number spoken is <code>4 - 1</code>, <i><code>3</code></i>.
     * </li>
     * <li>
     * <i>Turn 6</i>: The last number spoken, <code>3</code> had also been spoken before, most recently on turns <code>5</code> and <code>2</code>.
     * So, the 6th number spoken is <code>5 - 2</code>, <i><code>3</code></i>.
     * </li>
     * <li> <i>Turn 7</i>: Since <code>3</code> was just spoken twice in a row, and the last two turns are <code>1</code> turn apart, the <code>7</code>th number spoken is <i><code>1</code></i>. </li>
     * <li> <i>Turn 8</i>: Since <code>1</code> is new, the <code>8</code>th number spoken is <i><code>0</code></i>. </li>
     * <li> <i>Turn 9</i>: <code>0</code> was last spoken on turns <code>8</code> and <code>4</code>, so the <code>9</code>th number spoken is the difference between them, <i><code>4</code></i>. </li>
     * <li> <i>Turn 10</i>: <code>4</code> is new, so the <code>10</code>th number spoken is <i><code>0</code></i>. </li>
     * </ul>
     * <p>
     * (The game ends when the Elves get sick of playing or dinner is ready, whichever comes first.)
     * <p>
     * Their question for you is: what will be the <i><code>2020</code>th</i> number spoken?
     * In the example above, the <code>2020</code>th number spoken will be <code>436</code>.
     * <p>
     * Here are a few more examples:
     * <ul>
     * <li> Given the starting numbers <code>1,3,2</code>, the <code>2020</code>th number spoken is <code>1</code>. </li>
     * <li> Given the starting numbers <code>2,1,3</code>, the <code>2020</code>th number spoken is <code>10</code>. </li>
     * <li> Given the starting numbers <code>1,2,3</code>, the <code>2020</code>th number spoken is <code>27</code>. </li>
     * <li> Given the starting numbers <code>2,3,1</code>, the <code>2020</code>th number spoken is <code>78</code>. </li>
     * <li> Given the starting numbers <code>3,2,1</code>, the <code>2020</code>th number spoken is <code>438</code>. </li>
     * <li> Given the starting numbers <code>3,1,2</code>, the <code>2020</code>th number spoken is <code>1836</code>. </li>
     * </ul>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Given your starting numbers, <i>what will be the <code>2020</code>th number spoken</i>?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final int[] startingNumbers = context.stream()
                .flatMap(line -> Arrays.stream(line.split(",")))
                .mapToInt(Integer::parseInt)
                .toArray();
        return playGame(2020, startingNumbers);
    }

    /**
     * Impressed, the Elves issue you a challenge: determine the <code>30000000</code>th number spoken.
     * For example, given the same starting numbers as above:
     * <ul>
     * <li> Given <code>0,3,6</code>, the <code>30000000</code>th number spoken is <code>175594</code>. </li>
     * <li> Given <code>1,3,2</code>, the <code>30000000</code>th number spoken is <code>2578</code>. </li>
     * <li> Given <code>2,1,3</code>, the <code>30000000</code>th number spoken is <code>3544142</code>. </li>
     * <li> Given <code>1,2,3</code>, the <code>30000000</code>th number spoken is <code>261214</code>. </li>
     * <li> Given <code>2,3,1</code>, the <code>30000000</code>th number spoken is <code>6895259</code>. </li>
     * <li> Given <code>3,2,1</code>, the <code>30000000</code>th number spoken is <code>18</code>. </li>
     * <li> Given <code>3,1,2</code>, the <code>30000000</code>th number spoken is <code>362</code>. </li>
     * </ul>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Given your starting numbers, <i>what will be the <code>30000000</code>th number spoken</i>?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final int[] startingNumbers = context.stream()
                .flatMap(line -> Arrays.stream(line.split(",")))
                .mapToInt(Integer::parseInt)
                .toArray();
        return playGame(30000000, startingNumbers);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /**
     * Play the game, play the game, play the game....
     *
     * @param rounds the number of rounds to play for
     * @param inputs the starting numbers
     * @return the final number
     */
    public static long playGame(final int rounds, final int... inputs) {
        final int[][] memo = new int[rounds][];

        int previousNumber = 0;
        for (int i = 0; i < rounds; i++) {
            final int thisNumber = i < inputs.length
                    ? inputs[i]
                    : memo[previousNumber] != null
                            ? memo[previousNumber][0] - memo[previousNumber][1]
                            : 0;

            if (memo[thisNumber] == null) {
                memo[thisNumber] = new int[]{i, i};
            } else {
                memo[thisNumber][1] = memo[thisNumber][0];
                memo[thisNumber][0] = i;
            }

            previousNumber = thisNumber;
        }

        return previousNumber;
    }

}
