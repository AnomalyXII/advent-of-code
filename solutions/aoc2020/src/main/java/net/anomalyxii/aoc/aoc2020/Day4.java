package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;

import static java.util.Arrays.asList;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 4: Passport Processing.
 */
@Solution(year = 2020, day = 4, title = "Passport Processing")
public class Day4 {

    static final String[] REQUIRED_FIELDS = {
            "byr", // (Birth Year)
            "iyr", // (Issue Year)
            "eyr", // (Expiration Year)
            "hgt", // (Height)
            "hcl", // (Hair Color)
            "ecl", // (Eye Color)
            "pid", // (Passport ID)
    };

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You arrive at the airport only to realize that you grabbed your North Pole Credentials instead of your passport.
     * While these documents are extremely similar, North Pole Credentials aren't issued by a country and therefore aren't actually valid documentation for travel in most of the world.
     * <p>
     * It seems like you're not the only one having problems, though; a very long line has formed for the automatic passport scanners, and the delay could upset your travel itinerary.
     * <p>
     * Due to some questionable network security, you realize you might be able to solve both of these problems at the same time.
     * <p>
     * The automatic passport scanners are slow because they're having trouble <i>detecting which passports have all required fields</i>.
     * The expected fields are as follows:
     * <ul>
     * <li> <code>byr</code> (Birth Year) </li>
     * <li> <code>iyr</code> (Issue Year) </li>
     * <li> <code>eyr</code> (Expiration Year) </li>
     * <li> <code>hgt</code> (Height) </li>
     * <li> <code>hcl</code> (Hair Color) </li>
     * <li> <code>ecl</code> (Eye Color) </li>
     * <li> <code>pid</code> (Passport ID) </li>
     * <li> <code>cid</code> (Country ID) </li>
     * </ul>
     * Passport data is validated in batch files (your puzzle input).
     * Each passport is represented as a sequence of key:value pairs separated by spaces or newlines.
     * Passports are separated by blank lines.
     * <p>
     * Here is an example batch file containing four passports:
     * <pre>
     * ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
     * byr:1937 iyr:2017 cid:147 hgt:183cm
     *
     * iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
     * hcl:#cfa07d byr:1929
     *
     * hcl:#ae17e1 iyr:2013
     * eyr:2024
     * ecl:brn pid:760753108 byr:1931
     * hgt:179cm
     *
     * hcl:#cfa07d eyr:2025 pid:166559648
     * iyr:2011 ecl:brn hgt:59in
     * </pre>
     * <p>
     * The first passport is <i>valid</i> - all eight fields are present.
     * The second passport is <i>invalid</i> - it is missing <code>hgt</code> (the Height field).
     * <p>
     * The third passport is interesting; the <i>only missing field</i> is <code>cid</code>, so it looks like data from North Pole Credentials, not a passport at all!
     * Surely, nobody would mind if you made the system temporarily ignore missing <code>cid</code> fields.
     * Treat this "passport" as <i>valid</i>.
     * <p>
     * The fourth passport is missing two fields, <code>cid</code> and <code>byr</code>.
     * Missing <code>cid</code> is fine, but missing any other field is not, so this passport is <i>invalid</i>.
     * <p>
     * According to the above rules, your improved system would report <i><code>2</code></i> valid passports.
     * <p>
     * Count the number of <i>valid</i> passports - those that have all required fields.
     * Treat <code>cid</code> as optional.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return In your batch file, how many passports are valid?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.streamBatches()
                .map(Day4::parsePassport)
                .filter(passport -> isPassportValid(passport, (k, v) -> true))
                .count();
    }

    /**
     * <p>
     * The line is moving more quickly now, but you overhear airport security talking about how passports with invalid data are getting through.
     * Better add some data validation, quick!
     * <p>
     * You can continue to ignore the cid field, but each other field has strict rules about what values are valid for automatic validation:
     * <ul>
     * <li> <code>byr</code> (Birth Year) - four digits; at least <code>1920</code> and at most <code>2002</code>. </li>
     * <li> <code>iyr</code> (Issue Year) - four digits; at least <code>2010</code> and at most <code>2020</code>. </li>
     * <li> <code>eyr</code> (Expiration Year) - four digits; at least <code>2020</code> and at most <code>2030</code>. </li>
     * <li>
     * <code>hgt</code> (Height) - a number followed by either <code>cm</code> or <code>in</code>:
     * <ul>
     * <li> If <code>cm</code>, the number must be at least <code>150</code> and at most <code>193</code>. </li>
     * <li> If <code>in</code>, the number must be at least <code>59</code> and at most <code>76</code>. </li>
     * </ul>
     * </li>
     * <li> <code>hcl</code> (Hair Color) - a <code>#</code> followed by exactly six characters <code>0-9</code> or <code>a-f</code>. </li>
     * <li> <code>ecl</code> (Eye Color) - exactly one of: <code>amb</code> <code>blu</code> <code>brn</code> <code>gry</code> <code>grn</code> <code>hzl</code> <code>oth</code>. </li>
     * <li> <code>pid</code> (Passport ID) - a nine-digit number, including leading zeroes. </li>
     * <li> <code>cid</code> (Country ID) - ignored, missing or not. </li>
     * </ul>
     * Your job is to count the passports where all required fields are both present and valid according to the above rules.
     * Here are some example values:
     * <pre>
     * byr valid:   2002
     * byr invalid: 2003
     *
     * hgt valid:   60in
     * hgt valid:   190cm
     * hgt invalid: 190in
     * hgt invalid: 190
     *
     * hcl valid:   #123abc
     * hcl invalid: #123abz
     * hcl invalid: 123abc
     *
     * ecl valid:   brn
     * ecl invalid: wat
     *
     * pid valid:   000000001
     * pid invalid: 0123456789
     * </pre>
     * <p>
     * Here are some invalid passports:
     * <pre>
     * eyr:1972 cid:100
     * hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926
     *
     * iyr:2019
     * hcl:#602927 eyr:1967 hgt:170cm
     * ecl:grn pid:012533040 byr:1946
     *
     * hcl:dab227 iyr:2012
     * ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277
     *
     * hgt:59cm ecl:zzz
     * eyr:2038 hcl:74454a iyr:2023
     * pid:3556412378 byr:2007
     * </pre>
     * <p>
     * Here are some valid passports:
     * <pre>
     * pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
     * hcl:#623a2f
     *
     * eyr:2029 ecl:blu cid:129 byr:1989
     * iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm
     *
     * hcl:#888785
     * hgt:164cm byr:2001 iyr:2015 cid:88
     * pid:545766238 ecl:hzl
     * eyr:2022
     *
     * iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
     * </pre>
     * <p>
     * Count the number of <i>valid</i> passports - those that have all required fields and valid values.
     * Continue to treat <code>cid</code> as optional.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return In your batch file, how many passports are valid?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return context.streamBatches()
                .map(Day4::parsePassport)
                .filter(passport -> isPassportValid(passport, Day4::validateField))
                .count();
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /**
     * Check if the given passport contains all the required keys.
     *
     * @param passport the passport
     * @return {@literal true} if the passport is valid; {@literal false} otherwise
     */
    static boolean isPassportValid(final Map<String, String> passport) {
        return isPassportValid(passport, (k, v) -> true);
    }

    /**
     * Check if the given passport contains all the required keys.
     *
     * @param passport  the passport
     * @param validator the field validator
     * @return {@literal true} if the passport is valid; {@literal false} otherwise
     */
    static boolean isPassportValid(final Map<String, String> passport, final BiPredicate<String, String> validator) {
        final boolean allFields = passport.keySet().containsAll(asList(REQUIRED_FIELDS));
        if (!allFields) {
            return false;
        }

        return passport.entrySet().stream()
                .allMatch(entry -> validator.test(entry.getKey(), entry.getValue()));
    }

    /**
     * Validate a given passport field.
     *
     * @param field the field key
     * @param value the field value
     * @return the answer
     */
    static boolean validateField(final String field, final String value) {
        try {
            switch (field) {
                case "byr":
                    final int byr = Integer.parseInt(value);
                    return byr >= 1920 && byr <= 2002;
                case "iyr":
                    final int iyr = Integer.parseInt(value);
                    return iyr >= 2010 && iyr <= 2020;
                case "eyr":
                    final int eyr = Integer.parseInt(value);
                    return eyr >= 2020 && eyr <= 2030;
                case "hgt":
                    final int hgt = Integer.parseInt(value.substring(0, value.length() - 2));
                    if (value.endsWith("cm"))
                        return hgt >= 150 && hgt <= 193;
                    else if (value.endsWith("in"))
                        return hgt >= 59 && hgt <= 76;
                    else
                        return false;
                case "hcl":
                    return value.matches("#[a-fA-F0-9]{6}");
                case "ecl":
                    return value.matches("amb|blu|brn|gry|grn|hzl|oth");
                case "pid":
                    return value.matches("[0-9]{9}");
                case "cid":
                    return true;
                default:
                    throw new IllegalArgumentException("Invalid field: " + field);
            }
        } catch (final Exception e) {
            return false;
        }
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Parse the passport from the given lines.
     */
    private static Map<String, String> parsePassport(final List<String> lines) {
        final Map<String, String> passport = new HashMap<>();
        for (final String line : lines) {
            final String[] fields = line.split(" ");
            for (final String field : fields) {
                final String[] parts = field.split(":", 2);
                passport.put(parts[0], parts[1]);
            }
        }

        return passport;
    }

}
