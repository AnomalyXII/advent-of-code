package net.anomalyxii.aoc.aoc2020;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Bounds;

import java.util.List;
import java.util.Optional;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 9: Encoding Error.
 */
@Solution(year = 2020, day = 9, title = "Encoding Error")
public class Day9 {

    private final int preambleLength;

    // ****************************************
    // Constructors
    // ****************************************

    public Day9() {
        this(25);
    }

    Day9(final int preambleLength) {
        this.preambleLength = preambleLength;
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * With your neighbor happily enjoying their video game, you turn your attention to an open data port on the little screen in the seat in front of you.
     * <p>
     * Though the port is non-standard, you manage to connect it to your computer through the clever use of several paperclips.
     * Upon connection, the port outputs a series of numbers (your puzzle input).
     * <p>
     * The data appears to be encrypted with the eXchange-Masking Addition System (XMAS) which, conveniently for you, is an old cypher with an important weakness.
     * <p>
     * XMAS starts by transmitting a <i>preamble</i> of 25 numbers.
     * After that, each number you receive should be the sum of any two of the 25 immediately previous numbers.
     * The two numbers will have different values, and there might be more than one such pair.
     * <p>
     * For example, suppose your preamble consists of the numbers 1 through 25 in a random order.
     * To be valid, the next number must be the sum of two of those numbers:
     * <ul>
     * <li> <code>26</code> would be a <i>valid</i> next number, as it could be <code>1</code> plus <code>25</code> (or many other pairs, like <code>2</code> and <code>24</code>). </li>
     * <li> <code>49</code> would be a <i>valid</i> next number, as it is the sum of <code>24</code> and <code>25</code>. </li>
     * <li> <code>100</code> would <i>not</i> be valid; no two of the previous <code>25</code> numbers sum to <code>100</code>. </li>
     * <li> <code>50</code> would also <i>not</i> be valid; although <code>25</code> appears in the previous 25 numbers, the two numbers in the pair must be different. </li>
     * </ul>
     * <p>
     * Suppose the 26th number is <code>45</code>, and the first number (no longer an option, as it is more than 25 numbers ago) was <code>20</code>.
     * Now, for the next number to be valid, there needs to be some pair of numbers among <code>1</code>-<code>19</code>, <code>21</code>-<code>25</code>, or <code>45</code> that add up to it:
     * <ul>
     * <li> <code>26</code> would still be a valid next number, as <code>1</code> and <code>25</code> are still within the previous 25 numbers. </li>
     * <li> <code>65</code> would not be valid, as no two of the available numbers sum to it. </li>
     * <li> <code>64</code> and <code>66</code> would both be valid, as they are the result of <code>19+45</code> and <code>21+45</code> respectively. </li>
     * </ul>
     * <p>
     * Here is a larger example which only considers the previous <code>5</code> numbers (and has a preamble of length 5):
     * <pre>
     * 35
     * 20
     * 15
     * 25
     * 47
     * 40
     * 62
     * 55
     * 65
     * 95
     * 102
     * 117
     * 150
     * 182
     * 127
     * 219
     * 299
     * 277
     * 309
     * 576
     * </pre>
     * <p>
     * In this example, after the 5-number preamble, almost every number is the sum of two of the previous 5 numbers; the only number that does not follow this rule is <code>127</code>.
     * <p>
     * The first step of attacking the weakness in the XMAS data is to find the first number in the list (after the preamble) which is not the sum of two of the 25 numbers before it.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the first number that does not have this property?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<Long> numbers = context.process(Long::parseLong);
        return findEncryptionError(numbers, preambleLength);
    }

    /**
     * The final step in breaking the XMAS encryption relies on the invalid number you just found: you must <i>find a contiguous set of at least two numbers</i> in your list which sum to the invalid number from step 1.
     * <p>
     * Again consider the above example:
     * <pre>
     * 35
     * 20
     * 15
     * 25
     * 47
     * 40
     * 62
     * 55
     * 65
     * 95
     * 102
     * 117
     * 150
     * 182
     * 127
     * 219
     * 299
     * 277
     * 309
     * 576
     * </pre>
     * <p>
     * In this list, adding up all of the numbers from <code>15</code> through <code>40</code> produces the invalid number from step 1, <code>127</code>.
     * (Of course, the contiguous set of numbers in your actual list might be much longer.)
     * <p>
     * To find the <i>encryption weakness</i>, add together the smallest and largest number in this contiguous range; in this example, these are <code>15</code> and <code>47</code>, producing <i><code>62</code></i>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the encryption weakness in your XMAS-encrypted list of numbers?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<Long> numbers = context.process(Long::parseLong);
        final long target = findEncryptionError(numbers, preambleLength);
        return findEncryptionWeakness(numbers, target);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Search the `List` of given numbers for the first occurrence that is
     * not the sum of any of the previous X numbers.
     */
    private static long findEncryptionError(final List<Long> numbers, final int preambleLength) {
        return Bounds.of(preambleLength, numbers.size() - 1)
                .minPointSatisfying(i -> {
                    final long next = numbers.get(i);
                    final boolean match = Bounds.of(0, preambleLength - 2)
                            .anyMatch(x -> Bounds.of(x + 1, preambleLength - 1)
                                    .anyMatch(y -> {
                                        final long windowX = numbers.get(i - (preambleLength - x));
                                        final long windowY = numbers.get(i - (preambleLength - y));
                                        return windowX + windowY == next;
                                    }));
                    return match
                            ? Optional.empty()
                            : Optional.of(next);
                })
                .orElseThrow(() -> new IllegalStateException("No encoding error was found"));
    }

    /*
     * Find the sum of (min() + max()) of the first contiguous run of numbers
     * that sum to the target value;
     */
    private static long findEncryptionWeakness(final List<Long> numbers, final long target) {

        long total = 0;
        int lowBound = 0, highBound = 0;
        while (highBound < numbers.size()) {
            final long next = numbers.get(highBound);
            total += next;

            while (total > target) {
                final long prev = numbers.get(lowBound++);
                total -= prev;
            }

            if (total == target) {
                // Todo: is there a way of tracking these as we go?
                final long min = numbers.subList(lowBound, highBound)
                        .stream()
                        .mapToLong(x -> x)
                        .min()
                        .orElseThrow();
                final long max = numbers.subList(lowBound, highBound)
                        .stream()
                        .mapToLong(x -> x)
                        .max()
                        .orElseThrow();

                return min + max;
            }

            highBound++;
        }

        throw new IllegalStateException("No encryption weakness was found");
    }

}
