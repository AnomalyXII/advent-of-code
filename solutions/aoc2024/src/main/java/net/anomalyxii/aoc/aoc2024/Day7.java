package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;

import static java.util.Arrays.stream;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 7: Bridge Repair.
 */
@Solution(year = 2024, day = 7, title = "Bridge Repair")
public class Day7 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The Historians take you to a familiar <a href="/2022/day/9">rope bridge</a> over a river in the middle of a jungle.
     * The Chief isn't on this side of the bridge, though; maybe he's on the other side?
     * <p>
     * When you go to cross the bridge, you notice a group of engineers trying to repair it.
     * (Apparently, it breaks pretty frequently.) You won't be able to cross until it's fixed.
     * <p>
     * You ask how long it'll take; the engineers tell you that it only needs final calibrations, but some young elephants were playing nearby and <em>stole all the operators</em> from their calibration equations!
     * They could finish the calibrations if only someone could determine which test values could possibly be produced by placing any combination of operators into their calibration equations (your puzzle input).
     * <p>
     * For example:
     * <pre>
     * 190: 10 19
     * 3267: 81 40 27
     * 83: 17 5
     * 156: 15 6
     * 7290: 6 8 6 15
     * 161011: 16 10 13
     * 192: 17 8 14
     * 21037: 9 7 18 13
     * 292: 11 6 16 20
     * </pre>
     * <p>
     * Each line represents a single equation.
     * The test value appears before the colon on each line; it is your job to determine whether the remaining numbers can be combined with operators to produce the test value.
     * <p>
     * Operators are <em>always evaluated left-to-right</em>, <em>not</em> according to precedence rules.
     * Furthermore, numbers in the equations cannot be rearranged.
     * Glancing into the jungle, you can see elephants holding two different types of operators: <em>add</em> (<code>+</code>) and <em>multiply</em> (<code>*</code>).
     * <p>
     * Only three of the above equations can be made true by inserting operators:
     * <ul>
     * <li><code>190: 10 19</code> has only one position that accepts an operator: between <code>10</code> and <code>19</code>.
     * Choosing <code>+</code> would give <code>29</code>, but choosing <code>*</code> would give the test value (<code>10 * 19 = 190</code>).</li>
     * <li><code>3267: 81 40 27</code> has two positions for operators.
     * Of the four possible configurations of the operators, <em>two</em> cause the right side to match the test value: <code>81 + 40 * 27</code> and <code>81 * 40 + 27</code> both equal <code>3267</code> (when evaluated left-to-right)!</li>
     * <li><code>292: 11 6 16 20</code> can be solved in exactly one way: <code>11 + 6 * 16 + 20</code>.</li>
     * </ul>
     * <p>
     * The engineers just need the <em>total calibration result</em>, which is the sum of the test values from just the equations that could possibly be true.
     * In the above example, the sum of the test values for the three equations listed above is <code><em>3749</em></code>.
     * <p>
     * Determine which equations could possibly be true.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is their total calibration result?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .map(Calibration::parse)
                .filter(Calibration::isSolvable)
                .mapToLong(Calibration::total)
                .sum();
    }

    /**
     * The engineers seem concerned; the total calibration result you gave them is nowhere close to being within safety tolerances.
     * Just then, you spot your mistake: some well-hidden elephants are holding a <em>third type of operator</em>.
     * <p>
     * The <a href="https://en.wikipedia.org/wiki/Concatenation" target="_blank">concatenation</a> operator (<code><span title="I think you mean &quot;.&quot;.">||</span></code>) combines the digits from its left and right inputs into a single number.
     * For example, <code>12 || 345</code> would become <code>12345</code>.
     * All operators are still evaluated left-to-right.
     * <p>
     * Now, apart from the three equations that could be made true using only addition and multiplication, the above example has three more equations that can be made true by inserting operators:
     * <ul>
     * <li><code>156: 15 6</code> can be made true through a single concatenation: <code>15 || 6 = 156</code>.</li>
     * <li><code>7290: 6 8 6 15</code> can be made true using <code>6 * 8 || 6 * 15</code>.</li>
     * <li><code>192: 17 8 14</code> can be made true using <code>17 || 8 + 14</code>.</li>
     * </ul>
     * <p>
     * Adding up all six test values (the three that could be made before using only <code>+</code> and <code>*</code> plus the new three that can now be made by also using <code>||</code>) produces the new <em>total calibration result</em> of <code><em>11387</em></code>.
     * <p>
     * Using your new knowledge of elephant hiding spots, determine which equations could possibly be true.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is their total calibration result?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return context.stream()
                .map(Calibration::parse)
                .filter(Calibration::isSolvableWithConcatenation)
                .mapToLong(Calibration::total)
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        return context.stream()
                .map(Calibration::parse)
                .reduce(
                        LongTuple.NULL,
                        (result, calibration) -> {
                            if (calibration.isSolvable())
                                return result.add(calibration.total, calibration.total);
                            else if (calibration.isSolvableWithConcatenation())
                                return result.add(0, calibration.total);
                            return result;
                        },
                        LongTuple::add
                );
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A calibration, with all operators removed.
     */
    private record Calibration(long total, int[] parts) {

        // Helper Methods

        /*
         * Check if this calibration is solvable.
         */
        boolean isSolvable() {
            return isSolvable(parts, parts[0], 1, total);
        }

        /*
         * Check if this calibration is solvable when using the concatenation
         * operator.
         */
        boolean isSolvableWithConcatenation() {
            return isSolvableWithConcatenation(parts, parts[0], 1, total);
        }

        // Static Helper Methods

        /*
         * Parse the `Calibration`.
         */
        public static Calibration parse(final String line) {
            final String[] totalAndParts = line.split(":\\s*", 2);

            final long total = Long.parseLong(totalAndParts[0]);
            final String[] parts = totalAndParts[1].split("\\s+");
            return new Calibration(total, stream(parts).mapToInt(Integer::parseInt).toArray());
        }

        /*
         * Check if a given calibration is solvable.
         */
        private static boolean isSolvable(final int[] parts, final long lhs, final int idx, final long total) {
            if (idx == parts.length)
                return lhs == total;

            return isSolvable(parts, lhs + parts[idx], idx + 1, total)
                    || isSolvable(parts, lhs * parts[idx], idx + 1, total);
        }

        /*
         * Check if a given calibration is solvable when using the concatenation
         * operator.
         */
        private static boolean isSolvableWithConcatenation(final int[] parts, final long lhs, final int idx, final long total) {
            if (idx == parts.length)
                return lhs == total;

            return isSolvableWithConcatenation(parts, lhs + parts[idx], idx + 1, total)
                    || isSolvableWithConcatenation(parts, lhs * parts[idx], idx + 1, total)
                    || isSolvableWithConcatenation(parts, concatenate(lhs, parts[idx]), idx + 1, total);
        }

        /*
         * Concatenate two numbers.
         */
        private static long concatenate(final long lhs, final int rhs) {
            final int log = (int) Math.log10(rhs);
            final int power = (int) Math.pow(10, log + 1);
            return lhs * power + rhs;
        }
    }
}

