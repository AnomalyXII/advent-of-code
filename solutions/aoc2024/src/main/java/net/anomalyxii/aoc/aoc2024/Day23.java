package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.ObjectTuple;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 23: LAN Party.
 */
@Solution(year = 2024, day = 23, title = "LAN Party")
public class Day23 {

    /*
     * Conservative estimate for maximum range of a single character in a
     * computer hostname (assumes a-z only...).
     */
    private static final int MAX_1 = 0x001F;

    /*
     * Number of bits used to store a single character of a hostname.
     */
    private static final int SIZE_1 = 5;

    /*
     * Conservative estimate for maximum range of all characters in a
     * computer hostname (assumes exactly 2 a-z).
     */
    private static final int MAX_2 = 0x03FF;

    /*
     * Number of bits used to store all characters in a hostname.
     */
    private static final int SIZE_2 = 10;

    /*
     * Arbitrary number for maximum number of links from any one computer.
     *
     * Set to 16 to conserve memory, but could be increased if needed.
     * Actually only supports 15 links, as the first entry is always the
     * actual number of entries in the array.
     */
    private static final int MAX_LINKS = 0x10;

    /*
     * Representation of `t` in a hostname.
     */
    private static final int MAGIC_T = 19;

    /*
     * Computer says "no"...
     */
    private static final int[] NO_COMPUTERS = new int[0];

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * As The Historians wander around a secure area at Easter Bunny HQ, you come across posters for a <a href="https://en.wikipedia.org/wiki/LAN_party" target="_blank">LAN party</a> scheduled for today!
     * Maybe you can find it; you connect to a nearby <a href="/2016/day/9">datalink port</a> and download a map of the local network (your puzzle input).
     * <p>
     * The network map provides a list of every <em>connection between two computers</em>.
     * For example:
     * <pre>
     * kh-tc
     * qp-kh
     * de-cg
     * ka-co
     * yn-aq
     * qp-ub
     * cg-tb
     * vc-aq
     * tb-ka
     * wh-tc
     * yn-cg
     * kh-ub
     * ta-co
     * de-co
     * tc-td
     * tb-wq
     * wh-td
     * ta-ka
     * td-qp
     * aq-cg
     * wq-ub
     * ub-vc
     * de-ta
     * wq-aq
     * wq-vc
     * wh-yn
     * ka-de
     * kh-ta
     * co-tc
     * wh-qp
     * tb-vc
     * td-yn
     * </pre>
     * <p>
     * Each line of text in the network map represents a single connection; the line <code>kh-tc</code> represents a connection between the computer named <code>kh</code> and the computer named <code>tc</code>.
     * Connections aren't directional; <code>tc-kh</code> would mean exactly the same thing.
     * <p>
     * LAN parties typically involve multiplayer games, so maybe you can locate it by finding groups of connected computers.
     * Start by looking for <em>sets of three computers</em> where each computer in the set is connected to the other two computers.
     * <p>
     * In this example, there are <code>12</code> such sets of three inter-connected computers:
     * <pre>
     * aq,cg,yn
     * aq,vc,wq
     * co,de,ka
     * co,de,ta
     * co,ka,ta
     * de,ka,ta
     * kh,qp,ub
     * qp,td,wh
     * tb,vc,wq
     * tc,td,wh
     * td,wh,yn
     * ub,vc,wq
     * </pre>
     * <p>
     * If the Chief Historian is here, <em>and</em> he's at the LAN party, it would be best to know that right away.
     * You're pretty sure his computer's name starts with <code>t</code>, so consider only sets of three computers where at least one computer's name starts with <code>t</code>.
     * That narrows the list down to <code><em>7</em></code> sets of three inter-connected computers:
     * <pre>
     * co,de,<em>ta</em>
     * co,ka,<em>ta</em>
     * de,ka,<em>ta</em>
     * qp,<em>td</em>,wh
     * <em>tb</em>,vc,wq
     * <em>tc</em>,<em>td</em>,wh
     * <em>td</em>,wh,yn
     * </pre>
     * <p>
     * Find all the sets of three inter-connected computers.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many contain at least one computer with a name that starts with <code>t</code>?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final int[][] computers = parseComputers(context);
        return countClustersOfThreeContainingAComputerStartingWithT(computers);
    }

    /**
     * There are still way too many results to go through them all.
     * You'll have to find the LAN party another way and go there yourself.
     * <p>
     * Since it doesn't seem like any employees are around, you figure they must all be at the LAN party.
     * If that's true, the LAN party will be the <em>largest set of computers that are all connected to each other</em>.
     * That is, for each computer at the LAN party, that computer will have a connection to every other computer at the LAN party.
     * <p>
     * In the above example, the largest set of computers that are all connected to each other is made up of <code>co</code>, <code>de</code>, <code>ka</code>, and <code>ta</code>.
     * Each computer in this set has a connection to every other computer in the set:
     * <pre>
     * ka-co
     * ta-co
     * de-co
     * ta-ka
     * de-ta
     * ka-de
     * </pre>
     * <p>
     * The LAN party posters say that the <em>password</em> to get into the LAN party is the name of every computer at the LAN party, sorted alphabetically, then joined together with commas.
     * (The people running the LAN party are clearly a bunch of <span title="You caught me. I'm a giant nerd.">nerds</span>.)
     * In this example, the password would be <code><em>co,de,ka,ta</em></code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the password to get into the LAN party?
     */
    @Part(part = II)
    public String calculateAnswerForPart2(final SolutionContext context) {
        final int[][] computers = parseComputers(context);
        return findLanPartyPassword(computers);
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link ObjectTuple} containing the answers for both parts
     */
    @Optimised
    public ObjectTuple<Integer, String> calculateAnswers(final SolutionContext context) {
        final int[][] computers = parseComputers(context);
        return new ObjectTuple<>(
                countClustersOfThreeContainingAComputerStartingWithT(computers),
                findLanPartyPassword(computers)
        );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Parse all the links between computers.
     */
    private static int[][] parseComputers(final SolutionContext context) {
        final int[][] computers = new int[MAX_2][];
        context.stream()
                .flatMapToInt(Day23::parseLink)
                .sorted()
                .forEach(link -> registerLink(link, computers));
        return computers;
    }

    /*
     * Parse a link into an integer representation.
     *
     * Return two copies, representing the bi-directionalness.
     */
    private static IntStream parseLink(final String line) {
        assert line.length() == SIZE_1;

        final int c1 = toInt(line, 0, 1);
        final int c2 = toInt(line, 3, 4);
        return IntStream.of(
                // "Forwards"
                c1 << SIZE_2 | c2,
                // ... and "backwards"
                c2 << SIZE_2 | c1
        );
    }

    /*
     * Register a uni-directional link.
     */
    private static void registerLink(final int link, final int[][] computers) {
        final int c1 = link >> SIZE_2 & MAX_2;
        final int c2 = link & MAX_2;
        assert (c1 << SIZE_2 | c2) == link;

        if (computers[c1] == null) {
            computers[c1] = new int[MAX_LINKS];
            Arrays.fill(computers[c1], -1);
            computers[c1][0] = 0;
        }

        final int c1c = ++computers[c1][0];
        assert c1c < MAX_LINKS;
        computers[c1][c1c] = c2;
    }

    /*
     * Count the number of clusters of 3 computers that contain at least one
     * computer with a hostname starting with `t`.
     */
    private static int countClustersOfThreeContainingAComputerStartingWithT(final int[][] allLinks) {
        final Set<Long> result = new HashSet<>();
        for (int c1 = 0; c1 <= allLinks.length; c1++) {
            assert c1 >= 0;
            if (c1 >> SIZE_1 != MAGIC_T) continue; // Doesn't start with a `t`...

            final int[] links1 = allLinks[c1];
            if (links1 == null) continue; // Not set...

            final int numLinks1 = links1[0];
            for (int i2 = 1; i2 <= numLinks1; i2++) {
                final int c2 = links1[i2];
                assert c2 >= 0;

                final int[] links2 = allLinks[c2];
                assert links2 != null; // Should always be a back reference...

                final int numLinks2 = links2[0];
                for (int i3 = 1; i3 <= numLinks2; i3++) {
                    final int c3 = links2[i3];
                    assert c3 >= 0;

                    final int[] links3 = allLinks[c3];
                    assert links3 != null; // Should always be a back reference...

                    final int idx = Arrays.binarySearch(links1, 1, numLinks1, c3);
                    if (idx < 1) continue; // Not found in links1...

                    final long[] r = new long[] { c1, c2, c3 };
                    Arrays.sort(r);
                    if (r[0] == r[1] || r[1] == r[2]) continue;
                    result.add(r[0] << 20 | r[1] << SIZE_2 | r[2]);
                }
            }
        }

        return result.size();
    }

    /*
     * Find the password to the LAN party.
     */
    private static String findLanPartyPassword(final int[][] computers) {
        final int[] allComputers = IntStream.range(0, MAX_2)
                .filter(i -> computers[i] != null)
                //.sorted() // _should_ already be sorted???
                .toArray();

        return IntStream.of(findLanParty(computers, allComputers))
                .mapToObj(Day23::toString)
                .collect(Collectors.joining(","));
    }

    /*
     * Find the LAN party by finding the largest cluster of computers.
     */
    private static int[] findLanParty(final int[][] computers, final int[] allComputers) {
        if (allComputers.length == 0) return NO_COMPUTERS;

        int[] result = NO_COMPUTERS;
        for (final int start : allComputers) {
            if (Arrays.binarySearch(result, start) >= 0) continue;

            final int[] links = computers[start];
            final int[] remaining = Arrays.stream(links, 1, links[0] + 1)
                    .filter(link -> Arrays.binarySearch(allComputers, link) >= 0)
                    //.sorted()
                    .toArray();
            // Not enough left to beat the current size?
            if (remaining.length < result.length) continue;

            final int[] cluster = findLanParty(computers, remaining);
            if (cluster.length < result.length) continue; // Not big enough to matter...

            result = new int[cluster.length + 1];
            result[0] = start;
            System.arraycopy(cluster, 0, result, 1, cluster.length);
            Arrays.sort(result);
        }
        return result;
    }

    /*
     * Convert a `String` representation of a computer into an `int`.
     */
    private static int toInt(final String line, final int upper, final int lower) {
        final int c1a = line.charAt(upper) - 'a';
        assert c1a < MAX_1;
        final int c1b = line.charAt(lower) - 'a';
        assert c1b < MAX_1;
        return c1a << SIZE_1 | c1b;
    }

    /*
     * Convert an `int` representation of a computer into a `String`.
     */
    private static String toString(final int c) {
        return new String(new char[]{(char) ('a' + (c >> SIZE_1 & MAX_1)), (char) ('a' + (c & MAX_1))});
    }

}

