package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;

import java.util.Arrays;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 9: Disk Fragmenter.
 */
@Solution(year = 2024, day = 9, title = "Disk Fragmenter")
public class Day9 {

    // ****************************************
    // Private Members
    // ****************************************

    private final int initialDiskSizeHint;

    // ****************************************
    // Constructors
    // ****************************************

    public Day9() {
        this(1024);
    }

    Day9(final int initialDiskSizeHint) {
        this.initialDiskSizeHint = initialDiskSizeHint;
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Another push of the button leaves you in the familiar hallways of some friendly <a href="/2021/day/23">amphipods</a>!
     * Good thing you each somehow got your own personal mini submarine.
     * The Historians jet away in search of the Chief, mostly by driving directly into walls.
     * <p>
     * While The Historians quickly figure out how to pilot these things, you notice an amphipod in the corner struggling with his computer.
     * He's trying to make more contiguous free space by compacting all of the files, but his program isn't working; you offer to help.
     * <p>
     * He shows you the <em>disk map</em> (your puzzle input) he's already generated.
     * For example:
     * <pre>
     * 2333133121414131402
     * </pre>
     * <p>
     * The disk map uses a dense format to represent the layout of <em>files</em> and <em>free space</em> on the disk.
     * The digits alternate between indicating the length of a file and the length of free space.
     * <p>
     * So, a disk map like <code>12345</code> would represent a one-block file, two blocks of free space, a three-block file, four blocks of free space, and then a five-block file.
     * A disk map like <code>90909</code> would represent three nine-block files in a row (with no free space between them).
     * <p>
     * Each file on disk also has an <em>ID number</em> based on the order of the files as they appear <em>before</em> they are rearranged, starting with ID <code>0</code>.
     * So, the disk map <code>12345</code> has three files: a one-block file with ID <code>0</code>, a three-block file with ID <code>1</code>, and a five-block file with ID <code>2</code>.
     * Using one character for each block where digits are the file ID and <code>.</code> is free space, the disk map <code>12345</code> represents these individual blocks:
     * <pre>
     * 0..111....22222
     * </pre>
     * <p>
     * The first example above, <code>2333133121414131402</code>, represents these individual blocks:
     * <pre>
     * 00...111...2...333.44.5555.6666.777.888899
     * </pre>
     * <p>
     * The amphipod would like to <em>move file blocks one at a time</em> from the end of the disk to the leftmost free space block (until there are no gaps remaining between file blocks).
     * For the disk map <code>12345</code>, the process looks like this:
     * <pre>
     * 0..111....22222
     * 02.111....2222.
     * 022111....222..
     * 0221112...22...
     * 02211122..2....
     * 022111222......
     * </pre>
     * <p>
     * The first example requires a few more steps:
     * <pre>
     * 00...111...2...333.44.5555.6666.777.888899
     * 009..111...2...333.44.5555.6666.777.88889.
     * 0099.111...2...333.44.5555.6666.777.8888..
     * 00998111...2...333.44.5555.6666.777.888...
     * 009981118..2...333.44.5555.6666.777.88....
     * 0099811188.2...333.44.5555.6666.777.8.....
     * 009981118882...333.44.5555.6666.777.......
     * 0099811188827..333.44.5555.6666.77........
     * 00998111888277.333.44.5555.6666.7.........
     * 009981118882777333.44.5555.6666...........
     * 009981118882777333644.5555.666............
     * 00998111888277733364465555.66.............
     * 0099811188827773336446555566..............
     * </pre>
     * <p>
     * The final step of this file-compacting process is to update the <em>filesystem checksum</em>.
     * To calculate the checksum, add up the result of multiplying each of these blocks' position with the file ID number it contains.
     * The leftmost block is in position <code>0</code>.
     * If a block contains free space, skip it instead.
     * <p>
     * Continuing the first example, the first few blocks' position multiplied by its file ID number are <code>0 * 0 = 0</code>, <code>1 * 0 = 0</code>, <code>2 * 9 = 18</code>, <code>3 * 9 = 27</code>, <code>4 * 8 = 32</code>, and so on.
     * In this example, the checksum is the sum of these, <code><em>1928</em></code>.
     * <p>
     * <span title="Bonus points if you make a cool animation of this process.">Compact the amphipod's hard drive</span> using the process he requested.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the resulting filesystem checksum?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .flatMapToInt(String::chars)
                .map(c -> c - '0')
                .boxed()
                .reduce(
                        DiskMap.ofSize(initialDiskSizeHint),
                        DiskMap::addBlock,
                        Day9::preventMerge
                )
                .defragment()
                .calculateChecksum();
    }

    /**
     * Upon completion, two things immediately become clear.
     * First, the disk definitely has a lot more contiguous free space, just like the amphipod hoped.
     * Second, the computer is running much more slowly!
     * Maybe introducing all of that <a href="https://en.wikipedia.org/wiki/File_system_fragmentation" target="_blank">file system fragmentation</a> was a bad idea?
     * <p>
     * The eager amphipod already has a new plan: rather than move individual blocks, he'd like to try compacting the files on his disk by moving <em>whole files</em> instead.
     * <p>
     * This time, attempt to move whole files to the leftmost span of free space blocks that could fit the file.
     * Attempt to move each file exactly once in order of <em>decreasing file ID number</em> starting with the file with the highest file ID number.
     * If there is no span of free space to the left of a file that is large enough to fit the file, the file does not move.
     * <p>
     * The first example from above now proceeds differently:
     * <pre>
     * 00...111...2...333.44.5555.6666.777.888899
     * 0099.111...2...333.44.5555.6666.777.8888..
     * 0099.1117772...333.44.5555.6666.....8888..
     * 0099.111777244.333....5555.6666.....8888..
     * 00992111777.44.333....5555.6666.....8888..
     * </pre>
     * <p>
     * The process of updating the filesystem checksum is the same; now, this example's checksum would be <code><em>2858</em></code>.
     * <p>
     * Start over, now compacting the amphipod's hard drive using this new method instead.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the resulting filesystem checksum?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return context.stream()
                .flatMapToInt(String::chars)
                .map(c -> c - '0')
                .boxed()
                .reduce(
                        DiskMap.ofSize(initialDiskSizeHint),
                        DiskMap::addBlock,
                        Day9::preventMerge
                )
                .optimise()
                .calculateChecksum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final DiskMap map = context.stream()
                .flatMapToInt(String::chars)
                .map(c -> c - '0')
                .boxed()
                .reduce(
                        DiskMap.ofSize(1024),
                        DiskMap::addBlock,
                        Day9::preventMerge
                );
        return new LongTuple(
                map.defragment().calculateChecksum(),
                map.optimise().calculateChecksum()
        );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Prevent merging - it should never happen!
     */
    private static DiskMap preventMerge(final DiskMap a, final DiskMap b) {
        throw new IllegalStateException("No need to merge");
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Represents a map of files and free space on disk.
     */
    private static final class DiskMap {

        /*
         * Indicates free space
         */
        private static final int FREE_SPACE = -1;

        // Private Members

        private int currentId = 0;
        private boolean isFile = true;

        private int[] items;
        private int size = 0;

        // Constructors

        DiskMap(final int[] initialItems) {
            this.items = initialItems;
        }

        // Helper Methods

        /*
         * Defragment the disk by moving all blocks to the left-most free space.
         */
        DiskMap defragment() {
            final int[] defragged = Arrays.copyOf(this.items, size);

            int forward = 0;
            int backwards = size - 1;
            while (forward < backwards) {
                if (defragged[forward] != FREE_SPACE) {
                    ++forward;
                    continue;
                }

                while (defragged[backwards] == FREE_SPACE && forward < backwards)
                    --backwards;

                defragged[forward++] = defragged[backwards];
                defragged[backwards--] = FREE_SPACE;
            }

            final DiskMap map = new DiskMap(defragged);
            map.currentId = this.currentId;
            map.isFile = this.isFile;
            map.size = this.size;
            return map;
        }

        /*
         * Optimise the disk by moving all files to the left-most free space that
         * is large enough to hold the full file.
         */
        DiskMap optimise() {
            final int[] optimised = Arrays.copyOf(this.items, size);

            int backwards = size - 1;
            int knownForward = 0;
            out: while (backwards > knownForward) {
                if (optimised[backwards] == FREE_SPACE) {
                    --backwards;
                    continue;
                }

                final int currentId = optimised[backwards];
                final int currentEnd = backwards;
                while (optimised[backwards] == currentId && backwards > 0)
                    --backwards;

                final int itemSize = currentEnd - backwards;

                int forwards = knownForward;
                while (forwards <= backwards) {
                    if (optimised[forwards] != FREE_SPACE) {
                        ++forwards;
                        continue;
                    }
                    knownForward = forwards;
                    break;
                }

                while (forwards <= backwards) {
                    if (optimised[forwards] != FREE_SPACE) {
                        ++forwards;
                        continue;
                    }

                    final int currentSpaceStart = forwards;
                    while (optimised[forwards] == FREE_SPACE && forwards <= backwards)
                        ++forwards;

                    final int currentSpaceSize = forwards - currentSpaceStart;
                    if (currentSpaceSize >= itemSize) {
                        for (int p = 0; p < itemSize; p++) {
                            optimised[currentSpaceStart + p] = currentId;
                            optimised[backwards + p + 1] = FREE_SPACE;
                        }
                        continue out;
                    }
                }
            }

            final DiskMap map = new DiskMap(optimised);
            map.currentId = this.currentId;
            map.isFile = this.isFile;
            map.size = this.size;
            return map;
        }

        /*
         * Calculate the checksum of the disk map.
         */
        long calculateChecksum() {
            long checksum = 0;
            for (int i = 0; i < size; i++)
                if (items[i] != FREE_SPACE)
                    checksum += (long) i * items[i];
            return checksum;
        }

        /*
         * Add a new block to the disk map.
         */
        DiskMap addBlock(final int size) {
            final int id = isFile ? currentId++ : FREE_SPACE;
            final int newSize = this.size + size;
            for (int p = this.size; p < newSize; p++) {
                if (newSize >= items.length)
                    items = Arrays.copyOf(items, items.length * 2);
                items[p] = id;
            }
            this.size = newSize;
            this.isFile = !this.isFile;
            return this;
        }

        // Static Helper Methods

        /*
         * Create a new `DiskMap`, allocating a certain number of blocks for
         * storage. This `DiskMap` can still grow if the number of blocks exceeds
         * this size, it is merely a suggestion for what an appropriate starting
         * point is.
         */
        static DiskMap ofSize(final int initialDiskSizeHint) {
            return new DiskMap(new int[initialDiskSizeHint]);
        }

    }

}

