package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 1: Historian Hysteria.
 */
@Solution(year = 2024, day = 1, title = "Historian Hysteria")
public class Day1 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The <em>Chief Historian</em> is always present for the big Christmas sleigh launch, but nobody has seen him in months!
     * Last anyone heard, he was visiting locations that are historically significant to the North Pole; a group of Senior Historians has asked you to accompany them as they check the places they think he was most likely to visit.
     * <p>
     * As each location is checked, they will mark it on their list with a <em class="star">star</em>.
     * They figure the Chief Historian <em>must</em> be in one of the first fifty places they'll look, so in order to save Christmas, you need to help them get <em class="star">fifty stars</em> on their list before Santa takes off on December 25th.
     * <p>
     * Collect stars by solving puzzles.
     * Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first.
     * Each puzzle grants <em class="star">one star</em>.
     * Good luck!
     * <p>
     * You haven't even left yet and the group of Elvish Senior Historians has already hit a problem: their list of locations to check is currently <em>empty</em>.
     * Eventually, someone decides that the best place to check first would be the Chief Historian's office.
     * <p>
     * Upon pouring into the office, everyone confirms that the Chief Historian is indeed nowhere to be found.
     * Instead, the Elves discover an assortment of notes and lists of historically significant locations!
     * This seems to be the planning the Chief Historian was doing before he left.
     * Perhaps these notes can be used to determine which locations to search?
     * <p>
     * Throughout the Chief's office, the historically significant locations are listed not by name but by a unique number called the <em>location ID</em>.
     * To make sure they don't miss anything, The Historians split into two groups, each searching the office and trying to create their own complete list of location IDs.
     * <p>
     * There's just one problem: by holding the two lists up <em>side by side</em> (your puzzle input), it quickly becomes clear that the lists aren't very similar.
     * Maybe you can help The Historians reconcile their lists?
     * <p>
     * For example:
     * <pre>
     * 3   4
     * 4   3
     * 2   5
     * 1   3
     * 3   9
     * 3   3
     * </pre>
     * <p>
     * Maybe the lists are only off by a small amount!
     * To find out, pair up the numbers and measure how far apart they are.
     * Pair up the <em>smallest number in the left list</em> with the <em>smallest number in the right list</em>, then the <em>second-smallest left number</em> with the <em>second-smallest right number</em>, and so on.
     * <p>
     * Within each pair, figure out <em>how far apart</em> the two numbers are; you'll need to <em>add up all of those distances</em>.
     * For example, if you pair up a <code>3</code> from the left list with a <code>7</code> from the right list, the distance apart is <code>4</code>; if you pair up a <code>9</code> with a <code>3</code>, the distance apart is <code>6</code>.
     * <p>
     * In the example list above, the pairs and distances would be as follows:
     * <ul>
     * <li>The smallest number in the left list is <code>1</code>, and the smallest number in the right list is <code>3</code>.
     * The distance between them is <code><em>2</em></code>.</li>
     * <li>The second-smallest number in the left list is <code>2</code>, and the second-smallest number in the right list is another <code>3</code>.
     * The distance between them is <code><em>1</em></code>.</li>
     * <li>The third-smallest number in both lists is <code>3</code>, so the distance between them is <code><em>0</em></code>.</li>
     * <li>The next numbers to pair up are <code>3</code> and <code>4</code>, a distance of <code><em>1</em></code>.</li>
     * <li>The fifth-smallest numbers in each list are <code>3</code> and <code>5</code>, a distance of <code><em>2</em></code>.</li>
     * <li>Finally, the largest number in the left list is <code>4</code>, while the largest number in the right list is <code>9</code>; these are a distance <code><em>5</em></code> apart.</li>
     * </ul>
     * <p>
     * To find the <em>total distance</em> between the left list and the right list, add up the distances between all of the pairs you found.
     * In the example above, this is <code>2 + 1 + 0 + 1 + 2 + 5</code>, a total distance of <code><em>11</em></code>!
     * <p>
     * Your actual left and right lists contain many location IDs.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the total distance between your lists?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final List<String> lines = context.read();
        final int[] left = new int[lines.size()];
        final int[] right = new int[lines.size()];
        for (int i = 0, linesSize = lines.size(); i < linesSize; i++) {
            final String line = lines.get(i);
            final String[] split = line.split("\\s+");
            left[i] = Integer.parseInt(split[0]);
            right[i] = Integer.parseInt(split[1]);
        }

        Arrays.sort(left);
        Arrays.sort(right);

        return IntStream.range(0, lines.size())
                .map(i -> Math.abs(right[i] - left[i]))
                .sum();
    }

    /**
     * Your analysis only confirmed what everyone feared: the two lists of location IDs are indeed very different.
     * <p>
     * Or are they?
     * <p>
     * The Historians can't agree on which group made the mistakes <em>or</em> how to read most of the Chief's handwriting, but in the commotion you notice an interesting detail: <span title="We were THIS close to summoning the Alot of Location IDs!">a lot</span> of location IDs appear in both lists!
     * Maybe the other numbers aren't location IDs at all but rather misinterpreted handwriting.
     * <p>
     * This time, you'll need to figure out exactly how often each number from the left list appears in the right list.
     * Calculate a total <em>similarity score</em> by adding up each number in the left list after multiplying it by the number of times that number appears in the right list.
     * <p>
     * Here are the same example lists again:
     * <pre>
     * 3   4
     * 4   3
     * 2   5
     * 1   3
     * 3   9
     * 3   3
     * </pre>
     * <p>
     * For these example lists, here is the process of finding the similarity score:
     * <ul>
     * <li>The first number in the left list is <code>3</code>.
     * It appears in the right list three times, so the similarity score increases by <code>3 * 3 = <em>9</em></code>.</li>
     * <li>The second number in the left list is <code>4</code>.
     * It appears in the right list once, so the similarity score increases by <code>4 * 1 = <em>4</em></code>.</li>
     * <li>The third number in the left list is <code>2</code>.
     * It does not appear in the right list, so the similarity score does not increase (<code>2 * 0 = 0</code>).</li>
     * <li>The fourth number, <code>1</code>, also does not appear in the right list.</li>
     * <li>The fifth number, <code>3</code>, appears in the right list three times; the similarity score increases by <code><em>9</em></code>.</li>
     * <li>The last number, <code>3</code>, appears in the right list three times; the similarity score again increases by <code><em>9</em></code>.</li>
     * </ul>
     * <p>
     * So, for these example lists, the similarity score at the end of this process is <code><em>31</em></code> (<code>9 + 4 + 0 + 0 + 9 + 9</code>).
     * <p>
     * Once again consider your left and right lists.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is their similarity score?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Map<Integer, CounterPair> counts = new HashMap<>();
        context.stream()
                .map(line -> line.split("\\s+"))
                .forEach(split -> {
                    counts.computeIfAbsent(Integer.parseInt(split[0]), CounterPair::new).leftCount++;
                    counts.computeIfAbsent(Integer.parseInt(split[1]), CounterPair::new).rightCount++;
                });

        return counts.values().stream()
                .mapToInt(CounterPair::computeSimilarityScore)
                .sum();
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * Count the number of occurrences of specific given location ID.
     */
    private static final class CounterPair {
        private final int locationId;

        private int leftCount = 0;
        private int rightCount = 0;

        private CounterPair(final int locationId) {
            this.locationId = locationId;
        }

        public int computeSimilarityScore() {
            return locationId * leftCount * rightCount;
        }
    }

}

