package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;

import java.util.List;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 13: Claw Contraption.
 */
@Solution(year = 2024, day = 13, title = "Claw Contraption")
public class Day13 {

    /*
     * Can't win the prize :(
     */
    private static final int NO_PRIZE_FOR_YOU = 0;

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Next up: the <a href="/2020/day/24">lobby</a> of a resort on a tropical island.
     * The Historians take a moment to admire the hexagonal floor tiles before spreading out.
     * <p>
     * Fortunately, it looks like the resort has a new <a href="https://en.wikipedia.org/wiki/Amusement_arcade">arcade</a>!
     * Maybe you can win some prizes from the <a href="https://en.wikipedia.org/wiki/Claw_machine" target="_blank">claw machines</a>?
     * <p>
     * The claw machines here are a little unusual.
     * Instead of a joystick or directional buttons to control the claw, these machines have two buttons labeled <code>A</code> and <code>B</code>.
     * Worse, you can't just put in a token and play; it costs <em>3 tokens</em> to push the <code>A</code> button and <em>1 token</em> to push the <code>B</code> button.
     * <p>
     * With a little experimentation, you figure out that each machine's buttons are configured to move the claw a specific amount to the <em>right</em> (along the <code>X</code> axis) and a specific amount <em>forward</em> (along the <code>Y</code> axis) each time that button is pressed.
     * <p>
     * Each machine contains one <em>prize</em>; to win the prize, the claw must be positioned <em>exactly</em> above the prize on both the <code>X</code> and <code>Y</code> axes.
     * <p>
     * You wonder: what is the smallest number of tokens you would have to spend to win as many prizes as possible?
     * You assemble a list of every machine's button behavior and prize location (your puzzle input).
     * For example:
     * <pre>
     * Button A: X+94, Y+34
     * Button B: X+22, Y+67
     * Prize: X=8400, Y=5400
     *
     * Button A: X+26, Y+66
     * Button B: X+67, Y+21
     * Prize: X=12748, Y=12176
     *
     * Button A: X+17, Y+86
     * Button B: X+84, Y+37
     * Prize: X=7870, Y=6450
     *
     * Button A: X+69, Y+23
     * Button B: X+27, Y+71
     * Prize: X=18641, Y=10279
     * </pre>
     * <p>
     * This list describes the button configuration and prize location of four different claw machines.
     * <p>
     * For now, consider just the first claw machine in the list:
     * <ul>
     * <li>Pushing the machine's <code>A</code> button would move the claw <code>94</code> units along the <code>X</code> axis and <code>34</code> units along the <code>Y</code> axis.</li>
     * <li>Pushing the <code>B</code> button would move the claw <code>22</code> units along the <code>X</code> axis and <code>67</code> units along the <code>Y</code> axis.</li>
     * <li>The prize is located at <code>X=8400</code>, <code>Y=5400</code>; this means that from the claw's initial position, it would need to move exactly <code>8400</code> units along the <code>X</code> axis and exactly <code>5400</code> units along the <code>Y</code> axis to be perfectly aligned with the prize in this machine.</li>
     * </ul>
     * <p>
     * The cheapest way to win the prize is by pushing the <code>A</code> button <code>80</code> times and the <code>B</code> button <code>40</code> times.
     * This would line up the claw along the <code>X</code> axis (because <code>80*94 + 40*22 = 8400</code>) and along the <code>Y</code> axis (because <code>80*34 + 40*67 = 5400</code>).
     * Doing this would cost <code>80*3</code> tokens for the <span title="Half A presses are not allowed."><code>A</code> presses</span> and <code>40*1</code> for the <code>B</code> presses, a total of <code><em>280</em></code> tokens.
     * <p>
     * For the second and fourth claw machines, there is no combination of A and B presses that will ever win a prize.
     * <p>
     * For the third claw machine, the cheapest way to win the prize is by pushing the <code>A</code> button <code>38</code> times and the <code>B</code> button <code>86</code> times.
     * Doing this would cost a total of <code><em>200</em></code> tokens.
     * <p>
     * So, the most prizes you could possibly win is two; the minimum tokens you would have to spend to win all (two) prizes is <code><em>480</em></code>.
     * <p>
     * You estimate that each button would need to be pressed <em>no more than <code>100</code> times</em> to win a prize.
     * How else would someone be expected to play?
     * <p>
     * Figure out how to win as many prizes as possible.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the fewest tokens you would have to spend to win all possible prizes?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.streamBatches()
                .map(ButtonsAndPrize::parseInput)
                .mapToLong(bap -> determineMinimumTokens(bap, 0))
                .sum();
    }

    /**
     * As you go to win the first prize, you discover that the claw is nowhere near where you expected it would be.
     * Due to a unit conversion error in your measurements, the position of every prize is actually <code>10000000000000</code> higher on both the <code>X</code> and <code>Y</code> axis!
     * <p>
     * Add <code>10000000000000</code> to the <code>X</code> and <code>Y</code> position of every prize.
     * After making this change, the example above would now look like this:
     * <pre>
     * Button A: X+94, Y+34
     * Button B: X+22, Y+67
     * Prize: X=10000000008400, Y=10000000005400
     *
     * Button A: X+26, Y+66
     * Button B: X+67, Y+21
     * Prize: X=10000000012748, Y=10000000012176
     *
     * Button A: X+17, Y+86
     * Button B: X+84, Y+37
     * Prize: X=10000000007870, Y=10000000006450
     *
     * Button A: X+69, Y+23
     * Button B: X+27, Y+71
     * Prize: X=10000000018641, Y=10000000010279
     * </pre>
     * <p>
     * Now, it is only possible to win a prize on the second and fourth claw machines.
     * Unfortunately, it will take <em>many more than <code>100</code> presses</em> to do so.
     * <p>
     * Using the corrected prize coordinates, figure out how to win as many prizes as possible.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the fewest tokens you would have to spend to win all possible prizes?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return context.streamBatches()
                .map(ButtonsAndPrize::parseInput)
                .mapToLong(bap -> determineMinimumTokens(bap, 10000000000000L))
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        return context.streamBatches()
                .map(ButtonsAndPrize::parseInput)
                .reduce(
                        LongTuple.NULL,
                        (res, bap) -> res.add(
                                determineMinimumTokens(bap, 0L),
                                determineMinimumTokens(bap, 10000000000000L)
                        ),
                        LongTuple::add
                );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Determine the minimum number of tokens needed to get the prize, if it
     * is possible to do so.
     */
    private static long determineMinimumTokens(final ButtonsAndPrize r, final long prizeOffset) {
        final long prizex = prizeOffset + r.prizex();
        final long prizey = prizeOffset + r.prizey();

        // (i * ax) + (j * bx) = prizex
        // (i * ay) + (j * by) = prizey

        // Solve simultaneous equation...
        // (i * ax * by) + (j * bx * by) = prizex * by
        // (i * ay * bx) + (j * by * bx) = prizey * bx

        // (i * (ax * by - ay * bx)) = (prizex * by) - (prizey * bx)
        // i = (prizex * by) - (prizey * bx) / (ax * by - ay * bx)

        final long i = ((prizex * r.by()) - (prizey * r.bx()))
                / (r.ax() * r.by() - r.ay() * r.bx());

        // j = (prizex - (i * ax)) / bx
        final long j = (prizex - (i * r.ax())) / r.bx();

        if (i < 0 || j < 0) return NO_PRIZE_FOR_YOU;

        final boolean checkx = (i * r.ax()) + (j * r.bx()) == prizex;
        final boolean checky = (i * r.ay()) + (j * r.by()) == prizey;
        if (!checkx || !checky) return NO_PRIZE_FOR_YOU;

        return 3 * i + j;
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Contains the button configurations along with the prize co-ords.
     */
    private record ButtonsAndPrize(
            long ax,
            long ay,
            long bx,
            long by,
            long prizex,
            long prizey
    ) {

        // Static Helper Methods

        /*
         * Parse a `ButtonsAndPrize` from the given batch.
         */
        private static ButtonsAndPrize parseInput(final List<String> batch) {
            final String buttonA = batch.get(0);
            final String buttonB = batch.get(1);
            final String prize = batch.get(2);

            final String[] splitA = buttonA.split("[:,]\\s*");

            final String[] splitB = buttonB.split("[:,]\\s*");

            final String[] splitPrize = prize.split("[:,]\\s*");
            return new ButtonsAndPrize(
                    Integer.parseInt(splitA[1].substring(2)),
                    Integer.parseInt(splitA[2].substring(2)),
                    Integer.parseInt(splitB[1].substring(2)),
                    Integer.parseInt(splitB[2].substring(2)),
                    Integer.parseInt(splitPrize[1].substring(2)),
                    Integer.parseInt(splitPrize[2].substring(2))
            );
        }
    }

}

