package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 11: Plutonian Pebbles.
 */
@Solution(year = 2024, day = 11, title = "Plutonian Pebbles")
public class Day11 {

    private static final int MIN_RUNS = 25;
    private static final int MAX_RUNS = 75;

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The ancient civilization on <a href="/2019/day/20">Pluto</a> was known for its ability to manipulate spacetime, and while The Historians explore their infinite corridors, you've noticed a strange set of physics-defying stones.
     * <p>
     * At first glance, they seem like normal stones: they're arranged in a perfectly <em>straight line</em>, and each stone has a <em>number</em> engraved on it.
     * <p>
     * The strange part is that every time you <span title="No, they're not statues. Why do you ask?">blink</span>, the stones <em>change</em>.
     * <p>
     * Sometimes, the number engraved on a stone changes.
     * Other times, a stone might <em>split in two</em>, causing all the other stones to shift over a bit to make room in their perfectly straight line.
     * <p>
     * As you observe them for a while, you find that the stones have a consistent behavior.
     * Every time you blink, the stones each <em>simultaneously</em> change according to the <em>first applicable rule</em> in this list:
     * <ul>
     * <li>If the stone is engraved with the number <code>0</code>, it is replaced by a stone engraved with the number <code>1</code>.</li>
     * <li>If the stone is engraved with a number that has an <em>even</em> number of digits, it is replaced by <em>two stones</em>.
     * The left half of the digits are engraved on the new left stone, and the right half of the digits are engraved on the new right stone.
     * (The new numbers don't keep extra leading zeroes: <code>1000</code> would become stones <code>10</code> and <code>0</code>.)</li>
     * <li>If none of the other rules apply, the stone is replaced by a new stone; the old stone's number <em>multiplied by 2024</em> is engraved on the new stone.</li>
     * </ul>
     * <p>
     * No matter how the stones change, their <em>order is preserved</em>, and they stay on their perfectly straight line.
     * <p>
     * How will the stones evolve if you keep blinking at them?
     * You take a note of the number engraved on each stone in the line (your puzzle input).
     * <p>
     * If you have an arrangement of five stones engraved with the numbers <code>0 1 10 99 999</code> and you blink once, the stones transform as follows:
     * <ul>
     * <li>The first stone, <code>0</code>, becomes a stone marked <code>1</code>.</li>
     * <li>The second stone, <code>1</code>, is multiplied by 2024 to become <code>2024</code>.</li>
     * <li>The third stone, <code>10</code>, is split into a stone marked <code>1</code> followed by a stone marked <code>0</code>.</li>
     * <li>The fourth stone, <code>99</code>, is split into two stones marked <code>9</code>.</li>
     * <li>The fifth stone, <code>999</code>, is replaced by a stone marked <code>2021976</code>.</li>
     * </ul>
     * <p>
     * So, after blinking once, your five stones would become an arrangement of seven stones engraved with the numbers <code>1 2024 1 0 9 9 2021976</code>.
     * <p>
     * Here is a longer example:
     * <pre>
     * Initial arrangement:
     * 125 17
     *
     * After 1 blink:
     * 253000 1 7
     *
     * After 2 blinks:
     * 253 0 2024 14168
     *
     * After 3 blinks:
     * 512072 1 20 24 28676032
     *
     * After 4 blinks:
     * 512 72 2024 2 0 2 4 2867 6032
     *
     * After 5 blinks:
     * 1036288 7 2 20 24 4048 1 4048 8096 28 67 60 32
     *
     * After 6 blinks:
     * 2097446912 14168 4048 2 0 2 4 40 48 2024 40 48 80 96 2 8 6 7 6 0 3 2
     * </pre>
     * <p>
     * In this example, after blinking six times, you would have <code>22</code> stones.
     * After blinking 25 times, you would have <code><em>55312</em></code> stones!
     * <p>
     * Consider the arrangement of stones in front of you.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many stones will you have after blinking 25 times?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<Stone> stones = context.processLine(line -> stream(line.split("\\s+"))
                .map(Stone::create)
                .peek(s -> s.age[0] = 1)
                .toList());

        return countStones(blinkRepeatedly(stones, MIN_RUNS), MIN_RUNS);
    }

    /**
     * The Historians sure are taking a long time.
     * To be fair, the infinite corridors <em>are</em> very large.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many stones would you have after blinking a total of 75 times?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<Stone> stones = context.processLine(line -> stream(line.split("\\s+"))
                .map(Stone::create)
                .peek(s -> s.age[0] = 1)
                .toList());

        return countStones(blinkRepeatedly(stones, MAX_RUNS), MAX_RUNS);
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final List<Stone> stones = context.processLine(line -> stream(line.split("\\s+"))
                .map(Stone::create)
                .peek(s -> s.age[0] = 1)
                .toList());

        final Collection<Stone> finalStones = blinkRepeatedly(stones, MAX_RUNS);

        return new LongTuple(
                countStones(finalStones, 25),
                countStones(finalStones, MAX_RUNS)
        );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Blink a given number of times.
     */
    private static Collection<Stone> blinkRepeatedly(final Collection<Stone> stones, final int numberOfBlinks) {
        final Map<Long, Stone> stoneIdx = stones.stream()
                .collect(Collectors.toMap(s -> s.number, Function.identity()));

        final Set<Long> next = new HashSet<>(stoneIdx.keySet());
        for (int age = 0; age < numberOfBlinks; age++)
            next.addAll(blink(next, stoneIdx, age));

        return stoneIdx.values();
    }

    /*
     * Blink once.
     */
    private static Set<Long> blink(final Set<Long> numbers, final Map<Long, Stone> stoneIdx, final int age) {
        final Set<Long> nextNumbers = new HashSet<>();
        final Iterator<Long> iterator = numbers.iterator();
        while (iterator.hasNext()) {
            final long number = iterator.next();
            iterator.remove();

            final Stone stone = stoneIdx.get(number);
            assert stone != null : "Stone " + number + " not found!";

            if (number == 0) {
                update(stone, age, 1L, nextNumbers, stoneIdx);
                continue;
            }

            final int length = (int) Math.log10(number);
            if (length % 2 == 0) {
                update(stone, age, number * 2024, nextNumbers, stoneIdx);
                continue;
            }

            final int divisor = (int) Math.pow(10.0, (length + 1) / 2.0);
            final long lhs = number / divisor;
            final long rhs = number - (lhs * divisor);
            update(stone, age, lhs, nextNumbers, stoneIdx);
            update(stone, age, rhs, nextNumbers, stoneIdx);
        }
        return nextNumbers;
    }

    /*
     * Update the queue and index state for the next number.
     */
    private static void update(
            final Stone stone,
            final int age,
            final long nextNumber,
            final Set<Long> next,
            final Map<Long, Stone> stoneIdx
    ) {
        final long count = stone.age[age];
        final Stone nextStone = stoneIdx.computeIfAbsent(nextNumber, Stone::new);
        nextStone.age[age + 1] += count;
        next.add(nextNumber);
    }

    /*
     * Count the number of stones that exist after a given number of blinks.
     */
    private static long countStones(final Collection<Stone> stones, final int age) {
        return stones.stream()
                .mapToLong(s -> s.age[age])
                .sum();
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A stone with a given number, and the number of times it appears in the
     * line after a number of blinks.
     */
    private static final class Stone {

        // Private Members

        private final long number;
        private final long[] age = new long[MAX_RUNS + 1];

        // Constructors

        private Stone(final long number) {
            this.number = number;
        }

        // Static Helper Methods

        /*
         * Create a new stone.
         */
        static Stone create(final String number) {
            return new Stone(Long.parseLong(number));
        }

    }

}

