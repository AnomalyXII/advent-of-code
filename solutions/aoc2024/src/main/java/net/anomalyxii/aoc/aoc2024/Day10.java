package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.*;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 10: Hoof It.
 */
@Solution(year = 2024, day = 10, title = "Hoof It")
public class Day10 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You all arrive at a <a href="/2023/day/15">Lava Production Facility</a> on a floating island in the sky.
     * As the others begin to search the massive industrial complex, you feel a small nose boop your leg and look down to discover a <span title="i knew you would come back">reindeer</span> wearing a hard hat.
     * <p>
     * The reindeer is holding a book titled "Lava Island Hiking Guide".
     * However, when you open the book, you discover that most of it seems to have been scorched by lava!
     * As you're about to ask how you can help, the reindeer brings you a blank <a href="https://en.wikipedia.org/wiki/Topographic_map" target="_blank">topographic map</a> of the surrounding area (your puzzle input) and looks up at you excitedly.
     * <p>
     * Perhaps you can help fill in the missing hiking trails?
     * <p>
     * The topographic map indicates the <em>height</em> at each position using a scale from <code>0</code> (lowest) to <code>9</code> (highest).
     * For example:
     * <pre>
     * 0123
     * 1234
     * 8765
     * 9876
     * </pre>
     * <p>
     * Based on un-scorched scraps of the book, you determine that a good hiking trail is <em>as long as possible</em> and has an <em>even, gradual, uphill slope</em>.
     * For all practical purposes, this means that a <em>hiking trail</em> is any path that starts at height <code>0</code>, ends at height <code>9</code>, and always increases by a height of exactly 1 at each step.
     * Hiking trails never include diagonal steps - only up, down, left, or right (from the perspective of the map).
     * <p>
     * You look up from the map and notice that the reindeer has helpfully begun to construct a small pile of pencils, markers, rulers, compasses, stickers, and other equipment you might need to update the map with hiking trails.
     * <p>
     * A <em>trailhead</em> is any position that starts one or more hiking trails - here, these positions will always have height <code>0</code>.
     * Assembling more fragments of pages, you establish that a trailhead's <em>score</em> is the number of <code>9</code>-height positions reachable from that trailhead via a hiking trail.
     * In the above example, the single trailhead in the top left corner has a score of <code>1</code> because it can reach a single <code>9</code> (the one in the bottom left).
     * <p>
     * This trailhead has a score of <code>2</code>:
     * <pre>
     * ...0...
     * ...1...
     * ...2...
     * 6543456
     * 7.....7
     * 8.....8
     * 9.....9
     * </pre>
     * <p>
     * (The positions marked <code>.</code> are impassable tiles to simplify these examples; they do not appear on your actual topographic map.)
     * <p>
     * This trailhead has a score of <code>4</code> because every <code>9</code> is reachable via a hiking trail except the one immediately to the left of the trailhead:
     * <pre>
     * ..90..9
     * ...1.98
     * ...2..7
     * 6543456
     * 765.987
     * 876....
     * 987....
     * </pre>
     * <p>
     * This topographic map contains <em>two</em> trailheads; the trailhead at the top has a score of <code>1</code>, while the trailhead at the bottom has a score of <code>2</code>:
     * <pre>
     * 10..9..
     * 2...8..
     * 3...7..
     * 4567654
     * ...8..3
     * ...9..2
     * .....01
     * </pre>
     * <p>
     * Here's a larger example:
     * <pre>
     * 89010123
     * 78121874
     * 87430965
     * 96549874
     * 45678903
     * 32019012
     * 01329801
     * 10456732
     * </pre>
     * <p>
     * This larger example has 9 trailheads.
     * Considering the trailheads in reading order, they have scores of <code>5</code>, <code>6</code>, <code>5</code>, <code>3</code>, <code>1</code>, <code>3</code>, <code>5</code>, <code>3</code>, and <code>5</code>.
     * Adding these scores together, the sum of the scores of all trailheads is <code><em>36</em></code>.
     * <p>
     * The reindeer gleefully carries over a protractor and adds it to the pile.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the scores of all trailheads on your topographic map?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final Grid grid = context.readGrid(c -> c - '0');
        return grid.stream()
                .mapToInt(c -> calculateTrailScore(grid, c))
                .sum();
    }

    /**
     * The reindeer spends a few minutes reviewing your hiking trail map before realizing something, disappearing for a few minutes, and finally returning with yet another slightly-charred piece of paper.
     * <p>
     * The paper describes a second way to measure a trailhead called its <em>rating</em>.
     * A trailhead's rating is the <em>number of distinct hiking trails</em> which begin at that trailhead.
     * For example:
     * <pre>
     * .....0.
     * ..4321.
     * ..5..2.
     * ..6543.
     * ..7..4.
     * ..8765.
     * ..9....
     * </pre>
     * <p>
     * The above map has a single trailhead; its rating is <code>3</code> because there are exactly three distinct hiking trails which begin at that position:
     * <pre>
     * .....0.
     * .....0.
     * .....0.
     * ..4321.
     * .....1.
     * .....1.
     * ..5....
     * .....2.
     * .....2.
     * ..6....
     * ..6543.
     * .....3.
     * ..7....
     * ..7....
     * .....4.
     * ..8....
     * ..8....
     * ..8765.
     * ..9....
     * ..9....
     * ..9....
     * </pre>
     * <p>
     * Here is a map containing a single trailhead with rating <code>13</code>:
     * <pre>
     * ..90..9
     * ...1.98
     * ...2..7
     * 6543456
     * 765.987
     * 876....
     * 987....
     * </pre>
     * <p>
     * This map contains a single trailhead with rating <code>227</code> (because there are <code>121</code> distinct hiking trails that lead to the <code>9</code> on the right edge and <code>106</code> that lead to the <code>9</code> on the bottom edge):
     * <pre>
     * 012345
     * 123456
     * 234567
     * 345678
     * 4.6789
     * 56789.
     * </pre>
     * <p>
     * Here's the larger example from before:
     * <pre>
     * 89010123
     * 78121874
     * 87430965
     * 96549874
     * 45678903
     * 32019012
     * 01329801
     * 10456732
     * </pre>
     * <p>
     * Considering its trailheads in reading order, they have ratings of <code>20</code>, <code>24</code>, <code>10</code>, <code>4</code>, <code>1</code>, <code>4</code>, <code>5</code>, <code>8</code>, and <code>5</code>.
     * The sum of all trailhead ratings in this larger example topographic map is <code><em>81</em></code>.
     * <p>
     * You're not sure how, but the reindeer seems to have crafted some tiny flags out of toothpicks and bits of paper and is using them to mark trailheads on your topographic map.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the ratings of all trailheads?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Grid grid = context.readGrid(c -> c - '0');
        return grid.stream()
                .mapToInt(c -> calculateTrailRating(grid, c))
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final Grid grid = context.readGrid(c -> c - '0');
        return grid.stream()
                .reduce(
                        IntTuple.NULL,
                        (r, c) -> {
                            final int value = grid.get(c);
                            if (value != 0) return r;

                            return r.add(
                                    calculateTrailScore(grid, c),
                                    calculateTrailRating(grid, c)
                            );
                        },
                        IntTuple::add
                );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Calculate the trail score for a given `Coordinate`.
     */
    private int calculateTrailScore(final Grid grid, final Coordinate c) {
        if (grid.get(c) != 0) return 0;

        final Deque<Coordinate> current = new ArrayDeque<>();
        current.addLast(c);
        for (int i = 1; i <= 9 && !current.isEmpty(); i++) {
            final Set<Coordinate> nexts = new HashSet<>();
            while (!current.isEmpty()) {
                final int finalI = i;
                final Coordinate next = current.pollFirst();
                grid.forEachAdjacentTo(
                        next,
                        c2 -> {
                            final int value = grid.get(c2);
                            if (value != finalI) return;

                            nexts.add(c2);
                        }
                );
            }
            current.addAll(nexts);
        }
        return current.size();
    }

    /*
     * Calculate the trail rating for a given `Coordinate`.
     */
    private int calculateTrailRating(final Grid grid, final Coordinate c) {
        if (grid.get(c) != 0) return 0;

        final Deque<Coordinate> current = new ArrayDeque<>();
        final Map<Coordinate, Integer> ratings = new HashMap<>();
        current.addLast(c);
        ratings.put(c, 1);

        for (int i = 1; i <= 9 && !current.isEmpty(); i++) {
            final Set<Coordinate> nexts = new HashSet<>();
            while (!current.isEmpty()) {
                final int finalI = i;
                final Coordinate next = current.pollFirst();
                grid.forEachAdjacentTo(
                        next,
                        c2 -> {
                            final int value = grid.get(c2);
                            if (value != finalI) return;

                            nexts.add(c2);
                            final int rating = ratings.get(next);
                            ratings.compute(c2, (k, v) -> v == null ? rating : v + rating);
                        }
                );
            }
            current.addAll(nexts);
        }
        return current.stream()
                .mapToInt(ratings::get)
                .sum();
    }

}

