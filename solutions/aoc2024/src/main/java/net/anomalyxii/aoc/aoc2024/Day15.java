package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Direction;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 15: Warehouse Woes.
 */
@Solution(year = 2024, day = 15, title = "Warehouse Woes")
public class Day15 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You appear back inside your own mini submarine!
     * Each Historian drives their mini submarine in a different direction; maybe the Chief has his own submarine down here somewhere as well?
     * <p>
     * You look up to see a vast school of <a href="/2021/day/6">lanternfish</a> swimming past you.
     * On closer inspection, they seem quite anxious, so you drive your mini submarine over to see if you can help.
     * <p>
     * Because lanternfish populations grow rapidly, they need a lot of food, and that food needs to be stored somewhere.
     * That's why these lanternfish have built elaborate warehouse complexes operated by robots!
     * <p>
     * These lanternfish seem so anxious because they have lost control of the robot that operates one of their most important warehouses!
     * It is currently running <span title="Wesnoth players might solve their Warehouse Woes with a Warehouse Wose!">amok</span>, pushing around boxes in the warehouse with no regard for lanternfish logistics <em>or</em> lanternfish inventory management strategies.
     * <p>
     * Right now, none of the lanternfish are brave enough to swim up to an unpredictable robot so they could shut it off.
     * However, if you could anticipate the robot's movements, maybe they could find a safe option.
     * <p>
     * The lanternfish already have a map of the warehouse and a list of movements the robot will <em>attempt</em> to make (your puzzle input).
     * The problem is that the movements will sometimes fail as boxes are shifted around, making the actual movements of the robot difficult to predict.
     * <p>
     * For example:
     * <pre>
     * ##########
     * #..O..O.O#
     * #......O.#
     * #.OO..O.O#
     * #..O@..O.#
     * #O#..O...#
     * #O..O..O.#
     * #.OO.O.OO#
     * #....O...#
     * ##########
     *
     * &lt;vv&gt;^&lt;v^&gt;v&gt;^vv^v&gt;v&lt;&gt;v^v&lt;v&lt;^vv&lt;&lt;&lt;^&gt;&lt;&lt;&gt;&lt;&gt;&gt;v&lt;vvv&lt;&gt;^v^&gt;^&lt;&lt;&lt;&gt;&lt;&lt;v&lt;&lt;&lt;v^vv^v&gt;^
     * vvv&lt;&lt;^&gt;^v^^&gt;&lt;&lt;&gt;&gt;&gt;&lt;&gt;^&lt;&lt;&gt;&lt;^vv^^&lt;&gt;vvv&lt;&gt;&gt;&lt;^^v&gt;^&gt;vv&lt;&gt;v&lt;&lt;&lt;&lt;v&lt;^v&gt;^&lt;^^&gt;&gt;&gt;^&lt;v&lt;v
     * &gt;&lt;&gt;vv&gt;v^v^&lt;&gt;&gt;&lt;&gt;&gt;&gt;&gt;&lt;^^&gt;vv&gt;v&lt;^^^&gt;&gt;v^v^&lt;^^&gt;v^^&gt;v^&lt;^v&gt;v&lt;&gt;&gt;v^v^&lt;v&gt;v^^&lt;^^vv&lt;
     * &lt;&lt;v&lt;^&gt;&gt;^^^^&gt;&gt;&gt;v^&lt;&gt;vvv^&gt;&lt;v&lt;&lt;&lt;&gt;^^^vv^&lt;vvv&gt;^&gt;v&lt;^^^^v&lt;&gt;^&gt;vvvv&gt;&lt;&gt;&gt;v^&lt;&lt;^^^^^
     * ^&gt;&lt;^&gt;&lt;&gt;&gt;&gt;&lt;&gt;^^&lt;&lt;^^v&gt;&gt;&gt;&lt;^&lt;v&gt;^&lt;vv&gt;&gt;v&gt;&gt;&gt;^v&gt;&lt;&gt;^v&gt;&lt;&lt;&lt;&lt;v&gt;&gt;v&lt;v&lt;v&gt;vvv&gt;^&lt;&gt;&lt;&lt;&gt;^&gt;&lt;
     * ^&gt;&gt;&lt;&gt;^v&lt;&gt;&lt;^vvv&lt;^^&lt;&gt;&lt;v&lt;&lt;&lt;&lt;&lt;&gt;&lt;^v&lt;&lt;&lt;&gt;&lt;&lt;&lt;^^&lt;v&lt;^^^&gt;&lt;^&gt;&gt;^&lt;v^&gt;&lt;&lt;&lt;^&gt;&gt;^v&lt;v^v&lt;v^
     * &gt;^&gt;&gt;^v&gt;vv&gt;^&lt;&lt;^v&lt;&gt;&gt;&lt;&lt;&gt;&lt;&lt;v&lt;&lt;v&gt;&lt;&gt;v&lt;^vv&lt;&lt;&lt;&gt;^^v^&gt;^^&gt;&gt;&gt;&lt;&lt;^v&gt;&gt;v^v&gt;&lt;^^&gt;&gt;^&lt;&gt;vv^
     * &lt;&gt;&lt;^^&gt;^^^&lt;&gt;&lt;vvvvv^v&lt;v&lt;&lt;&gt;^v&lt;v&gt;v&lt;&lt;^&gt;&lt;&lt;&gt;&lt;&lt;&gt;&lt;&lt;&lt;^^&lt;&lt;&lt;^&lt;&lt;&gt;&gt;&lt;&lt;&gt;&lt;^^^&gt;^^&lt;&gt;^&gt;v&lt;&gt;
     * ^^&gt;vv&lt;^v^v&lt;vv&gt;^&lt;&gt;&lt;v&lt;^v&gt;^^^&gt;&gt;&gt;^^vvv^&gt;vvv&lt;&gt;&gt;&gt;^&lt;^&gt;&gt;&gt;&gt;&gt;^&lt;&lt;^v&gt;^vvv&lt;&gt;^&lt;&gt;&lt;&lt;v&gt;
     * v^^&gt;&gt;&gt;&lt;&lt;^^&lt;&gt;&gt;^v^&lt;v^vv&lt;&gt;v^&lt;&lt;&gt;^&lt;^v^v&gt;&lt;^&lt;&lt;&lt;&gt;&lt;&lt;^&lt;v&gt;&lt;v&lt;&gt;vv&gt;&gt;v&gt;&lt;v^&lt;vv&lt;&gt;v^&lt;&lt;^
     * </pre>
     * <p>
     * As the robot (<code>@</code>) attempts to move, if there are any boxes (<code>O</code>) in the way, the robot will also attempt to push those boxes.
     * However, if this action would cause the robot or a box to move into a wall (<code>#</code>), noobject moves instead, including the robot.
     * The initial positions of these are shown on the map at the top of the document the lanternfish gave you.
     * <p>
     * The rest of the document describes the <em>moves</em> (<code>^</code> for up, <code>v</code> for down, <code>&lt;</code> for left, <code>&gt;</code> for right) that the robot will attempt to make, in order.
     * (The moves form a single giant sequence; they are broken into multiple lines just to make copy-pasting easier.
     * Newlines within the move sequence should be ignored.)
     * <p>
     * Here is a smaller example to get started:
     * <pre>
     * ########
     * #..O.O.#
     * ##@.O..#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #......#
     * ########
     *
     * &lt;^^&gt;&gt;&gt;vv&lt;v&gt;&gt;v&lt;&lt;
     * </pre>
     * <p>
     * Were the robot to attempt the given sequence of moves, it would push around the boxes as follows:
     * <pre>
     * Initial state:
     * ########
     * #..O.O.#
     * ##<em>@</em>.O..#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #......#
     * ########
     *
     * Move &lt;:
     * ########
     * #..O.O.#
     * ##<em>@</em>.O..#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #......#
     * ########
     *
     * Move ^:
     * ########
     * #.<em>@</em>O.O.#
     * ##..O..#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #......#
     * ########
     *
     * Move ^:
     * ########
     * #.<em>@</em>O.O.#
     * ##..O..#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #......#
     * ########
     *
     * Move &gt;:
     * ########
     * #..<em>@</em>OO.#
     * ##..O..#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #......#
     * ########
     *
     * Move &gt;:
     * ########
     * #...<em>@</em>OO#
     * ##..O..#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #......#
     * ########
     *
     * Move &gt;:
     * ########
     * #...<em>@</em>OO#
     * ##..O..#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #......#
     * ########
     *
     * Move v:
     * ########
     * #....OO#
     * ##..<em>@</em>..#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #...O..#
     * ########
     *
     * Move v:
     * ########
     * #....OO#
     * ##..<em>@</em>..#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #...O..#
     * ########
     *
     * Move &lt;:
     * ########
     * #....OO#
     * ##.<em>@</em>...#
     * #...O..#
     * #.#.O..#
     * #...O..#
     * #...O..#
     * ########
     *
     * Move v:
     * ########
     * #....OO#
     * ##.....#
     * #..<em>@</em>O..#
     * #.#.O..#
     * #...O..#
     * #...O..#
     * ########
     *
     * Move &gt;:
     * ########
     * #....OO#
     * ##.....#
     * #...<em>@</em>O.#
     * #.#.O..#
     * #...O..#
     * #...O..#
     * ########
     *
     * Move &gt;:
     * ########
     * #....OO#
     * ##.....#
     * #....<em>@</em>O#
     * #.#.O..#
     * #...O..#
     * #...O..#
     * ########
     *
     * Move v:
     * ########
     * #....OO#
     * ##.....#
     * #.....O#
     * #.#.O<em>@</em>.#
     * #...O..#
     * #...O..#
     * ########
     *
     * Move &lt;:
     * ########
     * #....OO#
     * ##.....#
     * #.....O#
     * #.#O<em>@</em>..#
     * #...O..#
     * #...O..#
     * ########
     *
     * Move &lt;:
     * ########
     * #....OO#
     * ##.....#
     * #.....O#
     * #.#O<em>@</em>..#
     * #...O..#
     * #...O..#
     * ########
     * </pre>
     * <p>
     * The larger example has many more moves; after the robot has finished those moves, the warehouse would look like this:
     * <pre>
     * ##########
     * #.O.O.OOO#
     * #........#
     * #OO......#
     * #OO<em>@</em>.....#
     * #O#.....O#
     * #O.....OO#
     * #O.....OO#
     * #OO....OO#
     * ##########
     * </pre>
     * <p>
     * The lanternfish use their own custom Goods Positioning System (GPS for short) to track the locations of the boxes.
     * The <em>GPS coordinate</em> of a box is equal to 100 times its distance from the top edge of the map plus its distance from the left edge of the map.
     * (This process does not stop at wall tiles; measure all the way to the edges of the map.)
     * <p>
     * So, the box shown below has a distance of <code>1</code> from the top edge of the map and <code>4</code> from the left edge of the map, resulting in a GPS coordinate of <code>100 * 1 + 4 = 104</code>.
     * <pre>
     * #######
     * #...O..
     * #......
     * </pre>
     * <p>
     * The lanternfish would like to know the <em>sum of all boxes' GPS coordinates</em> after the robot finishes moving.
     * In the larger example, the sum of all boxes' GPS coordinates is <code><em>10092</em></code>.
     * In the smaller example, the sum is <code><em>2028</em></code>.
     * <p>
     * Predict the motion of the robot and boxes in the warehouse.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return After the robot is finished moving, what is the sum of all boxes' GPS coordinates?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final List<List<String>> batches = context.readBatches();

        final Map<Coordinate, Character> coordinates = findObjects(batches.getFirst());
        final String instructions = String.join("", batches.getLast());

        traceRobotPath(instructions, coordinates);

        return calculateGpsCoordinateSum(coordinates);
    }

    /**
     * The lanternfish use your information to find a safe moment to swim in and turn off the malfunctioning robot!
     * Just as they start preparing a festival in your honor, reports start coming in that a <em>second</em> warehouse's robot is <em>also</em> malfunctioning.
     * <p>
     * This warehouse's layout is surprisingly similar to the one you just helped.
     * There is one key difference: everyobject except the robot is <em>twice as wide</em>!
     * The robot's list of movements doesn't change.
     * <p>
     * To get the wider warehouse's map, start with your original map and, for each tile, make the following changes:
     * <ul>
     * <li>If the tile is <code>#</code>, the new map contains <code>##</code> instead.</li>
     * <li>If the tile is <code>O</code>, the new map contains <code>[]</code> instead.</li>
     * <li>If the tile is <code>.</code>, the new map contains <code>..</code> instead.</li>
     * <li>If the tile is <code>@</code>, the new map contains <code>@.</code> instead.</li>
     * </ul>
     * <p>
     * This will produce a new warehouse map which is twice as wide and with wide boxes that are represented by <code>[]</code>.
     * (The robot does not change size.)
     * <p>
     * The larger example from before would now look like this:
     * <pre>
     * ####################
     * ##....[]....[]..[]##
     * ##............[]..##
     * ##..[][]....[]..[]##
     * ##....[]@.....[]..##
     * ##[]##....[]......##
     * ##[]....[]....[]..##
     * ##..[][]..[]..[][]##
     * ##........[]......##
     * ####################
     * </pre>
     * <p>
     * Because boxes are now twice as wide but the robot is still the same size and speed, boxes can be aligned such that they directly push two other boxes at once.
     * For example, consider this situation:
     * <pre>
     * #######
     * #...#.#
     * #.....#
     * #..OO@#
     * #..O..#
     * #.....#
     * #######
     *
     * &lt;vv&lt;&lt;^^&lt;&lt;^^
     * </pre>
     * <p>
     * After appropriately resizing this map, the robot would push around these boxes as follows:
     * <pre>
     * Initial state:
     * ##############
     * ##......##..##
     * ##..........##
     * ##....[][]<em>@</em>.##
     * ##....[]....##
     * ##..........##
     * ##############
     *
     * Move &lt;:
     * ##############
     * ##......##..##
     * ##..........##
     * ##...[][]<em>@</em>..##
     * ##....[]....##
     * ##..........##
     * ##############
     *
     * Move v:
     * ##############
     * ##......##..##
     * ##..........##
     * ##...[][]...##
     * ##....[].<em>@</em>..##
     * ##..........##
     * ##############
     *
     * Move v:
     * ##############
     * ##......##..##
     * ##..........##
     * ##...[][]...##
     * ##....[]....##
     * ##.......<em>@</em>..##
     * ##############
     *
     * Move &lt;:
     * ##############
     * ##......##..##
     * ##..........##
     * ##...[][]...##
     * ##....[]....##
     * ##......<em>@</em>...##
     * ##############
     *
     * Move &lt;:
     * ##############
     * ##......##..##
     * ##..........##
     * ##...[][]...##
     * ##....[]....##
     * ##.....<em>@</em>....##
     * ##############
     *
     * Move ^:
     * ##############
     * ##......##..##
     * ##...[][]...##
     * ##....[]....##
     * ##.....<em>@</em>....##
     * ##..........##
     * ##############
     *
     * Move ^:
     * ##############
     * ##......##..##
     * ##...[][]...##
     * ##....[]....##
     * ##.....<em>@</em>....##
     * ##..........##
     * ##############
     *
     * Move &lt;:
     * ##############
     * ##......##..##
     * ##...[][]...##
     * ##....[]....##
     * ##....<em>@</em>.....##
     * ##..........##
     * ##############
     *
     * Move &lt;:
     * ##############
     * ##......##..##
     * ##...[][]...##
     * ##....[]....##
     * ##...<em>@</em>......##
     * ##..........##
     * ##############
     *
     * Move ^:
     * ##############
     * ##......##..##
     * ##...[][]...##
     * ##...<em>@</em>[]....##
     * ##..........##
     * ##..........##
     * ##############
     *
     * Move ^:
     * ##############
     * ##...[].##..##
     * ##...<em>@</em>.[]...##
     * ##....[]....##
     * ##..........##
     * ##..........##
     * ##############
     * </pre>
     * <p>
     * This warehouse also uses GPS to locate the boxes.
     * For these larger boxes, distances are measured from the edge of the map to the closest edge of the box in question.
     * So, the box shown below has a distance of <code>1</code> from the top edge of the map and <code>5</code> from the left edge of the map, resulting in a GPS coordinate of <code>100 * 1 + 5 = 105</code>.
     * <pre>
     * ##########
     * ##...[]...
     * ##........
     * </pre>
     * <p>
     * In the scaled-up version of the larger example from above, after the robot has finished all of its moves, the warehouse would look like this:
     * <pre>
     * ####################
     * ##[].......[].[][]##
     * ##[]...........[].##
     * ##[]........[][][]##
     * ##[]......[]....[]##
     * ##..##......[]....##
     * ##..[]............##
     * ##..<em>@</em>......[].[][]##
     * ##......[][]..[]..##
     * ####################
     * </pre>
     * <p>
     * The sum of these boxes' GPS coordinates is <code><em>9021</em></code>.
     * <p>
     * Predict the motion of the robot and boxes in this new, scaled-up warehouse.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of all boxes' final GPS coordinates?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final List<List<String>> batches = context.readBatches();

        final Map<Coordinate, Character> smolCoordinates = findObjects(batches.getFirst());
        final String instructions = String.join("", batches.getLast());

        final Map<Coordinate, Character> coordinates = smolCoordinates.entrySet().stream()
                .flatMap(e -> {
                    final Coordinate c = e.getKey();
                    final Character object = e.getValue();
                    assert object != null;

                    if (object == '@')
                        return Stream.of(Map.entry(new Coordinate(c.x() * 2, c.y()), '@'));

                    if (object == '#')
                        return Stream.of(
                                Map.entry(new Coordinate(c.x() * 2, c.y()), '#'),
                                Map.entry(new Coordinate(c.x() * 2 + 1, c.y()), '#')
                        );

                    if (object == 'O')
                        return Stream.of(
                                Map.entry(new Coordinate(c.x() * 2, c.y()), '['),
                                Map.entry(new Coordinate(c.x() * 2 + 1, c.y()), ']')
                        );

                    throw new IllegalStateException("Invalid object to expand: " + object);
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        traceRobotPath(instructions, coordinates);

        return calculateGpsCoordinateSum(coordinates);
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final List<List<String>> batches = context.readBatches();

        final Map<Coordinate, Character> smolCoordinates = findObjects(batches.getFirst());
        final String instructions = String.join("", batches.getLast());

        final Map<Coordinate, Character> coordinates = smolCoordinates.entrySet().stream()
                .flatMap(e -> {
                    final Coordinate c = e.getKey();
                    final Character object = e.getValue();
                    assert object != null;

                    if (object == '@')
                        return Stream.of(Map.entry(new Coordinate(c.x() * 2, c.y()), '@'));

                    if (object == '#')
                        return Stream.of(
                                Map.entry(new Coordinate(c.x() * 2, c.y()), '#'),
                                Map.entry(new Coordinate(c.x() * 2 + 1, c.y()), '#')
                        );

                    if (object == 'O')
                        return Stream.of(
                                Map.entry(new Coordinate(c.x() * 2, c.y()), '['),
                                Map.entry(new Coordinate(c.x() * 2 + 1, c.y()), ']')
                        );

                    throw new IllegalStateException("Invalid object to expand: " + object);
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        traceRobotPath(instructions, smolCoordinates);
        traceRobotPath(instructions, coordinates);

        return new IntTuple(
                calculateGpsCoordinateSum(smolCoordinates),
                calculateGpsCoordinateSum(coordinates)
        );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Find all the `Character`s.
     */
    private static Map<Coordinate, Character> findObjects(final List<String> batches) {
        final Map<Coordinate, Character> objects = new HashMap<>();
        for (int y = 0; y < batches.size(); y++) {
            final String line = batches.get(y);
            for (int x = 0; x < line.length(); x++) {
                final char sym = line.charAt(x);
                if (sym == '.') continue;

                final Coordinate coordinate = new Coordinate(x, y);
                objects.put(coordinate, sym);
            }
        }
        return objects;
    }

    /*
     * Find the starting position of the `Robot` in the warehouse.
     */
    private static Coordinate findRobot(final Map<Coordinate, Character> coordinates) {
        for (final Map.Entry<Coordinate, Character> entry : coordinates.entrySet())
            if (entry.getValue() == '@')
                return entry.getKey();
        throw new IllegalStateException("Did not find Robot starting position");
    }

    /*
     * Trace the path of a `Robot` through the warehouse.
     */
    @SuppressWarnings("BoxedPrimitiveEquality")
    private static void traceRobotPath(
            final String instructions,
            final Map<Coordinate, Character> coordinates
    ) {
        Coordinate robot = findRobot(coordinates);
        out:
        for (int i = 0; i < instructions.length(); i++) {
            assert coordinates.get(robot) == '@';

            final char instruction = instructions.charAt(i);
            final Direction d = Direction.fromChar(instruction);
            assert d != null;

            final Coordinate nextRobot = robot.adjustBy(d);
            final SequencedSet<Coordinate> nexts = new LinkedHashSet<>(Set.of(nextRobot));
            final SequencedSet<Coordinate> objects = new LinkedHashSet<>();
            while (!nexts.isEmpty()) {
                final Coordinate next = nexts.removeFirst();
                final Character c = coordinates.get(next);
                if (c == null) continue;
                if (c == '#') continue out; // Can't move, give up...

                if (c == 'O') {
                    objects.add(next);
                    nexts.add(next.adjustBy(d));
                    continue;
                }

                if (c == '[') {
                    final Coordinate nextRight = next.adjustBy(Direction.RIGHT);
                    final boolean addedLeft = objects.add(next);
                    final boolean addedRight = objects.add(nextRight);
                    assert addedLeft == addedRight;
                    if (addedLeft) {
                        nexts.add(next.adjustBy(d));
                        nexts.add(nextRight.adjustBy(d));
                    }
                    continue;
                }

                if (c == ']') {
                    final Coordinate nextLeft = next.adjustBy(Direction.LEFT);
                    final boolean addedRight = objects.add(next);
                    final boolean addedLeft = objects.add(nextLeft);
                    assert addedLeft == addedRight;
                    if (addedLeft) {
                        nexts.add(nextLeft.adjustBy(d));
                        nexts.add(next.adjustBy(d));
                    }
                    continue;
                }

                throw new IllegalStateException("Should not get here!");
            }

            objects.reversed().forEach(c -> {
                final Character object = coordinates.remove(c);
                assert object == 'O' || object == '[' || object == ']';

                final Character replaced = coordinates.put(c.adjustBy(d), object);
                assert replaced == null;
            });

            coordinates.remove(robot);
            coordinates.put(nextRobot, '@');
            robot = nextRobot;
        }
    }

    /*
     * Calculate the sum of all Goods Positioning System coordinates.
     */
    private static int calculateGpsCoordinateSum(final Map<Coordinate, Character> coordinates) {
        return coordinates.entrySet().stream()
                .filter(e -> e.getValue() == 'O' || e.getValue() == '[')
                .map(Map.Entry::getKey)
                .mapToInt(c -> 100 * c.y() + c.x())
                .sum();
    }

}

