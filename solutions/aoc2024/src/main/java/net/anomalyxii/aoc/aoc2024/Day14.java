package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Velocity;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 14: Restroom Redoubt.
 */
@Solution(year = 2024, day = 14, title = "Restroom Redoubt")
public class Day14 {

    // ****************************************
    // Private Members
    // ****************************************

    private final int width;
    private final int height;

    // ****************************************
    // Constructors
    // ****************************************

    public Day14() {
        this(101, 103);
    }

    Day14(final int width, final int height) {
        this.width = width;
        this.height = height;
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * One of The Historians needs to use the bathroom; fortunately, you know there's a bathroom near an unvisited location on their list, and so you're all quickly teleported directly to the lobby of Easter Bunny Headquarters.
     * <p>
     * Unfortunately, EBHQ seems to have "improved" bathroom security <em>again</em> after your last <a href="/2016/day/2">visit</a>.
     * The area outside the bathroom is swarming with robots!
     * <p>
     * To get The Historian safely to the bathroom, you'll need a way to predict where the robots will be in the future.
     * Fortunately, they all seem to be moving on the tile floor in predictable <em>straight lines</em>.
     * <p>
     * You make a list (your puzzle input) of all of the robots' current <em>positions</em> (<code>p</code>) and <em>velocities</em> (<code>v</code>), one robot per line.
     * For example:
     * <pre>
     * p=0,4 v=3,-3
     * p=6,3 v=-1,-3
     * p=10,3 v=-1,2
     * p=2,0 v=2,-1
     * p=0,0 v=1,3
     * p=3,0 v=-2,-2
     * p=7,6 v=-1,-3
     * p=3,0 v=-1,-2
     * p=9,3 v=2,3
     * p=7,3 v=-1,2
     * p=2,4 v=2,-3
     * p=9,5 v=-3,-3
     * </pre>
     * <p>
     * Each robot's position is given as <code>p=x,y</code> where <code>x</code> represents the number of tiles the robot is from the left wall and <code>y</code> represents the number of tiles from the top wall (when viewed from above).
     * So, a position of <code>p=0,0</code> means the robot is all the way in the top-left corner.
     * <p>
     * Each robot's velocity is given as <code>v=x,y</code> where <code>x</code> and <code>y</code> are given in <em>tiles per second</em>.
     * Positive <code>x</code> means the robot is moving to the <em>right</em>, and positive <code>y</code> means the robot is moving <em>down</em>.
     * So, a velocity of <code>v=1,-2</code> means that each second, the robot moves <code>1</code> tile to the right and <code>2</code> tiles up.
     * <p>
     * The robots outside the actual bathroom are in a space which is <code>101</code> tiles wide and <code>103</code> tiles tall (when viewed from above).
     * However, in this example, the robots are in a space which is only <code>11</code> tiles wide and <code>7</code> tiles tall.
     * <p>
     * The robots are good at navigating over/under each other (due to a combination of springs, extendable legs, and quadcopters), so they can share the same tile and don't interact with each other.
     * Visually, the number of robots on each tile in this example looks like this:
     * <pre>
     * 1.12.......
     * ...........
     * ...........
     * ......11.11
     * 1.1........
     * .........1.
     * .......1...
     * </pre>
     * <p>
     * These robots have a unique feature for maximum bathroom security: they can <em>teleport</em>.
     * When a robot would run into an edge of the space they're in, they instead <em>teleport to the other side</em>, effectively wrapping around the edges.
     * Here is what robot <code>p=2,4 v=2,-3</code> does for the first few seconds:
     * <pre>
     * Initial state:
     * ...........
     * ...........
     * ...........
     * ...........
     * ..1........
     * ...........
     * ...........
     *
     * After 1 second:
     * ...........
     * ....1......
     * ...........
     * ...........
     * ...........
     * ...........
     * ...........
     *
     * After 2 seconds:
     * ...........
     * ...........
     * ...........
     * ...........
     * ...........
     * ......1....
     * ...........
     *
     * After 3 seconds:
     * ...........
     * ...........
     * ........1..
     * ...........
     * ...........
     * ...........
     * ...........
     *
     * After 4 seconds:
     * ...........
     * ...........
     * ...........
     * ...........
     * ...........
     * ...........
     * ..........1
     *
     * After 5 seconds:
     * ...........
     * ...........
     * ...........
     * .1.........
     * ...........
     * ...........
     * ...........
     * </pre>
     * <p>
     * The Historian can't wait much longer, so you don't have to simulate the robots for very long.
     * Where will the robots be after <code>100</code> seconds?
     * <p>
     * In the above example, the number of robots on each tile after 100 seconds has elapsed looks like this:
     * <pre>
     * ......2..1.
     * ...........
     * 1..........
     * .11........
     * .....1.....
     * ...12......
     * .1....1....
     * </pre>
     * <p>
     * To determine the safest area, count the <em>number of robots in each quadrant</em> after 100 seconds.
     * Robots that are exactly in the middle (horizontally or vertically) don't count as being in any quadrant, so the only relevant robots are:
     * <pre>
     * .....
     * 2..1.
     * .....
     * .....
     * 1....
     * .....
     *
     * .....
     * .....
     * ...12 .....
     * .1...
     * 1....
     * </pre>
     * <p>
     * In this example, the quadrants contain <code>1</code>, <code>3</code>, <code>4</code>, and <code>1</code> robot.
     * Multiplying these together gives a total <em>safety factor</em> of <code><em>12</em></code>.
     * <p>
     * Predict the motion of the robots in your list within a space which is <code>101</code> tiles wide and <code>103</code> tiles tall.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What will the safety factor be after exactly 100 seconds have elapsed?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final List<Robot> robots = context.stream()
                .map(Robot::parse)
                .toList();

        final List<Coordinate> finalPositions = robots.stream()
                .map(robot -> robot.calculatePositionAfter(100, width, height))
                .toList();

        return calculateSafetyFactor(finalPositions);
    }

    /**
     * During the bathroom break, someone notices that these robots seem awfully similar to ones built and used at the North Pole.
     * If they're the same type of robots, they should have a hard-coded <span title="This puzzle was originally going to be about the motion of space rocks in a fictitious arcade game called Meteoroids, but we just had an arcade puzzle.">Easter egg</span>: very rarely, most of the robots should arrange themselves into <em>a picture of a Christmas tree</em>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the fewest number of seconds that must elapse for the robots to display the Easter egg?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final List<Robot> robots = context.stream()
                .map(Robot::parse)
                .toList();

        return findEasterEgg(robots);
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final List<Robot> robots = context.stream()
                .map(Robot::parse)
                .toList();

        final List<Coordinate> nextPositions = robots.stream()
                .map(robot -> robot.calculatePositionAfter(100, width, height))
                .toList();

        final int safetyFactor = calculateSafetyFactor(nextPositions);

        return new IntTuple(
                safetyFactor,
                findEasterEgg(robots)
        );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Calculate the safety factor for the current position of robots.
     */
    private int calculateSafetyFactor(final Collection<Coordinate> nextPositions) {
        int q1 = 0;
        int q2 = 0;
        int q3 = 0;
        int q4 = 0;

        final int midx = width / 2;
        final int midy = height / 2;

        for (final Coordinate c : nextPositions) {
            if (c.x() < midx && c.y() < midy) ++q1;
            else if (c.x() > midx && c.y() < midy) ++q2;
            else if (c.x() < midx && c.y() > midy) ++q3;
            else if (c.x() > midx && c.y() > midy) ++q4;
        }

        return q1 * q2 * q3 * q4;
    }

    /*
     * Attempt to find the number of iterations before the robots form into
     * the Easter Egg configuration.
     */
    private int findEasterEgg(final List<Robot> robots) {
        for (int i = 101; i < 10_000; i++) {
            final int pos = i;
            final Set<Coordinate> newPositions = robots.stream()
                    .map(robot -> robot.calculatePositionAfter(pos, width, height))
                    .collect(Collectors.toSet());

            if (isReallyLazyAttemptToFindEasterEgg(newPositions))
                return i;
        }

        throw new IllegalStateException("Did not find a Christmas Tree after 10,000 iterations");
    }

    /*
     * Make a wild guess at whether the robots formed a tree by looking at
     * whether any of them assemble into a 5x5 corner...
     */
    private boolean isReallyLazyAttemptToFindEasterEgg(final Set<Coordinate> coordinates) {
        // Assume a high safety factor means that the robots are not clustered enough to make a sensible image
        if (calculateSafetyFactor(coordinates) > 150_000_000) return false;

        // For potential candidates, check if we can find a border.
        // If so, it's _probably_ the correct thing?
        return coordinates.stream()
                .anyMatch(c -> {
                    for (int x = 0; x < 5; x++)
                        if (!coordinates.contains(c.adjustBy(x, 0)))
                            return false;

                    for (int y = 0; y < 5; y++)
                        if (!coordinates.contains(c.adjustBy(0, y)))
                            return false;

                    return true;
                });
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A `Robot`, with a starting position and a velocity.
     */
    private record Robot(Coordinate start, Velocity velocity) {

        // Helper Methods

        /*
         * Calculate the position of this robot after a certain amount of time
         * has elapsed, given an area of a certain size.
         */
        Coordinate calculatePositionAfter(
                final int time,
                final int width,
                final int height
        ) {
            final Coordinate end = start.adjustBy(
                    velocity.h() * time,
                    velocity.v() * time
            );

            final int endX = end.x() % width;
            final int endY = end.y() % height;
            return new Coordinate(
                    endX >= 0 ? endX : endX + width,
                    endY >= 0 ? endY : endY + height
            );
        }

        // Static Helper Methods

        /*
         * Parse the `Robot` from a line of input.
         */
        static Robot parse(final String line) {
            final String[] parts = line.split("\\s+");

            return new Robot(
                    Coordinate.parse(parts[0].split("=")[1]),
                    Velocity.parse(parts[1].split("=")[1])
            );
        }

    }

}

