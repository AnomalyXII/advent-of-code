package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.NoChallenge;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.Arrays;
import java.util.List;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 25: Code Chronicle.
 */
@Solution(year = 2024, day = 25, title = "Code Chronicle")
public class Day25 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Out of ideas and time, The Historians agree that they should go back to check the <em>Chief Historian's office</em> one last time, just in case he went back there without you noticing.
     * <p>
     * When you get there, you are surprised to discover that the door to his office is <em>locked</em>!
     * You can hear someone inside, but knocking <span title="function knock() {&#10;  yield no_response;&#10;}">yields</span> no response.
     * The locks on this floor are all fancy, expensive, virtual versions of <a href="https://en.wikipedia.org/wiki/Pin_tumbler_lock" target="_blank">five-pin tumbler locks</a>, so you contact North Pole security to see if they can help open the door.
     * <p>
     * Unfortunately, they've lost track of which locks are installed and which keys go with them, so the best they can do is send over <em>schematics of every lock and every key</em> for the floor you're on (your puzzle input).
     * <p>
     * The schematics are in a cryptic file format, but they do contain manufacturer information, so you look up their support number.
     * <p>
     * "Our Virtual Five-Pin Tumbler product?
     * That's our most expensive model!
     * <em>Way</em> more secure than--"
     * You explain that you need to open a door and don't have a lot of time.
     * <p>
     * "Well, you can't know whether a key opens a lock without actually trying the key in the lock (due to quantum hidden variables), but you <em>can</em> rule out some of the key/lock combinations."
     * <p>
     * "The virtual system is complicated, but part of it really is a crude simulation of a five-pin tumbler lock, mostly for marketing reasons.
     * If you look at the schematics, you can figure out whether a key could possibly fit in a lock."
     * <p>
     * He transmits you some example schematics:
     * <pre>
     * #####
     * .####
     * .####
     * .####
     * .#.#.
     * .#...
     * .....
     *
     * #####
     * ##.##
     * .#.##
     * ...##
     * ...#.
     * ...#.
     * .....
     *
     * .....
     * #....
     * #....
     * #...#
     * #.#.#
     * #.###
     * #####
     *
     * .....
     * .....
     * #.#..
     * ###..
     * ###.#
     * ###.#
     * #####
     *
     * .....
     * .....
     * .....
     * #....
     * #.#..
     * #.#.#
     * #####
     * </pre>
     * <p>
     * "The locks are schematics that have the top row filled (<code>#</code>) and the bottom row empty (<code>.</code>); the keys have the top row empty and the bottom row filled.
     * If you look closely, you'll see that each schematic is actually a set of columns of various heights, either extending downward from the top (for locks) or upward from the bottom (for keys)."
     * <p>
     * "For locks, those are the pins themselves; you can convert the pins in schematics to a list of heights, one per column.
     * For keys, the columns make up the shape of the key where it aligns with pins; those can also be converted to a list of heights."
     * <p>
     * "So, you could say the first lock has pin heights <code>0,5,3,4,3</code>:"
     * <pre>
     * #####
     * .####
     * .####
     * .####
     * .#.#.
     * .#...
     * .....
     * </pre>
     * <p>
     * "Or, that the first key has heights <code>5,0,2,1,3</code>:"
     * <pre>
     * .....
     * #....
     * #....
     * #...#
     * #.#.#
     * #.###
     * #####
     * </pre>
     * <p>
     * "These seem like they should fit together; in the first four columns, the pins and key don't overlap.
     * However, this key <em>cannot</em> be for this lock: in the rightmost column, the lock's pin overlaps with the key, which you know because in that column the sum of the lock height and key height is more than the available space."
     * <p>
     * "So anyway, you can narrow down the keys you'd need to try by just testing each key with each lock, which means you would have to check... wait, you have <em>how</em> many locks?
     * But the only installation <em>that</em> size is at the North--"
     * You disconnect the call.
     * <p>
     * In this example, converting both locks to pin heights produces:
     * <pre>
     * 0,5,3,4,3
     * 1,2,0,5,3
     * </pre>
     * <p>
     * Converting all three keys to heights produces:
     * <pre>
     * 5,0,2,1,3
     * 4,3,4,0,2
     * 3,0,2,0,1
     * </pre>
     * <p>
     * Then, you can try every key with every lock:
     * <ul>
     * <li>Lock <code>0,5,3,4,3</code> and key <code>5,0,2,1,3</code>: <em>overlap</em> in the last column.</li>
     * <li>Lock <code>0,5,3,4,3</code> and key <code>4,3,4,0,2</code>: <em>overlap</em> in the second column.</li>
     * <li>Lock <code>0,5,3,4,3</code> and key <code>3,0,2,0,1</code>: all columns <em>fit</em>!</li>
     * <li>Lock <code>1,2,0,5,3</code> and key <code>5,0,2,1,3</code>: <em>overlap</em> in the first column.</li>
     * <li>Lock <code>1,2,0,5,3</code> and key <code>4,3,4,0,2</code>: all columns <em>fit</em>!</li>
     * <li>Lock <code>1,2,0,5,3</code> and key <code>3,0,2,0,1</code>: all columns <em>fit</em>!</li>
     * </ul>
     * <p>
     * So, in this example, the number of unique lock/key pairs that fit together without overlapping in any column is <code><em>3</em></code>.
     * <p>
     * Analyze your lock and key schematics.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many unique lock/key pairs fit together without overlapping in any column?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final int[][] locksAndKeys = parseSchematics(context);
        final int[] locks = locksAndKeys[0];
        final int[] keys = locksAndKeys[1];

        int matches = 0;
        for (final int lock : locks) {
            for (final int key : keys) {
                if (areCompatible(lock, key))
                    ++matches;
            }
        }

        return matches;
    }

    /**
     * You and The Historians crowd into the office, startling the Chief Historian awake!
     * The Historians all take turns looking confused until one asks where he's been for the last few months.
     * <p>
     * "I've been right here, working on this high-priority request from Santa!
     * I think the only time I even stepped away was about a month ago when I went to grab a cup of coffee..."
     * <p>
     * Just then, the Chief notices the time.
     * "Oh no!
     * I'm going to be late!
     * I must have fallen asleep trying to put the finishing touches on this <em>chronicle</em> Santa requested, but now I don't have enough time to go visit the last 50 places on my list and complete the chronicle before Santa leaves!
     * He said he needed it before tonight's sleigh launch."
     * <p>
     * One of The Historians holds up the list they've been using this whole time to keep track of where they've been searching.
     * Next to each place you all visited, they checked off that place with a <em class="star">star</em>.
     * Other Historians hold up their own notes they took on the journey; as The Historians, how could they resist writing everything down while visiting all those historically significant places?
     * <p>
     * The Chief's eyes get wide.
     * "With all this, we might just have enough time to finish the chronicle!
     * Santa said he wanted it wrapped up with a bow, so I'll call down to the wrapping department and... hey, could <em>you</em> bring it up to Santa?
     * I'll need to be in my seat to watch the sleigh launch by then."
     * <p>
     * You nod, and The Historians quickly work to collect their notes into the final set of pages for the chronicle.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return -
     */
    @Part(part = II)
    public NoChallenge calculateAnswerForPart2(final SolutionContext context) {
        return NoChallenge.NO_CHALLENGE;
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Process all the schematics.
     */
    private static int[][] parseSchematics(final SolutionContext context) {
        int numLocks = 0;
        int numKeys = 0;
        final int[] locks = new int[512];
        final int[] keys = new int[512];
        final List<List<String>> batches = context.readBatches();
        for (final List<String> batch : batches) {
            if (batch.getFirst().matches("^#+$")) locks[numLocks++] = processLockSchematic(batch);
            else if (batch.getLast().matches("^#+$")) keys[numKeys++] = parseKeySchematic(batch);
            else throw new IllegalStateException("Invalid schematic - was neither a lock nor a key");
        }

        return new int[][]{
                Arrays.copyOfRange(locks, 0, numLocks),
                Arrays.copyOfRange(keys, 0, numKeys)
        };
    }

    /*
     * Parse a lock schematic.
     */
    private static int processLockSchematic(final List<String> batch) {
        int lock = 0;
        for (int i = 1; i < batch.size(); i++) {
            for (int j = 0; j < batch.getFirst().length(); j++) {
                final char p = batch.get(i).charAt(j);
                if (p == '#') lock += (1 << (3 * j));
            }
        }
        return lock;
    }

    /*
     * Parse a key schematic.
     */
    private static int parseKeySchematic(final List<String> batch) {
        int key = 0;
        for (int i = 0; i < batch.size() - 1; i++) {
            for (int j = 0; j < batch.getLast().length(); j++) {
                final char p = batch.get(i).charAt(j);
                if (p == '#') key += (1 << (3 * j));
            }
        }
        return key;
    }

    /*
     * Check if the given lock and key are compatible.
     */
    private static boolean areCompatible(final int lock, final int key) {
        for (int i = 0; i < 5; i++) {
            final int lockPin = 0x07 & (lock >> (3 * i));
            final int keyPin = 0x07 & (key >> (3 * i));
            if (lockPin + keyPin >= 6)
                return false;
        }
        return true;
    }

}

