package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.ObjectTuple;
import net.anomalyxii.aoc.utils.algorithms.Dijkstra;
import net.anomalyxii.aoc.utils.algorithms.ShortestPath;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 18: RAM Run.
 */
@Solution(year = 2024, day = 18, title = "RAM Run")
public class Day18 {

    private static final ShortestPath SHORTEST_PATH = new Dijkstra();

    // ****************************************
    // Private Members
    // ****************************************

    private final int width;
    private final int height;
    private final int t;

    // ****************************************
    // Constructors
    // ****************************************

    public Day18() {
        this(70, 70, 1024);
    }

    Day18(final int width, final int height, final int t) {
        this.width = width;
        this.height = height;
        this.t = t;
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You and The Historians look a lot more pixelated than you remember.
     * You're <a href="/2017/day/2">inside a computer</a> at the North Pole!
     * <p>
     * Just as you're about to check out your surroundings, a program runs up to you.
     * "This region of memory isn't safe!
     * The User misunderstood what a <a href="https://en.wikipedia.org/wiki/Pushdown_automaton" target="_blank">pushdown automaton</a> is and their algorithm is pushing whole <em>bytes</em> down on top of us!
     * <span title="Pun intended.">Run</span>!"
     * <p>
     * The algorithm is fast - it's going to cause a byte to fall into your memory space once every <a href="https://www.youtube.com/watch?v=9eyFDBPk4Yw" target="_blank">nanosecond</a>!
     * Fortunately, you're <em>faster</em>, and by quickly scanning the algorithm, you create a <em>list of which bytes will fall</em> (your puzzle input) in the order they'll land in your memory space.
     * <p>
     * Your memory space is a two-dimensional grid with coordinates that range from <code>0</code> to <code>70</code> both horizontally and vertically.
     * However, for the sake of example, suppose you're on a smaller grid with coordinates that range from <code>0</code> to <code>6</code> and the following list of incoming byte positions:
     * <pre>
     * 5,4
     * 4,2
     * 4,5
     * 3,0
     * 2,1
     * 6,3
     * 2,4
     * 1,5
     * 0,6
     * 3,3
     * 2,6
     * 5,1
     * 1,2
     * 5,5
     * 2,5
     * 6,5
     * 1,4
     * 0,4
     * 6,4
     * 1,1
     * 6,1
     * 1,0
     * 0,5
     * 1,6
     * 2,0
     * </pre>
     * <p>
     * Each byte position is given as an <code>X,Y</code> coordinate, where <code>X</code> is the distance from the left edge of your memory space and <code>Y</code> is the distance from the top edge of your memory space.
     * <p>
     * You and The Historians are currently in the top left corner of the memory space (at <code>0,0</code>) and need to reach the exit in the bottom right corner (at <code>70,70</code> in your memory space, but at <code>6,6</code> in this example).
     * You'll need to simulate the falling bytes to plan out where it will be safe to run; for now, simulate just the first few bytes falling into your memory space.
     * <p>
     * As bytes fall into your memory space, they make that coordinate <em>corrupted</em>.
     * Corrupted memory coordinates cannot be entered by you or The Historians, so you'll need to plan your route carefully.
     * You also cannot leave the boundaries of the memory space; your only hope is to reach the exit.
     * <p>
     * In the above example, if you were to draw the memory space after the first <code>12</code> bytes have fallen (using <code>.</code> for safe and <code>#</code> for corrupted), it would look like this:
     * <pre>
     * ...#...
     * ..#..#.
     * ....#..
     * ...#..#
     * ..#..#.
     * .#..#..
     * #.#....
     * </pre>
     * <p>
     * You can take steps up, down, left, or right.
     * After just 12 bytes have corrupted locations in your memory space, the shortest path from the top left corner to the exit would take <code><em>22</em></code> steps.
     * Here (marked with <code>O</code>) is one such path:
     * <pre>
     * <em>O</em><em>O</em>.#<em>O</em><em>O</em><em>O</em>
     * .<em>O</em>#<em>O</em><em>O</em>#<em>O</em>
     * .<em>O</em><em>O</em><em>O</em>#<em>O</em><em>O</em>
     * ...#<em>O</em><em>O</em>#
     * ..#<em>O</em><em>O</em>#.
     * .#.<em>O</em>#..
     * #.#<em>O</em><em>O</em><em>O</em><em>O</em>
     * </pre>
     * <p>
     * Simulate the first kilobyte (<code>1024</code> bytes) falling onto your memory space.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Afterward, what is the minimum number of steps needed to reach the exit?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final List<Coordinate> corruptions = context.stream()
                .map(Coordinate::parse)
                .toList();

        return solve(corruptions, t);
    }

    /**
     * he Historians aren't as used to moving around in this pixelated universe as you are.
     * You're afraid they're not going to be fast enough to make it to the exit before the path is completely blocked.
     * <p>
     * To determine how fast everyone needs to go, you need to determine <em>the first byte that will cut off the path to the exit</em>.
     * <p>
     * In the above example, after the byte at <code>1,1</code> falls, there is still a path to the exit:
     * <pre>
     * <em>O</em>..#<em>O</em><em>O</em><em>O</em>
     * <em>O</em>##<em>O</em><em>O</em>#<em>O</em>
     * <em>O</em>#<em>O</em><em>O</em>#<em>O</em><em>O</em>
     * <em>O</em><em>O</em><em>O</em>#<em>O</em><em>O</em>#
     * ###<em>O</em><em>O</em>##
     * .##<em>O</em>###
     * #.#<em>O</em><em>O</em><em>O</em><em>O</em>
     * </pre>
     * <p>
     * However, after adding the very next byte (at <code>6,1</code>), there is no longer a path to the exit:
     * <pre>
     * ...#...
     * .##..#<em>#</em>
     * .#..#..
     * ...#..#
     * ###..##
     * .##.###
     * #.#....
     * </pre>
     * <p>
     * So, in this example, the coordinates of the first byte that prevents the exit from being reachable are <code><em>6,1</em></code>.
     * <p>
     * Simulate more of the bytes that are about to corrupt your memory space.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What are the coordinates of the first byte that will prevent the exit from being reachable from your starting position?
     */
    @Part(part = II)
    public String calculateAnswerForPart2(final SolutionContext context) {
        final List<Coordinate> corruptions = context.stream()
                .map(Coordinate::parse)
                .toList();

        final int idx = findCriticalPoint(corruptions, t + 1, corruptions.size());
        final Coordinate blocker = corruptions.get(idx);
        return blocker.x() + "," + blocker.y();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link ObjectTuple} containing the answers for both parts
     */
    @Optimised
    public ObjectTuple<Integer, String> calculateAnswers(final SolutionContext context) {
        final List<Coordinate> corruptions = context.stream()
                .map(Coordinate::parse)
                .toList();

        final int idx = findCriticalPoint(corruptions, t + 1, corruptions.size());
        final Coordinate blocker = corruptions.get(idx);
        return new ObjectTuple<>(
                solve(corruptions, t),
                blocker.x() + "," + blocker.y()
        );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Find the critical point that blocks off all escape routes.
     */
    private int findCriticalPoint(final List<Coordinate> corruptions, final int ts, final int te) {
        if (ts == te) return solve(corruptions, te) < Integer.MAX_VALUE ? te : -1;

        final int diff = te - ts;
        if (diff == 1) return solve(corruptions, te) < Integer.MAX_VALUE ? te : ts;

        final int mid = ts + (diff / 2);
        final int path = solve(corruptions, mid);
        return path == Integer.MAX_VALUE
                ? findCriticalPoint(corruptions, ts, mid)
                : findCriticalPoint(corruptions, mid, te);
    }

    /*
     * Find the shortest path.
     */
    private int solve(final List<Coordinate> corruptions, final int limit) {
        final Set<Coordinate> coordinates = new HashSet<>(corruptions.subList(0, limit));
        final Grid grid = Grid.size(width + 1, height + 1, c -> coordinates.contains(c) ? Integer.MAX_VALUE : 1);
        return (int) SHORTEST_PATH.solve(grid, grid.min(), grid.max());
    }

}

