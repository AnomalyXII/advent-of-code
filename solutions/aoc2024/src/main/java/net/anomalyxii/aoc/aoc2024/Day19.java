package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 19: Linen Layout.
 */
@Solution(year = 2024, day = 19, title = "Linen Layout")
public class Day19 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Today, The Historians take you up to the <a href="/2023/day/12">hot springs</a> on Gear Island!
     * Very <a href="https://www.youtube.com/watch?v=ekL881PJMjI" target="_blank">suspiciously</a>, absolutely nothing goes wrong as they begin their careful search of the vast field of helixes.
     * <p>
     * Could this <em>finally</em> be your chance to visit the <a href="https://en.wikipedia.org/wiki/Onsen" target="_blank">onsen</a> next door?
     * Only one way to find out.
     * <p>
     * After a brief conversation with the reception staff at the onsen front desk, you discover that you don't have the right kind of money to pay the admission fee.
     * However, before you can leave, the staff get your attention.
     * Apparently, they've heard about how you helped at the hot springs, and they're willing to make a deal: if you can simply help them <em>arrange their towels</em>, they'll let you in for <em>free</em>!
     * <p>
     * Every towel at this onsen is marked with a <em>pattern of colored stripes</em>.
     * There are only a few patterns, but for any particular pattern, the staff can get you as many towels with that pattern as you need.
     * Each <span title="It really seems like they've gathered a lot of magic into the towel colors.">stripe</span> can be <em>white</em> (<code>w</code>), <em>blue</em> (<code>u</code>), <em>black</em> (<code>b</code>), <em>red</em> (<code>r</code>), or <em>green</em> (<code>g</code>).
     * So, a towel with the pattern <code>ggr</code> would have a green stripe, a green stripe, and then a red stripe, in that order.
     * (You can't reverse a pattern by flipping a towel upside-down, as that would cause the onsen logo to face the wrong way.)
     * <p>
     * The Official Onsen Branding Expert has produced a list of <em>designs</em> - each a long sequence of stripe colors - that they would like to be able to display.
     * You can use any towels you want, but all of the towels' stripes must exactly match the desired design.
     * So, to display the design <code>rgrgr</code>, you could use two <code>rg</code> towels and then an <code>r</code> towel, an <code>rgr</code> towel and then a <code>gr</code> towel, or even a single massive <code>rgrgr</code> towel (assuming such towel patterns were actually available).
     * <p>
     * To start, collect together all of the available towel patterns and the list of desired designs (your puzzle input).
     * For example:
     * <pre><code>r, wr, b, g, bwu, rb, gb, br
     *
     * brwrr
     * bggr
     * gbbr
     * rrbgbr
     * ubwu
     * bwurrg
     * brgr
     * bbrgwb
     * </code></pre>
     * <p>
     * The first line indicates the available towel patterns; in this example, the onsen has unlimited towels with a single red stripe (<code>r</code>), unlimited towels with a white stripe and then a red stripe (<code>wr</code>), and so on.
     * <p>
     * After the blank line, the remaining lines each describe a design the onsen would like to be able to display.
     * In this example, the first design (<code>brwrr</code>) indicates that the onsen would like to be able to display a black stripe, a red stripe, a white stripe, and then two red stripes, in that order.
     * <p>
     * Not all designs will be possible with the available towels.
     * In the above example, the designs are possible or impossible as follows:
     * <ul>
     * <li><code>brwrr</code> can be made with a <code>br</code> towel, then a <code>wr</code> towel, and then finally an <code>r</code> towel.</li>
     * <li><code>bggr</code> can be made with a <code>b</code> towel, two <code>g</code> towels, and then an <code>r</code> towel.</li>
     * <li><code>gbbr</code> can be made with a <code>gb</code> towel and then a <code>br</code> towel.</li>
     * <li><code>rrbgbr</code> can be made with <code>r</code>, <code>rb</code>, <code>g</code>, and <code>br</code>.</li>
     * <li><code>ubwu</code> is <em>impossible</em>.</li>
     * <li><code>bwurrg</code> can be made with <code>bwu</code>, <code>r</code>, <code>r</code>, and <code>g</code>.</li>
     * <li><code>brgr</code> can be made with <code>br</code>, <code>g</code>, and <code>r</code>.</li>
     * <li><code>bbrgwb</code> is <em>impossible</em>.</li>
     * </ul>
     * <p>
     * In this example, <code><em>6</em></code> of the eight designs are possible with the available towel patterns.
     * <p>
     * To get into the onsen as soon as possible, consult your list of towel patterns and desired designs carefully.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many designs are possible?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<List<String>> batches = context.readBatches();
        final Map<Character, List<char[]>> towels = stream(batches.getFirst().getFirst().split(",\\s*"))
                .map(String::toCharArray)
                .collect(Collectors.groupingBy(c -> c[0]));
        final List<char[]> patterns = batches.getLast().stream()
                .map(String::toCharArray)
                .toList();

        return patterns.stream()
                .mapToLong(pattern -> tallyPossibleArrangements(pattern, towels))
                .mapToInt(count -> count > 0 ? 1 : 0)
                .sum();
    }

    /**
     * The staff don't really like some of the towel arrangements you came up with.
     * To avoid an endless cycle of towel rearrangement, maybe you should just give them every possible option.
     * <p>
     * Here are all of the different ways the above example's designs can be made:
     * <p>
     * <code>brwrr</code> can be made in two different ways: <code>b</code>, <code>r</code>, <code>wr</code>, <code>r</code> <em>or</em> <code>br</code>, <code>wr</code>, <code>r</code>.
     * <p>
     * <code>bggr</code> can only be made with <code>b</code>, <code>g</code>, <code>g</code>, and <code>r</code>.
     * <p>
     * <code>gbbr</code> can be made 4 different ways:
     * <ul>
     * <li><code>g</code>, <code>b</code>, <code>b</code>, <code>r</code></li>
     * <li><code>g</code>, <code>b</code>, <code>br</code></li>
     * <li><code>gb</code>, <code>b</code>, <code>r</code></li>
     * <li><code>gb</code>, <code>br</code></li>
     * </ul>
     * <p>
     * <code>rrbgbr</code> can be made 6 different ways:
     * <ul>
     * <li><code>r</code>, <code>r</code>, <code>b</code>, <code>g</code>, <code>b</code>, <code>r</code></li>
     * <li><code>r</code>, <code>r</code>, <code>b</code>, <code>g</code>, <code>br</code></li>
     * <li><code>r</code>, <code>r</code>, <code>b</code>, <code>gb</code>, <code>r</code></li>
     * <li><code>r</code>, <code>rb</code>, <code>g</code>, <code>b</code>, <code>r</code></li>
     * <li><code>r</code>, <code>rb</code>, <code>g</code>, <code>br</code></li>
     * <li><code>r</code>, <code>rb</code>, <code>gb</code>, <code>r</code></li>
     * </ul>
     * <p>
     * <code>bwurrg</code> can only be made with <code>bwu</code>, <code>r</code>, <code>r</code>, and <code>g</code>.
     * <p>
     * <code>brgr</code> can be made in two different ways: <code>b</code>, <code>r</code>, <code>g</code>, <code>r</code> <em>or</em> <code>br</code>, <code>g</code>, <code>r</code>.
     * <p>
     * <code>ubwu</code> and <code>bbrgwb</code> are still impossible.
     * <p>
     * Adding up all of the ways the towels in this example could be arranged into the desired designs yields <code><em>16</em></code> (<code>2 + 1 + 4 + 6 + 1 + 2</code>).
     * <p>
     * They'll let you into the onsen as soon as you have the list.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you add up the number of different ways you could make each design?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<List<String>> batches = context.readBatches();
        final Map<Character, List<char[]>> towels = stream(batches.getFirst().getFirst().split(",\\s*"))
                .map(String::toCharArray)
                .collect(Collectors.groupingBy(c -> c[0]));
        final List<char[]> patterns = batches.getLast().stream()
                .map(String::toCharArray)
                .toList();

        return patterns.stream()
                .mapToLong(pattern -> tallyPossibleArrangements(pattern, towels))
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final List<List<String>> batches = context.readBatches();
        final Map<Character, List<char[]>> towels = stream(batches.getFirst().getFirst().split(",\\s*"))
                .map(String::toCharArray)
                .collect(Collectors.groupingBy(c -> c[0]));
        final List<char[]> patterns = batches.getLast().stream()
                .map(String::toCharArray)
                .toList();

        return patterns.stream()
                .reduce(
                        LongTuple.NULL,
                        (result, pattern) -> {
                            final long tally = tallyPossibleArrangements(pattern, towels);
                            return result.add(
                                    tally > 0 ? 1 : 0,
                                    tally
                            );
                        },
                        LongTuple::add
                );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Tally all the possible arrangements of towels to make a pattern.
     */
    private long tallyPossibleArrangements(final char[] pattern, final Map<Character, List<char[]>> allTowels) {
        final long[] counts = new long[pattern.length + 1];
        counts[0] = 1;

        for (int start = 0; start < pattern.length; start++) {
            if (counts[start] == 0) continue;
            final List<char[]> towels = allTowels.get(pattern[start]);
            if (towels == null) continue;

            towels:
            for (final char[] towel : towels) {
                final int end = start + towel.length;
                if (end > pattern.length) continue;
                for (int i = 1; i < towel.length; i++)
                    if (pattern[start + i] != towel[i])
                        continue towels;

                counts[end] += counts[start];
            }
        }

        return counts[pattern.length];
    }

}

