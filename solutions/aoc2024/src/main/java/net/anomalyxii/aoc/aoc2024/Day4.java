package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;
import net.anomalyxii.aoc.utils.geometry.Velocity;

import java.util.concurrent.atomic.AtomicInteger;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 4: Ceres Search.
 */
@Solution(year = 2024, day = 4, title = "Ceres Search")
public class Day4 {

    /*
     * Search criteria for the XMAS word search.
     */
    private static final char[] SEARCH = new char[]{'X', 'M', 'A', 'S'};

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * "Looks like the Chief's not here.
     * Next!" One of The Historians pulls out a device and pushes the only button on it.
     * After a brief flash, you recognize the interior of the <a href="/2019/day/10">Ceres monitoring station</a>!
     * <p>
     * As the search for the Chief continues, a small Elf who lives on the station tugs on your shirt; she'd like to know if you could help her with her <em>word search</em> (your puzzle input).
     * She only has to find one word: <code>XMAS</code>.
     * <p>
     * This word search allows words to be horizontal, vertical, diagonal, written backwards, or even overlapping other words.
     * It's a little unusual, though, as you don't merely need to find one instance of <code>XMAS</code> - you need to find <em>all of them</em>.
     * Here are a few ways <code>XMAS</code> might appear, where irrelevant characters have been replaced with <code>.</code>:
     *
     * <pre>
     * ..X...
     * .SAMX.
     * .A..A.
     * XMAS.S
     * .X....
     * </pre>
     * <p>
     * The actual word search will be full of letters instead.
     * For example:
     * <pre>
     * MMMSXXMASM
     * MSAMXMSMSA
     * AMXSXMAAMM
     * MSAMASMSMX
     * XMASAMXAMM
     * XXAMMXXAMA
     * SMSMSASXSS
     * SAXAMASAAA
     * MAMMMXMMMM
     * MXMXAXMASX
     * </pre>
     * <p>
     * In this word search, <code>XMAS</code> occurs a total of <code><em>18</em></code> times; here's the same word search again, but where letters not involved in any <code>XMAS</code> have been replaced with <code>.</code>:
     * <pre>
     * ....XXMAS.
     * .SAMXMS...
     * ...S..A...
     * ..A.A.MS.X
     * XMASAMX.MM
     * X.....XA.A
     * S.S.S.S.SS
     * .A.A.A.A.A
     * ..M.M.M.MM
     * .X.X.XMASX
     * </pre>
     * <p>
     * Take a look at the little Elf's word search.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many times does <code>XMAS</code> appear?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final Grid grid = context.readGrid();
        final AtomicInteger counter = new AtomicInteger(0);
        grid.forEach((px) -> {
            final int vx = grid.get(px);
            if (vx != 'X') return;

            xmasSearch(grid, px, counter);
        });
        return counter.get();
    }

    /**
     * The Elf looks quizzically at you.
     * Did you misunderstand the assignment?
     * <p>
     * Looking for the instructions, you flip over the word search to find that this isn't actually an <code><em>XMAS</em></code> puzzle; it's an <span title="This part originally involved searching for something else, but this joke was too dumb to pass up."><code><em>X-MAS</em></code></span> puzzle in which you're supposed to find two <code>MAS</code> in the shape of an <code>X</code>.
     * One way to achieve that is like this:
     * <pre>
     * M.S
     * .A.
     * M.S
     * </pre>
     * <p>
     * Irrelevant characters have again been replaced with <code>.</code> in the above diagram.
     * Within the <code>X</code>, each <code>MAS</code> can be written forwards or backwards.
     * <p>
     * Here's the same example from before, but this time all of the <code>X-MAS</code>es have been kept instead:
     * <pre>
     * .M.S......
     * ..A..MSMS.
     * .M.S.MAA..
     * ..A.ASMSM.
     * .M.S.M....
     * ..........
     * S.S.S.S.S.
     * .A.A.A.A..
     * M.M.M.M.M.
     * ..........
     * </pre>
     * <p>
     * In this example, an <code>X-MAS</code> appears <code><em>9</em></code> times.
     * <p>
     * Flip the word search from the instructions back over to the word search side and try again.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many times does an <code>X-MAS</code> appear?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Grid grid = context.readGrid();
        final AtomicInteger counter = new AtomicInteger(0);
        grid.forEach((pa) -> {
            final int va = grid.get(pa);
            if (va != 'A') return;

            crossMasSearch(grid, pa, counter);
        });
        return counter.get();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final Grid grid = context.readGrid();
        final AtomicInteger part1 = new AtomicInteger(0);
        final AtomicInteger part2 = new AtomicInteger(0);

        grid.forEach((p) -> {
            final int v = grid.get(p);
            if (v == 'X') xmasSearch(grid, p, part1);
            else if (v == 'A') crossMasSearch(grid, p, part2);
        });

        return new IntTuple(part1.get(), part2.get());
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Search for possible occurrences of XMAS.
     */
    private static void xmasSearch(
            final Grid grid,
            final Coordinate start,
            final AtomicInteger counter
    ) {
        Velocity.directions()
                .forEach(direction -> {
                    Coordinate p = start;
                    for (final char search : SEARCH) {
                        if (!grid.contains(p)) return;
                        if (grid.get(p) != search) return;
                        p = p.adjustBy(direction);
                    }

                    counter.incrementAndGet();
                });
    }

    /*
     * Search for a possible occurrence of a MASxMAS.
     */
    private static void crossMasSearch(
            final Grid grid,
            final Coordinate start,
            final AtomicInteger counter
    ) {
        final Coordinate nw = start.adjustBy(Velocity.NORTH_EAST);
        if (!grid.contains(nw)) return;
        final Coordinate se = start.adjustBy(Velocity.SOUTH_WEST);
        if (!grid.contains(se)) return;

        final boolean m1 = (grid.get(nw) == 'M' && grid.get(se) == 'S')
                || (grid.get(nw) == 'S' && grid.get(se) == 'M');
        if (!m1) return;

        final Coordinate ne = start.adjustBy(Velocity.NORTH_WEST);
        if (!grid.contains(ne)) return;
        final Coordinate sw = start.adjustBy(Velocity.SOUTH_EAST);
        if (!grid.contains(sw)) return;

        final boolean m2 = (grid.get(ne) == 'M' && grid.get(sw) == 'S')
                || (grid.get(ne) == 'S' && grid.get(sw) == 'M');
        if (!m2) return;

        counter.incrementAndGet();
    }

}

