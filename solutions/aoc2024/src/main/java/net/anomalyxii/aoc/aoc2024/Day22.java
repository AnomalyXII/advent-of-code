package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;

import java.util.Arrays;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 22: Monkey Market.
 */
@Solution(year = 2024, day = 22, title = "Monkey Market")
public class Day22 {

    /*
     * A magic number.
     *
     * In this case, it's an upper bound for the number of bananas that could
     * ever realistically be acquired (assuming ~2330 monkeys).
     */
    private static final int MAGIC_NUMBER = 0x0000FFFF;

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * As you're all teleported deep into the jungle, a <a href="/2022/day/11">monkey</a> steals The Historians' device!
     * You'll need get it back while The Historians are looking for the Chief.
     * <p>
     * The monkey that stole the device seems willing to trade it, but only in exchange for an absurd number of bananas.
     * Your only option is to buy bananas on the Monkey Exchange Market.
     * <p>
     * You aren't sure how the Monkey Exchange Market works, but one of The Historians senses trouble and comes over to help.
     * Apparently, they've been studying these monkeys for a while and have deciphered their secrets.
     * <p>
     * Today, the Market is full of monkeys buying <em>good hiding spots</em>.
     * Fortunately, because of the time you recently spent in this jungle, you know lots of good hiding spots you can sell!
     * If you sell enough hiding spots, you should be able to get enough bananas to buy the device back.
     * <p>
     * On the Market, the buyers seem to use random prices, but their prices are actually only <a href="https://en.wikipedia.org/wiki/Pseudorandom_number_generator" target="_blank">pseudorandom</a>!
     * If you know the secret of how they pick their prices, you can wait for the perfect time to sell.
     * <p>
     * The part about secrets is literal, the Historian explains.
     * Each buyer produces a pseudorandom sequence of secret numbers where each secret is derived from the previous.
     * <p>
     * In particular, each buyer's <em>secret</em> number evolves into the next secret number in the sequence via the following process:
     * <ul>
     * <li>Calculate the result of <em>multiplying the secret number by <code>64</code></em>.
     * Then, <em>mix</em> this result into the secret number.
     * Finally, <em>prune</em> the secret number.</li>
     * <li>Calculate the result of <em>dividing the secret number by <code>32</code></em>.
     * Round the result down to the nearest integer.
     * Then, <em>mix</em> this result into the secret number.
     * Finally, <em>prune</em> the secret number.</li>
     * <li>Calculate the result of <em>multiplying the secret number by <code>2048</code></em>.
     * Then, <em>mix</em> this result into the secret number.
     * Finally, <em>prune</em> the secret number.</li>
     * </ul>
     * <p>
     * Each step of the above process involves <em>mixing</em> and <em>pruning</em>:
     * <ul>
     * <li>To <em>mix</em> a value into the secret number, calculate the <a href="https://en.wikipedia.org/wiki/Bitwise_operation#XOR" target="_blank">bitwise XOR</a> of the given value and the secret number.
     * Then, the secret number becomes the result of that operation.
     * (If the secret number is <code>42</code> and you were to <em>mix</em> <code>15</code> into the secret number, the secret number would become <code>37</code>.)</li>
     * <li>To <em>prune</em> the secret number, calculate the value of the secret number <a href="https://en.wikipedia.org/wiki/Modulo" target="_blank">modulo</a> <code>16777216</code>.
     * Then, the secret number becomes the result of that operation.
     * (If the secret number is <code>100000000</code> and you were to <em>prune</em> the secret number, the secret number would become <code>16113920</code>.)</li>
     * </ul>
     * <p>
     * After this process completes, the buyer is left with the next secret number in the sequence.
     * The buyer can repeat this process as many times as necessary to produce more secret numbers.
     * <p>
     * So, if a buyer had a secret number of <code>123</code>, that buyer's next ten secret numbers would be:
     * <pre>
     * 15887950
     * 16495136
     * 527345
     * 704524
     * 1553684
     * 12683156
     * 11100544
     * 12249484
     * 7753432
     * 5908254
     * </pre>
     * <p>
     * Each buyer uses their own secret number when choosing their price, so it's important to be able to predict the sequence of secret numbers for each buyer.
     * Fortunately, the Historian's research has uncovered the <em>initial secret number of each buyer</em> (your puzzle input).
     * For example:
     * <pre>
     * 1
     * 10
     * 100
     * 2024
     * </pre>
     * <p>
     * This list describes the <em>initial secret number</em> of four different secret-hiding-spot-buyers on the Monkey Exchange Market.
     * If you can simulate secret numbers from each buyer, you'll be able to predict all of their future prices.
     * <p>
     * In a single day, buyers each have time to generate <code>2000</code> <em>new</em> secret numbers.
     * In this example, for each buyer, their initial secret number and the 2000th new secret number they would generate are:
     * <pre>
     * 1: 8685429
     * 10: 4700978
     * 100: 15273692
     * 2024: 8667524
     * </pre>
     * <p>
     * Adding up the 2000th new secret number for each buyer produces <code><em>37327623</em></code>.
     * <p>
     * For each buyer, simulate the creation of 2000 new secret numbers.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the 2000th secret number generated by each buyer?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .mapToInt(Integer::parseInt)
                .mapToLong(init -> {
                    long next = init;
                    for (int i = 0; i < 2000; i++) {
                        next = op(next);
                    }
                    return next;
                })
                .sum();
    }

    /**
     * Of course, the secret numbers aren't the prices each buyer is offering!
     * That would be <span title="Some might say it would be...
     * bananas.">ridiculous</span>.
     * Instead, the <em>prices</em> the buyer offers are just the <em>ones digit</em> of each of their secret numbers.
     * <p>
     * So, if a buyer starts with a secret number of <code>123</code>, that buyer's first ten <em>prices</em> would be:
     * <pre>
     * 3 (from 123)
     * 0 (from 15887950)
     * 6 (from 16495136)
     * 5 (etc.)
     * 4
     * 4
     * 6
     * 4
     * 4
     * 2
     * </pre>
     * <p>
     * This price is the number of <em>bananas</em> that buyer is offering in exchange for your information about a new hiding spot.
     * However, you still don't speak <a href="/2022/day/21">monkey</a>, so you can't negotiate with the buyers directly.
     * The Historian speaks a little, but not enough to negotiate; instead, he can ask another monkey to negotiate on your behalf.
     * <p>
     * Unfortunately, the monkey only knows how to decide when to sell by looking at the <em>changes</em> in price.
     * Specifically, the monkey will only look for a specific sequence of <em>four consecutive changes</em> in price, then immediately sell when it sees that sequence.
     * <p>
     * So, if a buyer starts with a secret number of <code>123</code>, that buyer's first ten secret numbers, prices, and the associated changes would be:
     * <pre>
     * 123: 3
     * 15887950: 0 (-3)
     * 16495136: 6 (6)
     *   527345: 5 (-1)
     *   704524: 4 (-1)
     *  1553684: 4 (0)
     * 12683156: 6 (2)
     * 11100544: 4 (-2)
     * 12249484: 4 (0)
     *  7753432: 2 (-2)
     * </pre>
     * <p>
     * Note that the first price has no associated change because there was no previous price to compare it with.
     * <p>
     * In this short example, within just these first few prices, the highest price will be <code>6</code>, so it would be nice to give the monkey instructions that would make it sell at that time.
     * The first <code>6</code> occurs after only two changes, so there's no way to instruct the monkey to sell then, but the second <code>6</code> occurs after the changes <code>-1,-1,0,2</code>.
     * So, if you gave the monkey that sequence of changes, it would wait until the first time it sees that sequence and then immediately sell your hiding spot information at the current price, winning you <code>6</code> bananas.
     * <p>
     * Each buyer only wants to buy one hiding spot, so after the hiding spot is sold, the monkey will move on to the next buyer.
     * If the monkey <em>never</em> hears that sequence of price changes from a buyer, the monkey will never sell, and will instead just move on to the next buyer.
     * <p>
     * Worse, you can only give the monkey <em>a single sequence</em> of four price changes to look for.
     * You can't change the sequence between buyers.
     * <p>
     * You're going to need as many bananas as possible, so you'll need to <em>determine which sequence</em> of four price changes will cause the monkey to get you the <em>most bananas overall</em>.
     * Each buyer is going to generate <code>2000</code> secret numbers after their initial secret number, so, for each buyer, you'll have <em><code>2000</code> price changes</em> in which your sequence can occur.
     * <p>
     * Suppose the initial secret number of each buyer is:
     * <pre>
     * 1
     * 2
     * 3
     * 2024
     * </pre>
     * <p>
     * There are many sequences of four price changes you could tell the monkey, but for these four buyers, the sequence that will get you the most bananas is <code>-2,1,-1,3</code>.
     * Using that sequence, the monkey will make the following sales:
     * <ul>
     * <li>For the buyer with an initial secret number of <code>1</code>, changes <code>-2,1,-1,3</code> first occur when the price is <code><em>7</em></code>.</li>
     * <li>For the buyer with initial secret <code>2</code>, changes <code>-2,1,-1,3</code> first occur when the price is <code><em>7</em></code>.</li>
     * <li>For the buyer with initial secret <code>3</code>, the change sequence <code>-2,1,-1,3</code> <em>does not occur</em> in the first 2000 changes.</li>
     * <li>For the buyer starting with <code>2024</code>, changes <code>-2,1,-1,3</code> first occur when the price is <code><em>9</em></code>.</li>
     * </ul>
     * <p>
     * So, by asking the monkey to sell the first time each buyer's prices go down <code>2</code>, then up <code>1</code>, then down <code>1</code>, then up <code>3</code>, you would get <code><em>23</em></code> (<code>7 + 7 + 9</code>) bananas!
     * <p>
     * Figure out the best sequence to tell the monkey so that by looking for that same sequence of changes in every buyer's future prices, you get the most bananas in total.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the most bananas you can get?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final int[] sequenceToBananaHaul = new int[(MAGIC_NUMBER + 1) >> 1];
        final int[] seenSequences = new int[(MAGIC_NUMBER + 1) >> 5];
        context.stream()
                .mapToInt(Integer::parseInt)
                .forEach(init -> generateSecretNumbers(init, sequenceToBananaHaul, seenSequences));

        return IntStream.of(sequenceToBananaHaul)
                .flatMap(packed -> IntStream.of(packed >> 16 & MAGIC_NUMBER, packed & MAGIC_NUMBER))
                .max()
                .orElseThrow();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final int[] sequenceToBananaHaul = new int[(MAGIC_NUMBER + 1) >> 1];
        final int[] seenSequences = new int[(MAGIC_NUMBER + 1) >> 5];
        final long part1 = context.stream()
                .mapToInt(Integer::parseInt)
                .mapToLong(init -> generateSecretNumbers(init, sequenceToBananaHaul, seenSequences))
                .sum();

        final int part2 = IntStream.of(sequenceToBananaHaul)
                .flatMap(packed -> IntStream.of(packed >> 16 & MAGIC_NUMBER, packed & MAGIC_NUMBER))
                .max()
                .orElseThrow();

        return new LongTuple(part1, part2);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Generate 2000 secret numbers, and update the banana haul.
     */
    private static long generateSecretNumbers(final int init, final int[] sequenceToBananaHaul, final int[] seenSequences) {
        short deltas = 0;
        long next = init;

        Arrays.fill(seenSequences, 0);
        for (int i = 0; i < 2000; i++) {
            final long newNext = op(next);

            final byte qty = (byte) (newNext % 10);
            final byte prevQty = (byte) (next % 10);
            final byte delta = (byte) (qty - prevQty);
            deltas = (short) (deltas << 4 | 0x0F & delta);

            next = newNext;
            final int hash = MAGIC_NUMBER & deltas;
            final int mod = 1 << ((0x00FF & hash) & 0x1F);
            if (i < 3 || (seenSequences[hash >> 5] & mod) != 0) continue;
            seenSequences[hash >> 5] |= mod;

            final int packed = sequenceToBananaHaul[hash >> 1];
            final int unpackedTop = packed >> 16 & MAGIC_NUMBER;
            final int unpackedBottom = packed & MAGIC_NUMBER;

            sequenceToBananaHaul[hash >> 1] = (hash & 0x01) == 1
                    ? unpackedTop + qty << 16 | unpackedBottom
                    : unpackedTop << 16 | unpackedBottom + qty;
        }

        return next;
    }

    /*
     * Transform a secret number.
     */
    private static long op(final long next) {
        final long op1 = (next << 6 ^ next) & 0x0FFFFFF;
        final long op2 = (op1 >> 5 ^ op1) & 0x0FFFFFF;
        return (op2 << 11 ^ op2) & 0x0FFFFFF;
    }

}

