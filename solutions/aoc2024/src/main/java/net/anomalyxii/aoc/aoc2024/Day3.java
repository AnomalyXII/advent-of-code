package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 3: Mull It Over.
 */
@Solution(year = 2024, day = 3, title = "Mull It Over")
public class Day3 {

    private static final Pattern MATCHER = Pattern.compile("do\\(\\)|don't\\(\\)|mul\\(([0-9]{1,3}),([0-9]{1,3})\\)");

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * "Our computers are having issues, so I have no idea if we have any Chief Historians <span title="There's a spot reserved for Chief Historians between the green toboggans and the red toboggans. They've never actually had any Chief Historians in stock, but it's best to be prepared.">in stock</span>!
     * You're welcome to check the warehouse, though," says the mildly flustered shopkeeper at the <a href="/2020/day/2">North Pole Toboggan Rental Shop</a>.
     * The Historians head out to take a look.
     * <p>
     * The shopkeeper turns to you. "Any chance you can see why our computers are having issues again?"
     * <p>
     * The computer appears to be trying to run a program, but its memory (your puzzle input) is <em>corrupted</em>.
     * All of the instructions have been jumbled up!
     * <p>
     * It seems like the goal of the program is just to <em>multiply some numbers</em>.
     * It does that with instructions like <code>mul(X,Y)</code>, where <code>X</code> and <code>Y</code> are each 1-3 digit numbers.
     * For instance, <code>mul(44,46)</code> multiplies <code>44</code> by <code>46</code> to get a result of <code>2024</code>.
     * Similarly, <code>mul(123,4)</code> would multiply <code>123</code> by <code>4</code>.
     * <p>
     * However, because the program's memory has been corrupted, there are also many invalid characters that should be <em>ignored</em>, even if they look like part of a <code>mul</code> instruction.
     * Sequences like <code>mul(4*</code>, <code>mul(6,9!</code>, <code>?(12,34)</code>, or <code>mul ( 2 , 4 )</code> do <em>nothing</em>.
     * <p>
     * For example, consider the following section of corrupted memory:
     * <pre>x<em>mul(2,4)</em>%&mul[3,7]!@^do_not_<em>mul(5,5)</em>+mul(32,64]then(<em>mul(11,8)mul(8,5)</em>)</pre>
     * <p>
     * Only the four highlighted sections are real <code>mul</code> instructions.
     * Adding up the result of each instruction produces <code><em>161</em></code> (<code>2*4 + 5*5 + 11*8 + 8*5</code>).
     * <p>
     * Scan the corrupted memory for uncorrupted <code>mul</code> instructions.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you add up all of the results of the multiplications?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final ResultMemoryContext memory = new BasicMemoryContext();
        context.consume(line -> process(line, memory));
        return memory.result();
    }

    /**
     * As you scan through the corrupted memory, you notice that some of the conditional statements are also still intact.
     * If you handle some of the uncorrupted conditional statements in the program, you might be able to get an even more accurate result.
     * <p>
     * There are two new instructions you'll need to handle:
     * <ul>
     * <li>The <code>do()</code> instruction <em>enables</em> future <code>mul</code> instructions.</li>
     * <li>The <code>don't()</code> instruction <em>disables</em> future <code>mul</code> instructions.</li>
     * </ul>
     * <p>
     * Only the <em>most recent</em> <code>do()</code> or <code>don't()</code> instruction applies.
     * At the beginning of the program, <code>mul</code> instructions are <em>enabled</em>.
     * <p>
     * For example:
     * <pre>x<em>mul(2,4)</em>&mul[3,7]!^<em>don't()</em>_mul(5,5)+mul(32,64](mul(11,8)un<em>do()</em>?<em>mul(8,5)</em>)</pre>
     * <p>
     * This corrupted memory is similar to the example from before, but this time the <code>mul(5,5)</code> and <code>mul(11,8)</code> instructions are <em>disabled</em> because there is a <code>don't()</code> instruction before them.
     * The other <code>mul</code> instructions function normally, including the one at the end that gets re-<em>enabled</em> by a <code>do()</code> instruction.
     * <p>
     * This time, the sum of the results is <code><em>48</em></code> (<code>2*4 + 8*5</code>).
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Handle the new instructions; what do you get if you add up all of the results of just the enabled multiplications?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final ResultMemoryContext memory = new AdvancedMemoryContext();
        context.consume(line -> process(line, memory));
        return memory.result();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final CombinedMemoryContext memory = new CombinedMemoryContext();
        context.consume(line -> process(line, memory));
        return memory.result();
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Process the corrupted memory instructions.
     */
    private static void process(final String line, final MemoryContext memory) {
        final Matcher matcher = MATCHER.matcher(line);
        while (matcher.find()) {
            final String match = matcher.group(0);
            if (match.startsWith("do()"))
                memory.enable();
            else if (match.startsWith("don't()"))
                memory.disable();
            else
                memory.accumulate(matcher.group(1), matcher.group(2));
        }
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Hold the result of the processed memory.
     */
    private interface MemoryContext {

        // Interface Methods

        /**
         * Enable processing of {@code mul()} instructions.
         */
        void enable();

        /**
         * Disable processing of {@code mul()} instructions.
         */
        void disable();

        /**
         * Accumulate the result of a {@code mul()} instruction.
         */
        void accumulate(String first, String second);

    }

    /*
     * A `MemoryContext` that can be queried for the value currently
     * accumulated in memory.
     */
    private interface ResultMemoryContext extends MemoryContext {

        // Interface Methods

        /**
         * Get the accumulated result.
         *
         * @return the result
         */
        long result();

    }

    /*
     * A basic `MemoryContext` that will simply accumulate multiplication
     * results.
     */
    private static class BasicMemoryContext implements ResultMemoryContext {

        // Private Members

        private long result = 0;

        // MemoryContext Methods

        @Override
        public void enable() {
        }

        @Override
        public void disable() {
        }

        @Override
        public void accumulate(final String first, final String second) {
            result += ((long) Integer.parseInt(first) * Integer.parseInt(second));
        }

        // ResultMemoryContext Methods

        @Override
        public long result() {
            return result;
        }
    }

    /*
     * An advanced `MemoryContext` that will enable and disable
     * instructions as appropriate.
     */
    private static final class AdvancedMemoryContext extends BasicMemoryContext {

        // Private Members

        private boolean mulOperationsEnabled = true;

        // MemoryContext Methods

        @Override
        public void enable() {
            mulOperationsEnabled = true;
        }

        @Override
        public void disable() {
            mulOperationsEnabled = false;
        }

        @Override
        public void accumulate(final String first, final String second) {
            if (mulOperationsEnabled)
                super.accumulate(first, second);
        }
    }

    /*
     * A `MemoryContext`
     */
    private static final class CombinedMemoryContext implements MemoryContext {

        // Private Members

        private final ResultMemoryContext part1 = new BasicMemoryContext();
        private final ResultMemoryContext part2 = new AdvancedMemoryContext();

        // MemoryContext Methods

        @Override
        public void enable() {
            part1.enable();
            part2.enable();
        }

        @Override
        public void disable() {
            part1.disable();
            part2.disable();
        }

        @Override
        public void accumulate(final String first, final String second) {
            part1.accumulate(first, second);
            part2.accumulate(first, second);
        }

        // Helper Methods

        /*
         * Get the combined result.
         */
        LongTuple result() {
            return new LongTuple(part1.result(), part2.result());
        }
    }
}

