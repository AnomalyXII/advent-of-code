package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Direction;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.lang.reflect.Array;
import java.util.*;

import static java.lang.Math.min;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 16: Reindeer Maze.
 */
@Solution(year = 2024, day = 16, title = "Reindeer Maze")
public class Day16 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * It's time again for the <a href="/2015/day/14">Reindeer Olympics</a>!
     * This year, the big event is the <em>Reindeer Maze</em>, where the Reindeer compete for the <em><span title="I would say it's like Reindeer Golf, but knowing Reindeer, it's almost certainly nothing like Reindeer Golf.">lowest score</span></em>.
     * <p>
     * You and The Historians arrive to search for the Chief right as the event is about to start.
     * It wouldn't hurt to watch a little, right?
     * <p>
     * The Reindeer start on the Start Tile (marked <code>S</code>) facing <em>East</em> and need to reach the End Tile (marked <code>E</code>).
     * They can move forward one tile at a time (increasing their score by <code>1</code> point), but never into a wall (<code>#</code>).
     * They can also rotate clockwise or counterclockwise 90 degrees at a time (increasing their score by <code>1000</code> points).
     * <p>
     * To figure out the best place to sit, you start by grabbing a map (your puzzle input) from a nearby kiosk.
     * For example:
     * <pre>
     * ###############
     * #.......#....E#
     * #.#.###.#.###.#
     * #.....#.#...#.#
     * #.###.#####.#.#
     * #.#.#.......#.#
     * #.#.#####.###.#
     * #...........#.#
     * ###.#.#####.#.#
     * #...#.....#.#.#
     * #.#.#.###.#.#.#
     * #.....#...#.#.#
     * #.###.#.#.#.#.#
     * #S..#.....#...#
     * ###############
     * </pre>
     * <p>
     * There are many paths through this maze, but taking any of the best paths would incur a score of only <code><em>7036</em></code>.
     * This can be achieved by taking a total of <code>36</code> steps forward and turning 90 degrees a total of <code>7</code> times:
     * <pre>
     *
     * ###############
     * #.......#....<em>E</em>#
     * #.#.###.#.###<em>^</em>#
     * #.....#.#...#<em>^</em>#
     * #.###.#####.#<em>^</em>#
     * #.#.#.......#<em>^</em>#
     * #.#.#####.###<em>^</em>#
     * #..<em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>v</em>#<em>^</em>#
     * ###<em>^</em>#.#####<em>v</em>#<em>^</em>#
     * #<em>&gt;</em><em>&gt;</em><em>^</em>#.....#<em>v</em>#<em>^</em>#
     * #<em>^</em>#.#.###.#<em>v</em>#<em>^</em>#
     * #<em>^</em>....#...#<em>v</em>#<em>^</em>#
     * #<em>^</em>###.#.#.#<em>v</em>#<em>^</em>#
     * #S..#.....#<em>&gt;</em><em>&gt;</em><em>^</em>#
     * ###############
     * </pre>
     * <p>
     * Here's a second example:
     * <pre>
     * #################
     * #...#...#...#..E#
     * #.#.#.#.#.#.#.#.#
     * #.#.#.#...#...#.#
     * #.#.#.#.###.#.#.#
     * #...#.#.#.....#.#
     * #.#.#.#.#.#####.#
     * #.#...#.#.#.....#
     * #.#.#####.#.###.#
     * #.#.#.......#...#
     * #.#.###.#####.###
     * #.#.#...#.....#.#
     * #.#.#.#####.###.#
     * #.#.#.........#.#
     * #.#.#.#########.#
     * #S#.............#
     * #################
     * </pre>
     * <p>
     * In this maze, the best paths cost <code><em>11048</em></code> points; following one such path would look like this:
     * <pre>
     * #################
     * #...#...#...#..<em>E</em>#
     * #.#.#.#.#.#.#.#<em>^</em>#
     * #.#.#.#...#...#<em>^</em>#
     * #.#.#.#.###.#.#<em>^</em>#
     * #<em>&gt;</em><em>&gt;</em><em>v</em>#.#.#.....#<em>^</em>#
     * #<em>^</em>#<em>v</em>#.#.#.#####<em>^</em>#
     * #<em>^</em>#<em>v</em>..#.#.#<em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>^</em>#
     * #<em>^</em>#<em>v</em>#####.#<em>^</em>###.#
     * #<em>^</em>#<em>v</em>#..<em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>^</em>#...#
     * #<em>^</em>#<em>v</em>###<em>^</em>#####.###
     * #<em>^</em>#<em>v</em>#<em>&gt;</em><em>&gt;</em><em>^</em>#.....#.#
     * #<em>^</em>#<em>v</em>#<em>^</em>#####.###.#
     * #<em>^</em>#<em>v</em>#<em>^</em>........#.#
     * #<em>^</em>#<em>v</em>#<em>^</em>#########.#
     * #S#<em>&gt;</em><em>&gt;</em><em>^</em>..........#
     * #################
     * </pre>
     * <p>
     * Note that the path shown above includes one 90 degree turn as the very first move, rotating the Reindeer from facing East to facing North.
     * <p>
     * Analyze your map carefully.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the lowest score a Reindeer could possibly get?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final Grid grid = context.readGrid();
        final Coordinate start = grid.stream()
                .filter(c -> grid.get(c) == 'S')
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Could not find starting position"));
        final Coordinate end = grid.stream()
                .filter(c -> grid.get(c) == 'E')
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Could not find starting position"));

        return findShortestPath(grid, start, end, new HashSet<>());
    }

    /**
     * Now that you know what the best paths look like, you can figure out the best spot to sit.
     * <p>
     * Every non-wall tile (<code>S</code>, <code>.</code>, or <code>E</code>) is equipped with places to sit along the edges of the tile.
     * While determining which of these tiles would be the best spot to sit depends on a whole bunch of factors (how comfortable the seats are, how far away the bathrooms are, whether there's a pillar blocking your view, etc.), the most important factor is <em>whether the tile is on one of the best paths through the maze</em>.
     * If you sit somewhere else, you'd miss all the action!
     * <p>
     * So, you'll need to determine which tiles are part of <em>any</em> best path through the maze, including the <code>S</code> and <code>E</code> tiles.
     * <p>
     * In the first example, there are <code><em>45</em></code> tiles (marked <code>O</code>) that are part of at least one of the various best paths through the maze:
     * <pre>
     * ###############
     * #.......#....<em>O</em>#
     * #.#.###.#.###<em>O</em>#
     * #.....#.#...#<em>O</em>#
     * #.###.#####.#<em>O</em>#
     * #.#.#.......#<em>O</em>#
     * #.#.#####.###<em>O</em>#
     * #..<em>O</em><em>O</em><em>O</em><em>O</em><em>O</em><em>O</em><em>O</em><em>O</em><em>O</em>#<em>O</em>#
     * ###<em>O</em>#<em>O</em>#####<em>O</em>#<em>O</em>#
     * #<em>O</em><em>O</em><em>O</em>#<em>O</em>....#<em>O</em>#<em>O</em>#
     * #<em>O</em>#<em>O</em>#<em>O</em>###.#<em>O</em>#<em>O</em>#
     * #<em>O</em><em>O</em><em>O</em><em>O</em><em>O</em>#...#<em>O</em>#<em>O</em>#
     * #<em>O</em>###.#.#.#<em>O</em>#<em>O</em>#
     * #<em>O</em>..#.....#<em>O</em><em>O</em><em>O</em>#
     * ###############
     * </pre>
     * <p>
     * In the second example, there are <code><em>64</em></code> tiles that are part of at least one of the best paths:
     * <pre>
     * #################
     * #...#...#...#..<em>O</em>#
     * #.#.#.#.#.#.#.#<em>O</em>#
     * #.#.#.#...#...#<em>O</em>#
     * #.#.#.#.###.#.#<em>O</em>#
     * #<em>O</em><em>O</em><em>O</em>#.#.#.....#<em>O</em>#
     * #<em>O</em>#<em>O</em>#.#.#.#####<em>O</em>#
     * #<em>O</em>#<em>O</em>..#.#.#<em>O</em><em>O</em><em>O</em><em>O</em><em>O</em>#
     * #<em>O</em>#<em>O</em>#####.#<em>O</em>###<em>O</em>#
     * #<em>O</em>#<em>O</em>#..<em>O</em><em>O</em><em>O</em><em>O</em><em>O</em>#<em>O</em><em>O</em><em>O</em>#
     * #<em>O</em>#<em>O</em>###<em>O</em>#####<em>O</em>###
     * #<em>O</em>#<em>O</em>#<em>O</em><em>O</em><em>O</em>#..<em>O</em><em>O</em><em>O</em>#.#
     * #<em>O</em>#<em>O</em>#<em>O</em>#####<em>O</em>###.#
     * #<em>O</em>#<em>O</em>#<em>O</em><em>O</em><em>O</em><em>O</em><em>O</em><em>O</em><em>O</em>..#.#
     * #<em>O</em>#<em>O</em>#<em>O</em>#########.#
     * #<em>O</em>#<em>O</em><em>O</em><em>O</em>..........#
     * #################
     * </pre>
     * <p>
     * Analyze your map further.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many tiles are part of at least one of the best paths through the maze?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Grid grid = context.readGrid();
        final Coordinate start = grid.stream()
                .filter(c -> grid.get(c) == 'S')
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Could not find starting position"));
        final Coordinate end = grid.stream()
                .filter(c -> grid.get(c) == 'E')
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Could not find starting position"));

        final Set<Coordinate> onPath = new HashSet<>();
        findShortestPath(grid, start, end, onPath);
        return onPath.size();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final Grid grid = context.readGrid();
        final Coordinate start = grid.stream()
                .filter(c -> grid.get(c) == 'S')
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Could not find starting position"));
        final Coordinate end = grid.stream()
                .filter(c -> grid.get(c) == 'E')
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Could not find starting position"));

        final Set<Coordinate> onPath = new HashSet<>();
        final int shortestPath = findShortestPath(grid, start, end, onPath);
        return new IntTuple(shortestPath, onPath.size());
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Modification of Dijkstra's shortest path, to deal with directions.
     */
    private int findShortestPath(
            final Grid grid,
            final Coordinate from,
            final Coordinate to,
            final Set<Coordinate> onPath
    ) {
        final int[] dist = (int[]) Array.newInstance(int.class, grid.height() * grid.width() * 4);

        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[offset(grid, from, Direction.RIGHT)] = 0;

        final PriorityQueue<TrackedState> queue = new PriorityQueue<>(TrackedState.COMPARATOR);
        queue.add(prio(from, Direction.RIGHT, new ArrayList<>(List.of(from)), 0));

        TrackedState uk;
        int lowestScoreSoFar = Integer.MAX_VALUE;
        while ((uk = queue.poll()) != null) {
            final Coordinate uc = uk.coordinate;
            final Direction ud = uk.direction;

            final int priority = dist[offset(grid, uc, ud)];
            if (uk.priority > priority) continue;

            if (uc.equals(to) && priority <= lowestScoreSoFar) {
                if (priority < lowestScoreSoFar) {
                    lowestScoreSoFar = priority;
                    onPath.clear();
                }

                onPath.addAll(uk.path);
                continue;
            }

            for (final Direction d : new Direction[]{ud, ud.rotateClockwise(), ud.rotateAnticlockwise()}) {
                if (d == ud.reverse()) continue;

                final Coordinate neighbour = uc.adjustBy(d);
                final char sym = (char) grid.get(neighbour);
                if (sym == '#') continue;

                final boolean forward = ud == d;
                final int modifier = forward ? 0 : 1000;
                final int alt = priority + modifier + 1;

                final int idx = offset(grid, neighbour, d);
                final int neighbourPriority = dist[idx];
                final int target = min(neighbourPriority, lowestScoreSoFar);
                if (alt <= target) {
                    dist[idx] = alt;

                    final List<Coordinate> newPath = new ArrayList<>(uk.path);
                    newPath.add(neighbour);
                    queue.add(prio(neighbour, d, newPath, alt));
                }
            }
        }

        return lowestScoreSoFar;
    }

    /*
     * Compute the linear offset of the `Coordinate`/`Direction` pair.
     */
    private int offset(final Grid grid, final Coordinate from, final Direction direction) {
        return (from.y() * grid.width() + from.x()) * 4 + direction.ordinal();
    }

    /*
     * Wrap the given `Coordinate`, `Direction` and `Priority` into a
     * `TrackedState`.
     */
    private static TrackedState prio(
            final Coordinate coordinate,
            final Direction direction,
            final Collection<Coordinate> path,
            final int priority
    ) {
        return new TrackedState(coordinate, direction, path, priority);
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A combination of a `CoordinateAndDirection` and the priority.
     */
    private record TrackedState(Coordinate coordinate, Direction direction, Collection<Coordinate> path, int priority) {

        /*
         * A comparator for `TrackedState`.
         */
        static final Comparator<TrackedState> COMPARATOR = Comparator.comparingLong((TrackedState pp) -> pp.priority);

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (o == null || getClass() != o.getClass()) return false;
            final TrackedState that = (TrackedState) o;
            return Objects.equals(coordinate, that.coordinate)
                    && direction == that.direction
                    && priority == that.priority;
        }

        @Override
        public int hashCode() {
            return Objects.hash(coordinate, direction, priority);
        }

    }

}

