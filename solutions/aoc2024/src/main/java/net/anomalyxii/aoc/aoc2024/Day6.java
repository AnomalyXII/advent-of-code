package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Direction;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 6: Guard Gallivant.
 */
@Solution(year = 2024, day = 6, title = "Guard Gallivant")
public class Day6 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The Historians use their fancy <a href="4">device</a> again, this time to whisk you all away to the North Pole prototype suit manufacturing lab... in the year <a href="/2018/day/5">1518</a>!
     * It turns out that having direct access to history is very convenient for a group of historians.
     * <p>
     * You still have to be careful of time paradoxes, and so it will be important to avoid anyone from 1518 while The Historians search for the Chief.
     * Unfortunately, a single <em>guard</em> is patrolling this part of the lab.
     * <p>
     * Maybe you can work out where the guard will go ahead of time so that The Historians can search safely?
     * <p>
     * You start by making a map (your puzzle input) of the situation.
     * For example:
     * <pre>
     * ....#.....
     * .........#
     * ..........
     * ..#.......
     * .......#..
     * ..........
     * .#..^.....
     * ........#.
     * #.........
     * ......#...
     * </pre>
     * <p>
     * The map shows the current position of the guard with <code>^</code> (to indicate the guard is currently facing <em>up</em> from the perspective of the map).
     * Any <em>obstructions</em> - crates, desks, alchemical reactors, etc. - are shown as <code>#</code>.
     * <p>
     * Lab guards in 1518 follow a very strict patrol protocol which involves repeatedly following these steps:
     * <ul>
     * <li>If there is something directly in front of you, turn right 90 degrees.</li>
     * <li>Otherwise, take a step forward.</li>
     * </ul>
     * <p>
     * Following the above protocol, the guard moves up several times until she reaches an obstacle (in this case, a pile of failed suit prototypes):
     * <pre>
     * ....#.....
     * ....^....#
     * ..........
     * ..#.......
     * .......#..
     * ..........
     * .#........
     * ........#.
     * #.........
     * ......#...
     * </pre>
     * <p>
     * Because there is now an obstacle in front of the guard, she turns right before continuing straight in her new facing direction:
     * <pre>
     * ....#.....
     * ........&gt;#
     * ..........
     * ..#.......
     * .......#..
     * ..........
     * .#........
     * ........#.
     * #.........
     * ......#...
     * </pre>
     * <p>
     * Reaching another obstacle (a spool of several <em>very</em> long polymers), she turns right again and continues downward:
     * <pre>
     * ....#.....
     * .........#
     * ..........
     * ..#.......
     * .......#..
     * ..........
     * .#......v.
     * ........#.
     * #.........
     * ......#...
     * </pre>
     * <p>
     * This process continues for a while, but the guard eventually leaves the mapped area (after walking past a tank of universal solvent):
     * <pre>
     * ....#.....
     * .........#
     * ..........
     * ..#.......
     * .......#..
     * ..........
     * .#........
     * ........#.
     * #.........
     * ......#v..
     * </pre>
     * <p>
     * By predicting the guard's route, you can determine which specific positions in the lab will be in the patrol path.
     * <em>Including the guard's starting position</em>, the positions visited by the guard before leaving the area are marked with an <code>X</code>:
     * <pre>
     * ....#.....
     * ....XXXXX#
     * ....X...X.
     * ..#.X...X.
     * ..XXXXX#X.
     * ..X.X.X.X.
     * .#XXXXXXX.
     * .XXXXXXX#.
     * #XXXXXXX..
     * ......#X..
     * </pre>
     * <p>
     * In this example, the guard will visit <code><em>41</em></code> distinct positions on your map.
     * <p>
     * Predict the path of the guard.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many distinct positions will the guard visit before leaving the mapped area?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        return traceGuardRoute(context.readGrid());
    }

    /**
     * While The Historians begin working around the guard's patrol route, you borrow their fancy device and step outside the lab.
     * From the safety of a supply closet, you time travel through the last few months and <a href="/2018/day/4">record</a> the nightly status of the lab's guard post on the walls of the closet.
     * <p>
     * Returning after what seems like only a few seconds to The Historians, they explain that the guard's patrol area is simply too large for them to safely search the lab without getting caught.
     * <p>
     * Fortunately, they are <em>pretty sure</em> that adding a single new obstruction <em>won't</em> cause a time paradox.
     * They'd like to place the new obstruction in such a way that the guard will get <span title="This vulnerability was later fixed by having the guard always turn left instead."><em>stuck in a loop</em></span>, making the rest of the lab safe to search.
     * <p>
     * To have the lowest chance of creating a time paradox, The Historians would like to know <em>all</em> of the possible positions for such an obstruction.
     * The new obstruction can't be placed at the guard's starting position - the guard is there right now and would notice.
     * <p>
     * In the above example, there are only <code><em>6</em></code> different positions where a new obstruction would cause the guard to get stuck in a loop.
     * The diagrams of these six situations use <code>O</code> to mark the new obstruction, <code>|</code> to show a position where the guard moves up/down, <code>-</code> to show a position where the guard moves left/right, and <code>+</code> to show a position where the guard moves both up/down and left/right.
     * <p>
     * Option one, put a printing press next to the guard's starting position:
     * <pre>
     * ....#.....
     * ....+---+#
     * ....|...|.
     * ..#.|...|.
     * ....|..#|.
     * ....|...|.
     * .#.<em>O</em>^---+.
     * ........#.
     * #.........
     * ......#...
     * </pre>
     * <p>
     * Option two, put a stack of failed suit prototypes in the bottom right quadrant of the mapped area:
     *
     * <pre>
     * ....#.....
     * ....+---+#
     * ....|...|.
     * ..#.|...|.
     * ..+-+-+#|.
     * ..|.|.|.|.
     * .#+-^-+-+.
     * ......<em>O</em>.#.
     * #.........
     * ......#...
     * </pre>
     * <p>
     * Option three, put a crate of chimney-squeeze prototype fabric next to the standing desk in the bottom right quadrant:
     * <pre>
     * ....#.....
     * ....+---+#
     * ....|...|.
     * ..#.|...|.
     * ..+-+-+#|.
     * ..|.|.|.|.
     * .#+-^-+-+.
     * .+----+<em>O</em>#.
     * #+----+...
     * ......#...
     * </pre>
     * <p>
     * Option four, put an alchemical retroencabulator near the bottom left corner:
     * <pre>
     * ....#.....
     * ....+---+#
     * ....|...|.
     * ..#.|...|.
     * ..+-+-+#|.
     * ..|.|.|.|.
     * .#+-^-+-+.
     * ..|...|.#.
     * #<em>O</em>+---+...
     * ......#...
     * </pre>
     * <p>
     * Option five, put the alchemical retroencabulator a bit to the right instead:
     * <pre>
     * ....#.....
     * ....+---+#
     * ....|...|.
     * ..#.|...|.
     * ..+-+-+#|.
     * ..|.|.|.|.
     * .#+-^-+-+.
     * ....|.|.#.
     * #..<em>O</em>+-+...
     * ......#...
     * </pre>
     * <p>
     * Option six, put a tank of sovereign glue right next to the tank of universal solvent:
     * <pre>
     * ....#.....
     * ....+---+#
     * ....|...|.
     * ..#.|...|.
     * ..+-+-+#|.
     * ..|.|.|.|.
     * .#+-^-+-+.
     * .+----++#.
     * #+----++..
     * ......#<em>O</em>..
     * </pre>
     * <p>
     * It doesn't really matter what you choose to use as an obstacle so long as you and The Historians can put it into position without the guard noticing.
     * The important thing is having enough options that you can find one that minimizes time paradoxes, and in this example, there are <code><em>6</em></code> different positions you could choose.
     * <p>
     * You need to get the guard stuck in a loop by adding a single new obstruction.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many different positions could you choose for this obstruction?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        return calculatePossibleObstacleLocations(context.readGrid())
                .getAnswer2();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        return calculatePossibleObstacleLocations(context.readGrid());
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Trace the guard's route around the area.
     */
    private static int traceGuardRoute(final Grid grid) {
        final Coordinate start = findStartPosition(grid);

        final Set<Coordinate> visited = new HashSet<>();
        final ExitReason exitReason = traceGuardRouteFrom(grid, start, Direction.UP, null, visited::add);
        if (exitReason == ExitReason.LOOP)
            throw new IllegalStateException("Should not detect a loop when finding guard's path");

        return visited.size();
    }

    /*
     * Trace the guard's route around the area, calculating the possible
     * locations at which an obstacle could be placed to cause the path to
     * form a loop.
     */
    private static IntTuple calculatePossibleObstacleLocations(final Grid grid) {
        Coordinate current = findStartPosition(grid);
        Direction facing = Direction.UP;
        final Set<Coordinate> obstacles = new HashSet<>();
        final Set<Coordinate> notObstacles = new HashSet<>();
        final Set<Coordinate> path = new HashSet<>();
        do {
            path.add(current); // Should never place an obstacle where the guard has already walked

            final Coordinate next = current.adjustBy(facing);
            if (!grid.contains(next)) break;

            final int nextVal = grid.get(next);
            if (nextVal == '#') {
                facing = facing.rotateClockwise();
                continue;
            }

            final boolean tryToBlockPath = !path.contains(next)
                    && !obstacles.contains(next)
                    && !notObstacles.contains(next);

            if (tryToBlockPath) {
                final ExitReason newPathExitReason =
                        traceGuardRouteFrom(grid, current, facing, next, c -> {});

                if (newPathExitReason == ExitReason.LOOP) obstacles.add(next);
                else notObstacles.add(next);
            }

            current = next;
        } while (grid.contains(current));

        return new IntTuple(path.size(), obstacles.size());
    }

    /*
     * Find the starting position.
     */
    private static Coordinate findStartPosition(final Grid grid) {
        return grid.stream()
                .filter(c -> grid.get(c) == '^')
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Failed to find the guard's starting position."));
    }

    /*
     * Trace the guard's route around the area, starting from a certain
     * location and heading in a particular direction.
     */
    private static ExitReason traceGuardRouteFrom(
            final Grid grid,
            final Coordinate start,
            final Direction startingDirection,
            final Coordinate extraObstacle,
            final Consumer<Coordinate> onProgress
    ) {
        Coordinate current = start;
        Direction facing = startingDirection;
        final Set<VisitedSquare> visited = new HashSet<>();
        do {
            if (!visited.add(new VisitedSquare(current, facing)))
                return ExitReason.LOOP;

            onProgress.accept(current);

            final Coordinate next = current.adjustBy(facing);
            if (!grid.contains(next)) break;

            final int nextVal = extraObstacle != null && extraObstacle.equals(next)
                    ? '#'
                    : grid.get(next);
            if (nextVal == '#') {
                facing = facing.rotateClockwise();
                continue;
            }

            current = next;
        } while (grid.contains(current));

        return ExitReason.LEFT_AREA;
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A record of which squares the guard has visited, along with which
     * direction they were travelling at the time.
     */
    private record VisitedSquare(Coordinate pos, Direction direction) {

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (o == null || getClass() != o.getClass()) return false;
            final VisitedSquare that = (VisitedSquare) o;
            return Objects.equals(pos, that.pos) && direction == that.direction;
        }

        @Override
        public int hashCode() {
            return Objects.hash(pos, direction);
        }

    }

    /*
     * The reason that following the guard's path was terminated.
     */
    private enum ExitReason {

        /*
         * The guard left the grid.
         */
        LEFT_AREA,

        /*
         * The guard entered a loop.
         */
        LOOP,

        // end of constants
        ;
    }

}

