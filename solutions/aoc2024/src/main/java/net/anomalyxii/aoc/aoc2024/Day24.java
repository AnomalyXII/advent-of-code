package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 24: Crossed Wires.
 */
@Solution(year = 2024, day = 24, title = "Crossed Wires")
public class Day24 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You and The Historians arrive at the edge of a <a href="/2022/day/23">large grove</a> somewhere in the jungle.
     * After the last incident, the Elves installed a small device that monitors the fruit.
     * While The Historians search the grove, one of them asks if you can take a look at the monitoring device; apparently, it's been malfunctioning recently.
     * <p>
     * The device seems to be trying to produce a number through some boolean logic gates.
     * Each gate has two inputs and one output.
     * The gates all operate on values that are either <em>true</em> (<code>1</code>) or <em>false</em> (<code>0</code>).
     * <ul>
     * <li><code>AND</code> gates output <code>1</code> if <em>both</em> inputs are <code>1</code>; if either input is <code>0</code>, these gates output <code>0</code>.</li>
     * <li><code>OR</code> gates output <code>1</code> if <em>one or both</em> inputs is <code>1</code>; if both inputs are <code>0</code>, these gates output <code>0</code>.</li>
     * <li><code>XOR</code> gates output <code>1</code> if the inputs are <em>different</em>; if the inputs are the same, these gates output <code>0</code>.</li>
     * </ul>
     * <p>
     * Gates wait until both inputs are received before producing output; wires can carry <code>0</code>, <code>1</code> or no value at all.
     * There are no loops; once a gate has determined its output, the output will not change until the whole system is reset.
     * Each wire is connected to at most one gate output, but can be connected to many gate inputs.
     * <p>
     * Rather than risk getting shocked while tinkering with the live system, you write down all of the gate connections and initial wire values (your puzzle input) so you can consider them in relative safety.
     * For example:
     * <pre>
     * x00: 1
     * x01: 1
     * x02: 1
     * y00: 0
     * y01: 1
     * y02: 0
     *
     * x00 AND y00 -&gt; z00
     * x01 XOR y01 -&gt; z01
     * x02 OR y02 -&gt; z02
     * </pre>
     * <p>
     * Because gates wait for input, some wires need to start with a value (as inputs to the entire system).
     * The first section specifies these values.
     * For example, <code>x00: 1</code> means that the wire named <code>x00</code> starts with the value <code>1</code> (as if a gate is already outputting that value onto that wire).
     * <p>
     * The second section lists all of the gates and the wires connected to them.
     * For example, <code>x00 AND y00 -&gt; z00</code> describes an instance of an <code>AND</code> gate which has wires <code>x00</code> and <code>y00</code> connected to its inputs and which will write its output to wire <code>z00</code>.
     * <p>
     * In this example, simulating these gates eventually causes <code>0</code> to appear on wire <code>z00</code>, <code>0</code> to appear on wire <code>z01</code>, and <code>1</code> to appear on wire <code>z02</code>.
     * <p>
     * Ultimately, the system is trying to produce a <em>number</em> by combining the bits on all wires starting with <code>z</code>.
     * <code>z00</code> is the least significant bit, then <code>z01</code>, then <code>z02</code>, and so on.
     * <p>
     * In this example, the three output bits form the binary number <code>100</code> which is equal to the decimal number <code><em>4</em></code>.
     * <p>
     * Here's a larger example:
     * <pre>
     * x00: 1
     * x01: 0
     * x02: 1
     * x03: 1
     * x04: 0
     * y00: 1
     * y01: 1
     * y02: 1
     * y03: 1
     * y04: 1
     *
     * ntg XOR fgs -> mjb
     * y02 OR x01 -> tnw
     * kwq OR kpj -> z05
     * x00 OR x03 -> fst
     * tgd XOR rvg -> z01
     * vdt OR tnw -> bfw
     * bfw AND frj -> z10
     * ffh OR nrd -> bqk
     * y00 AND y03 -> djm
     * y03 OR y00 -> psh
     * bqk OR frj -> z08
     * tnw OR fst -> frj
     * gnj AND tgd -> z11
     * bfw XOR mjb -> z00
     * x03 OR x00 -> vdt
     * gnj AND wpb -> z02
     * x04 AND y00 -> kjc
     * djm OR pbm -> qhw
     * nrd AND vdt -> hwm
     * kjc AND fst -> rvg
     * y04 OR y02 -> fgs
     * y01 AND x02 -> pbm
     * ntg OR kjc -> kwq
     * psh XOR fgs -> tgd
     * qhw XOR tgd -> z09
     * pbm OR djm -> kpj
     * x03 XOR y03 -> ffh
     * x00 XOR y04 -> ntg
     * bfw OR bqk -> z06
     * nrd XOR fgs -> wpb
     * frj XOR qhw -> z04
     * bqk OR frj -> z07
     * y03 OR x01 -> nrd
     * hwm AND bqk -> z03
     * tgd XOR rvg -> z12
     * tnw OR pbm -> gnj
     * </pre>
     * <p>
     * After waiting for values on all wires starting with <code>z</code>, the wires in this system have the following values:
     * <pre>
     * bfw: 1
     * bqk: 1
     * djm: 1
     * ffh: 0
     * fgs: 1
     * frj: 1
     * fst: 1
     * gnj: 1
     * hwm: 1
     * kjc: 0
     * kpj: 1
     * kwq: 0
     * mjb: 1
     * nrd: 1
     * ntg: 0
     * pbm: 1
     * psh: 1
     * qhw: 1
     * rvg: 0
     * tgd: 0
     * tnw: 1
     * vdt: 1
     * wpb: 0
     * z00: 0
     * z01: 0
     * z02: 0
     * z03: 1
     * z04: 0
     * z05: 1
     * z06: 1
     * z07: 1
     * z08: 1
     * z09: 1
     * z10: 1
     * z11: 0
     * z12: 0
     * </pre>
     * <p>
     * Combining the bits from all wires starting with <code>z</code> produces the binary number <code>0011111101000</code>.
     * Converting this number to decimal produces <code><em>2024</em></code>.
     * <p>
     * Simulate the system of gates and wires.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What decimal number does it output on the wires starting with <code>z</code>?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<List<String>> batches = context.readBatches();
        final Map<String, Wire> wires = batches.getFirst().stream()
                .map(Wire::parse)
                .collect(Collectors.toMap(w -> w.id, w -> w));

        final Set<Gate> gates = batches.getLast().stream()
                .map(line -> Gate.parse(line, wires))
                .collect(Collectors.toSet());

        int remaining = gates.size();
        while (!gates.isEmpty()) {
            final Iterator<Gate> it = gates.iterator();
            while (it.hasNext()) {
                final Gate gate = it.next();
                if (!gate.canResolve()) continue;
                gate.apply();
                it.remove();
            }

            assert gates.size() != remaining : "Looped but did not resolve any further wires...";
            remaining = gates.size();
        }

        return reconstructNumberFromBits(wires, "z");
    }

    /**
     * After inspecting the monitoring device more closely, you determine that the system you're simulating is trying to <em>add two binary numbers</em>.
     * <p>
     * Specifically, it is treating the bits on wires starting with <code>x</code> as one binary number, treating the bits on wires starting with <code>y</code> as a second binary number, and then attempting to add those two numbers together.
     * The output of this operation is produced as a binary number on the wires starting with <code>z</code>.
     * (In all three cases, wire <code>00</code> is the least significant bit, then <code>01</code>, then <code>02</code>, and so on.)
     * <p>
     * The initial values for the wires in your puzzle input represent <em>just one instance</em> of a pair of numbers that sum to the wrong value.
     * Ultimately, <em>any</em> two binary numbers provided as input should be handled correctly.
     * That is, for any combination of bits on wires starting with <code>x</code> and wires starting with <code>y</code>, the sum of the two numbers those bits represent should be produced as a binary number on the wires starting with <code>z</code>.
     * <p>
     * For example, if you have an addition system with four <code>x</code> wires, four <code>y</code> wires, and five <code>z</code> wires, you should be able to supply any four-bit number on the <code>x</code> wires, any four-bit number on the <code>y</code> numbers, and eventually find the sum of those two numbers as a five-bit number on the <code>z</code> wires.
     * <p>
     * One of the many ways you could provide numbers to such a system would be to pass <code>11</code> on the <code>x</code> wires (<code>1011</code> in binary) and <code>13</code> on the <code>y</code> wires (<code>1101</code> in binary):
     * <pre>
     * x00: 1
     * x01: 1
     * x02: 0
     * x03: 1
     * y00: 1
     * y01: 0
     * y02: 1
     * y03: 1
     * </pre>
     * <p>
     * If the system were working correctly, then after all gates are finished processing, you should find <code>24</code> (<code>11+13</code>) on the <code>z</code> wires as the five-bit binary number <code>11000</code>:
     * <pre>
     * z00: 0
     * z01: 0
     * z02: 0
     * z03: 1
     * z04: 1
     * </pre>
     * <p>
     * Unfortunately, your actual system needs to add numbers with many more bits and therefore has many more wires.
     * <p>
     * Based on <span title="ENHANCE">forensic analysis</span> of scuff marks and scratches on the device, you can tell that there are exactly <em>four</em> pairs of gates whose output wires have been <em>swapped</em>.
     * (A gate can only be in at most one such pair; no gate's output was swapped multiple times.)
     * <p>
     * For example, the system below is supposed to find the bitwise <code>AND</code> of the six-bit number on <code>x00</code> through <code>x05</code> and the six-bit number on <code>y00</code> through <code>y05</code> and then write the result as a six-bit number on <code>z00</code> through <code>z05</code>:
     * <pre>
     * x00: 0
     * x01: 1
     * x02: 0
     * x03: 1
     * x04: 0
     * x05: 1
     * y00: 0
     * y01: 0
     * y02: 1
     * y03: 1
     * y04: 0
     * y05: 1
     *
     * x00 AND y00 -> z05
     * x01 AND y01 -> z02
     * x02 AND y02 -> z01
     * x03 AND y03 -> z03
     * x04 AND y04 -> z04
     * x05 AND y05 -> z00
     * </pre>
     * <p>
     * However, in this example, two pairs of gates have had their output wires swapped, causing the system to produce wrong answers.
     * The first pair of gates with swapped outputs is <code>x00 AND y00 -> z05</code> and <code>x05 AND y05 -> z00</code>; the second pair of gates is <code>x01 AND y01 -> z02</code> and <code>x02 AND y02 -> z01</code>.
     * Correcting these two swaps results in this system that works as intended for any set of initial values on wires that start with <code>x</code> or <code>y</code>:
     * <pre>
     * x00 AND y00 -> z00
     * x01 AND y01 -> z01
     * x02 AND y02 -> z02
     * x03 AND y03 -> z03
     * x04 AND y04 -> z04
     * x05 AND y05 -> z05
     * </pre>
     * <p>
     * In this example, two pairs of gates have outputs that are involved in a swap.
     * By sorting their output wires' names and joining them with commas, the list of wires involved in swaps is <code><em>z00,z01,z02,z05</em></code>.
     * <p>
     * Of course, your actual system is much more complex than this, and the gates that need their outputs swapped could be <em>anywhere</em>, not just attached to a wire starting with <code>z</code>.
     * If you were to determine that you need to swap output wires <code>aaa</code> with <code>eee</code>, <code>ooo</code> with <code>z99</code>, <code>bbb</code> with <code>ccc</code>, and <code>aoc</code> with <code>z24</code>, your answer would be <code><em>aaa,aoc,bbb,ccc,eee,ooo,z24,z99</em></code>.
     * <p>
     * Your system of gates and wires has <em>four</em> pairs of gates which need their output wires swapped - <em>eight</em> wires in total.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Determine which four pairs of gates need their outputs swapped so that your system correctly performs addition; what do you get if you sort the names of the eight wires involved in a swap and then join those names with commas?
     */
    @Part(part = II)
    public String calculateAnswerForPart2(final SolutionContext context) {
        final List<List<String>> batches = context.readBatches();
        final Map<String, Wire> wires = batches.getFirst().stream()
                .map(Wire::parse)
                .collect(Collectors.toMap(w -> w.id, w -> w));

        final Set<Gate> gates = batches.getLast().stream()
                .map(line -> Gate.parse(line, wires))
                .collect(Collectors.toSet());


        return findSwapsInAReallyDodgyWayThatProbablyIsNotGeneric(gates, wires);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Format an iterate number as a `x`-wire ID.
     */
    private static String x(final int n) {
        return "x%02d".formatted(n);
    }

    /*
     * Format an iterate number as a `y`-wire ID.
     */
    private static String y(final int n) {
        return "y%02d".formatted(n);
    }

    /*
     * Reconstruct the output number from the `z`-wire bits.
     */
    private static long reconstructNumberFromBits(final Map<String, Wire> wires, final String prefix) {
        return wires.entrySet().stream()
                .filter(e -> e.getKey().startsWith(prefix))
                .sorted(Map.Entry.<String, Wire>comparingByKey().reversed())
                .map(Map.Entry::getValue)
                .peek(w -> {
                    assert w.value != null;
                })
                .reduce(
                        0L,
                        (r, w) -> (r << 1) | (w.value ? 1 : 0),
                        (a, b) -> {
                            throw new IllegalArgumentException("Should not merge!");
                        }
                );
    }

    /*
     * Construct a `Set` of wire IDs that must be swapped to get the adder to
     * function correctly.
     *
     * This "works on my machine", but I doubt it'll work for other inputs :(
     */
    private static String findSwapsInAReallyDodgyWayThatProbablyIsNotGeneric(
            final Set<Gate> gates,
            final Map<String, Wire> wires
    ) {
        int maxXY = 0;
        while (wires.containsKey(x(maxXY)) && wires.containsKey(y(maxXY))) ++maxXY;


        final Map<Inputs, Gate> gatesByInputs = gates.stream()
                .collect(Collectors.toMap(
                        Gate::inputs,
                        Function.identity()
                ));

        // For n0 and n1, there should be a pattern of
        //  => xn1 ^ yn1 -> an1    // add x and y
        //  => xn1 & yn1 -> bn1    // carry x and y
        //  => an1 ^ dn0 -> zn1    // add prev carry
        //  => an1 & dn0 -> cn1    // carry prev carry
        //  => bn1 | cn1 -> dn1    // resolve new carry

        final Set<String> swaps = new HashSet<>();
        for (int n = 0; n < maxXY; n++) {
            final Wire xn1 = wires.get(x(n));
            final Wire yn1 = wires.get(y(n));
            assert xn1 != null : "Did not find Wire for " + x(n);
            assert yn1 != null : "Did not find Wire for " + y(n);

            //  => xn1 ^ yn1 -> an1
            //  => xn1 & yn1 -> bn1
            final Gate an1 = gatesByInputs.get(Inputs.of(xn1, yn1, Operation.XOR));
            final Gate bn1 = gatesByInputs.get(Inputs.of(xn1, yn1, Operation.AND));
            assert an1 != null : "Did not find " + xn1 + " XOR " + yn1;
            assert bn1 != null : "Did not find " + xn1 + " AND " + yn1;

            if (n == 0) {
                final boolean an1Valid = an1.out.id.startsWith("z");
                final boolean bn1Valid = !bn1.out.id.startsWith("z");
                assert an1Valid && bn1Valid : "an1 and bn1 should always be valid for n=0...";
                continue;
            }

            final boolean an1Valid = !an1.out.id.startsWith("z");
            assert an1Valid : "an1 should always be valid for n=" + n + "!";

            // Find an XOR with either an1 or bn1
            final Gate[] zn1s = gates.stream()
                    .filter(g -> g.hasAnyInputAndOp(Operation.XOR, an1.out, bn1.out))
                    .toArray(Gate[]::new);
            assert zn1s.length == 1;
            final Gate zn1 = zn1s[0];
            // Find an AND with either an1 or bn1
            final Gate[] cn1s = gates.stream()
                    .filter(g -> g.hasAnyInputAndOp(Operation.AND, an1.out, bn1.out))
                    .toArray(Gate[]::new);
            assert cn1s.length == 1;
            final Gate cn1 = cn1s[0];
            // Find an OR with either bn1, cn1 or zn1
            final Gate[] dn1s = gates.stream()
                    .filter(g -> g.hasAnyInputAndOp(Operation.OR, bn1.out, cn1.out, zn1.out))
                    .toArray(Gate[]::new);
            assert dn1s.length == 1;
            final Gate dn1 = dn1s[0];

            if (!zn1.out.id.startsWith("z")) {
                final boolean az = an1.out.id.startsWith("z");
                final boolean bz = bn1.out.id.startsWith("z");
                final boolean cz = cn1.out.id.startsWith("z");
                final boolean dz = dn1.out.id.startsWith("z");
                assert az || bz || cz || dz : "if zn1 does not output to a z-wire, something else should...";

                // Should swap an1 and zn1...
                if (az) {
                    assert !bz && !cz && !dz : "if an1 outputs to a z-wire, no other wire should...";
                    swaps.add(an1.out.id);
                    swaps.add(zn1.out.id);
                }

                if (bz) {
                    assert !az && !cz && !dz : "if bn1 outputs to a z-wire, no other wire should...";
                    swaps.add(bn1.out.id);
                    swaps.add(zn1.out.id);
                }

                if (cz) {
                    assert !az && !bz && !dz : "if cn1 outputs to a z-wire, no other wire should...";
                    swaps.add(cn1.out.id);
                    swaps.add(zn1.out.id);
                }

                if (dz) {
                    assert !az && !bz && !cz : "if dn1 outputs to a z-wire, no other wire should...";
                    swaps.add(dn1.out.id);
                    swaps.add(zn1.out.id);
                }
            }

            // Find the common input between zn1 and cn1
            final Wire[] dn0s = Stream.of(zn1.inputs.in1, zn1.inputs.in2, cn1.inputs.in1, cn1.inputs.in2, dn1.inputs.in1, dn1.inputs.in2)
                    .distinct()
                    .filter(w -> w != an1.out && w != bn1.out)
                    .filter(w -> zn1.hasInput(w) && cn1.hasInput(w))
                    .toArray(Wire[]::new);
            assert dn0s.length == 1;
            final Wire dn0 = dn0s[0];
            assert dn0 != null : "Did not find a common wire...";

            // Check that the dn0 wire is at least correct for zn1 and cn1...
            assert zn1.hasInput(dn0) : "zn1 should be an XOR with dn0...";
            assert cn1.hasInput(dn0) : "cn1 should be an AND with dn0...";

            if (!zn1.hasInput(an1.out)) {
                // Maybe an1 and bn1 have been swapped, otherwise, wat??
                assert zn1.hasInput(bn1.out);
                swaps.add(an1.out.id);
                swaps.add(bn1.out.id);
            }

            if (!cn1.hasInput(an1.out)) { // This check is mostly redundant, but here for completeness?
                // Maybe an1 and bn1 have been swapped, otherwise, wat??
                assert cn1.hasInput(bn1.out);
                swaps.add(an1.out.id);
                swaps.add(bn1.out.id);
            }

            // Check that the cn1 wire actually feeds into dn1...
            assert dn1.hasInput(cn1.out) || dn1.hasInput(bn1.out) : "dn1 should be an OR with bn1 and cn1";

            if (dn1.hasInput(cn1.out)) {
                if (!dn1.hasInput(bn1.out)) {
                    // Maybe bn1 and an1 have been swapped?
                    if (dn1.hasInput(an1.out)) {
                        swaps.add(an1.out.id);
                        swaps.add(bn1.out.id);
                    }

                    // Or maybe bn1 and zn1 have been swapped?
                    if (dn1.hasInput(zn1.out)) {
                        swaps.add(zn1.out.id);
                        swaps.add(bn1.out.id);
                    }
                }
            } else {
                // Maybe cn1 and an1 have been swapped?
                if (dn1.hasInput(an1.out)) {
                    swaps.add(an1.out.id);
                    swaps.add(cn1.out.id);
                }

                // Or maybe cn1 and zn1 have been swapped?
                if (dn1.hasInput(zn1.out)) {
                    swaps.add(zn1.out.id);
                    swaps.add(cn1.out.id);
                }
            }
        }

        return swaps.stream()
                .sorted()
                .collect(Collectors.joining(","));
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Represents a wire.
     */
    private static class Wire {

        // Private Members

        private final String id;
        private Boolean value;

        // Constructors

        Wire(final String id) {
            this.id = id;
        }

        // Helper Methods

        /*
         * Check to see if this wire has a value.
         */
        boolean hasKnownValue() {
            return value != null;
        }

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (o == null || getClass() != o.getClass()) return false;
            final Wire wire = (Wire) o;
            return Objects.equals(id, wire.id);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(id);
        }

        // To String

        @Override
        public String toString() {
            return "Wire{"
                    + "id='" + id + '\''
                    + ", value=" + value
                    + '}';
        }

        // Static Helper Methods

        /*
         * Parse a `Wire` with an initial value.
         */
        static Wire parse(final String line) {
            final String[] parts = line.split(":\\s*");
            final Wire wire = new Wire(parts[0]);
            wire.value = parts[1].equals("1");
            return wire;
        }
    }

    /*
     * Represents an operation on two wires.
     */
    private enum Operation {

        /*
         * An AND gate.
         */
        AND {
            @Override
            boolean apply(final Wire w1, final Wire w2) {
                return w1.value && w2.value;
            }
        },

        /*
         * An OR gate.
         */
        OR {
            @Override
            boolean apply(final Wire w1, final Wire w2) {
                return w1.value || w2.value;
            }
        },

        /*
         * An XOR gate.
         */
        XOR {
            @Override
            boolean apply(final Wire w1, final Wire w2) {
                return w1.value ^ w2.value;
            }
        }

        // End of constants
        ;

        // Helper Methods

        /*
         * Apply this logic gate to the input `Wire`s.
         */
        abstract boolean apply(Wire w1, Wire w2);

    }

    /*
     * Represents the input to a logic gate.
     */
    private record Inputs(Wire in1, Operation operation, Wire in2) {

        // Helper Methods

        /*
         * Check if all of the `Wire`s in this `Input` have a value set.
         */
        boolean canResolve() {
            return in1.hasKnownValue() && in2.hasKnownValue();
        }

        /*
         * Apply the operation to the input `Wire`s.
         */
        public boolean apply() {
            assert in1.value != null;
            assert in2.value != null;
            return operation.apply(in1, in2);
        }

        /*
         * Check if the given `Wire` is one of the inputs.
         */
        public boolean hasInput(final Wire in) {
            return in1.equals(in) || in2.equals(in);
        }

        /*
         * Check if any of the given `Wire`s are one of the inputs.
         */
        public boolean hasAnyInput(final Wire... ins) {
            for (final Wire in : ins) {
                if (hasInput(in)) return true;
            }
            return false;
        }

        /*
         * Check if any of the given `Wire`s are one of the inputs, and that the
         * `Operation` matches the one given.
         */
        public boolean hasAnyInputAndOp(final Operation operation, final Wire... ins) {
            return this.operation == operation && hasAnyInput(ins);
        }

        // Static Helper Methods

        /*
         * Parse the `Inputs` from a line of "<wire> <op> <wire>".
         */
        private static Inputs parse(final String line, final Map<String, Wire> wires) {
            final String[] parts = line.split("\\s+");
            return Inputs.of(
                    wires.computeIfAbsent(parts[0], Wire::new),
                    wires.computeIfAbsent(parts[2], Wire::new),
                    switch (parts[1]) {
                        case "AND" -> Operation.AND;
                        case "OR" -> Operation.OR;
                        case "XOR" -> Operation.XOR;
                        default -> throw new IllegalArgumentException("Unsupported binary operation: " + parts[1]);
                    }
            );
        }

        /*
         * Create an `Inputs` from the given `Wire`s and `Operation`.
         */
        private static Inputs of(final Wire in1, final Wire in2, final Operation operation) {
            // Sort the wire inputs alphabetically, to make finding them easier
            final int cmp = String.CASE_INSENSITIVE_ORDER.compare(in1.id, in2.id);
            final Wire w1 = cmp < 0 ? in1 : in2;
            final Wire w2 = cmp < 0 ? in2 : in1;
            return new Inputs(w1, operation, w2);
        }
    }

    /*
     * Represents a logic gate.
     */
    private record Gate(Inputs inputs, Wire out) {

        // Helper Methods

        /*
         * Check if all the `Wire`s that feed into this `Gate` have a value set.
         */
        boolean canResolve() {
            return inputs.canResolve();
        }

        /*
         * Apply the operation to the input `Wire`s and assign the result to the
         * output `Wire`.
         */
        public void apply() {
            out.value = inputs.apply();
        }

        /*
         * Check if the given `Wire` is is an input to this `Gate`.
         */
        public boolean hasInput(final Wire in) {
            return inputs.hasInput(in);
        }

        /*
         * Check if any of the given `Wire`s are an input to this `Gate`.
         */
        public boolean hasAnyInput(final Wire... ins) {
            return inputs.hasAnyInput(ins);
        }

        /*
         * Check if any of the given `Wire`s are an input to this `Gate`, and
         * that the `Operation` matches the one given.
         */
        public boolean hasAnyInputAndOp(final Operation operation, final Wire... ins) {
            return inputs.hasAnyInputAndOp(operation, ins);
        }

        // Static Helper Methods

        /*
         * Parse a `Gate` from a line of "<wire> <op> <wire> -> <wire>".
         */
        static Gate parse(final String line, final Map<String, Wire> wires) {
            final String[] parts = line.split("\\s*->\\s*");
            final Inputs in = Inputs.parse(parts[0], wires);
            final String out = parts[1];

            return new Gate(in, wires.computeIfAbsent(out, Wire::new));
        }

    }

}

