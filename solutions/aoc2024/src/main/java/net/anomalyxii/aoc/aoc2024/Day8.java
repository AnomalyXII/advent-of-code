package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.result.LongTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;
import net.anomalyxii.aoc.utils.geometry.Velocity;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 8: Resonant Collinearity.
 */
@Solution(year = 2024, day = 8, title = "Resonant Collinearity")
public class Day8 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You find yourselves on the <a href="/2016/day/25">roof</a> of a top-secret Easter Bunny installation.
     * <p>
     * While The Historians do their thing, you take a look at the familiar <em>huge antenna</em>.
     * Much to your surprise, it seems to have been reconfigured to emit a signal that makes people 0.1% more likely to buy Easter Bunny brand <span title="They could have imitated delicious chocolate, but the mediocre chocolate is WAY easier to imitate.">Imitation Mediocre</span> Chocolate as a Christmas gift!
     * Unthinkable!
     * <p>
     * Scanning across the city, you find that there are actually many such antennas.
     * Each antenna is tuned to a specific <em>frequency</em> indicated by a single lowercase letter, uppercase letter, or digit.
     * You create a map (your puzzle input) of these antennas.
     * For example:
     * <pre>
     * ............
     * ........0...
     * .....0......
     * .......0....
     * ....0.......
     * ......A.....
     * ............
     * ............
     * ........A...
     * .........A..
     * ............
     * ............
     * </pre>
     * <p>
     * The signal only applies its nefarious effect at specific <em>antinodes</em> based on the resonant frequencies of the antennas.
     * In particular, an antinode occurs at any point that is perfectly in line with two antennas of the same frequency - but only when one of the antennas is twice as far away as the other.
     * This means that for any pair of antennas with the same frequency, there are two antinodes, one on either side of them.
     * <p>
     * So, for these two antennas with frequency <code>a</code>, they create the two antinodes marked with <code>#</code>:
     * <pre>
     * ..........
     * ...#......
     * ..........
     * ....a.....
     * ..........
     * .....a....
     * ..........
     * ......#...
     * ..........
     * ..........
     * </pre>
     * <p>
     * Adding a third antenna with the same frequency creates several more antinodes.
     * It would ideally add four antinodes, but two are off the right side of the map, so instead it adds only two:
     * <pre>
     * ..........
     * ...#......
     * #.........
     * ....a.....
     * ........a.
     * .....a....
     * ..#.......
     * ......#...
     * ..........
     * ..........
     * </pre>
     * <p>
     * Antennas with different frequencies don't create antinodes; <code>A</code> and <code>a</code> count as different frequencies.
     * However, antinodes <em>can</em> occur at locations that contain antennas.
     * In this diagram, the lone antenna with frequency capital <code>A</code> creates no antinodes but has a lowercase-<code>a</code>-frequency antinode at its location:
     * <pre>
     * ..........
     * ...#......
     * #.........
     * ....a.....
     * ........a.
     * .....a....
     * ..#.......
     * ......A...
     * ..........
     * ..........
     * </pre>
     * <p>
     * The first example has antennas with two different frequencies, so the antinodes they create look like this, plus an antinode overlapping the topmost <code>A</code>-frequency antenna:
     * <pre>
     * ......#....#
     * ...#....0...
     * ....#0....#.
     * ..#....0....
     * ....0....#..
     * .#....A.....
     * ...#........
     * #......#....
     * ........A...
     * .........A..
     * ..........#.
     * ..........#.
     * </pre>
     * <p>
     * Because the topmost <code>A</code>-frequency antenna overlaps with a <code>0</code>-frequency antinode, there are <code><em>14</em></code> total unique locations that contain an antinode within the bounds of the map.
     * <p>
     * Calculate the impact of the signal.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many unique locations within the bounds of the map contain an antinode?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final Grid grid = context.readGrid();
        final Map<Character, List<Coordinate>> antennaeByFrequency = findAntennae(grid);

        return (int) antennaeByFrequency.values().stream()
                .flatMap(antennae -> streamAntinodes(grid, antennae))
                .distinct()
                .count();
    }

    /**
     * Watching over your shoulder as you work, one of The Historians asks if you took the effects of resonant harmonics into your calculations.
     * <p>
     * Whoops!
     * <p>
     * After updating your model, it turns out that an antinode occurs at <em>any grid position</em> exactly in line with at least two antennas of the same frequency, regardless of distance.
     * This means that some of the new antinodes will occur at the position of each antenna (unless that antenna is the only one of its frequency).
     * <p>
     * So, these three <code>T</code>-frequency antennas now create many antinodes:
     * <pre>
     * T....#....
     * ...T......
     * .T....#...
     * .........#
     * ..#.......
     * ..........
     * ...#......
     * ..........
     * ....#.....
     * ..........
     * </pre>
     * <p>
     * In fact, the three <code>T</code>-frequency antennas are all exactly in line with two antennas, so they are all also antinodes!
     * This brings the total number of antinodes in the above example to <code><em>9</em></code>.
     * <p>
     * The original example now has <code><em>34</em></code> antinodes, including the antinodes that appear on every antenna:
     * <pre>
     * ##....#....#
     * .#.#....0...
     * ..#.#0....#.
     * ..##...0....
     * ....0....#..
     * .#...#A....#
     * ...#..#.....
     * #....#.#....
     * ..#.....A...
     * ....#....A..
     * .#........#.
     * ...#......##
     * </pre>
     * <p>
     * Calculate the impact of the signal using this updated model.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many unique locations within the bounds of the map contain an antinode?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Grid grid = context.readGrid();
        final Map<Character, List<Coordinate>> antennaeByFrequency = findAntennae(grid);

        return (int) antennaeByFrequency.values().stream()
                .flatMap(antennae -> streamAllAntinodes(grid, antennae))
                .distinct()
                .count();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final Grid grid = context.readGrid();
        final Map<Character, List<Coordinate>> antennaeByFrequency = findAntennae(grid);

        final Set<Coordinate> part1 = new HashSet<>();
        final Set<Coordinate> part2 = new HashSet<>();
        antennaeByFrequency.values()
                .forEach(antennae -> {
                    part1.addAll(streamAntinodes(grid, antennae).collect(Collectors.toSet()));
                    part2.addAll(streamAllAntinodes(grid, antennae).collect(Collectors.toSet()));
                });
        return new IntTuple(part1.size(), part2.size());
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Find all the antennae, grouping them by frequency.
     */
    private static Map<Character, List<Coordinate>> findAntennae(final Grid grid) {
        return grid.stream()
                .filter(c -> grid.get(c) != '.')
                .collect(Collectors.groupingBy(c -> (char) grid.get(c)));
    }

    /*
     * Stream the immediate antinodes of a single frequency.
     */
    private static Stream<Coordinate> streamAntinodes(final Grid grid, final List<Coordinate> antennae) {
        final Stream.Builder<Coordinate> antinodes = Stream.builder();
        for (int a1 = 0; a1 < antennae.size() - 1; a1++) {
            final Coordinate antenna1 = antennae.get(a1);
            for (int a2 = a1 + 1; a2 < antennae.size(); a2++) {
                final Coordinate antenna2 = antennae.get(a2);

                final Velocity difference1 = new Velocity(-(antenna2.x() - antenna1.x()), -(antenna2.y() - antenna1.y()));
                final Coordinate anti1 = antenna1.adjustBy(difference1);
                if (grid.contains(anti1)) antinodes.add(anti1);

                final Velocity difference2 = new Velocity(antenna2.x() - antenna1.x(), antenna2.y() - antenna1.y());
                final Coordinate anti2 = antenna2.adjustBy(difference2);
                if (grid.contains(anti2)) antinodes.add(anti2);
            }
        }
        return antinodes.build();
    }

    /*
     * Stream ALL the antinodes of a single frequency.
     */
    private static Stream<Coordinate> streamAllAntinodes(final Grid grid, final List<Coordinate> antennae) {
        final Stream.Builder<Coordinate> antinodes = Stream.builder();
        for (int a1 = 0; a1 < antennae.size() - 1; a1++) {
            final Coordinate antenna1 = antennae.get(a1);
            for (int a2 = a1 + 1; a2 < antennae.size(); a2++) {
                final Coordinate antenna2 = antennae.get(a2);

                final Velocity difference1 = new Velocity(-(antenna2.x() - antenna1.x()), -(antenna2.y() - antenna1.y()));
                Coordinate anti1 = antenna1;
                do {
                    antinodes.add(anti1);
                    anti1 = anti1.adjustBy(difference1);
                } while (grid.contains(anti1));

                final Velocity difference2 = new Velocity(antenna2.x() - antenna1.x(), antenna2.y() - antenna1.y());
                Coordinate anti2 = antenna2;
                do {
                    antinodes.add(anti2);
                    anti2 = anti2.adjustBy(difference2);
                } while (grid.contains(anti2));
            }
        }
        return antinodes.build();
    }

}

