package net.anomalyxii.aoc.aoc2024;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 21: Keypad Conundrum.
 */
@Solution(year = 2024, day = 21, title = "Keypad Conundrum")
public class Day21 {

    /*
     * A sensible number of robots to control (i.e. part 1).
     */
    private static final int SENSIBLE_NUMBER_OF_ROBOTS = 3;

    /*
     * A stub number of robots to control (i.e. part 2).
     */
    private static final int STUPID_NUMBER_OF_ROBOTS = 26;

    /*
     * Cache of num key mappings.
     */
    private static final char[][] NUM_KEYS;

    /*
     * Cache of navigation key mappings.
     */
    private static final char[][] DIR_KEYS;

    /*
     * Maximum offset for a cache key.
     */
    private static final int MAX_IDX = 144;

    static {
        NUM_KEYS = new char[MAX_IDX][];
        NUM_KEYS[0] = new char[]{'A'};
        NUM_KEYS[1] = new char[]{'^', '<', 'A'};
        NUM_KEYS[2] = new char[]{'^', 'A'};
        NUM_KEYS[3] = new char[]{'^', '>', 'A'};
        NUM_KEYS[4] = new char[]{'^', '^', '<', 'A'};
        NUM_KEYS[5] = new char[]{'^', '^', 'A'};
        NUM_KEYS[6] = new char[]{'^', '^', '>', 'A'};
        NUM_KEYS[7] = new char[]{'^', '^', '^', '<', 'A'};
        NUM_KEYS[8] = new char[]{'^', '^', '^', 'A'};
        NUM_KEYS[9] = new char[]{'^', '^', '^', '>', 'A'};
        NUM_KEYS[11] = new char[]{'>', 'A'};
        NUM_KEYS[12] = new char[]{'>', 'v', 'A'};
        NUM_KEYS[13] = new char[]{'A'};
        NUM_KEYS[14] = new char[]{'>', 'A'};
        NUM_KEYS[15] = new char[]{'>', '>', 'A'};
        NUM_KEYS[16] = new char[]{'^', 'A'};
        NUM_KEYS[17] = new char[]{'^', '>', 'A'};
        NUM_KEYS[18] = new char[]{'^', '>', '>', 'A'};
        NUM_KEYS[19] = new char[]{'^', '^', 'A'};
        NUM_KEYS[20] = new char[]{'^', '^', '>', 'A'};
        NUM_KEYS[21] = new char[]{'^', '^', '>', '>', 'A'};
        NUM_KEYS[23] = new char[]{'>', '>', 'v', 'A'};
        NUM_KEYS[24] = new char[]{'v', 'A'};
        NUM_KEYS[25] = new char[]{'<', 'A'};
        NUM_KEYS[26] = new char[]{'A'};
        NUM_KEYS[27] = new char[]{'>', 'A'};
        NUM_KEYS[28] = new char[]{'<', '^', 'A'};
        NUM_KEYS[29] = new char[]{'^', 'A'};
        NUM_KEYS[30] = new char[]{'^', '>', 'A'};
        NUM_KEYS[31] = new char[]{'<', '^', '^', 'A'};
        NUM_KEYS[32] = new char[]{'^', '^', 'A'};
        NUM_KEYS[33] = new char[]{'^', '^', '>', 'A'};
        NUM_KEYS[35] = new char[]{'v', '>', 'A'};
        NUM_KEYS[36] = new char[]{'<', 'v', 'A'};
        NUM_KEYS[37] = new char[]{'<', '<', 'A'};
        NUM_KEYS[38] = new char[]{'<', 'A'};
        NUM_KEYS[39] = new char[]{'A'};
        NUM_KEYS[40] = new char[]{'<', '<', '^', 'A'};
        NUM_KEYS[41] = new char[]{'<', '^', 'A'};
        NUM_KEYS[42] = new char[]{'^', 'A'};
        NUM_KEYS[43] = new char[]{'<', '<', '^', '^', 'A'};
        NUM_KEYS[44] = new char[]{'<', '^', '^', 'A'};
        NUM_KEYS[45] = new char[]{'^', '^', 'A'};
        NUM_KEYS[47] = new char[]{'v', 'A'};
        NUM_KEYS[48] = new char[]{'>', 'v', 'v', 'A'};
        NUM_KEYS[49] = new char[]{'v', 'A'};
        NUM_KEYS[50] = new char[]{'v', '>', 'A'};
        NUM_KEYS[51] = new char[]{'v', '>', '>', 'A'};
        NUM_KEYS[52] = new char[]{'A'};
        NUM_KEYS[53] = new char[]{'>', 'A'};
        NUM_KEYS[54] = new char[]{'>', '>', 'A'};
        NUM_KEYS[55] = new char[]{'^', 'A'};
        NUM_KEYS[56] = new char[]{'^', '>', 'A'};
        NUM_KEYS[57] = new char[]{'^', '>', '>', 'A'};
        NUM_KEYS[59] = new char[]{'>', '>', 'v', 'v', 'A'};
        NUM_KEYS[60] = new char[]{'v', 'v', 'A'};
        NUM_KEYS[61] = new char[]{'<', 'v', 'A'};
        NUM_KEYS[62] = new char[]{'v', 'A'};
        NUM_KEYS[63] = new char[]{'v', '>', 'A'};
        NUM_KEYS[64] = new char[]{'<', 'A'};
        NUM_KEYS[65] = new char[]{'A'};
        NUM_KEYS[66] = new char[]{'>', 'A'};
        NUM_KEYS[67] = new char[]{'<', '^', 'A'};
        NUM_KEYS[68] = new char[]{'^', 'A'};
        NUM_KEYS[69] = new char[]{'^', '>', 'A'};
        NUM_KEYS[71] = new char[]{'v', 'v', '>', 'A'};
        NUM_KEYS[72] = new char[]{'<', 'v', 'v', 'A'};
        NUM_KEYS[73] = new char[]{'<', '<', 'v', 'A'};
        NUM_KEYS[74] = new char[]{'<', 'v', 'A'};
        NUM_KEYS[75] = new char[]{'v', 'A'};
        NUM_KEYS[76] = new char[]{'<', '<', 'A'};
        NUM_KEYS[77] = new char[]{'<', 'A'};
        NUM_KEYS[78] = new char[]{'A'};
        NUM_KEYS[79] = new char[]{'<', '<', '^', 'A'};
        NUM_KEYS[80] = new char[]{'<', '^', 'A'};
        NUM_KEYS[81] = new char[]{'^', 'A'};
        NUM_KEYS[83] = new char[]{'v', 'v', 'A'};
        NUM_KEYS[84] = new char[]{'>', 'v', 'v', 'v', 'A'};
        NUM_KEYS[85] = new char[]{'v', 'v', 'A'};
        NUM_KEYS[86] = new char[]{'v', 'v', '>', 'A'};
        NUM_KEYS[87] = new char[]{'v', 'v', '>', '>', 'A'};
        NUM_KEYS[88] = new char[]{'v', 'A'};
        NUM_KEYS[89] = new char[]{'v', '>', 'A'};
        NUM_KEYS[90] = new char[]{'v', '>', '>', 'A'};
        NUM_KEYS[91] = new char[]{'A'};
        NUM_KEYS[92] = new char[]{'>', 'A'};
        NUM_KEYS[93] = new char[]{'>', '>', 'A'};
        NUM_KEYS[95] = new char[]{'>', '>', 'v', 'v', 'v', 'A'};
        NUM_KEYS[96] = new char[]{'v', 'v', 'v', 'A'};
        NUM_KEYS[97] = new char[]{'<', 'v', 'v', 'A'};
        NUM_KEYS[98] = new char[]{'v', 'v', 'A'};
        NUM_KEYS[99] = new char[]{'v', 'v', '>', 'A'};
        NUM_KEYS[100] = new char[]{'<', 'v', 'A'};
        NUM_KEYS[101] = new char[]{'v', 'A'};
        NUM_KEYS[102] = new char[]{'v', '>', 'A'};
        NUM_KEYS[103] = new char[]{'<', 'A'};
        NUM_KEYS[104] = new char[]{'A'};
        NUM_KEYS[105] = new char[]{'>', 'A'};
        NUM_KEYS[107] = new char[]{'v', 'v', 'v', '>', 'A'};
        NUM_KEYS[108] = new char[]{'<', 'v', 'v', 'v', 'A'};
        NUM_KEYS[109] = new char[]{'<', '<', 'v', 'v', 'A'};
        NUM_KEYS[110] = new char[]{'<', 'v', 'v', 'A'};
        NUM_KEYS[111] = new char[]{'v', 'v', 'A'};
        NUM_KEYS[112] = new char[]{'<', '<', 'v', 'A'};
        NUM_KEYS[113] = new char[]{'<', 'v', 'A'};
        NUM_KEYS[114] = new char[]{'v', 'A'};
        NUM_KEYS[115] = new char[]{'<', '<', 'A'};
        NUM_KEYS[116] = new char[]{'<', 'A'};
        NUM_KEYS[117] = new char[]{'A'};
        NUM_KEYS[119] = new char[]{'v', 'v', 'v', 'A'};
        NUM_KEYS[132] = new char[]{'<', 'A'};
        NUM_KEYS[133] = new char[]{'^', '<', '<', 'A'};
        NUM_KEYS[134] = new char[]{'<', '^', 'A'};
        NUM_KEYS[135] = new char[]{'^', 'A'};
        NUM_KEYS[136] = new char[]{'^', '^', '<', '<', 'A'};
        NUM_KEYS[137] = new char[]{'<', '^', '^', 'A'};
        NUM_KEYS[138] = new char[]{'^', '^', 'A'};
        NUM_KEYS[139] = new char[]{'^', '^', '^', '<', '<', 'A'};
        NUM_KEYS[140] = new char[]{'<', '^', '^', '^', 'A'};
        NUM_KEYS[141] = new char[]{'^', '^', '^', 'A'};
        NUM_KEYS[143] = new char[]{'A'};

        DIR_KEYS = new char[MAX_IDX][];
        DIR_KEYS[0] = new char[]{'A'};
        DIR_KEYS[1] = new char[]{'>', '^', 'A'};
        DIR_KEYS[2] = new char[]{'>', 'A'};
        DIR_KEYS[3] = new char[]{'>', '>', 'A'};
        DIR_KEYS[11] = new char[]{'>', '>', '^', 'A'};
        DIR_KEYS[12] = new char[]{'v', '<', 'A'};
        DIR_KEYS[13] = new char[]{'A'};
        DIR_KEYS[14] = new char[]{'v', 'A'};
        DIR_KEYS[15] = new char[]{'v', '>', 'A'};
        DIR_KEYS[23] = new char[]{'>', 'A'};
        DIR_KEYS[24] = new char[]{'<', 'A'};
        DIR_KEYS[25] = new char[]{'^', 'A'};
        DIR_KEYS[26] = new char[]{'A'};
        DIR_KEYS[27] = new char[]{'>', 'A'};
        DIR_KEYS[35] = new char[]{'^', '>', 'A'};
        DIR_KEYS[36] = new char[]{'<', '<', 'A'};
        DIR_KEYS[37] = new char[]{'<', '^', 'A'};
        DIR_KEYS[38] = new char[]{'<', 'A'};
        DIR_KEYS[39] = new char[]{'A'};
        DIR_KEYS[47] = new char[]{'^', 'A'};
        DIR_KEYS[132] = new char[]{'v', '<', '<', 'A'};
        DIR_KEYS[133] = new char[]{'<', 'A'};
        DIR_KEYS[134] = new char[]{'<', 'v', 'A'};
        DIR_KEYS[135] = new char[]{'v', 'A'};
        DIR_KEYS[143] = new char[]{'A'};
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * As you teleport onto Santa's <a href="/2019/day/25">Reindeer-class starship</a>, The Historians begin to panic: someone from their search party is <em>missing</em>.
     * A quick life-form scan by the ship's computer reveals that when the missing Historian teleported, he arrived in another part of the ship.
     * <p>
     * The door to that area is locked, but the computer can't open it; it can only be opened by <em>physically typing</em> the door codes (your puzzle input) on the numeric keypad on the door.
     * <p>
     * The numeric keypad has four rows of buttons: <code>789</code>, <code>456</code>, <code>123</code>, and finally an empty gap followed by <code>0A</code>.
     * Visually, they are arranged like this:
     * <pre>
     * +---+---+---+
     * | 7 | 8 | 9 |
     * +---+---+---+
     * | 4 | 5 | 6 |
     * +---+---+---+
     * | 1 | 2 | 3 |
     * +---+---+---+
     *     | 0 | A |
     *     +---+---+
     * </pre>
     * <p>
     * Unfortunately, the area outside the door is currently <em>depressurized</em> and nobody can go near the door.
     * A robot needs to be sent instead.
     * <p>
     * The robot has no problem navigating the ship and finding the numeric keypad, but it's not designed for button pushing: it can't be told to push a specific button directly.
     * Instead, it has a robotic arm that can be controlled remotely via a <em>directional keypad</em>.
     * <p>
     * The directional keypad has two rows of buttons: a gap / <code>^</code> (up) / <code>A</code> (activate) on the first row and <code>&lt;</code> (left) / <code>v</code> (down) / <code>&gt;</code> (right) on the second row.
     * Visually, they are arranged like this:
     * <pre>
     * +---+---+
     *     | ^ | A |
     * +---+---+---+
     * | &lt; | v | &gt; |
     * +---+---+---+
     * </pre>
     * <p>
     * When the robot arrives at the numeric keypad, its robotic arm is pointed at the <code>A</code> button in the bottom right corner.
     * After that, this directional keypad remote control must be used to maneuver the robotic arm: the up / down / left / right buttons cause it to move its arm one button in that direction, and the <code>A</code> button causes the robot to briefly move forward, pressing the button being aimed at by the robotic arm.
     * <p>
     * For example, to make the robot type <code>029A</code> on the numeric keypad, one sequence of inputs on the directional keypad you could use is:
     * <ul>
     * <li><code>&lt;</code> to move the arm from <code>A</code> (its initial position) to <code>0</code>.</li>
     * <li><code>A</code> to push the <code>0</code> button.</li>
     * <li><code>^A</code> to move the arm to the <code>2</code> button and push it.</li>
     * <li><code>&gt;^^A</code> to move the arm to the <code>9</code> button and push it.</li>
     * <li><code>vvvA</code> to move the arm to the <code>A</code> button and push it.</li>
     * </ul>
     * <p>
     * In total, there are three shortest possible sequences of button presses on this directional keypad that would cause the robot to type <code>029A</code>: <code>&lt;A^A&gt;^^AvvvA</code>, <code>&lt;A^A^&gt;^AvvvA</code>, and <code>&lt;A^A^^&gt;AvvvA</code>.
     * <p>
     * Unfortunately, the area containing this directional keypad remote control is currently experiencing <em>high levels of radiation</em> and nobody can go near it.
     * A robot needs to be sent instead.
     * <p>
     * When the robot arrives at the directional keypad, its robot arm is pointed at the <code>A</code> button in the upper right corner.
     * After that, a <em>second, different</em> directional keypad remote control is used to control this robot (in the same way as the first robot, except that this one is typing on a directional keypad instead of a numeric keypad).
     * <p>
     * There are multiple shortest possible sequences of directional keypad button presses that would cause this robot to tell the first robot to type <code>029A</code> on the door.
     * One such sequence is <code>v&lt;&lt;A&gt;&gt;^A&lt;A&gt;AvA&lt;^AA&gt;A&lt;vAAA&gt;^A</code>.
     * <p>
     * Unfortunately, the area containing this second directional keypad remote control is currently <em><code>-40</code> degrees</em>!
     * Another robot will need to be sent to type on that directional keypad, too.
     * <p>
     * There are many shortest possible sequences of directional keypad button presses that would cause this robot to tell the second robot to tell the first robot to eventually type <code>029A</code> on the door.
     * One such sequence is <code>&lt;vA&lt;AA&gt;&gt;^AvAA&lt;^A&gt;A&lt;v&lt;A&gt;&gt;^AvA^A&lt;vA&gt;^A&lt;v&lt;A&gt;^A&gt;AAvA^A&lt;v&lt;A&gt;A&gt;^AAAvA&lt;^A&gt;A</code>.
     * <p>
     * Unfortunately, the area containing this third directional keypad remote control is currently <em>full of Historians</em>, so no robots can find a clear path there.
     * Instead, <em>you</em> will have to type this sequence yourself.
     * <p>
     * Were you to choose this sequence of button presses, here are all of the buttons that would be pressed on your directional keypad, the two robots' directional keypads, and the numeric keypad:
     * <pre>
     * &lt;vA&lt;AA&gt;&gt;^AvAA&lt;^A&gt;A&lt;v&lt;A&gt;&gt;^AvA^A&lt;vA&gt;^A&lt;v&lt;A&gt;^A&gt;AAvA^A&lt;v&lt;A&gt;A&gt;^AAAvA&lt;^A&gt;A
     * v&lt;&lt;A&gt;&gt;^A&lt;A&gt;AvA&lt;^AA&gt;A&lt;vAAA&gt;^A
     * &lt;A^A&gt;^^AvvvA
     * 029A
     * </pre>
     * <p>
     * In summary, there are the following keypads:
     * <ul>
     * <li>One directional keypad that <em>you</em> are using.</li>
     * <li>Two directional keypads that <em>robots</em> are using.</li>
     * <li>One numeric keypad (on a door) that a <em>robot</em> is using.</li>
     * </ul>
     * <p>
     * It is important to remember that these robots are not designed for button pushing.
     * In particular, if a robot arm is ever aimed at a <em>gap</em> where no button is present on the keypad, even for an instant, the robot will <em>panic</em> unrecoverably.
     * So, don't do that.
     * All robots will initially aim at the keypad's <code>A</code> key, wherever it is.
     * <p>
     * To unlock the door, <em>five</em> codes will need to be typed on its numeric keypad.
     * For example:
     * <pre>
     * 029A
     * 980A
     * 179A
     * 456A
     * 379A
     * </pre>
     * <p>
     * For each of these, here is a shortest sequence of button presses you could type to cause the desired code to be typed on the numeric keypad:
     * <pre>
     * 029A: &lt;vA&lt;AA&gt;&gt;^AvAA&lt;^A&gt;A&lt;v&lt;A&gt;&gt;^AvA^A&lt;vA&gt;^A&lt;v&lt;A&gt;^A&gt;AAvA^A&lt;v&lt;A&gt;A&gt;^AAAvA&lt;^A&gt;A
     * 980A: &lt;v&lt;A&gt;&gt;^AAAvA^A&lt;vA&lt;AA&gt;&gt;^AvAA&lt;^A&gt;A&lt;v&lt;A&gt;A&gt;^AAAvA&lt;^A&gt;A&lt;vA&gt;^A&lt;A&gt;A
     * 179A: &lt;v&lt;A&gt;&gt;^A&lt;vA&lt;A&gt;&gt;^AAvAA&lt;^A&gt;A&lt;v&lt;A&gt;&gt;^AAvA^A&lt;vA&gt;^AA&lt;A&gt;A&lt;v&lt;A&gt;A&gt;^AAAvA&lt;^A&gt;A
     * 456A: &lt;v&lt;A&gt;&gt;^AA&lt;vA&lt;A&gt;&gt;^AAvAA&lt;^A&gt;A&lt;vA&gt;^A&lt;A&gt;A&lt;vA&gt;^A&lt;A&gt;A&lt;v&lt;A&gt;A&gt;^AAvA&lt;^A&gt;A
     * 379A: &lt;v&lt;A&gt;&gt;^AvA^A&lt;vA&lt;AA&gt;&gt;^AAvA&lt;^A&gt;AAvA^A&lt;vA&gt;^AA&lt;A&gt;A&lt;v&lt;A&gt;A&gt;^AAAvA&lt;^A&gt;A
     * </pre>
     * <p>
     * The Historians are getting nervous; the ship computer doesn't remember whether the missing Historian is trapped in the area containing a <em>giant electromagnet</em> or <em>molten lava</em>.
     * You'll need to make sure that for each of the five codes, you find the <em>shortest sequence</em> of button presses necessary.
     * <p>
     * The <em>complexity</em> of a single code (like <code>029A</code>) is equal to the result of multiplying these two values:
     * <ul>
     * <li>The <em>length of the shortest sequence</em> of button presses you need to type on your directional keypad in order to cause the code to be typed on the numeric keypad; for <code>029A</code>, this would be <code>68</code>.</li>
     * <li>The <em>numeric part of the code</em> (ignoring leading zeroes); for <code>029A</code>, this would be <code>29</code>.</li>
     * </ul>
     * <p>
     * In the above example, complexity of the five codes can be found by calculating <code>68 * 29</code>, <code>60 * 980</code>, <code>68 * 179</code>, <code>64 * 456</code>, and <code>64 * 379</code>.
     * Adding these together produces <code><em>126384</em></code>.
     * <p>
     * Find the fewest number of button presses you'll need to perform in order to cause the robot in front of the door to type each code.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the complexities of the five codes on your list?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final long[][] cache = new long[SENSIBLE_NUMBER_OF_ROBOTS][];
        return context.stream()
                .mapToLong(line -> calculateComplexity(line, SENSIBLE_NUMBER_OF_ROBOTS, cache))
                .sum();
    }

    /**
     * Just as the missing Historian is released, The Historians realize that a <em>second</em> member of their search party has also been missing <span title="bum bum BUUUUUM">this entire time</span>!
     * <p>
     * A quick life-form scan reveals the Historian is also trapped in a locked area of the ship.
     * Due to a variety of hazards, robots are once again dispatched, forming another chain of remote control keypads managing robotic-arm-wielding robots.
     * <p>
     * This time, many more robots are involved.
     * In summary, there are the following keypads:
     * <ul>
     * <li>One directional keypad that <em>you</em> are using.</li>
     * <li><em>25</em> directional keypads that <em>robots</em> are using.</li>
     * <li>One numeric keypad (on a door) that a <em>robot</em> is using.</li>
     * </ul>
     * <p>
     * The keypads form a chain, just like before: your directional keypad controls a robot which is typing on a directional keypad which controls a robot which is typing on a directional keypad...
     * and so on, ending with the robot which is typing on the numeric keypad.
     * <p>
     * The door codes are the same this time around; only the number of robots and directional keypads has changed.
     * <p>
     * Find the fewest number of button presses you'll need to perform in order to cause the robot in front of the door to type each code.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the complexities of the five codes on your list?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final long[][] cache = new long[STUPID_NUMBER_OF_ROBOTS][];
        return context.stream()
                .mapToLong(line -> calculateComplexity(line, STUPID_NUMBER_OF_ROBOTS, cache))
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final long[][] cache1 = new long[SENSIBLE_NUMBER_OF_ROBOTS][];
        final long[][] cache2 = new long[STUPID_NUMBER_OF_ROBOTS][];
        return context.stream()
                .reduce(
                        LongTuple.NULL,
                        (result, line) -> result.add(
                                calculateComplexity(line, SENSIBLE_NUMBER_OF_ROBOTS, cache1),
                                calculateComplexity(line, STUPID_NUMBER_OF_ROBOTS, cache2)
                        ),
                        LongTuple::add
                );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Calculate the complexity of a numeric key combination.
     */
    static long calculateComplexity(final String line, final int depth) {
        return calculateComplexity(line, depth, new long[depth][]);
    }

    /*
     * Calculate the complexity of a numeric key combination.
     */
    private static long calculateComplexity(final String line, final int depth, final long[][] cache) {
        final int numeric = Integer.parseInt(line.substring(0, line.length() - 1));
        final long length = calculateInputLength(line.toCharArray(), 0, depth, cache);
        return numeric * length;
    }

    /*
     * Calculate the number of keys needed to generate a button sequence.
     */
    private static long calculateInputLength(
            final char[] line,
            final int depth,
            final int maxDepth,
            final long[][] cache
    ) {
        if (depth == maxDepth) return line.length;

        final long[] depthCache = cache[depth] == null ? (cache[depth] = new long[MAX_IDX]) : cache[depth];

        char current = 'A';
        long length = 0;
        for (final char next : line) {
            final int cc = idx(current, next);
            assert cc < MAX_IDX;

            final long result = depthCache[cc] > 0
                    ? depthCache[cc]
                    : (depthCache[cc] = calculateInputLength(
                            (depth == 0 ? NUM_KEYS : DIR_KEYS)[cc],
                            depth + 1,
                            maxDepth,
                            cache
                    ));

            length += result;
            assert length > 0;
            current = next;
        }

        assert current == 'A';
        return length;
    }

    /*
     * Calculate a (hopefully) unique cache index for a key transition.
     */
    private static int idx(final char current, final char next) {
        final byte upper = offset(current);
        final byte lower = offset(next);
        return upper * 12 + lower;
    }

    /*
     * Calculate a (hopefully) unique cache offset for each key.
     */
    private static byte offset(final char current) {
        return switch (current) {
            case '0', '<' -> 0;
            case '1', '^' -> 1;
            case '2', 'v' -> 2;
            case '3', '>' -> 3;
            case '4' -> 4;
            case '5' -> 5;
            case '6' -> 6;
            case '7' -> 7;
            case '8' -> 8;
            case '9' -> 9;
            case 'A' -> 11;
            default -> throw new IllegalArgumentException("Invalid index: " + current);
        };
    }

}

