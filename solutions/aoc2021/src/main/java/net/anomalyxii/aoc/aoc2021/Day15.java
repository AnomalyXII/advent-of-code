package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.algorithms.Dijkstra;
import net.anomalyxii.aoc.utils.algorithms.ShortestPath;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 15: Chiton.
 */
@Solution(year = 2021, day = 15, title = "Chiton")
public class Day15 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You've almost reached the exit of the cave, but the walls are getting closer together.
     * Your submarine can barely still fit, though; the main problem is that the walls of the cave are covered in <a href="https://en.wikipedia.org/wiki/Chiton" target="_blank">chitons</a>, and it would be best not to bump any of them.
     * <p>
     * The cavern is large, but has a very low ceiling, restricting your motion to two dimensions.
     * The shape of the cavern resembles a square; a quick scan of chiton density produces a map of <i>risk level</i> throughout the cave (your puzzle input).
     * For example:
     * <pre>
     * 1163751742
     * 1381373672
     * 2136511328
     * 3694931569
     * 7463417111
     * 1319128137
     * 1359912421
     * 3125421639
     * 1293138521
     * 2311944581
     * </pre>
     * <p>
     * You start in the top left position, your destination is the bottom right position, and you <span title="Can't go diagonal until we can repair the caterpillar unit.
     * Could be the liquid helium or the superconductors.">cannot move diagonally</span>.
     * The number at each position is its <i>risk level</i>; to determine the total risk of an entire path, add up the risk levels of each position you <i>enter</i> (that is, don't count the risk level of your starting position unless you enter it; leaving it adds no risk to your total).
     * <p>
     * Your goal is to find a path with the <i>lowest total risk</i>.
     * In this example, a path with the lowest total risk is highlighted here:
     * <pre>
     * <i>1</i>163751742
     * <i>1</i>381373672
     * <i>2136511</i>328
     * 369493<i>15</i>69
     * 7463417<i>1</i>11
     * 1319128<i>13</i>7
     * 13599124<i>2</i>1
     * 31254216<i>3</i>9
     * 12931385<i>21</i>
     * 231194458<i>1</i>
     * </pre>
     * <p>
     * The total risk of this path is <code><i>40</i></code> (the starting position is never entered, so its risk is not counted).
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the lowest total risk of any path from the top left to the bottom right?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Grid chiterns = context.readGrid(chr -> chr - '0');
        return findShortestPath(chiterns);
    }

    /**
     * Now that you know how to find low-risk paths in the cave, you can try to find your way out.
     * <p>
     * The entire cave is actually <i>five times larger in both dimensions</i> than you thought; the area you originally scanned is just one tile in a 5x5 tile area that forms the full map.
     * Your original map tile repeats to the right and downward; each time the tile repeats to the right or downward, all of its risk levels <i>are 1 higher</i> than the tile immediately up or left of it.
     * However, risk levels above <code>9</code> wrap back around to <code>1</code>.
     * So, if your original map had some position with a risk level of <code>8</code>, then that same position on each of the 25 total tiles would be as follows:
     * <pre>
     * 8 9 1 2 3
     * 9 1 2 3 4
     * 1 2 3 4 5
     * 2 3 4 5 6
     * 3 4 5 6 7
     * </pre>
     * <p>
     * Each single digit above corresponds to the example position with a value of <code>8</code> on the top-left tile.
     * Because the full map is actually five times larger in both dimensions, that position appears a total of 25 times, once in each duplicated tile, with the values shown above.
     * <p>
     * Here is the full five-times-as-large version of the first example above, with the original map in the top left corner highlighted:
     * <pre>
     * <i>1163751742</i>2274862853338597396444961841755517295286
     * <i>1381373672</i>2492484783351359589446246169155735727126
     * <i>2136511328</i>3247622439435873354154698446526571955763
     * <i>3694931569</i>4715142671582625378269373648937148475914
     * <i>7463417111</i>8574528222968563933317967414442817852555
     * <i>1319128137</i>2421239248353234135946434524615754563572
     * <i>1359912421</i>2461123532357223464346833457545794456865
     * <i>3125421639</i>4236532741534764385264587549637569865174
     * <i>1293138521</i>2314249632342535174345364628545647573965
     * <i>2311944581</i>3422155692453326671356443778246755488935
     * 22748628533385973964449618417555172952866628316397
     * 24924847833513595894462461691557357271266846838237
     * 32476224394358733541546984465265719557637682166874
     * 47151426715826253782693736489371484759148259586125
     * 85745282229685639333179674144428178525553928963666
     * 24212392483532341359464345246157545635726865674683
     * 24611235323572234643468334575457944568656815567976
     * 42365327415347643852645875496375698651748671976285
     * 23142496323425351743453646285456475739656758684176
     * 34221556924533266713564437782467554889357866599146
     * 33859739644496184175551729528666283163977739427418
     * 35135958944624616915573572712668468382377957949348
     * 43587335415469844652657195576376821668748793277985
     * 58262537826937364893714847591482595861259361697236
     * 96856393331796741444281785255539289636664139174777
     * 35323413594643452461575456357268656746837976785794
     * 35722346434683345754579445686568155679767926678187
     * 53476438526458754963756986517486719762859782187396
     * 34253517434536462854564757396567586841767869795287
     * 45332667135644377824675548893578665991468977611257
     * 44961841755517295286662831639777394274188841538529
     * 46246169155735727126684683823779579493488168151459
     * 54698446526571955763768216687487932779859814388196
     * 69373648937148475914825958612593616972361472718347
     * 17967414442817852555392896366641391747775241285888
     * 46434524615754563572686567468379767857948187896815
     * 46833457545794456865681556797679266781878137789298
     * 64587549637569865174867197628597821873961893298417
     * 45364628545647573965675868417678697952878971816398
     * 56443778246755488935786659914689776112579188722368
     * 55172952866628316397773942741888415385299952649631
     * 57357271266846838237795794934881681514599279262561
     * 65719557637682166874879327798598143881961925499217
     * 71484759148259586125936169723614727183472583829458
     * 28178525553928963666413917477752412858886352396999
     * 57545635726865674683797678579481878968159298917926
     * 57944568656815567976792667818781377892989248891319
     * 75698651748671976285978218739618932984172914319528
     * 56475739656758684176786979528789718163989182927419
     * 67554889357866599146897761125791887223681299833479
     * </pre>
     * <p>
     * Equipped with the full map, you can now find a path from the top left corner to the bottom right corner with the lowest total risk:
     * <pre>
     * <i>1</i>1637517422274862853338597396444961841755517295286
     * <i>1</i>3813736722492484783351359589446246169155735727126
     * <i>2</i>1365113283247622439435873354154698446526571955763
     * <i>3</i>6949315694715142671582625378269373648937148475914
     * <i>7</i>4634171118574528222968563933317967414442817852555
     * <i>1</i>3191281372421239248353234135946434524615754563572
     * <i>1</i>3599124212461123532357223464346833457545794456865
     * <i>3</i>1254216394236532741534764385264587549637569865174
     * <i>1</i>2931385212314249632342535174345364628545647573965
     * <i>2</i>3119445813422155692453326671356443778246755488935
     * <i>2</i>2748628533385973964449618417555172952866628316397
     * <i>2</i>4924847833513595894462461691557357271266846838237
     * <i>324</i>76224394358733541546984465265719557637682166874
     * 47<i>15</i>1426715826253782693736489371484759148259586125
     * 857<i>4</i>5282229685639333179674144428178525553928963666
     * 242<i>1</i>2392483532341359464345246157545635726865674683
     * 246<i>1123532</i>3572234643468334575457944568656815567976
     * 423653274<i>1</i>5347643852645875496375698651748671976285
     * 231424963<i>2342</i>5351743453646285456475739656758684176
     * 342215569245<i>332</i>66713564437782467554889357866599146
     * 33859739644496<i>1</i>84175551729528666283163977739427418
     * 35135958944624<i>61</i>6915573572712668468382377957949348
     * 435873354154698<i>44</i>652657195576376821668748793277985
     * 5826253782693736<i>4</i>893714847591482595861259361697236
     * 9685639333179674<i>1</i>444281785255539289636664139174777
     * 3532341359464345<i>2461</i>575456357268656746837976785794
     * 3572234643468334575<i>4</i>579445686568155679767926678187
     * 5347643852645875496<i>3</i>756986517486719762859782187396
     * 3425351743453646285<i>4564</i>757396567586841767869795287
     * 4533266713564437782467<i>554</i>8893578665991468977611257
     * 449618417555172952866628<i>3163</i>9777394274188841538529
     * 462461691557357271266846838<i>2</i>3779579493488168151459
     * 546984465265719557637682166<i>8</i>7487932779859814388196
     * 693736489371484759148259586<i>125</i>93616972361472718347
     * 17967414442817852555392896366<i>6413</i>91747775241285888
     * 46434524615754563572686567468379<i>7</i>67857948187896815
     * 46833457545794456865681556797679<i>26</i>6781878137789298
     * 645875496375698651748671976285978<i>21</i>873961893298417
     * 4536462854564757396567586841767869<i>7</i>952878971816398
     * 5644377824675548893578665991468977<i>6112</i>579188722368
     * 5517295286662831639777394274188841538<i>5</i>299952649631
     * 5735727126684683823779579493488168151<i>4</i>599279262561
     * 6571955763768216687487932779859814388<i>1</i>961925499217
     * 7148475914825958612593616972361472718<i>34725</i>83829458
     * 28178525553928963666413917477752412858886<i>3</i>52396999
     * 57545635726865674683797678579481878968159<i>2</i>98917926
     * 57944568656815567976792667818781377892989<i>24</i>8891319
     * 756986517486719762859782187396189329841729<i>1431</i>9528
     * 564757396567586841767869795287897181639891829<i>2</i>7419
     * 675548893578665991468977611257918872236812998<i>33479</i>
     * </pre>
     * <p>
     * The total risk of this path is <code><i>315</i></code> (the starting position is still never entered, so its risk is not counted).
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Using the full map, what is the lowest total risk of any path from the top left to the bottom right?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Grid chitons = context.readGrid(chr -> chr - '0');
        final Grid extraChitons = Grid.size(
                chitons.width() * 5,
                chitons.height() * 5,
                point -> {
                    final int offsetX = point.x() / chitons.width();
                    final int offsetY = point.y() / chitons.height();
                    final int offset = offsetX + offsetY;

                    final int ox = point.x() % chitons.width();
                    final int oy = point.y() % chitons.height();
                    final int value = chitons.get(new Coordinate(ox, oy));

                    return IntStream.range(0, offset)
                            .reduce(value, (result, iter) -> (result + 1) > 9 ? 1 : result + 1);
                }
        );

        return findShortestPath(extraChitons);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Find the shortest path between the top-left and bottom-right corners.
     */
    private long findShortestPath(final Grid chiterns) {
        final ShortestPath solver = new Dijkstra();
        return solver.solve(chiterns);
    }

}
