package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.*;
import java.util.function.Function;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 12: Passage Pathing.
 */
@Solution(year = 2021, day = 12, title = "Passage Pathing")
public class Day12 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * With your <span title="Sublime.">submarine's subterranean subsystems subsisting suboptimally</span>, the only way you're getting out of this cave anytime soon is by finding a path yourself.
     * Not just <i>a</i> path - the only way to know if you've found the <i>best</i> path is to find <i>all</i> of them.
     * <p>
     * Fortunately, the sensors are still mostly working, and so you build a rough map of the remaining caves (your puzzle input). For example:
     * <pre>
     * start-A
     * start-b
     * A-c
     * A-b
     * b-d
     * A-end
     * b-end
     * </pre>
     * <p>
     * This is a list of how all of the caves are connected.
     * You start in the cave named <code>start</code>, and your destination is the cave named <code>end</code>.
     * An entry like <code>b-d</code> means that cave <code>b</code> is connected to cave <code>d</code> - that is, you can move between them.
     * <p>
     * So, the above cave system looks roughly like this:
     * <pre>
     * start
     *     /   \
     * c--A-----b--d
     *     \   /
     *      end
     * </pre>
     * <p>
     * Your goal is to find the number of distinct <i>paths</i> that start at <code>start</code>, end at <code>end</code>, and don't visit small caves more than once.
     * There are two types of caves: <i>big</i> caves (written in uppercase, like <code>A</code>) and <i>small</i> caves (written in lowercase, like <code>b</code>).
     * It would be a waste of time to visit any small cave more than once, but big caves are large enough that it might be worth visiting them multiple times.
     * So, all paths you find should <i>visit small caves at most once</i>, and can <i>visit big caves any number of times</i>.
     * <p>
     * Given these rules, there are <code><i>10</i></code> paths through this example cave system:
     * <pre>
     * start,A,b,A,c,A,end
     * start,A,b,A,end
     * start,A,b,end
     * start,A,c,A,b,A,end
     * start,A,c,A,b,end
     * start,A,c,A,end
     * start,A,end
     * start,b,A,c,A,end
     * start,b,A,end
     * start,b,end
     * </pre>
     * <p>
     * (Each line in the above list corresponds to a single path; the caves visited by that path are listed in the order they are visited and separated by commas.)
     * <p>
     * Note that in this cave system, cave <code>d</code> is never visited by any path: to do so, cave <code>b</code> would need to be visited twice (once on the way to cave <code>d</code> and a second time when returning from cave <code>d</code>), and since cave <code>b</code> is small, this is not allowed.
     * <p>
     * Here is a slightly larger example:
     * <pre>
     * dc-end
     * HN-start
     * start-kj
     * dc-start
     * dc-HN
     * LN-dc
     * HN-end
     * kj-sa
     * kj-HN
     * kj-dc
     * </pre>
     * <p>
     * The <code>19</code> paths through it are as follows:
     * <pre>
     * start,HN,dc,HN,end
     * start,HN,dc,HN,kj,HN,end
     * start,HN,dc,end
     * start,HN,dc,kj,HN,end
     * start,HN,end
     * start,HN,kj,HN,dc,HN,end
     * start,HN,kj,HN,dc,end
     * start,HN,kj,HN,end
     * start,HN,kj,dc,HN,end
     * start,HN,kj,dc,end
     * start,dc,HN,end
     * start,dc,HN,kj,HN,end
     * start,dc,end
     * start,dc,kj,HN,end
     * start,kj,HN,dc,HN,end
     * start,kj,HN,dc,end
     * start,kj,HN,end
     * start,kj,dc,HN,end
     * start,kj,dc,end
     * </pre>
     * <p>
     * Finally, this even larger example has <code>226</code> paths through it:
     * <pre>
     * fs-end
     * he-DX
     * fs-he
     * start-DX
     * pj-DX
     * end-zg
     * zg-sl
     * zg-pj
     * pj-he
     * RW-he
     * fs-DX
     * pj-RW
     * zg-RW
     * start-pj
     * he-WI
     * zg-he
     * pj-fs
     * start-RW
     * </pre>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many paths through this cave system are there that visit small caves at most once?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return countPaths(context, false);
    }

    /**
     * After reviewing the available paths, you realize you might have time to visit a single small cave <i>twice</i>.
     * Specifically, big caves can be visited any number of times, a single small cave can be visited at most twice, and the remaining small caves can be visited at most once.
     * However, the caves named <code>start</code> and <code>end</code> can only be visited <i>exactly once each</i>: once you leave the <code>start</code> cave, you may not return to it, and once you reach the <code>end</code> cave, the path must end immediately.
     * <p>
     * Now, the <code>36</code> possible paths through the first example above are:
     * <pre>
     * start,A,b,A,b,A,c,A,end
     * start,A,b,A,b,A,end
     * start,A,b,A,b,end
     * start,A,b,A,c,A,b,A,end
     * start,A,b,A,c,A,b,end
     * start,A,b,A,c,A,c,A,end
     * start,A,b,A,c,A,end
     * start,A,b,A,end
     * start,A,b,d,b,A,c,A,end
     * start,A,b,d,b,A,end
     * start,A,b,d,b,end
     * start,A,b,end
     * start,A,c,A,b,A,b,A,end
     * start,A,c,A,b,A,b,end
     * start,A,c,A,b,A,c,A,end
     * start,A,c,A,b,A,end
     * start,A,c,A,b,d,b,A,end
     * start,A,c,A,b,d,b,end
     * start,A,c,A,b,end
     * start,A,c,A,c,A,b,A,end
     * start,A,c,A,c,A,b,end
     * start,A,c,A,c,A,end
     * start,A,c,A,end
     * start,A,end
     * start,b,A,b,A,c,A,end
     * start,b,A,b,A,end
     * start,b,A,b,end
     * start,b,A,c,A,b,A,end
     * start,b,A,c,A,b,end
     * start,b,A,c,A,c,A,end
     * start,b,A,c,A,end
     * start,b,A,end
     * start,b,d,b,A,c,A,end
     * start,b,d,b,A,end
     * start,b,d,b,end
     * start,b,end
     * </pre>
     * <p>
     * The slightly larger example above now has <code>103</code> paths through it, and the even larger example now has <code>3509</code> paths through it.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Given these new rules, how many paths through this cave system are there?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return countPaths(context, true);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Count the paths out of the cave system.
     */
    private long countPaths(final SolutionContext context, final boolean canVisitTwice) {
        final Map<String, Cave> caves = parseCaves(context);

        final Cave startCave = caves.get("start");
        final Cave endCave = caves.get("end");

        return countPaths(startCave, endCave, new LinkedList<>(), canVisitTwice);
    }

    /*
     * Count the paths out of the cave system.
     */
    private long countPaths(final Cave start, final Cave end, final Deque<Cave> visited, final boolean canVisitTwice) {
        if (start == end)
            return 1;

        visited.addLast(start);
        long count = 0;
        for (final Cave next : start.exits) {
            final boolean notAlreadyVisited = next.isLargeCave || !visited.contains(next);
            final boolean canActuallyVisitTwice = (canVisitTwice && !"start".equals(next.name) && !"end".equals(next.name));
            if (notAlreadyVisited || canActuallyVisitTwice)
                count += countPaths(next, end, visited, (canVisitTwice && notAlreadyVisited));
        }
        visited.removeLast();

        return count;
    }

    /*
     * Parse the cave system from the input file.
     */
    private Map<String, Cave> parseCaves(final SolutionContext context) {
        final Map<String, Cave> caves = new HashMap<>();
        context.consume(line -> {
            final String[] parts = line.split("\\s*-\\s*", 2);

            final Function<String, Cave> getOrCreateCave = name ->
                    caves.computeIfAbsent(name, k -> new Cave(name, new ArrayList<>(), name.matches("[A-Z]+")));

            final Cave start = getOrCreateCave.apply(parts[0]);
            final Cave end = getOrCreateCave.apply(parts[1]);
            start.exits.add(end);
            end.exits.add(start);
        });

        return caves;
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * A cave.
     */
    private record Cave(String name, List<Cave> exits, boolean isLargeCave) {
    }

}
