package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.NoChallenge;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid.MutableGrid;

import java.util.HashMap;
import java.util.Map;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 25: Sea Cucumber.
 */
@Solution(year = 2021, day = 25, title = "Sea Cucumber")
public class Day25 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * This is it: the bottom of the ocean trench, the last place the sleigh keys could be.
     * Your submarine's experimental antenna <i>still isn't boosted enough</i> to detect the keys, but they <i>must</i> be here.
     * All you need to do is <i>reach the seafloor</i> and find them.
     * <p>
     * At least, you'd touch down on the seafloor if you could; unfortunately, it's completely covered by two large herds of <a href="https://en.wikipedia.org/wiki/Sea_cucumber" target="_blank">sea cucumbers</a>, and there isn't an open space large enough for your submarine.
     * <p>
     * You suspect that the Elves must have done this before, because just then you discover the phone number of a deep-sea marine biologist on a handwritten note taped to the wall of the submarine's cockpit.
     * <p>
     * "Sea cucumbers?
     * Yeah, they're probably hunting for food.
     * But don't worry, they're predictable critters: they move in perfectly straight lines, only moving forward when there's space to do so.
     * They're actually quite polite!"
     * <p>
     * <p>
     * You explain that you'd like to predict when you could land your submarine.
     * <p>
     * "Oh that's easy, they'll eventually pile up and leave enough space for-- wait, did you say submarine? And the only place with that many sea cucumbers would be at the very bottom of the Mariana--" You hang up the phone.
     * <p>
     * There are two herds of sea cucumbers sharing the same region; one always moves <i>east</i> (<code>&gt;</code>), while the other always moves <i>south</i> (<code>v</code>).
     * Each location can contain at most one sea cucumber; the remaining locations are <i>empty</i> (<code>.</code>).
     * The submarine helpfully generates a map of the situation (your puzzle input).
     * For example:
     * <pre>
     * v...&gt;&gt;.vv&gt;
     * .vv&gt;&gt;.vv..
     * &gt;&gt;.&gt;v&gt;...v
     * &gt;&gt;v&gt;&gt;.&gt;.v.
     * v&gt;v.vv.v..
     * &gt;.&gt;&gt;..v...
     * .vv..&gt;.&gt;v.
     * v.v..&gt;&gt;v.v
     * ....v..v.&gt;
     * </pre>
     * <p>
     * Every <i>step</i>, the sea cucumbers in the east-facing herd attempt to move forward one location, then the sea cucumbers in the south-facing herd attempt to move forward one location.
     * When a herd moves forward, every sea cucumber in the herd first simultaneously considers whether there is a sea cucumber in the adjacent location it's facing (even another sea cucumber facing the same direction), and then every sea cucumber facing an empty location simultaneously moves into that location.
     * <p>
     * So, in a situation like this:
     * <pre>
     * ...&gt;&gt;&gt;&gt;&gt;...
     * </pre>
     * <p>
     * After one step, only the rightmost sea cucumber would have moved:
     * <pre>
     * ...&gt;&gt;&gt;&gt;.&gt;..
     * </pre>
     * <p>
     * After the next step, two sea cucumbers move:
     * <pre>
     * ...&gt;&gt;&gt;.&gt;.&gt;.
     * </pre>
     * <p>
     * During a single step, the east-facing herd moves first, then the south-facing herd moves.
     * So, given this situation:
     * <pre>
     * ..........
     * .&gt;v....v..
     * .......&gt;..
     * ..........
     * </pre>
     * <p>
     * After a single step, of the sea cucumbers on the left, only the south-facing sea cucumber has moved (as it wasn't out of the way in time for the east-facing cucumber on the left to move), but both sea cucumbers on the right have moved (as the east-facing sea cucumber moved out of the way of the south-facing sea cucumber):
     * <pre>
     * ..........
     * .&gt;........
     * ..v....v&gt;.
     * ..........
     * </pre>
     * <p>
     * Due to <i>strong water currents</i> in the area, sea cucumbers that move off the right edge of the map appear on the left edge, and sea cucumbers that move off the bottom edge of the map appear on the top edge.
     * Sea cucumbers always check whether their destination location is empty before moving, even if that destination is on the opposite side of the map:
     * <pre>
     * Initial state:
     * ...&gt;...
     * .......
     * ......&gt;
     * v.....&gt;
     * ......&gt;
     * .......
     * ..vvv..
     *
     * After 1 step:
     * ..vv&gt;..
     * .......
     * &gt;......
     * v.....&gt;
     * &gt;......
     * .......
     * ....v..
     *
     * After 2 steps:
     * ....v&gt;.
     * ..vv...
     * .&gt;.....
     * ......&gt;
     * v&gt;.....
     * .......
     * .......
     *
     * After 3 steps:
     * ......&gt;
     * ..v.v..
     * ..&gt;v...
     * &gt;......
     * ..&gt;....
     * v......
     * .......
     *
     * After 4 steps:
     * &gt;......
     * ..v....
     * ..&gt;.v..
     * .&gt;.v...
     * ...&gt;...
     * .......
     * v......
     * </pre>
     * <p>
     * To find a safe place to land your submarine, the sea cucumbers need to stop moving.
     * Again consider the first example:
     * <pre>
     * Initial state:
     * v...&gt;&gt;.vv&gt;
     * .vv&gt;&gt;.vv..
     * &gt;&gt;.&gt;v&gt;...v
     * &gt;&gt;v&gt;&gt;.&gt;.v.
     * v&gt;v.vv.v..
     * &gt;.&gt;&gt;..v...
     * .vv..&gt;.&gt;v.
     * v.v..&gt;&gt;v.v
     * ....v..v.&gt;
     *
     * After 1 step:
     * ....&gt;.&gt;v.&gt;
     * v.v&gt;.&gt;v.v.
     * &gt;v&gt;&gt;..&gt;v..
     * &gt;&gt;v&gt;v&gt;.&gt;.v
     * .&gt;v.v...v.
     * v&gt;&gt;.&gt;vvv..
     * ..v...&gt;&gt;..
     * vv...&gt;&gt;vv.
     * &gt;.v.v..v.v
     *
     * After 2 steps:
     * &gt;.v.v&gt;&gt;..v
     * v.v.&gt;&gt;vv..
     * &gt;v&gt;.&gt;.&gt;.v.
     * &gt;&gt;v&gt;v.&gt;v&gt;.
     * .&gt;..v....v
     * .&gt;v&gt;&gt;.v.v.
     * v....v&gt;v&gt;.
     * .vv..&gt;&gt;v..
     * v&gt;.....vv.
     *
     * After 3 steps:
     * v&gt;v.v&gt;.&gt;v.
     * v...&gt;&gt;.v.v
     * &gt;vv&gt;.&gt;v&gt;..
     * &gt;&gt;v&gt;v.&gt;.v&gt;
     * ..&gt;....v..
     * .&gt;.&gt;v&gt;v..v
     * ..v..v&gt;vv&gt;
     * v.v..&gt;&gt;v..
     * .v&gt;....v..
     *
     * After 4 steps:
     * v&gt;..v.&gt;&gt;..
     * v.v.&gt;.&gt;.v.
     * &gt;vv.&gt;&gt;.v&gt;v
     * &gt;&gt;.&gt;..v&gt;.&gt;
     * ..v&gt;v...v.
     * ..&gt;&gt;.&gt;vv..
     * &gt;.v.vv&gt;v.v
     * .....&gt;&gt;vv.
     * vvv&gt;...v..
     *
     * After 5 steps:
     * vv&gt;...&gt;v&gt;.
     * v.v.v&gt;.&gt;v.
     * &gt;.v.&gt;.&gt;.&gt;v
     * &gt;v&gt;.&gt;..v&gt;&gt;
     * ..v&gt;v.v...
     * ..&gt;.&gt;&gt;vvv.
     * .&gt;...v&gt;v..
     * ..v.v&gt;&gt;v.v
     * v.v.&gt;...v.
     *
     * ...
     *
     * After 10 steps:
     * ..&gt;..&gt;&gt;vv.
     * v.....&gt;&gt;.v
     * ..v.v&gt;&gt;&gt;v&gt;
     * v&gt;.&gt;v.&gt;&gt;&gt;.
     * ..v&gt;v.vv.v
     * .v.&gt;&gt;&gt;.v..
     * v.v..&gt;v&gt;..
     * ..v...&gt;v.&gt;
     * .vv..v&gt;vv.
     *
     * ...
     *
     * After 20 steps:
     * v&gt;.....&gt;&gt;.
     * &gt;vv&gt;.....v
     * .&gt;v&gt;v.vv&gt;&gt;
     * v&gt;&gt;&gt;v.&gt;v.&gt;
     * ....vv&gt;v..
     * .v.&gt;&gt;&gt;vvv.
     * ..v..&gt;&gt;vv.
     * v.v...&gt;&gt;.v
     * ..v.....v&gt;
     *
     * ...
     *
     * After 30 steps:
     * .vv.v..&gt;&gt;&gt;
     * v&gt;...v...&gt;
     * &gt;.v&gt;.&gt;vv.&gt;
     * &gt;v&gt;.&gt;.&gt;v.&gt;
     * .&gt;..v.vv..
     * ..v&gt;..&gt;&gt;v.
     * ....v&gt;..&gt;v
     * v.v...&gt;vv&gt;
     * v.v...&gt;vvv
     *
     * ...
     *
     * After 40 steps:
     * &gt;&gt;v&gt;v..v..
     * ..&gt;&gt;v..vv.
     * ..&gt;&gt;&gt;v.&gt;.v
     * ..&gt;&gt;&gt;&gt;vvv&gt;
     * v.....&gt;...
     * v.v...&gt;v&gt;&gt;
     * &gt;vv.....v&gt;
     * .&gt;v...v.&gt;v
     * vvv.v..v.&gt;
     *
     * ...
     *
     * After 50 steps:
     * ..&gt;&gt;v&gt;vv.v
     * ..v.&gt;&gt;vv..
     * v.&gt;&gt;v&gt;&gt;v..
     * ..&gt;&gt;&gt;&gt;&gt;vv.
     * vvv....&gt;vv
     * ..v....&gt;&gt;&gt;
     * v&gt;.......&gt;
     * .vv&gt;....v&gt;
     * .&gt;v.vv.v..
     *
     * ...
     *
     * After 55 steps:
     * ..&gt;&gt;v&gt;vv..
     * ..v.&gt;&gt;vv..
     * ..&gt;&gt;v&gt;&gt;vv.
     * ..&gt;&gt;&gt;&gt;&gt;vv.
     * v......&gt;vv
     * v&gt;v....&gt;&gt;v
     * vvv...&gt;..&gt;
     * &gt;vv.....&gt;.
     * .&gt;v.vv.v..
     *
     * After 56 steps:
     * ..&gt;&gt;v&gt;vv..
     * ..v.&gt;&gt;vv..
     * ..&gt;&gt;v&gt;&gt;vv.
     * ..&gt;&gt;&gt;&gt;&gt;vv.
     * v......&gt;vv
     * v&gt;v....&gt;&gt;v
     * vvv....&gt;.&gt;
     * &gt;vv......&gt;
     * .&gt;v.vv.v..
     *
     * After 57 steps:
     * ..&gt;&gt;v&gt;vv..
     * ..v.&gt;&gt;vv..
     * ..&gt;&gt;v&gt;&gt;vv.
     * ..&gt;&gt;&gt;&gt;&gt;vv.
     * v......&gt;vv
     * v&gt;v....&gt;&gt;v
     * vvv.....&gt;&gt;
     * &gt;vv......&gt;
     * .&gt;v.vv.v..
     *
     * After 58 steps:
     * ..&gt;&gt;v&gt;vv..
     * ..v.&gt;&gt;vv..
     * ..&gt;&gt;v&gt;&gt;vv.
     * ..&gt;&gt;&gt;&gt;&gt;vv.
     * v......&gt;vv
     * v&gt;v....&gt;&gt;v
     * vvv.....&gt;&gt;
     * &gt;vv......&gt;
     * .&gt;v.vv.v..
     * </pre>
     * <p>
     * In this example, the sea cucumbers stop moving after <code><i>58</i></code> steps.
     * <p>
     * Find somewhere safe to land your submarine.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the first step on which no sea cucumbers move?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final MutableGrid grid = context.readMutableGrid();

        long turn = 0;
        boolean atLeastOneCucumberMoved;
        do {
            final Map<Coordinate, Coordinate> movableEast = new HashMap<>();
            grid.forEachValue((coord, maybeCucumber) -> {
                if (maybeCucumber != '>') return;

                Coordinate destination = coord.adjustBy(1, 0);
                if (!grid.contains(destination))
                    destination = new Coordinate(0, destination.y());

                if (grid.get(destination) == '.')
                    movableEast.put(coord, destination);
            });

            movableEast.forEach((start, destination) -> {
                grid.set(start, '.');
                grid.set(destination, start.y() == destination.y() ? '>' : 'v');
            });

            final Map<Coordinate, Coordinate> movableSouth = new HashMap<>();
            grid.forEachValue((coord, maybeCucumber) -> {
                if (maybeCucumber != 'v') return;

                Coordinate destination = coord.adjustBy(0, 1);
                if (!grid.contains(destination))
                    destination = new Coordinate(destination.x(), 0);

                if (grid.get(destination) == '.')
                    movableSouth.put(coord, destination);
            });

            movableSouth.forEach((start, destination) -> {
                grid.set(start, '.');
                grid.set(destination, start.y() == destination.y() ? '>' : 'v');
            });

            atLeastOneCucumberMoved = !movableEast.isEmpty() || !movableSouth.isEmpty();
            ++turn;
        } while (atLeastOneCucumberMoved);

        return turn;
    }

    /**
     * Suddenly, the experimental antenna control console lights up:
     * <pre><i>Sleigh keys detected!</i></pre>
     * <p>
     * According to the console, the keys are <i>directly under the submarine</i>.
     * <span title="Thanks to the deep-sea marine biologist, who apparently works at the Biham-Middleton-Levine oceanic research institute.">You landed</span> right on them!
     * Using a robotic arm on the submarine, you move the sleigh keys into the airlock.
     * <p>
     * Now, you just need to get them to Santa in time to save Christmas!
     * You check your clock - it <i>is</i> Christmas.
     * There's no way you can get them back to the surface in time.
     * <p>
     * Just as you start to lose hope, you notice a button on the sleigh keys: <i>remote start</i>.
     * You can start the sleigh from the bottom of the ocean!
     * You just need some way to <i>boost the signal</i> from the keys so it actually reaches the sleigh.
     * Good thing the submarine has that experimental antenna!
     * You'll definitely need <b>50 stars</b> to boost it that far, though.
     * <p>
     * The experimental antenna control console lights up again:
     * <pre>
     * <i>
     * Energy source detected.
     * Integrating energy source from device "sleigh keys"...done.
     * Installing device drivers...done.
     * Recalibrating experimental antenna...done.
     * Boost strength due to matching signal phase: <b>1 star</b>
     * </i>
     * </pre>
     * <p>
     * Only <b>49 stars</b> to go.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return -
     */
    @Part(part = II)
    public NoChallenge calculateAnswerForPart2(final SolutionContext context) {
        return NoChallenge.NO_CHALLENGE;
    }

}