package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.LongAccumulator;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 9: Smoke Basin.
 */
@Solution(year = 2021, day = 9, title = "Smoke Basin")
public class Day9 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * These caves seem to be <a href="https://en.wikipedia.org/wiki/Lava_tube">lava tubes</a>.
     * Parts are even still volcanically active; small hydrothermal vents release smoke into the caves that slowly settles like rain.
     * <p>
     * If you can model how the smoke flows through the caves, you might be able to avoid it and be that much safer. The submarine generates a heightmap of the floor of the nearby caves for you (your puzzle input).
     * <p>
     * Smoke flows to the lowest point of the area it's in. For example, consider the following heightmap:
     * <pre>
     * 2199943210
     * 3987894921
     * 9856789892
     * 8767896789
     * 9899965678
     * </pre>
     * <p>
     * Each number corresponds to the height of a particular location, where <code>9</code> is the highest and <code>0</code> is the lowest a location can be.
     * <p>
     * Your first goal is to find the <i>low points</i> - the locations that are lower than any of its adjacent locations.
     * Most locations have four adjacent locations (up, down, left, and right); locations on the edge or corner of the map have three or two adjacent locations, respectively.
     * (Diagonal locations do not count as adjacent.)
     * <p>
     * In the above example, there are <i>four</i> low points, all highlighted: two are in the first row (a <code>1</code> and a <code>0</code>), one is in the third row (a <code>5</code>), and one is in the bottom row (also a <code>5</code>).
     * All other locations on the heightmap have some lower adjacent location, and so are not low points.
     * <p>
     * The <i>risk level</i> of a low point is <i>1 plus its height</i>.
     * In the above example, the risk levels of the low points are <code>2</code>, <code>1</code>, <code>6</code>, and <code>6</code>.
     * The sum of the risk levels of all low points in the heightmap is therefore <i><code>15</code></i>.
     * <p>
     * Find all of the low points on your heightmap.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the risk levels of all low points on your heightmap?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Grid heightMap = context.readGrid(chr -> chr - '0');

        final LongAccumulator sum = new LongAccumulator(Long::sum, 0);
        heightMap.forEachMatching(
                point -> isLowPoint(heightMap, point),
                point -> sum.accumulate(heightMap.get(point) + 1)
        );

        return sum.longValue();
    }

    /**
     * Next, you need to find the largest basins so you know what areas are most important to avoid.
     * <p>
     * A <i>basin</i> is all locations that eventually flow downward to a single low point.
     * Therefore, every low point has a basin, although some basins are very small.
     * Locations of height 9 do not count as being in any basin, and all other locations will always be part of exactly one basin.
     * <p>
     * The <i>size</i> of a basin is the number of locations within the basin, including the low point.
     * The example above has four basins.
     * <p>
     * The top-left basin, size <code>3</code>:
     * <pre>
     * 2199943210
     * 3987894921
     * 9856789892
     * 8767896789
     * 9899965678
     * </pre>
     * <p>
     * The top-right basin, size <code>9</code>:
     * <pre>
     * 2199943210
     * 3987894921
     * 9856789892
     * 8767896789
     * 9899965678
     * </pre>
     * <p>
     * The middle basin, size <code>14</code>:
     * <pre>
     * 2199943210
     * 3987894921
     * 9856789892
     * 8767896789
     * 9899965678
     * </pre>
     * <p>
     * The bottom-right basin, size <code>9</code>:
     * <pre>
     * 2199943210
     * 3987894921
     * 9856789892
     * 8767896789
     * 9899965678
     * </pre>
     * <p>
     * Find the three largest basins and multiply their sizes together.
     * In the above example, this is <code>9 * 14 * 9 = <i>1134</i></code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you multiply together the sizes of the three largest basins?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Grid heightMap = context.readGrid(chr -> chr - '0');

        final List<Long> basinSizes = new ArrayList<>();
        heightMap.forEachMatching(
                point -> isLowPoint(heightMap, point),
                point -> basinSizes.add(countBasin(heightMap, point))
        );

        basinSizes.sort((a, b) -> Long.compare(b, a));
        return basinSizes.get(0) * basinSizes.get(1) * basinSizes.get(2);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Determine whether this `Coordinate` is a "low-point".
     */
    private boolean isLowPoint(final Grid heightMap, final Coordinate coordinate) {
        return heightMap.adjacentTo(coordinate)
                .allMatch(neighbour -> heightMap.get(neighbour) > heightMap.get(coordinate));
    }

    /*
     * Count the number of `Coordinate`s in a basin.
     */
    private long countBasin(final Grid heightMap, final Coordinate coordinate) {
        return countBasin(heightMap, coordinate, new HashSet<>());
    }

    /*
     * Count the number of `Coordinate`s in a basin.
     */
    private long countBasin(final Grid heightMap, final Coordinate coordinate, final Set<Coordinate> visited) {
        if (!heightMap.contains(coordinate)) return 0;
        if (heightMap.get(coordinate) == 9) return 0;

        visited.add(coordinate);
        return heightMap.adjacentTo(coordinate)
                .filter(neighbour -> !visited.contains(neighbour))
                .mapToLong(neighbour -> countBasin(heightMap, neighbour, visited))
                .sum() + 1;
    }

}
