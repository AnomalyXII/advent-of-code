package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.function.UnaryOperator;

import static java.lang.Math.abs;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 19: Beacon Scanner.
 */
@Solution(year = 2021, day = 19, title = "Beacon Scanner")
public class Day19 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * As your <a href="17">probe</a> drifted down through this area, it released an assortment of <i>beacons</i> and <i>scanners</i> into the water.
     * It's difficult to navigate in the pitch black open waters of the ocean trench, but if you can build a map of the trench using data from the scanners, you should be able to safely reach the bottom.
     * <p>
     * The beacons and scanners float motionless in the water; they're designed to maintain the same position for long periods of time.
     * Each scanner is capable of detecting all beacons in a large cube centered on the scanner; beacons that are at most 1000 units away from the scanner in each of the three axes (<code>x</code>, <code>y</code>, and <code>z</code>) have their precise position determined relative to the scanner.
     * However, scanners cannot detect other scanners.
     * The submarine has automatically summarized the relative positions of beacons detected by each scanner (your puzzle input).
     * <p>
     * For example, if a scanner is at <code>x,y,z</code> coordinates <code>500,0,-500</code> and there are beacons at <code>-500,1000,-1500</code> and <code>1501,0,-500</code>, the scanner could report that the first beacon is at <code>-1000,1000,-1000</code> (relative to the scanner) but would not detect the second beacon at all.
     * <p>
     * Unfortunately, while each scanner can report the positions of all detected beacons relative to itself, <i>the scanners do not know their own position</i>.
     * You'll need to determine the positions of the beacons and scanners yourself.
     * <p>
     * The scanners and beacons map a single contiguous 3d region.
     * This region can be reconstructed by finding pairs of scanners that have overlapping detection regions such that there are <i>at least 12 beacons</i> that both scanners detect within the overlap.
     * By establishing 12 common beacons, you can precisely determine where the scanners are relative to each other, allowing you to reconstruct the beacon map one scanner at a time.
     * <p>
     * For a moment, consider only two dimensions.
     * Suppose you have the following scanner reports:
     * <pre>
     * --- scanner 0 ---
     * 0,2
     * 4,1
     * 3,3
     *
     * --- scanner 1 ---
     * -1,-1
     * -5,0
     * -2,1
     * </pre>
     * <p>
     * Drawing <code>x</code> increasing rightward, <code>y</code> increasing upward, scanners as <code>S</code>, and beacons as <code>B</code>, scanner <code>0</code> detects this:
     * <pre>
     * ...B.
     * B....
     * ....B
     * S....
     * </pre>
     * <p>
     * Scanner <code>1</code> detects this:
     * <pre>
     * ...B..
     * B....S
     * ....B.
     * </pre>
     * <p>
     * For this example, assume scanners only need 3 overlapping beacons.
     * Then, the beacons visible to both scanners overlap to produce the following complete map:
     * <pre>
     * ...B..
     * B....S
     * ....B.
     * S.....
     * </pre>
     * <p>
     * Unfortunately, there's a second problem: the scanners also don't know their <i>rotation or facing direction</i>.
     * Due to magnetic alignment, each scanner is rotated some integer number of 90-degree turns around all of the <code>x</code>, <code>y</code>, and <code>z</code> axes.
     * That is, one scanner might call a direction positive <code>x</code>, while another scanner might call that direction negative <code>y</code>.
     * Or, two scanners might agree on which direction is positive <code>x</code>, but one scanner might be upside-down from the perspective of the other scanner.
     * In total, each scanner could be in any of 24 different orientations: facing positive or negative <code>x</code>, <code>y</code>, or <code>z</code>, and considering any of four directions "up" from that facing.
     * <p>
     * For example, here is an arrangement of beacons as seen from a scanner in the same position but in different orientations:
     * <pre>
     * --- scanner 0 ---
     * -1,-1,1
     * -2,-2,2
     * -3,-3,3
     * -2,-3,1
     * 5,6,-4
     * 8,0,7
     *
     * --- scanner 0 ---
     * 1,-1,1
     * 2,-2,2
     * 3,-3,3
     * 2,-1,3
     * -5,4,-6
     * -8,-7,0
     *
     * --- scanner 0 ---
     * -1,-1,-1
     * -2,-2,-2
     * -3,-3,-3
     * -1,-3,-2
     * 4,6,5
     * -7,0,8
     *
     * --- scanner 0 ---
     * 1,1,-1
     * 2,2,-2
     * 3,3,-3
     * 1,3,-2
     * -4,-6,5
     * 7,0,8
     *
     * --- scanner 0 ---
     * 1,1,1
     * 2,2,2
     * 3,3,3
     * 3,1,2
     * -6,-4,-5
     * 0,7,-8
     * </pre>
     * <p>
     * By finding pairs of scanners that both see at least 12 of the same beacons, you can assemble the entire map.
     * For example, consider the following report:
     * <pre>
     * --- scanner 0 ---
     * 404,-588,-901
     * 528,-643,409
     * -838,591,734
     * 390,-675,-793
     * -537,-823,-458
     * -485,-357,347
     * -345,-311,381
     * -661,-816,-575
     * -876,649,763
     * -618,-824,-621
     * 553,345,-567
     * 474,580,667
     * -447,-329,318
     * -584,868,-557
     * 544,-627,-890
     * 564,392,-477
     * 455,729,728
     * -892,524,684
     * -689,845,-530
     * 423,-701,434
     * 7,-33,-71
     * 630,319,-379
     * 443,580,662
     * -789,900,-551
     * 459,-707,401
     *
     * --- scanner 1 ---
     * 686,422,578
     * 605,423,415
     * 515,917,-361
     * -336,658,858
     * 95,138,22
     * -476,619,847
     * -340,-569,-846
     * 567,-361,727
     * -460,603,-452
     * 669,-402,600
     * 729,430,532
     * -500,-761,534
     * -322,571,750
     * -466,-666,-811
     * -429,-592,574
     * -355,545,-477
     * 703,-491,-529
     * -328,-685,520
     * 413,935,-424
     * -391,539,-444
     * 586,-435,557
     * -364,-763,-893
     * 807,-499,-711
     * 755,-354,-619
     * 553,889,-390
     *
     * --- scanner 2 ---
     * 649,640,665
     * 682,-795,504
     * -784,533,-524
     * -644,584,-595
     * -588,-843,648
     * -30,6,44
     * -674,560,763
     * 500,723,-460
     * 609,671,-379
     * -555,-800,653
     * -675,-892,-343
     * 697,-426,-610
     * 578,704,681
     * 493,664,-388
     * -671,-858,530
     * -667,343,800
     * 571,-461,-707
     * -138,-166,112
     * -889,563,-600
     * 646,-828,498
     * 640,759,510
     * -630,509,768
     * -681,-892,-333
     * 673,-379,-804
     * -742,-814,-386
     * 577,-820,562
     *
     * --- scanner 3 ---
     * -589,542,597
     * 605,-692,669
     * -500,565,-823
     * -660,373,557
     * -458,-679,-417
     * -488,449,543
     * -626,468,-788
     * 338,-750,-386
     * 528,-832,-391
     * 562,-778,733
     * -938,-730,414
     * 543,643,-506
     * -524,371,-870
     * 407,773,750
     * -104,29,83
     * 378,-903,-323
     * -778,-728,485
     * 426,699,580
     * -438,-605,-362
     * -469,-447,-387
     * 509,732,623
     * 647,635,-688
     * -868,-804,481
     * 614,-800,639
     * 595,780,-596
     *
     * --- scanner 4 ---
     * 727,592,562
     * -293,-554,779
     * 441,611,-461
     * -714,465,-776
     * -743,427,-804
     * -660,-479,-426
     * 832,-632,460
     * 927,-485,-438
     * 408,393,-506
     * 466,436,-512
     * 110,16,151
     * -258,-428,682
     * -393,719,612
     * -211,-452,876
     * 808,-476,-593
     * -575,615,604
     * -485,667,467
     * -680,325,-822
     * -627,-443,-432
     * 872,-547,-609
     * 833,512,582
     * 807,604,487
     * 839,-516,451
     * 891,-625,532
     * -652,-548,-490
     * 30,-46,-14
     * </pre>
     * <p>
     * Because all coordinates are relative, in this example, all "absolute" positions will be expressed relative to scanner <code>0</code> (using the orientation of scanner <code>0</code> and as if scanner <code>0</code> is at coordinates <code>0,0,0</code>).
     * <p>
     * Scanners <code>0</code> and <code>1</code> have overlapping detection cubes; the 12 beacons they both detect (relative to scanner <code>0</code>) are at the following coordinates:
     * <pre>
     * -618,-824,-621
     * -537,-823,-458
     * -447,-329,318
     * 404,-588,-901
     * 544,-627,-890
     * 528,-643,409
     * -661,-816,-575
     * 390,-675,-793
     * 423,-701,434
     * -345,-311,381
     * 459,-707,401
     * -485,-357,347
     * </pre>
     * <p>
     * These same 12 beacons (in the same order) but from the perspective of scanner <code>1</code> are:
     * <pre>
     * 686,422,578
     * 605,423,415
     * 515,917,-361
     * -336,658,858
     * -476,619,847
     * -460,603,-452
     * 729,430,532
     * -322,571,750
     * -355,545,-477
     * 413,935,-424
     * -391,539,-444
     * 553,889,-390
     * </pre>
     * <p>
     * Because of this, scanner <code>1</code> must be at <code>68,-1246,-43</code> (relative to scanner <code>0</code>).
     * <p>
     * Scanner <code>4</code> overlaps with scanner <code>1</code>; the 12 beacons they both detect (relative to scanner <code>0</code>) are:
     * <pre>
     * 459,-707,401
     * -739,-1745,668
     * -485,-357,347
     * 432,-2009,850
     * 528,-643,409
     * 423,-701,434
     * -345,-311,381
     * 408,-1815,803
     * 534,-1912,768
     * -687,-1600,576
     * -447,-329,318
     * -635,-1737,486
     * </pre>
     * <p>
     * So, scanner <code>4</code> is at <code>-20,-1133,1061</code> (relative to scanner <code>0</code>).
     * <p>
     * Following this process, scanner <code>2</code> must be at <code>1105,-1205,1229</code> (relative to scanner <code>0</code>) and scanner <code>3</code> must be at <code>-92,-2380,-20</code> (relative to scanner <code>0</code>).
     * <p>
     * The full list of beacons (relative to scanner <code>0</code>) is:
     * <pre>
     * -892,524,684
     * -876,649,763
     * -838,591,734
     * -789,900,-551
     * -739,-1745,668
     * -706,-3180,-659
     * -697,-3072,-689
     * -689,845,-530
     * -687,-1600,576
     * -661,-816,-575
     * -654,-3158,-753
     * -635,-1737,486
     * -631,-672,1502
     * -624,-1620,1868
     * -620,-3212,371
     * -618,-824,-621
     * -612,-1695,1788
     * -601,-1648,-643
     * -584,868,-557
     * -537,-823,-458
     * -532,-1715,1894
     * -518,-1681,-600
     * -499,-1607,-770
     * -485,-357,347
     * -470,-3283,303
     * -456,-621,1527
     * -447,-329,318
     * -430,-3130,366
     * -413,-627,1469
     * -345,-311,381
     * -36,-1284,1171
     * -27,-1108,-65
     * 7,-33,-71
     * 12,-2351,-103
     * 26,-1119,1091
     * 346,-2985,342
     * 366,-3059,397
     * 377,-2827,367
     * 390,-675,-793
     * 396,-1931,-563
     * 404,-588,-901
     * 408,-1815,803
     * 423,-701,434
     * 432,-2009,850
     * 443,580,662
     * 455,729,728
     * 456,-540,1869
     * 459,-707,401
     * 465,-695,1988
     * 474,580,667
     * 496,-1584,1900
     * 497,-1838,-617
     * 527,-524,1933
     * 528,-643,409
     * 534,-1912,768
     * 544,-627,-890
     * 553,345,-567
     * 564,392,-477
     * 568,-2007,-577
     * 605,-1665,1952
     * 612,-1593,1893
     * 630,319,-379
     * 686,-3108,-505
     * 776,-3184,-501
     * 846,-3110,-434
     * 1135,-1161,1235
     * 1243,-1093,1063
     * 1660,-552,429
     * 1693,-557,386
     * 1735,-437,1738
     * 1749,-1800,1813
     * 1772,-405,1572
     * 1776,-675,371
     * 1779,-442,1789
     * 1780,-1548,337
     * 1786,-1538,337
     * 1847,-1591,415
     * 1889,-1729,1762
     * 1994,-1805,1792
     * </pre>
     * <p>
     * In total, there are <code><i>79</i></code> beacons.
     * <p>
     * Assemble the full map of beacons.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many beacons are there?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return resolveScanners(context).stream()
                .map(Scanner::beacons)
                .flatMap(Collection::stream)
                .distinct()
                .count();
    }

    /**
     * Sometimes, it's a good idea to appreciate just how <span title="The deepest parts of the ocean are about as deep as the altitude of a normal commercial aircraft, roughly 11 kilometers or 36000 feet.">big</span> the ocean is.
     * Using the <a href="https://en.wikipedia.org/wiki/Taxicab_geometry" target="_blank">Manhattan distance</a>, how far apart do the scanners get?
     * <p>
     * In the above example, scanners <code>2</code> (<code>1105,-1205,1229</code>) and <code>3</code> (<code>-92,-2380,-20</code>) are the largest Manhattan distance apart.
     * In total, they are <code>1197 + 1175 + 1249 = <i>3621</i></code> units apart.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the largest Manhattan distance between any two scanners?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Set<Scanner> scanners = resolveScanners(context);
        final LongAccumulator result = new LongAccumulator(Math::max, Long.MIN_VALUE);
        for (final Scanner scanner : scanners) {
            for (final Scanner other : scanners) {
                final int dx = abs(scanner.position.x - other.position.x);
                final int dy = abs(scanner.position.y - other.position.y);
                final int dz = abs(scanner.position.z - other.position.z);
                result.accumulate(dx + dy + dz);
            }
        }
        return result.longValue();
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Read in the relative `Scanner`s and attempt to normalise them all.
     */
    private Set<Scanner> resolveScanners(final SolutionContext context) {
        final List<Scanner> scanners = context.streamBatches()
                .map(Scanner::parse)
                .toList();

        final Scanner origin = scanners.getFirst();
        final Deque<Scanner> otherScanners = new ArrayDeque<>(scanners.subList(1, scanners.size()));

        final Set<Scanner> absoluteScanners = new HashSet<>();
        absoluteScanners.add(origin);
        while (!otherScanners.isEmpty()) {
            final Scanner scanner = otherScanners.removeFirst();
            resolve(scanner, absoluteScanners)
                    .ifPresentOrElse(
                            absoluteScanners::add,
                            () -> otherScanners.addLast(scanner)
                    );
        }
        return absoluteScanners;
    }

    /*
     * Attempt to resolve the given `Scanner` to an absolute position, based
     * on the overlap with another, already resolved, `Scanner`.
     */
    static Optional<Scanner> resolve(final Scanner scanner, final Set<Scanner> others) {
        for (final Scanner other : others) {
            final Optional<Scanner> resolved = resolve(scanner, other);
            if (resolved.isPresent())
                return resolved;
        }

        return Optional.empty();
    }

    /*
     * Attempt to resolve the given `Scanner` to an absolute position, based
     * on the overlap with a given, already resolved, `Scanner`.
     */
    static Optional<Scanner> resolve(final Scanner scanner, final Scanner other) {
        for (final Orientation o : Orientation.values()) {
            final Map<Position, AtomicInteger> counts = new HashMap<>();
            for (final Position reference : scanner.beacons) {
                for (final Position beacon : other.beacons) {
                    final Position rotatedReference = o.apply(reference);
                    final Position relativePosition = beacon.sub(rotatedReference);
                    counts.computeIfAbsent(relativePosition, k -> new AtomicInteger())
                            .incrementAndGet();
                }
            }

            final Optional<Scanner> result = counts.entrySet().stream()
                    .filter(matches -> matches.getValue().intValue() >= 12)
                    .findFirst()
                    .map(matches -> scanner.resolve(o, matches.getKey()));

            if (result.isPresent())
                return result;
        }
        return Optional.empty();
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * Various orientations that a `Scanner` can be in.
     */
    enum Orientation {

        O00(p -> p),
        O01(p -> new Position(-p.y, p.x, p.z)),
        O02(p -> new Position(-p.x, -p.y, p.z)),
        O03(p -> new Position(p.y, -p.x, p.z)),
        O04(p -> new Position(-p.z, p.y, p.x)),
        O05(p -> new Position(-p.y, -p.z, p.x)),
        O06(p -> new Position(p.z, -p.y, p.x)),
        O07(p -> new Position(p.y, p.z, p.x)),
        O08(p -> new Position(-p.x, p.y, -p.z)),
        O09(p -> new Position(-p.y, -p.x, -p.z)),
        O10(p -> new Position(p.x, -p.y, -p.z)),
        O11(p -> new Position(p.y, p.x, -p.z)),
        O12(p -> new Position(p.z, p.y, -p.x)),
        O13(p -> new Position(-p.y, p.z, -p.x)),
        O14(p -> new Position(-p.z, -p.y, -p.x)),
        O15(p -> new Position(p.y, -p.z, -p.x)),
        O16(p -> new Position(p.x, -p.z, p.y)),
        O17(p -> new Position(p.z, p.x, p.y)),
        O18(p -> new Position(-p.x, p.z, p.y)),
        O19(p -> new Position(-p.z, -p.x, p.y)),
        O21(p -> new Position(-p.x, -p.z, -p.y)),
        O22(p -> new Position(p.z, -p.x, -p.y)),
        O23(p -> new Position(p.x, p.z, -p.y)),
        O24(p -> new Position(-p.z, p.x, -p.y)),

        // End of constants
        ;

        private final UnaryOperator<Position> transformation;

        // Constructors

        Orientation(final UnaryOperator<Position> transformation) {
            this.transformation = transformation;
        }

        // Helper Methods

        /*
         * Transform a given position based on this orientation.
         */
        Position apply(final Position pos) {
            return transformation.apply(pos);
        }

    }

    /*
     * A position, in 3D space.
     */
    record Position(int x, int y, int z) {

        // Helper Methods

        /*
         * Add two `Position`s.
         */
        Position add(final Position other) {
            final Position transformed = Orientation.O00.apply(other);
            return new Position(
                    x + transformed.x,
                    y + transformed.y,
                    z + transformed.z
            );
        }

        /*
         * Subtract two `Position`s.
         */
        Position sub(final Position other) {
            final Position transformed = Orientation.O00.apply(other);
            return new Position(
                    x - transformed.x,
                    y - transformed.y,
                    z - transformed.z
            );
        }

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final Position position = (Position) o;
            return x == position.x && y == position.y && z == position.z;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y, z);
        }

    }

    /*
     * A scanner and the associated positions of detected beacons.
     */
    record Scanner(int id, Position position, List<Position> beacons) {

        // Helper Methods

        /*
         * Resolve the absolute location of this `Scanner`.
         */
        Scanner resolve(final Orientation o, final Position actualPos) {
            return new Scanner(
                    id,
                    actualPos,
                    beacons.stream()
                            .map(pos -> o.apply(pos).add(actualPos))
                            .toList()
            );
        }

        // Static Helper Methods

        /*
         * Parse the given lines into a `Scanner`.
         */
        static Scanner parse(final List<String> lines) {
            final String title = lines.removeFirst();
            final int id = Integer.parseInt(title.substring(12).substring(0, title.length() - 16));
            final List<Position> beacons = lines.stream()
                    .map(line -> {
                        final String[] parts = line.split("\\s*,\\s*");
                        return new Position(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), Integer.parseInt(parts[2]));
                    })
                    .toList();
            return new Scanner(id, new Position(0, 0, 0), beacons);
        }

    }

}
