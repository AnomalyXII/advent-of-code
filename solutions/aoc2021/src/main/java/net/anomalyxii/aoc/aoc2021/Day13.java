package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.Math.max;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 13: Transparent Origami.
 */
@Solution(year = 2021, day = 13, title = "Transparent Origami")
public class Day13 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You reach another volcanically active part of the cave.
     * It would be nice if you could do some kind of thermal imaging so you could tell ahead of time which caves are too hot to safely enter.
     * <p>
     * Fortunately, the submarine seems to be equipped with a thermal camera!
     * When you activate it, you are greeted with:
     * <pre>
     * Congratulations on your purchase! To activate this infrared thermal imaging
     * camera system, please enter the code found on page 1 of the manual.
     * </pre>
     * <p>
     * Apparently, the Elves have never used this feature.
     * To your surprise, you manage to find the manual; as you go to open it, page 1 falls out.
     * It's a large sheet of <a href="https://en.wikipedia.org/wiki/Transparency_(projection)" target="_blank">transparent paper</a>!
     * The transparent paper is marked with random dots and includes instructions on how to fold it up (your puzzle input).
     * <p>
     * For example:
     * <pre>
     * 6,10
     * 0,14
     * 9,10
     * 0,3
     * 10,4
     * 4,11
     * 6,0
     * 6,12
     * 4,1
     * 0,13
     * 10,12
     * 3,4
     * 3,0
     * 8,4
     * 1,10
     * 2,14
     * 8,10
     * 9,0
     *
     * fold along y=7
     * fold along x=5
     * </pre>
     * <p>
     * The first section is a list of dots on the transparent paper.
     * <code>0,0</code> represents the top-left coordinate.
     * The first value, <code>x</code>, increases to the right.
     * The second value, <code>y</code>, increases downward.
     * So, the coordinate <code>3,0</code> is to the right of <code>0,0</code>, and the coordinate <code>0,7</code> is below <code>0,0</code>.
     * The coordinates in this example form the following pattern, where <code>#</code> is a dot on the paper and <code>.</code> is an empty, unmarked position:
     * <pre>
     * ...#..#..#.
     * ....#......
     * ...........
     * #..........
     * ...#....#.#
     * ...........
     * ...........
     * ...........
     * ...........
     * ...........
     * .#....#.##.
     * ....#......
     * ......#...#
     * #..........
     * #.#........
     * </pre>
     * <p>
     * Then, there is a list of <i>fold instructions</i>.
     * Each instruction indicates a line on the transparent paper and wants you to fold the paper <i>up</i> (for horizontal <code>y=...</code> lines) or <i>left</i> (for vertical <code>x=...</code> lines).
     * In this example, the first fold instruction is <code>fold along y=7</code>, which designates the line formed by all of the positions where <code>y</code> is <code>7</code> (marked here with <code>-</code>):
     * <pre>
     * ...#..#..#.
     * ....#......
     * ...........
     * #..........
     * ...#....#.#
     * ...........
     * ...........
     * -----------
     * ...........
     * ...........
     * .#....#.##.
     * ....#......
     * ......#...#
     * #..........
     * #.#........
     * </pre>
     * <p>
     * Because this is a horizontal line, fold the bottom half <i>up</i>.
     * Some of the dots might end up overlapping after the fold is complete, but dots will never appear exactly on a fold line.
     * The result of doing this fold looks like this:
     * <pre>
     * #.##..#..#.
     * #...#......
     * ......#...#
     * #...#......
     * .#.#..#.###
     * ...........
     * ...........
     * </pre>
     * <p>
     * Now, only <code>17</code> dots are visible.
     * <p>
     * Notice, for example, the two dots in the bottom left corner before the transparent paper is folded; after the fold is complete, those dots appear in the top left corner (at <code>0,0</code> and <code>0,1</code>).
     * Because the paper is transparent, the dot just below them in the result (at <code>0,3</code>) remains visible, as it can be seen through the transparent paper.
     * <p>
     * Also notice that some dots can end up <i>overlapping</i>; in this case, the dots merge together and become a single dot.
     * <p>
     * The second fold instruction is <code>fold along x=5</code>, which indicates this line:
     * <pre>
     * #.##.|#..#.
     * #...#|.....
     * .....|#...#
     * #...#|.....
     * .#.#.|#.###
     * .....|.....
     * .....|.....
     * </pre>
     * <p>
     * Because this is a vertical line, fold <i>left</i>:
     * <pre>
     * #####
     * #...#
     * #...#
     * #...#
     * #####
     * .....
     * .....
     * </pre>
     * <p>
     * The instructions made a square!
     * <p>
     * The transparent paper is pretty big, so for now, focus on just completing the first fold.
     * After the first fold in the example above, <code><i>17</i></code> dots are visible - dots that end up overlapping after the fold is completed count as a single dot.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many dots are visible after completing just the first fold instruction on your transparent paper?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final DotsAndInstructions dotsAndInstructions = parse(context);

        final Instruction firstFold = dotsAndInstructions.instructions.getFirst();
        final Set<Coordinate> transformed = dotsAndInstructions.dots.stream()
                .map(firstFold::transform)
                .collect(Collectors.toSet());

        return transformed.size();
    }

    /**
     * <span title="How can you fold it that many times? You tell me, I'm not the one folding it.">Finish folding</span> the transparent paper according to the instructions.
     * The manual says the code is always <i>eight capital letters</i>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What code do you use to activate the infrared thermal imaging camera system?
     */
    @Part(part = II)
    public String calculateAnswerForPart2(final SolutionContext context) {
        final DotsAndInstructions dotsAndInstructions = parse(context);

        final Set<Coordinate> finalCoordinates = dotsAndInstructions.instructions.stream()
                .reduce(
                        new HashSet<>(dotsAndInstructions.dots),
                        (Set<Coordinate> transformed, Instruction instruction) -> transformed.stream()
                                .map(instruction::transform)
                                .collect(Collectors.toSet()),
                        (a, b) -> {
                            throw new IllegalStateException("Should not need to merge");
                        }
                );

        final Coordinate max = finalCoordinates.stream()
                .reduce(Coordinate.ORIGIN, (r, n) -> new Coordinate(max(r.x(), n.x()), max(r.y(), n.y())));

        // Letters appear to be 5x6?
        final Grid grid = Grid.size(((max.x() + 4) / 5) * 5, max.y() + 1, p -> finalCoordinates.contains(p) ? 1 : 0);
        return context.ocr().recognise(grid);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Parse the input into a `List` of `Coordinate`s and a `List` of
     * `Instruction`s.
     */
    private static DotsAndInstructions parse(final SolutionContext context) {
        final List<List<String>> input = context.readBatches();
        final List<Coordinate> dots = input.getFirst().stream()
                .map(Coordinate::parse)
                .toList();

        final List<Instruction> instructions = input.getLast().stream()
                .map(Instruction::parse)
                .toList();

        return new DotsAndInstructions(dots, instructions);
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    private record DotsAndInstructions(List<Coordinate> dots, List<Instruction> instructions) {

    }

    /*
     * The axis to fold along.
     */
    private enum Axis {

        X,
        Y,

        // End of constants
        ;

    }

    /*
     * An instruction on how to fold the paper.
     */
    private record Instruction(Axis axis, int value) {

        // Helper Methods

        /*
         * Transform the given `Point`.
         */
        Coordinate transform(final Coordinate dot) {
            if (axis == Axis.X) return new Coordinate(flip(dot.x(), value), dot.y());
            else if (axis == Axis.Y) return new Coordinate(dot.x(), flip(dot.y(), value));
            else throw new IllegalArgumentException("Invalid instruction!");
        }

        // Static Helper Methods

        /**
         * Create a new {@link Instruction} by parsing the line.
         *
         * @param instruction the instruction to parse
         * @return the parsed {@link Instruction}
         */
        static Instruction parse(final String instruction) {
            if (!instruction.startsWith("fold along "))
                throw new IllegalArgumentException("Invalid instruction: '" + instruction + "'");

            final String[] parts = instruction.split("\\s*=\\s*", 2);
            if (parts[0].endsWith("x")) return new Instruction(Axis.X, Integer.parseInt(parts[1]));
            else if (parts[0].endsWith("y")) return new Instruction(Axis.Y, Integer.parseInt(parts[1]));
            else throw new IllegalArgumentException("Failed to parse instruction: '" + instruction + "'");
        }

        /*
         * Flip the given point relative to the fold.
         */
        private static int flip(final int point, final int fold) {
            if (point <= fold) return point;
            else return fold - (point - fold);
        }

    }

}
