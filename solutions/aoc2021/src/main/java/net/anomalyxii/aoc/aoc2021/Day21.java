package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 21: Dirac Dice.
 */
@Solution(year = 2021, day = 21, title = "Dirac Dice")
public class Day21 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * There's not much to do as you slowly descend to the bottom of the ocean.
     * The submarine computer <span title="A STRANGE GAME.">challenges you to a nice game</span> of <i>Dirac Dice</i>.
     * <p>
     * This game consists of a single <a href="https://en.wikipedia.org/wiki/Dice" target="_blank">die</a>, two <a href="https://en.wikipedia.org/wiki/Glossary_of_board_games#piece" target="_blank">pawns</a>, and a game board with a circular track containing ten spaces marked <code>1</code> through <code>10</code> clockwise.
     * Each player's <i>starting space</i> is chosen randomly (your puzzle input).
     * Player 1 goes first.
     * <p>
     * Players take turns moving.
     * On each player's turn, the player rolls the die <i>three times</i> and adds up the results.
     * Then, the player moves their pawn that many times <i>forward</i> around the track (that is, moving clockwise on spaces in order of increasing value, wrapping back around to <code>1</code> after <code>10</code>).
     * So, if a player is on space <code>7</code> and they roll <code>2</code>, <code>2</code>, and <code>1</code>, they would move forward 5 times, to spaces <code>8</code>, <code>9</code>, <code>10</code>, <code>1</code>, and finally stopping on <code>2</code>.
     * <p>
     * After each player moves, they increase their <i>score</i> by the value of the space their pawn stopped on.
     * Players' scores start at <code>0</code>.
     * So, if the first player starts on space <code>7</code> and rolls a total of <code>5</code>, they would stop on space <code>2</code> and add <code>2</code> to their score (for a total score of <code>2</code>).
     * The game immediately ends as a win for any player whose score reaches <i>at least <code>1000</code></i>.
     * <p>
     * Since the first game is a practice game, the submarine opens a compartment labeled <i>deterministic dice</i> and a 100-sided die falls out.
     * This die always rolls <code>1</code> first, then <code>2</code>, then <code>3</code>, and so on up to <code>100</code>, after which it starts over at <code>1</code> again.
     * Play using this die.
     * <p>
     * For example, given these starting positions:
     * <pre>Player 1 starting position: 4
     * Player 2 starting position: 8
     * </pre>
     * <p>
     * This is how the game would go:
     * <ul>
     * <li> Player 1 rolls <code>1</code>+<code>2</code>+<code>3</code> and moves to space <code>10</code> for a total score of <code>10</code>. </li>
     * <li> Player 2 rolls <code>4</code>+<code>5</code>+<code>6</code> and moves to space <code>3</code> for a total score of <code>3</code>. </li>
     * <li> Player 1 rolls <code>7</code>+<code>8</code>+<code>9</code> and moves to space <code>4</code> for a total score of <code>14</code>. </li>
     * <li> Player 2 rolls <code>10</code>+<code>11</code>+<code>12</code> and moves to space <code>6</code> for a total score of <code>9</code>. </li>
     * <li> Player 1 rolls <code>13</code>+<code>14</code>+<code>15</code> and moves to space <code>6</code> for a total score of <code>20</code>. </li>
     * <li> Player 2 rolls <code>16</code>+<code>17</code>+<code>18</code> and moves to space <code>7</code> for a total score of <code>16</code>. </li>
     * <li> Player 1 rolls <code>19</code>+<code>20</code>+<code>21</code> and moves to space <code>6</code> for a total score of <code>26</code>. </li>
     * <li> Player 2 rolls <code>22</code>+<code>23</code>+<code>24</code> and moves to space <code>6</code> for a total score of <code>22</code>. </li>
     * </ul>
     * <p>
     * ...after many turns...
     * <ul>
     * <li> Player 2 rolls <code>82</code>+<code>83</code>+<code>84</code> and moves to space <code>6</code> for a total score of <code>742</code>. </li>
     * <li> Player 1 rolls <code>85</code>+<code>86</code>+<code>87</code> and moves to space <code>4</code> for a total score of <code>990</code>. </li>
     * <li> Player 2 rolls <code>88</code>+<code>89</code>+<code>90</code> and moves to space <code>3</code> for a total score of <code>745</code>. </li>
     * <li> Player 1 rolls <code>91</code>+<code>92</code>+<code>93</code> and moves to space <code>10</code> for a final score, <code>1000</code>. </li>
     * </ul>
     * <p>
     * Since player 1 has at least <code>1000</code> points, player 1 wins and the game ends.
     * At this point, the losing player had <code>745</code> points and the die had been rolled a total of <code>993</code> times; <code>745 * 993 = <i>739785</i></code>.
     * <p>
     * Play a practice game using the deterministic 100-sided die.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return The moment either player wins, what do you get if you multiply the score of the losing player by the number of times the die was rolled during the game?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        GameState state = loadInitialState(context);
        int turn = 0;
        do {
            final int roll = (6 + ((++turn - 1) * 9)) % 100;
            state = state.update(roll);
        } while (state.score1 < 1000 && state.score2 < 1000);

        return ((long) turn * 3) * Math.min(state.score1, state.score2);
    }

    /**
     * Now that you're warmed up, it's time to play the real game.
     * <p>
     * A second compartment opens, this time labeled <i>Dirac dice</i>.
     * Out of it falls a single three-sided die.
     * <p>
     * As you experiment with the die, you feel a little strange.
     * An informational brochure in the compartment explains that this is a <i>quantum die</i>: when you roll it, the universe <i>splits into multiple copies</i>, one copy for each possible outcome of the die.
     * In this case, rolling the die always splits the universe into <i>three copies</i>: one where the outcome of the roll was <code>1</code>, one where it was <code>2</code>, and one where it was <code>3</code>.
     * <p>
     * The game is played the same as before, although to prevent things from getting too far out of hand, the game now ends when either player's score reaches at least <code><i>21</i></code>.
     * <p>
     * Using the same starting positions as in the example above, player 1 wins in <code><i>444356092776315</i></code> universes, while player 2 merely wins in <code>341960390180808</code> universes.
     * <p>
     * Using your given starting positions, determine every possible outcome.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Find the player that wins in more universes; in how many universes does that player win?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final GameState state = loadInitialState(context);
        final DiracResult result = playQuantumGame(state, new HashMap<>());

        return Math.max(result.player1GamesWon, result.player2GamesWon);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Load the initial game state.
     */
    private GameState loadInitialState(final SolutionContext context) {
        final List<String> lines = context.read();

        final String first = lines.getFirst();
        final String second = lines.getLast();
        final int player1Pos = Integer.parseInt(first.substring(first.indexOf(':') + 2));
        final int player2Pos = Integer.parseInt(second.substring(second.indexOf(':') + 2));

        return new GameState(true, player1Pos, 0, player2Pos, 0);
    }

    /*
     * Play a game using the quantum die, caching the result.
     */
    private DiracResult playQuantumGame(final GameState state, final Map<GameState, DiracResult> cache) {
        final DiracResult cached = cache.get(state);
        if (cached != null) return cached;

        final DiracResult result = resolveState(state, cache);
        cache.put(state, result);
        return result;
    }

    /*
     * Play a game using the quantum die.
     */
    private DiracResult resolveState(final GameState state, final Map<GameState, DiracResult> cache) {
        if (state.isP1Turn && state.score1 >= 20) return new DiracResult(27, 0);
        if (!state.isP1Turn && state.score2 >= 20) return new DiracResult(0, 27);

        // 1 1 1
        // 1 1 2    ||    1 2 1    ||    2 1 1
        // 1 1 3    ||    1 3 1    ||    3 1 1    ||    1 2 2    ||    2 1 2    ||    2 2 1
        // 1 2 3    ||    2 1 3    ||    1 3 2    ||    2 3 1    ||    3 2 1    ||    3 1 2    ||    2 2 2
        // 1 3 3    ||    3 1 3    ||    3 3 1    ||    2 2 3    ||    2 3 2    ||    3 2 2
        // 2 3 3    ||    3 2 3    ||    3 3 2
        // 3 3 3
        final DiracResult r3 = advanceQuantumGame(3, state, cache);
        final DiracResult r4 = advanceQuantumGame(4, state, cache);
        final DiracResult r5 = advanceQuantumGame(5, state, cache);
        final DiracResult r6 = advanceQuantumGame(6, state, cache);
        final DiracResult r7 = advanceQuantumGame(7, state, cache);
        final DiracResult r8 = advanceQuantumGame(8, state, cache);
        final DiracResult r9 = advanceQuantumGame(9, state, cache);

        return new DiracResult(
                r3.player1GamesWon
                        + (r4.player1GamesWon * 3)
                        + (r5.player1GamesWon * 6)
                        + (r6.player1GamesWon * 7)
                        + (r7.player1GamesWon * 6)
                        + (r8.player1GamesWon * 3)
                        + r9.player1GamesWon,
                r3.player2GamesWon
                        + (r4.player2GamesWon * 3)
                        + (r5.player2GamesWon * 6)
                        + (r6.player2GamesWon * 6)
                        + (r7.player2GamesWon * 6)
                        + (r8.player2GamesWon * 3)
                        + r9.player2GamesWon
        );
    }

    /*
     * Advance a game being played with a quantum die.
     */
    private DiracResult advanceQuantumGame(final int roll, final GameState state, final Map<GameState, DiracResult> cache) {
        if (state.isP1Turn) {
            final int newPosition = calculateNewPos(state.position1, roll);
            final int newScore = state.score1 + newPosition;
            if (newScore >= 21) return new DiracResult(1, 0);
            return playQuantumGame(new GameState(false, newPosition, newScore, state.position2, state.score2), cache);
        } else {
            final int newPosition = calculateNewPos(state.position2, roll);
            final int newScore = state.score2 + newPosition;
            if (newScore >= 21) return new DiracResult(0, 1);
            return playQuantumGame(new GameState(true, state.position1, state.score1, newPosition, newScore), cache);
        }
    }

    /*
     * Calculate the new position of a player.
     */
    private static int calculateNewPos(final int pos, final int roll) {
        final int totalPos = pos + roll;
        return ((totalPos - 1) % 10) + 1;
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * The state of the game at any given point.
     */
    private record GameState(boolean isP1Turn, int position1, int score1, int position2, int score2) {

        // Helper Methods

        GameState update(final int roll) {
            if (isP1Turn) {
                final int newPosition = calculateNewPos(position1, roll);
                final int newScore = score1 + newPosition;
                return new GameState(false, newPosition, newScore, position2, score2);
            } else {
                final int newPosition = calculateNewPos(position2, roll);
                final int newScore = score2 + newPosition;
                return new GameState(true, position1, score1, newPosition, newScore);
            }
        }

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final GameState gameState = (GameState) o;
            return isP1Turn == gameState.isP1Turn
                    && position1 == gameState.position1
                    && score1 == gameState.score1
                    && position2 == gameState.position2
                    && score2 == gameState.score2;
        }

        @Override
        public int hashCode() {
            return Objects.hash(isP1Turn, position1, score1, position2, score2);
        }

    }

    /*
     * The result of playing one, universe splitting, game.
     */
    private record DiracResult(long player1GamesWon, long player2GamesWon) {
    }

}
