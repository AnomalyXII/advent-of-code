package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid.MutableGrid;

import java.util.concurrent.atomic.AtomicLong;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 11: Dumbo Octopus.
 */
@Solution(year = 2021, day = 11, title = "Dumbo Octopus")
public class Day11 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You enter a large cavern full of rare bioluminescent <a href="https://www.youtube.com/watch?v=eih-VSaS2g0">dumbo octopuses</a>!
     * They seem to not like the Christmas lights on your submarine, so you turn them off for now.
     * <p>
     * There are 100 <span title="I know it's weird; I grew saying 'octopi' too.">octopuses</span> arranged neatly in a 10 by 10 grid.
     * Each octopus slowly gains <i>energy</i> over time and <i>flashes</i> brightly for a moment when its energy is full.
     * Although your lights are off, maybe you could navigate through the cave without disturbing the octopuses if you could predict when the flashes of light will happen.
     * <p>
     * Each octopus has an <i>energy level</i> - your submarine can remotely measure the energy level of each octopus (your puzzle input).
     * For example:
     * <pre>
     * 5483143223
     * 2745854711
     * 5264556173
     * 6141336146
     * 6357385478
     * 4167524645
     * 2176841721
     * 6882881134
     * 4846848554
     * 5283751526
     * </pre>
     * <p>
     * The energy level of each octopus is a value between <code>0</code> and <code>9</code>.
     * Here, the top-left octopus has an energy level of <code>5</code>, the bottom-right one has an energy level of <code>6</code>, and so on.
     * <p>
     * You can model the energy levels and flashes of light in <i>steps</i>. During a single step, the following occurs:
     * <ul>
     * <li> First, the energy level of each octopus increases by <code>1</code>. </li>
     * <li>
     * Then, any octopus with an energy level greater than <code>9</code> <i>flashes</i>.
     * This increases the energy level of all adjacent octopuses by <code>1</code>, including octopuses that are diagonally adjacent.
     * If this causes an octopus to have an energy level greater than <code>9</code>, it <i>also flashes</i>.
     * This process continues as long as new octopuses keep having their energy level increased beyond <code>9</code>.
     * (An octopus can only flash <i>at most once per step</i>.)
     * </li>
     * <li> Finally, any octopus that flashed during this step has its energy level set to <code>0</code>, as it used all of its energy to flash. </li>
     * </ul>
     * <p>
     * Adjacent flashes can cause an octopus to flash on a step even if it begins that step with very little energy.
     * Consider the middle octopus with <code>1</code> energy in this situation:
     * <pre>
     * Before any steps:
     * 11111
     * 19991
     * 19191
     * 19991
     * 11111
     *
     * After step 1:
     * 34543
     * 4<i>000</i>4
     * 5<i>000</i>5
     * 4<i>000</i>4
     * 34543
     *
     * After step 2:
     * 45654
     * 51115
     * 61116
     * 51115
     * 45654
     * </pre>
     * <p>
     * An octopus is <i>highlighted</i> when it flashed during the given step.
     * <p>
     * Here is how the larger example above progresses:
     * <pre>
     * Before any steps:
     * 5483143223
     * 2745854711
     * 5264556173
     * 6141336146
     * 6357385478
     * 4167524645
     * 2176841721
     * 6882881134
     * 4846848554
     * 5283751526
     *
     * After step 1:
     * 6594254334
     * 3856965822
     * 6375667284
     * 7252447257
     * 7468496589
     * 5278635756
     * 3287952832
     * 7993992245
     * 5957959665
     * 6394862637
     *
     * After step 2:
     * 88<i>0</i>7476555
     * 5<i>0</i>89<i>0</i>87<i>0</i>54
     * 85978896<i>0</i>8
     * 84857696<i>00</i>
     * 87<i>00</i>9<i>0</i>88<i>00</i>
     * 66<i>000</i>88989
     * 68<i>0000</i>5943
     * <i>000000</i>7456
     * 9<i>000000</i>876
     * 87<i>0000</i>6848
     *
     * After step 3:
     * <i>00</i>5<i>0</i>9<i>00</i>866
     * 85<i>00</i>8<i>00</i>575
     * 99<i>000000</i>39
     * 97<i>000000</i>41
     * 9935<i>0</i>8<i>00</i>63
     * 77123<i>00000</i>
     * 791125<i>000</i>9
     * 221113<i>0000</i>
     * <i>0</i>421125<i>000</i>
     * <i>00</i>21119<i>000</i>
     *
     * After step 4:
     * 2263<i>0</i>31977
     * <i>0</i>923<i>0</i>31697
     * <i>00</i>3222115<i>0</i>
     * <i>00</i>41111163
     * <i>00</i>76191174
     * <i>00</i>53411122
     * <i>00</i>4236112<i>0</i>
     * 5532241122
     * 1532247211
     * 113223<i>0</i>211
     *
     * After step 5:
     * 4484144<i>000</i>
     * 2<i>0</i>44144<i>000</i>
     * 2253333493
     * 1152333274
     * 11873<i>0</i>3285
     * 1164633233
     * 1153472231
     * 6643352233
     * 2643358322
     * 2243341322
     *
     * After step 6:
     * 5595255111
     * 3155255222
     * 33644446<i>0</i>5
     * 2263444496
     * 2298414396
     * 2275744344
     * 2264583342
     * 7754463344
     * 3754469433
     * 3354452433
     *
     * After step 7:
     * 67<i>0</i>7366222
     * 4377366333
     * 4475555827
     * 34966557<i>0</i>9
     * 35<i>00</i>6256<i>0</i>9
     * 35<i>0</i>9955566
     * 3486694453
     * 8865585555
     * 486558<i>0</i>644
     * 4465574644
     *
     * After step 8:
     * 7818477333
     * 5488477444
     * 5697666949
     * 46<i>0</i>876683<i>0</i>
     * 473494673<i>0</i>
     * 474<i>00</i>97688
     * 69<i>0000</i>7564
     * <i>000000</i>9666
     * 8<i>00000</i>4755
     * 68<i>0000</i>7755
     *
     * After step 9:
     * 9<i>0</i>6<i>0000</i>644
     * 78<i>00000</i>976
     * 69<i>000000</i>8<i>0</i>
     * 584<i>00000</i>82
     * 5858<i>0000</i>93
     * 69624<i>00000</i>
     * 8<i>0</i>2125<i>000</i>9
     * 222113<i>000</i>9
     * 9111128<i>0</i>97
     * 7911119976
     *
     * After step 10:
     * <i>0</i>481112976
     * <i>00</i>31112<i>00</i>9
     * <i>00</i>411125<i>0</i>4
     * <i>00</i>811114<i>0</i>6
     * <i>00</i>991113<i>0</i>6
     * <i>00</i>93511233
     * <i>0</i>44236113<i>0</i>
     * 553225235<i>0</i>
     * <i>0</i>53225<i>0</i>6<i>00</i>
     * <i>00</i>3224<i>0000</i>
     * </pre>
     * <p>
     * After step 10, there have been a total of <code>204</code> flashes.
     * Fast forwarding, here is the same configuration every 10 steps:
     *
     * <pre>
     * After step 20:
     * 3936556452
     * 56865568<i>0</i>6
     * 449655569<i>0</i>
     * 444865558<i>0</i>
     * 445686557<i>0</i>
     * 568<i>00</i>86577
     * 7<i>00000</i>9896
     * <i>0000000</i>344
     * 6<i>000000</i>364
     * 46<i>0000</i>9543
     *
     * After step 30:
     * <i>0</i>643334118
     * 4253334611
     * 3374333458
     * 2225333337
     * 2229333338
     * 2276733333
     * 2754574565
     * 5544458511
     * 9444447111
     * 7944446119
     *
     * After step 40:
     * 6211111981
     * <i>0</i>421111119
     * <i>00</i>42111115
     * <i>000</i>3111115
     * <i>000</i>3111116
     * <i>00</i>65611111
     * <i>0</i>532351111
     * 3322234597
     * 2222222976
     * 2222222762
     *
     * After step 50:
     * 9655556447
     * 48655568<i>0</i>5
     * 448655569<i>0</i>
     * 445865558<i>0</i>
     * 457486557<i>0</i>
     * 57<i>000</i>86566
     * 6<i>00000</i>9887
     * 8<i>000000</i>533
     * 68<i>00000</i>633
     * 568<i>0000</i>538
     *
     * After step 60:
     * 25333342<i>00</i>
     * 274333464<i>0</i>
     * 2264333458
     * 2225333337
     * 2225333338
     * 2287833333
     * 3854573455
     * 1854458611
     * 1175447111
     * 1115446111
     *
     * After step 70:
     * 8211111164
     * <i>0</i>421111166
     * <i>00</i>42111114
     * <i>000</i>4211115
     * <i>0000</i>211116
     * <i>00</i>65611111
     * <i>0</i>532351111
     * 7322235117
     * 5722223475
     * 4572222754
     *
     * After step 80:
     * 1755555697
     * 59655556<i>0</i>9
     * 448655568<i>0</i>
     * 445865558<i>0</i>
     * 457<i>0</i>86557<i>0</i>
     * 57<i>000</i>86566
     * 7<i>00000</i>8666
     * <i>0000000</i>99<i>0</i>
     * <i>0000000</i>8<i>00</i>
     * <i>0000000000</i>
     *
     * After step 90:
     * 7433333522
     * 2643333522
     * 2264333458
     * 2226433337
     * 2222433338
     * 2287833333
     * 2854573333
     * 4854458333
     * 3387779333
     * 3333333333
     *
     * After step 100:
     * <i>0</i>397666866
     * <i>0</i>749766918
     * <i>00</i>53976933
     * <i>000</i>4297822
     * <i>000</i>4229892
     * <i>00</i>53222877
     * <i>0</i>532222966
     * 9322228966
     * 7922286866
     * 6789998766
     * </pre>
     * <p>
     * After 100 steps, there have been a total of <code><i>1656</i></code> flashes.
     * <p>
     * Given the starting energy levels of the dumbo octopuses in your cavern, simulate 100 steps.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many total flashes are there after 100 steps?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final MutableGrid octopi = context.readMutableGrid(chr -> chr - '0');

        final AtomicLong flashes = new AtomicLong(0L);
        for (int i = 0; i < 100; i++) {
            pulse(octopi, flashes::incrementAndGet);
        }
        return flashes.longValue();
    }

    /**
     * It seems like the individual flashes aren't bright enough to navigate.
     * However, you might have a better option: the flashes seem to be <i>synchronizing</i>!
     * <p>
     * In the example above, the first time all octopuses flash simultaneously is step <code><i>195</i></code>:
     * <pre>
     * After step 193:
     * 5877777777
     * 8877777777
     * 7777777777
     * 7777777777
     * 7777777777
     * 7777777777
     * 7777777777
     * 7777777777
     * 7777777777
     * 7777777777
     *
     * After step 194:
     * 6988888888
     * 9988888888
     * 8888888888
     * 8888888888
     * 8888888888
     * 8888888888
     * 8888888888
     * 8888888888
     * 8888888888
     * 8888888888
     *
     * After step 195:
     * <i>0000000000</i>
     * <i>0000000000</i>
     * <i>0000000000</i>
     * <i>0000000000</i>
     * <i>0000000000</i>
     * <i>0000000000</i>
     * <i>0000000000</i>
     * <i>0000000000</i>
     * <i>0000000000</i>
     * <i>0000000000</i>
     * </pre>
     * <p>
     * If you can calculate the exact moments when the octopuses will all flash simultaneously, you should be able to navigate through the cavern.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the first step during which all octopuses flash?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final MutableGrid octopi = context.readMutableGrid(chr -> chr - '0');

        long turn = 0;
        boolean inSync;
        do {
            final AtomicLong flashes = new AtomicLong(0L);
            pulse(octopi, flashes::incrementAndGet);
            inSync = flashes.intValue() == (octopi.area());

            ++turn;
        } while (!inSync);

        return turn;
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Increase the power level of every octopus by 1. Trigger a flash if the
     * power level of an octopus is over 9.
     */
    private static void pulse(final MutableGrid octopi, final Runnable onFlash) {
        // Increase all power levels by 1
        octopi.forEach(p -> increasePowerLevel(octopi, p));

        // Flash (ah-ha):
        final boolean[] gordonsAlive = new boolean[1];
        do {
            gordonsAlive[0] = false;
            octopi.forEach(p -> {
                if (octopi.get(p) == 10) {
                    octopi.set(p, 11);
                    octopi.forEachNeighbourOf(p, o -> increasePowerLevel(octopi, o));
                    gordonsAlive[0] = true;
                }
            });
        } while (gordonsAlive[0]);

        // Increase all power levels by 1
        octopi.forEach(p -> {
            if (octopi.get(p) >= 10) {
                onFlash.run();
                resetPowerLevel(octopi, p);
            }
        });
    }

    /*
     * Increase the power level of an octopus, assuming it is less than 9.
     */
    private static void increasePowerLevel(final MutableGrid octopi, final Coordinate coordinate) {
        octopi.set(coordinate, octopi.get(coordinate) > 9 ? octopi.get(coordinate) : octopi.get(coordinate) + 1);
    }

    /*
     * Reset the power level of an octopus to 0.
     */
    private static void resetPowerLevel(final MutableGrid octopi, final Coordinate coordinate) {
        octopi.set(coordinate, 0);
    }

}
