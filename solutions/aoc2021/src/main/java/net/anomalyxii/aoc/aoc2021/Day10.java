package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.OptionalLong;
import java.util.function.Function;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 10: Syntax Scoring.
 */
@Solution(year = 2021, day = 10, title = "Syntax Scoring")
public class Day10 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You ask the submarine to determine the best route out of the deep-sea cave, but it only replies:
     * <pre>
     * Syntax error in navigation subsystem on line: all of them
     * </pre>
     * <p>
     * <i>All of them?!</i>
     * The damage is worse than you thought.
     * You bring up a copy of the navigation subsystem (your puzzle input).
     * <p>
     * The navigation subsystem syntax is made of several lines containing <i>chunks</i>.
     * There are one or more chunks on each line, and chunks contain zero or more other chunks.
     * Adjacent chunks are not separated by any delimiter; if one chunk stops, the next chunk (if any) can immediately start.
     * Every chunk must <i>open</i> and <i>close</i> with one of four legal pairs of matching characters:
     * <ul>
     * <li> If a chunk opens with <code>(</code>, it must close with <code>)</code>. </li>
     * <li> If a chunk opens with <code>[</code>, it must close with <code>]</code>. </li>
     * <li> If a chunk opens with <code>{</code>, it must close with <code>}</code>. </li>
     * <li> If a chunk opens with <code>&lt;</code>, it must close with <code>&gt;</code>. </li>
     * </ul>
     * <p>
     * So, <code>()</code> is a legal chunk that contains no other chunks, as is <code>[]</code>.
     * More complex but valid chunks include <code>([])</code>, <code>{()()()}</code>, <code>&lt;([{}])&gt;</code>, <code>[&lt;&gt;({}){}[([])&lt;&gt;]]</code>, and even <code>(((((((((())))))))))</code>.
     * <p>
     * Some lines are <i>incomplete</i>, but others are <i>corrupted</i>.
     * Find and discard the corrupted lines first.
     * <p>
     * A corrupted line is one where a chunk <i>closes with the wrong character</i> - that is, where the characters it opens and closes with do not form one of the four legal pairs listed above.
     * <p>
     * Examples of corrupted chunks include <code>(]</code>, <code>{()()()&gt;</code>, <code>(((()))}</code>, and <code>&lt;([]){()}[{}])</code>.
     * Such a chunk can appear anywhere within a line, and its presence causes the whole line to be considered corrupted.
     * <p>
     * For example, consider the following navigation subsystem:
     * <pre>
     * [({(&lt;(())[]&gt;[[{[]{&lt;()&lt;&gt;&gt;
     * [(()[&lt;&gt;])]({[&lt;{&lt;&lt;[]&gt;&gt;(
     * {([(&lt;{}[&lt;&gt;[]}&gt;{[]{[(&lt;()&gt;
     * (((({&lt;&gt;}&lt;{&lt;{&lt;&gt;}{[]{[]{}
     * [[&lt;[([]))&lt;([[{}[[()]]]
     * [{[{({}]{}}([{[{{{}}([]
     * {&lt;[[]]&gt;}&lt;{[{[{[]{()[[[]
     * [&lt;(&lt;(&lt;(&lt;{}))&gt;&lt;([]([]()
     * &lt;{([([[(&lt;&gt;()){}]&gt;(&lt;&lt;{{
     * &lt;{([{{}}[&lt;[[[&lt;&gt;{}]]]&gt;[]]
     * </pre>
     * <p>
     * Some of the lines aren't corrupted, just incomplete; you can ignore these lines for now.
     * The remaining five lines are corrupted:
     * <ul>
     * <li> <code>{([(&lt;{}[&lt;&gt;[]}>{[]{[(&lt;()></code> - Expected <code>]</code>, but found <code>}</code> instead. </li>
     * <li> <code>[[&lt;[([]))&lt;([[{}[[()]]]</code> - Expected <code>]</code>, but found <code>)</code> instead. </li>
     * <li> <code>[{[{({}]{}}([{[{{{}}([]</code> - Expected <code>)</code>, but found <code>]</code> instead. </li>
     * <li> <code>[&lt;(&lt;(&lt;(&lt;{}))&gt;&lt;([]([]()</code> - Expected <code>&gt;</code>, but found <code>)</code> instead. </li>
     * <li> <code>&lt;{([([[(&lt;&gt;()){}]&gt;(&lt;&lt;{{</code> - Expected <code>]</code>, but found <code>&gt;</code> instead. </li>
     * </ul>
     * <p>
     * Stop at the first incorrect closing character on each corrupted line.
     * <p>
     * Did you know that syntax checkers actually have contests to see who can get the high score for syntax errors in a file?
     * It's true!
     * To calculate the syntax error score for a line, take the <i>first illegal character</i> on the line and look it up in the following table:
     * <ul>
     * <li> <code>)</code>: <code>3</code> points. </li>
     * <li> <code>]</code>: <code>57</code> points. </li>
     * <li> <code>}</code>: <code>1197</code> points. </li>
     * <li> <code>&gt;</code>: <code>25137</code> points. </li>
     * </ul>
     * <p>
     * In the above example, an illegal <code>)</code> was found twice (<code>2*3 = <i>6</i></code> points), an illegal <code>]</code> was found once (<i><code>57</code></i> points), an illegal <code>}</code> was found once (<i><code>1197</code></i> points), and an illegal <code>&gt;</code> was found once (<i><code>25137</code></i> points).
     * So, the total syntax error score for this file is <code>6+57+1197+25137 = <i>26397</i></code> points!
     * <p>
     * Find the first illegal character in each corrupted line of the navigation subsystem.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the total syntax error score for those errors?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<OptionalLong> maybeScores = context.process(line -> processLine(
                line,
                corrupted -> OptionalLong.of(corrupted.corruptedScore()),
                incomplete -> OptionalLong.empty()
        ));

        return maybeScores.stream()
                .filter(OptionalLong::isPresent)
                .mapToLong(OptionalLong::getAsLong)
                .sum();
    }

    /**
     * Now, discard the corrupted lines.
     * The remaining lines are <i>incomplete</i>.
     * <p>
     * Incomplete lines don't have any incorrect characters - instead, they're missing some closing characters at the end of the line.
     * To repair the navigation subsystem, you just need to figure out <i>the sequence of closing characters</i> that complete all open chunks in the line.
     * <p>
     * You can only use closing characters (<code>)</code>, <code>]</code>, <code>}</code>, or <code>&gt;</code>), and you must add them in the correct order so that only legal pairs are formed and all chunks end up closed.
     * <p>
     * In the example above, there are five incomplete lines:
     * <ul>
     * <li> <code>[({(&lt;(())[]&gt;[[{[]{&lt;()&lt;&gt;&gt;</code> - Complete by adding <code>}}]])})]</code>. </li>
     * <li> <code>[(()[&lt;&gt;])]({[&lt;{&lt;&lt;[]&gt;&gt;(</code> - Complete by adding <code>)}&gt;]})</code>. </li>
     * <li> <code>(((({&lt;&gt;}&lt;{&lt;{&lt;&gt;}{[]{[]{}</code> - Complete by adding <code>}}&gt;}&gt;))))</code>. </li>
     * <li> <code>{&lt;[[]]&gt;}&lt;{[{[{[]{()[[[]</code> - Complete by adding <code>]]}}]}]}&gt;</code>. </li>
     * <li> <code>&lt;{([{{}}[&lt;[[[&lt;&gt;{}]]]&gt;[]]</code> - Complete by adding <code>])}&gt;</code>. </li>
     * </ul>
     * <p>
     * Did you know that autocomplete tools <i>also</i> have contests?
     * It's true!
     * The score is determined by considering the completion string character-by-character.
     * Start with a total score of <code>0</code>.
     * Then, for each character, multiply the total score by <code>5</code> and then increase the total score by the point value given for the character in the following table:
     * <ul>
     * <li> <code>)</code>: <code>1</code> point. </li>
     * <li> <code>]</code>: <code>2</code> points. </li>
     * <li> <code>}</code>: <code>3</code> points. </li>
     * <li> <code>></code>: <code>4</code> points. </li>
     * </ul>
     * <p>
     * So, the last completion string above - <code>])}&gt;</code> - would be scored as follows:
     * <ul>
     * <li> Start with a total score of <code>0</code>. </li>
     * <li> Multiply the total score by <code>5</code> to get <code>0</code>, then add the value of <code>]</code> (2) to get a new total score of <code>2</code>. </li>
     * <li> Multiply the total score by <code>5</code> to get <code>10</code>, then add the value of <code>)</code> (1) to get a new total score of <code>11</code>. </li>
     * <li> Multiply the total score by <code>5</code> to get <code>55</code>, then add the value of <code>}</code> (3) to get a new total score of <code>58</code>. </li>
     * <li> Multiply the total score by <code>5</code> to get <code>290</code>, then add the value of <code>></code> (4) to get a new total score of <code>294</code>. </li>
     * </ul>
     * <p>
     * The five lines' completion strings have total scores as follows:
     * <ul>
     * <li> <code> }}]])})]</code> - <code>288957</code> total points. </li>
     * <li> <code> )}&gt;]})</code> - <code>5566</code> total points. </li>
     * <li> <code> }}&gt;}&gt;))))</code> - <code>1480781</code> total points. </li>
     * <li> <code> ]]}}]}]}&gt;</code> - <code>995444</code> total points. </li>
     * <li> <code> ])}&gt;</code> - <code>294</code> total points. </li>
     * </ul>
     * <p>
     * Autocomplete tools are an odd bunch: the winner is found by <i>sorting</i> all of the scores and then taking the <i>middle</i> score.
     * (There will always be an odd number of scores to consider.)
     * In this example, the middle score is <i><code>288957</code></i> because there are the same number of scores smaller and larger than it.
     * <p>
     * Find the completion string for each incomplete line, score the completion strings, and sort the scores.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the middle score?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<OptionalLong> maybeScores = context.process(line -> processLine(
                line,
                corrupted -> OptionalLong.empty(),
                incomplete -> OptionalLong.of(Stream.of(incomplete)
                                                      .mapToLong(CloseBracket::incompleteScore)
                                                      .reduce(0L, (result, next) -> (result * 5) + next))
        ));

        final long[] scores = maybeScores.stream()
                .filter(OptionalLong::isPresent)
                .mapToLong(OptionalLong::getAsLong)
                .sorted()
                .toArray();
        return scores[scores.length / 2];
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Process a line containing a syntax error.s
     */
    private static OptionalLong processLine(
            final String line,
            final Function<CloseBracket, OptionalLong> corrupted,
            final Function<CloseBracket[], OptionalLong> incomplete
    ) {

        final Deque<OpenBracket> stack = new ArrayDeque<>();
        for (final char chr : line.toCharArray()) {
            final Bracket bracket = Bracket.parse(chr);
            if (bracket.isOpen()) {
                stack.addFirst((OpenBracket) bracket);
                continue;
            }

            final OpenBracket prev = stack.removeFirst();
            if (prev.complements(bracket)) continue;

            return corrupted.apply((CloseBracket) bracket);
        }

        final CloseBracket[] missingChars = stack.stream()
                .map(OpenBracket::complement)
                .toArray(CloseBracket[]::new);
        return incomplete.apply(missingChars);

    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * Represents a type of bracket.
     */
    private interface Bracket {

        // Interface Methods

        /*
         * Return the complementing `Bracket`.
         */
        Bracket complement();

        /*
         * Check if this is an opening or closing `Bracket`.
         */
        boolean isOpen();

        /*
         * Check if the given `Bracket` is the complement of this one.
         */
        default boolean complements(final Bracket other) {
            return complement().equals(other);
        }

        // Static Helper Methods

        /*
         * Parse the bracket character.
         */
        static Bracket parse(final char chr) {
            return switch (chr) {
                case '(' -> OpenBracket.PARENTHESIS;
                case ')' -> CloseBracket.PARENTHESIS;
                case '[' -> OpenBracket.SQUARE;
                case ']' -> CloseBracket.SQUARE;
                case '{' -> OpenBracket.BRACE;
                case '}' -> CloseBracket.BRACE;
                case '<' -> OpenBracket.ANGLE;
                case '>' -> CloseBracket.ANGLE;
                default -> throw new IllegalStateException("Invalid bracket: '" + chr + "'");
            };
        }
    }

    /*
     * An opening bracket: `(`, `[`, `{`, `<`.
     */
    private enum OpenBracket implements Bracket {

        PARENTHESIS,
        SQUARE,
        BRACE,
        ANGLE,

        // End of constants
        ;

        // Bracket Methods

        @Override
        public CloseBracket complement() {
            return CloseBracket.valueOf(name()); // Bit hacky, but we get circular references
        }

        @Override
        public boolean isOpen() {
            return true;
        }
    }

    /*
     * A closing bracket: `)`, `]`, `}`, `>`.
     */
    private enum CloseBracket implements Bracket {

        PARENTHESIS(3, 1),
        SQUARE(57, 2),
        BRACE(1197, 3),
        ANGLE(25137, 4),

        // End of constants
        ;

        private final long corruptedScore;
        private final long incompleteScore;

        // Constructors

        CloseBracket(final long corruptedScore, final long incompleteScore) {
            this.corruptedScore = corruptedScore;
            this.incompleteScore = incompleteScore;
        }

        // Bracket Methods

        @Override
        public OpenBracket complement() {
            return OpenBracket.valueOf(name()); // Bit hacky, but we get circular references
        }

        @Override
        public boolean isOpen() {
            return false;
        }

        // Helper Methods

        /*
         * Get the corrupted score for this `Bracket`.
         */
        long corruptedScore() {
            return corruptedScore;
        }

        /*
         * Get the incomplete score for this `Bracket`.
         */
        long incompleteScore() {
            return incompleteScore;
        }

    }

}
