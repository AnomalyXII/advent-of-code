package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 8: Seven Segment Search.
 */
@Solution(year = 2021, day = 8, title = "Seven Segment Search")
public class Day8 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You barely reach the safety of the cave when the whale smashes into the cave mouth, collapsing it.
     * Sensors indicate another exit to this cave at a much greater depth, so you have no choice but to press on.
     * <p>
     * As your submarine slowly makes its way through the cave system, you notice that the four-digit <a href="https://en.wikipedia.org/wiki/Seven-segment_display">seven-segment displays</a> in your submarine are malfunctioning; they must have been damaged during the escape.
     * You'll be in a lot of trouble without them, so you'd better figure out what's wrong.
     * <p>
     * Each digit of a seven-segment display is rendered by turning on or off any of seven segments named <code>a</code> through <code>g</code>:
     * <pre>
     *   0:      1:      2:      3:      4:
     *  aaaa    ....    aaaa    aaaa    ....
     * b    c  .    c  .    c  .    c  b    c
     * b    c  .    c  .    c  .    c  b    c
     *  ....    ....    dddd    dddd    dddd
     * e    f  .    f  e    .  .    f  .    f
     * e    f  .    f  e    .  .    f  .    f
     *  gggg    ....    gggg    gggg    ....
     *
     *   5:      6:      7:      8:      9:
     *  aaaa    aaaa    aaaa    aaaa    aaaa
     * b    .  b    .  .    c  b    c  b    c
     * b    .  b    .  .    c  b    c  b    c
     *  dddd    dddd    ....    dddd    dddd
     * .    f  e    f  .    f  e    f  .    f
     * .    f  e    f  .    f  e    f  .    f
     *  gggg    gggg    ....    gggg    gggg
     * </pre>
     * <p>
     * So, to render a <code>1</code>, only segments <code>c</code> and <code>f</code> would be turned on; the rest would be off.
     * To render a <code>7</code>, only segments <code>a</code>, <code>c</code>, and <code>f</code> would be turned on.
     * <p>
     * The problem is that the signals which control the segments have been mixed up on each display.
     * The submarine is still trying to display numbers by producing output on signal wires <code>a</code> through <code>g</code>, but those wires are connected to segments <i>randomly</i>.
     * Worse, the wire/segment connections are mixed up separately for each four-digit display!
     * (All of the digits <i>within</i> a display use the same connections, though.)
     * <p>
     * So, you might know that only signal wires <code>b</code> and <code>g</code> are turned on, but that doesn't mean <i>segments</i> <code>b</code> and <code>g</code> are turned on: the only digit that uses two segments is <code>1</code>, so it must mean segments <code>c</code> and <code>f</code> are meant to be on.
     * With just that information, you still can't tell which wire (<code>b</code>/<code>g</code>) goes to which segment (<code>c</code>/<code>f</code>).
     * For that, you'll need to collect more information.
     * <p>
     * For each display, you watch the changing signals for a while, make a note of <i>all ten unique signal patterns</i> you see, and then write down a single <i>four digit output value</i> (your puzzle input).
     * Using the signal patterns, you should be able to work out which pattern corresponds to which digit.
     * <p>
     * For example, here is what you might see in a single entry in your notes:
     * <pre>
     * acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
     * cdfeb fcadb cdfeb cdbaf
     * </pre>
     * <p>
     * (The entry is wrapped here to two lines so it fits; in your notes, it will all be on a single line.)
     * <p>
     * Each entry consists of ten <i>unique signal patterns</i>, a <code>|</code> delimiter, and finally the <i>four digit output value</i>.
     * Within an entry, the same wire/segment connections are used (but you don't know what the connections actually are).
     * The unique signal patterns correspond to the ten different ways the submarine tries to render a digit using the current wire/segment connections.
     * Because <code>7</code> is the only digit that uses three segments, <code>dab</code> in the above example means that to render a <code>7</code>, signal lines <code>d</code>, <code>a</code>, and <code>b</code> are on.
     * Because <code>4</code> is the only digit that uses four segments, <code>eafb</code> means that to render a <code>4</code>, signal lines <code>e</code>, <code>a</code>, <code>f</code>, and <code>b</code> are on.
     * <p>
     * Using this information, you should be able to work out which combination of signal wires corresponds to each of the ten digits.
     * Then, you can decode the four digit output value. Unfortunately, in the above example, all of the digits in the output value (<code>cdfeb fcadb cdfeb cdbaf</code>) use five segments and are more difficult to deduce.
     * <p>
     * For now, <i>focus on the easy digits</i>.
     * Consider this larger example:
     * <pre>
     * be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb |
     * fdgacbe cefdb cefbgd gcbe
     * edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec |
     * fcgedb cgb dgebacf gc
     * fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef |
     * cg cg fdcagb cbg
     * fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega |
     * efabcd cedba gadfec cb
     * aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga |
     * gecf egdcabf bgf bfgea
     * fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf |
     * gebdcfa ecba ca fadegcb
     * dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf |
     * cefg dcbef fcge gbcadfe
     * bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd |
     * ed bcgafe cdgba cbgef
     * egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg |
     * gbdfcae bgc cg cgb
     * gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc |
     * fgae cfgab fg bagce
     * </pre>
     * <p>
     * Because the digits <code>1</code>, <code>4</code>, <code>7</code>, and <code>8</code> each use a unique number of segments, you should be able to tell which combinations of signals correspond to those digits.
     * <i>Counting only digits in the output values</i> (the part after <code>|</code> on each line), in the above example, there are <i><code>26</code></i> instances of digits that use a unique number of segments (highlighted above).
     *
     * @param context the {@link SolutionContext} to solve against
     * @return In the output values, how many times do digits 1, 4, 7, or 8 appear?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        // Ignore the left hand side for now?
        final List<Long> counts = context.process(line -> {
            final String[] parts = line.split("\\s*[|]\\s*", 2);
            // Ignore the left hand side for now?
            final String[] outputs = parts[1].split("\\s+");
            return Arrays.stream(outputs)
                    .map(String::length)
                    .filter(length -> length == 2 || length == 4 || length == 3 || length == 7)
                    .count();
        });

        return counts.stream()
                .mapToLong(i -> i)
                .sum();
    }

    /**
     * Through a little deduction, you should now be able to determine the remaining digits.
     * Consider again the first example above:
     * <pre>
     * acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
     * cdfeb fcadb cdfeb cdbaf
     * </pre>
     * <p>
     * After some careful analysis, the mapping between signal wires and segments only make sense in the following configuration:
     * <pre>
     *  dddd
     * e    a
     * e    a
     *  ffff
     * g    b
     * g    b
     *  cccc
     *  </pre>
     * <p>
     * So, the unique signal patterns would correspond to the following digits:
     * <ul>
     * <li> <code>acedgfb</code>: <code>8</code> </li>
     * <li> <code>cdfbe</code>: <code>5</code> </li>
     * <li> <code>gcdfa</code>: <code>2</code> </li>
     * <li> <code>fbcad</code>: <code>3</code> </li>
     * <li> <code>dab</code>: <code>7</code> </li>
     * <li> <code>cefabd</code>: <code>9</code> </li>
     * <li> <code>cdfgeb</code>: <code>6</code> </li>
     * <li> <code>eafb</code>: <code>4</code> </li>
     * <li> <code>cagedb</code>: <code>0</code> </li>
     * <li> <code>ab</code>: <code>1</code> </li>
     * </ul>
     * <p>
     * Then, the four digits of the output value can be decoded:
     * <ul>
     * <li> <code>cdfeb</code>: <i><code>5</code></i> </li>
     * <li> <code>fcadb</code>: <i><code>3</code></i> </li>
     * <li> <code>cdfeb</code>: <i><code>5</code></i> </li>
     * <li> <code>cdbaf</code>: <i><code>3</code></i> </li>
     * </ul>
     * <p>
     * Therefore, the output value for this entry is <i><code>5353</code></i>.
     * <p>
     * Following this same process for each entry in the second, larger example above, the output value of each entry can be determined:
     * <ul>
     * <li> <code>fdgacbe cefdb cefbgd gcbe</code>: <code>8394</code> </li>
     * <li> <code>fcgedb cgb dgebacf gc</code>: <code>9781</code> </li>
     * <li> <code>cg cg fdcagb cbg</code>: <code>1197</code> </li>
     * <li> <code>efabcd cedba gadfec cb</code>: <code>9361</code> </li>
     * <li> <code>gecf egdcabf bgf bfgea</code>: <code>4873</code> </li>
     * <li> <code>gebdcfa ecba ca fadegcb</code>: <code>8418</code> </li>
     * <li> <code>cefg dcbef fcge gbcadfe</code>: <code>4548</code> </li>
     * <li> <code>ed bcgafe cdgba cbgef</code>: <code>1625</code> </li>
     * <li> <code>gbdfcae bgc cg cgb</code>: <code>8717</code> </li>
     * <li> <code>fgae cfgab fg bagce</code>: <code>4315</code> </li>
     * </ul>
     * <p>
     * Adding all of the output values in this larger example produces <i><code>61229</code></i>.
     * <p>
     * For each entry, determine all of the wire/segment connections and decode the four-digit output values.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you add up all of the output values?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<Long> results = context.process(line -> {
            final String[] parts = line.split("\\s*[|]\\s*", 2);

            final String[] inputs = parts[0].split("\\s+");
            final String[] outputs = parts[1].split("\\s+");

            return parse(inputs, outputs);
        });

        return results.stream()
                .mapToLong(i -> i)
                .sum();
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Parse the inputs and use this to calculate the output.
     */
    private long parse(final String[] inputs, final String[] outputs) {
        final int[] matches = new int[10];
        final int[] reverseMatches = new int[128]; // Sparse, but meh

        // Match 1, 4, 7 and 8, store 0, 2, 3, 5, 6, 9
        final Deque<Integer> remaining = new ArrayDeque<>();
        for (final String input : inputs) {
            if (input.length() == 2) assign(matches, reverseMatches, 1, toInt(input));
            else if (input.length() == 4) assign(matches, reverseMatches, 4, toInt(input));
            else if (input.length() == 3) assign(matches, reverseMatches, 7, toInt(input));
            else if (input.length() == 7) assign(matches, reverseMatches, 8, toInt(input));
            else remaining.addLast(toInt(input));
        }

        while (!remaining.isEmpty()) {
            final int input = remaining.pollFirst();
            if (filterOverlap(input, matches[4], 2, 0, 3, 2)) assign(matches, reverseMatches, 2, input);
            else if (filterOverlap(input, matches[7], 3, 2, 2, 0)) assign(matches, reverseMatches, 3, input);
            else if (filterOverlap(input, matches[4], 3, 1, 2, 1)) assign(matches, reverseMatches, 5, input);
            else if (filterOverlap(input, matches[7], 2, 0, 4, 1)) assign(matches, reverseMatches, 6, input);
            else if (filterOverlap(input, matches[4], 4, 1, 2, 0)) assign(matches, reverseMatches, 9, input);
            else matches[0] = input;
        }

        return Arrays.stream(outputs)
                .mapToInt(this::toInt)
                .mapToLong(val -> reverseMatches[val])
                .reduce(0, (result, next) -> result * 10 + next);
    }

    /*
     * Cache the match.
     */
    private void assign(final int[] matches, final int[] reverseMatches, final int index, final int value) {
        matches[index] = value;
        reverseMatches[value] = index;
    }

    /*
     * Convert the char codes to an int.
     */
    private int toInt(final String input) {
        int result = 0;
        for (final char chr : input.toCharArray()) {
            result |= (1 << (chr - 'a'));
        }
        return result;
    }

    /*
     * Determine the overlap between the given input and the reference value.
     */
    private boolean filterOverlap(final int input, final int reference, final int tp, final int tn, final int fp, final int fn) {
        int tp$ = 0, tn$ = 0, fp$ = 0, fn$ = 0;

        for (int i = 0; i < 7; i++) {
            final int inputBit = (input >> i) & 0x01;
            final int matchBit = (reference >> i) & 0x01;

            if (matchBit == 1 && inputBit == 1) ++tp$;
            else if (matchBit == 1) ++fn$;
            else if (inputBit == 0) ++tn$;
            else ++fp$;
        }

        return tp$ == tp && tn$ == tn && fp$ == fp && fn$ == fn;
    }

}
