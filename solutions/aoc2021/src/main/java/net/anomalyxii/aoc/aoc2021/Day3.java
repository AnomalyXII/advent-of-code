package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.ArrayList;
import java.util.List;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 3: Binary Diagnostic.
 */
@Solution(year = 2021, day = 3, title = "Binary Diagnostic")
public class Day3 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The submarine has been making some odd creaking noises, so you ask it to produce a diagnostic report just in case.
     * <p>
     * The diagnostic report (your puzzle input) consists of a list of binary numbers which, when decoded properly, can tell you many useful things about the conditions of the submarine.
     * The first parameter to check is the <i>power consumption</i>.
     * <p>
     * You need to use the binary numbers in the diagnostic report to generate two new binary numbers (called the <i>gamma rate</i> and the <i>epsilon rate</i>).
     * The power consumption can then be found by multiplying the gamma rate by the epsilon rate.
     * <p>
     * Each bit in the gamma rate can be determined by finding the <i>most common bit in the corresponding position</i> of all numbers in the diagnostic report.
     * For example, given the following diagnostic report:
     * <pre>
     * 00100
     * 11110
     * 10110
     * 10111
     * 10101
     * 01111
     * 00111
     * 11100
     * 10000
     * 11001
     * 00010
     * 01010
     * </pre>
     * <p>
     * Considering only the first bit of each number, there are five <code>0</code> bits and seven <code>1</code> bits.
     * Since the most common bit is <code>1</code>, the first bit of the gamma rate is <code>1</code>.
     * <p>
     * The most common second bit of the numbers in the diagnostic report is <code>0</code>, so the second bit of the gamma rate is <code>0</code>.
     * <p>
     * The most common value of the third, fourth, and fifth bits are <code>1</code>, <code>1</code>, and <code>0</code>, respectively, and so the final three bits of the gamma rate are <code>110</code>.
     * <p>
     * So, the gamma rate is the binary number <code>10110</code>, or <i><code>22</code></i> in decimal.
     * <p>
     * The epsilon rate is calculated in a similar way; rather than use the most common bit, the least common bit from each position is used.
     * So, the epsilon rate is <code>01001</code>, or <code>9</code> in decimal.
     * Multiplying the gamma rate (<code>22</code>) by the epsilon rate (<code>9</code>) produces the power consumption, <i><code>198</code></i>.
     * <p>
     * Use the binary numbers in your diagnostic report to calculate the gamma rate and epsilon rate, then multiply them together.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the power consumption of the submarine? (Be sure to represent your answer in decimal, not binary.)
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<String> inputs = context.read();

        final int inputLength = inputs.getFirst().length();

        long gammaRate = 0, epsilonRate = 0;
        for (int pos = 0; pos < inputLength; pos++) {
            int msbIs1Counter = 0;
            for (final String input : inputs) {
                final char bit = input.charAt(pos);
                if (bit == '0') --msbIs1Counter;
                else if (bit == '1') ++msbIs1Counter;
                else throw parseError(input, bit, pos);
            }
            gammaRate = (gammaRate << 1) | (msbIs1Counter >= 0 ? 1 : 0);
            epsilonRate = (epsilonRate << 1) | (msbIs1Counter >= 0 ? 0 : 1);
        }

        return gammaRate * epsilonRate;
    }

    /**
     * Next, you should verify the <i>life support rating</i>, which can be determined by multiplying the <i>oxygen generator rating</i> by the <i>CO2 scrubber rating</i>.
     * <p>
     * Both the oxygen generator rating and the CO2 scrubber rating are values that can be found in your diagnostic report - finding them is the tricky part.
     * Both values are located using a similar process that involves filtering out values until only one remains.
     * Before searching for either rating value, start with the full list of binary numbers from your diagnostic report and <i>consider just the first bit</i> of those numbers.
     * Then:
     * <ul>
     * <li>
     * Keep only numbers selected by the bit criteria for the type of rating value for which you are searching.
     * Discard numbers which do not match the bit criteria.
     * </li>
     * <li> If you only have one number left, stop; this is the rating value for which you are searching. </li>
     * <li> Otherwise, repeat the process, considering the next bit to the right. </li>
     * </ul>
     * <p>
     * The <i>bit criteria</i> depends on which type of rating value you want to find:
     * <ul>
     * <li>
     * To find <i>oxygen generator rating</i>, determine the most common value (<code>0</code> or <code>1</code>) in the current bit position, and keep only numbers with that bit in that position.
     * If <code>0</code> and <code>1</code> are equally common, keep values with a <code>1</code> in the position being considered.
     * </li>
     * <li>
     * To find <i>CO2 scrubber rating</i>, determine the least common value (<code>0</code> or <code>1</code>) in the current bit position, and keep only numbers with that bit in that position.
     * If <code>0</code> and <code>1</code> are equally common, keep values with a <code>0</code> in the position being considered.
     * </li>
     * </ul>
     * <p>
     * For example, to determine the oxygen generator rating value using the same example diagnostic report from above:
     * <ul>
     * <li>
     * Start with all 12 numbers and consider only the first bit of each number.
     * There are more <code>1</code> bits (7) than <code>0</code> bits (5), so keep only the 7 numbers with a <code>1</code> in the first position: <code>11110</code>, <code>10110</code>, <code>10111</code>, <code>10101</code>, <code>11100</code>, <code>10000</code>, and <code>11001</code>.
     * </li>
     * <li> Then, consider the second bit of the 7 remaining numbers: there are more <code>0</code> bits (4) than <code>1</code> bits (3), so keep only the 4 numbers with a <code>0</code> in the second position: <code>10110</code>, <code>10111</code>, <code>10101</code>, and <code>10000</code>. </li>
     * <li> In the third position, three of the four numbers have a <code>1</code>, so keep those three: <code>10110</code>, <code>10111</code>, and <code>10101</code>. </li>
     * <li> In the fourth position, two of the three numbers have a <code>1</code>, so keep those two: <code>10110</code> and <code>10111</code>. </li>
     * <li>
     * In the fifth position, there are an equal number of <code>0</code> bits and <code>1</code> bits (one each).
     * So, to find the <i>oxygen generator rating</i>, keep the number with a <code>1</code> in that position: <code>10111</code>.
     * </li>
     * <li> As there is only one number left, stop; the oxygen generator rating is <code>10111</code>, or <i><code>23</code></i> in decimal. </li>
     * </ul>
     * <p>
     * Then, to determine the <i>CO2 scrubber rating</i> value from the same example above:
     * <ul>
     * <li>
     * Start again with all 12 numbers and consider only the first bit of each number.
     * There are fewer 0 bits (5) than <code>1</code> bits (7), so keep only the 5 numbers with a <code>0</code> in the first position: <code>00100</code>, <code>01111</code>, <code>00111</code>, <code>00010</code>, and <code>01010</code>.
     * </li>
     * <li> Then, consider the second bit of the 5 remaining numbers: there are fewer <code>1</code> bits (2) than <code>0</code> bits (3), so keep only the 2 numbers with a <code>1</code> in the second position: <code>01111</code> and <code>01010</code>. </li>
     * <li>
     * In the third position, there are an equal number of <code>0</code> bits and <code>1</code> bits (one each).
     * So, to find the <i>CO2 scrubber rating</i>, keep the number with a <code>0</code> in that position: <code>01010</code>.
     * </li>
     * <li> As there is only one number left, stop; the <i>CO2 scrubber rating</i> is <code>01010</code>, or <code>10</code> in decimal. </li>
     * </ul>
     * <p>
     * Finally, to find the life support rating, multiply the oxygen generator rating (<code>23</code>) by the CO2 scrubber rating (<code>10</code>) to get <i><code>230</code></i>.
     * <p>
     * Use the binary numbers in your diagnostic report to calculate the oxygen generator rating and CO2 scrubber rating, then multiply them together.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the life support rating of the submarine? (Be sure to represent your answer in decimal, not binary.)
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<String> inputs = context.read();

        final String oxygenGeneratorRatingStr = filterByMostCommonMsb(inputs, false);
        final String co2ScrubberRatingStr = filterByMostCommonMsb(inputs, true);

        final long oxygenGeneratorRating = Integer.parseInt(oxygenGeneratorRatingStr, 2);
        final long co2ScrubberRating = Integer.parseInt(co2ScrubberRatingStr, 2);
        return oxygenGeneratorRating * co2ScrubberRating;
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Filter the provided inputs by checking the most-signification bit and
     * retaining only the entries with (or without, in the case of inverted
     * filtering) the corresponding bit. Continue this process moving to the
     * next bit (now considered the most-significant bit).
     */
    private String filterByMostCommonMsb(final List<String> inputs, final boolean invert) {
        List<String> filteredRatings = new ArrayList<>(inputs);
        int pos = 0;
        while (filteredRatings.size() > 1) {
            final List<String> msbIs0 = new ArrayList<>();
            final List<String> msbIs1 = new ArrayList<>();
            for (final String rating : filteredRatings) {
                final char bit = rating.charAt(pos);
                if (bit == '0') msbIs0.add(rating);
                else if (bit == '1') msbIs1.add(rating);
                else throw parseError(rating, bit, pos);
            }
            filteredRatings = (msbIs0.size() > msbIs1.size()) ^ invert ? msbIs0 : msbIs1;
            ++pos;
        }
        return filteredRatings.getFirst();
    }

    /*
     * Return a IllegalArgumentException explaining why the given input was invalid.
     */
    private IllegalArgumentException parseError(final String input, final char bit, final int pos) {
        return new IllegalArgumentException("Invalid bit [" + bit + "] at position " + pos + " in input '" + input + "'");
    }

}
