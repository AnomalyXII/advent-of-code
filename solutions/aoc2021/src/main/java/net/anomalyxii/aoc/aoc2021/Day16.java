package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 16: Packet Decoder.
 */
@Solution(year = 2021, day = 16, title = "Packet Decoder")
public class Day16 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * As you leave the cave and reach open waters, you receive a transmission from the Elves back on the ship.
     * <p>
     * The transmission was sent using the Buoyancy Interchange Transmission System (<span title="Just be glad it wasn't sent using the BuoyancY Transmission Encoding System.">BITS</span>), a method of packing numeric expressions into a binary sequence.
     * Your submarine's computer has saved the transmission in <a href="https://en.wikipedia.org/wiki/Hexadecimal" target="_blank">hexadecimal</a> (your puzzle input).
     * <p>
     * The first step of decoding the message is to convert the hexadecimal representation into binary.
     * Each character of hexadecimal corresponds to four bits of binary data:
     * <pre>
     * 0 = 0000
     * 1 = 0001
     * 2 = 0010
     * 3 = 0011
     * 4 = 0100
     * 5 = 0101
     * 6 = 0110
     * 7 = 0111
     * 8 = 1000
     * 9 = 1001
     * A = 1010
     * B = 1011
     * C = 1100
     * D = 1101
     * E = 1110
     * F = 1111
     * </pre>
     * <p>
     * The BITS transmission contains a single <i>packet</i> at its outermost layer which itself contains many other packets.
     * The hexadecimal representation of this packet might encode a few extra <code>0</code> bits at the end; these are not part of the transmission and should be ignored.
     * <p>
     * Every packet begins with a standard header: the first three bits encode the packet <i>version</i>, and the next three bits encode the packet <i>type ID</i>.
     * These two values are numbers; all numbers encoded in any packet are represented as binary with the most significant bit first.
     * For example, a version encoded as the binary sequence <code>100</code> represents the number <code>4</code>.
     * <p>
     * Packets with type ID <code>4</code> represent a <i>literal value</i>.
     * Literal value packets encode a single binary number.
     * To do this, the binary number is padded with leading zeroes until its length is a multiple of four bits, and then it is broken into groups of four bits.
     * Each group is prefixed by a <code>1</code> bit except the last group, which is prefixed by a <code>0</code> bit.
     * These groups of five bits immediately follow the packet header.
     * For example, the hexadecimal string <code>D2FE28</code> becomes:
     * <pre>
     * 110100101111111000101000
     * VVVTTTAAAAABBBBBCCCCC
     * </pre>
     * <p>
     * Below each bit is a label indicating its purpose:
     * <ul>
     * <li> The three bits labeled <code>V</code> (<code>110</code>) are the packet version, <code>6</code>. </li>
     * <li> The three bits labeled <code>T</code> (<code>100</code>) are the packet type ID, <code>4</code>, which means the packet is a literal value. </li>
     * <li> The five bits labeled <code>A</code> (<code>10111</code>) start with a <code>1</code> (not the last group, keep reading) and contain the first four bits of the number, <code>0111</code>. </li>
     * <li> The five bits labeled <code>B</code> (<code>11110</code>) start with a <code>1</code> (not the last group, keep reading) and contain four more bits of the number, <code>1110</code>. </li>
     * <li> The five bits labeled <code>C</code> (<code>00101</code>) start with a <code>0</code> (last group, end of packet) and contain the last four bits of the number, <code>0101</code>. </li>
     * <li> The three unlabeled <code>0</code> bits at the end are extra due to the hexadecimal representation and should be ignored. </li>
     * </ul>
     * <p>
     * So, this packet represents a literal value with binary representation <code>011111100101</code>, which is <code>2021</code> in decimal.
     * <p>
     * Every other type of packet (any packet with a type ID other than <code>4</code>) represent an <i>operator</i> that performs some calculation on one or more sub-packets contained within.
     * Right now, the specific operations aren't important; focus on parsing the hierarchy of sub-packets.
     * <p>
     * An operator packet contains one or more packets.
     * To indicate which subsequent binary data represents its sub-packets, an operator packet can use one of two modes indicated by the bit immediately after the packet header; this is called the <i>length type ID</i>:
     * <ul>
     * <li> If the length type ID is <code>0</code>, then the next <i>15</i> bits are a number that represents the <i>total length in bits</i> of the sub-packets contained by this packet. </li>
     * <li> If the length type ID is <code>1</code>, then the next <i>11</i> bits are a number that represents the <i>number of sub-packets immediately contained</i> by this packet. </li>
     * </ul>
     * <p>
     * Finally, after the length type ID bit and the 15-bit or 11-bit field, the sub-packets appear.
     * <p>
     * For example, here is an operator packet (hexadecimal string <code>38006F45291200</code>) with length type ID <code>0</code> that contains two sub-packets:
     * <pre>
     * 00111000000000000110111101000101001010010001001000000000
     * VVVTTTILLLLLLLLLLLLLLLAAAAAAAAAAABBBBBBBBBBBBBBBB
     * </pre>
     * <ul>
     * <li> The three bits labeled <code>V</code> (<code>001</code>) are the packet version, <code>1</code>. </li>
     * <li> The three bits labeled <code>T</code> (<code>110</code>) are the packet type ID, <code>6</code>, which means the packet is an operator. </li>
     * <li> The bit labeled <code>I</code> (<code>0</code>) is the length type ID, which indicates that the length is a 15-bit number representing the number of bits in the sub-packets. </li>
     * <li> The 15 bits labeled <code>L</code> (<code>000000000011011</code>) contain the length of the sub-packets in bits, <code>27</code>. </li>
     * <li> The 11 bits labeled <code>A</code> contain the first sub-packet, a literal value representing the number <code>10</code>. </li>
     * <li> The 16 bits labeled <code>B</code> contain the second sub-packet, a literal value representing the number <code>20</code>. </li>
     * </ul>
     * <p>
     * After reading 11 and 16 bits of sub-packet data, the total length indicated in <code>L</code> (27) is reached, and so parsing of this packet stops.
     * <p>
     * As another example, here is an operator packet (hexadecimal string <code>EE00D40C823060</code>) with length type ID <code>1</code> that contains three sub-packets:
     * <pre>
     * 11101110000000001101010000001100100000100011000001100000
     * VVVTTTILLLLLLLLLLLAAAAAAAAAAABBBBBBBBBBBCCCCCCCCCCC
     * </pre>
     * <ul>
     * <li> The three bits labeled <code>V</code> (<code>111</code>) are the packet version, <code>7</code>. </li>
     * <li> The three bits labeled <code>T</code> (<code>011</code>) are the packet type ID, <code>3</code>, which means the packet is an operator. </li>
     * <li> The bit labeled <code>I</code> (<code>1</code>) is the length type ID, which indicates that the length is a 11-bit number representing the number of sub-packets. </li>
     * <li> The 11 bits labeled <code>L</code> (<code>00000000011</code>) contain the number of sub-packets, <code>3</code>. </li>
     * <li> The 11 bits labeled <code>A</code> contain the first sub-packet, a literal value representing the number <code>1</code>. </li>
     * <li> The 11 bits labeled <code>B</code> contain the second sub-packet, a literal value representing the number <code>2</code>. </li>
     * <li> The 11 bits labeled <code>C</code> contain the third sub-packet, a literal value representing the number <code>3</code>. </li>
     * </ul>
     * <p>
     * After reading 3 complete sub-packets, the number of sub-packets indicated in <code>L</code> (3) is reached, and so parsing of this packet stops.
     * <p>
     * For now, parse the hierarchy of the packets throughout the transmission and <i>add up all of the version numbers</i>.
     * <p>
     * Here are a few more examples of hexadecimal-encoded transmissions:
     * <ul>
     * <li> <code>8A004A801A8002F478</code> represents an operator packet (version 4) which contains an operator packet (version 1) which contains an operator packet (version 5) which contains a literal value (version 6); this packet has a version sum of <code><i>16</i></code>. </li>
     * <li>
     * <code>620080001611562C8802118E34</code> represents an operator packet (version 3) which contains two sub-packets; each sub-packet is an operator packet that contains two literal values.
     * This packet has a version sum of <code><i>12</i></code>.
     * </li>
     * <li>
     * <code>C0015000016115A2E0802F182340</code> has the same structure as the previous example, but the outermost packet uses a different length type ID.
     * This packet has a version sum of <code><i>23</i></code>.
     * </li>
     * <li> <code>A0016C880162017C3686B18A3D4780</code> is an operator packet that contains an operator packet that contains an operator packet that contains five literal values; it has a version sum of <code><i>31</i></code>. </li>
     * </ul>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Decode the structure of your hexadecimal-encoded BITS transmission; what do you get if you add up the version numbers in all packets?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.processLine(line -> {
            final AtomicLong versionSum = new AtomicLong(0);
            Packet.parse(line, versionSum::addAndGet);
            return versionSum.longValue();
        });
    }

    /**
     * Now that you have the structure of your transmission decoded, you can calculate the value of the expression it represents.
     * <p>
     * Literal values (type ID <code>4</code>) represent a single number as described above.
     * The remaining type IDs are more interesting:
     * <ul>
     * <li>
     * Packets with type ID <code>0</code> are <i>sum</i> packets - their value is the sum of the values of their sub-packets.
     * If they only have a single sub-packet, their value is the value of the sub-packet.
     * </li>
     * <li>
     * Packets with type ID <code>1</code> are <i>product</i> packets - their value is the result of multiplying together the values of their sub-packets.
     * If they only have a single sub-packet, their value is the value of the sub-packet.
     * </li>
     * <li>Packets with type ID <code>2</code> are <i>minimum</i> packets - their value is the minimum of the values of their sub-packets.</li>
     * <li>Packets with type ID <code>3</code> are <i>maximum</i> packets - their value is the maximum of the values of their sub-packets.</li>
     * <li>
     * Packets with type ID <code>5</code> are <i>greater than</i> packets - their value is <i>1</i> if the value of the first sub-packet is greater than the value of the second sub-packet; otherwise, their value is <i>0</i>.
     * These packets always have exactly two sub-packets.
     * </li>
     * <li>
     * Packets with type ID <code>6</code> are <i>less than</i> packets - their value is <i>1</i> if the value of the first sub-packet is less than the value of the second sub-packet; otherwise, their value is <i>0</i>.
     * These packets always have exactly two sub-packets.
     * </li>
     * <li>
     * Packets with type ID <code>7</code> are <i>equal to</i> packets - their value is <i>1</i> if the value of the first sub-packet is equal to the value of the second sub-packet; otherwise, their value is <i>0</i>.
     * These packets always have exactly two sub-packets.
     * </li>
     * </ul>
     * <p>
     * Using these rules, you can now work out the value of the outermost packet in your BITS transmission.
     * <p>
     * For example:
     * <ul>
     * <li> <code>C200B40A82</code> finds the sum of <code>1</code> and <code>2</code>, resulting in the value <code><i>3</i></code>. </li>
     * <li> <code>04005AC33890</code> finds the product of <code>6</code> and <code>9</code>, resulting in the value <code><i>54</i></code>. </li>
     * <li> <code>880086C3E88112</code> finds the minimum of <code>7</code>, <code>8</code>, and <code>9</code>, resulting in the value <code><i>7</i></code>. </li>
     * <li> <code>CE00C43D881120</code> finds the maximum of <code>7</code>, <code>8</code>, and <code>9</code>, resulting in the value <code><i>9</i></code>. </li>
     * <li> <code>D8005AC2A8F0</code> produces <code>1</code>, because <code>5</code> is less than <code>15</code>. </li>
     * <li> <code>F600BC2D8F</code> produces <code>0</code>, because <code>5</code> is not greater than <code>15</code>. </li>
     * <li> <code>9C005AC2F8F0</code> produces <code>0</code>, because <code>5</code> is not equal to <code>15</code>. </li>
     * <li> <code>9C0141080250320F1802104A08</code> produces <code>1</code>, because <code>1</code> + <code>3</code> = <code>2</code> * <code>2</code>. </li>
     * </ul>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you evaluate the expression represented by your hexadecimal-encoded BITS transmission?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return context.processLine(line -> Packet.parse(line, version -> {
        }).value);
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * Read bits from the input String.
     */
    private static final class BitReader {

        // Private Members

        private final String input;
        private int position = 0;

        // Constructors

        BitReader(final String input) {
            this.input = input;
        }

        // Helper Methods

        /*
         * Get the current position in the input String.
         */
        int currentPos() {
            return position;
        }

        /*
         * Read a single bit as a `boolean`.
         */
        boolean readBitAsBoolean() {
            return "1".equals(readBits(1));
        }

        /*
         * Read a number of bits and return the value as an `int`.
         */
        int readBitsAsInt(final int size) {

            return Integer.parseInt(readBits(size), 2);
        }

        // Private Methods

        /*
         * Read a String of bits.
         */
        private String readBits(final int size) {
            return input.substring(position, (position += size));
        }

        // Static Methods

        /*
         * Create a `BitReader` for the given line.
         */
        static BitReader fromHexString(final String line) {
            return new BitReader(line.chars()
                                         .map(chr -> Integer.parseInt(Character.toString(chr), 16))
                                         .mapToObj(Integer::toBinaryString)
                                         .map(b -> String.format("%4s", b).replace(' ', '0'))
                                         .collect(Collectors.joining()));
        }

    }

    /*
     * A packet.
     */
    private record Packet(int version, PacketType type, long value) {

        // Helper Methods

        /*
         * Parse a packet.
         */
        private static Packet parse(final String line, final IntConsumer onNewPacket) {
            return parse(BitReader.fromHexString(line), onNewPacket);
        }

        /*
         * Parse a packet.
         */
        private static Packet parse(final BitReader reader, final IntConsumer onNewPacket) {
            final int version = reader.readBitsAsInt(3);
            final int typeCode = reader.readBitsAsInt(3);
            final PacketType packetType = PacketType.fromCode(typeCode);

            onNewPacket.accept(version);
            final long value = packetType.resolve(reader, onNewPacket);

            return new Packet(version, packetType, value);
        }

    }

    /*
     * The various packet types.
     */
    private enum PacketType {

        LITERAL(4) {
            @Override
            long resolve(final BitReader reader, final IntConsumer versionSum) {
                boolean last;
                long result = 0;
                do {
                    last = !reader.readBitAsBoolean();
                    result = (result << 4) | reader.readBitsAsInt(4);
                } while (!last);

                return result;
            }
        },

        SUM(0) {
            @Override
            long resolve(final BitReader reader, final IntConsumer versionSum) {
                return parseChildren(reader, versionSum)
                        .sum();
            }
        },
        PRODUCT(1) {
            @Override
            long resolve(final BitReader reader, final IntConsumer versionSum) {
                return parseChildren(reader, versionSum)
                        .reduce(1L, (result, next) -> result * next);
            }
        },
        MINIMUM(2) {
            @Override
            long resolve(final BitReader reader, final IntConsumer versionSum) {
                return parseChildren(reader, versionSum).min().orElseThrow();
            }
        },
        MAXIMUM(3) {
            @Override
            long resolve(final BitReader reader, final IntConsumer versionSum) {
                return parseChildren(reader, versionSum).max().orElseThrow();
            }
        },
        GREATER(5) {
            @Override
            long resolve(final BitReader reader, final IntConsumer versionSum) {
                final long[] literals = parseChildren(reader, versionSum).toArray();
                return literals[0] > literals[1] ? 1L : 0L;
            }
        },
        LESS_THAN(6) {
            @Override
            long resolve(final BitReader reader, final IntConsumer versionSum) {
                final long[] literals = parseChildren(reader, versionSum).toArray();
                return literals[0] < literals[1] ? 1L : 0L;
            }
        },
        EQUAL_TO(7) {
            @Override
            long resolve(final BitReader reader, final IntConsumer versionSum) {
                final long[] literals = parseChildren(reader, versionSum).toArray();
                return literals[0] == literals[1] ? 1L : 0L;
            }
        },

        // End of constants
        ;

        private final int code;

        // Constructors

        PacketType(final int code) {
            this.code = code;
        }

        // Helper Methods

        /*
         * Resolve the value of this `Packet`, based on the `PacketType`.
         */
        abstract long resolve(BitReader reader, IntConsumer onNewPacket);

        // Private Helper Methods

        /*
         * Resolve the `PacketType` from the given code.
         */
        static PacketType fromCode(final int code) {
            for (final PacketType type : values()) {
                if (type.code == code)
                    return type;
            }

            throw new IllegalArgumentException("Invalid packet type: " + code);
        }

        /*
         * Parse the payload of the `Packet`.
         */
        private static LongStream parseChildren(final BitReader reader, final IntConsumer onNewPacket) {
            final LongStream.Builder builder = LongStream.builder();
            final boolean relativeLength = reader.readBitAsBoolean();
            if (!relativeLength) {
                final int length = reader.readBitsAsInt(15);
                final int endPosition = reader.currentPos() + length;
                while (reader.currentPos() < endPosition)
                    builder.add(Packet.parse(reader, onNewPacket).value);
            } else {
                final int length = reader.readBitsAsInt(11);
                for (int i = 0; i < length; i++)
                    builder.add(Packet.parse(reader, onNewPacket).value);

            }
            return builder.build();
        }

    }

}
