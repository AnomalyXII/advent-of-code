package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.concurrent.atomic.AtomicLong;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 2: Dive.
 */
@Solution(year = 2021, day = 2, title = "Dive")
public class Day2 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Now, you need to figure out how to pilot this thing.
     * <p>
     * It seems like the submarine can take a series of commands like <code>forward 1</code>, <code>down 2</code>, or <code>up 3</code>:
     * <ul>
     * <li> <code>forward X</code> increases the horizontal position by <code>X</code> units. </li>
     * <li> <code>down X</code> increases the depth by <code>X</code> units. </li>
     * <li> <code>up X</code> decreases the depth by <code>X</code> units. </li>
     * </ul>
     * <p>
     * Note that since you're on a submarine, down and up affect your depth, and so they have the opposite result of what you might expect.
     * <p>
     * The submarine seems to already have a planned course (your puzzle input).
     * You should probably figure out where it's going.
     * For example:
     * <pre>
     * forward 5
     * down 5
     * forward 8
     * up 3
     * down 8
     * forward 2
     * </pre>
     * <p>
     * Your horizontal position and depth both start at 0.
     * The steps above would then modify them as follows:
     * <ul>
     * <li> <code>forward 5</code> adds <code>5</code> to your horizontal position, a total of <code>5</code>. </li>
     * <li> <code>down 5</code> adds <code>5</code> to your depth, resulting in a value of <code>5</code>. </li>
     * <li> <code>forward 8</code> adds <code>8</code> to your horizontal position, a total of <code>13</code>. </li>
     * <li> <code>up 3</code> decreases your depth by <code>3</code>, resulting in a value of <code>2</code>. </li>
     * <li> <code>down 8</code> adds <code>8</code> to your depth, resulting in a value of <code>10</code>. </li>
     * <li> <code>forward 2</code> adds <code>2</code> to your horizontal position, a total of <code>15</code>. </li>
     * </ul>
     * <p>
     * After following these instructions, you would have a horizontal position of <code>15</code> and a depth of <code>10</code>.
     * (Multiplying these together produces <i><code>150</code></i>.)
     * <p>
     * Calculate the horizontal position and depth you would have after following the planned course.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you multiply your final horizontal position by your final depth?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final AtomicLong position = new AtomicLong(0);
        final AtomicLong depth = new AtomicLong(0);

        context.consume(instruction -> {
            final String[] parts = instruction.split(" ", 2);

            final int delta = Integer.parseInt(parts[1]);
            if ("forward".equals(parts[0]))
                position.addAndGet(delta);
            else if ("down".equals(parts[0]))
                depth.addAndGet(delta);
            else if ("up".equals(parts[0]))
                depth.addAndGet(-delta);
        });

        return position.longValue() * depth.longValue();
    }

    /**
     * Based on your calculations, the planned course doesn't seem to make any sense.
     * You find the submarine manual and discover that the process is actually slightly more complicated.
     * <p>
     * In addition to horizontal position and depth, you'll also need to track a third value, <i>aim</i>, which also starts at <code>0</code>.
     * The commands also mean something entirely different than you first thought:
     * <ul>
     * <li> <code>down X</code> <i>increases</i> your aim by <code>X</code> units. </li>
     * <li> <code>up X</code> <i>decreases</i> your aim by <code>X</code> units. </li>
     * <li>
     * <code>forward X</code> does two things:
     * <ul>
     * <li> It increases your horizontal position by <code>X</code> units. </li>
     * <li> It increases your depth by your aim <i>multiplied</i> by <code>X</code>. </li>
     * </ul>
     * </li>
     * </ul>
     * Again note that since you're on a submarine, <code>down</code> and <code>up</code> do the opposite of what you might expect: "down" means aiming in the positive direction.
     * <p>
     * Now, the above example does something different:
     * <ul>
     * <li>
     * <code>forward 5</code> adds <code>5</code> to your horizontal position, a total of <code>5</code>.
     * Because your aim is <code>0</code>, your depth does not change.
     * </li>
     * <li> <code>down 5</code> adds <code>5</code> to your aim, resulting in a value of <code>5</code>. </li>
     * <li>
     * <code>forward 8</code> adds <code>8</code> to your horizontal position, a total of <code>13</code>.
     * Because your aim is <code>5</code>, your depth increases by <code>8*5=40</code>.
     * </li>
     * <li> <code>up 3</code> decreases your aim by <code>3</code>, resulting in a value of <code>2</code>. </li>
     * <li> <code>down 8</code> adds <code>8</code> to your aim, resulting in a value of <code>10</code>. </li>
     * <li>
     * <code>forward 2</code> adds <code>2</code> to your horizontal position, a total of <code>15</code>.
     * Because your aim is <code>10</code>, your depth increases by <code>2*10=20</code> to a total of <code>60</code>.
     * </li>
     * </ul>
     * <p>
     * After following these new instructions, you would have a horizontal position of <code>15</code> and a depth of <code>60</code>.
     * (Multiplying these produces <i><code>900</code></i>.)
     * <p>
     * Using this new interpretation of the commands, calculate the horizontal position and depth you would have after following the planned course.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you multiply your final horizontal position by your final depth?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final AtomicLong aim = new AtomicLong(0);
        final AtomicLong position = new AtomicLong(0);
        final AtomicLong depth = new AtomicLong(0);

        context.consume(instruction -> {
            final String[] parts = instruction.split(" ", 2);
            final int delta = Integer.parseInt(parts[1]);
            if ("forward".equals(parts[0])) {
                position.addAndGet(delta);
                depth.addAndGet(aim.longValue() * delta);
            } else if ("down".equals(parts[0])) {
                aim.addAndGet(delta);
            } else if ("up".equals(parts[0])) {
                aim.addAndGet(-delta);
            }
        });

        return position.longValue() * depth.longValue();
    }

    // ****************************************
    // Helper Methods
    // ****************************************


}
