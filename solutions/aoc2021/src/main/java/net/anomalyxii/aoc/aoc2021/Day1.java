package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.List;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 1: Sonar Sweep.
 */
@Solution(year = 2021, day = 1, title = "Sonar Sweep")
public class Day1 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You're minding your own business on a ship at sea when the overboard alarm goes off!
     * You rush to see if you can help.
     * Apparently, one of the Elves tripped and accidentally sent the sleigh keys flying into the ocean!
     * <p>
     * Before you know it, you're inside a submarine the Elves keep ready for situations like this.
     * It's covered in Christmas lights (because of course it is), and it even has an experimental antenna that should be able to track the keys if you can boost its signal strength high enough; there's a little meter that indicates the antenna's signal strength by displaying 0-50 <b>stars</b>.
     * <p>
     * Your instincts tell you that in order to save Christmas, you'll need to get all <b>fifty stars</b> by December 25th.
     * <p>
     * Collect stars by solving puzzles.
     * Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first.
     * Each puzzle grants <b>one star</b>.
     * Good luck!
     * <p>
     * As the submarine drops below the surface of the ocean, it automatically performs a sonar sweep of the nearby sea floor.
     * On a small screen, the sonar sweep report (your puzzle input) appears: each line is a measurement of the sea floor depth as the sweep looks further and further away from the submarine.
     * <p>
     * For example, suppose you had the following report:
     * <pre>
     * 199
     * 200
     * 208
     * 210
     * 200
     * 207
     * 240
     * 269
     * 260
     * 263
     * </pre>
     * <p>
     * This report indicates that, scanning outward from the submarine, the sonar sweep found depths of 199, 200, 208, 210, and so on.
     * <p>
     * The first order of business is to figure out how quickly the depth increases, just so you know what you're dealing with - you never know if the keys will get carried into deeper water by an ocean current or a fish or something.
     * <p>
     * To do this, count <i>the number of times a depth measurement increases</i> from the previous measurement.
     * (There is no measurement before the first measurement.)
     * In the example above, the changes are as follows:
     * <pre>
     * 199 (N/A - no previous measurement)
     * 200 (increased)
     * 208 (increased)
     * 210 (increased)
     * 200 (decreased)
     * 207 (increased)
     * 240 (increased)
     * 269 (increased)
     * 260 (decreased)
     * 263 (increased)
     * </pre>
     * <p>
     * In this example, there are <i><code>7</code></i> measurements that are larger than the previous measurement.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many measurements are larger than the previous measurement?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return countIncreases(context.process(Integer::parseInt), 1);
    }

    /**
     * Considering every single measurement isn't as useful as you expected: there's just too much noise in the data.
     * <p>
     * Instead, consider sums of a <i>three-measurement sliding window</i>.
     * Again considering the above example:
     * <pre>
     * 199  A
     * 200  A B
     * 208  A B C
     * 210    B C D
     * 200  E   C D
     * 207  E F   D
     * 240  E F G
     * 269    F G H
     * 260      G H
     * 263        H
     * </pre>
     * <p>
     * Start by comparing the first and second three-measurement windows.
     * The measurements in the first window are marked <code>A</code> (<code>199</code>, <code>200</code>, <code>208</code>); their sum is <code>199 + 200 + 208 = 607</code>.
     * The second window is marked <code>B</code> (<code>200</code>, <code>208</code>, <code>210</code>); its sum is <code>618</code>.
     * The sum of measurements in the second window is larger than the sum of the first, so this first comparison <i>increased</i>.
     * <p>
     * Your goal now is to count <i>the number of times the sum of measurements in this sliding window increases</i> from the previous sum.
     * So, compare <code>A</code> with <code>B</code>, then compare <code>B</code> with <code>C</code>, then <code>C</code> with <code>D</code>, and so on.
     * Stop when there aren't enough measurements left to create a new three-measurement sum.
     * <p>
     * In the above example, the sum of each three-measurement window is as follows:
     * <pre>
     * A: 607 (N/A - no previous sum)
     * B: 618 (increased)
     * C: 618 (no change)
     * D: 617 (decreased)
     * E: 647 (increased)
     * F: 716 (increased)
     * G: 769 (increased)
     * H: 792 (increased)
     * </pre>
     * <p>
     * In this example, there are <i><code>5</code></i> sums that are larger than the previous sum.
     * <p>
     * Consider sums of a three-measurement sliding window.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many sums are larger than the previous sum?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return countIncreases(context.process(Integer::parseInt), 3);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Count the number of times the sum of a window of inputs increases.
     */
    private long countIncreases(final List<Integer> depths, final int windowSize) {
        int count = 0;

        Integer prevSum = null;
        for (int i = windowSize - 1; i < depths.size(); i++) {
            int currentSum = depths.get(i);
            for (int j = 1; j < windowSize; j++) {
                currentSum += depths.get(i - j);
            }

            if (prevSum != null) {
                if (currentSum > prevSum) ++count;
            }
            prevSum = currentSum;
        }

        return count;
    }

}
