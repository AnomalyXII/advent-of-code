package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.Arrays;
import java.util.stream.LongStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 6: Lanturnfish.
 */
@Solution(year = 2021, day = 6, title = "Lanturnfish")
public class Day6 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The sea floor is getting steeper.
     * Maybe the sleigh keys got carried this way?
     * <p>
     * A massive school of glowing <a href="https://en.wikipedia.org/wiki/Lanternfish">lanternfish</a> swims past.
     * They must spawn quickly to reach such large numbers - maybe <i>exponentially</i> quickly?
     * You should model their growth rate to be sure.
     * <p>
     * Although you know nothing about this specific species of lanternfish, you make some guesses about their attributes.
     * Surely, each lanternfish creates a new lanternfish once every <i>7</i> days.
     * <p>
     * However, this process isn't necessarily synchronized between every lanternfish - one lanternfish might have 2 days left until it creates another lanternfish, while another might have 4.
     * So, you can model each fish as a single number that represents <i>the number of days until it creates a new lanternfish</i>.
     * <p>
     * Furthermore, you reason, a <i>new</i> lanternfish would surely need slightly longer before it's capable of producing more lanternfish: two more days for its first cycle.
     * <p>
     * So, suppose you have a lanternfish with an internal timer value of <code>3</code>:
     * <ul>
     * <li> After one day, its internal timer would become <code>2</code>. </li>
     * <li> After another day, its internal timer would become <code>1</code>. </li>
     * <li> After another day, its internal timer would become <code>0</code>. </li>
     * <li> After another day, its internal timer would reset to <code>6</code>, and it would create a new lanternfish with an internal timer of <code>8</code>. </li>
     * <li> After another day, the first lanternfish would have an internal timer of <code>5</code>, and the second lanternfish would have an internal timer of <code>7</code>. </li>
     * </ul>
     * <p>
     * A lanternfish that creates a new fish resets its timer to <code>6</code>, not <code>7</code> (because <code>0</code> is included as a valid timer value).
     * The new lanternfish starts with an internal timer of <code>8</code> and does not start counting down until the next day.
     * <p>
     * Realizing what you're trying to do, the submarine automatically produces a list of the ages of several hundred nearby lanternfish (your puzzle input).
     * For example, suppose you were given the following list:
     * <pre>
     * 3,4,3,1,2
     * </pre>
     * <p>
     * This list means that the first fish has an internal timer of <code>3</code>, the second fish has an internal timer of <code>4</code>, and so on until the fifth fish, which has an internal timer of <code>2</code>.
     * Simulating these fish over several days would proceed as follows:
     * <pre>
     * Initial state: 3,4,3,1,2
     * After  1 day:  2,3,2,0,1
     * After  2 days: 1,2,1,6,0,8
     * After  3 days: 0,1,0,5,6,7,8
     * After  4 days: 6,0,6,4,5,6,7,8,8
     * After  5 days: 5,6,5,3,4,5,6,7,7,8
     * After  6 days: 4,5,4,2,3,4,5,6,6,7
     * After  7 days: 3,4,3,1,2,3,4,5,5,6
     * After  8 days: 2,3,2,0,1,2,3,4,4,5
     * After  9 days: 1,2,1,6,0,1,2,3,3,4,8
     * After 10 days: 0,1,0,5,6,0,1,2,2,3,7,8
     * After 11 days: 6,0,6,4,5,6,0,1,1,2,6,7,8,8,8
     * After 12 days: 5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8
     * After 13 days: 4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8
     * After 14 days: 3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8
     * After 15 days: 2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7
     * After 16 days: 1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8
     * After 17 days: 0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8
     * After 18 days: 6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8
     * </pre>
     * <p>
     * Each day, a <code>0</code> becomes a <code>6</code> and adds a new <code>8</code> to the end of the list, while each other number decreases by 1 if it was present at the start of the day.
     * <p>
     * In this example, after 18 days, there are a total of <code>26</code> fish. After 80 days, there would be a total of <i><code>5934</code></i>.
     * <p>
     * Find a way to simulate lanternfish.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many lanternfish would there be after 80 days?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return simulate(context, 80);
    }

    /**
     * Suppose the lanternfish live forever and have unlimited food and space.
     * Would they take over the entire ocean?
     * <p>
     * After 256 days in the example above, there would be a total of <i><code>26984457539</code></i> lanternfish!
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many lanternfish would there be after 256 days?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return simulate(context, 256);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Simulate the lifecycle of a school of Lanturnfish.
     */
    private long simulate(final SolutionContext context, final int numberOfDays) {
        final long[] fish = context.stream()
                .flatMap(line -> Arrays.stream(line.split(",\\s*")))
                .map(Integer::parseInt)
                .reduce(
                        new long[9],
                        (result, x) -> {
                            result[x]++;
                            return result;
                        },
                        (a, b) -> a
                );

        for (int day = 0; day < numberOfDays; day++) {
            final long fishAt0Days = fish[0];

            //noinspection SuspiciousSystemArraycopy
            System.arraycopy(fish, 1, fish, 0, 8);

            fish[6] += fishAt0Days;
            fish[8] = fishAt0Days;
        }

        return LongStream.of(fish).sum();
    }

}
