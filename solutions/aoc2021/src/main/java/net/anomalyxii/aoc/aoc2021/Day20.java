package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Area;
import net.anomalyxii.aoc.utils.geometry.Coordinate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.ToIntFunction;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 20: Trench Map.
 */
@Solution(year = 2021, day = 20, title = "Trench Map")
public class Day20 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * With the scanners fully deployed, you turn their attention to mapping the floor of the ocean trench.
     * <p>
     * When you get back the image from the scanners, it seems to just be random noise.
     * Perhaps you can combine an image enhancement algorithm and the input image (your puzzle input) to clean it up a little.
     * <p>
     * For example:
     * <pre>
     * ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
     * #..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###
     * .######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.
     * .#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....
     * .#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..
     * ...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....
     * ..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#
     *
     * #..#.
     * #....
     * ##..#
     * ..#..
     * ..###
     * </pre>
     * <p>
     * The first section is the <em>image enhancement algorithm</em>.
     * It is normally given on a single line, but it has been wrapped to multiple lines in this example for legibility.
     * The second section is the <em>input image</em>, a two-dimensional grid of <em>light pixels</em> (<code>#</code>) and <em>dark pixels</em> (<code>.</code>).
     * <p>
     * The image enhancement algorithm describes how to enhance an image by <em>simultaneously</em> converting all pixels in the input image into an output image.
     * Each pixel of the output image is determined by looking at a 3x3 square of pixels centered on the corresponding input image pixel.
     * So, to determine the value of the pixel at (5,10) in the output image, nine pixels from the input image need to be considered: (4,9), (4,10), (4,11), (5,9), (5,10), (5,11), (6,9), (6,10), and (6,11).
     * These nine input pixels are combined into a single binary number that is used as an index in the <em>image enhancement algorithm</em> string.
     * <p>
     * For example, to determine the output pixel that corresponds to the very middle pixel of the input image, the nine pixels marked by <code>[...]</code> would need to be considered:
     * <pre>
     * # . . # .
     * #[. . .].
     * #[# . .]#
     * .[. # .].
     * . . # # #
     * </pre>
     * <p>
     * Starting from the top-left and reading across each row, these pixels are <code>...</code>, then <code>#..</code>, then <code>.#.</code>; combining these forms <code>...#...#.</code>.
     * By turning dark pixels (<code>.</code>) into <code>0</code> and light pixels (<code>#</code>) into <code>1</code>, the binary number <code>000100010</code> can be formed, which is <code>34</code> in decimal.
     * <p>
     * The image enhancement algorithm string is exactly 512 characters long, enough to match every possible 9-bit binary number.
     * The first few characters of the string (numbered starting from zero) are as follows:
     * <pre>
     * 0         10        20        30  <em>34</em>    40        50        60        70
     * |         |         |         |   <em>|</em>     |         |         |         |
     * ..#.#..#####.#.#.#.###.##.....###.<em>#</em>#.#..###.####..#####..#....#..#..##..##
     * </pre>
     * <p>
     * In the middle of this first group of characters, the character at index 34 can be found: <code>#</code>.
     * So, the output pixel in the center of the output image should be <code>#</code>, a <em>light pixel</em>.
     * <p>
     * This process can then be repeated to calculate every pixel of the output image.
     * <p>
     * Through advances in imaging technology, the images being operated on here are <em>infinite</em> in size.
     * <em>Every</em> pixel of the infinite output image needs to be calculated exactly based on the relevant pixels of the input image.
     * The small input image you have is only a small region of the actual infinite input image; the rest of the input image consists of dark pixels (<code>.</code>).
     * For the purposes of the example, to save on space, only a portion of the infinite-sized input and output images will be shown.
     * <p>
     * The starting input image, therefore, looks something like this, with more dark pixels (<code>.</code>) extending forever in every direction not shown here:
     * <pre>
     * ...............
     * ...............
     * ...............
     * ...............
     * ...............
     * .....#..#......
     * .....#.........
     * .....##..#.....
     * .......#.......
     * .......###.....
     * ...............
     * ...............
     * ...............
     * ...............
     * ...............
     * </pre>
     * <p>
     * By applying the image enhancement algorithm to every pixel simultaneously, the following output image can be obtained:
     * <pre>
     * ...............
     * ...............
     * ...............
     * ...............
     * .....##.##.....
     * ....#..#.#.....
     * ....##.#..#....
     * ....####..#....
     * .....#..##.....
     * ......##..#....
     * .......#.#.....
     * ...............
     * ...............
     * ...............
     * ...............
     * </pre>
     * <p>
     * Through further advances in imaging technology, the above output image can also be used as an input image!
     * This allows it to be enhanced <em>a second time</em>:
     * <pre>
     * ...............
     * ...............
     * ...............
     * ..........#....
     * ....#..#.#.....
     * ...#.#...###...
     * ...#...##.#....
     * ...#.....#.#...
     * ....#.#####....
     * .....#.#####...
     * ......##.##....
     * .......###.....
     * ...............
     * ...............
     * ...............
     * </pre>
     * <p>
     * Truly incredible - now the small details are really starting to come through.
     * After enhancing the original input image twice, <code><em>35</em></code> pixels are lit.
     * <p>
     * Start with the original input image and apply the image enhancement algorithm twice, being careful to account for the infinite size of the images.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many pixels are lit in the resulting image?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<String> lines = new ArrayList<>(context.read());

        final String algorithm = extractAlgorithm(lines);
        final Set<Coordinate> lightPixels = enhance(algorithm, lines, 2);
        return lightPixels.size();
    }

    /**
     * You still can't quite make out the details in the image.
     * Maybe you just didn't <a href="https://en.wikipedia.org/wiki/Kernel_(image_processing)" target="_blank">enhance</a> it <span title="Yeah, that's definitely the problem.">enough</span>.
     * <p>
     * If you enhance the starting input image in the above example a total of <em>50</em> times, <code><em>3351</em></code> pixels are lit in the final output image.
     * <p>
     * Start again with the original input image and apply the image enhancement algorithm 50 times.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many pixels are lit in the resulting image?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<String> lines = new ArrayList<>(context.read());

        final String algorithm = extractAlgorithm(lines);
        final Set<Coordinate> lightPixels = enhance(algorithm, lines, 50);
        return lightPixels.size();
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Extract the algorithm from the first line of the input.
     */
    private String extractAlgorithm(final List<String> lines) {
        final String algorithm = lines.removeFirst();
        assert algorithm.length() == 512;
        assert algorithm.matches("[.#]+");

        final String blankLine = lines.removeFirst();
        assert blankLine.isBlank(); // Skip blank line...
        return algorithm;
    }

    /*
     * Enhance the given "image" a number of times.
     */
    private Set<Coordinate> enhance(final String algorithm, final List<String> image, final int iterations) {
        final int height = image.size();
        final int width = image.getFirst().length();

        final boolean[] cache = new boolean[algorithm.length()];
        for (int i = 0; i < algorithm.length(); i++)
            cache[i] = algorithm.charAt(i) == '#';

        final Set<Coordinate> lightPixels = new HashSet<>();
        for (int y = 0; y < height; y++) {
            final String line = image.get(y);
            for (int x = 0; x < width; x++) {
                assert line.length() == width;
                assert line.matches("[.#]+");

                if (line.charAt(x) == '#')
                    lightPixels.add(new Coordinate(x, y));
            }
        }

        final boolean[] borderIsWhite = new boolean[]{false};
        final ToIntFunction<Coordinate> inArea = p -> lightPixels.contains(p) ? 1 : 0;
        final ToIntFunction<Coordinate> inBorder = p -> borderIsWhite[0] ? 1 : 0;

        final Area area = Area.ofOrigin(width - 1, height - 1);
        for (int i = 0; i < iterations; i++) {
            final Set<Coordinate> newLightPixels = new HashSet<>();

            final Area currentArea = area.grow(i);
            currentArea.grow(1).forEach(p -> {
                final int value = p.neighboursAndSelf()
                        .mapToInt(n -> currentArea.contains(n)
                                ? inArea.applyAsInt(n)
                                : inBorder.applyAsInt(n))
                        .reduce(0, (result, val) -> (result << 1) | val);

                if (cache[value]) newLightPixels.add(p);
            });

            if (!borderIsWhite[0]) borderIsWhite[0] = cache[0];
            else borderIsWhite[0] = cache[511];

            lightPixels.clear();
            lightPixels.addAll(newLightPixels);
        }

        assert !borderIsWhite[0]; // If border is white, we have an infinite number of pixels...
        return lightPixels;
    }

}
