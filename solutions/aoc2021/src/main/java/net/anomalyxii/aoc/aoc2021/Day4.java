package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 4: Giant Squid.
 */
@Solution(year = 2021, day = 4, title = "Giant Squid")
public class Day4 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You're already almost 1.5km (almost a mile) below the surface of the ocean, already so deep that you can't see any sunlight.
     * What you <i>can</i> see, however, is a giant squid that has attached itself to the outside of your submarine.
     * <p>
     * Maybe it wants to play <a href="https://en.wikipedia.org/wiki/Bingo_(American_version)">bingo</a>?
     * <p>
     * Bingo is played on a set of boards each consisting of a 5x5 grid of numbers.
     * Numbers are chosen at random, and the chosen number is <i>marked</i> on all boards on which it appears.
     * (Numbers may not appear on all boards.)
     * If all numbers in any row or any column of a board are marked, that board <i>wins</i>.
     * (Diagonals don't count.)
     * <p>
     * The submarine has a <i>bingo subsystem</i> to help passengers (currently, you and the giant squid) pass the time.
     * It automatically generates a random order in which to draw numbers and a random set of boards (your puzzle input).
     * For example:
     * <pre>
     * 7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1
     *
     * 22 13 17 11  0
     *  8  2 23  4 24
     * 21  9 14 16  7
     *  6 10  3 18  5
     *  1 12 20 15 19
     *
     *  3 15  0  2 22
     *  9 18 13 17  5
     * 19  8  7 25 23
     * 20 11 10 24  4
     * 14 21 16 12  6
     *
     * 14 21 17 24  4
     * 10 16 15  9 19
     * 18  8 23 26 20
     * 22 11 13  6  5
     *  2  0 12  3  7
     * </pre>
     * <p>
     * After the first five numbers are drawn (<code>7</code>, <code>4</code>, <code>9</code>, <code>5</code>, and <code>11</code>), there are no winners, but the boards are marked as follows (shown here adjacent to each other to save space):
     * <pre>
     * 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
     *  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
     * 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
     *  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
     *  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
     * </pre>
     * <p>
     * After the next six numbers are drawn (<code>17</code>, <code>23</code>, <code>2</code>, <code>0</code>, <code>14</code>, and <code>21</code>), there are still no winners:
     * <pre>
     * 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
     *  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
     * 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
     *  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
     *  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
     * </pre>
     * <p>
     * Finally, <code>24</code> is drawn:
     * <pre>
     * 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
     *  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
     * 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
     *  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
     *  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
     * </pre>
     * <p>
     * At this point, the third board <i>wins</i> because it has at least one complete row or column of marked numbers (in this case, the entire top row is marked: <code>14 21 17 24 4</code>).
     * <p>
     * The <i>score</i> of the winning board can now be calculated.
     * Start by finding the <i>sum of all unmarked numbers</i> on that board; in this case, the sum is <code>188</code>.
     * Then, multiply that sum by the <i>number that was just called</i> when the board won, <code>24</code>, to get the final score, <code>188 * 24 = <i>4512</i></code>.
     * <p>
     * To guarantee victory against the giant squid, figure out which board will win first.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What will your final score be if you choose that board?
     * @throws IllegalStateException if multiple boards win at the same time
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<List<String>> batches = context.readBatches();

        final List<Integer> inputs = batches.getFirst().stream()
                .flatMap(line -> Arrays.stream(line.split(",\\s*")))
                .map(Integer::valueOf)
                .toList();

        final List<Board> boards = batches.subList(1, batches.size()).stream()
                .map(Board::create)
                .toList();

        for (final Integer input : inputs) {
            final List<Board> completedBoards = boards.stream()
                    .peek(board -> board.consume(input))
                    .filter(Board::isComplete)
                    .toList();

            final int numCompletedBoards = completedBoards.size();
            if (numCompletedBoards > 1)
                throw new IllegalStateException("Multiple boards (" + numCompletedBoards + ") completed!");
            else if (numCompletedBoards == 1)
                return completedBoards.getFirst().calculateScore();
        }

        throw new IllegalStateException("The game completed with no board winning?");
    }

    /**
     * On the other hand, it might be wise to try a different strategy: let the giant squid win.
     * <p>
     * You aren't sure how many bingo boards a giant squid could play at once, so rather than waste time counting its arms, the safe thing to do is to <i>figure out which board will win last</i> and choose that one.
     * That way, no matter which boards it picks, it will win for sure.
     * <p>
     * In the above example, the second board is the last to win, which happens after 13 is eventually called and its middle column is completely marked.
     * If you were to keep playing until this point, the second board would have a sum of unmarked numbers equal to 148 for a final score of <code>148 * 13 = <i>1924</i></code>.
     * <p>
     * Figure out which board will win last.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Once it wins, what would its final score be?
     * @throws IllegalStateException if no boards win (and thus, no boards lose)
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<List<String>> batches = context.readBatches();

        final List<Integer> inputs = batches.getFirst().stream()
                .flatMap(line -> Arrays.stream(line.split(",\\s*")))
                .map(Integer::valueOf)
                .toList();

        final List<Board> boards = batches.subList(1, batches.size()).stream()
                .map(Board::create)
                .collect(Collectors.toCollection(ArrayList::new)); // Make it mutable!

        List<Board> mostRecentlyCompleted = null;
        for (final Integer input : inputs) {
            if (boards.isEmpty()) break;

            final List<Board> completedBoards = boards.stream()
                    .peek(board -> board.consume(input))
                    .filter(Board::isComplete)
                    .toList();

            boards.removeAll(completedBoards);
            mostRecentlyCompleted = completedBoards;
        }

        if (mostRecentlyCompleted == null)
            throw new IllegalStateException("The game completed with no board winning (thus, none losing)?");
        else if (mostRecentlyCompleted.size() > 1)
            throw new IllegalStateException("More than 1 board lost :(");

        return mostRecentlyCompleted.getFirst().calculateScore();
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * A bingo Board.
     */
    private static final class Board {

        // Private Members

        private final int size;
        private final int[][] board;

        private int lastNumberConsumed = 0;

        // Constructors

        Board(final int size, final int[][] board) {
            this.size = size;
            this.board = board;
        }

        // Board Methods

        /*
         * Consume a number, marking it off the board if possible.
         */
        void consume(final int input) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    if (board[i][j] == input) {
                        board[i][j] = Integer.MIN_VALUE;
                        lastNumberConsumed = input;
                    }
                }
            }
        }

        /*
         * Check if this `Board` is now complete.
         */
        boolean isComplete() {
            for (int i = 0; i < size; i++) {
                boolean complete = board[i][0] == Integer.MIN_VALUE;
                for (int j = 1; j < size && complete; j++) {
                    complete = board[i][j] == Integer.MIN_VALUE;
                }
                if (complete) return true;
            }

            for (int j = 0; j < size; j++) {
                boolean complete = board[0][j] == Integer.MIN_VALUE;
                for (int i = 1; i < size && complete; i++) {
                    complete = board[i][j] == Integer.MIN_VALUE;
                }
                if (complete) return true;
            }

            return false;
        }

        /*
         * Calculate the score of the board.
         */
        long calculateScore() {
            long sum = 0;
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    if (board[i][j] != Integer.MIN_VALUE) {
                        sum += board[i][j];
                    }
                }
            }

            return sum * lastNumberConsumed;
        }

        // Static Helper Methods

        /*
         * Create a new `Board` from the given input.
         */
        static Board create(final List<String> inputs) {
            final int size = inputs.size();
            final int[][] board = new int[size][];
            for (int i = 0; i < inputs.size(); i++) {
                final String line = inputs.get(i).trim();
                final String[] split = line.split("\\s+", size);
                board[i] = Arrays.stream(split).mapToInt(Integer::parseInt).toArray();
            }

            return new Board(size, board);
        }
    }

}
