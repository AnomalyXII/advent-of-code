package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 14: Extended Polymerization.
 */
@Solution(year = 2021, day = 14, title = "Extended Polymerization")
public class Day14 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The incredible pressures at this depth are starting to put a strain on your submarine.
     * The submarine has <a href="https://en.wikipedia.org/wiki/Polymerization" target="_blank">polymerization</a> equipment that would produce suitable materials to reinforce the submarine, and the nearby volcanically-active caves should even have the necessary input elements in sufficient quantities.
     * <p>
     * The submarine manual contains <span title="HO&#xa;&#xa;HO -&gt; OH">instructions</span> for finding the optimal polymer formula; specifically, it offers a <i>polymer template</i> and a list of <i>pair insertion</i> rules (your puzzle input).
     * You just need to work out what polymer would result after repeating the pair insertion process a few times.
     * <p>
     * For example:
     * <pre>
     * NNCB
     *
     * CH -&gt; B
     * HH -&gt; N
     * CB -&gt; H
     * NH -&gt; C
     * HB -&gt; C
     * HC -&gt; B
     * HN -&gt; C
     * NN -&gt; C
     * BH -&gt; H
     * NC -&gt; B
     * NB -&gt; B
     * BN -&gt; B
     * BB -&gt; N
     * BC -&gt; B
     * CC -&gt; N
     * CN -&gt; C
     * </pre>
     * <p>
     * The first line is the <i>polymer template</i> - this is the starting point of the process.
     * <p>
     * The following section defines the <i>pair insertion</i> rules.
     * A rule like <code>AB -&gt; C</code> means that when elements <code>A</code> and <code>B</code> are immediately adjacent, element <code>C</code> should be inserted between them.
     * These insertions all happen simultaneously.
     * <p>
     * So, starting with the polymer template <code>NNCB</code>, the first step simultaneously considers all three pairs:
     * <ul>
     * <li> The first pair (<code>NN</code>) matches the rule <code>NN -&gt; C</code>, so element <code><i>C</i></code> is inserted between the first <code>N</code> and the second <code>N</code>. </li>
     * <li> The second pair (<code>NC</code>) matches the rule <code>NC -&gt; B</code>, so element <code><i>B</i></code> is inserted between the <code>N</code> and the <code>C</code>. </li>
     * <li> The third pair (<code>CB</code>) matches the rule <code>CB -&gt; H</code>, so element <code><i>H</i></code> is inserted between the <code>C</code> and the <code>B</code>. </li>
     * </ul>
     * <p>
     * Note that these pairs overlap: the second element of one pair is the first element of the next pair.
     * Also, because all pairs are considered simultaneously, inserted elements are not considered to be part of a pair until the next step.
     * <p>
     * After the first step of this process, the polymer becomes <code>N<i>C</i>N<i>B</i>C<i>H</i>B</code>.
     * <p>
     * Here are the results of a few steps using the above rules:
     * <pre>
     * Template:     NNCB
     * After step 1: NCNBCHB
     * After step 2: NBCCNBBBCBHCB
     * After step 3: NBBBCNCCNBBNBNBBCHBHHBCHB
     * After step 4: NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB
     * </pre>
     * <p>
     * This polymer grows quickly.
     * After step 5, it has length 97; After step 10, it has length 3073. After step 10, <code>B</code> occurs 1749 times, <code>C</code> occurs 298 times, <code>H</code> occurs 161 times, and <code>N</code> occurs 865 times; taking the quantity of the most common element (<code>B</code>, 1749) and subtracting the quantity of the least common element (<code>H</code>, 161) produces <code>1749 - 161 = <i>1588</i></code>.
     * <p>
     * Apply 10 steps of pair insertion to the polymer template and find the most and least common elements in the result.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return enhance(context, 10);
    }

    /**
     * The resulting polymer isn't nearly strong enough to reinforce the submarine.
     * You'll need to run more steps of the pair insertion process; a total of <i>40 steps</i> should do it.
     * <p>
     * In the above example, the most common element is <code>B</code> (occurring <code>2192039569602</code> times) and the least common element is <code>H</code> (occurring <code>3849876073</code> times); subtracting these produces <code><i>2188189693529</i></code>.
     * <p>
     * Apply <i>40</i> steps of pair insertion to the polymer template and find the most and least common elements in the result.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return enhance(context, 40);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Enhance the polymer a given number of times.
     */
    private long enhance(final SolutionContext context, final int iterations) {
        final List<List<String>> input = context.readBatches();
        final String template = input.getFirst().getFirst();
        final Map<ElementPair, PairInsertion> rules = input.getLast().stream()
                .map(PairInsertion::parse)
                .collect(Collectors.toMap(PairInsertion::pair, Function.identity()));

        final Map<ElementPair, AtomicLong> pairs = new HashMap<>();
        for (int n = 0; n < template.length() - 1; n++) {
            pairs.computeIfAbsent(new ElementPair(template.charAt(n), template.charAt(n + 1)), k -> new AtomicLong(0))
                    .incrementAndGet();
        }

        for (int iteration = 0; iteration < iterations; iteration++) {
            final Map<ElementPair, AtomicLong> nextPairs = new HashMap<>();
            for (final Map.Entry<ElementPair, AtomicLong> entry : pairs.entrySet()) {
                final ElementPair pair = entry.getKey();
                final PairInsertion expansion = rules.get(pair);
                nextPairs.computeIfAbsent(new ElementPair(pair.left, expansion.insert), k -> new AtomicLong(0))
                        .addAndGet(entry.getValue().longValue());
                nextPairs.computeIfAbsent(new ElementPair(expansion.insert, pair.right), k -> new AtomicLong(0))
                        .addAndGet(entry.getValue().longValue());
            }

            pairs.clear();
            pairs.putAll(nextPairs);
        }


        final long[] counts = new long[26];
        ++counts[template.charAt(template.length() - 1) - 'A'];
        for (final Map.Entry<ElementPair, AtomicLong> entry : pairs.entrySet()) {
            final ElementPair pair = entry.getKey();
            counts[pair.left - 'A'] += entry.getValue().longValue();
        }

        long mostFrequent = Long.MIN_VALUE;
        long leastFrequent = Long.MAX_VALUE;
        for (int i = 0; i < 26; i++) {
            final long count = counts[i];
            if (count == 0) continue;

            if (count > mostFrequent)
                mostFrequent = count;
            if (count < leastFrequent)
                leastFrequent = count;
        }

        return mostFrequent - leastFrequent;
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * A pair of elements.
     */
    private record ElementPair(char left, char right) {

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final ElementPair that = (ElementPair) o;
            return left == that.left && right == that.right;
        }

        @Override
        public int hashCode() {
            return Objects.hash(left, right);
        }

    }

    /*
     * A rule that defines the new element that is inserted within an
     * `ElementPair`.
     */
    private record PairInsertion(ElementPair pair, char insert) {

        // Helper Methods

        static PairInsertion parse(final String rule) {
            final String[] parts = rule.split("\\s*->\\s*");
            return new PairInsertion(new ElementPair(parts[0].charAt(0), parts[0].charAt(1)), parts[1].charAt(0));
        }

    }

}
