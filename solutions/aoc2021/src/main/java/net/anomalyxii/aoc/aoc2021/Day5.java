package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Coordinate;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 5: Hydrothermal Venture.
 */
@Solution(year = 2021, day = 5, title = "Hydrothermal Venture")
public class Day5 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You come across a field of <a href="https://en.wikipedia.org/wiki/Hydrothermal_vent">hydrothermal vents</a> on the ocean floor!
     * These vents constantly produce large, opaque clouds, so it would be best to avoid them if possible.
     * <p>
     * They tend to form in <i>lines</i>; the submarine helpfully produces a list of nearby lines of vents (your puzzle input) for you to review. For example:
     * <pre>
     * 0,9 -> 5,9
     * 8,0 -> 0,8
     * 9,4 -> 3,4
     * 2,2 -> 2,1
     * 7,0 -> 7,4
     * 6,4 -> 2,0
     * 0,9 -> 2,9
     * 3,4 -> 1,4
     * 0,0 -> 8,8
     * 5,5 -> 8,2
     * </pre>
     * <p>
     * Each line of vents is given as a line segment in the format <code>x1,y1 -> x2,y2</code> where <code>x1</code>,<code>y1</code> are the coordinates of one end the line segment and x2,y2 are the coordinates of the other end.
     * These line segments include the points at both ends.
     * In other words:
     * <ul>
     * <li> An entry like <code>1,1 -> 1,3</code> covers points <code>1,1</code>, <code>1,2</code>, and <code>1,3</code>. </li>
     * <li> An entry like <code>9,7 -> 7,7</code> covers points <code>9,7</code>, <code>8,7</code>, and <code>7,7</code>. </li>
     * </ul>
     * <p>
     * For now, <i>only consider horizontal and vertical lines</i>: lines where either <code>x1 = x2</code> or <code>y1 = y2</code>.
     * <p>
     * So, the horizontal and vertical lines from the above list would produce the following diagram:
     * <pre>
     * .......1..
     * ..1....1..
     * ..1....1..
     * .......1..
     * .112111211
     * ..........
     * ..........
     * ..........
     * ..........
     * 222111....
     * </pre>
     * <p>
     * In this diagram, the top left corner is <code>0,0</code> and the bottom right corner is <code>9,9</code>.
     * Each position is shown as the <i>number of lines which cover that point</i> or <code>.</code> if no line covers that point.
     * The top-left pair of <code>1</code>s, for example, comes from <code>2,2 -> 2,1</code>; the very bottom row is formed by the overlapping lines <code>0,9 -> 5,9</code> and <code>0,9 -> 2,9</code>.
     * <p>
     * To avoid the most dangerous areas, you need to determine <i>the number of points where at least two lines overlap</i>.
     * In the above example, this is anywhere in the diagram with a <code>2</code> or larger - a total of <i><code>5</code></i> points.
     * <p>
     * Consider only horizontal and vertical lines.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return At how many points do at least two lines overlap?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return countOverlaps(context, r -> !r.isDiagonal());
    }

    /**
     * Unfortunately, considering only horizontal and vertical lines doesn't give you the full picture; you need to also consider <i>diagonal lines</i>.
     * <p>
     * Because of the limits of the hydrothermal vent mapping system, the lines in your list will only ever be horizontal, vertical, or a diagonal line at exactly 45 degrees.
     * In other words:
     * <ul>
     * <li> An entry like <code>1,1 -> 3,3</code> covers points <code>1,1</code>, <code>2,2</code>, and <code>3,3</code>. </li>
     * <li> An entry like <code>9,7 -> 7,9</code> covers points <code>9,7</code>, <code>8,8</code>, and <code>7,9</code>. </li>
     * </ul>
     * <p>
     * Considering all lines from the above example would now produce the following diagram:
     * <pre>
     * 1.1....11.
     * .111...2..
     * ..2.1.111.
     * ...1.2.2..
     * .112313211
     * ...1.2....
     * ..1...1...
     * .1.....1..
     * 1.......1.
     * 222111....
     * </pre>
     * <p>
     * You still need to determine <i>the number of points where at least two lines overlap</i>.
     * In the above example, this is still anywhere in the diagram with a <code>2</code> or larger - now a total of <i><code>12</code></i> points.
     * <p>
     * Consider all of the lines.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return At how many points do at least two lines overlap?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return countOverlaps(context, r -> true);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Count the number of times two or more `Range`s intersect.
     */
    private long countOverlaps(final SolutionContext context, final Predicate<Range> filter) {
        return countOverlaps(context.stream(), filter);
    }

    /*
     * Count the number of times two or more `Range`s intersect.
     */
    private long countOverlaps(final Stream<String> stream, final Predicate<Range> filter) {
        return stream.map(Range::parse)
                .filter(filter)
                .flatMap(Range::interpolate)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .values()
                .stream()
                .filter(value -> value.intValue() > 1)
                .count();
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * A range of `Point`s, represented by a `start` and an `end`.
     */
    private record Range(Coordinate start, Coordinate end) {

        // Helper Methods

        /**
         * Determine whether this {@link Range} runs diagonally.
         *
         * @return {@literal true} if diagonal; {@literal false} otherwise
         */
        boolean isDiagonal() {
            return start.x() != end.x() && start.y() != end.y();
        }

        /**
         * Interpolate this {@link Range} into a {@link Stream} of all
         * {@link Coordinate Coordinates} that lie between the {@literal start}
         * and {@literal end}.
         *
         * @return a {@link Stream} of {@link Coordinate Points}
         */
        Stream<Coordinate> interpolate() {
            final int dx = Integer.compare(end.x(), start.x());
            final int dy = Integer.compare(end.y(), start.y());

            final Stream.Builder<Coordinate> points = Stream.builder();
            points.add(start);
            for (int cx = start.x() + dx, cy = start.y() + dy; cx != end.x() || cy != end.y(); cx += dx, cy += dy) {
                points.add(new Coordinate(cx, cy));
            }
            points.add(end);

            return points.build();
        }

        /**
         * Create a new {@link Range} by parsing the given (x1,y1) -> (x2,y2) coordinates.
         *
         * @param range the range
         * @return the {@link Range}
         */
        static Range parse(final String range) {
            final String[] parts = range.split("\\s*->\\s*", 2);
            final Coordinate start = Coordinate.parse(parts[0]);
            final Coordinate end = Coordinate.parse(parts[1]);

            return new Range(start, end);
        }

    }

}
