package net.anomalyxii.aoc.aoc2021;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Optional;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 18: Snailfish.
 */
@Solution(year = 2021, day = 18, title = "Snailfish")
public class Day18 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You descend into the ocean trench and encounter some <a href="https://en.wikipedia.org/wiki/Snailfish" target="_blank">snailfish</a>.
     * They say they saw the sleigh keys!
     * They'll even tell you which direction the keys went if you help one of the smaller snailfish with his <i><span title="Or 'maths', if you have more than one.">math</span> homework</i>.
     * <p>
     * Snailfish numbers aren't like regular numbers.
     * Instead, every snailfish number is a <i>pair</i> - an ordered list of two elements.
     * Each element of the pair can be either a regular number or another pair.
     * <p>
     * Pairs are written as <code>[x,y]</code>, where <code>x</code> and <code>y</code> are the elements within the pair.
     * Here are some example snailfish numbers, one snailfish number per line:
     * <pre>
     * [1,2]
     * [[1,2],3]
     * [9,[8,7]]
     * [[1,9],[8,5]]
     * [[[[1,2],[3,4]],[[5,6],[7,8]]],9]
     * [[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]
     * [[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]
     * </pre>
     * <p>
     * This snailfish homework is about <i>addition</i>.
     * To add two snailfish numbers, form a pair from the left and right parameters of the addition operator.
     * For example, <code>[1,2]</code> + <code>[[3,4],5]</code> becomes <code>[[1,2],[[3,4],5]]</code>.
     * <p>
     * There's only one problem: <i>snailfish numbers must always be reduced</i>, and the process of adding two snailfish numbers can result in snailfish numbers that need to be reduced.
     * <p>
     * To <i>reduce a snailfish number</i>, you must repeatedly do the first action in this list that applies to the snailfish number:
     * <ul>
     * <li> If any pair is <i>nested inside four pairs</i>, the leftmost such pair <i>explodes</i>. </li>
     * <li> If any regular number is <i>10 or greater</i>, the leftmost such regular number <i>splits</i>. </li>
     * </ul>
     * <p>
     * Once no action in the above list applies, the snailfish number is reduced.
     * <p>
     * During reduction, at most one action applies, after which the process returns to the top of the list of actions.
     * For example, if <i>split</i> produces a pair that meets the <i>explode</i> criteria, that pair <i>explodes</i> before other <i>splits</i> occur.
     * <p>
     * To <i>explode</i> a pair, the pair's left value is added to the first regular number to the left of the exploding pair (if any), and the pair's right value is added to the first regular number to the right of the exploding pair (if any).
     * Exploding pairs will always consist of two regular numbers.
     * Then, the entire exploding pair is replaced with the regular number <code>0</code>.
     * <p>
     * Here are some examples of a single explode action:
     * <ul>
     * <li> <code>[[[[<i>[9,8]</i>,1],2],3],4]</code> becomes <code>[[[[<i>0</i>,<i>9</i>],2],3],4]</code> (the <code>9</code> has no regular number to its left, so it is not added to any regular number). </li>
     * <li> <code>[7,[6,[5,[4,<i>[3,2]</i>]]]]</code> becomes <code>[7,[6,[5,[<i>7</i>,<i>0</i>]]]]</code> (the <code>2</code> has no regular number to its right, and so it is not added to any regular number). </li>
     * <li> <code>[[6,[5,[4,<i>[3,2]</i>]]],1]</code> becomes <code>[[6,[5,[<i>7</i>,<i>0</i>]]],<i>3</i>]</code>. </li>
     * <li> <code>[[3,[2,[1,<i>[7,3]</i>]]],[6,[5,[4,[3,2]]]]]</code> becomes <code>[[3,[2,[<i>8</i>,<i>0</i>]]],[<i>9</i>,[5,[4,[3,2]]]]]</code> (the pair <code>[3,2]</code> is unaffected because the pair <code>[7,3]</code> is further to the left; <code>[3,2]</code> would explode on the next action). </li>
     * <li> <code>[[3,[2,[8,0]]],[9,[5,[4,<i>[3,2]</i>]]]]</code> becomes <code>[[3,[2,[8,0]]],[9,[5,[<i>7</i>,<i>0</i>]]]]</code>. </li>
     * </ul>
     * <p>
     * To <i>split</i> a regular number, replace it with a pair; the left element of the pair should be the regular number divided by two and rounded <i>down</i>, while the right element of the pair should be the regular number divided by two and rounded <i>up</i>.
     * For example, <code>10</code> becomes <code>[5,5]</code>, <code>11</code> becomes <code>[5,6]</code>, <code>12</code> becomes <code>[6,6]</code>, and so on.
     * <p>
     * Here is the process of finding the reduced result of <code>[[[[4,3],4],4],[7,[[8,4],9]]]</code> + <code>[1,1]</code>:
     * <pre>
     * after addition: [[[[<i>[4,3]</i>,4],4],[7,[[8,4],9]]],[1,1]]
     * after explode:  [[[[0,7],4],[7,[<i>[8,4]</i>,9]]],[1,1]]
     * after explode:  [[[[0,7],4],[<i>15</i>,[0,13]]],[1,1]]
     * after split:    [[[[0,7],4],[[7,8],[0,<i>13</i>]]],[1,1]]
     * after split:    [[[[0,7],4],[[7,8],[0,<i>[6,7]</i>]]],[1,1]]
     * after explode:  [[[[0,7],4],[[7,8],[6,0]]],[8,1]]
     * </pre>
     * <p>
     * Once no reduce actions apply, the snailfish number that remains is the actual result of the addition operation: <code>[[[[0,7],4],[[7,8],[6,0]]],[8,1]]</code>.
     * <p>
     * The homework assignment involves adding up a <i>list of snailfish numbers</i> (your puzzle input).
     * The snailfish numbers are each listed on a separate line.
     * Add the first snailfish number and the second, then add that result and the third, then add that result and the fourth, and so on until all numbers in the list have been used once.
     * <p>
     * For example, the final sum of this list is <code>[[[[1,1],[2,2]],[3,3]],[4,4]]</code>:
     * <pre>
     * [1,1]
     * [2,2]
     * [3,3]
     * [4,4]
     * </pre>
     * <p>
     * The final sum of this list is <code>[[[[3,0],[5,3]],[4,4]],[5,5]]</code>:
     * <pre>
     * [1,1]
     * [2,2]
     * [3,3]
     * [4,4]
     * [5,5]
     * </pre>
     * <p>
     * The final sum of this list is <code>[[[[5,0],[7,4]],[5,5]],[6,6]]</code>:
     * <pre>
     * [1,1]
     * [2,2]
     * [3,3]
     * [4,4]
     * [5,5]
     * [6,6]
     * </pre>
     * <p>
     * Here's a slightly larger example:
     * <pre>
     * [[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
     * [7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
     * [[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
     * [[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
     * [7,[5,[[3,8],[1,4]]]]
     * [[2,[2,2]],[8,[8,1]]]
     * [2,9]
     * [1,[[[9,3],9],[[9,0],[0,7]]]]
     * [[[5,[7,4]],7],1]
     * [[[[4,2],2],6],[8,7]]
     * </pre>
     * <p>
     * The final sum <code>[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]</code> is found after adding up the above snailfish numbers:
     * <pre>
     * [[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
     * + [7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
     * = [[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]
     *
     *   [[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]
     * + [[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
     * = [[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]
     *
     *   [[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]
     * + [[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
     * = [[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]
     *
     *   [[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]
     * + [7,[5,[[3,8],[1,4]]]]
     * = [[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]
     *
     *   [[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]
     * + [[2,[2,2]],[8,[8,1]]]
     * = [[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]
     *
     *   [[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]
     * + [2,9]
     * = [[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]
     *
     *   [[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]
     * + [1,[[[9,3],9],[[9,0],[0,7]]]]
     * = [[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]
     *
     *   [[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]
     * + [[[5,[7,4]],7],1]
     * = [[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]
     *
     *   [[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]
     * + [[[[4,2],2],6],[8,7]]
     * = [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]
     * </pre>
     * <p>
     * To check whether it's the right answer, the snailfish teacher only checks the <i>magnitude</i> of the final sum.
     * The magnitude of a pair is 3 times the magnitude of its left element plus 2 times the magnitude of its right element.
     * The magnitude of a regular number is just that number.
     * <p>
     * For example, the magnitude of <code>[9,1]</code> is <code>3*9 + 2*1 = <i>29</i></code>; the magnitude of <code>[1,9]</code> is <code>3*1 + 2*9 = <i>21</i></code>.
     * Magnitude calculations are recursive: the magnitude of <code>[[9,1],[1,9]]</code> is <code>3*29 + 2*21 = <i>129</i></code>.
     * <p>
     * Here are a few more magnitude examples:
     * <ul>
     * <li> <code>[[1,2],[[3,4],5]]</code> becomes <code><i>143</i></code>. </li>
     * <li> <code>[[[[0,7],4],[[7,8],[6,0]]],[8,1]]</code> becomes <code><i>1384</i></code>. </li>
     * <li> <code>[[[[1,1],[2,2]],[3,3]],[4,4]]</code> becomes <code><i>445</i></code>. </li>
     * <li> <code>[[[[3,0],[5,3]],[4,4]],[5,5]]</code> becomes <code><i>791</i></code>. </li>
     * <li> <code>[[[[5,0],[7,4]],[5,5]],[6,6]]</code> becomes <code><i>1137</i></code>. </li>
     * <li> <code>[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]</code> becomes <code><i>3488</i></code>. </li>
     * </ul>
     * <p>
     * So, given this example homework assignment:
     * <pre>
     * [[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
     * [[[5,[2,8]],4],[5,[[9,9],0]]]
     * [6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
     * [[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
     * [[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
     * [[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
     * [[[[5,4],[7,7]],8],[[8,3],8]]
     * [[9,3],[[9,9],[6,[4,9]]]]
     * [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
     * [[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]
     * </pre>
     * <p>
     * The final sum is:
     * <pre>
     * [[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]
     * </pre>
     * <p>
     * The magnitude of this final sum is <code><i>4140</i></code>.
     * <p>
     * Add up all of the snailfish numbers from the homework assignment in the order they appear.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the magnitude of the final sum?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.process(SnailfishNumber::parse).stream()
                .reduce(Day18::add)
                .orElseThrow()
                .magnitude();
    }

    /**
     * You notice a second question on the back of the homework assignment:
     * <p>
     * What is the largest magnitude you can get from adding only two of the snailfish numbers?
     * <p>
     * Note that snailfish addition is not <a href="https://en.wikipedia.org/wiki/Commutative_property" target="_blank">commutative</a> - that is, <code>x + y</code> and <code>y + x</code> can produce different results.
     * <p>
     * Again considering the last example homework assignment above:
     * <pre>
     * [[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
     * [[[5,[2,8]],4],[5,[[9,9],0]]]
     * [6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
     * [[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
     * [[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
     * [[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
     * [[[[5,4],[7,7]],8],[[8,3],8]]
     * [[9,3],[[9,9],[6,[4,9]]]]
     * [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
     * [[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]
     * </pre>
     * <p>
     * The largest magnitude of the sum of any two snailfish numbers in this list is <code><i>3993</i></code>.
     * This is the magnitude of <code>[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]</code> + <code>[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]</code>, which reduces to <code>[[[[7,8],[6,6]],[[6,0],[7,7]]],[[[7,8],[8,8]],[[7,9],[0,6]]]]</code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the largest magnitude of any sum of two different snailfish numbers from the homework assignment?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<SnailfishNumber> numbers = context.process(SnailfishNumber::parse);
        return numbers.stream()
                .mapToLong(number -> numbers.stream()
                        .filter(other -> number != other)
                        .map(other -> add(number, other))
                        .mapToLong(SnailfishNumber::magnitude)
                        .max()
                        .orElse(0)
                )
                .max()
                .orElseThrow();
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Add the two `SnailfishNumber`s together.
     */
    private static SnailfishNumber add(final SnailfishNumber number, final SnailfishNumber other) {
        return reduce(new CompoundNumber(number.adjustDepth(1), other.adjustDepth(1), 0));
    }

    /*
     * Reduce the given `SnailfishNumber`.
     */
    private static SnailfishNumber reduce(final SnailfishNumber number) {
        SnailfishNumber current = number;
        Optional<SnailfishNumber> next = Optional.of(current);
        while (next.isPresent()) {
            current = next.get();
            next = reduceOnce(current);
        }
        return current;
    }

    /*
     * Perform a single reduction on the given `SnailfishNumber`.
     */
    private static Optional<SnailfishNumber> reduceOnce(final SnailfishNumber number) {
        final ExplosionResult explosionResult = number.maybeExplode();
        if (explosionResult != ExplosionResult.NO_EXPLOSION)
            return Optional.of(explosionResult.epicentre);

        final SplitResult splitResult = number.maybeSplit();
        if (splitResult != SplitResult.NO_SPLIT)
            return Optional.of(splitResult.result);

        return Optional.empty();
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * A number used in snailfish arithmetic.
     */
    private sealed interface SnailfishNumber permits SimpleNumber, CompoundNumber {

        // Interface Methods

        /*
         * Calculate the magnitude of this `SnailfishNumber`.
         */
        long magnitude();

        /*
         * Adjust the left-hand side of this `SnailfishNumber` by the given
         * amount.
         */
        SnailfishNumber adjustLeft(long amount);

        /*
         * Adjust the right-hand side of this `SnailfishNumber` by the given
         * amount.
         */
        SnailfishNumber adjustRight(long amount);

        /*
         * Adjust the depth of this `SnailfishNumber` by the given amount.
         */
        SnailfishNumber adjustDepth(int amount);

        /*
         * Trigger an explosion, if possible, on this `SnailfishNumber`.
         */
        ExplosionResult maybeExplode();

        /*
         * Trigger a split, if possible, on this `SnailfishNumber`.
         */
        SplitResult maybeSplit();

        // Static Helper Methods

        /*
         * Parse a `SnailfishNumber` from the given line.
         */
        static SnailfishNumber parse(final String line) {
            final SnailfishNumberBuilder builder = new SnailfishNumberBuilder();
            for (int i = 0; i < line.length(); i++) {
                final char c = line.charAt(i);
                switch (c) {
                    case '[' -> builder.onStartCompound();
                    case ']' -> builder.onEndCompound();
                    case ',' -> builder.onNextInPair();
                    case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' -> builder.onRegularDigit(c - '0');
                    default -> throw new IllegalArgumentException("Invalid character '" + c + "' in Snailfish number");
                }
            }
            return builder.build();
        }
    }

    /*
     * A "simple" number.
     */
    private record SimpleNumber(long value, int depth) implements SnailfishNumber {

        // SnailfishNumber Methods

        @Override
        public long magnitude() {
            return value;
        }

        @Override
        public SnailfishNumber adjustLeft(final long amount) {
            return new SimpleNumber(value + amount, depth);
        }

        @Override
        public SnailfishNumber adjustRight(final long amount) {
            return new SimpleNumber(value + amount, depth);
        }

        @Override
        public SnailfishNumber adjustDepth(final int amount) {
            return new SimpleNumber(value, depth + amount);
        }

        @Override
        public ExplosionResult maybeExplode() {
            return ExplosionResult.NO_EXPLOSION;
        }

        @Override
        public SplitResult maybeSplit() {
            return value > 9
                    ? SplitResult.of(this)
                    : SplitResult.NO_SPLIT;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

    }

    /*
     * A "compound" number.
     */
    private record CompoundNumber(SnailfishNumber left, SnailfishNumber right, int depth) implements SnailfishNumber {

        // SnailfishNumber Methods

        @Override
        public long magnitude() {
            return 3 * left.magnitude() + 2 * right.magnitude();
        }

        @Override
        public SnailfishNumber adjustLeft(final long amount) {
            return new CompoundNumber(left.adjustLeft(amount), right, depth);
        }

        @Override
        public SnailfishNumber adjustRight(final long amount) {
            return new CompoundNumber(left, right.adjustRight(amount), depth);
        }

        @Override
        public SnailfishNumber adjustDepth(final int amount) {
            return new CompoundNumber(left.adjustDepth(1), right.adjustDepth(1), depth + amount);
        }

        @Override
        public ExplosionResult maybeExplode() {
            if (depth >= 4) return ExplosionResult.of(this);

            final ExplosionResult leftExplosion = left.maybeExplode();
            if (leftExplosion != ExplosionResult.NO_EXPLOSION)
                return leftExplosion.consumeRight(right);

            final ExplosionResult rightExplosion = right.maybeExplode();
            if (rightExplosion != ExplosionResult.NO_EXPLOSION)
                return rightExplosion.consumeLeft(left);

            return ExplosionResult.NO_EXPLOSION;
        }

        @Override
        public SplitResult maybeSplit() {
            final SplitResult leftSplit = left.maybeSplit();
            if (leftSplit != SplitResult.NO_SPLIT)
                return leftSplit.consumeRight(right);
            final SplitResult rightSplit = right.maybeSplit();
            if (rightSplit != SplitResult.NO_SPLIT)
                return rightSplit.consumeLeft(left);
            return SplitResult.NO_SPLIT;
        }

        @Override
        public String toString() {
            return "[" + left + "," + right + "]";
        }

    }

    /*
     * Build a `SnailfishNumber`.
     */
    private static final class SnailfishNumberBuilder {

        // Private Members

        private int depth = 0;
        private final Deque<SnailfishNumber> stack = new ArrayDeque<>();

        // Builder Methods

        /*
         * A new pair (a.k.a. "compound" number) has started.
         */
        public void onStartCompound() {
            ++depth;
        }

        /*
         * A new pair (a.k.a. "compound" number) has ended.
         */
        public void onEndCompound() {
            assert stack.size() >= 2;
            final SnailfishNumber right = stack.removeLast();
            final SnailfishNumber left = stack.removeLast();
            --depth;
            assert depth >= 0;
            stack.addLast(new CompoundNumber(left, right, depth));
        }

        /*
         * The first element in a pair has ended.
         */
        public void onNextInPair() {
            // No-op?
            assert !stack.isEmpty();
        }

        /*
         * A simple number has been read.
         */
        public void onRegularDigit(final int i) {
            stack.addLast(new SimpleNumber(i, depth));
        }

        /*
         * Build the `SnailfishNumber`.
         */
        public SnailfishNumber build() {
            assert stack.size() <= 1;
            return stack.isEmpty()
                    ? null
                    : stack.removeLast();
        }

    }

    /*
     * The result of exploding a `SnailfishNumber`.
     */
    private record ExplosionResult(long left, long right, int depth, SnailfishNumber epicentre) {

        /*
         * Indicates no explosion occurred.
         */
        static final ExplosionResult NO_EXPLOSION = new ExplosionResult(0, 0, 0, null);

        // Helper Methods

        /*
         * Return a new `ExplosionResult` as a result of consuming the left-hand
         * side of a `CompoundNumber` pair.
         */
        ExplosionResult consumeLeft(final SnailfishNumber number) {
            final SnailfishNumber newEpicentre = left > 0
                    ? new CompoundNumber(number.adjustRight(left), epicentre, depth - 1)
                    : new CompoundNumber(number, epicentre, depth - 1);
            return new ExplosionResult(0, right, depth - 1, newEpicentre);
        }

        /*
         * Return a new `ExplosionResult` as a result of consuming the right-hand
         * side of a `CompoundNumber` pair.
         */
        ExplosionResult consumeRight(final SnailfishNumber number) {
            final SnailfishNumber newEpicentre = right > 0
                    ? new CompoundNumber(epicentre, number.adjustLeft(right), depth - 1)
                    : new CompoundNumber(epicentre, number, depth - 1);
            return new ExplosionResult(left, 0, depth - 1, newEpicentre);
        }

        // Static Helper Methods

        /*
         * Create a new explosion for the given `CompoundNumber`.
         */
        static ExplosionResult of(final CompoundNumber number) {
            return new ExplosionResult(
                    number.left.magnitude(),
                    number.right.magnitude(),
                    number.depth,
                    new SimpleNumber(0, number.depth)
            );
        }

    }

    /*
     * The result of splitting a `SnailfishNumber`.
     */
    private record SplitResult(int depth, CompoundNumber result) {

        /*
         * Indicates no split occurred.
         */
        public static final SplitResult NO_SPLIT = new SplitResult(0, null);

        // Helper Methods

        /*
         * Return a new `SplitResult` as a result of consuming the left-hand side
         * of a `CompoundNumber` pair.
         */
        public SplitResult consumeLeft(final SnailfishNumber left) {
            return new SplitResult(depth - 1, new CompoundNumber(left, result, depth - 1));
        }

        /*
         * Return a new `SplitResult` as a result of consuming the right-hand
         * side of a `CompoundNumber` pair.
         */
        public SplitResult consumeRight(final SnailfishNumber right) {
            return new SplitResult(depth - 1, new CompoundNumber(result, right, depth - 1));
        }

        // Static Helper Methods

        /*
         * Creates a new split for the given `SimpleNumber`.
         */
        static SplitResult of(final SimpleNumber number) {
            return new SplitResult(
                    number.depth,
                    new CompoundNumber(
                            new SimpleNumber(number.value / 2, number.depth + 1),
                            new SimpleNumber((number.value + 1) / 2, number.depth + 1),
                            number.depth
                    )
            );
        }
    }

}
