package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.List;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 3: Rucksack Reorganization.
 */
@Solution(year = 2022, day = 3, title = "Rucksack Reorganization")
public class Day3 {


    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * One Elf has the important job of loading all of the <a href="https://en.wikipedia.org/wiki/Rucksack" target="_blank">rucksacks</a> with supplies for the <span title="Where there's jungle, there's hijinxs.">jungle</span> journey.
     * Unfortunately, that Elf didn't quite follow the packing instructions, and so a few items now need to be rearranged.
     * <p>
     * Each rucksack has two large <em>compartments</em>.
     * All items of a given type are meant to go into exactly one of the two compartments.
     * The Elf that did the packing failed to follow this rule for exactly one item type per rucksack.
     * <p>
     * The Elves have made a list of all of the items currently in each rucksack (your puzzle input), but they need your help finding the errors.
     * Every item type is identified by a single lowercase or uppercase letter (that is, <code>a</code> and <code>A</code> refer to different types of items).
     * <p>
     * The list of items for each rucksack is given as characters all on a single line.
     * A given rucksack always has the same number of items in each of its two compartments, so the first half of the characters represent items in the first compartment, while the second half of the characters represent items in the second compartment.
     * <p>
     * For example, suppose you have the following list of contents from six rucksacks:
     * <pre>
     * vJrwpWtwJgWrhcsFMMfFFhFp
     * jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
     * PmmdzqPrVvPwwTWBwg
     * wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
     * ttgJtRGJQctTZtZT
     * CrZsJsPPZsGzwwsLwLmpwMDw
     * </pre>
     *
     * <ul>
     * <li>
     * The first rucksack contains the items <code>vJrwpWtwJgWrhcsFMMfFFhFp</code>, which means its first compartment contains the items <code>vJrwpWtwJgWr</code>, while the second compartment contains the items <code>hcsFMMfFFhFp</code>.
     * The only item type that appears in both compartments is lowercase <code><em>p</em></code>.
     * </li>
     * <li>
     * The second rucksack's compartments contain <code>jqHRNqRjqzjGDLGL</code> and <code>rsFMfFZSrLrFZsSL</code>.
     * The only item type that appears in both compartments is uppercase <code><em>L</em></code>.
     * </li>
     * <li>The third rucksack's compartments contain <code>PmmdzqPrV</code> and <code>vPwwTWBwg</code>; the only common item type is uppercase <code><em>P</em></code>.</li>
     * <li>The fourth rucksack's compartments only share item type <code><em>v</em></code>.</li>
     * <li>The fifth rucksack's compartments only share item type <code><em>t</em></code>.</li>
     * <li>The sixth rucksack's compartments only share item type <code><em>s</em></code>.</li>
     * </ul>
     * <p>
     * To help prioritize item rearrangement, every item type can be converted to a <em>priority</em>:
     * <ul>
     * <li>Lowercase item types <code>a</code> through <code>z</code> have priorities 1 through 26.</li>
     * <li>Uppercase item types <code>A</code> through <code>Z</code> have priorities 27 through 52.</li>
     * </ul>
     * <p>
     * In the above example, the priority of the item type that appears in both compartments of each rucksack is 16 (<code>p</code>), 38 (<code>L</code>), 42 (<code>P</code>), 22 (<code>v</code>), 20 (<code>t</code>), and 19 (<code>s</code>); the sum of these is <code><em>157</em></code>.
     * <p>
     * Find the item type that appears in both compartments of each rucksack.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the priorities of those item types?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .mapToLong(line -> IntStream.range(0, line.length() / 2)
                        .map(line::charAt)
                        .filter(chr -> line.indexOf(chr, line.length() / 2) >= 0)
                        .distinct()
                        .map(Day3::toScore)
                        .sum())
                .sum();
    }

    /**
     * As you finish identifying the misplaced items, the Elves come to you with another issue.
     * <p>
     * For safety, the Elves are divided into groups of three.
     * Every Elf carries a badge that identifies their group.
     * For efficiency, within each group of three Elves, the badge is the <em>only item type carried by all three Elves</em>.
     * That is, if a group's badge is item type <code>B</code>, then all three Elves will have item type <code>B</code> somewhere in their rucksack, and at most two of the Elves will be carrying any other item type.
     * <p>
     * The problem is that someone forgot to put this year's updated authenticity sticker on the badges.
     * All of the badges need to be pulled out of the rucksacks so the new authenticity stickers can be attached.
     * <p>
     * Additionally, nobody wrote down which item type corresponds to each group's badges.
     * The only way to tell which item type is the right one is by finding the one item type that is <em>common between all three Elves</em> in each group.
     * <p>
     * Every set of three lines in your list corresponds to a single group, but each group can have a different badge item type.
     * So, in the above example, the first group's rucksacks are the first three lines:
     * <pre>
     * vJrwpWtwJgWrhcsFMMfFFhFp
     * jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
     * PmmdzqPrVvPwwTWBwg
     * </pre>
     * <p>
     * And the second group's rucksacks are the next three lines:
     * <pre>
     * wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
     * ttgJtRGJQctTZtZT
     * CrZsJsPPZsGzwwsLwLmpwMDw
     * </pre>
     * <p>
     * In the first group, the only item type that appears in all three rucksacks is lowercase <code>r</code>; this must be their badges.
     * In the second group, their badge item type must be <code>Z</code>.
     * <p>
     * Priorities for these items must still be found to organize the sticker attachment efforts: here, they are 18 (<code>r</code>) for the first group and 52 (<code>Z</code>) for the second group.
     * The sum of these is <code><em>70</em></code>.
     * <p>
     * Find the item type that corresponds to the badges of each three-Elf group.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the priorities of those item types?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<String> backpacks = context.read();
        return IntStream.iterate(0, i -> i < backpacks.size(), i -> i + 3)
                .mapToLong(i -> {
                    final String e1 = backpacks.get(i);
                    final String e2 = backpacks.get(i + 1);
                    final String e3 = backpacks.get(i + 2);
                    return e1.chars()
                            .filter(c -> e2.indexOf((char) c) >= 0 && e3.indexOf((char) c) >= 0)
                            .mapToObj(c -> (char) c)
                            .findFirst()
                            .map(Day3::toScore)
                            .orElseThrow(() -> new IllegalStateException("Failed to find a common badge!"));
                })
                .sum();
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Convert the item indicator to a score.
     */
    private static int toScore(final int c) {
        if (c >= 'a' && c <= 'z') return 1 + (c - 'a');
        else if (c >= 'A' && c <= 'Z') return 27 + (c - 'A');
        else throw new IllegalStateException("Cannot determine score of item: " + ((char) c));
    }

}
