package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Direction;
import net.anomalyxii.aoc.utils.geometry.Grid;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;
import static net.anomalyxii.aoc.utils.geometry.Direction.*;

/**
 * Day 8: Treetop Tree House.
 */
@Solution(year = 2022, day = 8, title = "Treetop Tree House")
public class Day8 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The expedition comes across a peculiar patch of tall trees all planted carefully in a grid.
     * The Elves explain that a previous expedition planted these trees as a reforestation effort.
     * Now, they're curious if this would be a good location for a <a href="https://en.wikipedia.org/wiki/Tree_house" target="_blank">tree house</a>.
     * <p>
     * First, determine whether there is enough tree cover here to keep a tree house <em>hidden</em>.
     * To do this, you need to count the number of trees that are <em>visible from outside the grid</em> when looking directly along a row or column.
     * <p>
     * The Elves have already launched a <a href="https://en.wikipedia.org/wiki/Quadcopter" target="_blank">quadcopter</a> to generate a map with the height of each tree (<span title="The Elves have already launched a quadcopter (your puzzle input).">your puzzle input</span>).
     * For example:
     * <pre>
     * 30373
     * 25512
     * 65332
     * 33549
     * 35390
     * </pre>
     * <p>
     * Each tree is represented as a single digit whose value is its height, where <code>0</code> is the shortest and <code>9</code> is the tallest.
     * <p>
     * A tree is <em>visible</em> if all of the other trees between it and an edge of the grid are <em>shorter</em> than it.
     * Only consider trees in the same row or column; that is, only look up, down, left, or right from any given tree.
     * <p>
     * All of the trees around the edge of the grid are <em>visible</em> - since they are already on the edge, there are no trees to block the view.
     * In this example, that only leaves the <em>interior nine trees</em> to consider:
     * <ul>
     * <li>The top-left <code>5</code> is <em>visible</em> from the left and top.
     * (It isn't visible from the right or bottom since other trees of height <code>5</code> are in the way.)</li>
     * <li>The top-middle <code>5</code> is <em>visible</em> from the top and right.</li>
     * <li>The top-right <code>1</code> is not visible from any direction; for it to be visible, there would need to only be trees of height <em>0</em> between it and an edge.</li>
     * <li>The left-middle <code>5</code> is <em>visible</em>, but only from the right.</li>
     * <li>The center <code>3</code> is not visible from any direction; for it to be visible, there would need to be only trees of at most height <code>2</code> between it and an edge.</li>
     * <li>The right-middle <code>3</code> is <em>visible</em> from the right.</li>
     * <li>In the bottom row, the middle <code>5</code> is <em>visible</em>, but the <code>3</code> and <code>4</code> are not.</li>
     * </ul>
     * <p>
     * With 16 trees visible on the edge and another 5 visible in the interior, a total of <code><em>21</em></code> trees are visible in this arrangement.
     * Consider your map;
     *
     * @param context the {@link SolutionContext} to solve against
     * @return how many trees are visible from outside the grid?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Grid forest = context.readGrid(c -> c - '0');

        return forest.entries()
                .filter(entry -> {
                    final Coordinate coordinate = entry.getKey();
                    final int value = entry.getValue();
                    return isVisibleInDirection(forest, coordinate, value, UP)
                            || isVisibleInDirection(forest, coordinate, value, LEFT)
                            || isVisibleInDirection(forest, coordinate, value, DOWN)
                            || isVisibleInDirection(forest, coordinate, value, RIGHT);
                })
                .count();
    }

    /**
     * Content with the amount of tree cover available, the Elves just need to know the best spot to build their tree house: they would like to be able to see a lot of <em>trees</em>.
     * <p>
     * To measure the viewing distance from a given tree, look up, down, left, and right from that tree; stop if you reach an edge or at the first tree that is the same height or taller than the tree under consideration.
     * (If a tree is right on the edge, at least one of its viewing distances will be zero.)
     * <p>
     * The Elves don't care about distant trees taller than those found by the rules above; the proposed tree house has large <a href="https://en.wikipedia.org/wiki/Eaves" target="_blank">eaves</a> to keep it dry, so they wouldn't be able to see higher than the tree house anyway.
     * <p>
     * In the example above, consider the middle <code>5</code> in the second row:
     * <pre>
     * 30373
     * 25<em>5</em>12
     * 65332
     * 33549
     * 35390
     * </pre>
     * <ul>
     * <li>Looking up, its view is not blocked; it can see <code><em>1</em></code> tree (of height <code>3</code>).</li>
     * <li>Looking left, its view is blocked immediately; it can see only <code><em>1</em></code> tree (of height <code>5</code>, right next to it).</li>
     * <li>Looking right, its view is not blocked; it can see <code><em>2</em></code> trees.</li>
     * <li>Looking down, its view is blocked eventually; it can see <code><em>2</em></code> trees (one of height <code>3</code>, then the tree of height <code>5</code> that blocks its view).</li>
     * </ul>
     * <p>
     * A tree's <em>scenic score</em> is found by <em>multiplying together</em> its viewing distance in each of the four directions.
     * For this tree, this is <code><em>4</em></code> (found by multiplying <code>1 * 1 * 2 * 2</code>).
     * <p>
     * However, you can do even better: consider the tree of height <code>5</code> in the middle of the fourth row:
     * <pre>
     * 30373
     * 25512
     * 65332
     * 33<em>5</em>49
     * 35390
     * </pre>
     * <ul>
     * <li>Looking up, its view is blocked at <code><em>2</em></code> trees (by another tree with a height of <code>5</code>).</li>
     * <li>Looking left, its view is not blocked; it can see <code><em>2</em></code> trees.</li>
     * <li>Looking down, its view is also not blocked; it can see <code><em>1</em></code> tree.</li>
     * <li>Looking right, its view is blocked at <code><em>2</em></code> trees (by a massive tree of height <code>9</code>).</li>
     * </ul>
     * <p>
     * This tree's scenic score is <code><em>8</em></code> (<code>2 * 2 * 1 * 2</code>); this is the ideal spot for the tree house.
     * <p>
     * Consider each tree on your map.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the highest scenic score possible for any tree?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Grid forest = context.readGrid(c -> c - '0');
        return forest.entries()
                .mapToLong(entry -> {
                    final Coordinate coordinate = entry.getKey();
                    final int value = entry.getValue();

                    return calculateViewingDistanceInDirection(forest, coordinate, value, UP)
                            * calculateViewingDistanceInDirection(forest, coordinate, value, LEFT)
                            * calculateViewingDistanceInDirection(forest, coordinate, value, DOWN)
                            * calculateViewingDistanceInDirection(forest, coordinate, value, RIGHT);
                })
                .max()
                .orElseThrow(() -> new IllegalStateException("No trees were found!"));
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Check if a tree is visible, i.e. there are no trees of equal or greater
     * height, in a given direction.
     */
    private static boolean isVisibleInDirection(
            final Grid forest,
            final Coordinate coordinate,
            final int value,
            final Direction direction
    ) {

        Coordinate next = coordinate.adjustBy(direction);
        while (forest.contains(next)) {
            if (forest.get(next) >= value)
                return false;
            next = next.adjustBy(direction);
        }

        return true;
    }

    /*
     * Calculate the viewing distance from a given tree.
     *
     * The viewing distance is equal to the number of trees that can be seen
     * in the given direction, until reaching a tree of greater or equal
     * height to the starting tree, or the edge of the forest.
     *
     * Note: if the viewing distance is terminated by reaching a taller tree,
     * this tree _is_ included in the viewing distance.
     */
    private static long calculateViewingDistanceInDirection(
            final Grid forest,
            final Coordinate coordinate,
            final int value,
            final Direction direction
    ) {
        int viewingDistance = 0;
        Coordinate next = coordinate.adjustBy(direction);
        while (forest.contains(next)) {
            viewingDistance += 1;

            if (forest.get(next) >= value) break;
            next = next.adjustBy(direction);
        }

        return viewingDistance;
    }

}
