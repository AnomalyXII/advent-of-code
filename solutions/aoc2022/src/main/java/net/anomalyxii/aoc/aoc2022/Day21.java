package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.maths.Operation;

import java.util.*;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 21: Monkey Math.
 */
@Solution(year = 2022, day = 21, title = "Monkey Math")
public class Day21 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The <a href="11">monkeys</a> are back!
     * You're worried they're going to try to steal your stuff again, but it seems like they're just holding their ground and making various monkey noises at you.
     * <p>
     * Eventually, one of the elephants realizes you don't speak monkey and comes over to interpret.
     * As it turns out, they overheard you talking about trying to find the grove; they can show you a shortcut if you answer their <em>riddle</em>.
     * <p>
     * Each monkey is given a <em>job</em>: either to <em>yell a specific number</em> or to <em>yell the result of a math operation</em>.
     * All of the number-yelling monkeys know their number from the start; however, the math operation monkeys need to wait for two other monkeys to yell a number, and those two other monkeys might <em>also</em> be waiting on other monkeys.
     * <p>
     * Your job is to <em>work out the number the monkey named <code>root</code> will yell</em> before the monkeys figure it out themselves.
     * <p>
     * For example:
     * <pre>
     * root: pppw + sjmn
     * dbpl: 5
     * cczh: sllz + lgvd
     * zczc: 2
     * ptdq: humn - dvpt
     * dvpt: 3
     * lfqf: 4
     * humn: 5
     * ljgn: 2
     * sjmn: drzm * dbpl
     * sllz: 4
     * pppw: cczh / lfqf
     * lgvd: ljgn * ptdq
     * drzm: hmdt - zczc
     * hmdt: 32
     * </pre>
     * <p>
     * Each line contains the name of a monkey, a colon, and then the job of that monkey:
     * <ul>
     * <li>A lone number means the monkey's job is simply to yell that number.</li>
     * <li>A job like <code>aaaa + bbbb</code> means the monkey waits for monkeys <code>aaaa</code> and <code>bbbb</code> to yell each of their numbers; the monkey then yells the sum of those two numbers.</li>
     * <li><code>aaaa - bbbb</code> means the monkey yells <code>aaaa</code>'s number minus <code>bbbb</code>'s number.</li>
     * <li>Job <code>aaaa * bbbb</code> will yell <code>aaaa</code>'s number multiplied by <code>bbbb</code>'s number.</li>
     * <li>Job <code>aaaa / bbbb</code> will yell <code>aaaa</code>'s number divided by <code>bbbb</code>'s number.</li>
     * </ul>
     * <p>
     * So, in the above example, monkey <code>drzm</code> has to wait for monkeys <code>hmdt</code> and <code>zczc</code> to yell their numbers.
     * Fortunately, both <code>hmdt</code> and <code>zczc</code> have jobs that involve simply yelling a single number, so they do this immediately: <code>32</code> and <code>2</code>.
     * Monkey <code>drzm</code> can then yell its number by finding <code>32</code> minus <code>2</code>: <code><em>30</em></code>.
     * <p>
     * Then, monkey <code>sjmn</code> has one of its numbers (<code>30</code>, from monkey <code>drzm</code>), and already has its other number, <code>5</code>, from <code>dbpl</code>.
     * This allows it to yell its own number by finding <code>30</code> multiplied by <code>5</code>: <code><em>150</em></code>.
     * <p>
     * This process continues until <code>root</code> yells a number: <code><em>152</em></code>.
     * <p>
     * However, your actual situation involves <span title="Advent of Code 2022: Now With Considerably More Monkeys">considerably more monkeys</span>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What number will the monkey named <code>root</code> yell?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<MathsMonkey> monkeys = new ArrayList<>(context.process(MathsMonkey::parse));

        final Map<String, Long> solved = new HashMap<>();
        while (!solved.containsKey("root") && !monkeys.isEmpty()) {
            final Iterator<MathsMonkey> it = monkeys.iterator();
            while (it.hasNext()) {
                final MathsMonkey monkey = it.next();
                if (monkey.canSolve(solved)) {
                    final long answer = monkey.shoutNumber(solved);
                    solved.put(monkey.name(), answer);
                    it.remove();
                }
            }
        }

        assert solved.containsKey("root");
        return solved.get("root");
    }

    /**
     * Due to some kind of monkey-elephant-human mistranslation, you seem to have misunderstood a few key details about the riddle.
     * <p>
     * First, you got the wrong job for the monkey named <code>root</code>; specifically, you got the wrong math operation.
     * The correct operation for monkey <code>root</code> should be <code>=</code>, which means that it still listens for two numbers (from the same two monkeys as before), but now checks that the two numbers <em>match</em>.
     * <p>
     * Second, you got the wrong monkey for the job starting with <code>humn:</code>.
     * It isn't a monkey - it's <em>you</em>.
     * Actually, you got the job wrong, too: you need to figure out <em>what number you need to yell</em> so that <code>root</code>'s equality check passes.
     * (The number that appears after <code>humn:</code> in your input is now irrelevant.)
     * <p>
     * In the above example, the number you need to yell to pass <code>root</code>'s equality test is <code><em>301</em></code>.
     * (This causes <code>root</code> to get the same number, <code>150</code>, from both of its monkeys.)
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What number do you yell to pass <code>root</code>'s equality test?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<MathsMonkey> monkeys = new ArrayList<>(context.process(MathsMonkey::parse));

        final MathsMonkey humn = monkeys.stream().filter(m -> "humn".equals(m.name())).findFirst().orElseThrow();
        final MathsMonkey root = monkeys.stream().filter(m -> "root".equals(m.name())).findFirst().orElseThrow();
        monkeys.remove(humn);
        monkeys.remove(root);

        final Map<String, Long> solved = new HashMap<>();
        final Map<String, MathsMonkey> cantSolveYet = new HashMap<>();
        while (!solved.containsKey("root") && !monkeys.isEmpty()) {
            final Iterator<MathsMonkey> it = monkeys.iterator();
            while (it.hasNext()) {
                final MathsMonkey monkey = it.next();
                if (monkey.canSolve(solved)) {
                    final long answer = monkey.shoutNumber(solved);
                    solved.put(monkey.name(), answer);
                    it.remove();
                } else if (monkey.dependsOnHuman(cantSolveYet)) {
                    cantSolveYet.put(monkey.name(), monkey);
                    it.remove();
                }
            }
        }

        cantSolveYet.put("humn", new HumanMonkey());
        return root.determineHumanNumber(solved, cantSolveYet);
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A monkey that can, apparently, also do some kind of maths.
     */
    private sealed interface MathsMonkey permits HumanMonkey, NumberMonkey, OperationMonkey {

        // Interface Methods

        /**
         * Get the name of this {@link MathsMonkey monkey}.
         *
         * @return the monkey's name
         */
        String name();

        /**
         * Check if this {@link MathsMonkey monkey} has enough information to
         * determine what number it should shout.
         *
         * @param previous a {@link Map} of {@link MathsMonkey monkeys} and the number they shouted
         * @return {@literal true} if this {@link MathsMonkey} can shout its number; {@literal false} otherwise
         */
        boolean canSolve(Map<String, Long> previous);

        /**
         * Check if this {@link MathsMonkey monkey} depends on a
         * {@link HumanMonkey human} shouting a number.
         *
         * @param needsHuman a {@link Map} of {@link MathsMonkey monkeys} are blocked on a {@link HumanMonkey human}
         * @return {@literal true} if this {@link MathsMonkey} cannot shout a number; {@literal false} otherwise
         */
        boolean dependsOnHuman(Map<String, MathsMonkey> needsHuman);

        /**
         * Get the {@link MathsMonkey monkey} to shout out its number.
         *
         * @param previous a {@link Map} of {@link MathsMonkey monkeys} and the number they shouted
         * @return the number that this {@link MathsMonkey} should shout
         */
        long shoutNumber(Map<String, Long> previous);

        /**
         * Convince this {@link MathsMonkey monkey} to reverse engineer the
         * puzzle and determine what number a {@link HumanMonkey human} should
         * shout for the game to complete successfully.
         *
         * @param previous   a {@link Map} of {@link MathsMonkey monkeys} and the number they shouted
         * @param needsHuman a {@link Map} of {@link MathsMonkey monkeys} are blocked on a {@link HumanMonkey human}
         * @return the number that a {@link HumanMonkey} should shout
         */
        long determineHumanNumber(Map<String, Long> previous, Map<String, MathsMonkey> needsHuman);

        /**
         * Convince this {@link MathsMonkey monkey} to reverse engineer the
         * puzzle and determine what number a {@link HumanMonkey human} should
         * shout for the game to complete successfully.
         *
         * @param toSolveFor the target that this {@link MathsMonkey} is aiming for
         * @param previous   a {@link Map} of {@link MathsMonkey monkeys} and the number they shouted
         * @param needsHuman a {@link Map} of {@link MathsMonkey monkeys} are blocked on a {@link HumanMonkey human}
         * @return the number that a {@link HumanMonkey} should shout
         */
        long determineHumanNumber(long toSolveFor, Map<String, Long> previous, Map<String, MathsMonkey> needsHuman);

        // Helper Methods

        /**
         * Create a new {@link MathsMonkey monkey} from the given input line.
         *
         * @param line the input
         * @return the {@link MathsMonkey}
         */
        static MathsMonkey parse(final String line) {
            final String[] parts = line.split(": ");
            final String name = parts[0];
            if (parts[1].matches("[0-9]+"))
                return new NumberMonkey(name, Integer.parseInt(parts[1]));

            final String[] parts2 = parts[1].split(" ");
            return new OperationMonkey(name, parts2[0], Operation.fromString(parts2[1]), parts2[2]);
        }

    }

    /*
     * A `MathsMonkey` that will just shout a number.
     */
    private record NumberMonkey(String name, int number) implements MathsMonkey {

        // MathsMonkey Methods

        @Override
        public boolean canSolve(final Map<String, Long> previous) {
            return true;
        }

        @Override
        public boolean dependsOnHuman(final Map<String, MathsMonkey> needsHuman) {
            return false;
        }

        @Override
        public long shoutNumber(final Map<String, Long> previous) {
            return number;
        }

        @Override
        public long determineHumanNumber(final Map<String, Long> previous, final Map<String, MathsMonkey> needsHuman) {
            return number;
        }

        @Override
        public long determineHumanNumber(final long toSolveFor, final Map<String, Long> previous, final Map<String, MathsMonkey> needsHuman) {
            assert toSolveFor == number;
            return number;
        }

    }

    /*
     * A `MathsMonkey` that must perform a maths operation to determine what
     * number it needs to shout.
     */
    private record OperationMonkey(String name, String lhs, Operation op, String rhs) implements MathsMonkey {

        // MathsMonkey Methods

        @Override
        public boolean canSolve(final Map<String, Long> previous) {
            return previous.containsKey(lhs) && previous.containsKey(rhs);
        }

        @Override
        public boolean dependsOnHuman(final Map<String, MathsMonkey> needsHuman) {
            return "humn".equals(lhs)
                    || needsHuman.containsKey(lhs)
                    || "humn".equals(rhs)
                    || needsHuman.containsKey(rhs);
        }

        @Override
        public long determineHumanNumber(final Map<String, Long> previous, final Map<String, MathsMonkey> needsHuman) {
            if (previous.containsKey(lhs)) {
                final long left = previous.get(lhs);
                return needsHuman.get(rhs).determineHumanNumber(left, previous, needsHuman);
            }

            if (previous.containsKey(rhs)) {
                final long right = previous.get(rhs);
                return needsHuman.get(lhs).determineHumanNumber(right, previous, needsHuman);
            }

            throw new IllegalStateException("Eeek!");
        }

        @Override
        public long determineHumanNumber(final long toSolveFor, final Map<String, Long> previous, final Map<String, MathsMonkey> needsHuman) {
            if (previous.containsKey(lhs)) {
                final long left = op.reverseRight(toSolveFor, previous.get(lhs));
                return needsHuman.get(rhs).determineHumanNumber(left, previous, needsHuman);
            }

            if (previous.containsKey(rhs)) {
                final long right = op.reverseLeft(toSolveFor, previous.get(rhs));
                return needsHuman.get(lhs).determineHumanNumber(right, previous, needsHuman);
            }

            throw new IllegalStateException("Eeek!");
        }

        @Override
        public long shoutNumber(final Map<String, Long> previous) {
            final long left = previous.get(lhs);
            final long right = previous.get(rhs);
            return op.apply(left, right);
        }
    }

    /*
     * A "Human" `MathsMonkey` that doesn't know what number it needs to
     * shout but can, possibly, derive it from what the monkeys that depend
     * on it.
     */
    private static final class HumanMonkey implements MathsMonkey {

        // MathsMonkey Methods

        @Override
        public String name() {
            return "humn";
        }

        @Override
        public boolean canSolve(final Map<String, Long> previous) {
            return false;
        }

        @Override
        public boolean dependsOnHuman(final Map<String, MathsMonkey> needsHuman) {
            return false;
        }

        @Override
        public long shoutNumber(final Map<String, Long> previous) {
            throw new IllegalStateException("Should not be solving for HUMN");
        }

        @Override
        public long determineHumanNumber(final Map<String, Long> previous, final Map<String, MathsMonkey> needsHuman) {
            throw new IllegalStateException("Should not be solving for HUMN");
        }

        @Override
        public long determineHumanNumber(final long toSolveFor, final Map<String, Long> previous, final Map<String, MathsMonkey> needsHuman) {
            return toSolveFor;
        }

    }

}

