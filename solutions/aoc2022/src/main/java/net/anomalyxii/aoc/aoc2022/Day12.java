package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.algorithms.Dijkstra;
import net.anomalyxii.aoc.utils.algorithms.ShortestPath;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.ArrayList;
import java.util.List;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 12: Hill Climbing Algorithm.
 */
@Solution(year = 2022, day = 12, title = "Hill Climbing Algorithm")
public class Day12 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You try contacting the Elves using your <span title="When you look up the specs for your handheld device, every field just says &quot;plot&quot;.">handheld device</span>, but the river you're following must be too low to get a decent signal.
     * <p>
     * You ask the device for a heightmap of the surrounding area (your puzzle input).
     * The heightmap shows the local area from above broken into a grid; the elevation of each square of the grid is given by a single lowercase letter, where <code>a</code> is the lowest elevation, <code>b</code> is the next-lowest, and so on up to the highest elevation, <code>z</code>.
     * <p>
     * Also included on the heightmap are marks for your current position (<code>S</code>) and the location that should get the best signal (<code>E</code>).
     * Your current position (<code>S</code>) has elevation <code>a</code>, and the location that should get the best signal (<code>E</code>) has elevation <code>z</code>.
     * <p>
     * You'd like to reach <code>E</code>, but to save energy, you should do it in <em>as few steps as possible</em>.
     * During each step, you can move exactly one square up, down, left, or right.
     * To avoid needing to get out your climbing gear, the elevation of the destination square can be <em>at most one higher</em> than the elevation of your current square; that is, if your current elevation is <code>m</code>, you could step to elevation <code>n</code>, but not to elevation <code>o</code>.
     * (This also means that the elevation of the destination square can be much lower than the elevation of your current square.)
     * <p>
     * For example:
     * <pre>
     * <em>S</em>abqponm
     * abcryxxl
     * accsz<em>E</em>xk
     * acctuvwj
     * abdefghi
     * </pre>
     * <p>
     * Here, you start in the top-left corner; your goal is near the middle.
     * You could start by moving down or right, but eventually you'll need to head toward the <code>e</code> at the bottom.
     * From there, you can spiral around to the goal:
     * <pre>
     * v..v&lt;&lt;&lt;&lt;
     * &gt;v.vv&lt;&lt;^
     * .&gt;vv&gt;E^^
     * ..v&gt;&gt;&gt;^^
     * ..&gt;&gt;&gt;&gt;&gt;^
     * </pre>
     * <p>
     * In the above diagram, the symbols indicate whether the path exits each square moving up (<code>^</code>), down (<code>v</code>), left (<code>&lt;</code>), or right (<code>&gt;</code>).
     * The location that should get the best signal is still <code>E</code>, and <code>.</code> marks unvisited squares.
     * <p>
     * This path reaches the goal in <code><em>31</em></code> steps, the fewest possible.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the fewest steps required to move from your current position to the location that should get the best signal?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Grid grid = context.readGrid();
        final Coordinate start = findStart(grid);
        final Coordinate end = findEnd(grid);

        final ShortestPath sp = new Dijkstra(Day12::resolve);
        return sp.solve(grid, start, end);
    }

    /**
     * As you walk up the hill, you suspect that the Elves will want to turn this into a hiking trail.
     * The beginning isn't very scenic, though; perhaps you can find a better starting point.
     * <p>
     * To maximize exercise while hiking, the trail should start as low as possible: elevation <code>a</code>.
     * The goal is still the square marked <code>E</code>.
     * However, the trail should still be direct, taking the fewest steps to reach its goal.
     * So, you'll need to find the shortest path from <em>any square at elevation <code>a</code></em> to the square marked <code>E</code>.
     * <p>
     * Again consider the example from above:
     * <pre>
     * <em>S</em>abqponm
     * abcryxxl
     * accsz<em>E</em>xk
     * acctuvwj
     * abdefghi
     * </pre>
     * <p>
     * Now, there are six choices for starting position (five marked <code>a</code>, plus the square marked <code>S</code> that counts as being at elevation <code>a</code>).
     * If you start at the bottom-left square, you can reach the goal most quickly:
     * <pre>
     * ...v&lt;&lt;&lt;&lt;
     * ...vv&lt;&lt;^
     * ...v&gt;E^^
     * .&gt;v&gt;&gt;&gt;^^
     * &gt;^&gt;&gt;&gt;&gt;&gt;^
     * </pre>
     * <p>
     * This path reaches the goal in only <code><em>29</em></code> steps, the fewest possible.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the fewest steps required to move starting from any square with elevation <code>a</code> to the location that should get the best signal?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Grid grid = context.readGrid();

        long shortestPath = Long.MAX_VALUE;
        for (final Coordinate start : findPossibleStarts(grid)) {
            final Coordinate end = findEnd(grid);

            final ShortestPath sp = new Dijkstra(Day12::resolve);
            final long distance = sp.solve(grid, start, end);
            if (distance < shortestPath)
                shortestPath = distance;
        }

        return shortestPath;
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Find the "start" co-ordinate (marked with an 'S').
     */
    private static Coordinate findStart(final Grid grid) {
        for (final Coordinate coord : grid) {
            if (grid.get(coord) == 'S')
                return coord;
        }

        throw new IllegalStateException("Start co-ordinate was not found?");
    }

    /*
     * Find all possible "start" co-ordinates (marked with an 'S' or an 'a').
     */
    private static List<Coordinate> findPossibleStarts(final Grid grid) {
        final List<Coordinate> possibleStarts = new ArrayList<>();
        for (final Coordinate coord : grid) {
            final int height = grid.get(coord);
            if (height == 'S' || height == 'a')
                possibleStarts.add(coord);
        }

        if (possibleStarts.isEmpty())
            throw new IllegalStateException("Start co-ordinate was not found?");

        return possibleStarts;
    }

    /*
     * Find the "end" co-ordinate (marked with an 'E').
     */
    private static Coordinate findEnd(final Grid grid) {
        for (final Coordinate coord : grid) {
            if (grid.get(coord) == 'E')
                return coord;
        }

        throw new IllegalStateException("Start co-ordinate was not found?");
    }

    /*
     * Calculate the cost of moving from one position to another.
     *
     * The cost is 1 if the current position is _at most_ one unit lower than
     * the target position; otherwise the cost is /too damn high/.
     */
    private static long resolve(final long curr, final long next, final long prio) {
        final long current = (curr == 'S' ? 'a' : curr) - 'a';
        final long target = (next == 'E' ? 'z' : next) - 'a';
        return target <= (current + 1) ? prio + 1L : (long) Integer.MAX_VALUE;
    }

}

