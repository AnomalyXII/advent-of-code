package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.Arrays.stream;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 19: Not Enough Minerals.
 */
@Solution(year = 2022, day = 19, title = "Not Enough Minerals")
public class Day19 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Your scans show that the lava did indeed form obsidian!
     * <p>
     * The wind has changed direction enough to stop sending lava droplets toward you, so you and the elephants exit the cave.
     * As you do, you notice a collection of <a href="https://en.wikipedia.org/wiki/Geode" target="_blank">geodes</a> around the pond.
     * Perhaps you could use the obsidian to create some <em>geode-cracking robots</em> and break them open?
     * <p>
     * To collect the obsidian from the bottom of the pond, you'll need waterproof <em>obsidian-collecting robots</em>.
     * Fortunately, there is an abundant amount of clay nearby that you can use to make them waterproof.
     * <p>
     * In order to harvest the clay, you'll need special-purpose <em>clay-collecting robots</em>.
     * To make any type of robot, you'll need <em>ore</em>, which is also plentiful but in the opposite direction from the clay.
     * <p>
     * Collecting ore requires <em>ore-collecting robots</em> with big drills.
     * Fortunately, <em>you have exactly one ore-collecting robot</em> in your pack that you can use to <span title="If You Give A Mouse An Ore-Collecting Robot">kickstart</span> the whole operation.
     * <p>
     * Each robot can collect 1 of its resource type per minute.
     * It also takes one minute for the robot factory (also conveniently from your pack) to construct any type of robot, although it consumes the necessary resources available when construction begins.
     * <p>
     * The robot factory has many <em>blueprints</em> (your puzzle input) you can choose from, but once you've configured it with a blueprint, you can't change it.
     * You'll need to work out which blueprint is best.
     * <p>
     * For example:
     * <pre>
     * Blueprint 1:
     *   Each ore robot costs 4 ore.
     *   Each clay robot costs 2 ore.
     *   Each obsidian robot costs 3 ore and 14 clay.
     *   Each geode robot costs 2 ore and 7 obsidian.
     *
     * Blueprint 2:
     *   Each ore robot costs 2 ore.
     *   Each clay robot costs 3 ore.
     *   Each obsidian robot costs 3 ore and 8 clay.
     *   Each geode robot costs 3 ore and 12 obsidian.
     * </pre>
     * <p>
     * (Blueprints have been line-wrapped here for legibility.
     * The robot factory's actual assortment of blueprints are provided one blueprint per line.)
     * <p>
     * The elephants are starting to look hungry, so you shouldn't take too long; you need to figure out which blueprint would maximize the number of opened geodes after <em>24 minutes</em> by figuring out which robots to build and when to build them.
     * <p>
     * Using blueprint 1 in the example above, the largest number of geodes you could open in 24 minutes is <code><em>9</em></code>.
     * One way to achieve that is:
     * <pre>
     * == Minute 1 ==
     * 1 ore-collecting robot collects 1 ore; you now have 1 ore.
     *
     * == Minute 2 ==
     * 1 ore-collecting robot collects 1 ore; you now have 2 ore.
     *
     * == Minute 3 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 1 ore-collecting robot collects 1 ore; you now have 1 ore.
     * The new clay-collecting robot is ready; you now have 1 of them.
     *
     * == Minute 4 ==
     * 1 ore-collecting robot collects 1 ore; you now have 2 ore.
     * 1 clay-collecting robot collects 1 clay; you now have 1 clay.
     *
     * == Minute 5 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 1 ore-collecting robot collects 1 ore; you now have 1 ore.
     * 1 clay-collecting robot collects 1 clay; you now have 2 clay.
     * The new clay-collecting robot is ready; you now have 2 of them.
     *
     * == Minute 6 ==
     * 1 ore-collecting robot collects 1 ore; you now have 2 ore.
     * 2 clay-collecting robots collect 2 clay; you now have 4 clay.
     *
     * == Minute 7 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 1 ore-collecting robot collects 1 ore; you now have 1 ore.
     * 2 clay-collecting robots collect 2 clay; you now have 6 clay.
     * The new clay-collecting robot is ready; you now have 3 of them.
     *
     * == Minute 8 ==
     * 1 ore-collecting robot collects 1 ore; you now have 2 ore.
     * 3 clay-collecting robots collect 3 clay; you now have 9 clay.
     *
     * == Minute 9 ==
     * 1 ore-collecting robot collects 1 ore; you now have 3 ore.
     * 3 clay-collecting robots collect 3 clay; you now have 12 clay.
     *
     * == Minute 10 ==
     * 1 ore-collecting robot collects 1 ore; you now have 4 ore.
     * 3 clay-collecting robots collect 3 clay; you now have 15 clay.
     *
     * == Minute 11 ==
     * Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
     * 1 ore-collecting robot collects 1 ore; you now have 2 ore.
     * 3 clay-collecting robots collect 3 clay; you now have 4 clay.
     * The new obsidian-collecting robot is ready; you now have 1 of them.
     *
     * == Minute 12 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 1 ore-collecting robot collects 1 ore; you now have 1 ore.
     * 3 clay-collecting robots collect 3 clay; you now have 7 clay.
     * 1 obsidian-collecting robot collects 1 obsidian; you now have 1 obsidian.
     * The new clay-collecting robot is ready; you now have 4 of them.
     *
     * == Minute 13 ==
     * 1 ore-collecting robot collects 1 ore; you now have 2 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 11 clay.
     * 1 obsidian-collecting robot collects 1 obsidian; you now have 2 obsidian.
     *
     * == Minute 14 ==
     * 1 ore-collecting robot collects 1 ore; you now have 3 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 15 clay.
     * 1 obsidian-collecting robot collects 1 obsidian; you now have 3 obsidian.
     *
     * == Minute 15 ==
     * Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
     * 1 ore-collecting robot collects 1 ore; you now have 1 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 5 clay.
     * 1 obsidian-collecting robot collects 1 obsidian; you now have 4 obsidian.
     * The new obsidian-collecting robot is ready; you now have 2 of them.
     *
     * == Minute 16 ==
     * 1 ore-collecting robot collects 1 ore; you now have 2 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 9 clay.
     * 2 obsidian-collecting robots collect 2 obsidian; you now have 6 obsidian.
     *
     * == Minute 17 ==
     * 1 ore-collecting robot collects 1 ore; you now have 3 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 13 clay.
     * 2 obsidian-collecting robots collect 2 obsidian; you now have 8 obsidian.
     *
     * == Minute 18 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 1 ore-collecting robot collects 1 ore; you now have 2 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 17 clay.
     * 2 obsidian-collecting robots collect 2 obsidian; you now have 3 obsidian.
     * The new geode-cracking robot is ready; you now have 1 of them.
     *
     * == Minute 19 ==
     * 1 ore-collecting robot collects 1 ore; you now have 3 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 21 clay.
     * 2 obsidian-collecting robots collect 2 obsidian; you now have 5 obsidian.
     * 1 geode-cracking robot cracks 1 geode; you now have 1 open geode.
     *
     * == Minute 20 ==
     * 1 ore-collecting robot collects 1 ore; you now have 4 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 25 clay.
     * 2 obsidian-collecting robots collect 2 obsidian; you now have 7 obsidian.
     * 1 geode-cracking robot cracks 1 geode; you now have 2 open geodes.
     *
     * == Minute 21 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 1 ore-collecting robot collects 1 ore; you now have 3 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 29 clay.
     * 2 obsidian-collecting robots collect 2 obsidian; you now have 2 obsidian.
     * 1 geode-cracking robot cracks 1 geode; you now have 3 open geodes.
     * The new geode-cracking robot is ready; you now have 2 of them.
     *
     * == Minute 22 ==
     * 1 ore-collecting robot collects 1 ore; you now have 4 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 33 clay.
     * 2 obsidian-collecting robots collect 2 obsidian; you now have 4 obsidian.
     * 2 geode-cracking robots crack 2 geodes; you now have 5 open geodes.
     *
     * == Minute 23 ==
     * 1 ore-collecting robot collects 1 ore; you now have 5 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 37 clay.
     * 2 obsidian-collecting robots collect 2 obsidian; you now have 6 obsidian.
     * 2 geode-cracking robots crack 2 geodes; you now have 7 open geodes.
     *
     * == Minute 24 ==
     * 1 ore-collecting robot collects 1 ore; you now have 6 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 41 clay.
     * 2 obsidian-collecting robots collect 2 obsidian; you now have 8 obsidian.
     * 2 geode-cracking robots crack 2 geodes; you now have 9 open geodes.
     * </pre>
     * <p>
     * However, by using blueprint 2 in the example above, you could do even better: the largest number of geodes you could open in 24 minutes is <code><em>12</em></code>.
     * <p>
     * Determine the <em>quality level</em> of each blueprint by <em>multiplying that blueprint's ID number</em> with the largest number of geodes that can be opened in 24 minutes using that blueprint.
     * In this example, the first blueprint has ID 1 and can open 9 geodes, so its quality level is <code><em>9</em></code>.
     * The second blueprint has ID 2 and can open 12 geodes, so its quality level is <code><em>24</em></code>.
     * Finally, if you <em>add up the quality levels</em> of all of the blueprints in the list, you get <code><em>33</em></code>.
     * <p>
     * Determine the quality level of each blueprint using the largest number of geodes it could produce in 24 minutes.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you add up the quality level of all of the blueprints in your list?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Blueprints blueprints = Blueprints.parse(context);
        return blueprints.calculateQuantityLevelScore();
    }

    /**
     * While you were choosing the best blueprint, the elephants found some food on their own, so you're not in as much of a hurry; you figure you probably have <em>32 minutes</em> before the wind changes direction again and you'll need to get out of range of the erupting volcano.
     * <p>
     * Unfortunately, one of the elephants <em>ate most of your blueprint list</em>!
     * Now, only the first three blueprints in your list are intact.
     * <p>
     * In 32 minutes, the largest number of geodes blueprint 1 (from the example above) can open is <code><em>56</em></code>.
     * One way to achieve that is:
     * <pre>
     * == Minute 1 ==
     * 1 ore-collecting robot collects 1 ore; you now have 1 ore.
     *
     * == Minute 2 ==
     * 1 ore-collecting robot collects 1 ore; you now have 2 ore.
     *
     * == Minute 3 ==
     * 1 ore-collecting robot collects 1 ore; you now have 3 ore.
     *
     * == Minute 4 ==
     * 1 ore-collecting robot collects 1 ore; you now have 4 ore.
     *
     * == Minute 5 ==
     * Spend 4 ore to start building an ore-collecting robot.
     * 1 ore-collecting robot collects 1 ore; you now have 1 ore.
     * The new ore-collecting robot is ready; you now have 2 of them.
     *
     * == Minute 6 ==
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     *
     * == Minute 7 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     * The new clay-collecting robot is ready; you now have 1 of them.
     *
     * == Minute 8 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     * 1 clay-collecting robot collects 1 clay; you now have 1 clay.
     * The new clay-collecting robot is ready; you now have 2 of them.
     *
     * == Minute 9 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     * 2 clay-collecting robots collect 2 clay; you now have 3 clay.
     * The new clay-collecting robot is ready; you now have 3 of them.
     *
     * == Minute 10 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     * 3 clay-collecting robots collect 3 clay; you now have 6 clay.
     * The new clay-collecting robot is ready; you now have 4 of them.
     *
     * == Minute 11 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     * 4 clay-collecting robots collect 4 clay; you now have 10 clay.
     * The new clay-collecting robot is ready; you now have 5 of them.
     *
     * == Minute 12 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     * 5 clay-collecting robots collect 5 clay; you now have 15 clay.
     * The new clay-collecting robot is ready; you now have 6 of them.
     *
     * == Minute 13 ==
     * Spend 2 ore to start building a clay-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     * 6 clay-collecting robots collect 6 clay; you now have 21 clay.
     * The new clay-collecting robot is ready; you now have 7 of them.
     *
     * == Minute 14 ==
     * Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 2 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 14 clay.
     * The new obsidian-collecting robot is ready; you now have 1 of them.
     *
     * == Minute 15 ==
     * 2 ore-collecting robots collect 2 ore; you now have 4 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 21 clay.
     * 1 obsidian-collecting robot collects 1 obsidian; you now have 1 obsidian.
     *
     * == Minute 16 ==
     * Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 14 clay.
     * 1 obsidian-collecting robot collects 1 obsidian; you now have 2 obsidian.
     * The new obsidian-collecting robot is ready; you now have 2 of them.
     *
     * == Minute 17 ==
     * Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 2 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 7 clay.
     * 2 obsidian-collecting robots collect 2 obsidian; you now have 4 obsidian.
     * The new obsidian-collecting robot is ready; you now have 3 of them.
     *
     * == Minute 18 ==
     * 2 ore-collecting robots collect 2 ore; you now have 4 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 14 clay.
     * 3 obsidian-collecting robots collect 3 obsidian; you now have 7 obsidian.
     *
     * == Minute 19 ==
     * Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 7 clay.
     * 3 obsidian-collecting robots collect 3 obsidian; you now have 10 obsidian.
     * The new obsidian-collecting robot is ready; you now have 4 of them.
     *
     * == Minute 20 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 2 ore-collecting robots collect 2 ore; you now have 3 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 14 clay.
     * 4 obsidian-collecting robots collect 4 obsidian; you now have 7 obsidian.
     * The new geode-cracking robot is ready; you now have 1 of them.
     *
     * == Minute 21 ==
     * Spend 3 ore and 14 clay to start building an obsidian-collecting robot.
     * 2 ore-collecting robots collect 2 ore; you now have 2 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 7 clay.
     * 4 obsidian-collecting robots collect 4 obsidian; you now have 11 obsidian.
     * 1 geode-cracking robot cracks 1 geode; you now have 1 open geode.
     * The new obsidian-collecting robot is ready; you now have 5 of them.
     *
     * == Minute 22 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 2 ore-collecting robots collect 2 ore; you now have 2 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 14 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 9 obsidian.
     * 1 geode-cracking robot cracks 1 geode; you now have 2 open geodes.
     * The new geode-cracking robot is ready; you now have 2 of them.
     *
     * == Minute 23 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 2 ore-collecting robots collect 2 ore; you now have 2 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 21 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 7 obsidian.
     * 2 geode-cracking robots crack 2 geodes; you now have 4 open geodes.
     * The new geode-cracking robot is ready; you now have 3 of them.
     *
     * == Minute 24 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 2 ore-collecting robots collect 2 ore; you now have 2 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 28 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 5 obsidian.
     * 3 geode-cracking robots crack 3 geodes; you now have 7 open geodes.
     * The new geode-cracking robot is ready; you now have 4 of them.
     *
     * == Minute 25 ==
     * 2 ore-collecting robots collect 2 ore; you now have 4 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 35 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 10 obsidian.
     * 4 geode-cracking robots crack 4 geodes; you now have 11 open geodes.
     *
     * == Minute 26 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 2 ore-collecting robots collect 2 ore; you now have 4 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 42 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 8 obsidian.
     * 4 geode-cracking robots crack 4 geodes; you now have 15 open geodes.
     * The new geode-cracking robot is ready; you now have 5 of them.
     *
     * == Minute 27 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 2 ore-collecting robots collect 2 ore; you now have 4 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 49 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 6 obsidian.
     * 5 geode-cracking robots crack 5 geodes; you now have 20 open geodes.
     * The new geode-cracking robot is ready; you now have 6 of them.
     *
     * == Minute 28 ==
     * 2 ore-collecting robots collect 2 ore; you now have 6 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 56 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 11 obsidian.
     * 6 geode-cracking robots crack 6 geodes; you now have 26 open geodes.
     *
     * == Minute 29 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 2 ore-collecting robots collect 2 ore; you now have 6 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 63 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 9 obsidian.
     * 6 geode-cracking robots crack 6 geodes; you now have 32 open geodes.
     * The new geode-cracking robot is ready; you now have 7 of them.
     *
     * == Minute 30 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 2 ore-collecting robots collect 2 ore; you now have 6 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 70 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 7 obsidian.
     * 7 geode-cracking robots crack 7 geodes; you now have 39 open geodes.
     * The new geode-cracking robot is ready; you now have 8 of them.
     *
     * == Minute 31 ==
     * Spend 2 ore and 7 obsidian to start building a geode-cracking robot.
     * 2 ore-collecting robots collect 2 ore; you now have 6 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 77 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 5 obsidian.
     * 8 geode-cracking robots crack 8 geodes; you now have 47 open geodes.
     * The new geode-cracking robot is ready; you now have 9 of them.
     *
     * == Minute 32 ==
     * 2 ore-collecting robots collect 2 ore; you now have 8 ore.
     * 7 clay-collecting robots collect 7 clay; you now have 84 clay.
     * 5 obsidian-collecting robots collect 5 obsidian; you now have 10 obsidian.
     * 9 geode-cracking robots crack 9 geodes; you now have 56 open geodes.
     * </pre>
     * <p>
     * However, blueprint 2 from the example above is still better; using it, the largest number of geodes you could open in 32 minutes is <code><em>62</em></code>.
     * <p>
     * You <em>no longer have enough blueprints to worry about quality levels</em>.
     * Instead, for each of the first three blueprints, determine the largest number of geodes you could open; then, multiply these three values together.
     * <p>
     * Don't worry about quality levels; instead, just determine the largest number of geodes you could open using each of the first three blueprints.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you multiply these numbers together?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Blueprints blueprints = Blueprints.parse(context);
        return blueprints.calculateMaxGeodeHaul(3);
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * The state of a simulation.
     *
     * Contains the current minute, and a packed store of currently owned
     * `@Robots` and `@Resources`.
     */
    private record State(int minute, @Robots int robots, @Resources int resources) {
    }

    /*
     * A template on how to build a robot.
     */
    private record RobotSpec(int id, String name, int ore, int clay, int obsidian, int geode) {

        // Static Helper Methods

        /**
         * Parse a {@link RobotSpec} from the given line.
         *
         * @param line the line of text to parse
         * @return the {@link RobotSpec}
         */
        public static RobotSpec parse(final String line) {
            final String[] words = line.split(" ");

            final String name = words[1];
            final Map<String, Integer> costs = new HashMap<>();
            for (int i = 4; i < words.length; i += 3)
                costs.put(words[i + 1], Integer.parseInt(words[i]));

            return new RobotSpec(
                    Blueprint.type2idx(name),
                    name,
                    costs.getOrDefault("ore", 0),
                    costs.getOrDefault("clay", 0),
                    costs.getOrDefault("obsidian", 0),
                    costs.getOrDefault("geode", 0)
            );
        }
    }

    /*
     * A blueprint for creating robots.
     */
    static class Blueprint {

        private static final int GEODE = 3;
        private static final int OBSIDIAN = 2;
        private static final int CLAY = 1;
        private static final int ORE = 0;

        private static final int ONE_GEODE = resource(GEODE);
        private static final int ONE_OBSIDIAN = resource(OBSIDIAN);
        private static final int ONE_CLAY = resource(CLAY);
        private static final int ONE_ORE = resource(ORE);

        private static final String[] MINERALS = {"ore", "clay", "obsidian", "geode"};

        // Private Members

        private final int id;
        private final Map<String, RobotSpec> specs;
        private final int maxOre;
        private final int maxClay;
        private final int maxObsidian;

        // Constructors

        Blueprint(final int id, final Map<String, RobotSpec> specs) {
            this.id = id;
            this.specs = specs;
            this.maxOre = specs.values().stream().mapToInt(spec -> spec.ore).max().orElse(0);
            this.maxClay = specs.values().stream().mapToInt(spec -> spec.clay).max().orElse(0);
            this.maxObsidian = specs.values().stream().mapToInt(spec -> spec.obsidian).max().orElse(0);
        }

        // Helper Methods

        /**
         * Calculate the optimum yield of geodes with a given time limit.
         *
         * @param timeLimit the time limit
         * @return the maximum number of geodes that can be farmed
         */
        long calculateGeodeReturn(final int timeLimit) {
            return makeDifficultDecisions(new State(0, 1, 0), 0L, new HashMap<>(), timeLimit);
        }

        // Private Helper Methods

        /*
         * Make decisions on what kind of robot to build next
         */
        private long makeDifficultDecisions(final State state, final long bestSoFar, final Map<State, Long> cache, final int timeLimit) {
            if (state.minute == timeLimit)
                return resource(GEODE, state.resources);
            if (cache.containsKey(state))
                return cache.get(state);

            @Resource final short geodes = resource(GEODE, state.resources);
            @Robot final short geobots = robot(GEODE, state.robots);
            final long timeLeft = timeLimit - state.minute;
            final long optimisticScore = geodes + (geobots * timeLeft) + (timeLeft * (timeLeft - 1) / 2);
            if (optimisticScore <= bestSoFar)
                return Long.MIN_VALUE; // Give up early?

            long best = bestSoFar;

            final RobotSpec geodeSpec = this.specs.get(MINERALS[GEODE]);
            if (canMakeRobot(geodeSpec, state.resources))
                best = max(best, simulate(state, geodeSpec, best, cache, timeLimit));

            if (robot(OBSIDIAN, state.robots) < maxObsidian) {
                final RobotSpec spec = this.specs.get(MINERALS[OBSIDIAN]);
                if (canMakeRobot(spec, state.resources))
                    best = max(best, simulate(state, spec, best, cache, timeLimit));
            }

            if (robot(CLAY, state.robots) < maxClay && robot(GEODE, state.robots) == 0) {
                final RobotSpec spec = this.specs.get(MINERALS[CLAY]);
                if (canMakeRobot(spec, state.resources))
                    best = max(best, simulate(state, spec, best, cache, timeLimit));
            }

            if (robot(ORE, state.robots) < maxOre && robot(OBSIDIAN, state.robots) == 0) {
                final RobotSpec spec = this.specs.get(MINERALS[ORE]);
                if (canMakeRobot(spec, state.resources))
                    best = max(best, simulate(state, spec, best, cache, timeLimit));
            }

            best = max(best, simulate(state, bestSoFar, cache, timeLimit));

            cache.put(state, best);
            return best;
        }

        /*
         * Simulate what would happen if a robot isn't built this minute.
         */
        private long simulate(final State state, final long bestSoFar, final Map<State, Long> cache, final int timeLimit) {
            @Robots final int nextRobots = state.robots;
            @Resources int nextResources = state.resources;

            // Farm the new materials
            nextResources +=
                    (ONE_GEODE * robot(GEODE, state.robots))
                            + (ONE_OBSIDIAN * robot(OBSIDIAN, state.robots))
                            + (ONE_CLAY * robot(CLAY, state.robots))
                            + (ONE_ORE * robot(ORE, state.robots));

            return makeDifficultDecisions(new State(state.minute + 1, nextRobots, nextResources), bestSoFar, cache, timeLimit);
        }

        /*
         * Simulate what would happen if the specified `RobotSpec` was built.
         */
        private long simulate(final State state, final RobotSpec spec, final long bestSoFar, final Map<State, Long> cache, final int timeLimit) {
            @Robots int nextRobots = state.robots;
            @Resources int nextResources = state.resources;

            // Input a new Robot into the factory
            nextResources -=
                    (ONE_GEODE * spec.geode)
                            + (ONE_OBSIDIAN * spec.obsidian)
                            + (ONE_CLAY * spec.clay)
                            + (ONE_ORE * spec.ore);

            // Farm the new materials
            nextResources +=
                    (ONE_GEODE * robot(GEODE, state.robots))
                            + (ONE_OBSIDIAN * robot(OBSIDIAN, state.robots))
                            + (ONE_CLAY * robot(CLAY, state.robots))
                            + (ONE_ORE * robot(ORE, state.robots));

            // Receive a new Robot from the factory
            nextRobots += robot(spec.id);

            return makeDifficultDecisions(new State(state.minute + 1, nextRobots, nextResources), bestSoFar, cache, timeLimit);
        }

        // Static Helper Methods

        /**
         * Parse a {@link Blueprint} from the given line of text.
         *
         * @param line the line of text to parse
         * @return the {@link Blueprint}
         */
        public static Blueprint parse(final String line) {
            final String[] parts = line.split(": ");
            final int id = Integer.parseInt(parts[0].substring(10));
            final Map<String, RobotSpec> specs = stream(parts[1].split("[.] *"))
                    .map(RobotSpec::parse)
                    .collect(Collectors.toMap(spec -> spec.name, spec -> spec));
            return new Blueprint(id, specs);
        }

        // Private Static Helper Methods

        /*
         * Check if there are enough resources available to build a robot.
         */
        private static boolean canMakeRobot(final RobotSpec spec, @Resources final int resources) {
            return (resource(GEODE, resources) >= spec.geode)
                    && (resource(OBSIDIAN, resources) >= spec.obsidian)
                    && (resource(CLAY, resources) >= spec.clay)
                    && (resource(ORE, resources) >= spec.ore);
        }

        /*
         * Convert the text-based resource/robot name to an ID.
         */
        private static int type2idx(final String type) {
            return switch (type) {
                case "geode" -> GEODE;
                case "obsidian" -> OBSIDIAN;
                case "clay" -> CLAY;
                case "ore" -> ORE;
                default -> throw new IllegalArgumentException("Invalid resource type: " + type);
            };
        }

        /*
         * Represent a single robot of the given type.
         */
        private static @Robots int robot(final int id) {
            return (0x0001 << (8 * id));
        }

        /*
         * Extract the count of robots of the given type from the compressed
         * storage of robots.
         */
        private static @Robot short robot(final int id, @Robots final int robots) {
            final int r = 0x00FF & (robots >> (8 * id));
            return (short) r;
        }

        /*
         * Represent a single resource of the given type.
         */
        private static @Resources int resource(final int id) {
            return (0x0001 << (8 * id));
        }

        /*
         * Extract the count of resources of the given type from the compressed
         * storage of resources.
         */
        private static @Resource short resource(final int id, @Resources final int resources) {
            final int r = 0x00FF & (resources >> (8 * id));
            return (short) r;
        }

    }

    /*
     * A storage of multiple `Blueprint`s.
     */
    private record Blueprints(List<Blueprint> blueprints) {

        // Helper Methods

        /**
         * Calculate the total quality score of all the stored
         * {@link Blueprint Blueprints}.
         * <p>
         * The quality score is the {@link Blueprint#id ID} multiplied by the
         * {@link Blueprint#calculateGeodeReturn(int) maximum number of geodes}
         * that can be farmed in 24 minutes.
         *
         * @return the sum of all quality scores
         */
        long calculateQuantityLevelScore() {
            return blueprints.stream()
                    .mapToLong(blueprint -> blueprint.id * blueprint.calculateGeodeReturn(24))
                    .sum();
        }

        /**
         * Calculate the total geode haul of the first {@literal n} stored
         * {@link Blueprint Blueprints} and return the product of these.
         *
         * @return the product of the haul of {@literal n} {@link Blueprint blueprints}
         */
        long calculateMaxGeodeHaul(final int limit) {
            return blueprints.subList(0, min(limit, blueprints.size())).stream()
                    .mapToLong(blueprint -> blueprint.calculateGeodeReturn(32))
                    .reduce(1, (a, b) -> a * b);
        }

        // Static Helper Methods

        /**
         * Parse {@link Blueprints} from the given {@link SolutionContext}.
         *
         * @param context the {@link SolutionContext} to load from
         * @return the {@link Blueprints}
         */
        public static Blueprints parse(final SolutionContext context) {
            return new Blueprints(context.process(Blueprint::parse));
        }

    }

    /*
     * Checker annotation (maybe not enforced?) to indicate a packed count
     * of multiple `@Robot`s.
     */
    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE})
    private @interface Robots {

    }

    /*
     * Checker annotation (maybe not enforced?) to indicate an unpacked count
     * of a single type of robot.
     */
    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE})
    private @interface Robot {

    }

    /*
     * Checker annotation (maybe not enforced?) to indicate a packed count
     * of multiple `@Resource`s.
     */
    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE})
    private @interface Resources {

    }

    /*
     * Checker annotation (maybe not enforced?) to indicate an unpacked count
     * of a single type of resources.
     */
    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE})
    private @interface Resource {

    }

}

