package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.Math.max;
import static java.util.Arrays.asList;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 16: Proboscidea Volcanium.
 */
@Solution(year = 2022, day = 16, title = "Proboscidea Volcanium")
public class Day16 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The sensors have led you to the origin of the distress signal: yet another handheld device, just like the one the Elves gave you.
     * However, you don't see any Elves around; instead, the device is surrounded by elephants!
     * They must have gotten lost in these tunnels, and one of the elephants apparently figured out how to turn on the distress signal.
     * <p>
     * The ground rumbles again, much stronger this time.
     * What kind of cave is this, exactly? You scan the cave with your handheld device; it reports mostly igneous rock, some ash, pockets of pressurized gas, magma... this isn't just a cave, it's a volcano!
     * <p>
     * You need to get the elephants out of here, quickly.
     * Your device estimates that you have <em>30 minutes</em> before the volcano erupts, so you don't have time to go back out the way you came in.
     * <p>
     * You scan the cave for other options and discover a network of pipes and pressure-release <em>valves</em>.
     * You aren't sure how such a system got into a volcano, but you don't have time to complain; your device produces a report (your puzzle input) of each valve's <em>flow rate</em> if it were opened (in pressure per minute) and the tunnels you could use to move between the valves.
     * <p>
     * There's even a valve in the room you and the elephants are currently standing in labeled <code>AA</code>.
     * You estimate it will take you one minute to open a single valve and one minute to follow any tunnel from one valve to another.
     * What is the most pressure you could release?
     * <p>
     * For example, suppose you had the following scan output:
     * <pre>
     * Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
     * Valve BB has flow rate=13; tunnels lead to valves CC, AA
     * Valve CC has flow rate=2; tunnels lead to valves DD, BB
     * Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
     * Valve EE has flow rate=3; tunnels lead to valves FF, DD
     * Valve FF has flow rate=0; tunnels lead to valves EE, GG
     * Valve GG has flow rate=0; tunnels lead to valves FF, HH
     * Valve HH has flow rate=22; tunnel leads to valve GG
     * Valve II has flow rate=0; tunnels lead to valves AA, JJ
     * Valve JJ has flow rate=21; tunnel leads to valve II
     * </pre>
     * <p>
     * All of the valves begin <em>closed</em>.
     * You start at valve <code>AA</code>, but it must be damaged or <span title="Wait, sir! The valve, sir! it appears to be... jammed!">jammed</span> or something: its flow rate is <code>0</code>, so there's no point in opening it.
     * However, you could spend one minute moving to valve <code>BB</code> and another minute opening it; doing so would release pressure during the remaining <em>28 minutes</em> at a flow rate of <code>13</code>, a total eventual pressure release of <code>28 * 13 = <em>364</em></code>.
     * Then, you could spend your third minute moving to valve <code>CC</code> and your fourth minute opening it, providing an additional <em>26 minutes</em> of eventual pressure release at a flow rate of <code>2</code>, or <code><em>52</em></code> total pressure released by valve <code>CC</code>.
     * <p>
     * Making your way through the tunnels like this, you could probably open many or all of the valves by the time 30 minutes have elapsed.
     * However, you need to release as much pressure as possible, so you'll need to be methodical.
     * Instead, consider this approach:
     * <pre>
     * == Minute 1 ==
     * No valves are open.
     * You move to valve DD.
     *
     * == Minute 2 ==
     * No valves are open.
     * You open valve DD.
     *
     * == Minute 3 ==
     * Valve DD is open, releasing <em>20</em> pressure.
     * You move to valve CC.
     *
     * == Minute 4 ==
     * Valve DD is open, releasing <em>20</em> pressure.
     * You move to valve BB.
     *
     * == Minute 5 ==
     * Valve DD is open, releasing <em>20</em> pressure.
     * You open valve BB.
     *
     * == Minute 6 ==
     * Valves BB and DD are open, releasing <em>33</em> pressure.
     * You move to valve AA.
     *
     * == Minute 7 ==
     * Valves BB and DD are open, releasing <em>33</em> pressure.
     * You move to valve II.
     *
     * == Minute 8 ==
     * Valves BB and DD are open, releasing <em>33</em> pressure.
     * You move to valve JJ.
     *
     * == Minute 9 ==
     * Valves BB and DD are open, releasing <em>33</em> pressure.
     * You open valve JJ.
     *
     * == Minute 10 ==
     * Valves BB, DD, and JJ are open, releasing <em>54</em> pressure.
     * You move to valve II.
     *
     * == Minute 11 ==
     * Valves BB, DD, and JJ are open, releasing <em>54</em> pressure.
     * You move to valve AA.
     *
     * == Minute 12 ==
     * Valves BB, DD, and JJ are open, releasing <em>54</em> pressure.
     * You move to valve DD.
     *
     * == Minute 13 ==
     * Valves BB, DD, and JJ are open, releasing <em>54</em> pressure.
     * You move to valve EE.
     *
     * == Minute 14 ==
     * Valves BB, DD, and JJ are open, releasing <em>54</em> pressure.
     * You move to valve FF.
     *
     * == Minute 15 ==
     * Valves BB, DD, and JJ are open, releasing <em>54</em> pressure.
     * You move to valve GG.
     *
     * == Minute 16 ==
     * Valves BB, DD, and JJ are open, releasing <em>54</em> pressure.
     * You move to valve HH.
     *
     * == Minute 17 ==
     * Valves BB, DD, and JJ are open, releasing <em>54</em> pressure.
     * You open valve HH.
     *
     * == Minute 18 ==
     * Valves BB, DD, HH, and JJ are open, releasing <em>76</em> pressure.
     * You move to valve GG.
     *
     * == Minute 19 ==
     * Valves BB, DD, HH, and JJ are open, releasing <em>76</em> pressure.
     * You move to valve FF.
     *
     * == Minute 20 ==
     * Valves BB, DD, HH, and JJ are open, releasing <em>76</em> pressure.
     * You move to valve EE.
     *
     * == Minute 21 ==
     * Valves BB, DD, HH, and JJ are open, releasing <em>76</em> pressure.
     * You open valve EE.
     *
     * == Minute 22 ==
     * Valves BB, DD, EE, HH, and JJ are open, releasing <em>79</em> pressure.
     * You move to valve DD.
     *
     * == Minute 23 ==
     * Valves BB, DD, EE, HH, and JJ are open, releasing <em>79</em> pressure.
     * You move to valve CC.
     *
     * == Minute 24 ==
     * Valves BB, DD, EE, HH, and JJ are open, releasing <em>79</em> pressure.
     * You open valve CC.
     *
     * == Minute 25 ==
     * Valves BB, CC, DD, EE, HH, and JJ are open, releasing <em>81</em> pressure.
     *
     * == Minute 26 ==
     * Valves BB, CC, DD, EE, HH, and JJ are open, releasing <em>81</em> pressure.
     *
     * == Minute 27 ==
     * Valves BB, CC, DD, EE, HH, and JJ are open, releasing <em>81</em> pressure.
     *
     * == Minute 28 ==
     * Valves BB, CC, DD, EE, HH, and JJ are open, releasing <em>81</em> pressure.
     *
     * == Minute 29 ==
     * Valves BB, CC, DD, EE, HH, and JJ are open, releasing <em>81</em> pressure.
     *
     * == Minute 30 ==
     * Valves BB, CC, DD, EE, HH, and JJ are open, releasing <em>81</em> pressure.
     * </pre>
     * <p>
     * This approach lets you release the most pressure possible in 30 minutes with this valve layout, <code><em>1651</em></code>.
     * <p>
     * Work out the steps to release the most pressure in 30 minutes.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the most pressure you can release?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Network network = Network.parse(context);

        final Map<Long, Long> result = network.solve(30);
        return result.values().stream()
                .mapToLong(l -> l)
                .max()
                .orElseThrow();
    }

    /**
     * You're worried that even with an optimal approach, the pressure released won't be enough.
     * What if you got one of the elephants to help you?
     * <p>
     * It would take you 4 minutes to teach an elephant how to open the right valves in the right order, leaving you with only <em>26 minutes</em> to actually execute your plan.
     * Would having two of you working together be better, even if it means having less time?
     * (Assume that you teach the elephant before opening any valves yourself, giving you both the same full 26 minutes.)
     * <p>
     * In the example above, you could teach the elephant to help you as follows:
     * <pre>
     * == Minute 1 ==
     * No valves are open.
     * You move to valve II.
     * The elephant moves to valve DD.
     *
     * == Minute 2 ==
     * No valves are open.
     * You move to valve JJ.
     * The elephant opens valve DD.
     *
     * == Minute 3 ==
     * Valve DD is open, releasing <em>20</em> pressure.
     * You open valve JJ.
     * The elephant moves to valve EE.
     *
     * == Minute 4 ==
     * Valves DD and JJ are open, releasing <em>41</em> pressure.
     * You move to valve II.
     * The elephant moves to valve FF.
     *
     * == Minute 5 ==
     * Valves DD and JJ are open, releasing <em>41</em> pressure.
     * You move to valve AA.
     * The elephant moves to valve GG.
     *
     * == Minute 6 ==
     * Valves DD and JJ are open, releasing <em>41</em> pressure.
     * You move to valve BB.
     * The elephant moves to valve HH.
     *
     * == Minute 7 ==
     * Valves DD and JJ are open, releasing <em>41</em> pressure.
     * You open valve BB.
     * The elephant opens valve HH.
     *
     * == Minute 8 ==
     * Valves BB, DD, HH, and JJ are open, releasing <em>76</em> pressure.
     * You move to valve CC.
     * The elephant moves to valve GG.
     *
     * == Minute 9 ==
     * Valves BB, DD, HH, and JJ are open, releasing <em>76</em> pressure.
     * You open valve CC.
     * The elephant moves to valve FF.
     *
     * == Minute 10 ==
     * Valves BB, CC, DD, HH, and JJ are open, releasing <em>78</em> pressure.
     * The elephant moves to valve EE.
     *
     * == Minute 11 ==
     * Valves BB, CC, DD, HH, and JJ are open, releasing <em>78</em> pressure.
     * The elephant opens valve EE.
     *
     * (At this point, all valves are open.)
     *
     * == Minute 12 ==
     * Valves BB, CC, DD, EE, HH, and JJ are open, releasing <em>81</em> pressure.
     *
     * ...
     *
     * == Minute 20 ==
     * Valves BB, CC, DD, EE, HH, and JJ are open, releasing <em>81</em> pressure.
     *
     * ...
     *
     * == Minute 26 ==
     * Valves BB, CC, DD, EE, HH, and JJ are open, releasing <em>81</em> pressure.
     * </pre>
     * <p>
     * With the elephant helping, after 26 minutes, the best you could do would release a total of <code><em>1707</em></code> pressure.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return With you and an elephant working together for 26 minutes, what is the most pressure you could release?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Network network = Network.parse(context);
        final Map<Long, Long> result = network.solve(26);
        return result.entrySet().stream()
                .flatMapToLong(entry1 -> result.entrySet().stream()
                        .filter(entry2 -> (entry1.getKey() & entry2.getKey()) == 0)
                        .mapToLong(entry2 -> entry1.getValue() + entry2.getValue()))
                .max()
                .orElseThrow();
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A room within the tunnel `Network` that contains a valve.
     */
    private record Room(long id, String name, int rate, Set<String> tunnels) {

        private static final Pattern INPUT_REGEX = Pattern.compile("Valve ([A-Z]+) has flow rate=([0-9]+); tunnels? leads? to valves? ((?:[A-Z]+, ?)*[A-Z]+)");

        // Constructors

        private Room(final long id, final String name, final int rate, final Set<String> tunnels) {
            this.id = id;
            this.name = name;
            this.rate = rate;
            this.tunnels = new TreeSet<>(tunnels);
        }

        // Helper Methods

        /**
         * Get the name of this {@link Room}.
         *
         * @return the name of this {@link Room}
         */
        @Override
        public String name() {
            return name;
        }

        /**
         * Check if this {@link Room} is actually worth visiting.
         * <p>
         * Although every {@link Room} contains a valve, some valves are jammed
         * and thus will not release any pressure if opened. This makes visiting
         * these {@link Room Rooms} pointless.
         *
         * @return {@literal true} if this {@link Room} is worth visiting; {@literal false} otherwise
         */
        boolean worthOpening() {
            return rate > 0;
        }

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final Room room = (Room) o;
            return id == room.id && rate == room.rate
                    && Objects.equals(name, room.name)
                    && Objects.equals(tunnels, room.tunnels);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, name, rate, tunnels);
        }


        // Static Helper Methods

        /*
         * Parse a `Room` specification from the given input line.
         */
        static Room parse(final int count, final String line) {
            final Matcher matcher = INPUT_REGEX.matcher(line);
            if (!matcher.matches())
                throw new IllegalArgumentException("Invalid room specification: '" + line + "'");

            assert count < 64 : "We're gonna need a bigger int...";
            return new Room(
                    1L << count,
                    matcher.group(1),
                    Integer.parseInt(matcher.group(2)),
                    new LinkedHashSet<>(asList(matcher.group(3).split(",\\s*")))
            );
        }
    }

    /*
     * A network of `Room`s, connected via tunnels, within a volcano.
     */
    private static final class Network {

        // Private Members

        private final Map<String, Room> rooms;
        private final Map<Long, Room> valves;

        private final Map<Room, Map<Room, Integer>> distanceCache = new HashMap<>();

        // Constructors

        Network(final Map<String, Room> rooms, final Map<Long, Room> valves) {
            this.rooms = new TreeMap<>(rooms);
            this.valves = valves;
        }

        // Helper Methods

        Map<Long, Long> solve(final int timeLimit) {
            final Map<Long, Long> results = new HashMap<>();
            findRoutes(findRoom("AA"), timeLimit, 0, 0, results);
            return results;
        }

        // Private Helper Methods

        /*
         * Find best routes, shamelessly stolen from
         * https://github.com/juanplopes/advent-of-code-2022/blob/main/day16.py ...
         */
        private void findRoutes(final Room room, final long timeLeft, final long pressure, final long visited, final Map<Long, Long> answer) {
            answer.compute(visited, (key, prev) -> prev != null ? max(prev, pressure) : pressure);
            for (final Room next : valves()) {
                final long wouldLeave = timeLeft - distanceTo(room, next) - 1;
                if ((visited & next.id) != 0 || wouldLeave <= 0)
                    continue;

                findRoutes(next, wouldLeave, pressure + wouldLeave * next.rate, visited | next.id, answer);
            }
        }

        /*
         * Find a `Room` with a given name.~
         */
        private Room findRoom(final String room) {
            return rooms.get(room);
        }

        /*
         * Return a `Set` of all `Room`s that contain a (useful) valve.
         */
        private Set<Room> valves() {
            return new HashSet<>(valves.values());
        }

        // Static Helper Methods

        /*
         * Create a new `Network` by parsing the given input.
         */
        static Network parse(final SolutionContext context) {
            final AtomicInteger counter = new AtomicInteger(0);

            final Map<String, Room> rooms = context.stream()
                    .map(line -> Room.parse(counter.incrementAndGet(), line))
                    .collect(Collectors.toMap(Room::name, Function.identity()));
            final Map<Long, Room> valves = rooms.values().stream()
                    .filter(Room::worthOpening)
                    .collect(Collectors.toMap(room -> room.id, room -> room));

            final Network network = new Network(rooms, valves);

            // Pre-cache all distances...
            valves.forEach((k1, from) -> valves.forEach((k2, to) -> network.distanceTo(from, to)));

            return network;

        }

        /*
         * Calculate the distance (in minutes) between two `Room`s.
         */
        private int distanceTo(final Room from, final Room to) {
            return distanceCache
                    .computeIfAbsent(from, k -> new HashMap<>())
                    .computeIfAbsent(to, k -> {
                        final Deque<Room> paths = new ArrayDeque<>();
                        paths.add(from);
                        for (int i = 0; i < rooms.size(); i++) {
                            if (paths.contains(to))
                                return i;
                            final Set<Room> next = new HashSet<>();
                            while (!paths.isEmpty()) {
                                for (final String tunnel : paths.removeFirst().tunnels)
                                    next.add(findRoom(tunnel));
                            }
                            paths.addAll(next);
                        }
                        throw new IllegalStateException("Failed to find valid path from " + from + " to " + to);
                    });
        }
    }

}

