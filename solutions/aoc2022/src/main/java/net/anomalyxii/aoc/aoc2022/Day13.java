package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 13: Distress Signal.
 */
@Solution(year = 2022, day = 13, title = "Distress Signal")
public class Day13 {

    private static final CompoundPacketData SEPARATOR_PACKET_1 = new CompoundPacketData(new CompoundPacketData(new SimplePacketData(2)));
    private static final CompoundPacketData SEPARATOR_PACKET_2 = new CompoundPacketData(new CompoundPacketData(new SimplePacketData(6)));

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You climb the hill and again try contacting the Elves.
     * However, you instead receive a signal you weren't expecting: a <em>distress signal</em>.
     * <p>
     * Your handheld device must still not be working properly; the packets from the distress signal got decoded <em>out of order</em>.
     * You'll need to re-order the list of received packets (your puzzle input) to decode the message.
     * <p>
     * Your list consists of pairs of packets; pairs are separated by a blank line.
     * You need to identify <em>how many pairs of packets are in the right order</em>.
     * <p>
     * For example:
     * <pre>
     * [1,1,3,1,1]
     * [1,1,5,1,1]
     *
     * [[1],[2,3,4]]
     * [[1],4]
     *
     * [9]
     * [[8,7,6]]
     *
     * [[4,4],4,4]
     * [[4,4],4,4,4]
     *
     * [7,7,7,7]
     * [7,7,7]
     *
     * []
     * [3]
     *
     * [[[]]]
     * [[]]
     *
     * [1,[2,[3,[4,[5,6,7]]]],8,9]
     * [1,[2,[3,[4,[5,6,0]]]],8,9]
     * </pre>
     * <p>
     * <span title="The snailfish called.
     * They want their distress signal back.">Packet data consists of lists and integers.</span> Each list starts with <code>[</code>, ends with <code>]</code>, and contains zero or more comma-separated values (either integers or other lists).
     * Each packet is always a list and appears on its own line.
     * <p>
     * When comparing two values, the first value is called <em>left</em> and the second value is called <em>right</em>.
     * Then:
     * <ul>
     * <li>
     * If <em>both values are integers</em>, the <em>lower integer</em> should come first.
     * If the left integer is lower than the right integer, the inputs are in the right order.
     * If the left integer is higher than the right integer, the inputs are not in the right order.
     * Otherwise, the inputs are the same integer; continue checking the next part of the input.
     * </li>
     * <li>
     * If <em>both values are lists</em>, compare the first value of each list, then the second value, and so on.
     * If the left list runs out of items first, the inputs are in the right order.
     * If the right list runs out of items first, the inputs are not in the right order.
     * If the lists are the same length and no comparison makes a decision about the order, continue checking the next part of the input.
     * </li>
     * <li>
     * If <em>exactly one value is an integer</em>, convert the integer to a list which contains that integer as its only value, then retry the comparison.
     * For example, if comparing <code>[0,0,0]</code> and <code>2</code>, convert the right value to <code>[2]</code> (a list containing <code>2</code>); the result is then found by instead comparing <code>[0,0,0]</code> and <code>[2]</code>.
     * </li>
     * </ul>
     * <p>
     * Using these rules, you can determine which of the pairs in the example are in the right order:
     * <pre>
     * == Pair 1 ==
     * - Compare [1,1,3,1,1] vs [1,1,5,1,1]
     *   - Compare 1 vs 1
     *   - Compare 1 vs 1
     *   - Compare 3 vs 5
     *     - Left side is smaller, so inputs are <em>in the right order</em>
     *
     * == Pair 2 ==
     * - Compare [[1],[2,3,4]] vs [[1],4]
     *   - Compare [1] vs [1]
     *     - Compare 1 vs 1
     *   - Compare [2,3,4] vs 4
     *     - Mixed types; convert right to [4] and retry comparison
     *     - Compare [2,3,4] vs [4]
     *       - Compare 2 vs 4
     *         - Left side is smaller, so inputs are <em>in the right order</em>
     *
     * == Pair 3 ==
     * - Compare [9] vs [[8,7,6]]
     *   - Compare 9 vs [8,7,6]
     *     - Mixed types; convert left to [9] and retry comparison
     *     - Compare [9] vs [8,7,6]
     *       - Compare 9 vs 8
     *         - Right side is smaller, so inputs are <em>not</em> in the right order
     *
     * == Pair 4 ==
     * - Compare [[4,4],4,4] vs [[4,4],4,4,4]
     *   - Compare [4,4] vs [4,4]
     *     - Compare 4 vs 4
     *     - Compare 4 vs 4
     *   - Compare 4 vs 4
     *   - Compare 4 vs 4
     *   - Left side ran out of items, so inputs are <em>in the right order</em>
     *
     * == Pair 5 ==
     * - Compare [7,7,7,7] vs [7,7,7]
     *   - Compare 7 vs 7
     *   - Compare 7 vs 7
     *   - Compare 7 vs 7
     *   - Right side ran out of items, so inputs are <em>not</em> in the right order
     *
     * == Pair 6 ==
     * - Compare [] vs [3]
     *   - Left side ran out of items, so inputs are <em>in the right order</em>
     *
     * == Pair 7 ==
     * - Compare [[[]]] vs [[]]
     *   - Compare [[]] vs []
     *     - Right side ran out of items, so inputs are <em>not</em> in the right order
     *
     * == Pair 8 ==
     * - Compare [1,[2,[3,[4,[5,6,7]]]],8,9] vs [1,[2,[3,[4,[5,6,0]]]],8,9]
     *   - Compare 1 vs 1
     *   - Compare [2,[3,[4,[5,6,7]]]] vs [2,[3,[4,[5,6,0]]]]
     *     - Compare 2 vs 2
     *     - Compare [3,[4,[5,6,7]]] vs [3,[4,[5,6,0]]]
     *       - Compare 3 vs 3
     *       - Compare [4,[5,6,7]] vs [4,[5,6,0]]
     *         - Compare 4 vs 4
     *         - Compare [5,6,7] vs [5,6,0]
     *           - Compare 5 vs 5
     *           - Compare 6 vs 6
     *           - Compare 7 vs 0
     *             - Right side is smaller, so inputs are <em>not</em> in the right order
     * </pre>
     * <p>
     * What are the indices of the pairs that are already <em>in the right order</em>? (The first pair has index 1, the second pair has index 2, and so on.)
     * In the above example, the pairs in the right order are 1, 2, 4, and 6; the sum of these indices is <code><em>13</em></code>.
     * <p>
     * Determine which pairs of packets are already in the right order.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the indices of those pairs?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final AtomicInteger index = new AtomicInteger(0);
        final LongAccumulator sum = new LongAccumulator(Long::sum, 0);
        context.streamBatches().forEach(batch -> {
            final int idx = index.incrementAndGet(); // We start from 1?

            final PacketData lhs = PacketData.parse(batch.getFirst());
            final PacketData rhs = PacketData.parse(batch.getLast());

            final int cmp = lhs.compareTo(rhs);
            if (cmp < 0)
                sum.accumulate(idx);
        });
        return sum.longValue();
    }

    /**
     * Now, you just need to put <em>all</em> of the packets in the right order.
     * Disregard the blank lines in your list of received packets.
     * <p>
     * The distress signal protocol also requires that you include two additional <em>divider packets</em>:
     * <pre>
     * [[2]]
     * [[6]]
     * </pre>
     * <p>
     * Using the same rules as before, organize all packets - the ones in your list of received packets as well as the two divider packets - into the correct order.
     * <p>
     * For the example above, the result of putting the packets in the correct order is:
     * <pre>
     * []
     * [[]]
     * [[[]]]
     * [1,1,3,1,1]
     * [1,1,5,1,1]
     * [[1],[2,3,4]]
     * [1,[2,[3,[4,[5,6,0]]]],8,9]
     * [1,[2,[3,[4,[5,6,7]]]],8,9]
     * [[1],4]
     * <em>[[2]]</em>
     * [3]
     * [[4,4],4,4]
     * [[4,4],4,4,4]
     * <em>[[6]]</em>
     * [7,7,7]
     * [7,7,7,7]
     * [[8,7,6]]
     * [9]
     * </pre>
     * <p>
     * Afterward, locate the divider packets.
     * To find the <em>decoder key</em> for this distress signal, you need to determine the indices of the two divider packets and multiply them together.
     * (The first packet is at index 1, the second packet is at index 2, and so on.)
     * In this example, the divider packets are <em>10th</em> and <em>14th</em>, and so the decoder key is <code><em>140</em></code>.
     * <p>
     * Organize all of the packets into the correct order.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the decoder key for the distress signal?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<PacketData> packets = new ArrayList<>();

        packets.add(SEPARATOR_PACKET_1);
        packets.add(SEPARATOR_PACKET_2);

        context.streamBatches().forEach(batch -> {
            packets.add(PacketData.parse(batch.getFirst()));
            packets.add(PacketData.parse(batch.getLast()));
        });

        packets.sort(Comparator.naturalOrder());

        final long packet1Position = packets.indexOf(SEPARATOR_PACKET_1) + 1;
        final long packet2Position = packets.indexOf(SEPARATOR_PACKET_2) + 1;
        return packet1Position * packet2Position;
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /*
     * Represents a single piece of data contained within a packet.
     */
    private sealed interface PacketData extends Comparable<PacketData> permits CompoundPacketData, SimplePacketData {

        // Interface Methods

        /**
         * Convert this {@link PacketData} into a {@link CompoundPacketData}.
         *
         * @return the {@link CompoundPacketData}
         */
        CompoundPacketData asCompoundPacketData();

        /**
         * Apply a {@link Function transformation} to this {@link PacketData}.
         *
         * @param onSimple   the {@link Function} to apply if this is {@link SimplePacketData}
         * @param onCompound the {@link Function} to apply if this is {@link CompoundPacketData}
         * @param <T>        the type of the resulting value
         * @return the resulting value
         */
        <T> T fold(Function<SimplePacketData, T> onSimple, Function<CompoundPacketData, T> onCompound);

        // Static Helper Methods

        /*
         * Parse the given line into some `PacketData`.
         */
        static PacketData parse(final String line) {

            final Deque<CompoundPacketData> data = new ArrayDeque<>();
            data.addLast(new CompoundPacketData()); // Root...

            for (int i = 0; i < line.length(); i++) {
                final char c = line.charAt(i);
                if (c == '[') {
                    final CompoundPacketData current = new CompoundPacketData();
                    final CompoundPacketData prev = data.getLast();

                    prev.add(current);
                    data.addLast(current);
                    continue;
                }

                if (c == ']') {
                    data.removeLast();
                    continue;
                }

                if (Character.isDigit(c)) {
                    int val = c - '0';
                    while (Character.isDigit(line.charAt(i + 1))) {
                        final int n = line.charAt(++i);
                        val = (val * 10) + (n - '0');
                    }

                    final CompoundPacketData prev = data.getLast();
                    prev.add(new SimplePacketData(val));
                    continue;
                }

                if (c == ',' || c == ' ')
                    continue;

                throw new IllegalStateException("Could not parse input: '" + line + "': invalid character [" + c + "] at position " + i);
            }

            final CompoundPacketData root = data.getFirst();
            return root.data.getFirst();
        }

    }

    /*
     * `PacketData` consisting of multiple pieces of sub-data.
     */
    private static final class CompoundPacketData implements PacketData {

        // Private Members

        private final List<PacketData> data;

        // Constructors

        private CompoundPacketData() {
            this.data = new ArrayList<>();
        }

        private CompoundPacketData(final PacketData... data) {
            this.data = asList(data);
        }

        private CompoundPacketData(final SimplePacketData data) {
            this.data = Collections.singletonList(data);
        }

        // Public Methods

        /**
         * Add some sub-data to this {@link CompoundPacketData}.
         *
         * @param data the {@link PacketData} to add
         */
        void add(final PacketData data) {
            this.data.add(data);
        }

        // PacketData Methods

        @Override
        public CompoundPacketData asCompoundPacketData() {
            return this;
        }

        @Override
        public <T> T fold(final Function<SimplePacketData, T> onSimple, final Function<CompoundPacketData, T> onCompound) {
            return onCompound.apply(this);
        }

        // Comparable Methods

        @Override
        public int compareTo(final PacketData o) {
            final CompoundPacketData lo = o.asCompoundPacketData();

            int i, j;
            for (i = 0, j = 0; i < data.size() && j < lo.data.size(); i++, j++) {
                final PacketData lhs = data.get(i);
                final PacketData rhs = lo.data.get(j);

                final int cmp = lhs.compareTo(rhs);
                if (cmp != 0) return cmp;
            }

            return Integer.compare(data.size(), lo.data.size());
        }

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final CompoundPacketData that = (CompoundPacketData) o;
            return data.equals(that.data);
        }

        @Override
        public int hashCode() {
            return Objects.hash(data);
        }

        // To String

        @Override
        public String toString() {
            return data.stream()
                    .map(PacketData::toString)
                    .collect(Collectors.joining(",", "[", "]"));
        }

    }

    /*
     * `PacketData` containing only a single `Integer` value.
     */
    private record SimplePacketData(int val) implements PacketData {

        // PacketData Methods

        @Override
        public CompoundPacketData asCompoundPacketData() {
            return new CompoundPacketData(this);
        }

        @Override
        public <T> T fold(final Function<SimplePacketData, T> onSimple, final Function<CompoundPacketData, T> onCompound) {
            return onSimple.apply(this);
        }

        // Comparable Methods

        @Override
        public int compareTo(final PacketData o) {
            return o.fold(
                    so -> val - so.val,
                    co -> asCompoundPacketData().compareTo(co)
            );
        }

        // To String

        @Override
        public String toString() {
            return Integer.toString(val);
        }

    }

}

