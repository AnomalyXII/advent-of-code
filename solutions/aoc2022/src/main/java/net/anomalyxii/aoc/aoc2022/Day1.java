package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 1: Calorie Counting.
 */
@Solution(year = 2022, day = 1, title = "Calorie Counting")
public class Day1 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Santa's reindeer typically eat regular reindeer food, but they need a lot of <a href="/2018/day/25">magical energy</a> to deliver presents on Christmas.
     * For that, their favorite snack is a special type of <b>star</b> fruit that only grows deep in the jungle.
     * The Elves have brought you on their annual expedition to the grove where the fruit grows.
     * <p>
     * To supply enough magical energy, the expedition needs to retrieve a minimum of <b>fifty stars</b> by December 25th.
     * Although the Elves assure you that the grove has plenty of fruit, you decide to grab any fruit you see along the way, just in case.
     * <p>
     * Collect stars by solving puzzles.
     * Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first.
     * Each puzzle grants <b>one star</b>.
     * Good luck!
     * <p>
     * The jungle must be too overgrown and difficult to navigate in vehicles or access from the air; the Elves' expedition traditionally goes on foot.
     * As your boats approach land, the Elves begin taking inventory of their supplies.
     * One important consideration is food - in particular, the number of <em>Calories</em> each Elf is carrying (your puzzle input).
     * <p>
     * The Elves take turns writing down the number of Calories contained by the various meals, snacks, rations, <span title="By &quot;etc&quot;, you're pretty sure they just mean &quot;more snacks&quot;.">etc.</span> that they've brought with them, one item per line.
     * Each Elf separates their own inventory from the previous Elf's inventory (if any) by a blank line.
     * <p>
     * For example, suppose the Elves finish writing their items' Calories and end up with the following list:
     * <pre>
     * 1000
     * 2000
     * 3000
     *
     * 4000
     *
     * 5000
     * 6000
     *
     * 7000
     * 8000
     * 9000
     *
     * 10000
     * </pre>
     * <p>
     * <p>
     * This list represents the Calories of the food carried by five Elves:
     * <ul>
     * <li>The first Elf is carrying food with <code>1000</code>, <code>2000</code>, and <code>3000</code> Calories, a total of <code><em>6000</em></code> Calories.</li>
     * <li>The second Elf is carrying one food item with <code><em>4000</em></code> Calories.</li>
     * <li>The third Elf is carrying food with <code>5000</code> and <code>6000</code> Calories, a total of <code><em>11000</em></code> Calories.</li>
     * <li>The fourth Elf is carrying food with <code>7000</code>, <code>8000</code>, and <code>9000</code> Calories, a total of <code><em>24000</em></code> Calories.</li>
     * <li>The fifth Elf is carrying one food item with <code><em>10000</em></code> Calories.</li>
     * </ul>
     * In case the Elves get hungry and need extra snacks, they need to know which Elf to ask: they'd like to know how many Calories are being carried by the Elf carrying the <em>most</em> Calories.
     * <p>
     * In the example above, this is <i><code>24000</code></i> (carried by the fourth Elf).
     * <p>
     * Find the Elf carrying the most Calories.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many total Calories is that Elf carrying?
     * @throws IllegalStateException if no elves are carrying snacks
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.streamBatches()
                .mapToLong(counts -> counts.stream().mapToLong(Long::parseLong).sum())
                .max()
                .orElseThrow(() -> new IllegalStateException("No result found"));
    }

    /**
     * By the time you calculate the answer to the Elves' question, they've already realized that the Elf carrying the most Calories of food might eventually <em>run out of snacks</em>.
     * <p>
     * To avoid this unacceptable situation, the Elves would instead like to know the total Calories carried by the <em>top three</em> Elves carrying the most Calories.
     * That way, even if one of those Elves runs out of snacks, they still have two backups.
     * <p>
     * In the example above, the top three Elves are the fourth Elf (with <code>24000</code> Calories), then the third Elf (with <code>11000</code> Calories), then the fifth Elf (with <code>10000</codE> Calories).
     * The sum of the Calories carried by these three elves is <code><em>45000</em></code>.
     * <p>
     * Find the top three Elves carrying the most Calories.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many Calories are those Elves carrying in total?
     * @throws IllegalStateException if there are fewer than three elves carrying snacks
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final long[] calories = context.streamBatches()
                .mapToLong(counts -> counts.stream().mapToLong(Long::parseLong).sum())
                .sorted()
                .toArray();

        if (calories.length < 3)
            throw new IllegalStateException("There are fewer than three elves carrying snacks :(");

        return calories[calories.length - 1] + calories[calories.length - 2] + calories[calories.length - 3];
    }

}
