package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 6: Tuning Trouble.
 */
@Solution(year = 2022, day = 6, title = "Tuning Trouble")
public class Day6 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The preparations are finally complete; you and the Elves leave camp on foot and begin to make your way toward the <em class="star">star</em> fruit grove.
     * <p>
     * As you move through the dense undergrowth, one of the Elves gives you a handheld <em>device</em>.
     * He says that it has many fancy features, but the most important one to set up right now is the <em>communication system</em>.
     * <p>
     * However, because he's heard you have <a href="/2016/day/6">significant</a> <a href="/2016/day/25">experience</a> <a href="/2019/day/7">dealing</a> <a href="/2019/day/9">with</a> <a href="/2019/day/16">signal-based</a> <a href="/2021/day/25">systems</a>, he convinced the other Elves that it would be okay to give you their one malfunctioning device - surely you'll have no problem fixing it.
     * <p>
     * As if inspired by comedic timing, the device emits a few <span title="The magic smoke, on the other hand, seems to be contained... FOR NOW!">colorful sparks</span>.
     * <p>
     * To be able to communicate with the Elves, the device needs to <em>lock on to their signal</em>.
     * The signal is a series of seemingly-random characters that the device receives one at a time.
     * <p>
     * To fix the communication system, you need to add a subroutine to the device that detects a <em>start-of-packet marker</em> in the datastream.
     * In the protocol being used by the Elves, the start of a packet is indicated by a sequence of <em>four characters that are all different</em>.
     * <p>
     * The device will send your subroutine a datastream buffer (your puzzle input); your subroutine needs to identify the first position where the four most recently received characters were all different.
     * Specifically, it needs to report the number of characters from the beginning of the buffer to the end of the first such four-character marker.
     * <p>
     * For example, suppose you receive the following datastream buffer:
     * <pre><code>mjqjpqmgbljsphdztnvjfqwrcgsmlb</code></pre>
     * <p>
     * After the first three characters (<code>mjq</code>) have been received, there haven't been enough characters received yet to find the marker.
     * The first time a marker could occur is after the fourth character is received, making the most recent four characters <code>mjqj</code>.
     * Because <code>j</code> is repeated, this isn't a marker.
     * <p>
     * The first time a marker appears is after the <em>seventh</em> character arrives.
     * Once it does, the last four characters received are <code>jpqm</code>, which are all different.
     * In this case, your subroutine should report the value <code><em>7</em></code>, because the first start-of-packet marker is complete after 7 characters have been processed.
     * <p>
     * Here are a few more examples:
     * <ul>
     * <li><code>bvwbjplbgvbhsrlpgdmjqwftvncz</code>: first marker after character <code><em>5</em></code></li>
     * <li><code>nppdvjthqldpwncqszvftbrmjlhg</code>: first marker after character <code><em>6</em></code></li>
     * <li><code>nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg</code>: first marker after character <code><em>10</em></code></li>
     * <li><code>zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw</code>: first marker after character <code><em>11</em></code></li>
     * </ul>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many characters need to be processed before the first start-of-packet marker is detected?
     * @throws IllegalStateException if no start-of-packet marker is found
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return locateStartOfSignal(context, Marker.START_OF_PACKET);
    }

    /**
     * Your device's communication system is correctly detecting packets, but still isn't working.
     * It looks like it also needs to look for <em>messages</em>.
     * <p>
     * A <em>start-of-message marker</em> is just like a start-of-packet marker, except it consists of <em>14 distinct characters</em> rather than 4.
     * <p>
     * Here are the first positions of start-of-message markers for all of the above examples:
     * <ul>
     * <li><code>mjqjpqmgbljsphdztnvjfqwrcgsmlb</code>: first marker after character <code><em>19</em></code></li>
     * <li><code>bvwbjplbgvbhsrlpgdmjqwftvncz</code>: first marker after character <code><em>23</em></code></li>
     * <li><code>nppdvjthqldpwncqszvftbrmjlhg</code>: first marker after character <code><em>23</em></code></li>
     * <li><code>nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg</code>: first marker after character <code><em>29</em></code></li>
     * <li><code>zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw</code>: first marker after character <code><em>26</em></code></li>
     * </ul>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many characters need to be processed before the first start-of-message marker is detected?
     * @throws IllegalStateException if no start-of-message marker is found
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return locateStartOfSignal(context, Marker.START_OF_MESSAGE);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Find the earliest occurrence of a specified `Marker`.
     */
    private static int locateStartOfSignal(final SolutionContext context, final Marker marker) {
        final String line = context.readLine();
        for (int i = marker.length; i < line.length(); i++)
            if (marker.isMarker(line, i))
                return i;
        throw new IllegalStateException("No signal found :(");
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Represents a marker within a signal.
     */
    private enum Marker {

        /**
         * Indicates the start of a packet.
         */
        START_OF_PACKET(4),

        /**
         * Indicates the start of a message.
         */
        START_OF_MESSAGE(14),

        // End of constants
        ;

        private final int length;

        // Constructors

        Marker(final int length) {
            this.length = length;
        }

        // Helper Methods

        /**
         * Check if the given line contains this signal marker, starting from a
         * specified position within the signal.
         *
         * @param line  the signal
         * @param start the starting position within the signal
         * @return {@literal true} if the signal contains the given {@link Marker}
         */
        boolean isMarker(final String line, final int start) {
            for (int i = 1; i <= length; i++) {
                final String substring = line.substring(start - (i - 1), start);
                final char ch = line.charAt(start - i);
                if (substring.indexOf(ch) >= 0)
                    return false;
            }

            return true;
        }

    }

}
