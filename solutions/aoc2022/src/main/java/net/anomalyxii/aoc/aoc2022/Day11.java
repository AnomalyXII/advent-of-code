package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.maths.Operation;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.function.IntFunction;
import java.util.function.LongUnaryOperator;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 11: Monkey in the Middle.
 */
@Solution(year = 2022, day = 11, title = "Monkey in the Middle")
public class Day11 {

    private static final Pattern STARTING_ITEMS = Pattern.compile(" {2}Starting items: ((?:[0-9]+, )*[0-9]+)");
    private static final Pattern OPERATION = Pattern.compile(" {2}Operation: new = (old|[0-9]+) ([+/*-]) (old|[0-9]+)");
    private static final Pattern TEST_CONDITION = Pattern.compile(" {2}Test: divisible by ([0-9]+)");
    private static final Pattern TEST_IF_TRUE = Pattern.compile(" {4}If true: throw to monkey ([0-9]+)");
    private static final Pattern TEST_IF_FALSE = Pattern.compile(" {4}If false: throw to monkey ([0-9]+)");

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * As you finally start making your way upriver, you realize your pack is much lighter than you remember.
     * Just then, one of the items from your pack goes flying overhead.
     * Monkeys are playing <a href="https://en.wikipedia.org/wiki/Keep_away" target="_blank">Keep Away</a> with your missing things!
     * <p>
     * To get your stuff back, you need to be able to predict where the monkeys will throw your items.
     * After some careful observation, you realize the monkeys operate based on <em>how worried you are about each item</em>.
     * <p>
     * You take some notes (your puzzle input) on the items each monkey currently has, how worried you are about those items, and how the monkey makes decisions based on your worry level.
     * For example:
     * <pre>
     * Monkey 0:
     *   Starting items: 79, 98
     *   Operation: new = old * 19
     *   Test: divisible by 23
     *     If true: throw to monkey 2
     *     If false: throw to monkey 3
     *
     * Monkey 1:
     *   Starting items: 54, 65, 75, 74
     *   Operation: new = old + 6
     *   Test: divisible by 19
     *     If true: throw to monkey 2
     *     If false: throw to monkey 0
     *
     * Monkey 2:
     *   Starting items: 79, 60, 97
     *   Operation: new = old * old
     *   Test: divisible by 13
     *     If true: throw to monkey 1
     *     If false: throw to monkey 3
     *
     * Monkey 3:
     *   Starting items: 74
     *   Operation: new = old + 3
     *   Test: divisible by 17
     *     If true: throw to monkey 0
     *     If false: throw to monkey 1
     * </pre>
     * <p>
     * Each monkey has several attributes:
     * <ul>
     * <li><code>Starting items</code> lists your <em>worry level</em> for each item the monkey is currently holding in the order they will be inspected.</li>
     * <li><code>Operation</code> shows how your worry level changes as that monkey inspects an item.
     * (An operation like <code>new = old * 5</code> means that your worry level after the monkey inspected the item is five times whatever your worry level was before inspection.)</li>
     * <li><code>Test</code> shows how the monkey uses your worry level to decide where to throw an item next.
     *   <ul>
     *   <li><code>If true</code> shows what happens with an item if the <code>Test</code> was true.</li>
     *   <li><code>If false</code> shows what happens with an item if the <code>Test</code> was false.</li>
     *   </ul>
     * </li>
     * </ul>
     * <p>
     * After each monkey inspects an item but before it tests your worry level, your relief that the monkey's inspection didn't damage the item causes your worry level to be <em>divided by three</em> and rounded down to the nearest integer.
     * <p>
     * The monkeys take turns inspecting and throwing items.
     * On a single monkey's <em>turn</em>, it inspects and throws all of the items it is holding one at a time and in the order listed.
     * Monkey <code>0</code> goes first, then monkey <code>1</code>, and so on until each monkey has had one turn.
     * The process of each monkey taking a single turn is called a <em>round</em>.
     * <p>
     * When a monkey throws an item to another monkey, the item goes on the <em>end</em> of the recipient monkey's list.
     * A monkey that starts a round with no items could end up inspecting and throwing many items by the time its turn comes around.
     * If a monkey is holding no items at the start of its turn, its turn ends.
     * <p>
     * In the above example, the first round proceeds as follows:
     * <pre>
     * Monkey 0:
     *   Monkey inspects an item with a worry level of 79.
     *     Worry level is multiplied by 19 to 1501.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 500.
     *     Current worry level is not divisible by 23.
     *     Item with worry level 500 is thrown to monkey 3.
     *   Monkey inspects an item with a worry level of 98.
     *     Worry level is multiplied by 19 to 1862.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 620.
     *     Current worry level is not divisible by 23.
     *     Item with worry level 620 is thrown to monkey 3.
     * Monkey 1:
     *   Monkey inspects an item with a worry level of 54.
     *     Worry level increases by 6 to 60.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 20.
     *     Current worry level is not divisible by 19.
     *     Item with worry level 20 is thrown to monkey 0.
     *   Monkey inspects an item with a worry level of 65.
     *     Worry level increases by 6 to 71.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 23.
     *     Current worry level is not divisible by 19.
     *     Item with worry level 23 is thrown to monkey 0.
     *   Monkey inspects an item with a worry level of 75.
     *     Worry level increases by 6 to 81.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 27.
     *     Current worry level is not divisible by 19.
     *     Item with worry level 27 is thrown to monkey 0.
     *   Monkey inspects an item with a worry level of 74.
     *     Worry level increases by 6 to 80.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 26.
     *     Current worry level is not divisible by 19.
     *     Item with worry level 26 is thrown to monkey 0.
     * Monkey 2:
     *   Monkey inspects an item with a worry level of 79.
     *     Worry level is multiplied by itself to 6241.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 2080.
     *     Current worry level is divisible by 13.
     *     Item with worry level 2080 is thrown to monkey 1.
     *   Monkey inspects an item with a worry level of 60.
     *     Worry level is multiplied by itself to 3600.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 1200.
     *     Current worry level is not divisible by 13.
     *     Item with worry level 1200 is thrown to monkey 3.
     *   Monkey inspects an item with a worry level of 97.
     *     Worry level is multiplied by itself to 9409.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 3136.
     *     Current worry level is not divisible by 13.
     *     Item with worry level 3136 is thrown to monkey 3.
     * Monkey 3:
     *   Monkey inspects an item with a worry level of 74.
     *     Worry level increases by 3 to 77.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 25.
     *     Current worry level is not divisible by 17.
     *     Item with worry level 25 is thrown to monkey 1.
     *   Monkey inspects an item with a worry level of 500.
     *     Worry level increases by 3 to 503.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 167.
     *     Current worry level is not divisible by 17.
     *     Item with worry level 167 is thrown to monkey 1.
     *   Monkey inspects an item with a worry level of 620.
     *     Worry level increases by 3 to 623.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 207.
     *     Current worry level is not divisible by 17.
     *     Item with worry level 207 is thrown to monkey 1.
     *   Monkey inspects an item with a worry level of 1200.
     *     Worry level increases by 3 to 1203.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 401.
     *     Current worry level is not divisible by 17.
     *     Item with worry level 401 is thrown to monkey 1.
     *   Monkey inspects an item with a worry level of 3136.
     *     Worry level increases by 3 to 3139.
     *     Monkey gets bored with item.
     *     Worry level is divided by 3 to 1046.
     *     Current worry level is not divisible by 17.
     *     Item with worry level 1046 is thrown to monkey 1.
     * </pre>
     * <p>
     * After round 1, the monkeys are holding items with these worry levels:
     * <pre>
     * Monkey 0: 20, 23, 27, 26
     * Monkey 1: 2080, 25, 167, 207, 401, 1046
     * Monkey 2:
     * Monkey 3:
     * </pre>
     * <p>
     * Monkeys 2 and 3 aren't holding any items at the end of the round; they both inspected items during the round and threw them all before the round ended.
     * <p>
     * This process continues for a few more rounds:
     * <pre>
     * After round 2, the monkeys are holding items with these worry levels:
     * Monkey 0: 695, 10, 71, 135, 350
     * Monkey 1: 43, 49, 58, 55, 362
     * Monkey 2:
     * Monkey 3:
     *
     * After round 3, the monkeys are holding items with these worry levels:
     * Monkey 0: 16, 18, 21, 20, 122
     * Monkey 1: 1468, 22, 150, 286, 739
     * Monkey 2:
     * Monkey 3:
     *
     * After round 4, the monkeys are holding items with these worry levels:
     * Monkey 0: 491, 9, 52, 97, 248, 34
     * Monkey 1: 39, 45, 43, 258
     * Monkey 2:
     * Monkey 3:
     *
     * After round 5, the monkeys are holding items with these worry levels:
     * Monkey 0: 15, 17, 16, 88, 1037
     * Monkey 1: 20, 110, 205, 524, 72
     * Monkey 2:
     * Monkey 3:
     *
     * After round 6, the monkeys are holding items with these worry levels:
     * Monkey 0: 8, 70, 176, 26, 34
     * Monkey 1: 481, 32, 36, 186, 2190
     * Monkey 2:
     * Monkey 3:
     *
     * After round 7, the monkeys are holding items with these worry levels:
     * Monkey 0: 162, 12, 14, 64, 732, 17
     * Monkey 1: 148, 372, 55, 72
     * Monkey 2:
     * Monkey 3:
     *
     * After round 8, the monkeys are holding items with these worry levels:
     * Monkey 0: 51, 126, 20, 26, 136
     * Monkey 1: 343, 26, 30, 1546, 36
     * Monkey 2:
     * Monkey 3:
     *
     * After round 9, the monkeys are holding items with these worry levels:
     * Monkey 0: 116, 10, 12, 517, 14
     * Monkey 1: 108, 267, 43, 55, 288
     * Monkey 2:
     * Monkey 3:
     *
     * After round 10, the monkeys are holding items with these worry levels:
     * Monkey 0: 91, 16, 20, 98
     * Monkey 1: 481, 245, 22, 26, 1092, 30
     * Monkey 2:
     * Monkey 3:
     *
     * ...
     *
     * After round 15, the monkeys are holding items with these worry levels:
     * Monkey 0: 83, 44, 8, 184, 9, 20, 26, 102
     * Monkey 1: 110, 36
     * Monkey 2:
     * Monkey 3:
     *
     * ...
     *
     * After round 20, the monkeys are holding items with these worry levels:
     * Monkey 0: 10, 12, 14, 26, 34
     * Monkey 1: 245, 93, 53, 199, 115
     * Monkey 2:
     * Monkey 3:
     * </pre>
     * <p>
     * Chasing all of the monkeys at once is impossible; you're going to have to focus on the <em>two most active</em> monkeys if you want any hope of getting your stuff back.
     * Count the <em>total number of times each monkey inspects items</em> over 20 rounds:
     * <pre>
     * <em>Monkey 0 inspected items 101 times.</em>
     * Monkey 1 inspected items 95 times.
     * Monkey 2 inspected items 7 times.
     * <em>Monkey 3 inspected items 105 times.</em>
     * </pre>
     * <p>
     * In this example, the two most active monkeys inspected items 101 and 105 times.
     * The level of <em>monkey business</em> in this situation can be found by multiplying these together: <code><em>10605</em></code>.
     * <p>
     * Figure out which monkeys to chase by counting how many items they inspect over 20 rounds.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the level of monkey business after 20 rounds of stuff-slinging simian shenanigans?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<Monkey> monkeys = Monkey.parseAll(context);
        return playKeepAway(20, monkeys, panik -> panik / 3);
    }

    /**
     * You're worried you might not ever get your items back.
     * So worried, in fact, that your relief that a monkey's inspection didn't damage an item <em>no longer causes your worry level to be divided by three</em>.
     * <p>
     * Unfortunately, that relief was all that was keeping your worry levels from reaching <em>ridiculous levels</em>.
     * You'll need to <em>find another way to keep your worry levels manageable</em>.
     * <p>
     * At this rate, you might be putting up with these monkeys for a <em>very long time</em> - possibly <em><code>10000</code> rounds</em>!
     * <p>
     * With these new rules, you can still figure out the <span title="Monkey business monkey business monkey business, monkey numbers...
     * is this working?">monkey business</span> after 10000 rounds.
     * Using the same example above:
     * <pre>
     * == After round 1 ==
     * Monkey 0 inspected items 2 times.
     * Monkey 1 inspected items 4 times.
     * Monkey 2 inspected items 3 times.
     * Monkey 3 inspected items 6 times.
     *
     * == After round 20 ==
     * Monkey 0 inspected items 99 times.
     * Monkey 1 inspected items 97 times.
     * Monkey 2 inspected items 8 times.
     * Monkey 3 inspected items 103 times.
     *
     * == After round 1000 ==
     * Monkey 0 inspected items 5204 times.
     * Monkey 1 inspected items 4792 times.
     * Monkey 2 inspected items 199 times.
     * Monkey 3 inspected items 5192 times.
     *
     * == After round 2000 ==
     * Monkey 0 inspected items 10419 times.
     * Monkey 1 inspected items 9577 times.
     * Monkey 2 inspected items 392 times.
     * Monkey 3 inspected items 10391 times.
     *
     * == After round 3000 ==
     * Monkey 0 inspected items 15638 times.
     * Monkey 1 inspected items 14358 times.
     * Monkey 2 inspected items 587 times.
     * Monkey 3 inspected items 15593 times.
     *
     * == After round 4000 ==
     * Monkey 0 inspected items 20858 times.
     * Monkey 1 inspected items 19138 times.
     * Monkey 2 inspected items 780 times.
     * Monkey 3 inspected items 20797 times.
     *
     * == After round 5000 ==
     * Monkey 0 inspected items 26075 times.
     * Monkey 1 inspected items 23921 times.
     * Monkey 2 inspected items 974 times.
     * Monkey 3 inspected items 26000 times.
     *
     * == After round 6000 ==
     * Monkey 0 inspected items 31294 times.
     * Monkey 1 inspected items 28702 times.
     * Monkey 2 inspected items 1165 times.
     * Monkey 3 inspected items 31204 times.
     *
     * == After round 7000 ==
     * Monkey 0 inspected items 36508 times.
     * Monkey 1 inspected items 33488 times.
     * Monkey 2 inspected items 1360 times.
     * Monkey 3 inspected items 36400 times.
     *
     * == After round 8000 ==
     * Monkey 0 inspected items 41728 times.
     * Monkey 1 inspected items 38268 times.
     * Monkey 2 inspected items 1553 times.
     * Monkey 3 inspected items 41606 times.
     *
     * == After round 9000 ==
     * Monkey 0 inspected items 46945 times.
     * Monkey 1 inspected items 43051 times.
     * Monkey 2 inspected items 1746 times.
     * Monkey 3 inspected items 46807 times.
     *
     * == After round 10000 ==
     * <em>Monkey 0 inspected items 52166 times.</em>
     * Monkey 1 inspected items 47830 times.
     * Monkey 2 inspected items 1938 times.
     * <em>Monkey 3 inspected items 52013 times.</em>
     * </pre>
     * <p>
     * After 10000 rounds, the two most active monkeys inspected items 52166 and 52013 times.
     * Multiplying these together, the level of <em>monkey business</em> in this situation is now <code><em>2713310158</em></code>.
     * <p>
     * Worry levels are no longer divided by three after each item is inspected; you'll need to find another way to keep your worry levels manageable.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Starting again from the initial state in your puzzle input, what is the level of monkey business after 10000 rounds?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<Monkey> monkeys = Monkey.parseAll(context);
        final long reliefMod = monkeys.stream()
                .mapToLong(m -> m.test.divisor)
                .reduce(1, (a, b) -> a * b);
        return playKeepAway(10_000, monkeys, panik -> panik % reliefMod);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Simulate the `Monkey`s playing keep-away.
     */
    private static long playKeepAway(final int rounds, final List<Monkey> monkeys, final LongUnaryOperator calmingOperation) {
        for (int round = 0; round < rounds; round++)
            monkeys.forEach(monkey -> monkey.muckAround(calmingOperation, monkeys::get));

        final long[] business = monkeys.stream()
                .mapToLong(monkey -> monkey.counter)
                .sorted()
                .toArray();
        return business[business.length - 2] * business[business.length - 1];
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Inspect an item, and work out the intensity of panic that inspecting
     * the item will generate.
     */
    private record Inspector(LongUnaryOperator lhs, Operation op, LongUnaryOperator rhs) {

        // Inspector Methods

        /**
         * Inspect an item with a given level of panic and work out what the new
         * amount of panic will be after the inspection is finished.
         *
         * @param panic the panic-level of an item
         * @return the new panic level
         */
        public long inspect(final long panic) {
            return op.apply(lhs.applyAsLong(panic), rhs.applyAsLong(panic));
        }

    }

    /*
     * Work out where to throw an item to,
     */
    private record Discarder(int divisor, int toTrue, int toFalse) {

        // Discarder Methods

        /**
         * Test an item, with a given worry level, and decide which
         * {@link Monkey} to throw the item to next.
         *
         * @param panik the panik level of the item
         * @return the ID of the {@link Monkey} to throw to
         */
        public int test(final long panik) {
            return panik % divisor == 0 ? toTrue : toFalse;
        }

    }

    /*
     * A Monkey, that contains some items that will be inspected and then
     * discarded.
     */
    private static final class Monkey {

        // Private members

        private final Deque<Long> items;
        private final Inspector operation;
        private final Discarder test;

        private int counter = 0;

        // Constructors

        Monkey(final Deque<Long> items, final Inspector operation, final Discarder test) {
            this.items = items;
            this.operation = operation;
            this.test = test;
        }

        // Monkey Methods

        /**
         * Simulate a {@link Monkey} messing with the items they are currently
         * holding.
         * <p>
         * For each item, the {@link Monkey} will first inspect the item (raising
         * the panic-level for that item) and then discard the item (reducing the
         * panic-level) before tossing the item to a different {@link Monkey}.
         *
         * @param calmingOperation   a calming operation, used to reduce panic-levels
         * @param nextMonkeyResolver resolve a {@link Monkey} based on its ID
         */
        public void muckAround(final LongUnaryOperator calmingOperation, final IntFunction<Monkey> nextMonkeyResolver) {
            while (!items.isEmpty()) {
                final long item = items.removeFirst();
                final long panik = operation.inspect(item);
                assert panik >= 0 : "Panik overflow (" + panik + ")";

                final long relief = calmingOperation.applyAsLong(panik);

                final int nextMonkey = test.test(relief);

                ++counter;

                nextMonkeyResolver.apply(nextMonkey).receive(relief);
            }
        }

        /**
         * Receive an item from another {@link Monkey}.
         *
         * @param item the panic-level of the item to receive
         */
        public void receive(final long item) {
            items.addLast(item);
        }

        // Static Helper Methods

        /**
         * Parse a {@link List} of {@link Monkey Monkies}.
         *
         * @param context the {@link SolutionContext} to read from
         * @return the {@link List} of {@link Monkey Monkies}
         */
        public static List<Monkey> parseAll(final SolutionContext context) {
            return context.streamBatches()
                    .map(Monkey::parse)
                    .toList();
        }

        /**
         * Parse a {@link Monkey}.
         *
         * @param batch the lines defining the {@link Monkey}
         * @return the {@link Monkey}
         */
        public static Monkey parse(final List<String> batch) {
            return new Monkey(
                    extractStartingItems(batch),
                    extractOperation(batch),
                    extractTest(batch)
            );
        }

        /*
         * Extract the `List` of starting items.
         */
        private static ArrayDeque<Long> extractStartingItems(final List<String> batch) {
            return new ArrayDeque<>(
                    Arrays.stream(getMatchResult(STARTING_ITEMS, batch.get(1))
                                          .group(1)
                                          .split(", "))
                            .map(Long::valueOf)
                            .toList()
            );
        }

        /*
         * Extract the inspection operation.
         */
        private static Inspector extractOperation(final List<String> batch) {
            final MatchResult result = getMatchResult(OPERATION, batch.get(2));
            final LongUnaryOperator lhs = "old".equals(result.group(1))
                    ? old -> old
                    : old -> Long.parseLong(result.group(1));
            final LongUnaryOperator rhs = "old".equals(result.group(3))
                    ? old -> old
                    : old -> Long.parseLong(result.group(3));
            return new Inspector(lhs, Operation.fromString(result.group(2)), rhs);
        }

        /*
         * Extract the discarder operation.
         */
        private static Discarder extractTest(final List<String> batch) {
            final MatchResult testResult = getMatchResult(TEST_CONDITION, batch.get(3));
            final MatchResult testTrueResult = getMatchResult(TEST_IF_TRUE, batch.get(4));
            final MatchResult testFalseResult = getMatchResult(TEST_IF_FALSE, batch.get(5));

            final int worryMod = Integer.parseInt(testResult.group(1));
            final int toTrue = Integer.parseInt(testTrueResult.group(1));
            final int toFalse = Integer.parseInt(testFalseResult.group(1));

            return new Discarder(worryMod, toTrue, toFalse);
        }

        /*
         * Verify a line matches and create a `MatchResult`.
         */
        private static MatchResult getMatchResult(final Pattern pattern, final String line) {
            final Matcher matcher = pattern.matcher(line);
            if (!matcher.matches())
                throw new IllegalStateException("Expected to process a line matching /" + pattern + "/; but was: " + line);
            return matcher.toMatchResult();
        }

    }

}

