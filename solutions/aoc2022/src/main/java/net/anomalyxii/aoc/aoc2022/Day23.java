package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Area;
import net.anomalyxii.aoc.utils.geometry.Bounds;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Velocity;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 23: Unstable Diffusion.
 */
@Solution(year = 2022, day = 23, title = "Unstable Diffusion")
public class Day23 {

    private static final Velocity[] DIRECTIONS = {Velocity.NORTH, Velocity.SOUTH, Velocity.WEST, Velocity.EAST};

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You enter a large crater of gray dirt where the grove is supposed to be.
     * All around you, plants you imagine were expected to be full of fruit are instead withered and broken.
     * A large group of Elves has formed in the middle of the grove.
     * <p>
     * "...but this volcano has been dormant for months.
     * Without ash, the fruit can't grow!"
     * <p>
     * You look up to see a massive, snow-capped mountain towering above you.
     * <p>
     * "It's not like there are other active volcanoes here; we've looked everywhere."
     * <p>
     * "But our scanners show active magma flows; clearly it's going <em>somewhere</em>."
     * <p>
     * They finally notice you at the edge of the grove, your pack almost overflowing from the random <em class="star">star</em> fruit you've been collecting.
     * Behind you, elephants and monkeys explore the grove, looking concerned.
     * Then, the Elves recognize the ash cloud slowly spreading above your recent detour.
     * <p>
     * "Why do you--" "How is--" "Did you just--"
     * <p>
     * Before any of them can form a complete question, another Elf speaks up: "Okay, new plan.
     * We have almost enough fruit already, and ash from the plume should spread here eventually.
     * If we quickly plant new seedlings now, we can still make it to the extraction point.
     * Spread out!"
     * <p>
     * The Elves each reach into their pack and pull out a tiny plant.
     * The plants rely on important nutrients from the ash, so they can't be planted too close together.
     * <p>
     * There isn't enough time to let the Elves figure out where to plant the seedlings themselves; you quickly scan the grove (your puzzle input) and note their positions.
     * <p>
     * For example:
     * <pre>
     * ....#..
     * ..###.#
     * #...#.#
     * .#...##
     * #.###..
     * ##.#.##
     * .#..#..
     * </pre>
     * <p>
     * The scan shows Elves <code>#</code> and empty ground <code>.</code>; outside your scan, more empty ground extends a long way in every direction.
     * The scan is oriented so that <em>north is up</em>; orthogonal directions are written N (north), S (south), W (west), and E (east), while diagonal directions are written NE, NW, SE, SW.
     * <p>
     * The Elves follow a time-consuming process to figure out where they should each go; you can speed up this process considerably.
     * The process consists of some number of <em>rounds</em> during which Elves alternate between considering where to move and actually moving.
     * <p>
     * During the <em>first half</em> of each round, each Elf considers the eight positions adjacent to themself.
     * If no other Elves are in one of those eight positions, the Elf <em>does not do anything</em> during this round.
     * Otherwise, the Elf looks in each of four directions in the following order and <em>proposes</em> moving one step in the <em>first valid direction</em>:
     * <ul>
     * <li>If there is no Elf in the N, NE, or NW adjacent positions, the Elf proposes moving <em>north</em> one step.</li>
     * <li>If there is no Elf in the S, SE, or SW adjacent positions, the Elf proposes moving <em>south</em> one step.</li>
     * <li>If there is no Elf in the W, NW, or SW adjacent positions, the Elf proposes moving <em>west</em> one step.</li>
     * <li>If there is no Elf in the E, NE, or SE adjacent positions, the Elf proposes moving <em>east</em> one step.</li>
     * </ul>
     * <p>
     * After each Elf has had a chance to propose a move, the <em>second half</em> of the round can begin.
     * Simultaneously, each Elf moves to their proposed destination tile if they were the <em>only</em> Elf to propose moving to that position.
     * If two or more Elves propose moving to the same position, <em>none</em> of those Elves move.
     * <p>
     * Finally, at the end of the round, the <em>first direction</em> the Elves considered is moved to the end of the list of directions.
     * For example, during the second round, the Elves would try proposing a move to the south first, then west, then east, then north.
     * On the third round, the Elves would first consider west, then east, then north, then south.
     * <p>
     * As a smaller example, consider just these five Elves:
     * <pre>
     * .....
     * ..##.
     * ..#..
     * .....
     * ..##.
     * .....
     * </pre>
     * <p>
     * The northernmost two Elves and southernmost two Elves all propose moving north, while the middle Elf cannot move north and proposes moving south.
     * The middle Elf proposes the same destination as the southwest Elf, so neither of them move, but the other three do:
     * <pre>
     * ..##.
     * .....
     * ..#..
     * ...#.
     * ..#..
     * .....
     * </pre>
     * <p>
     * Next, the northernmost two Elves and the southernmost Elf all propose moving south.
     * Of the remaining middle two Elves, the west one cannot move south and proposes moving west, while the east one cannot move south <em>or</em> west and proposes moving east.
     * All five Elves succeed in moving to their proposed positions:
     * <pre>
     * .....
     * ..##.
     * .#...
     * ....#
     * .....
     * ..#..
     * </pre>
     * <p>
     * Finally, the southernmost two Elves choose not to move at all.
     * Of the remaining three Elves, the west one proposes moving west, the east one proposes moving east, and the middle one proposes moving north; all three succeed in moving:
     * <pre>
     * ..#..
     * ....#
     * #....
     * ....#
     * .....
     * ..#..
     * </pre>
     * <p>
     * At this point, no Elves need to move, and so the process ends.
     * <p>
     * The larger example above proceeds as follows:
     * <pre>
     * == Initial State ==
     * ..............
     * ..............
     * .......#......
     * .....###.#....
     * ...#...#.#....
     * ....#...##....
     * ...#.###......
     * ...##.#.##....
     * ....#..#......
     * ..............
     * ..............
     * ..............
     *
     * == End of Round 1 ==
     * ..............
     * .......#......
     * .....#...#....
     * ...#..#.#.....
     * .......#..#...
     * ....#.#.##....
     * ..#..#.#......
     * ..#.#.#.##....
     * ..............
     * ....#..#......
     * ..............
     * ..............
     *
     * == End of Round 2 ==
     * ..............
     * .......#......
     * ....#.....#...
     * ...#..#.#.....
     * .......#...#..
     * ...#..#.#.....
     * .#...#.#.#....
     * ..............
     * ..#.#.#.##....
     * ....#..#......
     * ..............
     * ..............
     *
     * == End of Round 3 ==
     * ..............
     * .......#......
     * .....#....#...
     * ..#..#...#....
     * .......#...#..
     * ...#..#.#.....
     * .#..#.....#...
     * .......##.....
     * ..##.#....#...
     * ...#..........
     * .......#......
     * ..............
     *
     * == End of Round 4 ==
     * ..............
     * .......#......
     * ......#....#..
     * ..#...##......
     * ...#.....#.#..
     * .........#....
     * .#...###..#...
     * ..#......#....
     * ....##....#...
     * ....#.........
     * .......#......
     * ..............
     *
     * == End of Round 5 ==
     * .......#......
     * ..............
     * ..#..#.....#..
     * .........#....
     * ......##...#..
     * .#.#.####.....
     * ...........#..
     * ....##..#.....
     * ..#...........
     * ..........#...
     * ....#..#......
     * ..............
     * </pre>
     * <p>
     * After a few more rounds...
     * <pre>
     * == End of Round 10 ==
     * .......#......
     * ...........#..
     * ..#.#..#......
     * ......#.......
     * ...#.....#..#.
     * .#......##....
     * .....##.......
     * ..#........#..
     * ....#.#..#....
     * ..............
     * ....#..#..#...
     * ..............
     * </pre>
     * <p>
     * To make sure they're on the right track, the Elves like to check after round 10 that they're making good progress toward covering enough ground.
     * To do this, count the number of empty ground tiles contained by the smallest rectangle that contains every Elf.
     * (The edges of the rectangle should be aligned to the N/S/E/W directions; the Elves do not have the patience to calculate <span title="Arbitrary Rectangles is my Piet Mondrian cover band.">arbitrary rectangles</span>.)
     * In the above example, that rectangle is:
     * <pre>
     * ......#.....
     * ..........#.
     * .#.#..#.....
     * .....#......
     * ..#.....#..#
     * #......##...
     * ....##......
     * .#........#.
     * ...#.#..#...
     * ............
     * ...#..#..#..
     * </pre>
     * <p>
     * In this region, the number of empty ground tiles is <code><em>110</em></code>.
     * <p>
     * Simulate the Elves' process and find the smallest rectangle that contains the Elves after 10 rounds.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many empty ground tiles does that rectangle contain?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Set<Coordinate> elves = loadStartingPositions(context);
        for (int round = 0; round < 10; round++)
            runSimulation(round, elves);

        return calculateEmptyGroundTiles(elves);
    }

    /**
     * It seems you're on the right track.
     * Finish simulating the process and figure out where the Elves need to go.
     * How many rounds did you save them?
     * <p>
     * In the example above, the <em>first round where no Elf moved</em> was round <code><em>20</em></code>:
     * <pre>
     * .......#......
     * ....#......#..
     * ..#.....#.....
     * ......#.......
     * ...#....#.#..#
     * #.............
     * ....#.....#...
     * ..#.....#.....
     * ....#.#....#..
     * .........#....
     * ....#......#..
     * .......#......
     * </pre>
     * <p>
     * Figure out where the Elves need to go.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the number of the first round where no Elf moves?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Set<Coordinate> elves = loadStartingPositions(context);

        int round = 0;
        boolean elfMoved;
        do {
            elfMoved = runSimulation(round++, elves);
        } while (elfMoved);

        return round;
    }


    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Load the initial elf positions.
     */
    private static Set<Coordinate> loadStartingPositions(final SolutionContext context) {
        final List<String> input = context.read();
        return IntStream.range(0, input.size())
                .mapToObj(y -> IntStream.range(0, input.get(y).length())
                        .filter(x -> input.get(y).charAt(x) == '#')
                        .mapToObj(x -> new Coordinate(x, y))
                        .collect(Collectors.toSet()))
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    /*
     * Simulate all the elves moving (or not) on a single round.
     */
    private static boolean runSimulation(final int round, final Set<Coordinate> elves) {
        final Set<Coordinate> collisions = new HashSet<>();
        final Map<Coordinate, Coordinate> moves = new HashMap<>();

        for (final Coordinate elf : elves) {
            final boolean shouldMove = elf.neighbours().anyMatch(elves::contains);
            if (!shouldMove) continue;

            // try to determine a move:
            for (int v = 0; v < 4; v++) {
                final Velocity d = DIRECTIONS[(round + v) % 4];
                final Velocity dx = new Velocity(d.v(), d.h());
                final Velocity dxx = new Velocity(-d.v(), -d.h());

                final Coordinate n = elf.adjustBy(d); // Main direction
                final Coordinate nx = n.adjustBy(dx); // TODO: name better...!
                final Coordinate nxx = n.adjustBy(dxx);
                if (!elves.contains(n) && !elves.contains(nx) && !elves.contains(nxx)) {
                    // Candidate move!
                    if (!collisions.contains(n) && !moves.containsKey(n)) {
                        final Coordinate e = moves.put(n, elf);
                        assert e == null;
                    } else {
                        moves.remove(n);
                        collisions.add(n);
                    }
                    break;
                }
            }
        }

        elves.removeAll(moves.values());
        elves.addAll(moves.keySet());
        return !moves.isEmpty();
    }

    /*
     * Calculate the area of ground covered by the elves and, within that
     * area, how many units of ground are not covered by an elf.
     */
    private static long calculateEmptyGroundTiles(final Set<Coordinate> elves) {
        final Area area = elves.stream()
                .reduce(
                        Area.NULL,
                        (a, elf) -> Area.of(
                                Bounds.of(min(a.w().min(), elf.x()), max(a.w().max(), elf.x())),
                                Bounds.of(min(a.h().min(), elf.y()), max(a.h().max(), elf.y()))
                        ),
                        (a, b) -> {
                            throw new IllegalArgumentException("Shouldn't merge?");
                        }
                );

        return area.area() - elves.size();
    }

}

