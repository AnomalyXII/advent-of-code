package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.function.IntConsumer;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 10: Cathode-Ray Tube.
 */
@Solution(year = 2022, day = 10, title = "Cathode-Ray Tube")
public class Day10 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You avoid the ropes, plunge into the river, and swim to shore.
     * <p>
     * The Elves yell something about meeting back up with them upriver, but the river is too loud to tell exactly what they're saying.
     * They finish crossing the bridge and disappear from view.
     * <p>
     * Situations like this must be why the Elves prioritized getting the communication system on your handheld device working.
     * You pull it out of your pack, but the amount of water slowly draining from a big crack in its screen tells you it probably won't be of much immediate use.
     * <p>
     * <em>Unless</em>, that is, you can design a replacement for the device's video system!
     * It seems to be some kind of <a href="https://en.wikipedia.org/wiki/Cathode-ray_tube" target="_blank">cathode-ray tube</a> screen and simple CPU that are both driven by a precise <em>clock circuit</em>.
     * The clock circuit ticks at a constant rate; each tick is called a <em>cycle</em>.
     * <p>
     * Start by figuring out the signal being sent by the CPU.
     * The CPU has a single register, <code>X</code>, which starts with the value <code>1</code>.
     * It supports only two instructions:
     * <ul>
     * <li>
     * <code>addx V</code> takes <em>two cycles</em> to complete.
     * <em>After</em> two cycles, the <code>X</code> register is increased by the value <code>V</code>.
     * (<code>V</code> can be negative.)
     * </li>
     * <li>
     * <code>noop</code> takes <em>one cycle</em> to complete.
     * It has no other effect.
     * </li>
     * </ul>
     * <p>
     * The CPU uses these instructions in a program (your puzzle input) to, somehow, tell the screen what to draw.
     * <p>
     * Consider the following small program:
     * <pre>
     * noop
     * addx 3
     * addx -5
     * </pre>
     * <p>
     * Execution of this program proceeds as follows:
     * <ul>
     * <li>
     * At the start of the first cycle, the <code>noop</code> instruction begins execution.
     * During the first cycle, <code>X</code> is <code>1</code>.
     * After the first cycle, the <code>noop</code> instruction finishes execution, doing nothing.
     * </li>
     * <li>
     * At the start of the second cycle, the <code>addx 3</code> instruction begins execution.
     * During the second cycle, <code>X</code> is still <code>1</code>.
     * </li>
     * <li>
     * During the third cycle, <code>X</code> is still <code>1</code>.
     * After the third cycle, the <code>addx 3</code> instruction finishes execution, setting <code>X</code> to <code>4</code>.
     * </li>
     * <li>
     * At the start of the fourth cycle, the <code>addx -5</code> instruction begins execution.
     * During the fourth cycle, <code>X</code> is still <code>4</code>.
     * </li>
     * <li>
     * During the fifth cycle, <code>X</code> is still <code>4</code>.
     * After the fifth cycle, the <code>addx -5</code> instruction finishes execution, setting <code>X</code> to <code>-1</code>.
     * </li>
     * </ul>
     * <p>
     * Maybe you can learn something by looking at the value of the <code>X</code> register throughout execution.
     * For now, consider the <em>signal strength</em> (the cycle number multiplied by the value of the <code>X</code> register) <em>during</em> the 20th cycle and every 40 cycles after that (that is, during the 20th, 60th, 100th, 140th, 180th, and 220th cycles).
     * <p>
     * For example, consider this larger program:
     * <pre>
     * addx 15
     * addx -11
     * addx 6
     * addx -3
     * addx 5
     * addx -1
     * addx -8
     * addx 13
     * addx 4
     * noop
     * addx -1
     * addx 5
     * addx -1
     * addx 5
     * addx -1
     * addx 5
     * addx -1
     * addx 5
     * addx -1
     * addx -35
     * addx 1
     * addx 24
     * addx -19
     * addx 1
     * addx 16
     * addx -11
     * noop
     * noop
     * addx 21
     * addx -15
     * noop
     * noop
     * addx -3
     * addx 9
     * addx 1
     * addx -3
     * addx 8
     * addx 1
     * addx 5
     * noop
     * noop
     * noop
     * noop
     * noop
     * addx -36
     * noop
     * addx 1
     * addx 7
     * noop
     * noop
     * noop
     * addx 2
     * addx 6
     * noop
     * noop
     * noop
     * noop
     * noop
     * addx 1
     * noop
     * noop
     * addx 7
     * addx 1
     * noop
     * addx -13
     * addx 13
     * addx 7
     * noop
     * addx 1
     * addx -33
     * noop
     * noop
     * noop
     * addx 2
     * noop
     * noop
     * noop
     * addx 8
     * noop
     * addx -1
     * addx 2
     * addx 1
     * noop
     * addx 17
     * addx -9
     * addx 1
     * addx 1
     * addx -3
     * addx 11
     * noop
     * noop
     * addx 1
     * noop
     * addx 1
     * noop
     * noop
     * addx -13
     * addx -19
     * addx 1
     * addx 3
     * addx 26
     * addx -30
     * addx 12
     * addx -1
     * addx 3
     * addx 1
     * noop
     * noop
     * noop
     * addx -9
     * addx 18
     * addx 1
     * addx 2
     * noop
     * noop
     * addx 9
     * noop
     * noop
     * noop
     * addx -1
     * addx 2
     * addx -37
     * addx 1
     * addx 3
     * noop
     * addx 15
     * addx -21
     * addx 22
     * addx -6
     * addx 1
     * noop
     * addx 2
     * addx 1
     * noop
     * addx -10
     * noop
     * noop
     * addx 20
     * addx 1
     * addx 2
     * addx 2
     * addx -6
     * addx -11
     * noop
     * noop
     * noop
     * </pre>
     * <p>
     * The interesting signal strengths can be determined as follows:
     * <ul>
     * <li>
     * During the 20th cycle, register <code>X</code> has the value <code>21</code>, so the signal strength is 20 * 21 = <em>420</em>.
     * (The 20th cycle occurs in the middle of the second <code>addx -1</code>, so the value of register <code>X</code> is the starting value, <code>1</code>, plus all of the other <code>addx</code> values up to that point: 1 + 15 - 11 + 6 - 3 + 5 - 1 - 8 + 13 + 4 = 21.)
     * </li>
     * <li>During the 60th cycle, register <code>X</code> has the value <code>19</code>, so the signal strength is 60 * 19 = <code><em>1140</em></code>.</li>
     * <li>During the 100th cycle, register <code>X</code> has the value <code>18</code>, so the signal strength is 100 * 18 = <code><em>1800</em></code>.</li>
     * <li>During the 140th cycle, register <code>X</code> has the value <code>21</code>, so the signal strength is 140 * 21 = <code><em>2940</em></code>.</li>
     * <li>During the 180th cycle, register <code>X</code> has the value <code>16</code>, so the signal strength is 180 * 16 = <code><em>2880</em></code>.</li>
     * <li>During the 220th cycle, register <code>X</code> has the value <code>18</code>, so the signal strength is 220 * 18 = <code><em>3960</em></code>.</li>
     * </ul>
     * <p>
     * The sum of these signal strengths is <code><em>13140</em></code>.
     * <p>
     * Find the signal strength during the 20th, 60th, 100th, 140th, 180th, and 220th cycles.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of these six signal strengths?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final CPU cpu = CPU.create(context);

        final LongAccumulator sum = new LongAccumulator(Long::sum, 0);
        cpu.process((tick, x) -> {
            if ((tick - 20) % 40 == 0)
                sum.accumulate(x * tick);
        });

        return sum.longValue();
    }

    /**
     * It seems like the <code>X</code> register controls the horizontal position of a <a href="https://en.wikipedia.org/wiki/Sprite_(computer_graphics)" target="_blank">sprite</a>.
     * Specifically, the sprite is 3 pixels wide, and the <code>X</code> register sets the horizontal position of the <em>middle</em> of that sprite.
     * (In this system, there is no such thing as "vertical position": if the sprite's horizontal position puts its pixels where the CRT is currently drawing, then those pixels will be drawn.)
     * <p>
     * You count the pixels on the CRT: 40 wide and 6 high.
     * This CRT screen draws the top row of pixels left-to-right, then the row below that, and so on.
     * The left-most pixel in each row is in position <code>0</code>, and the right-most pixel in each row is in position <code>39</code>.
     * <p>
     * Like the CPU, the CRT is tied closely to the clock circuit: the CRT draws <em>a single pixel during each cycle</em>.
     * Representing each pixel of the screen as a <code>#</code>, here are the cycles during which the first and last pixel in each row are drawn:
     * <pre>
     * Cycle   1 -&gt; <em>#</em>######################################<em>#</em> &lt;- Cycle  40
     * Cycle  41 -&gt; <em>#</em>######################################<em>#</em> &lt;- Cycle  80
     * Cycle  81 -&gt; <em>#</em>######################################<em>#</em> &lt;- Cycle 120
     * Cycle 121 -&gt; <em>#</em>######################################<em>#</em> &lt;- Cycle 160
     * Cycle 161 -&gt; <em>#</em>######################################<em>#</em> &lt;- Cycle 200
     * Cycle 201 -&gt; <em>#</em>######################################<em>#</em> &lt;- Cycle 240
     * </pre>
     * <p>
     * So, by <a href="https://en.wikipedia.org/wiki/Racing_the_Beam" target="_blank">carefully</a> <a href="https://www.youtube.com/watch?v=sJFnWZH5FXc" target="_blank"><span title="While you're at it, go watch everything else by Retro Game Mechanics Explained, too.">timing</span></a> the CPU instructions and the CRT drawing operations, you should be able to determine whether the sprite is visible the instant each pixel is drawn.
     * If the sprite is positioned such that one of its three pixels is the pixel currently being drawn, the screen produces a <em>lit</em> pixel (<code>#</code>); otherwise, the screen leaves the pixel <em>dark</em> (<code>.</code>).
     * <p>
     * The first few pixels from the larger example above are drawn as follows:
     * <pre>
     * Sprite position: ###.....................................
     *
     * Start cycle   1: begin executing addx 15
     * During cycle  1: CRT draws pixel in position 0
     * Current CRT row: #
     *
     * During cycle  2: CRT draws pixel in position 1
     * Current CRT row: ##
     * End of cycle  2: finish executing addx 15 (Register X is now 16)
     * Sprite position: ...............###......................
     *
     * Start cycle   3: begin executing addx -11
     * During cycle  3: CRT draws pixel in position 2
     * Current CRT row: ##.
     *
     * During cycle  4: CRT draws pixel in position 3
     * Current CRT row: ##..
     * End of cycle  4: finish executing addx -11 (Register X is now 5)
     * Sprite position: ....###.................................
     *
     * Start cycle   5: begin executing addx 6
     * During cycle  5: CRT draws pixel in position 4
     * Current CRT row: ##..#
     *
     * During cycle  6: CRT draws pixel in position 5
     * Current CRT row: ##..##
     * End of cycle  6: finish executing addx 6 (Register X is now 11)
     * Sprite position: ..........###...........................
     *
     * Start cycle   7: begin executing addx -3
     * During cycle  7: CRT draws pixel in position 6
     * Current CRT row: ##..##.
     *
     * During cycle  8: CRT draws pixel in position 7
     * Current CRT row: ##..##..
     * End of cycle  8: finish executing addx -3 (Register X is now 8)
     * Sprite position: .......###..............................
     *
     * Start cycle   9: begin executing addx 5
     * During cycle  9: CRT draws pixel in position 8
     * Current CRT row: ##..##..#
     *
     * During cycle 10: CRT draws pixel in position 9
     * Current CRT row: ##..##..##
     * End of cycle 10: finish executing addx 5 (Register X is now 13)
     * Sprite position: ............###.........................
     *
     * Start cycle  11: begin executing addx -1
     * During cycle 11: CRT draws pixel in position 10
     * Current CRT row: ##..##..##.
     *
     * During cycle 12: CRT draws pixel in position 11
     * Current CRT row: ##..##..##..
     * End of cycle 12: finish executing addx -1 (Register X is now 12)
     * Sprite position: ...........###..........................
     *
     * Start cycle  13: begin executing addx -8
     * During cycle 13: CRT draws pixel in position 12
     * Current CRT row: ##..##..##..#
     *
     * During cycle 14: CRT draws pixel in position 13
     * Current CRT row: ##..##..##..##
     * End of cycle 14: finish executing addx -8 (Register X is now 4)
     * Sprite position: ...###..................................
     *
     * Start cycle  15: begin executing addx 13
     * During cycle 15: CRT draws pixel in position 14
     * Current CRT row: ##..##..##..##.
     *
     * During cycle 16: CRT draws pixel in position 15
     * Current CRT row: ##..##..##..##..
     * End of cycle 16: finish executing addx 13 (Register X is now 17)
     * Sprite position: ................###.....................
     *
     * Start cycle  17: begin executing addx 4
     * During cycle 17: CRT draws pixel in position 16
     * Current CRT row: ##..##..##..##..#
     *
     * During cycle 18: CRT draws pixel in position 17
     * Current CRT row: ##..##..##..##..##
     * End of cycle 18: finish executing addx 4 (Register X is now 21)
     * Sprite position: ....................###.................
     *
     * Start cycle  19: begin executing noop
     * During cycle 19: CRT draws pixel in position 18
     * Current CRT row: ##..##..##..##..##.
     * End of cycle 19: finish executing noop
     *
     * Start cycle  20: begin executing addx -1
     * During cycle 20: CRT draws pixel in position 19
     * Current CRT row: ##..##..##..##..##..
     *
     * During cycle 21: CRT draws pixel in position 20
     * Current CRT row: ##..##..##..##..##..#
     * End of cycle 21: finish executing addx -1 (Register X is now 20)
     * Sprite position: ...................###..................
     * </pre>
     * <p>
     * Allowing the program to run to completion causes the CRT to produce the following image:
     * <pre>
     * ##..##..##..##..##..##..##..##..##..##..
     * ###...###...###...###...###...###...###.
     * ####....####....####....####....####....
     * #####.....#####.....#####.....#####.....
     * ######......######......######......####
     * #######.......#######.......#######.....
     * </pre>
     * <p>
     * Render the image given by your program.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What eight capital letters appear on your CRT?
     */
    @Part(part = II)
    public String calculateAnswerForPart2(final SolutionContext context) {
        final CPU cpu = CPU.create(context);
        final CRT display = new CRT(40);

        cpu.process((tick, x) -> {
            final int col = (tick - 1) % 40;

            if (col < cpu.x - 1 || col > cpu.x + 1) display.skipPixel();
            else display.drawPixel();
        });

        return context.ocr().recognise(display.toGrid());
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A consumer for each tick.
     */
    private interface TickConsumer {

        /**
         * A consumer that receives a tick and the current value of the {@code X}
         * register.
         *
         * @param tick the current tick
         * @param x    the value of the {@code X} register
         */
        void accept(int tick, long x);

    }

    /*
     * A processor of `Instruction`s, which can perform an action every tick.
     */
    private static final class CPU {

        // Private Members

        private final Deque<Instruction> instructions;

        private long x = 1;

        // Constructors

        CPU(final Deque<Instruction> instructions) {
            this.instructions = instructions;
        }

        // Public Methods

        /**
         * Run the {@link Instruction instructions}, allowing an
         * {@link TickConsumer action} to be performed each tick, between
         * starting and completing an {@link Instruction}.
         *
         * @param onTick the {@link TickConsumer action} to be performed
         */
        public void process(final TickConsumer onTick) {
            Instruction currentInstruction = null;
            for (int i = 1; !instructions.isEmpty(); i++) {
                if (currentInstruction == null)
                    currentInstruction = instructions.removeFirst();

                onTick.accept(i, x);

                currentInstruction = currentInstruction.onTick(inc -> this.x += inc);
            }
        }

        // Static Helper Methods

        /**
         * Create a new {@link CPU}, reading the {@link Instruction Instructions}
         * from the given {@link SolutionContext}.
         *
         * @param context the {@link SolutionContext} to create from
         * @return the new {@link CPU}
         */
        static CPU create(final SolutionContext context) {
            final List<Instruction> instructions = context.process(Instruction::parse);
            return new CPU(new ArrayDeque<>(instructions));
        }

    }

    /*
     * A display that renders a single pixel each tick.
     */
    private static final class CRT {

        // Private Members

        private final int width;

        private final List<String> rows = new ArrayList<>();
        private final StringBuilder current = new StringBuilder();

        // Constructors

        CRT(final int width) {
            this.width = width;
        }

        // Modifiers

        /**
         * Draw a pixel (i.e make it lit).
         */
        public void drawPixel() {
            render('#');
        }

        /**
         * Skip a pixel (i.e. leave it dark).
         */
        public void skipPixel() {
            render('.');
        }

        // Helper Methods

        /**
         * Convert the rendered pixels in this {@link CRT CRT display} to a
         * {@link Grid}.
         *
         * @return the {@link Grid}
         */
        public Grid toGrid() {
            // Should never call this whilst mid-render, but just in case...
            final List<String> rows = current.isEmpty()
                    ? new ArrayList<>(this.rows)
                    : Stream.concat(this.rows.stream(), Stream.of(current.toString())).toList();

            return Grid.size(width, rows.size(), c -> rows.get(c.y()).charAt(c.x()) == '#' ? 1 : 0);
        }


        // Private Helper Methods

        /*
         * Render a single pixel, either lit (`#`) or dark (`.`).
         *
         * If this is the last pixel on the row, automatically move on to a new
         * row.
         */
        private void render(final char c) {
            current.append(c);
            if (current.length() == width)
                nextRow();
        }

        /*
         * Move the display on to a new row, resetting the pixel back to the
         * first column (i.e. column 0).
         */
        private void nextRow() {
            rows.add(current.toString());
            current.setLength(0);
        }

    }

    /*
     * An instruction to be executed by the `CPU`.
     */
    private abstract static class Instruction {

        // Private Members

        private int remainingCycles;

        // Constructors

        Instruction(final int remainingCycles) {
            this.remainingCycles = remainingCycles;
        }

        // Helper Methods

        public Instruction onTick(final IntConsumer onComplete) {
            --remainingCycles;
            if (remainingCycles == 0) {
                fireOnComplete(onComplete);
                return null;
            }
            return this;
        }

        // Abstract Methods

        protected abstract void fireOnComplete(IntConsumer onComplete);

        // Static Helper Methods

        static Instruction parse(final String line) {
            if (line.equals("noop")) return new NoOp();
            else if (line.startsWith("addx")) return new AddX(Integer.parseInt(line.substring(5)));
            else throw new IllegalStateException("Invalid instruction: '" + line + "'");
        }

    }

    private static final class NoOp extends Instruction {

        // Constructors

        NoOp() {
            super(1);
        }

        // Instruction Methods

        @Override
        protected void fireOnComplete(final IntConsumer onComplete) {

        }

    }

    private static final class AddX extends Instruction {

        // Private Members

        private final int value;

        // Constructors

        AddX(final int value) {
            super(2);
            this.value = value;
        }

        // Instruction Methods

        @Override
        protected void fireOnComplete(final IntConsumer onComplete) {
            onComplete.accept(value);
        }

    }

}

