package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 5: Supply Stacks.
 */
@Solution(year = 2022, day = 5, title = "Supply Stacks")
public class Day5 {

    /*
     * Matches a re-arrangement instruction of the form:
     *   "move n from X to Y".
     */
    private static final Pattern INSTRUCTION_REGEX = Pattern.compile("move ([0-9]+) from ([0-9]+) to ([0-9]+)");

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The expedition can depart as soon as the final supplies have been unloaded from the ships.
     * Supplies are stored in stacks of marked <em>crates</em>, but because the needed supplies are buried under many other crates, the crates need to be rearranged.
     * <p>
     * The ship has a <em>giant cargo crane</em> capable of moving crates between stacks.
     * To ensure none of the crates get crushed or fall over, the crane operator will rearrange them in a series of carefully-planned steps.
     * After the crates are rearranged, the desired crates will be at the top of each stack.
     * <p>
     * The Elves don't want to interrupt the crane operator during this delicate procedure, but they forgot to ask her <em>which</em> crate will end up where, and they want to be ready to unload them as soon as possible so they can embark.
     * <p>
     * They do, however, have a drawing of the starting stacks of crates <em>and</em> the rearrangement procedure (your puzzle input).
     * For example:
     * <pre>
     *     [D]
     * [N] [C]
     * [Z] [M] [P]
     *  1   2   3
     *
     * move 1 from 2 to 1
     * move 3 from 1 to 3
     * move 2 from 2 to 1
     * move 1 from 1 to 2
     * </pre>
     * <p>
     * In this example, there are three stacks of crates.
     * Stack 1 contains two crates: crate <code>Z</code> is on the bottom, and crate <code>N</code> is on top.
     * Stack 2 contains three crates; from bottom to top, they are crates <code>M</code>, <code>C</code>, and <code>D</code>.
     * Finally, stack 3 contains a single crate, <code>P</code>.
     * <p>
     * Then, the rearrangement procedure is given.
     * In each step of the procedure, a quantity of crates is moved from one stack to a different stack.
     * In the first step of the above rearrangement procedure, one crate is moved from stack 2 to stack 1, resulting in this configuration:
     * <pre>
     * [D]
     * [N] [C]
     * [Z] [M] [P]
     *  1   2   3
     * </pre>
     * <p>
     * In the second step, three crates are moved from stack 1 to stack 3.
     * Crates are moved <em>one at a time</em>, so the first crate to be moved (<code>D</code>) ends up below the second and third crates:
     * <pre>
     *         [Z]
     *         [N]
     *     [C] [D]
     *     [M] [P]
     *  1   2   3
     * </pre>
     * <p>
     * Then, both crates are moved from stack 2 to stack 1.
     * Again, because crates are moved <em>one at a time</em>, crate <code>C</code> ends up below crate <code>M</code>:
     * <pre>
     *         [Z]
     *         [N]
     * [M]     [D]
     * [C]     [P]
     *  1   2   3
     * </pre>
     * <p>
     * Finally, one crate is moved from stack 1 to stack 2:
     * <pre>
     *         [<em>Z</em>]
     *         [N]
     *         [D]
     * [<em>C</em>] [<em>M</em>] [P]
     *  1   2   3
     * </pre>
     * <p>
     * The Elves just need to know <em>which crate will end up on top of each stack</em>; in this example, the top crates are <code>C</code> in stack 1, <code>M</code> in stack 2, and <code>Z</code> in stack 3, so you should combine these together and give the Elves the message <code><em>CMZ</em></code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return After the rearrangement procedure completes, what crate ends up on top of each stack?
     */
    @Part(part = I)
    public String calculateAnswerForPart1(final SolutionContext context) {
        return new CrateMover9000().organiseCargo(context.stream());
    }

    /**
     * As you watch the crane operator expertly rearrange the crates, you notice the process isn't following your prediction.
     * <p>
     * Some mud was covering the writing on the side of the crane, and you quickly wipe it away.
     * The crane isn't a CrateMover 9000 - it's a <em><span title="It's way better than the old CrateMover 1006.">CrateMover 9001</span></em>.
     * <p>
     * The CrateMover 9001 is notable for many new and exciting features: air conditioning, leather seats, an extra cup holder, and <em>the ability to pick up and move multiple crates at once</em>.
     * <p>
     * Again considering the example above, the crates begin in the same configuration:
     * <pre>
     *     [D]
     * [N] [C]
     * [Z] [M] [P]
     *  1   2   3
     * </pre>
     * <p>
     * Moving a single crate from stack 2 to stack 1 behaves the same as before:
     * <pre>
     * [D]
     * [N] [C]
     * [Z] [M] [P]
     *  1   2   3
     * </pre>
     * <p>
     * However, the action of moving three crates from stack 1 to stack 3 means that those three moved crates <em>stay in the same order</em>, resulting in this new configuration:
     * <pre>
     *         [D]
     *         [N]
     *     [C] [Z]
     *     [M] [P]
     *  1   2   3
     * </pre>
     * <p>
     * Next, as both crates are moved from stack 2 to stack 1, they <em>retain their order</em> as well:
     * <pre>
     *         [D]
     *         [N]
     * [C]     [Z]
     * [M]     [P]
     *  1   2   3
     * </pre>
     * <p>
     * Finally, a single crate is still moved from stack 1 to stack 2, but now it's crate <code>C</code> that gets moved:
     * <pre>
     *         [<em>D</em>]
     *         [N]
     *         [Z]
     * [<em>M</em>] [<em>C</em>] [P]
     *  1   2   3
     * </pre>
     * <p>
     * In this example, the CrateMover 9001 has put the crates in a totally different order: <code><em>MCD</em></code>.
     * <p>
     * Before the rearrangement process finishes, update your simulation so that the Elves know where they should stand to be ready to unload the final supplies.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return After the rearrangement procedure completes, what crate ends up on top of each stack?
     */
    @Part(part = II)
    public String calculateAnswerForPart2(final SolutionContext context) {
        return new CrateMover9001().organiseCargo(context.stream());
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * A parsing phase.
     */
    private enum Phase {

        /**
         * The initial parsing phase, which involves interpreting the diagram
         * showing the layout of crates.
         */
        ASSEMBLE {
            @Override
            public Phase nextPhase() {
                return REARRANGE;
            }
        },

        /**
         * The subsequent parsing phase, which involves processing the
         * instructions for moving crates between stacks.
         */
        REARRANGE {
            @Override
            public Phase nextPhase() {
                throw new IllegalStateException("Cannot transition onwards from REARRANGE phase!");
            }
        },

        // End of constants
        ;

        public abstract Phase nextPhase();
    }

    /*
     * A system of assembling and rearranging crates.
     */
    private abstract static class CrateMover {

        // Private Members

        private Phase phase = Phase.ASSEMBLE;

        // Public Methods

        /**
         * Process the given input in two phases:
         * <ol>
         * <li>First: read in the diagram of crates;</li>
         * <li>Next: reorder the crates based on the given instructions.</li>
         * </ol>
         *
         * @param input the input lines
         * @return the top crate on each of the rearranged stacks.
         */
        public String organiseCargo(final Stream<String> input) {
            final List<Deque<Character>> stacks = new ArrayList<>();
            input.forEach(line -> {
                if (line.isBlank()) phase = phase.nextPhase();
                else if (phase == Phase.ASSEMBLE) assemble(stacks, line);
                else if (phase == Phase.REARRANGE) rearrange(stacks, line);
                else throw new IllegalStateException("In neither ASSEMBLE nor REARRANGE phase - aborting!");
            });

            return stacks.stream()
                    .map(Deque::removeFirst)
                    .map(Object::toString)
                    .collect(Collectors.joining());
        }

        // Protected Helper Methods

        /**
         * Rearrange one or more crates by removing them from the top of one
         * stack and placing them onto the top of another.
         *
         * @param stacks   the crate stacks
         * @param from     the stack to remove crates from
         * @param to       the stack to add crates to
         * @param quantity the quantity of crates to move
         */
        protected abstract void performRearrangement(List<Deque<Character>> stacks, int from, int to, int quantity);

        // Private Helper Methods

        /*
         * Process a given line by inserting any crates specified onto the
         * corresponding crate stack.
         */
        private void assemble(final List<Deque<Character>> stacks, final String line) {
            for (int i = 1; i < line.length(); i += 4) {
                if (Character.isAlphabetic(line.charAt(i))) {
                    while (stacks.size() <= i / 4) {
                        stacks.add(new ArrayDeque<>());
                    }

                    stacks.get(i / 4).addLast(line.charAt(i));
                }
            }
        }

        /*
         * Rearrange one or more crates by removing them from the top of one
         * stack and placing them onto the top of another.
         */
        private void rearrange(final List<Deque<Character>> stacks, final String line) {
            final Matcher matcher = INSTRUCTION_REGEX.matcher(line);
            if (!matcher.matches())
                throw new IllegalStateException("Invalid instruction: \"" + line + "\"");

            final int quantity = Integer.parseInt(matcher.group(1));
            final int from = Integer.parseInt(matcher.group(2));
            final int to = Integer.parseInt(matcher.group(3));

            performRearrangement(stacks, from, to, quantity);
        }

    }

    /*
     * Imitate a CrateMover9000 - this will move crates one at a time.
     */
    private static final class CrateMover9000 extends CrateMover {

        @Override
        protected void performRearrangement(
                final List<Deque<Character>> stacks,
                final int from,
                final int to,
                final int quantity
        ) {
            int remainingQuantity = quantity;
            while (remainingQuantity-- > 0) {
                final Character top = stacks.get(from - 1).removeFirst();
                stacks.get(to - 1).addFirst(top);
            }
        }

    }

    /*
     * Imitate a CrateMover9001 - this will move multiple crates at once.
     */
    private static final class CrateMover9001 extends CrateMover {

        @Override
        protected void performRearrangement(
                final List<Deque<Character>> stacks,
                final int from,
                final int to,
                final int quantity
        ) {
            final Deque<Character> interim = new ArrayDeque<>();
            int remainingQuantity = quantity;
            while (remainingQuantity-- > 0) {
                final Character top = stacks.get(from - 1).removeFirst();
                interim.addLast(top);
            }

            while (!interim.isEmpty()) {
                stacks.get(to - 1).addFirst(interim.removeLast());
            }
        }

    }

}
