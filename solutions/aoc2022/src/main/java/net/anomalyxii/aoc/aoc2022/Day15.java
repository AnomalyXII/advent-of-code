package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Bounds;
import net.anomalyxii.aoc.utils.geometry.Coordinate;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.lang.Math.max;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 15: Beacon Exclusion Zone.
 */
@Solution(year = 2022, day = 15, title = "Beacon Exclusion Zone")
public class Day15 {

    /*
     * Input RegExp for a `Sensor`.
     */
    private static final Pattern INPUT = Pattern.compile("Sensor at x=(-?[0-9]+), y=(-?[0-9]+): closest beacon is at x=(-?[0-9]+), y=(-?[0-9]+)");

    // ****************************************
    // Private Members
    // ****************************************

    private final int row;
    private final int minXY;
    private final int maxXY;

    // ****************************************
    // Constructors
    // ****************************************

    public Day15() {
        this(2000000, 0, 4000000);
    }

    Day15(final int row, final int minXY, final int maxXY) {
        this.row = row;
        this.minXY = minXY;
        this.maxXY = maxXY;
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You feel the ground rumble again as the distress signal leads you to a large network of subterranean tunnels.
     * You don't have time to search them all, but you don't need to: your pack contains a set of deployable <em>sensors</em> that you imagine were originally built to locate lost Elves.
     * <p>
     * The sensors aren't very powerful, but that's okay; your handheld device indicates that you're close enough to the source of the distress signal to use them.
     * You pull the emergency sensor system out of your pack, hit the big button on top, and the sensors zoom off down the tunnels.
     * <p>
     * Once a sensor finds a spot it thinks will give it a good reading, it attaches itself to a hard surface and begins monitoring for the nearest signal source <em>beacon</em>.
     * Sensors and beacons always exist at integer coordinates.
     * Each sensor knows its own position and can <em>determine the position of a beacon precisely</em>; however, sensors can only lock on to the one beacon <em>closest to the sensor</em> as measured by the <a href="https://en.wikipedia.org/wiki/Taxicab_geometry" target="_blank">Manhattan distance</a>.
     * (There is never a tie where two beacons are the same distance to a sensor.)
     * <p>
     * It doesn't take long for the sensors to report back their positions and closest beacons (your puzzle input).
     * For example:
     * <pre>
     * Sensor at x=2, y=18: closest beacon is at x=-2, y=15
     * Sensor at x=9, y=16: closest beacon is at x=10, y=16
     * Sensor at x=13, y=2: closest beacon is at x=15, y=3
     * Sensor at x=12, y=14: closest beacon is at x=10, y=16
     * Sensor at x=10, y=20: closest beacon is at x=10, y=16
     * Sensor at x=14, y=17: closest beacon is at x=10, y=16
     * Sensor at x=8, y=7: closest beacon is at x=2, y=10
     * Sensor at x=2, y=0: closest beacon is at x=2, y=10
     * Sensor at x=0, y=11: closest beacon is at x=2, y=10
     * Sensor at x=20, y=14: closest beacon is at x=25, y=17
     * Sensor at x=17, y=20: closest beacon is at x=21, y=22
     * Sensor at x=16, y=7: closest beacon is at x=15, y=3
     * Sensor at x=14, y=3: closest beacon is at x=15, y=3
     * Sensor at x=20, y=1: closest beacon is at x=15, y=3
     * </pre>
     * <p>
     * So, consider the sensor at <code>2,18</code>; the closest beacon to it is at <code>-2,15</code>.
     * For the sensor at <code>9,16</code>, the closest beacon to it is at <code>10,16</code>.
     * <p>
     * Drawing sensors as <code>S</code> and beacons as <code>B</code>, the above arrangement of sensors and beacons looks like this:
     * <pre>
     *                1    1    2    2
     *      0    5    0    5    0    5
     *  0 ....S.......................
     *  1 ......................S.....
     *  2 ...............S............
     *  3 ................SB..........
     *  4 ............................
     *  5 ............................
     *  6 ............................
     *  7 ..........S.......S.........
     *  8 ............................
     *  9 ............................
     * 10 ....B.......................
     * 11 ..S.........................
     * 12 ............................
     * 13 ............................
     * 14 ..............S.......S.....
     * 15 B...........................
     * 16 ...........SB...............
     * 17 ................S..........B
     * 18 ....S.......................
     * 19 ............................
     * 20 ............S......S........
     * 21 ............................
     * 22 .......................B....
     * </pre>
     * <p>
     * This isn't necessarily a comprehensive map of all beacons in the area, though.
     * Because each sensor only identifies its closest beacon, if a sensor detects a beacon, you know there are no other beacons that close or closer to that sensor.
     * There could still be beacons that just happen to not be the closest beacon to any sensor.
     * Consider the sensor at <code>8,7</code>:
     * <pre>
     *                1    1    2    2
     *      0    5    0    5    0    5
     * -2 ..........#.................
     * -1 .........###................
     *  0 ....S...#####...............
     *  1 .......#######........S.....
     *  2 ......#########S............
     *  3 .....###########SB..........
     *  4 ....#############...........
     *  5 ...###############..........
     *  6 ..#################.........
     *  7 .#########<em>S</em>#######S#........
     *  8 ..#################.........
     *  9 ...###############..........
     * 10 ....<em>B</em>############...........
     * 11 ..S..###########............
     * 12 ......#########.............
     * 13 .......#######..............
     * 14 ........#####.S.......S.....
     * 15 B........###................
     * 16 ..........#SB...............
     * 17 ................S..........B
     * 18 ....S.......................
     * 19 ............................
     * 20 ............S......S........
     * 21 ............................
     * 22 .......................B....
     * </pre>
     * <p>
     * This sensor's closest beacon is at <code>2,10</code>, and so you know there are no beacons that close or closer (in any positions marked <code>#</code>).
     * <p>
     * None of the detected beacons seem to be producing the distress signal, so you'll need to <span title="&quot;When you have eliminated all which is impossible, then whatever remains, however improbable, must be where the missing beacon is.&quot; - Sherlock Holmes">work out</span> where the distress beacon is by working out where it <em>isn't</em>.
     * For now, keep things simple by counting the positions where a beacon cannot possibly be along just a single row.
     * <p>
     * So, suppose you have an arrangement of beacons and sensors like in the example above and, just in the row where <code>y=10</code>, you'd like to count the number of positions a beacon cannot possibly exist.
     * The coverage from all sensors near that row looks like this:
     * <pre>
     *                  1    1    2    2
     *        0    5    0    5    0    5
     *  9 ...#########################...
     * <em>10 ..####B######################..</em>
     * 11 .###S#############.###########.
     * </pre>
     * <p>
     * In this example, in the row where <code>y=10</code>, there are <code><em>26</em></code> positions where a beacon cannot be present.
     * <p>
     * Consult the report from the sensors you just deployed.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return In the row where <code>y=2000000</code>, how many positions cannot contain a beacon?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<Sensor> sensors = loadSensorData(context);

        final long coverageScore = sensors.stream()
                .map(sensor -> sensor.findCoverageForRow(row))
                .flatMap(Optional::stream)
                .sorted()
                .collect(reduce())
                .mapToLong(Bounds::length)
                .sum();
        final long beaconsOnRow = sensors.stream()
                .map(sensor -> sensor.beacon)
                .filter(beacon -> beacon.y() == row)
                .distinct()
                .count();
        return coverageScore - beaconsOnRow;
    }

    /**
     * Your handheld device indicates that the distress signal is coming from a beacon nearby.
     * The distress beacon is not detected by any sensor, but the distress beacon must have <code>x</code> and <code>y</code> coordinates each no lower than <code>0</code> and no larger than <code>4000000</code>.
     * <p>
     * To isolate the distress beacon's signal, you need to determine its <em>tuning frequency</em>, which can be found by multiplying its <code>x</code> coordinate by <code>4000000</code> and then adding its <code>y</code> coordinate.
     * <p>
     * In the example above, the search space is smaller: instead, the <code>x</code> and <code>y</code> coordinates can each be at most <code>20</code>.
     * With this reduced search area, there is only a single position that could have a beacon: <code>x=14, y=11</code>.
     * The tuning frequency for this distress beacon is <code><em>56000011</em></code>.
     * <p>
     * Find the only possible position for the distress beacon.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is its tuning frequency?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<Sensor> sensors = loadSensorData(context);

        return IntStream.rangeClosed(minXY, maxXY)
                .mapToObj(y -> findMissingCoordinate(sensors, y))
                .flatMap(Optional::stream)
                .mapToLong(missing -> (missing.x() * 4000000L) + missing.y())
                .findFirst()
                .orElseThrow();
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Load the `Sensor` data.
     */
    private static List<Sensor> loadSensorData(final SolutionContext context) {
        return context.process(Sensor::parse);
    }

    /*
     * Determine the coverage of all sensors across a given row and return
     * the missing `Coordinate` if there is a gap.
     *
     * Behaviour is not well-defined if there is more than one missing
     * `Coordinate`!
     */
    private Optional<Coordinate> findMissingCoordinate(final List<Sensor> sensors, final int y) {
        return sensors.stream()
                .map(sensor -> sensor.findCoverageForRow(y))
                .flatMap(Optional::stream)
                .sorted()
                // This could theoretically be necessary, but in practice doesn't seem to be?
                //.filter(bounds -> bounds.max() >= minXY && bounds.min() <= maxXY)
                .collect(reduce())
                .skip(1) // Skip the first (and, normally, only) entry
                .findFirst() // Really "findSecond()"!!
                .map(bounds -> new Coordinate(bounds.min() - 1, y));
    }

    /*
     * Reduce a `Set` of `Bounds` into as few contiguous `Bounds` as
     * possible.
     */
    private Collector<? super Bounds, ?, Stream<Bounds>> reduce() {
        return new Collector<Bounds, Stream.Builder<Bounds>, Stream<Bounds>>() {

            private final BoundsReducer reducer = new BoundsReducer();

            @Override
            public Supplier<Stream.Builder<Bounds>> supplier() {
                return Stream::builder;
            }

            @Override
            public BiConsumer<Stream.Builder<Bounds>, Bounds> accumulator() {
                return reducer::accept;
            }

            @Override
            public BinaryOperator<Stream.Builder<Bounds>> combiner() {
                return reducer::combine;
            }

            @Override
            public Function<Stream.Builder<Bounds>, Stream<Bounds>> finisher() {
                return reducer::build;
            }

            @Override
            public Set<Collector.Characteristics> characteristics() {
                return Collections.emptySet();
            }
        };
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Represents a Sensor and corresponding Beacon.
     */
    private record Sensor(Coordinate location, Coordinate beacon) {

        // Helper Methods

        /*
         * Calculate the Manhattan Distance from this sensor to the detected
         * beacon.
         */
        int calculateManhattanDistance() {
            return location.calculateManhattanDistance(beacon);
        }

        /*
         * Find the sensor coverage for a given row.
         */
        Optional<Bounds> findCoverageForRow(final int y) {
            final int maxDistance = calculateManhattanDistance();

            final Coordinate startingPoint = new Coordinate(location.x(), y);
            final int distanceToStartingPoint = location.calculateManhattanDistance(startingPoint);
            if (distanceToStartingPoint > maxDistance)
                return Optional.empty(); // I think we don't care?

            final int dx = maxDistance - distanceToStartingPoint;
            final int minX = location.x() - dx;
            final int maxX = location.x() + dx;
            return Optional.of(new Bounds(minX, maxX));
        }

        // Static Helper Methods

        /**
         * Parse the {@link Sensor} data from the provided line.
         * <p>
         * The input is expected to be in the form:
         * <code>/Sensor at x=(-?[0-9]+), y=(-?[0-9]+): closest beacon is at x=(-?[0-9]+), y=(-?[0-9]+)/</code>
         *
         * @param line the input line
         * @return the {@link Sensor}
         * @throws IllegalArgumentException if the specified input line is invalid
         */
        static Sensor parse(final String line) {
            final Matcher matcher = INPUT.matcher(line);
            if (!matcher.matches())
                throw new IllegalArgumentException("Invalid line: '" + line + "'");

            final Coordinate sensor = new Coordinate(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)));
            final Coordinate beacon = new Coordinate(Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)));
            return new Sensor(sensor, beacon);
        }

    }

    /*
     * Utility class for reducing multiple `Bounds` into a `Stream`.
     */
    private static final class BoundsReducer {

        private int min = Integer.MAX_VALUE;
        private int max = Integer.MIN_VALUE;

        /*
         * Accept the next `Bounds` to be reduced.
         */
        private void accept(final Stream.Builder<Bounds> builder, final Bounds bounds) {
            if (min == Integer.MAX_VALUE && max == Integer.MIN_VALUE) {
                min = bounds.min();
                max = bounds.max();
                return;
            }

            if (bounds.min() <= (max + 1)) {
                max = max(max, bounds.max());
                return;
            }

            builder.add(new Bounds(min, max));

            min = bounds.min();
            max = bounds.max();
        }

        /*
         * Build the final `Stream` of reduced `Bounds`.
         */
        private Stream<Bounds> build(final Stream.Builder<Bounds> builder) {
            builder.add(new Bounds(min, max));
            return builder.build();
        }

        /*
         * Combine two partially reduced `Stream`s.
         *
         * Actually: throw because ugh.
         */
        private Stream.Builder<Bounds> combine(final Stream.Builder<Bounds> left, final Stream.Builder<Bounds> right) {
            throw new IllegalArgumentException("Should not need to combine!");
        }
    }


}

