package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;
import static net.anomalyxii.aoc.utils.geometry.Velocity.*;

/**
 * Day 24: Blizzard Basin.
 */
@Solution(year = 2022, day = 24, title = "Blizzard Basin")
public class Day24 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * With everything replanted for next year (and with elephants and monkeys to tend the grove), you and the Elves leave for the extraction point.
     * <p>
     * Partway up the mountain that shields the grove is a flat, open area that serves as the extraction point.
     * It's a bit of a climb, but nothing the expedition can't handle.
     * <p>
     * At least, that would normally be true; now that the mountain is covered in snow, things have become more difficult than the Elves are used to.
     * <p>
     * As the expedition reaches a valley that must be traversed to reach the extraction site, you find that strong, turbulent winds are pushing small <em>blizzards</em> of snow and sharp ice around the valley.
     * It's a good thing everyone packed warm clothes!
     * To make it across safely, you'll need to find a way to avoid them.
     * <p>
     * Fortunately, it's easy to see all of this from the entrance to the valley, so you make a map of the valley and the blizzards (your puzzle input).
     * For example:
     * <pre>
     * #.#####
     * #.....#
     * #&gt;....#
     * #.....#
     * #...v.#
     * #.....#
     * #####.#
     * </pre>
     * <p>
     * The walls of the valley are drawn as <code>#</code>; everything else is ground.
     * Clear ground - where there is currently no blizzard - is drawn as <code>.</code>.
     * Otherwise, blizzards are drawn with an arrow indicating their direction of motion: up (<code>^</code>), down (<code>v</code>), left (<code>&lt;</code>), or right (<code>&gt;</code>).
     * <p>
     * The above map includes two blizzards, one moving right (<code>&gt;</code>) and one moving down (<code>v</code>).
     * In one minute, each blizzard moves one position in the direction it is pointing:
     * <pre>
     * #.#####
     * #.....#
     * #.&gt;...#
     * #.....#
     * #.....#
     * #...v.#
     * #####.#
     * </pre>
     * <p>
     * Due to <span title="I think, anyway. Do I look like a theoretical blizzacist?">conservation of blizzard energy</span>, as a blizzard reaches the wall of the valley, a new blizzard forms on the opposite side of the valley moving in the same direction.
     * After another minute, the bottom downward-moving blizzard has been replaced with a new downward-moving blizzard at the top of the valley instead:
     * <pre>
     * #.#####
     * #...v.#
     * #..&gt;..#
     * #.....#
     * #.....#
     * #.....#
     * #####.#
     * </pre>
     * <p>
     * Because blizzards are made of tiny snowflakes, they pass right through each other.
     * After another minute, both blizzards temporarily occupy the same position, marked <code>2</code>:
     * <pre>
     * #.#####
     * #.....#
     * #...2.#
     * #.....#
     * #.....#
     * #.....#
     * #####.#
     * </pre>
     * <p>
     * After another minute, the situation resolves itself, giving each blizzard back its personal space:
     * <pre>
     * #.#####
     * #.....#
     * #....&gt;#
     * #...v.#
     * #.....#
     * #.....#
     * #####.#
     * </pre>
     * <p>
     * Finally, after yet another minute, the rightward-facing blizzard on the right is replaced with a new one on the left facing the same direction:
     * <pre>
     * #.#####
     * #.....#
     * #&gt;....#
     * #.....#
     * #...v.#
     * #.....#
     * #####.#
     * </pre>
     * <p>
     * This process repeats at least as long as you are observing it, but probably forever.
     * <p>
     * Here is a more complex example:
     * <pre>
     * #.######
     * #&gt;&gt;.&lt;^&lt;#
     * #.&lt;..&lt;&lt;#
     * #&gt;v.&gt;&lt;&gt;#
     * #&lt;^v^^&gt;#
     * ######.#
     * </pre>
     * <p>
     * Your expedition begins in the only non-wall position in the top row and needs to reach the only non-wall position in the bottom row.
     * On each minute, you can <em>move</em> up, down, left, or right, or you can <em>wait</em> in place.
     * You and the blizzards act <em>simultaneously</em>, and you cannot share a position with a blizzard.
     * <p>
     * In the above example, the fastest way to reach your goal requires <code><em>18</em></code> steps.
     * Drawing the position of the expedition as <code>E</code>, one way to achieve this is:
     * <pre>
     * Initial state:
     * #<em>E</em>######
     * #&gt;&gt;.&lt;^&lt;#
     * #.&lt;..&lt;&lt;#
     * #&gt;v.&gt;&lt;&gt;#
     * #&lt;^v^^&gt;#
     * ######.#
     *
     * Minute 1, move down:
     * #.######
     * #<em>E</em>&gt;3.&lt;.#
     * #&lt;..&lt;&lt;.#
     * #&gt;2.22.#
     * #&gt;v..^&lt;#
     * ######.#
     *
     * Minute 2, move down:
     * #.######
     * #.2&gt;2..#
     * #<em>E</em>^22^&lt;#
     * #.&gt;2.^&gt;#
     * #.&gt;..&lt;.#
     * ######.#
     *
     * Minute 3, wait:
     * #.######
     * #&lt;^&lt;22.#
     * #<em>E</em>2&lt;.2.#
     * #&gt;&lt;2&gt;..#
     * #..&gt;&lt;..#
     * ######.#
     *
     * Minute 4, move up:
     * #.######
     * #<em>E</em>&lt;..22#
     * #&lt;&lt;.&lt;..#
     * #&lt;2.&gt;&gt;.#
     * #.^22^.#
     * ######.#
     *
     * Minute 5, move right:
     * #.######
     * #2<em>E</em>v.&lt;&gt;#
     * #&lt;.&lt;..&lt;#
     * #.^&gt;^22#
     * #.2..2.#
     * ######.#
     *
     * Minute 6, move right:
     * #.######
     * #&gt;2<em>E</em>&lt;.&lt;#
     * #.2v^2&lt;#
     * #&gt;..&gt;2&gt;#
     * #&lt;....&gt;#
     * ######.#
     *
     * Minute 7, move down:
     * #.######
     * #.22^2.#
     * #&lt;v<em>E</em>&lt;2.#
     * #&gt;&gt;v&lt;&gt;.#
     * #&gt;....&lt;#
     * ######.#
     *
     * Minute 8, move left:
     * #.######
     * #.&lt;&gt;2^.#
     * #.<em>E</em>&lt;&lt;.&lt;#
     * #.22..&gt;#
     * #.2v^2.#
     * ######.#
     *
     * Minute 9, move up:
     * #.######
     * #&lt;<em>E</em>2&gt;&gt;.#
     * #.&lt;&lt;.&lt;.#
     * #&gt;2&gt;2^.#
     * #.v&gt;&lt;^.#
     * ######.#
     *
     * Minute 10, move right:
     * #.######
     * #.2<em>E</em>.&gt;2#
     * #&lt;2v2^.#
     * #&lt;&gt;.&gt;2.#
     * #..&lt;&gt;..#
     * ######.#
     *
     * Minute 11, wait:
     * #.######
     * #2^<em>E</em>^2&gt;#
     * #&lt;v&lt;.^&lt;#
     * #..2.&gt;2#
     * #.&lt;..&gt;.#
     * ######.#
     *
     * Minute 12, move down:
     * #.######
     * #&gt;&gt;.&lt;^&lt;#
     * #.&lt;<em>E</em>.&lt;&lt;#
     * #&gt;v.&gt;&lt;&gt;#
     * #&lt;^v^^&gt;#
     * ######.#
     *
     * Minute 13, move down:
     * #.######
     * #.&gt;3.&lt;.#
     * #&lt;..&lt;&lt;.#
     * #&gt;2<em>E</em>22.#
     * #&gt;v..^&lt;#
     * ######.#
     *
     * Minute 14, move right:
     * #.######
     * #.2&gt;2..#
     * #.^22^&lt;#
     * #.&gt;2<em>E</em>^&gt;#
     * #.&gt;..&lt;.#
     * ######.#
     *
     * Minute 15, move right:
     * #.######
     * #&lt;^&lt;22.#
     * #.2&lt;.2.#
     * #&gt;&lt;2&gt;<em>E</em>.#
     * #..&gt;&lt;..#
     * ######.#
     *
     * Minute 16, move right:
     * #.######
     * #.&lt;..22#
     * #&lt;&lt;.&lt;..#
     * #&lt;2.&gt;&gt;<em>E</em>#
     * #.^22^.#
     * ######.#
     *
     * Minute 17, move down:
     * #.######
     * #2.v.&lt;&gt;#
     * #&lt;.&lt;..&lt;#
     * #.^&gt;^22#
     * #.2..2<em>E</em>#
     * ######.#
     *
     * Minute 18, move down:
     * #.######
     * #&gt;2.&lt;.&lt;#
     * #.2v^2&lt;#
     * #&gt;..&gt;2&gt;#
     * #&lt;....&gt;#
     * ######<em>E</em>#
     * </pre>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the fewest number of minutes required to avoid the blizzards and reach the goal?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Map result = extractMap(context);

        return result.blizzards().navigate(result.entrance(), result.extraction(), 0);
    }

    /**
     * As the expedition reaches the far side of the valley, one of the Elves looks especially dismayed:
     * <p>
     * He <em>forgot his snacks</em> at the entrance to the valley!
     * <p>
     * Since you're so good at dodging blizzards, the Elves humbly request that you go back for his snacks.
     * From the same initial conditions, how quickly can you make it from the start to the goal, then back to the start, then back to the goal?
     * <p>
     * In the above example, the first trip to the goal takes <code>18</code> minutes, the trip back to the start takes <code>23</code> minutes, and the trip back to the goal again takes <code>13</code> minutes, for a total time of <code><em>54</em></code> minutes.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the fewest number of minutes required to reach the goal, go back to the start, then reach the goal again?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Grid grid = context.readGrid();

        final Coordinate entrance = new Coordinate(1, 0); // TODO: maybe don't hardcode this...
        assert grid.get(entrance) == '.';

        final Coordinate extraction = new Coordinate(grid.width() - 2, grid.height() - 1); // TODO: maybe don't hardcode this...
        assert grid.get(extraction) == '.';

        final Blizzards blizzards = Blizzards.fromGrid(grid);

        final int there = blizzards.navigate(entrance, extraction, 0);
        final int back = blizzards.navigate(extraction, entrance, there);
        return blizzards.navigate(entrance, extraction, back);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Parse the input to create a `Map` containing the locations of all
     * `Blizzards`, plus the entrance and the exit.
     */
    private static Map extractMap(final SolutionContext context) {
        final Grid grid = context.readGrid();

        final Coordinate entrance = new Coordinate(1, 0); // TODO: maybe don't hardcode this...
        assert grid.get(entrance) == '.';

        final Coordinate extraction = new Coordinate(grid.width() - 2, grid.height() - 1); // TODO: maybe don't hardcode this...
        assert grid.get(extraction) == '.';

        final Blizzards blizzards = Blizzards.fromGrid(grid);
        return new Map(entrance, extraction, blizzards);
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * Shows the locations of the entrance, exit and all blizzard.
     */
    private record Map(Coordinate entrance, Coordinate extraction, Blizzards blizzards) {

    }

    /*
     * A blizzard, localised to a single `Coordinate` and moving with a
     * particular `Velocity`.
     */
    private record Blizzard(Velocity direction, Coordinate position) {

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final Blizzard blizzard = (Blizzard) o;
            return Objects.equals(direction, blizzard.direction) && Objects.equals(position, blizzard.position);
        }

        @Override
        public int hashCode() {
            return Objects.hash(direction, position);
        }

    }

    /*
     * All the localised `Blizzard`s that need to be avoided.
     */
    private record Blizzards(Grid grid, Area arena, Set<Blizzard> blizzards) {

        private static final int NORTH_WIND = '^';
        private static final int EAST_WIND = '>';
        private static final int SOUTH_WIND = 'v';
        private static final int WEST_WIND = '<';

        // Helper Methods

        /**
         * Navigate from the entrance {@link Coordinate} to the extraction
         * {@link Coordinate}, avoiding every {@link Blizzard} on the way.
         *
         * @param entrance   the entrance {@link Coordinate}
         * @param extraction the extraction {@link Coordinate}
         * @param time       the current time
         * @return the number of steps needed
         */
        int navigate(final Coordinate entrance, final Coordinate extraction, final int time) {
            final Set<Coordinate> toCheck = new HashSet<>();
            toCheck.add(entrance);

            int t = time;
            while (!toCheck.isEmpty()) {

                final Set<Coordinate> impasses = blizzardsGonnaBlowBlowBlow(t);
                final Set<Coordinate> nextStates = new HashSet<>();

                final Iterator<Coordinate> iterator = toCheck.iterator();
                while (iterator.hasNext()) {
                    final Coordinate current = iterator.next();
                    iterator.remove();

                    if (current.equals(extraction)) return t - 1;

                    // Can we stay where we are?
                    if (!impasses.contains(current))
                        nextStates.add(current);

                    current.adjacent()
                            .filter(grid::contains)
                            .filter(next -> grid.get(next) != '#')
                            .filter(next -> !impasses.contains(next))
                            .forEach(nextStates::add);
                }

                ++t;
                toCheck.addAll(nextStates);
            }

            throw new IllegalStateException("Ran out of moves without reaching the extraction point!");
        }

        // Private Helper Methods

        /*
         * Determine the position of every `Blizzard` after a given number of
         * turns.
         */
        private Set<Coordinate> blizzardsGonnaBlowBlowBlow(final int turn) {
            final Set<Coordinate> current = new HashSet<>();
            for (final Blizzard blizzard : this.blizzards) {
                Coordinate next = blizzard.position.adjustBy(new Velocity(turn * blizzard.direction.h(), turn * blizzard.direction.v()));

                // TODO: this can probably be optimised?
                while (!arena.contains(next)) {
                    next = next.adjustBy(new Velocity(arena.width() * -blizzard.direction.h(), arena.height() * -blizzard.direction.v()));
                }

                assert arena.contains(next);
                current.add(next);
            }
            return current;
        }

        // Static Helper Methods

        /**
         * Create a new map of the {@link Blizzards} from the given input.
         *
         * @param grid the {@link Grid}
         * @return the {@link Blizzards}
         */
        public static Blizzards fromGrid(final Grid grid) {
            final Set<Blizzard> blizzards = grid.entries()
                    .filter(e -> e.getValue() == NORTH_WIND || e.getValue() == EAST_WIND || e.getValue() == SOUTH_WIND || e.getValue() == WEST_WIND)
                    .map(e -> switch (e.getValue()) {
                        case NORTH_WIND -> new Blizzard(UP, e.getKey());
                        case EAST_WIND -> new Blizzard(RIGHT, e.getKey());
                        case SOUTH_WIND -> new Blizzard(DOWN, e.getKey());
                        case WEST_WIND -> new Blizzard(LEFT, e.getKey());
                        default -> throw new IllegalArgumentException("Invalid wind direction: " + e.getValue());
                    })
                    .collect(Collectors.toSet());

            final Area arena = Area.of(
                    Bounds.of(grid.min().x() + 1, grid.max().x() - 1),
                    Bounds.of(grid.min().y() + 1, grid.max().y() - 1)
            );

            return new Blizzards(grid, arena, blizzards);
        }

    }

}

