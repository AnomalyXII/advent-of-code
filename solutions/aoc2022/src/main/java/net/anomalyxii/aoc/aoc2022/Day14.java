package net.anomalyxii.aoc.aoc2022;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.utils.geometry.Coordinate;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 14: Regolith Reservoir.
 */
@Solution(year = 2022, day = 14, title = "Regolith Reservoir")
public class Day14 {

    /*
     * The entry point of the sand: (500,0).
     */
    private static final Coordinate START = new Coordinate(500, 0);

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The distress signal leads you to a giant waterfall!
     * Actually, hang on - the signal seems like it's coming from the waterfall itself, and that doesn't make any sense.
     * However, you do notice a little path that leads <em>behind</em> the waterfall.
     * <p>
     * Correction: the distress signal leads you behind a giant waterfall!
     * There seems to be a large cave system here, and the signal definitely leads further inside.
     * <p>
     * As you begin to make your way deeper underground, you feel the ground rumble for a moment.
     * Sand begins pouring into the cave! If you don't quickly figure out where the sand is going, you could quickly become trapped!
     * <p>
     * Fortunately, your <a href="/2018/day/17">familiarity</a> with analyzing the path of falling material will come in handy here.
     * You scan a two-dimensional vertical slice of the cave above you (your puzzle input) and discover that it is mostly <em>air</em> with structures made of <em>rock</em>.
     * <p>
     * Your scan traces the path of each solid rock structure and reports the <code>x,y</code> coordinates that form the shape of the path, where <code>x</code> represents distance to the right and <code>y</code> represents distance down.
     * Each path appears as a single line of text in your scan.
     * After the first point of each path, each point indicates the end of a straight horizontal or vertical line to be drawn from the previous point.
     * For example:
     * <pre>
     * 498,4 -&gt; 498,6 -&gt; 496,6
     * 503,4 -&gt; 502,4 -&gt; 502,9 -&gt; 494,9
     * </pre>
     * <p>
     * This scan means that there are two paths of rock; the first path consists of two straight lines, and the second path consists of three straight lines.
     * (Specifically, the first path consists of a line of rock from <code>498,4</code> through <code>498,6</code> and another line of rock from <code>498,6</code> through <code>496,6</code>.)
     * <p>
     * The sand is pouring into the cave from point <code>500,0</code>.
     * <p>
     * Drawing rock as <code>#</code>, air as <code>.</code>, and the source of the sand as <code>+</code>, this becomes:
     * <pre>
     *   4     5  5
     *   9     0  0
     *   4     0  3
     * 0 ......+...
     * 1 ..........
     * 2 ..........
     * 3 ..........
     * 4 ....#...##
     * 5 ....#...#.
     * 6 ..###...#.
     * 7 ........#.
     * 8 ........#.
     * 9 #########.
     * </pre>
     * <p>
     * Sand is produced <em>one unit at a time</em>, and the next unit of sand is not produced until the previous unit of sand <em>comes to rest</em>.
     * A unit of sand is large enough to fill one tile of air in your scan.
     * <p>
     * A unit of sand always falls <em>down one step</em> if possible.
     * If the tile immediately below is blocked (by rock or sand), the unit of sand attempts to instead move diagonally <em>one step down and to the left</em>.
     * If that tile is blocked, the unit of sand attempts to instead move diagonally <em>one step down and to the right</em>.
     * Sand keeps moving as long as it is able to do so, at each step trying to move down, then down-left, then down-right.
     * If all three possible destinations are blocked, the unit of sand <em>comes to rest</em> and no longer moves, at which point the next unit of sand is created back at the source.
     * <p>
     * So, drawing sand that has come to rest as <code>o</code>, the first unit of sand simply falls straight down and then stops:
     * <pre>
     * ......+...
     * ..........
     * ..........
     * ..........
     * ....#...##
     * ....#...#.
     * ..###...#.
     * ........#.
     * ......<em>o</em>.#.
     * #########.
     * </pre>
     * <p>
     * The second unit of sand then falls straight down, lands on the first one, and then comes to rest to its left:
     * <pre>
     * ......+...
     * ..........
     * ..........
     * ..........
     * ....#...##
     * ....#...#.
     * ..###...#.
     * ........#.
     * .....oo.#.
     * #########.
     * </pre>
     * <p>
     * After a total of five units of sand have come to rest, they form this pattern:
     * <pre>
     * ......+...
     * ..........
     * ..........
     * ..........
     * ....#...##
     * ....#...#.
     * ..###...#.
     * ......o.#.
     * ....oooo#.
     * #########.
     * </pre>
     * <p>
     * After a total of 22 units of sand:
     * <pre>
     * ......+...
     * ..........
     * ......o...
     * .....ooo..
     * ....#ooo##
     * ....#ooo#.
     * ..###ooo#.
     * ....oooo#.
     * ...ooooo#.
     * #########.
     * </pre>
     * <p>
     * Finally, only two more units of sand can possibly come to rest:
     * <pre>
     * ......+...
     * ..........
     * ......o...
     * .....ooo..
     * ....#ooo##
     * ...<em>o</em>#ooo#.
     * ..###ooo#.
     * ....oooo#.
     * .<em>o</em>.ooooo#.
     * #########.
     * </pre>
     * <p>
     * Once all <code><em>24</em></code> units of sand shown above have come to rest, all further sand flows out the bottom, falling into the endless void.
     * Just for fun, the path any new sand takes before falling forever is shown here with <code>~</code>:
     * <pre>
     * .......+...
     * .......~...
     * ......~o...
     * .....~ooo..
     * ....~#ooo##
     * ...~o#ooo#.
     * ..~###ooo#.
     * ..~..oooo#.
     * .~o.ooooo#.
     * ~#########.
     * ~..........
     * ~..........
     * ~..........
     * </pre>
     * <p>
     * Using your scan, simulate the falling sand.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many units of sand come to rest before sand starts flowing into the abyss below?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Set<Coordinate> walls = context.stream()
                .flatMap(Day14::buildTheWall)
                .collect(Collectors.toSet());

        final int lowestWall = walls.stream()
                .mapToInt(Coordinate::y)
                .max()
                .orElseThrow();

        int count = 0;
        toInfinityAndBeyond:
        while (true) {
            Coordinate grain = START;

            Coordinate next;
            while ((next = fall(grain, n -> isOpenSpace(n, walls))) != null) {
                if (grain.y() >= lowestWall)
                    break toInfinityAndBeyond;

                grain = next;
            }

            // Can't move any more...
            ++count;
            walls.add(grain);
        }

        return count;
    }

    /**
     * You realize you misread the scan.
     * There isn't an <span title="Endless Void is my C cover band.">endless void</span> at the bottom of the scan - there's floor, and you're standing on it!
     * <p>
     * You don't have time to scan the floor, so assume the floor is an infinite horizontal line with a <code>y</code> coordinate equal to <em>two plus the highest <code>y</code> coordinate</em> of any point in your scan.
     * <p>
     * In the example above, the highest <code>y</code> coordinate of any point is <code>9</code>, and so the floor is at <code>y=11</code>.
     * (This is as if your scan contained one extra rock path like <code>-infinity,11 -&gt; infinity,11</code>.)
     * With the added floor, the example above now looks like this:
     * <pre>
     *         ...........+........
     *         ....................
     *         ....................
     *         ....................
     *         .........#...##.....
     *         .........#...#......
     *         .......###...#......
     *         .............#......
     *         .............#......
     *         .....#########......
     *         ....................
     * &lt;-- etc #################### etc --&gt;
     * </pre>
     * <p>
     * To find somewhere safe to stand, you'll need to simulate falling sand until a unit of sand comes to rest at <code>500,0</code>, blocking the source entirely and stopping the flow of sand into the cave.
     * In the example above, the situation finally looks like this after <code><em>93</em></code> units of sand come to rest:
     * <pre>
     * ............o............
     * ...........ooo...........
     * ..........ooooo..........
     * .........ooooooo.........
     * ........oo#ooo##o........
     * .......ooo#ooo#ooo.......
     * ......oo###ooo#oooo......
     * .....oooo.oooo#ooooo.....
     * ....oooooooooo#oooooo....
     * ...ooo#########ooooooo...
     * ..ooooo.......ooooooooo..
     * #########################
     * </pre>
     * <p>
     * Using your scan, simulate the falling sand until the source of the sand becomes blocked.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many units of sand come to rest?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Set<Coordinate> walls = context.stream()
                .flatMap(Day14::buildTheWall)
                .collect(Collectors.toSet());

        final int groundLevel = walls.stream()
                .mapToInt(Coordinate::y)
                .max()
                .orElseThrow() + 2;

        int count = 0;
        while (!walls.contains(START)) {
            Coordinate grain = START;
            Coordinate next;
            while ((next = fall(grain, n -> isOpenSpace(n, walls, groundLevel))) != null)
                grain = next;

            // Can't move any more...
            ++count;
            walls.add(grain);
        }

        return count;
    }


    // ****************************************
    // Test Methods
    // ****************************************

    /*
     * Build a wall from a chain of `Coordinate`s.
     */
    private static Stream<Coordinate> buildTheWall(final String line) {
        final List<Coordinate> coords = stream(line.split("\\s*->\\s*"))
                .map(Coordinate::parse)
                .toList();
        final Stream.Builder<Coordinate> builder = Stream.builder();
        for (int i = 1; i < coords.size(); i++) {
            Coordinate prev = coords.get(i - 1);
            final Coordinate next = coords.get(i);

            final int dx = prev.y() == next.y() ? prev.x() < next.x() ? 1 : -1 : 0;
            final int dy = prev.x() == next.x() ? prev.y() < next.y() ? 1 : -1 : 0;
            if (dx == dy)
                throw new IllegalStateException("Cannot model a diagonal wall!");

            while (!prev.equals(next)) {
                builder.add(prev);
                prev = prev.adjustBy(dx, dy);
            }
        }
        builder.add(coords.get(coords.size() - 1));
        return builder.build();
    }

    private Coordinate fall(final Coordinate grain, final Predicate<Coordinate> isOpenSpace) {
        Coordinate next;

        // Directly down...
        next = grain.adjustBy(0, 1);
        if (isOpenSpace.test(next))
            return next;

        // Down and left...
        next = grain.adjustBy(-1, 1);
        if (isOpenSpace.test(next))
            return next;

        // Down and right...
        next = grain.adjustBy(1, 1);
        if (isOpenSpace.test(next))
            return next;

        return null;
    }

    /*
     * Check if the target `Coordinate` is not blocked by a wall.
     */
    private static boolean isOpenSpace(final Coordinate target, final Set<Coordinate> walls) {
        return !walls.contains(target);
    }

    /*
     * Check if the target `Coordinate` is not blocked by a wall or the floor.
     */
    private static boolean isOpenSpace(final Coordinate down, final Set<Coordinate> walls, final int groundLevel) {
        return !walls.contains(down) && down.y() != groundLevel;
    }

}

