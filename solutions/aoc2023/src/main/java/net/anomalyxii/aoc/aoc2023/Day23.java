package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Direction;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.*;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 23: A Long Walk.
 */
@Solution(year = 2023, day = 23, title = "A Long Walk")
public class Day23 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The Elves resume water filtering operations!
     * Clean water starts flowing over the edge of Island Island.
     * <p>
     * They offer to help <em>you</em> go over the edge of Island Island, too!
     * Just <span title="It'll be fiiiiiiiine.">hold on tight</span> to one end of this impossibly long rope and they'll lower you down a safe distance from the massive waterfall you just created.
     * <p>
     * As you finally reach Snow Island, you see that the water isn't really reaching the ground: it's being <em>absorbed by the air</em> itself.
     * It looks like you'll finally have a little downtime while the moisture builds up to snow-producing levels.
     * Snow Island is pretty scenic, even without any snow; why not take a walk?
     * <p>
     * There's a map of nearby hiking trails (your puzzle input) that indicates <em>paths</em> (<code>.</code>), <em>forest</em> (<code>#</code>), and steep <em>slopes</em> (<code>^</code>, <code>&gt;</code>, <code>v</code>, and <code>&lt;</code>).
     * <p>
     * For example:
     * <pre>
     * #.#####################
     * #.......#########...###
     * #######.#########.#.###
     * ###.....#.>.>.###.#.###
     * ###v#####.#v#.###.#.###
     * ###.>...#.#.#.....#...#
     * ###v###.#.#.#########.#
     * ###...#.#.#.......#...#
     * #####.#.#.#######.#.###
     * #.....#.#.#.......#...#
     * #.#####.#.#.#########v#
     * #.#...#...#...###...>.#
     * #.#.#v#######v###.###v#
     * #...#.>.#...>.>.#.###.#
     * #####v#.#.###v#.#.###.#
     * #.....#...#...#.#.#...#
     * #.#########.###.#.#.###
     * #...###...#...#...#.###
     * ###.###.#.###v#####v###
     * #...#...#.#.>.>.#.>.###
     * #.###.###.#.###.#.#v###
     * #.....###...###...#...#
     * #####################.#
     * </pre>
     * <p>
     * You're currently on the single path tile in the top row; your goal is to reach the single path tile in the bottom row.
     * Because of all the mist from the waterfall, the slopes are probably quite <em>icy</em>; if you step onto a slope tile, your next step must be <em>downhill</em> (in the direction the arrow is pointing).
     * To make sure you have the most scenic hike possible, <em>never step onto the same tile twice</em>.
     * What is the longest hike you can take?
     * <p>
     * In the example above, the longest hike you can take is marked with <code>O</code>, and your starting position is marked <code>S</code>:
     * <pre>
     * #S#####################
     * #OOOOOOO#########...###
     * #######O#########.#.###
     * ###OOOOO#OOO>.###.#.###
     * ###O#####O#O#.###.#.###
     * ###OOOOO#O#O#.....#...#
     * ###v###O#O#O#########.#
     * ###...#O#O#OOOOOOO#...#
     * #####.#O#O#######O#.###
     * #.....#O#O#OOOOOOO#...#
     * #.#####O#O#O#########v#
     * #.#...#OOO#OOO###OOOOO#
     * #.#.#v#######O###O###O#
     * #...#.>.#...>OOO#O###O#
     * #####v#.#.###v#O#O###O#
     * #.....#...#...#O#O#OOO#
     * #.#########.###O#O#O###
     * #...###...#...#OOO#O###
     * ###.###.#.###v#####O###
     * #...#...#.#.>.>.#.>O###
     * #.###.###.#.###.#.#O###
     * #.....###...###...#OOO#
     * #####################O#
     * </pre>
     * <p>
     * This hike contains <code><em>94</em></code> steps.
     * (The other possible hikes you could have taken were <code>90</code>, <code>86</code>, <code>82</code>, <code>82</code>, and <code>74</code> steps long.)
     * <p>
     * Find the longest hike you can take through the hiking trails listed on your map.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many steps long is the longest hike?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        return findLongestPath(context, false);
    }

    /**
     * As you reach the trailhead, you realize that the ground isn't as slippery as you expected; you'll have <em>no problem</em> climbing up the steep slopes.
     * <p>
     * Now, treat all <em>slopes</em> as if they were normal <em>paths</em> (<code>.</code>).
     * You still want to make sure you have the most scenic hike possible, so continue to ensure that you <em>never step onto the same tile twice</em>.
     * What is the longest hike you can take?
     * <p>
     * In the example above, this increases the longest hike to <code><em>154</em></code> steps:
     * <pre>
     * #S#####################
     * #OOOOOOO#########OOO###
     * #######O#########O#O###
     * ###OOOOO#.>OOO###O#O###
     * ###O#####.#O#O###O#O###
     * ###O>...#.#O#OOOOO#OOO#
     * ###O###.#.#O#########O#
     * ###OOO#.#.#OOOOOOO#OOO#
     * #####O#.#.#######O#O###
     * #OOOOO#.#.#OOOOOOO#OOO#
     * #O#####.#.#O#########O#
     * #O#OOO#...#OOO###...>O#
     * #O#O#O#######O###.###O#
     * #OOO#O>.#...>O>.#.###O#
     * #####O#.#.###O#.#.###O#
     * #OOOOO#...#OOO#.#.#OOO#
     * #O#########O###.#.#O###
     * #OOO###OOO#OOO#...#O###
     * ###O###O#O###O#####O###
     * #OOO#OOO#O#OOO>.#.>O###
     * #O###O###O#O###.#.#O###
     * #OOOOO###OOO###...#OOO#
     * #####################O#
     * </pre>
     * <p>
     * Find the longest hike you can take through the surprisingly dry hiking trails listed on your map.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many steps long is the longest hike?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        return findLongestPath(context, true);
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final Grid map = context.readGrid();

        final Coordinate start = IntStream.range(0, map.width())
                .mapToObj(x -> new Coordinate(x, 0))
                .filter(x -> map.get(x) == '.')
                .findFirst()
                .orElseThrow();

        final Coordinate end = IntStream.range(0, map.width())
                .mapToObj(x -> new Coordinate(x, map.height() - 1))
                .filter(x -> map.get(x) == '.')
                .findFirst()
                .orElseThrow();

        return new IntTuple(
                findLongestPath(map, start, end, false),
                findLongestPath(map, start, end, true)
        );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Find the _longest_ path through the forest.
     */
    private static int findLongestPath(
            final SolutionContext context,
            final boolean ignoreSlopes
    ) {
        final Grid map = context.readGrid();

        final Coordinate start = IntStream.range(0, map.width())
                .mapToObj(x -> new Coordinate(x, 0))
                .filter(x -> map.get(x) == '.')
                .findFirst()
                .orElseThrow();

        final Coordinate end = IntStream.range(0, map.width())
                .mapToObj(x -> new Coordinate(x, map.height() - 1))
                .filter(x -> map.get(x) == '.')
                .findFirst()
                .orElseThrow();

        return findLongestPath(map, start, end, ignoreSlopes);
    }

    /*
     * Find the longest path between two points in the forest.
     */
    private static int findLongestPath(
            final Grid map,
            final Coordinate start,
            final Coordinate end,
            final boolean ignoreSlopes
    ) {
        final Map<Coordinate, Junction> junctions = findJunctions(map);
        connectJunctions(map, ignoreSlopes, junctions);

        final Junction startJunc = junctions.get(start);
        final Junction endJunc = junctions.get(end);
        return startJunc.findLongestPathTo(endJunc);
    }

    /*
     * Find all the junctions in the forest.
     */
    private static Map<Coordinate, Junction> findJunctions(final Grid map) {
        final LongSupplier idProvider = new IdSupplier();

        return map.stream()
                .filter(coord -> {
                    if (map.get(coord) == '#') return false;
                    final int exits = map.adjacentTo(coord)
                            .mapToInt(map::get)
                            .filter(type -> type != '#')
                            .map(x -> 1)
                            .sum();

                    // exits == 2 => path
                    // exits == 1 => start / finish

                    return exits != 2;
                })
                .collect(Collectors.toMap(
                        Function.identity(),
                        point -> new Junction(idProvider.getAsLong(), point)
                ));
    }

    /*
     * Traverse the various forest paths to work out which `Junction`s are
     * connected and how many steps it takes to reach them.
     */
    private static void connectJunctions(final Grid map, final boolean ignoreSlopes, final Map<Coordinate, Junction> junctions) {
        junctions.values().forEach(junction -> connectJunction(map, junction, ignoreSlopes, junctions));
    }

    /*
     * Traverse a forest path to work out which `Junction`s are connected to
     * it and how many steps it takes to reach them.
     */
    private static void connectJunction(
            final Grid map,
            final Junction junction,
            final boolean ignoreSlopes,
            final Map<Coordinate, Junction> junctions
    ) {
        Direction.stream()
                .map(startDirection -> walkUntilJunction(map, ignoreSlopes, junctions, junction, startDirection))
                .forEach(path -> {
                    if (path == null) return;

                    final Junction connection = junctions.get(path.removeLast());
                    assert connection != null;
                    junction.addEdge(connection, path.size());
                });
    }

    /*
     * Keep walking along a forest path until reaching a `Junction`.
     */
    private static SequencedSet<Coordinate> walkUntilJunction(
            final Grid map,
            final boolean ignoreSlopes,
            final Map<Coordinate, Junction> junctions,
            final Junction junction,
            final Direction startDirection
    ) {
        final SequencedSet<Coordinate> path = new LinkedHashSet<>();
        path.add(junction.point);

        Coordinate head = junction.point.adjustBy(startDirection);
        if (!map.contains(head) || map.get(head) == '#')
            return null;

        do {
            path.add(head);

            final Direction slop = ignoreSlopes ? null : Direction.fromChar(map.get(head));
            head = walk(map, head, slop, path);

            if (junctions.containsKey(head)) {
                path.add(head);
                return path;
            }
        } while (head != null);

        return null;
    }

    /*
     * Go forward one space.
     */
    private static Coordinate walk(final Grid map, final Coordinate head, final Direction slope, final Set<Coordinate> path) {
        return Direction.stream()
                .filter(direction -> slope == null || slope == direction)
                .map(head::adjustBy)
                .filter(coord -> map.contains(coord) && map.get(coord) != '#' && !path.contains(coord))
                .findFirst()
                .orElse(null);

    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Represent a crossroads within the forest.
     */
    private static class Junction {

        // Private Members

        private final long id;
        private final Coordinate point;
        private final Map<Junction, Integer> edges = new HashMap<>();

        // Constructors

        Junction(final long id, final Coordinate point) {
            this.id = id;
            this.point = point;
        }

        // Helper Methods

        /*
         * Add a (directed) connection between this and another `Junction`.
         */
        void addEdge(final Junction coordinate, final int length) {
            edges.put(coordinate, length);
        }

        /*
         * Find the longest path from here to the specified target `Junction`.
         */
        int findLongestPathTo(final Junction target) {
            return findLongestPathTo(target, 0);
        }

        // Private Helper Methods

        /*
         * Find the longest path from here to the specified target `Junction`,
         * ensuring that the route does not double back on itself.
         */
        int findLongestPathTo(final Junction endJunc, final long visited) {
            if (this == endJunc) return 0;

            final long newVisited = visited | id;
            return edges.entrySet().stream()
                    .filter(entry -> (newVisited & entry.getKey().id) == 0)
                    .mapToInt(entry -> entry.getKey().findLongestPathTo(endJunc, newVisited) + entry.getValue())
                    .reduce(
                            Integer.MIN_VALUE,
                            Math::max
                    );
        }

    }

    /*
     * Supplier of an ID for a `Junction`.
     */
    private static final class IdSupplier implements LongSupplier {

        // Private Members
        private long next = 1;

        // LongSupplier Methods

        @Override
        public long getAsLong() {
            final long id = next;
            next <<= 1;
            assert next > 0;
            return id;
        }

    }

}

