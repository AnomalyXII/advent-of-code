package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;
import net.anomalyxii.aoc.utils.maths.Factors;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 8: Haunted Wasteland.
 */
@Solution(year = 2023, day = 8, title = "Haunted Wasteland")
public class Day8 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You're still riding a camel across Desert Island when you spot a sandstorm quickly approaching.
     * When you turn to warn the Elf, she disappears before your eyes!
     * To be fair, she had just finished warning you about <em>ghosts</em> a few minutes ago.
     * <p>
     * One of the camel's pouches is labeled "maps" - sure enough, it's full of documents (your puzzle input) about how to navigate the desert.
     * At least, you're pretty sure that's what they are; one of the documents contains a list of left/right instructions, and the rest of the documents seem to describe some kind of <em>network</em> of labeled nodes.
     * <p>
     * It seems like you're meant to use the <em>left/right</em> instructions to <em>navigate the network</em>.
     * Perhaps if you have the camel follow the same instructions, you can escape the haunted wasteland!
     * <p>
     * After examining the maps for a bit, two nodes stick out: <code>AAA</code> and <code>ZZZ</code>.
     * You feel like <code>AAA</code> is where you are now, and you have to follow the left/right instructions until you reach <code>ZZZ</code>.
     * <p>
     * This format defines each <em>node</em> of the network individually.
     * For example:
     * <pre>
     * RL
     *
     * AAA = (BBB, CCC)
     * BBB = (DDD, EEE)
     * CCC = (ZZZ, GGG)
     * DDD = (DDD, DDD)
     * EEE = (EEE, EEE)
     * GGG = (GGG, GGG)
     * ZZZ = (ZZZ, ZZZ)
     * </pre>
     * <p>
     * Starting with <code>AAA</code>, you need to <em>look up the next element</em> based on the next left/right instruction in your input.
     * In this example, start with <code>AAA</code> and go <em>right</em> (<code>R</code>) by choosing the right element of <code>AAA</code>, <code><em>CCC</em></code>.
     * Then, <code>L</code> means to choose the <em>left</em> element of <code>CCC</code>, <code><em>ZZZ</em></code>.
     * By following the left/right instructions, you reach <code>ZZZ</code> in <code><em>2</em></code> steps.
     * <p>
     * Of course, you might not find <code>ZZZ</code> right away.
     * If you run out of left/right instructions, repeat the whole sequence of instructions as necessary: <code>RL</code> really means <code>RLRLRLRLRLRLRLRL...</code> and so on.
     * For example, here is a situation that takes <code><em>6</em></code> steps to reach <code>ZZZ</code>:
     * <pre>
     * LLR
     *
     * AAA = (BBB, BBB)
     * BBB = (AAA, ZZZ)
     * ZZZ = (ZZZ, ZZZ)
     * </pre>
     * <p>
     * Starting at <code>AAA</code>, follow the left/right instructions.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many steps are required to reach <code>ZZZ</code>?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Maps maps = Maps.parse(context);
        return maps.calculateDistanceToExit();
    }

    /**
     * The <span title="Duhduhduhduhduh! Dah, duhduhduhduhduh!">sandstorm</span> is upon you and you aren't any closer to escaping the wasteland.
     * You had the camel follow the instructions, but you've barely left your starting position.
     * It's going to take <em>significantly more steps</em> to escape!
     * <p>
     * What if the map isn't for people - what if the map is for <em>ghosts</em>? Are ghosts even bound by the laws of spacetime? Only one way to find out.
     * <p>
     * After examining the maps a bit longer, your attention is drawn to a curious fact: the number of nodes with names ending in <code>A</code> is equal to the number ending in <code>Z</code>!
     * If you were a ghost, you'd probably just <em>start at every node that ends with <code>A</code></em> and follow all of the paths at the same time until they all simultaneously end up at nodes that end with <code>Z</code>.
     * <p>
     * For example:
     * <pre>
     * LR
     *
     * 11A = (11B, XXX)
     * 11B = (XXX, 11Z)
     * 11Z = (11B, XXX)
     * 22A = (22B, XXX)
     * 22B = (22C, 22C)
     * 22C = (22Z, 22Z)
     * 22Z = (22B, 22B)
     * XXX = (XXX, XXX)
     * </pre>
     * <p>
     * Here, there are two starting nodes, <code>11A</code> and <code>22A</code> (because they both end with <code>A</code>).
     * As you follow each left/right instruction, use that instruction to <em>simultaneously</em> navigate away from both nodes you're currently on.
     * Repeat this process until <em>all</em> of the nodes you're currently on end with <code>Z</code>.
     * (If only some of the nodes you're on end with <code>Z</code>, they act like any other node and you continue as normal.)
     * In this example, you would proceed as follows:
     * <ul>
     * <li>Step 0: You are at <code>11A</code> and <code>22A</code>.</li>
     * <li>Step 1: You choose all of the <em>left</em> paths, leading you to <code>11B</code> and <code>22B</code>.</li>
     * <li>Step 2: You choose all of the <em>right</em> paths, leading you to <code><em>11Z</em></code> and <code>22C</code>.</li>
     * <li>Step 3: You choose all of the <em>left</em> paths, leading you to <code>11B</code> and <code><em>22Z</em></code>.</li>
     * <li>Step 4: You choose all of the <em>right</em> paths, leading you to <code><em>11Z</em></code> and <code>22B</code>.</li>
     * <li>Step 5: You choose all of the <em>left</em> paths, leading you to <code>11B</code> and <code>22C</code>.</li>
     * <li>Step 6: You choose all of the <em>right</em> paths, leading you to <code><em>11Z</em></code> and <code><em>22Z</em></code>.</li>
     * </ul>
     * <p>
     * So, in this example, you end up entirely on nodes that end in <code>Z</code> after <code><em>6</em></code> steps.
     * <p>
     * Simultaneously start on every node that ends with <code>A</code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many steps does it take before you're only on nodes that end with <code>Z</code>?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Maps maps = Maps.parse(context);
        return maps.calculateDistanceToAllExits();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final Maps maps = Maps.parse(context);
        return new LongTuple(
                maps.calculateDistanceToExit(),
                maps.calculateDistanceToAllExits()
        );
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A collection of maps and directions for traversing them.
     */
    private record Maps(char[] directions, Map<String, String[]> maps) {

        private static final String GENERIC_ENTRANCE = "AAA";
        private static final String GENERIC_EXIT = "ZZZ";

        // Helper Methods

        /*
         * Calculate the distance from the entrance (AAA) to the exit (ZZZ).
         */
        long calculateDistanceToExit() {
            return calculateDistance(GENERIC_ENTRANCE, GENERIC_EXIT::equals);
        }

        /*
         * Calculate the convergence of every entrance (??A) to every exit (??Z).
         */
        public long calculateDistanceToAllExits() {
            final String[] locations = maps.keySet().stream()
                    .filter(Maps::isEntrance)
                    .toArray(String[]::new);

            final int[] results = stream(locations)
                    .mapToInt(start -> calculateDistance(start, Maps::isExit))
                    .toArray();

            return stream(results)
                    .mapToLong(r -> (long) r)
                    .reduce(Factors::lowestCommonMultiple)
                    .orElse(0L);
        }

        /*
         * Calculate the distance from an arbitrary start point.
         */
        private int calculateDistance(final String start, final Predicate<String> endTest) {
            int count = 0;
            String location = start;
            do {
                location = map(count++, location);
            } while (!endTest.test(location));
            return count;
        }

        /*
         * Map from one location to another, based on the distance travelled.
         */
        private String map(final int distance, final String location) {
            final char side = directions[distance % directions.length];
            final String[] map = maps.get(location);
            return (side == 'L' ? map[0] : map[1]);
        }

        // Static Helper Methods

        /*
         * Parse the maps.
         */
        static Maps parse(final SolutionContext context) {
            final List<List<String>> parts = context.readBatches();
            final char[] directions = parts.getFirst().getFirst().toCharArray();

            final Map<String, String[]> maps = parts.getLast().stream()
                    .map(line -> line.split("\\s*=\\s*"))
                    .collect(Collectors.toMap(
                            map -> map[0].trim(),
                            map -> map[1].substring(1, map[1].length() - 1).split(",\\s*")
                    ));

            return new Maps(directions, maps);
        }

        /*
         * Check if a location is an entrance (??A).
         */
        private static boolean isEntrance(final String key) {
            return key.endsWith("A");
        }

        /*
         * Check if a location is an exit (??Z).
         */
        private static boolean isExit(final String location) {
            return location.endsWith("Z");
        }

    }

}

