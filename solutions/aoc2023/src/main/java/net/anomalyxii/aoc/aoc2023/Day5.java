package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;
import net.anomalyxii.aoc.result.ObjectTuple;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 5: If You Give A Seed A Fertilizer.
 */
@Solution(year = 2023, day = 5, title = "If You Give A Seed A Fertilizer")
public class Day5 {

    /*
     * Nothing to process...
     */
    private static final Stream<Range> EMPTY = Stream.empty();

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You take the boat and find the gardener right where you were told he would be: managing a giant "garden" that looks more to you like a farm.
     * <p>
     * "A water source? Island Island <em>is</em> the water source!"
     * You point out that Snow Island isn't receiving any water.
     * <p>
     * "Oh, we had to stop the water because we <em>ran out of sand</em> to <a href="https://en.wikipedia.org/wiki/Sand_filter" target="_blank">filter</a> it with!
     * Can't make snow with dirty water.
     * Don't worry, I'm sure we'll get more sand soon; we only turned off the water a few days... weeks... oh no." His face sinks into a look of horrified realization.
     * <p>
     * "I've been so busy making sure everyone here has food that I completely forgot to check why we stopped getting more sand!
     * There's a ferry leaving soon that is headed over in that direction - it's much faster than your boat.
     * Could you please go check it out?"
     * <p>
     * You barely have time to agree to this request when he brings up another.
     * "While you wait for the ferry, maybe you can help us with our <em>food production problem</em>.
     * The latest Island Island <a href="https://en.wikipedia.org/wiki/Almanac" target="_blank">Almanac</a> just arrived and we're having trouble making sense of it."
     * <p>
     * The almanac (your puzzle input) lists all of the seeds that need to be planted.
     * It also lists what type of soil to use with each kind of seed, what type of fertilizer to use with each kind of soil, what type of water to use with each kind of fertilizer, and so on.
     * Every type of seed, soil, fertilizer and so on is identified with a number, but numbers are reused by each category - that is, soil <code>123</code> and fertilizer <code>123</code> aren't necessarily related to each other.
     * <p>
     * For example:
     * <pre>
     * seeds: 79 14 55 13
     *
     * seed-to-soil map:
     * 50 98 2
     * 52 50 48
     *
     * soil-to-fertilizer map:
     * 0 15 37
     * 37 52 2
     * 39 0 15
     *
     * fertilizer-to-water map:
     * 49 53 8
     * 0 11 42
     * 42 0 7
     * 57 7 4
     *
     * water-to-light map:
     * 88 18 7
     * 18 25 70
     *
     * light-to-temperature map:
     * 45 77 23
     * 81 45 19
     * 68 64 13
     *
     * temperature-to-humidity map:
     * 0 69 1
     * 1 0 69
     *
     * humidity-to-location map:
     * 60 56 37
     * 56 93 4
     * </pre>
     * <p>
     * The almanac starts by listing which seeds need to be planted: seeds <code>79</code>, <code>14</code>, <code>55</code>, and <code>13</code>.
     * <p>
     * The rest of the almanac contains a list of <em>maps</em> which describe how to convert numbers from a <em>source category</em> into numbers in a <em>destination category</em>.
     * That is, the section that starts with <code>seed-to-soil map:</code> describes how to convert a <em>seed number</em> (the source) to a <em>soil number</em> (the destination).
     * This lets the gardener and his team know which soil to use with which seeds, which water to use with which fertilizer, and so on.
     * <p>
     * Rather than list every source number and its corresponding destination number one by one, the maps describe entire <em>ranges</em> of numbers that can be converted.
     * Each line within a map contains <span title="Don't blame me for the weird order.
     * Blame LXC container.conf UID mappings.">three numbers</span>: the <em>destination range start</em>, the <em>source range start</em>, and the <em>range length</em>.
     * <p>
     * Consider again the example <code>seed-to-soil map</code>:
     * <pre>
     * 50 98 2
     * 52 50 48
     * </pre>
     * <p>
     * The first line has a <em>destination range start</em> of <code>50</code>, a <em>source range start</em> of <code>98</code>, and a <em>range length</em> of <code>2</code>.
     * This line means that the source range starts at <code>98</code> and contains two values: <code>98</code> and <code>99</code>.
     * The destination range is the same length, but it starts at <code>50</code>, so its two values are <code>50</code> and <code>51</code>.
     * With this information, you know that seed number <code>98</code> corresponds to soil number <code>50</code> and that seed number <code>99</code> corresponds to soil number <code>51</code>.
     * <p>
     * The second line means that the source range starts at <code>50</code> and contains <code>48</code> values: <code>50</code>, <code>51</code>, ..., <code>96</code>, <code>97</code>.
     * This corresponds to a destination range starting at <code>52</code> and also containing <code>48</code> values: <code>52</code>, <code>53</code>, ..., <code>98</code>, <code>99</code>.
     * So, seed number <code>53</code> corresponds to soil number <code>55</code>.
     * <p>
     * Any source numbers that <em>aren't mapped</em> correspond to the <em>same</em> destination number.
     * So, seed number <code>10</code> corresponds to soil number <code>10</code>.
     * <p>
     * So, the entire list of seed numbers and their corresponding soil numbers looks like this:
     * <pre>
     * seed  soil
     * 0     0
     * 1     1
     * ...
     * ...
     * 48    48
     * 49    49
     * 50    52
     * 51    53
     * ...
     * ...
     * 96    98
     * 97    99
     * 98    50
     * 99    51
     * </pre>
     * <p>
     * With this map, you can look up the soil number required for each initial seed number:
     * <ul>
     * <li>Seed number <code>79</code> corresponds to soil number <code>81</code>.</li>
     * <li>Seed number <code>14</code> corresponds to soil number <code>14</code>.</li>
     * <li>Seed number <code>55</code> corresponds to soil number <code>57</code>.</li>
     * <li>Seed number <code>13</code> corresponds to soil number <code>13</code>.</li>
     * </ul>
     * <p>
     * The gardener and his team want to get started as soon as possible, so they'd like to know the closest location that needs a seed.
     * Using these maps, find <em>the lowest location number that corresponds to any of the initial seeds</em>.
     * To do this, you'll need to convert each seed number through other categories until you can find its corresponding <em>location number</em>.
     * In this example, the corresponding types are:
     * <ul>
     * <li>Seed <code>79</code>, soil <code>81</code>, fertilizer <code>81</code>, water <code>81</code>, light <code>74</code>, temperature <code>78</code>, humidity <code>78</code>, <em>location <code>82</code></em>.</li>
     * <li>Seed <code>14</code>, soil <code>14</code>, fertilizer <code>53</code>, water <code>49</code>, light <code>42</code>, temperature <code>42</code>, humidity <code>43</code>, <em>location <code>43</code></em>.</li>
     * <li>Seed <code>55</code>, soil <code>57</code>, fertilizer <code>57</code>, water <code>53</code>, light <code>46</code>, temperature <code>82</code>, humidity <code>82</code>, <em>location <code>86</code></em>.</li>
     * <li>Seed <code>13</code>, soil <code>13</code>, fertilizer <code>52</code>, water <code>41</code>, light <code>34</code>, temperature <code>34</code>, humidity <code>35</code>, <em>location <code>35</code></em>.</li>
     * </ul>
     * <p>
     * So, the lowest location number in this example is <code><em>35</em></code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the lowest location number that corresponds to any of the initial seed numbers?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return processAlmanac(context, Range::extractSingleSeedRanges)
                .mapToLong(Range::start)
                .min()
                .orElseThrow();
    }

    /**
     * Everyone will starve if you only plant such a small number of seeds.
     * Re-reading the almanac, it looks like the <code>seeds:</code> line actually describes <em>ranges of seed numbers</em>.
     * <p>
     * The values on the initial <code>seeds:</code> line come in pairs.
     * Within each pair, the first value is the <em>start</em> of the range and the second value is the <em>length</em> of the range.
     * So, in the first line of the example above:
     * <pre>
     * seeds: 79 14 55 13
     * </pre>
     * <p>
     * This line describes two ranges of seed numbers to be planted in the garden.
     * The first range starts with seed number <code>79</code> and contains <code>14</code> values: <code>79</code>, <code>80</code>, ..., <code>91</code>, <code>92</code>.
     * The second range starts with seed number <code>55</code> and contains <code>13</code> values: <code>55</code>, <code>56</code>, ..., <code>66</code>, <code>67</code>.
     * <p>
     * Now, rather than considering four seed numbers, you need to consider a total of <em>27</em> seed numbers.
     * <p>
     * In the above example, the lowest location number can be obtained from seed number <code>82</code>, which corresponds to soil <code>84</code>, fertilizer <code>84</code>, water <code>84</code>, light <code>77</code>, temperature <code>45</code>, humidity <code>46</code>, and <em>location <code>46</code></em>.
     * So, the lowest location number is <code><em>46</em></code>.
     * <p>
     * Consider all of the initial seed numbers listed in the ranges on the first line of the almanac.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the lowest location number that corresponds to any of the initial seed numbers?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return processAlmanac(context, Range::extractSeedRanges)
                .mapToLong(Range::start)
                .min()
                .orElseThrow();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final ObjectTuple<Stream<Range>, Stream<Range>> results = context.streamBatches()
                .reduce(
                        new ObjectTuple<>(EMPTY, EMPTY),
                        (tup, batch) -> new ObjectTuple<>(
                                processMapping(Range::extractSingleSeedRanges, tup.answer1(), batch),
                                processMapping(Range::extractSeedRanges, tup.answer2(), batch)
                        ),
                        (a, b) -> {
                            throw new IllegalStateException("Cannot merge!");
                        }
                );
        return new LongTuple(
                results.answer1().mapToLong(range -> range.start).min().orElseThrow(),
                results.answer2().mapToLong(range -> range.start).min().orElseThrow()
        );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Process the entire almanac.
     */
    private Stream<Range> processAlmanac(final SolutionContext context, final Function<String, Stream<Range>> seedExtractor) {
        return context.streamBatches()
                .reduce(
                        EMPTY,
                        (list, batch) -> processMapping(seedExtractor, list, batch),
                        Range::merge
                );
    }

    /*
     * Process a single mapping of almanac values.
     */
    private static Stream<Range> processMapping(
            final Function<String, Stream<Range>> seedExtractor,
            final Stream<Range> list,
            final List<String> batch
    ) {
        final String identifier = batch.getFirst();
        if (identifier.startsWith("seeds: "))
            return seedExtractor.apply(identifier.substring(7));

        final SortedSet<RangeMap> bounds = batch.subList(1, batch.size()).stream()
                .map(RangeMap::parse)
                .collect(Collectors.toCollection(TreeSet::new));

        return list
                .flatMap(sourceRange -> RangeMap.offset(sourceRange, bounds))
                .sorted();
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A range of numbers.
     */
    private record Range(long start, long end) implements Comparable<Range> {

        // Comparable Methods

        @Override
        public int compareTo(final Range o) {
            return Long.compare(start, o.start);
        }

        // Static Helper Methods

        /*
         * Create a `Range` that contains only one value.
         */
        static Range singleton(final long start) {
            return new Range(start, start);
        }

        /*
         * Create a `Range` from a given start point with a given length.
         */
        static Range from(final long start, final long length) {
            return new Range(start, start + (length - 1));
        }

        /*
         * Extract a `Set` of `Range`s that represent a single seed.
         */
        static Stream<Range> extractSingleSeedRanges(final String seeds) {
            return Arrays.stream(seeds.split("\\s"))
                    .map(Long::valueOf)
                    .map(Range::singleton)
                    .sorted();
        }

        /*
         * Extract a `Set` of `Range`s that represent multiple seeds.
         */
        static Stream<Range> extractSeedRanges(final String seeds) {
            final List<Long> parts = Arrays.stream(seeds.split("\\s"))
                    .map(Long::valueOf)
                    .toList();

            final Stream.Builder<Range> seedRanges = Stream.builder();
            for (int i = 0; i < parts.size(); i += 2)
                seedRanges.add(Range.from(parts.get(i), parts.get(i + 1)));
            return seedRanges.build();
        }

        /*
         * Merge two `Stream`s of `Range`s.
         */
        static Stream<Range> merge(final Stream<Range> first, final Stream<Range> second) {
            return Stream.concat(first, second);
        }
    }

    /*
     * A mapping between two number ranges.
     */
    private record RangeMap(Range source, Range destination)
            implements Comparable<RangeMap> {

        // Comparable Methods

        @Override
        public int compareTo(final RangeMap o) {
            return Long.compare(source.start, o.source.start);
        }

        // Helper Methods

        /*
         * Map an input based on the relative position in this `RangeMap`.
         */
        long offset(final long input) {
            return (input - source.start) + destination.start;
        }

        // Static Helper Methods

        /*
         * Parse a `RangeMap` from a given line of format:
         *   <destination start> <source start> <length>
         */
        static RangeMap parse(final String line) {
            final String[] parts = line.split("\\s");
            return RangeMap.from(
                    Long.parseLong(parts[0]),
                    Long.parseLong(parts[1]),
                    Long.parseLong(parts[2])
            );
        }

        /*
         * Parse a `RangeMap` from a destination start point, a source start
         * point and a length for the two `Range`s.
         */
        static RangeMap from(final long destination, final long source, final long length) {
            return new RangeMap(
                    Range.from(source, length),
                    Range.from(destination, length)
            );
        }

        /*
         * Transform all numbers in a given `Range` according to the `RangeMap`s
         * provided.
         */
        static Stream<Range> offset(final Range sourceRange, final SortedSet<RangeMap> bounds) {
            final Stream.Builder<Range> result = Stream.builder();
            final Iterator<RangeMap> boundsIterator = bounds.iterator();

            RangeMap currentBounds = null;
            long current = sourceRange.start;
            while (current <= sourceRange.end) {
                if (currentBounds == null) {
                    if (!boundsIterator.hasNext()) {
                        result.add(new Range(current, sourceRange.end));
                        return result.build();
                    }

                    currentBounds = boundsIterator.next();
                }

                if (current < currentBounds.source.start) {
                    final long end = Math.min(sourceRange.end, currentBounds.source.start - 1);
                    if (end >= current)
                        result.add(new Range(current, end));
                    current = end + 1;
                    continue;
                }

                if (current > currentBounds.source.end) {
                    currentBounds = null;
                    continue;
                }

                final long end = Math.min(currentBounds.source.end, sourceRange.end);
                result.add(new Range(currentBounds.offset(current), currentBounds.offset(end)));
                current = end + 1;
            }

            return result.build();
        }
    }

}

