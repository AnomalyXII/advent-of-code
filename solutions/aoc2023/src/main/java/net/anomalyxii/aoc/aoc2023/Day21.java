package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 21: Step Counter.
 */
@Solution(year = 2023, day = 21, title = "Step Counter")
public class Day21 {

    /*
     * Default step target for an Elf.
     */
    private static final int DEFAULT_STEP_TARGET = 64;
    private static final int DEFAULT_ACTUAL_STEP_TARGET = 26501365;

    // ****************************************
    // Private Members
    // ****************************************

    private final int stepTarget;
    private final int actualStepTarget;

    // ****************************************
    // Constructors
    // ****************************************

    public Day21() {
        this(DEFAULT_STEP_TARGET, DEFAULT_ACTUAL_STEP_TARGET);
    }

    Day21(final int stepTarget, final int actualStepTarget) {
        this.stepTarget = stepTarget;
        this.actualStepTarget = actualStepTarget;
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You manage to catch the <a href="7">airship</a> right as it's dropping someone else off on their all-expenses-paid trip to Desert Island!
     * It even helpfully drops you off near the <a href="5">gardener</a> and his massive farm.
     * <p>
     * "You got the sand flowing again! Great work! Now we just need to wait until we have enough sand to filter the water for Snow Island and we'll have snow again in no time."
     * <p>
     * While you wait, one of the Elves that works with the gardener heard how good you are at solving problems and would like your help.
     * He needs to get his <a href="https://en.wikipedia.org/wiki/Pedometer" target="_blank">steps</a> in for the day, and so he'd like to know <em>which garden plots he can reach with exactly his remaining <code>64</code> steps</em>.
     * <p>
     * He gives you an up-to-date map (your puzzle input) of his starting position (<code>S</code>), garden plots (<code>.</code>), and rocks (<code>#</code>).
     * For example:
     * <pre>
     * ...........
     * .....###.#.
     * .###.##..#.
     * ..#.#...#..
     * ....#.#....
     * .##..S####.
     * .##..#...#.
     * .......##..
     * .##.#.####.
     * .##..##.##.
     * ...........
     * </pre>
     * <p>
     * The Elf starts at the starting position (<code>S</code>) which also counts as a garden plot.
     * Then, he can take one step north, south, east, or west, but only onto tiles that are garden plots.
     * This would allow him to reach any of the tiles marked <code>O</code>:
     * <pre>
     * ...........
     * .....###.#.
     * .###.##..#.
     * ..#.#...#..
     * ....#O#....
     * .##.OS####.
     * .##..#...#.
     * .......##..
     * .##.#.####.
     * .##..##.##.
     * ...........
     * </pre>
     * <p>
     * Then, he takes a second step.
     * Since at this point he could be at <em>either</em> tile marked <code>O</code>, his second step would allow him to reach any garden plot that is one step north, south, east, or west of <em>any</em> tile that he could have reached after the first step:
     * <pre>
     * ...........
     * .....###.#.
     * .###.##..#.
     * ..#.#O..#..
     * ....#.#....
     * .##O.O####.
     * .##.O#...#.
     * .......##..
     * .##.#.####.
     * .##..##.##.
     * ...........
     * </pre>
     * <p>
     * After two steps, he could be at any of the tiles marked <code>O</code> above, including the starting position (either by going north-then-south or by going west-then-east).
     * <p>
     * A single third step leads to even more possibilities:
     * <pre>
     * ...........
     * .....###.#.
     * .###.##..#.
     * ..#.#.O.#..
     * ...O#O#....
     * .##.OS####.
     * .##O.#...#.
     * ....O..##..
     * .##.#.####.
     * .##..##.##.
     * ...........
     * </pre>
     * <p>
     * He will continue like this until his steps for the day have been exhausted.
     * After a total of <code>6</code> steps, he could reach any of the garden plots marked <code>O</code>:
     * <pre>
     * ...........
     * .....###.#.
     * .###.##.O#.
     * .O#O#O.O#..
     * O.O.#.#.O..
     * .##O.O####.
     * .##.O#O..#.
     * .O.O.O.##..
     * .##.#.####.
     * .##O.##.##.
     * ...........
     * </pre>
     * <p>
     * In this example, if the Elf's goal was to get exactly <code>6</code> more steps today, he could use them to reach any of <code><em>16</em></code> garden plots.
     * <p>
     * However, the Elf <em>actually needs to get <code>64</code> steps today</em>, and the map he's handed you is much larger than the example map.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Starting from the garden plot marked <code>S</code> on your map, how many garden plots could the Elf reach in exactly <code>64</code> steps?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Grid map = context.readGrid();

        final Coordinate start = map.stream()
                .filter(c -> map.get(c) == 'S')
                .findFirst()
                .orElseThrow();

        final int[] results = walk(map, start, stepTarget);
        return results[stepTarget - 1];
    }

    /**
     * The Elf seems confused by your answer until he realizes his mistake: he was reading from a <span title="Next up: 729.">list</span> of his favorite numbers that are both perfect squares and perfect cubes, not his step counter.
     * <p>
     * The <em>actual</em> number of steps he needs to get today is exactly <code><em>26501365</em></code>.
     * <p>
     * He also points out that the garden plots and rocks are set up so that the map <em>repeats infinitely</em> in every direction.
     * <p>
     * So, if you were to look one additional map-width or map-height out from the edge of the example map above, you would find that it keeps repeating:
     * <pre>
     * .................................
     * .....###.#......###.#......###.#.
     * .###.##..#..###.##..#..###.##..#.
     * ..#.#...#....#.#...#....#.#...#..
     * ....#.#........#.#........#.#....
     * .##...####..##...####..##...####.
     * .##..#...#..##..#...#..##..#...#.
     * .......##.........##.........##..
     * .##.#.####..##.#.####..##.#.####.
     * .##..##.##..##..##.##..##..##.##.
     * .................................
     * .................................
     * .....###.#......###.#......###.#.
     * .###.##..#..###.##..#..###.##..#.
     * ..#.#...#....#.#...#....#.#...#..
     * ....#.#........#.#........#.#....
     * .##...####..##..S####..##...####.
     * .##..#...#..##..#...#..##..#...#.
     * .......##.........##.........##..
     * .##.#.####..##.#.####..##.#.####.
     * .##..##.##..##..##.##..##..##.##.
     * .................................
     * .................................
     * .....###.#......###.#......###.#.
     * .###.##..#..###.##..#..###.##..#.
     * ..#.#...#....#.#...#....#.#...#..
     * ....#.#........#.#........#.#....
     * .##...####..##...####..##...####.
     * .##..#...#..##..#...#..##..#...#.
     * .......##.........##.........##..
     * .##.#.####..##.#.####..##.#.####.
     * .##..##.##..##..##.##..##..##.##.
     * .................................
     * </pre>
     * <p>
     * This is just a tiny three-map-by-three-map slice of the inexplicably-infinite farm layout; garden plots and rocks repeat as far as you can see.
     * The Elf still starts on the one middle tile marked <code>S</code>, though - every other repeated <code>S</code> is replaced with a normal garden plot (<code>.</code>).
     * <p>
     * Here are the number of reachable garden plots in this new infinite version of the example map for different numbers of steps:
     * <ul>
     * <li>In exactly <code>6</code> steps, he can still reach <code><em>16</em></code> garden plots.</li>
     * <li>In exactly <code>10</code> steps, he can reach any of <code><em>50</em></code> garden plots.</li>
     * <li>In exactly <code>50</code> steps, he can reach <code><em>1594</em></code> garden plots.</li>
     * <li>In exactly <code>100</code> steps, he can reach <code><em>6536</em></code> garden plots.</li>
     * <li>In exactly <code>500</code> steps, he can reach <code><em>167004</em></code> garden plots.</li>
     * <li>In exactly <code>1000</code> steps, he can reach <code><em>668697</em></code> garden plots.</li>
     * <li>In exactly <code>5000</code> steps, he can reach <code><em>16733044</em></code> garden plots.</li>
     * </ul>
     * <p>
     * However, the step count the Elf needs is much larger!
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Starting from the garden plot marked <code>S</code> on your infinite map, how many garden plots could the Elf reach in exactly <code>26501365</code> steps?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Grid map = Grid.parseInfinite(context.stream());
        final Coordinate start = map.stream()
                .filter(c -> map.get(c) == 'S')
                .findFirst()
                .orElseThrow();

        final int offset = start.x();
        final int width = map.width();

        final int[] results = walk(map, start, offset + width * 2);
        final int target = (actualStepTarget - offset) / width;
        return mathsH4x(target, results[offset - 1], results[offset + width - 1], results[offset + width + width - 1]);
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final Grid map = Grid.parseInfinite(context.stream());
        final Coordinate start = map.stream()
                .filter(c -> map.get(c) == 'S')
                .findFirst()
                .orElseThrow();

        final int offset = start.x();
        final int width = map.width();

        final int[] results = walk(map, start, offset + width * 2);
        final int target = (actualStepTarget - offset) / width;

        final long answer1 = results[stepTarget - 1];
        final long answer2 = mathsH4x(target, results[offset - 1], results[offset + width - 1], results[offset + width + width - 1]);

        return new LongTuple(answer1, answer2);
    }

    // ****************************************
    // Helper Methods
    // ****************************************

    /*
     * Walk the garden map and see how many plots can be visited.
     */
    private int[] walk(final Grid map, final Coordinate start, final int steps) {
        final int[] results = new int[steps];

        Set<Coordinate> positions = Set.of(start);
        final Set<Coordinate> visited = new HashSet<>();
        for (int i = 0; i < steps; i++) {
            positions = positions.stream()
                    .flatMap(Coordinate::adjacent)
                    .filter(visited::add)
                    .filter(neighbour -> map.get(neighbour) != '#')
                    .collect(Collectors.toSet());
            results[i] = positions.size() + (i > 1 ? results[i - 2] : 0);
        }

        return results;
    }

    /*
     * Do some crazy maths that I found on Reddit to efficiently solve Pt II.
     *
     * See: https://www.reddit.com/r/adventofcode/comments/18nevo3/comment/keao4q8/.
     */
    private static long mathsH4x(final long target, final int a, final int b, final int c) {
        return a + target * (b - a + (target - 1) * (c - b - b + a) / 2);
    }

}

