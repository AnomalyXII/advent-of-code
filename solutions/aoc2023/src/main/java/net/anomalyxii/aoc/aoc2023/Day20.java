package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;
import net.anomalyxii.aoc.utils.maths.Factors;

import java.util.*;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.function.Function;
import java.util.function.LongConsumer;
import java.util.stream.Collectors;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 20: Pulse Propagation.
 */
@Solution(year = 2023, day = 20, title = "Pulse Propagation")
public class Day20 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * With your help, the Elves manage to find the right parts and fix all of the machines.
     * Now, they just need to send the command to boot up the machines and get the sand flowing again.
     * <p>
     * The machines are far apart and wired together with long <em>cables</em>.
     * The cables don't connect to the machines directly, but rather to communication <em>modules</em> attached to the machines that perform various initialization tasks and also act as communication relays.
     * <p>
     * Modules communicate using <em>pulses</em>.
     * Each pulse is either a <em>high pulse</em> or a <em>low pulse</em>.
     * When a module sends a pulse, it sends that type of pulse to each module in its list of <em>destination modules</em>.
     * <p>
     * There are several different types of modules:
     * <p>
     * <em>Flip-flop</em> modules (prefix <code>%</code>) are either <em>on</em> or <em>off</em>; they are initially <em>off</em>.
     * If a flip-flop module receives a high pulse, it is ignored and nothing happens.
     * However, if a flip-flop module receives a low pulse, it <em>flips between on and off</em>.
     * If it was off, it turns on and sends a high pulse.
     * If it was on, it turns off and sends a low pulse.
     * <p>
     * <em>Conjunction</em> modules (prefix <code>&amp;</code>) <em>remember</em> the type of the most recent pulse received from <em>each</em> of their connected input modules; they initially default to remembering a <em>low pulse</em> for each input.
     * When a pulse is received, the conjunction module first updates its memory for that input.
     * Then, if it remembers <em>high pulses</em> for all inputs, it sends a <em>low pulse</em>; otherwise, it sends a <em>high pulse</em>.
     * <p>
     * There is a single <em>broadcast module</em> (named <code>broadcaster</code>).
     * When it receives a pulse, it sends the same pulse to all of its destination modules.
     * <p>
     * Here at Desert Machine Headquarters, there is a module with a single button on it called, aptly, the <em>button module</em>.
     * When you push the button, a single <em>low pulse</em> is sent directly to the <code>broadcaster</code> module.
     * <p>
     * After pushing the button, you must wait until all pulses have been delivered and fully handled before pushing it again.
     * Never push the button if modules are still processing pulses.
     * <p>
     * Pulses are always processed <em>in the order they are sent</em>.
     * So, if a pulse is sent to modules <code>a</code>, <code>b</code>, and <code>c</code>, and then module <code>a</code> processes its pulse and sends more pulses, the pulses sent to modules <code>b</code> and <code>c</code> would have to be handled first.
     * <p>
     * The module configuration (your puzzle input) lists each module.
     * The name of the module is preceded by a symbol identifying its type, if any.
     * The name is then followed by an arrow and a list of its destination modules.
     * For example:
     * <pre>
     * broadcaster -&gt; a, b, c
     * %a -&gt; b
     * %b -&gt; c
     * %c -&gt; inv
     * &amp;inv -&gt; a
     * </pre>
     * <p>
     * In this module configuration, the broadcaster has three destination modules named <code>a</code>, <code>b</code>, and <code>c</code>.
     * Each of these modules is a flip-flop module (as indicated by the <code>%</code> prefix).
     * <code>a</code> outputs to <code>b</code> which outputs to <code>c</code> which outputs to another module named <code>inv</code>.
     * <code>inv</code> is a conjunction module (as indicated by the <code>&amp;</code> prefix) which, because it has only one input, acts like an <span title="This puzzle originally had a separate inverter module type until I realized it was just a worse conjunction module.">inverter</span> (it sends the opposite of the pulse type it receives); it outputs to <code>a</code>.
     * <p>
     * By pushing the button once, the following pulses are sent:
     * <pre>
     * button -low-&gt; broadcaster
     * broadcaster -low-&gt; a
     * broadcaster -low-&gt; b
     * broadcaster -low-&gt; c
     * a -high-&gt; b
     * b -high-&gt; c
     * c -high-&gt; inv
     * inv -low-&gt; a
     * a -low-&gt; b
     * b -low-&gt; c
     * c -low-&gt; inv
     * inv -high-&gt; a
     * </pre>
     * <p>
     * After this sequence, the flip-flop modules all end up <em>off</em>, so pushing the button again repeats the same sequence.
     * <p>
     * Here's a more interesting example:
     * <pre>
     * broadcaster -> a
     * %a -> inv, con
     * &amp;inv -> b
     * %b -> con
     * &amp;con -> output
     * </pre>
     * <p>
     * This module configuration includes the <code>broadcaster</code>, two flip-flops (named <code>a</code> and <code>b</code>), a single-input conjunction module (<code>inv</code>), a multi-input conjunction module (<code>con</code>), and an untyped module named <code>output</code> (for testing purposes).
     * The multi-input conjunction module <code>con</code> watches the two flip-flop modules and, if they're both on, sends a <em>low pulse</em> to the <code>output</code> module.
     * <p>
     * Here's what happens if you push the button once:
     * <pre>
     * button -low-&gt; broadcaster
     * broadcaster -low-&gt; a
     * a -high-&gt; inv
     * a -high-&gt; con
     * inv -low-&gt; b
     * con -high-&gt; output
     * b -high-&gt; con
     * con -low-&gt; output
     * </pre>
     * <p>
     * Both flip-flops turn on and a low pulse is sent to <code>output</code>!
     * However, now that both flip-flops are on and <code>con</code> remembers a high pulse from each of its two inputs, pushing the button a second time does something different:
     * <pre>
     * button -low-&gt; broadcaster
     * broadcaster -low-&gt; a
     * a -low-&gt; inv
     * a -low-&gt; con
     * inv -high-&gt; b
     * con -high-&gt; output
     * </pre>
     * <p>
     * Flip-flop <code>a</code> turns off!
     * Now, <code>con</code> remembers a low pulse from module <code>a</code>, and so it sends only a high pulse to <code>output</code>.
     * <p>
     * Push the button a third time:
     * <pre>
     * button -low-&gt; broadcaster
     * broadcaster -low-&gt; a
     * a -high-&gt; inv
     * a -high-&gt; con
     * inv -low-&gt; b
     * con -low-&gt; output
     * b -low-&gt; con
     * con -high-&gt; output
     * </pre>
     * <p>
     * This time, flip-flop <code>a</code> turns on, then flip-flop <code>b</code> turns off.
     * However, before <code>b</code> can turn off, the pulse sent to <code>con</code> is handled first, so it <em>briefly remembers all high pulses</em> for its inputs and sends a low pulse to <code>output</code>.
     * After that, flip-flop <code>b</code> turns off, which causes <code>con</code> to update its state and send a high pulse to <code>output</code>.
     * <p>
     * Finally, with <code>a</code> on and <code>b</code> off, push the button a fourth time:
     * <pre>
     * button -low-&gt; broadcaster
     * broadcaster -low-&gt; a
     * a -low-&gt; inv
     * a -low-&gt; con
     * inv -high-&gt; b
     * con -high-&gt; output
     * </pre>
     * <p>
     * This completes the cycle: <code>a</code> turns off, causing <code>con</code> to remember only low pulses and restoring all modules to their original states.
     * <p>
     * To get the cables warmed up, the Elves have pushed the button <code>1000</code> times.
     * How many pulses got sent as a result (including the pulses sent by the button itself)?
     * <p>
     * In the first example, the same thing happens every time the button is pushed: <code>8</code> low pulses and <code>4</code> high pulses are sent.
     * So, after pushing the button <code>1000</code> times, <code>8000</code> low pulses and <code>4000</code> high pulses are sent.
     * Multiplying these together gives <code><em>32000000</em></code>.
     * <p>
     * In the second example, after pushing the button <code>1000</code> times, <code>4250</code> low pulses and <code>2750</code> high pulses are sent.
     * Multiplying these together gives <code><em>11687500</em></code>.
     * <p>
     * Consult your module configuration; determine the number of low pulses and high pulses that would be sent after pushing the button <code>1000</code> times, waiting for all pulses to be fully handled after each push of the button.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you multiply the total number of low pulses sent by the total number of high pulses sent?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Wiring wiring = Wiring.fromInput(context);

        for (int i = 0; i < 1000; i++)
            wiring.pushTheButton();

        final long highPulses = wiring.highPulses.get();
        final long lowPulses = wiring.lowPulses.get();
        return highPulses * lowPulses;
    }

    /**
     * The final machine responsible for moving the sand down to Island Island has a module attached named <code>rx</code>.
     * The machine turns on when a <em>single low pulse</em> is sent to <code>rx</code>.
     * <p>
     * Reset all modules to their default states.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Waiting for all pulses to be fully handled after each button press, what is the fewest number of button presses required to deliver a single low pulse to the module named <code>rx</code>?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Wiring wiring = Wiring.fromInput(context);

        while (!wiring.isStrongAndStable())
            wiring.pushTheButton();

        return wiring.calculateFirstCompleteCycle();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final Wiring wiring = Wiring.fromInput(context);

        LongTuple answer = LongTuple.NULL;
        for (int i = 0; i < 1000 || !wiring.isStrongAndStable(); i++) {
            if (i == 1000)
                answer = answer.add(wiring.highPulses.get() * wiring.lowPulses.get(), 0);

            wiring.pushTheButton();
        }

        return answer.add(0, wiring.calculateFirstCompleteCycle());
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * Whether a pulse is a high pulse or a low pulse.
     */
    private enum PulseType {

        /*
         * A high pulse.
         */
        HIGH,

        /*
         * A low pulse.
         */
        LOW,

        // End of constants
        ;

        // Helper Methods

        /*
         * Invert this `PulseType`.
         */
        PulseType invert() {
            return switch (this) {
                case HIGH -> LOW;
                case LOW -> HIGH;
            };
        }

    }

    /*
     * Represents a pulse sent through the machinery.
     */
    private record Pulse(String from, String to, PulseType type) {

        // Helper Methods

        /*
         * Increase the total pulse count for this type of `Pulse`.
         */
        void accumulate(final LongConsumer onHigh, final LongConsumer onLow) {
            switch (type) {
                case HIGH -> onHigh.accept(1);
                case LOW -> onLow.accept(1);
                default -> throw new IllegalArgumentException("Invalid PulseType: " + type);
            }
        }

    }

    /*
     * Represents the wiring of modules within the complicated Elf machinery.
     */
    private static class Wiring {

        /*
         * Identify the broadcaster node.
         */
        private static final String BROADCASTER_LABEL = "broadcaster";

        // Private Members#

        private final Map<String, Module> modules;
        private final List<FlipFlopModule> flipFlops;
        private final LongAccumulator highPulses = new LongAccumulator(Long::sum, 0L);
        private final LongAccumulator lowPulses = new LongAccumulator(Long::sum, 0L);

        // Constructors

        Wiring(final Map<String, Module> modules, final List<FlipFlopModule> flipFlops) {
            this.modules = modules;
            this.flipFlops = flipFlops;
        }

        // Helper Methods

        /*
         * Initialise the wiring by sending a low pulse to all modules.
         */
        void init() {
            final Set<String> keys = new HashSet<>(modules.keySet());
            keys.stream()
                    .map(this::resolve)
                    .forEach(module -> module.downstream()
                            .forEach(downstream -> resolve(downstream).init(module.name())));
        }

        /*
         * Push the button.
         */
        void pushTheButton() {

            flipFlops.forEach(FlipFlopModule::prePress);

            final Deque<Pulse> queue = new ArrayDeque<>();
            queue.addFirst(new Pulse(null, BROADCASTER_LABEL, PulseType.LOW));
            while (!queue.isEmpty()) {
                final Pulse pulse = queue.pollFirst();
                final Module to = resolve(pulse.to);

                pulse.accumulate(highPulses::accumulate, lowPulses::accumulate);
                queue.addAll(to.accept(pulse));
            }

            flipFlops.forEach(FlipFlopModule::postPress);

        }

        /*
         * Check if this wiring is "strong and stable" by deciding whether every
         * `FlipFlopModule` has been able to complete a "stable" loop.
         *
         * We detect a "stable" loop by tracking the number of button presses it
         * takes to flip one of these modules and mark the "stable" loop as
         * complete at the end of the first "irregular" period.
         */
        boolean isStrongAndStable() {
            return flipFlops.stream().allMatch(flipflop -> flipflop.loopLength > 0);
        }

        /*
         * Calculate the number of button presses it would take to complete a
         * full cycle of the wiring grid. A complete cycle would require all
         * `FlipFlopModule`s to be at the end of their "stable" loop at the
         * same time.
         */
        long calculateFirstCompleteCycle() {
            final Map<Integer, Long> loops = flipFlops.stream()
                    .collect(Collectors.groupingBy(mod -> mod.loopLength, Collectors.counting()));

            return loops.entrySet().stream()
                    .filter(entry -> entry.getValue() > 1)
                    .mapToLong(Map.Entry::getKey)
                    .reduce(Factors::lowestCommonMultiple)
                    .orElseThrow();
        }

        /*
         * Resolve a `Module` based on the name.
         */
        private Module resolve(final String from) {
            return modules.computeIfAbsent(from, Sink::new);
        }

        // Static Helper Methods

        /*
         * Create a new `Wiring` from the given input.
         */
        static Wiring fromInput(final SolutionContext context) {
            final Map<String, Module> modules = Module.fromInput(context);
            final List<FlipFlopModule> flipFlops = modules.values().stream()
                    .filter(module -> module instanceof FlipFlopModule)
                    .map(FlipFlopModule.class::cast)
                    .toList();

            final Wiring wiring = new Wiring(modules, flipFlops);
            wiring.init();
            return wiring;
        }

    }

    /*
     * A module that receives and sends pulses.
     */
    private sealed interface Module
            permits AbstractModule, FlipFlopModule, ConjunctionModule,
            BroadcasterModule, Sink {

        // Interface Methods

        /*
         * Get the name of this `Module`.
         */
        String name();

        /*
         * Get all the downstream `Module` names.
         */
        List<String> downstream();

        /*
         * Tell a module about an upstream connection.
         */
        void init(String upstream);

        /*
         * Accept a `Pulse`.
         */
        List<Pulse> accept(Pulse pulse);

        // Static Helper Methods

        /*
         * Parse some `Module`s from the input text.
         */
        static Map<String, Module> fromInput(final SolutionContext context) {
            return context.stream()
                    .map(Module::parse)
                    .collect(Collectors.toMap(
                            Module::name,
                            Function.identity()
                    ));
        }

        /*
         * Parse a `Module` from a given line.
         */
        private static Module parse(final String line) {
            final String[] parts = line.split("\\s*->\\s*");
            final String identifier = parts[0];
            final List<String> downstream = Arrays.asList(parts[1].split(",\\s*"));

            if ("broadcaster".equals(identifier)) {
                return new BroadcasterModule(identifier, downstream);
            }

            if (identifier.startsWith("%")) {
                final String name = identifier.substring(1);
                return new FlipFlopModule(name, downstream);
            }

            if (identifier.startsWith("&")) {
                final String name = identifier.substring(1);
                return new ConjunctionModule(name, downstream);
            }

            throw new IllegalStateException("Could not process module definition: [" + line + "]");
        }

    }

    /*
     * A base class for various other `Module`s.
     */
    private abstract static sealed class AbstractModule
            implements Module
            permits FlipFlopModule, ConjunctionModule, BroadcasterModule {

        // Private Members

        protected final String name;
        protected final List<String> downstream;

        // Constructors

        AbstractModule(final String name, final List<String> downstream) {
            this.name = name;
            this.downstream = downstream;
        }

        // Module Methods

        @Override
        public String name() {
            return name;
        }

        @Override
        public List<String> downstream() {
            return Collections.unmodifiableList(downstream);
        }

        @Override
        public void init(final String upstream) {

        }

        // Helper Methods

        /*
         * Send a pulse to all downstream `Module`s.
         */
        List<Pulse> sendPulse(final PulseType type) {
            return downstream.stream()
                    .map(next -> new Pulse(name, next, type))
                    .toList();
        }

    }

    /*
     * A `Module` that flips state on receiving a low pulse.
     */
    private static final class FlipFlopModule extends AbstractModule implements Module {

        // Private Members

        private PulseType state = PulseType.LOW;

        // Stuff for part 2...
        private PulseType cachedState;
        private int loopLength = -1;
        private int cumulativeSegmentLength = 0;
        private int segmentLength = 0;


        // Constructors

        FlipFlopModule(final String name, final List<String> downstream) {
            super(name, downstream);
        }

        // Module Methods

        @Override
        public List<Pulse> accept(final Pulse pulse) {
            if (pulse.type == PulseType.HIGH) return Collections.emptyList();
            state = state.invert();
            return sendPulse(state);
        }

        // Helper Methods

        /*
         * Cache the current state of this `FlipFlopModule` prior to pressing the
         * button.
         */
        public void prePress() {
            cachedState = state;
            ++segmentLength;
        }

        /*
         * Update the loop tracking for this `FlipFlopModule` after all pulses
         * originating from a single button-press have been processed.
         */
        public void postPress() {
            if (state == cachedState || loopLength != -1)
                return;

            cumulativeSegmentLength += segmentLength;

            if (!isPowerOf2(segmentLength))
                this.loopLength = cumulativeSegmentLength;

            segmentLength = 0;
        }

        // Static Helper Methods

        /*
         * Check if a given number is a power of 2.
         */
        private static boolean isPowerOf2(final int loopLength) {
            int cmp = 1;
            while (cmp <= loopLength) {
                if ((cmp <<= 1) == loopLength) return true;
            }
            return false;
        }

    }

    /*
     * A `Module` that acts as NAND gate for multiple inputs.
     */
    private static final class ConjunctionModule extends AbstractModule implements Module {

        // Private Members

        private final Map<String, PulseType> upstreams = new HashMap<>();

        // Constructors

        ConjunctionModule(final String name, final List<String> downstream) {
            super(name, downstream);
        }

        // Module Methods

        @Override
        public void init(final String upstream) {
            upstreams.put(upstream, PulseType.LOW);
        }

        @Override
        public List<Pulse> accept(final Pulse pulse) {
            upstreams.put(pulse.from, pulse.type);

            final PulseType type = upstreams.values().stream().allMatch(val -> val == PulseType.HIGH)
                    ? PulseType.LOW
                    : PulseType.HIGH;
            return sendPulse(type);
        }

    }

    /*
     * A `Module` that forwards on `Pulse`s.
     */
    private static final class BroadcasterModule extends AbstractModule implements Module {

        // Constructors

        BroadcasterModule(final String name, final List<String> downstream) {
            super(name, downstream);
        }

        // Module Methods

        @Override
        public List<Pulse> accept(final Pulse pulse) {
            return sendPulse(pulse.type);
        }

    }

    /*
     * A `Module` that just receives `Pulse`s but does nothing with them.
     */
    private record Sink(String name) implements Module {

        // Module Methods

        @Override
        public List<String> downstream() {
            return Collections.emptyList();
        }

        @Override
        public void init(final String upstream) {

        }

        @Override
        public List<Pulse> accept(final Pulse pulse) {
            return Collections.emptyList();
        }

    }


}

