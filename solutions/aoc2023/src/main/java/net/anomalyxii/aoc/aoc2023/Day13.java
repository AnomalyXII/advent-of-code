package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;

import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 13: Point of Incidence.
 */
@Solution(year = 2023, day = 13, title = "Point of Incidence")
public class Day13 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * With your help, the hot springs team locates an appropriate spring which launches you neatly and precisely up to the edge of <em>Lava Island</em>.
     * <p>
     * There's just one problem: you don't see any <em>lava</em>.
     * <p>
     * You <em>do</em> see a lot of ash and igneous rock; there are even what look like gray mountains scattered around.
     * After a while, you make your way to a nearby cluster of mountains only to discover that the valley between them is completely full of large <em>mirrors</em>.
     * Most of the mirrors seem to be aligned in a consistent way; perhaps you should head in that direction?
     * <p>
     * As you move through the valley of mirrors, you find that several of them have fallen from the large metal frames keeping them in place.
     * The mirrors are extremely flat and shiny, and many of the fallen mirrors have lodged into the ash at strange angles.
     * Because the terrain is all one color, it's hard to tell where it's safe to walk or where you're about to run into a mirror.
     * <p>
     * You note down the patterns of ash (<code>.</code>) and rocks (<code>#</code>) that you see as you walk (your puzzle input); perhaps by carefully analyzing these patterns, you can figure out where the mirrors are!
     * <p>
     * For example:
     * <pre>
     * #.##..##.
     * ..#.##.#.
     * ##......#
     * ##......#
     * ..#.##.#.
     * ..##..##.
     * #.#.##.#.
     *
     * #...##..#
     * #....#..#
     * ..##..###
     * #####.##.
     * #####.##.
     * ..##..###
     * #....#..#
     * </pre>
     * <p>
     * To find the reflection in each pattern, you need to find a perfect reflection across either a horizontal line between two rows or across a vertical line between two columns.
     * <p>
     * In the first pattern, the reflection is across a vertical line between two columns; arrows on each of the two columns point at the line between the columns:
     * <pre>
     * 123456789
     *     &gt;&lt;
     * #.##..##.
     * ..#.##.#.
     * ##......#
     * ##......#
     * ..#.##.#.
     * ..##..##.
     * #.#.##.#.
     *     &gt;&lt;
     * 123456789
     * </pre>
     * <p>
     * In this pattern, the line of reflection is the vertical line between columns 5 and 6.
     * Because the vertical line is not perfectly in the middle of the pattern, part of the pattern (column 1) has nowhere to reflect onto and can be ignored; every other column has a reflected column within the pattern and must match exactly: column 2 matches column 9, column 3 matches 8, 4 matches 7, and 5 matches 6.
     * <p>
     * The second pattern reflects across a horizontal line instead:
     * <pre>
     * 1 #...##..# 1
     * 2 #....#..# 2
     * 3 ..##..### 3
     * 4v#####.##.v4
     * 5^#####.##.^5
     * 6 ..##..### 6
     * 7 #....#..# 7
     * </pre>
     * <p>
     * This pattern reflects across the horizontal line between rows 4 and 5.
     * Row 1 would reflect with a hypothetical row 8, but since that's not in the pattern, row 1 doesn't need to match anything.
     * The remaining rows match: row 2 matches row 7, row 3 matches row 6, and row 4 matches row 5.
     * <p>
     * To <em>summarize</em> your pattern notes, add up <em>the number of columns</em> to the left of each vertical line of reflection; to that, also add <em>100 multiplied by the number of rows</em> above each horizontal line of reflection.
     * In the above example, the first pattern's vertical line has <code>5</code> columns to its left and the second pattern's horizontal line has <code>4</code> rows above it, a total of <code><em>405</em></code>.
     * <p>
     * Find the line of reflection in each of the patterns in your notes.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What number do you get after summarizing all of your notes?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        return context.streamBatches()
                .mapToInt(this::calculateReflectionScore)
                .sum();
    }

    /**
     * You resume walking through the valley of mirrors and - <em>SMACK!</em> - run directly into one.
     * Hopefully <span title="Sorry, Nobody saw that.">nobody</span> was watching, because that must have been pretty embarrassing.
     * <p>
     * Upon closer inspection, you discover that every mirror has exactly one <em>smudge</em>: exactly one <code>.</code> or <code>#</code> should be the opposite type.
     * <p>
     * In each pattern, you'll need to locate and fix the smudge that causes a <em>different reflection line</em> to be valid.
     * (The old reflection line won't necessarily continue being valid after the smudge is fixed.)
     * <p>
     * Here's the above example again:
     * <pre>
     * #.##..##.
     * ..#.##.#.
     * ##......#
     * ##......#
     * ..#.##.#.
     * ..##..##.
     * #.#.##.#.
     *
     * #...##..#
     * #....#..#
     * ..##..###
     * #####.##.
     * #####.##.
     * ..##..###
     * #....#..#
     * </pre>
     * <p>
     * The first pattern's smudge is in the top-left corner.
     * If the top-left <code>#</code> were instead <code>.</code>, it would have a different, horizontal line of reflection:
     * <pre>
     * 1 ..##..##.
     * 1
     * 2 ..#.##.#.
     * 2
     * 3v##......#v3
     * 4^##......#^4
     * 5 ..#.##.#.
     * 5
     * 6 ..##..##.
     * 6
     * 7 #.#.##.#.
     * 7
     * </pre>
     * <p>
     * With the smudge in the top-left corner repaired, a new horizontal line of reflection between rows 3 and 4 now exists.
     * Row 7 has no corresponding reflected row and can be ignored, but every other row matches exactly: row 1 matches row 6, row 2 matches row 5, and row 3 matches row 4.
     * <p>
     * In the second pattern, the smudge can be fixed by changing the fifth symbol on row 2 from <code>.</code> to <code>#</code>:
     * <pre>
     * 1v#...##..#v1
     * 2^#...##..#^2
     * 3 ..##..### 3
     * 4 #####.##.
     * 4
     * 5 #####.##.
     * 5
     * 6 ..##..### 6
     * 7 #....#..# 7
     * </pre>
     * <p>
     * Now, the pattern has a different horizontal line of reflection between rows 1 and 2.
     * <p>
     * Summarize your notes as before, but instead use the new different reflection lines.
     * In this example, the first pattern's new horizontal line has 3 rows above it and the second pattern's new horizontal line has 1 row above it, summarizing to the value <code><em>400</em></code>.
     * <p>
     * In each pattern, fix the smudge and find the different line of reflection.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What number do you get after summarizing the new reflection line in each pattern in your notes?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        return context.streamBatches()
                .mapToInt(this::calculateReflectionWithSmudgeScore)
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        return context.streamBatches()
                .reduce(
                        IntTuple.NULL,
                        (tup, line) -> tup.add(
                                calculateReflectionScore(line),
                                calculateReflectionWithSmudgeScore(line)
                        ),
                        IntTuple::add
                );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Calculate the "score" of a reflection.
     */
    private int calculateReflectionScore(final List<String> image) {

        final OptionalInt horizontalReflection = findHorizontalReflection(image);
        if (horizontalReflection.isPresent())
            return 100 * (horizontalReflection.getAsInt() + 1);

        final OptionalInt verticalReflection = findVerticalReflection(image);
        if (verticalReflection.isPresent())
            return verticalReflection.getAsInt() + 1;

        throw new IllegalStateException("No reflection found :(");
    }

    /*
     * Calculate the "score" of a reflection that has a single smudge on it.
     */
    private int calculateReflectionWithSmudgeScore(final List<String> image) {
        final OptionalInt horizontalReflection = findHorizontalReflectionWithSmudge(image);
        if (horizontalReflection.isPresent())
            return 100 * (horizontalReflection.getAsInt() + 1);

        final OptionalInt verticalReflection = findVerticalReflectionWithSmudge(image);
        if (verticalReflection.isPresent())
            return verticalReflection.getAsInt() + 1;

        throw new IllegalStateException("No reflection found :(");
    }

    /*
     * Find a horizontal reflection point.
     */
    private OptionalInt findHorizontalReflection(final List<String> image) {

        int border = -1;
        outer:
        for (int r = 0; r < image.size() - 1; r++) {

            final String row = image.get(r);
            if (row.equals(image.get(r + 1))) {
                for (int rt = r, rb = r + 1; rt >= 0 && rb < image.size(); rt--, rb++) {
                    if (!image.get(rt).equals(image.get(rb)))
                        continue outer;
                }

                border = r;
            }

        }

        return border == -1 ? OptionalInt.empty() : OptionalInt.of(border);

    }

    /*
     * Find a horizontal reflection point accounting for a single smudge.
     */
    private OptionalInt findHorizontalReflectionWithSmudge(final List<String> image) {

        int border = -1;
        outer:
        for (int r = 0; r < image.size() - 1; r++) {

            final String row = image.get(r);

            if (row.equals(image.get(r + 1)) || mightHaveASmudge(row, image.get(r + 1))) {
                boolean mightHaveSmudge = true;
                for (int rt = r, rb = r + 1; rt >= 0 && rb < image.size(); rt--, rb++) {
                    if (!image.get(rt).equals(image.get(rb)))
                        if (mightHaveSmudge && mightHaveASmudge(image.get(rt), image.get(rb)))
                            mightHaveSmudge = false;
                        else continue outer;
                }

                if (!mightHaveSmudge)
                    border = r;
            }

        }

        return border == -1 ? OptionalInt.empty() : OptionalInt.of(border);

    }

    /*
     * Find a vertical reflection point.
     */
    private OptionalInt findVerticalReflection(final List<String> image) {
        final List<String> rotated = rotateImage90(image);
        return findHorizontalReflection(rotated);
    }

    /*
     * Find a vertical reflection point, accounting for a single smudge.
     */
    private OptionalInt findVerticalReflectionWithSmudge(final List<String> image) {
        final List<String> rotated = rotateImage90(image);
        return findHorizontalReflectionWithSmudge(rotated);
    }

    /*
     * Rotate an image 90 degrees.
     */
    private static List<String> rotateImage90(final List<String> image) {
        final int width = image.getFirst().length();
        return IntStream.range(0, width)
                .mapToObj(c -> image.stream().map(row -> Character.toString(row.charAt(c))).collect(Collectors.joining()))
                .toList();
    }

    /*
     * Check if an two lines might be reflected, accounting for a smudge.
     */
    private static boolean mightHaveASmudge(final String top, final String bottom) {
        assert top.length() == bottom.length();

        int differences = 0;
        for (int idx = 0; idx < top.length(); idx++)
            if (top.charAt(idx) != bottom.charAt(idx)) ++differences;

        return differences == 1;
    }

}

