package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 3: Gear Ratios.
 */
@Solution(year = 2023, day = 3, title = "Gear Ratios")
public class Day3 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You and the Elf eventually reach a <a href="https://en.wikipedia.org/wiki/Gondola_lift" target="_blank">gondola lift</a> station; he says the gondola lift will take you up to the <em>water source</em>, but this is as far as he can bring you.
     * You go inside.
     * <p>
     * It doesn't take long to find the gondolas, but there seems to be a problem: they're not moving.
     * <p>
     * "Aaah!"
     * <p>
     * You turn around to see a slightly-greasy Elf with a wrench and a look of surprise.
     * "Sorry, I wasn't expecting anyone!
     * The gondola lift isn't working right now; it'll still be a while before I can fix it." You offer to help.
     * <p>
     * The engineer explains that an engine part seems to be missing from the engine, but nobody can figure out which one.
     * If you can <em>add up all the part numbers</em> in the engine schematic, it should be easy to work out which part is missing.
     * <p>
     * The engine schematic (your puzzle input) consists of a visual representation of the engine.
     * There are lots of numbers and symbols you don't really understand, but apparently <em>any number adjacent to a symbol</em>, even diagonally, is a "part number" and should be included in your sum.
     * (Periods (<code>.</code>) do not count as a symbol.)
     * <p>
     * Here is an example engine schematic:
     * <pre>
     * 467..114..
     * ...*......
     * ..35..633.
     * ......#...
     * 617*......
     * .....+.58.
     * ..592.....
     * ......755.
     * ...$.*....
     * .664.598..
     * </pre>
     * <p>
     * In this schematic, two numbers are <em>not</em> part numbers because they are not adjacent to a symbol: <code>114</code> (top right) and <code>58</code> (middle right).
     * Every other number is adjacent to a symbol and so <em>is</em> a part number; their sum is <code><em>4361</em></code>.
     * <p>
     * Of course, the actual engine schematic is much larger.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of all of the part numbers in the engine schematic?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final Schematic schematic = Schematic.parse(context.stream());
        return schematic.symbols.keySet().stream()
                .flatMap(Coordinate::neighbours)
                .map(schematic.parts::get)
                .filter(Objects::nonNull)
                .distinct()
                .mapToInt(part -> part.id)
                .sum();
    }

    /**
     * The engineer finds the missing part and installs it in the engine!
     * As the engine springs to life, you jump in the closest gondola, finally ready to ascend to the water source.
     * <p>
     * You don't seem to be going very fast, though.
     * Maybe something is still wrong?
     * Fortunately, the gondola has a phone labeled "help", so you pick it up and the engineer answers.
     * <p>
     * Before you can explain the situation, she suggests that you look out the window.
     * There stands the engineer, holding a phone in one hand and waving with the other.
     * You're going so slowly that you haven't even left the station.
     * You exit the gondola.
     * <p>
     * The missing part wasn't the only issue - one of the gears in the engine is wrong.
     * A <em>gear</em> is any <code>*</code> symbol that is adjacent to <em>exactly two part numbers</em>.
     * Its <em>gear ratio</em> is the result of <span title="They're magic gears.">multiplying</span> those two numbers together.
     * <p>
     * This time, you need to find the gear ratio of every gear and add them all up so that the engineer can figure out which gear needs to be replaced.
     * <p>
     * Consider the same engine schematic again:
     * <pre>
     * 467..114..
     * ...*......
     * ..35..633.
     * ......#...
     * 617*......
     * .....+.58.
     * ..592.....
     * ......755.
     * ...$.*....
     * .664.598..
     * </pre>
     * <p>
     * In this schematic, there are <em>two</em> gears.
     * The first is in the top left; it has part numbers <code>467</code> and <code>35</code>, so its gear ratio is <code>16345</code>.
     * The second gear is in the lower right; its gear ratio is <code>451490</code>.
     * (The <code>*</code> adjacent to <code>617</code> is <em>not</em> a gear because it is only adjacent to one part number.)
     * Adding up all of the gear ratios produces <code><em>467835</em></code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of all of the gear ratios in your engine schematic?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final Schematic schematic = Schematic.parse(context.stream());
        return schematic.symbols.entrySet().stream()
                .filter(symbol -> symbol.getValue() == '*')
                .map(Map.Entry::getKey)
                .map(coord -> coord.neighbours()
                        .map(schematic.parts::get)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toCollection(LinkedHashSet::new)))
                .filter(neighbours -> neighbours.size() == 2)
                .mapToInt(neighbours -> neighbours.getFirst().id * neighbours.getLast().id)
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final Schematic schematic = Schematic.parse(context.stream());
        final Set<MachinePart> uniqueParts = new HashSet<>();
        return schematic.symbols.entrySet().stream()
                .reduce(
                        LongTuple.NULL,
                        (tup, entry) -> {
                            final Coordinate coord = entry.getKey();
                            final SequencedSet<MachinePart> parts = coord.neighbours()
                                    .map(schematic.parts::get)
                                    .filter(Objects::nonNull)
                                    .collect(Collectors.toCollection(LinkedHashSet::new));

                            final int part1 = parts.stream()
                                    .filter(uniqueParts::add)
                                    .mapToInt(part -> part.id)
                                    .sum();

                            final int part2 = parts.size() == 2
                                    ? parts.getFirst().id * parts.getLast().id
                                    : 0;

                            return tup.add(part1, part2);
                        },
                        LongTuple::add
                );
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Parse a schematic, identifying all `MachinePart`s and symbols.
     */
    private record Schematic(Map<Coordinate, Character> symbols, Map<Coordinate, MachinePart> parts) {

        // Static Helper Methods

        /*
         * Extract the `Schematic` plan from the challenge input.
         */
        static Schematic parse(final Stream<String> lines) {
            final Map<Coordinate, Character> symbols = new HashMap<>();
            final Map<Coordinate, MachinePart> parts = new HashMap<>();

            final Iterator<String> iteratorY = lines.iterator();
            for (int y = 0; iteratorY.hasNext(); y++)
                parseLine(y, iteratorY, symbols, parts);

            return new Schematic(symbols, parts);
        }

        /*
         * Parse a line in a `Schematic`.
         */
        private static void parseLine(
                final int y,
                final Iterator<String> iteratorY,
                final Map<Coordinate, Character> symbols,
                final Map<Coordinate, MachinePart> parts
        ) {
            int value = -1;
            final SequencedSet<Coordinate> coordinates = new LinkedHashSet<>();

            final String line = iteratorY.next();
            for (int x = 0; x < line.length(); x++) {
                final int chr = line.charAt(x);
                final Coordinate current = new Coordinate(x, y);

                if (!Character.isDigit(chr)) {
                    if (chr != '.')
                        symbols.put(current, (char) chr);

                    if (value > -1) {
                        final MachinePart part = new MachinePart(value, coordinates.getFirst(), coordinates.getLast());
                        coordinates.forEach(coord -> parts.put(coord, part));

                        value = -1;
                        coordinates.clear();
                    }
                    continue;
                }

                if (value < 0)
                    value = 0;

                final int val = chr - '0';
                value = (value * 10) + val;
                coordinates.add(current);
            }

            if (value > -1) {
                final MachinePart part = new MachinePart(value, coordinates.getFirst(), coordinates.getLast());
                coordinates.forEach(coord -> parts.put(coord, part));
            }
        }

    }

    /*
     * Store details of a machine part.
     */
    private record MachinePart(int id, Coordinate start, Coordinate end) {
    }

}

