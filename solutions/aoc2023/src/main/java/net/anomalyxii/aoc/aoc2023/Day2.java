package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;

import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.Math.max;
import static java.util.Arrays.stream;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 2: Cube Conundrum.
 */
@Solution(year = 2023, day = 2, title = "Cube Conundrum")
public class Day2 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You're launched high into the atmosphere!
     * The apex of your trajectory just barely reaches the surface of a large island floating in the sky.
     * You gently land in a fluffy pile of leaves.
     * It's quite cold, but you don't see much snow.
     * An Elf runs over to greet you.
     * <p>
     * The Elf explains that you've arrived at <em>Snow Island</em> and apologizes for the lack of snow.
     * He'll be happy to explain the situation, but it's a bit of a walk, so you have some time.
     * They don't get many visitors up here; <span title="No, the Elf's name is not 'WOPR'. It's Joshua.">would you like to play a game</span> in the meantime?
     * <p>
     * As you walk, the Elf shows you a small bag and some cubes which are either red, green, or blue.
     * Each time you play this game, he will hide a secret number of cubes of each color in the bag, and your goal is to figure out information about the number of cubes.
     * <p>
     * To get information, once a bag has been loaded with cubes, the Elf will reach into the bag, grab a handful of random cubes, show them to you, and then put them back in the bag.
     * He'll do this a few times per game.
     * <p>
     * You play several games and record the information from each game (your puzzle input).
     * Each game is listed with its ID number (like the <code>11</code> in <code>Game 11: ...</code>) followed by a semicolon-separated list of subsets of cubes that were revealed from the bag (like <code>3 red, 5 green, 4 blue</code>).
     * <p>
     * For example, the record of a few games might look like this:
     * <pre>
     * Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
     * Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
     * Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
     * Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
     * Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
     * </pre>
     * <p>
     * In game 1, three sets of cubes are revealed from the bag (and then put back again).
     * The first set is 3 blue cubes and 4 red cubes; the second set is 1 red cube, 2 green cubes, and 6 blue cubes; the third set is only 2 green cubes.
     * <p>
     * The Elf would first like to know which games would have been possible if the bag contained <em>only 12 red cubes, 13 green cubes, and 14 blue cubes</em>?
     * <p>
     * In the example above, games 1, 2, and 5 would have been <em>possible</em> if the bag had been loaded with that configuration.
     * However, game 3 would have been <em>impossible</em> because at one point the Elf showed you 20 red cubes at once; similarly, game 4 would also have been <em>impossible</em> because the Elf showed you 15 blue cubes at once.
     * If you add up the IDs of the games that would have been possible, you get <code><em>8</em></code>.
     * <p>
     * Determine which games would have been possible if the bag had been loaded with only 12 red cubes, 13 green cubes, and 14 blue cubes.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the IDs of those games?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .map(Game::parse)
                .filter(game -> game.isPossible(12, 13, 14))
                .mapToInt(game -> game.id)
                .sum();
    }

    /**
     * The Elf says they've stopped producing snow because they aren't getting any <em>water</em>!
     * He isn't sure why the water stopped; however, he can show you how to get to the water source to check it out for yourself.
     * It's just up ahead!
     * <p>
     * As you continue your walk, the Elf poses a second question: in each game you played, what is the <em>fewest number of cubes of each color</em> that could have been in the bag to make the game possible?
     * <p>
     * Again consider the example games from earlier:
     * <pre>
     * Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
     * Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
     * Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
     * Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
     * Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
     * </pre>
     * <ul>
     * <li>
     * In game 1, the game could have been played with as few as 4 red, 2 green, and 6 blue cubes.
     * If any color had even one fewer cube, the game would have been impossible.
     * </li>
     * <li>Game 2 could have been played with a minimum of 1 red, 3 green, and 4 blue cubes.<li>
     * <li>Game 3 must have been played with at least 20 red, 13 green, and 6 blue cubes.</li>
     * <li>Game 4 required at least 14 red, 3 green, and 15 blue cubes.</li>
     * <li>Game 5 needed no fewer than 6 red, 3 green, and 2 blue cubes in the bag.</li>
     * </ul>
     * <p>
     * The <em>power</em> of a set of cubes is equal to the numbers of red, green, and blue cubes multiplied together.
     * The power of the minimum set of cubes in game 1 is <code>48</code>.
     * In games 2-5 it was <code>12</code>, <code>1560</code>, <code>630</code>, and <code>36</code>, respectively.
     * Adding up these five powers produces the sum <code><em>2286</em></code>.
     * <p>
     * For each game, find the minimum set of cubes that must have been present.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the power of these sets?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return context.stream()
                .map(Game::parse)
                .mapToInt(Game::calculatePower)
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        return context.stream()
                .map(Game::parse)
                .reduce(
                        LongTuple.NULL,
                        (tup, game) ->
                                game.isPossible(12, 13, 14)
                                        ? tup.add(game.id, game.calculatePower())
                                        : tup.add(0, game.calculatePower()),
                        LongTuple::add
                );
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * The colour of a ball.
     */
    private enum Colour {
        RED,
        GREEN,
        BLUE,

        // End of constants
        ;
    }

    /*
     * Represents multiple draws within a single game round.
     */
    private record Game(int id, List<Sample> samples) {

        // Helper Methods

        /**
         * Decide whether this {@link Game} would have been possible with the
         * specified number of red, green and blue balls.
         *
         * @param red   the maximum number of red balls available
         * @param green the maximum number of green balls available
         * @param blue  the maximum number of blue balls available
         * @return {@literal true} if this {@link Game} would have been possible; {@literal false} otherwise
         */
        public boolean isPossible(final int red, final int green, final int blue) {
            return samples.stream().noneMatch(sample -> sample.exceeds(red, green, blue));
        }

        /**
         * Calculate the "power" of this {@link Game}.
         * <p>
         * The "power" is the product of the minimum number of red, blue and
         * green balls that would have been needed for the {@link Game} to be
         * possible.
         *
         * @return the "power" of this {@link Game}
         */
        public int calculatePower() {
            return samples.stream()
                    .reduce(Sample::reduce)
                    .map(Sample::power)
                    .orElse(0);
        }

        // Static Helper Methods

        /**
         * Parse a {@link Game} from a textual representation.
         *
         * @param line the text
         * @return the {@link Game}
         */
        static Game parse(final String line) {
            final String[] parts = line.split(": *", 2);
            final String[] identifier = parts[0].split(" ");

            return new Game(
                    Integer.parseInt(identifier[1]),
                    stream(parts[1].split("; *"))
                            .map(Sample::parse)
                            .toList()
            );
        }
    }

    /*
     * A single sample of balls drawn during a `Game`.
     */
    private record Sample(Map<Colour, Integer> counts) {

        // Helper Methods

        /**
         * Check if this {@link Sample} exceeds a given number of balls.
         *
         * @param red   the maximum number of red balls
         * @param green the maximum number of green balls
         * @param blue  the maximum number of blue balls
         * @return {@literal true} if the red, green or blue balls of this sample exceeds the maximum allowed; {@literal false} otherwise
         */
        public boolean exceeds(final int red, final int green, final int blue) {
            return counts.getOrDefault(Colour.RED, 0) > red
                    || counts.getOrDefault(Colour.GREEN, 0) > green
                    || counts.getOrDefault(Colour.BLUE, 0) > blue;
        }

        /**
         * Combine this {@link Sample} with another to produce one that contains
         * the greatest number of balls sampled for each {@link Colour}.
         *
         * @param other the other {@link Sample}
         * @return the combined {@link Sample}
         */
        public Sample reduce(final Sample other) {
            final Map<Colour, Integer> reduced = new EnumMap<>(Colour.class);
            reduced.put(Colour.RED, max(red(), other.red()));
            reduced.put(Colour.GREEN, max(green(), other.green()));
            reduced.put(Colour.BLUE, max(blue(), other.blue()));
            return new Sample(reduced);
        }

        /**
         * Calculate the "power" of this {@link Sample}.
         *
         * @return the "power"
         */
        public int power() {
            return counts.getOrDefault(Colour.RED, 1)
                    * counts.getOrDefault(Colour.GREEN, 1)
                    * counts.getOrDefault(Colour.BLUE, 1);
        }

        // Private Helper Methods

        /*
         * Get the number of red balls in this `Sample`.
         */
        private int red() {
            return counts.getOrDefault(Colour.RED, 0);
        }

        /*
         * Get the number of green balls in this `Sample`.
         */
        private int green() {
            return counts.getOrDefault(Colour.GREEN, 0);
        }

        /*
         * Get the number of blue balls in this `Sample`.
         */
        private int blue() {
            return counts.getOrDefault(Colour.BLUE, 0);
        }

        // Static Helper Methods

        /**
         * Parse a {@link Sample} from a textual representation.
         *
         * @param line the text
         * @return the {@link Sample}
         */
        static Sample parse(final String line) {
            final String[] parts = line.trim().split(", *");
            return new Sample(
                    stream(parts)
                            .map(part -> part.split(" ", 2))
                            .collect(Collectors.groupingBy(
                                    part -> Colour.valueOf(part[1].toUpperCase(Locale.UK)),
                                    () -> new EnumMap<>(Colour.class),
                                    Collectors.summingInt(part -> Integer.parseInt(part[0]))
                            ))
            );
        }
    }

}

