package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Direction;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.PriorityQueue;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 17: Clumsy Crucible.
 */
@Solution(year = 2023, day = 17, title = "Clumsy Crucible")
public class Day17 {

    /*
     * No value set.
     */
    private static final int NO_VAL = Integer.MAX_VALUE;

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The lava starts flowing rapidly once the Lava Production Facility is operational. As you <span title="see you soon?">leave</span>, the reindeer offers you a parachute, allowing you to quickly reach Gear Island.
     * <p>
     * As you descend, your bird's-eye view of Gear Island reveals why you had trouble finding anyone on your way up: half of Gear Island is empty, but the half below you is a giant factory city!
     * <p>
     * You land near the gradually-filling pool of lava at the base of your new <em>lavafall</em>. Lavaducts will eventually carry the lava throughout the city, but to make use of it immediately, Elves are loading it into large <a href="https://en.wikipedia.org/wiki/Crucible" target="_blank">crucibles</a> on wheels.
     * <p>
     * The crucibles are top-heavy and pushed by hand. Unfortunately, the crucibles become very difficult to steer at high speeds, and so it can be hard to go in a straight line for very long.
     * <p>
     * To get Desert Island the machine parts it needs as soon as possible, you'll need to find the best way to get the crucible <em>from the lava pool to the machine parts factory</em>. To do this, you need to minimize <em>heat loss</em> while choosing a route that doesn't require the crucible to go in a <em>straight line</em> for too long.
     * <p>
     * Fortunately, the Elves here have a map (your puzzle input) that uses traffic patterns, ambient temperature, and hundreds of other parameters to calculate exactly how much heat loss can be expected for a crucible entering any particular city block.
     * <p>
     * For example:
     * <pre>
     * 2413432311323
     * 3215453535623
     * 3255245654254
     * 3446585845452
     * 4546657867536
     * 1438598798454
     * 4457876987766
     * 3637877979653
     * 4654967986887
     * 4564679986453
     * 1224686865563
     * 2546548887735
     * 4322674655533
     * </pre>
     * <p>
     * Each city block is marked by a single digit that represents the <em>amount of heat loss if the crucible enters that block</em>. The starting point, the lava pool, is the top-left city block; the destination, the machine parts factory, is the bottom-right city block. (Because you already start in the top-left block, you don't incur that block's heat loss unless you leave that block and then return to it.)
     * <p>
     * Because it is difficult to keep the top-heavy crucible going in a straight line for very long, it can move <em>at most three blocks</em> in a single direction before it must turn 90 degrees left or right. The crucible also can't reverse direction; after entering each city block, it may only turn left, continue straight, or turn right.
     * <p>
     * One way to <em>minimize heat loss</em> is this path:
     * <pre>
     * 2<em>&gt;</em><em>&gt;</em>34<em>^</em><em>&gt;</em><em>&gt;</em><em>&gt;</em>1323
     * 32<em>v</em><em>&gt;</em><em>&gt;</em><em>&gt;</em>35<em>v</em>5623
     * 32552456<em>v</em><em>&gt;</em><em>&gt;</em>54
     * 3446585845<em>v</em>52
     * 4546657867<em>v</em><em>&gt;</em>6
     * 14385987984<em>v</em>4
     * 44578769877<em>v</em>6
     * 36378779796<em>v</em><em>&gt;</em>
     * 465496798688<em>v</em>
     * 456467998645<em>v</em>
     * 12246868655<em>&lt;</em><em>v</em>
     * 25465488877<em>v</em>5
     * 43226746555<em>v</em><em>&gt;</em>
     * </pre>
     * <p>
     * This path never moves more than three consecutive blocks in the same direction and incurs a heat loss of only <code><em>102</em></code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Directing the crucible from the lava pool to the machine parts factory, but not moving more than three consecutive blocks in the same direction, what is the least heat loss it can incur?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final Grid grid = context.readGrid(c -> c - '0');
        final Memo memo = Memo.forCrucible(grid);
        return solve(grid, memo);
    }

    /**
     * The crucibles of lava simply aren't large enough to provide an adequate supply of lava to the machine parts factory. Instead, the Elves are going to upgrade to <em>ultra crucibles</em>.
     * <p>
     * Ultra crucibles are even more difficult to steer than normal crucibles. Not only do they have trouble going in a straight line, but they also have trouble turning!
     * <p>
     * Once an ultra crucible starts moving in a direction, it needs to move <em>a minimum of four blocks</em> in that direction before it can turn (or even before it can stop at the end). However, it will eventually start to get wobbly: an ultra crucible can move a maximum of <em>ten consecutive blocks</em> without turning.
     * <p>
     * In the above example, an ultra crucible could follow this path to minimize heat loss:
     * <pre>
     * 2<em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em>1323
     * 32154535<em>v</em>5623
     * 32552456<em>v</em>4254
     * 34465858<em>v</em>5452
     * 45466578<em>v</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em>
     * 143859879845<em>v</em>
     * 445787698776<em>v</em>
     * 363787797965<em>v</em>
     * 465496798688<em>v</em>
     * 456467998645<em>v</em>
     * 122468686556<em>v</em>
     * 254654888773<em>v</em>
     * 432267465553<em>v</em>
     * </pre>
     * <p>
     * In the above example, an ultra crucible would incur the minimum possible heat loss of <code><em>94</em></code>.
     * <p>
     * Here's another example:
     * <pre>
     * 111111111111
     * 999999999991
     * 999999999991
     * 999999999991
     * 999999999991
     * </pre>
     * <p>
     * Sadly, an ultra crucible would need to take an unfortunate path like this one:
     * <pre>
     * 1<em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em>1111
     * 9999999<em>v</em>9991
     * 9999999<em>v</em>9991
     * 9999999<em>v</em>9991
     * 9999999<em>v</em><em>&gt;</em><em>&gt;</em><em>&gt;</em><em>&gt;</em>
     * </pre>
     * <p>
     * This route causes the ultra crucible to incur the minimum possible heat loss of <code><em>71</em></code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Directing the <em>ultra crucible</em> from the lava pool to the machine parts factory, what is the least heat loss it can incur?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Grid grid = context.readGrid(c -> c - '0');

        final Memo memo = Memo.forUltraCrucible(grid);
        return solve(grid, memo);
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final Grid grid = context.readGrid(c -> c - '0');

        final Memo part1 = Memo.forCrucible(grid);
        final Memo part2 = Memo.forUltraCrucible(grid);

        return new IntTuple(solve(grid, part1), solve(grid, part2));
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Calculate the path from top-left to bottom-right that results in the
     * least amount of heat loss.
     */
    private static int solve(final Grid grid, final Memo memo) {
        final Coordinate from = grid.min();
        final Coordinate to = grid.max();

        memo.dist[memo.offset(from, Direction.DOWN, 0)] = 0;
        memo.dist[memo.offset(from, Direction.RIGHT, 0)] = 0;

        final PriorityQueue<CoordinateWithPriority> queue = new PriorityQueue<>(CoordinateWithPriority.COMPARATOR);
        queue.add(prio(from, Direction.DOWN, 0, 0));
        queue.add(prio(from, Direction.RIGHT, 0, 0));

        CoordinateWithPriority uk;
        while ((uk = queue.poll()) != null) {
            final Coordinate uc = uk.coordinate;
            final Direction ud = uk.direction;
            final int un = uk.count;
            final int up = uk.priority;

            final int priority = memo.get(uk);
            if (up > priority) continue;

            Direction.forEach(direction -> {
                final CoordinateWithPriority next = memo.next(grid, direction, uc, ud, un, up);
                if (next == null) return;

                final int neighbourPriority = memo.get(next);
                if (next.priority < neighbourPriority) {
                    memo.update(uc, next);
                    queue.add(next);
                }
            });
        }

        return memo.shortestPath(to);
    }

    /*
     * Wrap the given `Coordinate` and `Priority` into a
     * `CoordinateWithPriority`.
     */
    private static CoordinateWithPriority prio(
            final Coordinate coordinate,
            final Direction direction,
            final int count,
            final int priority
    ) {
        return new CoordinateWithPriority(coordinate, direction, count, priority);
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A combination of a `Coordinate` and the priority.
     */
    private record CoordinateWithPriority(Coordinate coordinate, Direction direction, int count, int priority) {

        static final Comparator<CoordinateWithPriority> COMPARATOR = Comparator.comparingLong((CoordinateWithPriority pp) -> pp.priority);

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final CoordinateWithPriority that = (CoordinateWithPriority) o;
            return priority == that.priority
                    && direction == that.direction
                    && count == that.count
                    && Objects.equals(coordinate, that.coordinate);
        }

        @Override
        public int hashCode() {
            return Objects.hash(coordinate, direction, count, priority);
        }

    }

    /*
     * Holds memoised data for calculating the shortest path.
     */
    private record Memo(int[] dist, Coordinate[] prev, int width, int min, int max) {

        // Helper Methods

        /*
         * Calculate the next `Coordinate` to visit, and the priority of that
         * visit compared to other `Coordinate`s.
         */
        CoordinateWithPriority next(
                final Grid grid,
                final Direction nextDirection,
                final Coordinate currentCoordinate,
                final Direction currentDirection,
                final int currentDirectionCount,
                final int currentPriority
        ) {
            if (nextDirection == currentDirection.reverse()) return null;
            if (currentDirection == nextDirection) {
                final Coordinate neighbour = currentCoordinate.adjustBy(nextDirection);
                if (!grid.contains(neighbour)) return null;

                final int nextCount = currentDirectionCount + 1;
                if (nextCount == (max - min) + 1) return null;

                final int score = grid.get(neighbour);
                return prio(neighbour, nextDirection, nextCount, score + currentPriority);
            }

            int sum = 0;
            Coordinate next = currentCoordinate;
            for (int i = 0; i < min; i++) {
                next = next.adjustBy(nextDirection);
                if (!grid.contains(next)) return null;

                sum += grid.get(next);
            }

            return prio(next, nextDirection, 0, sum + currentPriority);
        }

        /*
         * Wrap the given `Coordinate` and `Priority` into a
         * `CoordinateWithPriority`.
         */
        int offset(
                final Coordinate coordinate,
                final Direction direction,
                final int count
        ) {
            final int cidx = coordinate.y() * width + coordinate.x();
            final int didx = cidx * 4 + direction.ordinal();
            final int boundary = (max - min) + 1;
            assert count >= 0 && count <= boundary : "Count: 0 <= " + count + " <= " + boundary;
            return (didx * boundary) + count;
        }

        /*
         * Get the currently set priority of a given `Coordinate`.
         */
        int get(final CoordinateWithPriority next) {
            return dist[offset(next.coordinate, next.direction, next.count)];
        }

        /*
         * Update the memo with the result of a given visit.
         */
        void update(final Coordinate current, final CoordinateWithPriority next) {
            dist[offset(next.coordinate, next.direction, next.count)] = next.priority;
            prev[offset(next.coordinate, next.direction, next.count)] = current;
        }

        /*
         * Return the length of the shortest path to the destination cell.
         */
        int shortestPath(final Coordinate to) {
            return Arrays.stream(dist, offset(to, Direction.UP, 0), offset(to, Direction.RIGHT, (max - min) + 1))
                    .min()
                    .orElseThrow();
        }

        // Static Helper Methods

        /*
         * Create a `Memo` for a regular crucible.
         */
        static Memo forCrucible(final Grid grid) {
            return forMinMaxDistance(grid, 1, 3);
        }

        /*
         * Create a `Memo` for an ultra crucible.
         */
        static Memo forUltraCrucible(final Grid grid) {
            return forMinMaxDistance(grid, 4, 10);
        }

        /*
         * Create a `Memo` for a crucible that has both a minimum and maximum
         * travel distance in any direction.
         */
        private static Memo forMinMaxDistance(final Grid grid, final int min, final int max) {
            final int distanceSize = (max - min) + 1;
            final int size = grid.height() * grid.width() * 4 * distanceSize;
            final int[] dist = (int[]) Array.newInstance(int.class, size);
            final Coordinate[] prev = (Coordinate[]) Array.newInstance(Coordinate.class, size);

            Arrays.fill(dist, NO_VAL);
            Arrays.fill(prev, null);
            return new Memo(dist, prev, grid.width(), min, max);
        }
    }

}

