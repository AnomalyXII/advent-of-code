package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Direction;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.*;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 16: The Floor Will Be Lava.
 */
@Solution(year = 2023, day = 16, title = "The Floor Will Be Lava")
public class Day16 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * With the beam of light completely focused <em>somewhere</em>, the reindeer leads you deeper still into the Lava Production Facility.
     * At some point, you realize that the steel facility walls have been replaced with cave, and the doorways are just cave, and the floor is cave, and you're pretty sure this is actually just a giant cave.
     * <p>
     * Finally, as you approach what must be the heart of the mountain, you see a bright light in a cavern up ahead.
     * There, you discover that the <span title="Not anymore, there's a blanket!">beam</span> of light you so carefully focused is emerging from the cavern wall closest to the facility and pouring all of its energy into a contraption on the opposite side.
     * <p>
     * Upon closer inspection, the contraption appears to be a flat, two-dimensional square grid containing <em>empty space</em> (<code>.</code>), <em>mirrors</em> (<code>/</code> and <code>\</code>), and <em>splitters</em> (<code>|</code> and <code>-</code>).
     * <p>
     * The contraption is aligned so that most of the beam bounces around the grid, but each tile on the grid converts some of the beam's light into <em>heat</em> to melt the rock in the cavern.
     * <p>
     * You note the layout of the contraption (your puzzle input).
     * For example:
     * <pre>
     * .|...\....
     * |.-.\.....
     * .....|-...
     * ........|.
     * ..........
     * .........\
     * ..../.\\..
     * .-.-/..|..
     * .|....-|.\
     * ..//.|....
     * </pre>
     * <p>
     * The beam enters in the top-left corner from the left and heading to the <em>right</em>.
     * Then, its behavior depends on what it encounters as it moves:
     * <ul>
     * <li>If the beam encounters <em>empty space</em> (<code>.</code>), it continues in the same direction.</li>
     * <li>If the beam encounters a <em>mirror</em> (<code>/</code> or <code>\</code>), the beam is <em>reflected</em> 90 degrees depending on the angle of the mirror.
     * For instance, a rightward-moving beam that encounters a <code>/</code> mirror would continue <em>upward</em> in the mirror's column, while a rightward-moving beam that encounters a <code>\</code> mirror would continue <em>downward</em> from the mirror's column.</li>
     * <li>If the beam encounters the <em>pointy end of a splitter</em> (<code>|</code> or <code>-</code>), the beam passes through the splitter as if the splitter were <em>empty space</em>.
     * For instance, a rightward-moving beam that encounters a <code>-</code> splitter would continue in the same direction.</li>
     * <li>If the beam encounters the <em>flat side of a splitter</em> (<code>|</code> or <code>-</code>), the beam is <em>split into two beams</em> going in each of the two directions the splitter's pointy ends are pointing.
     * For instance, a rightward-moving beam that encounters a <code>|</code> splitter would split into two beams: one that continues <em>upward</em> from the splitter's column and one that continues <em>downward</em> from the splitter's column.</li>
     * </ul>
     * <p>
     * Beams do not interact with other beams; a tile can have many beams passing through it at the same time.
     * A tile is <em>energized</em> if that tile has at least one beam pass through it, reflect in it, or split in it.
     * <p>
     * In the above example, here is how the beam of light bounces around the contraption:
     * <pre>
     * &gt;|&lt;&lt;&lt;\....
     * |v-.\^....
     * .v...|-&gt;&gt;&gt;
     * .v...v^.|.
     * .v...v^...
     * .v...v^..\
     * .v../2\\..
     * &lt;-&gt;-/vv|..
     * .|&lt;&lt;&lt;2-|.\
     * .v//.|.v..
     * </pre>
     * <p>
     * Beams are only shown on empty tiles; arrows indicate the direction of the beams.
     * If a tile contains beams moving in multiple directions, the number of distinct directions is shown instead.
     * Here is the same diagram but instead only showing whether a tile is <em>energized</em> (<code>#</code>) or not (<code>.</code>):
     * <pre>
     * ######....
     * .#...#....
     * .#...#####
     * .#...##...
     * .#...##...
     * .#...##...
     * .#..####..
     * ########..
     * .#######..
     * .#...#.#..
     * </pre>
     * <p>
     * Ultimately, in this example, <code><em>46</em></code> tiles become <em>energized</em>.
     * <p>
     * The light isn't energizing enough tiles to produce lava; to debug the contraption, you need to start by analyzing the current situation.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return With the beam starting in the top-left heading right, how many tiles end up being energized?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final Grid grid = context.readGrid();
        return countEnergisedCells(grid, new Beam(Coordinate.ORIGIN, Direction.RIGHT));
    }

    /**
     * As you try to work out what might be wrong, the reindeer tugs on your shirt and leads you to a nearby control panel.
     * There, a collection of buttons lets you align the contraption so that the beam enters from <em>any edge tile</em> and heading away from that edge.
     * (You can choose either of two directions for the beam if it starts on a corner; for instance, if the beam starts in the bottom-right corner, it can start heading either left or upward.)
     * <p>
     * So, the beam could start on any tile in the top row (heading downward), any tile in the bottom row (heading upward), any tile in the leftmost column (heading right), or any tile in the rightmost column (heading left).
     * To produce lava, you need to find the configuration that <em>energizes as many tiles as possible</em>.
     * <p>
     * In the above example, this can be achieved by starting the beam in the fourth tile from the left in the top row:
     * <pre>
     * .|&lt;2&lt;\....
     * |v-v\^....
     * .v.v.|-&gt;&gt;&gt;
     * .v.v.v^.|.
     * .v.v.v^...
     * .v.v.v^..\
     * .v.v/2\\..
     * &lt;-2-/vv|..
     * .|&lt;&lt;&lt;2-|.\
     * .v//.|.v..
     * </pre>
     * <p>
     * Using this configuration, <code><em>51</em></code> tiles are energized:
     * <pre>
     * .#####....
     * .#.#.#....
     * .#.#.#####
     * .#.#.##...
     * .#.#.##...
     * .#.#.##...
     * .#.#####..
     * ########..
     * .#######..
     * .#...#.#..
     * </pre>
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Find the initial beam configuration that energizes the largest number of tiles; how many tiles are energized in that configuration?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Grid grid = context.readGrid();

        return testEdges(grid);
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final Grid grid = context.readGrid();

        return new IntTuple(
                countEnergisedCells(grid, new Beam(Coordinate.ORIGIN, Direction.RIGHT)),
                testEdges(grid)
        );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Create a `Stream` of `Beam`s that start along every edge.
     */
    private static int testEdges(final Grid grid) {
        int max = 0;
        for (int y = 0; y < grid.height(); y++) {
            final int scoreRight = countEnergisedCells(grid, new Beam(new Coordinate(0, y), Direction.RIGHT));
            if (scoreRight > max) max = scoreRight;

            final int scoreLeft = countEnergisedCells(grid, new Beam(new Coordinate(grid.width() - 1, y), Direction.LEFT));
            if (scoreLeft > max) max = scoreLeft;
        }

        for (int x = 0; x < grid.width(); x++) {
            final int scoreDown = countEnergisedCells(grid, new Beam(new Coordinate(x, 0), Direction.DOWN));
            if (scoreDown > max) max = scoreDown;

            final int scoreUp = countEnergisedCells(grid, new Beam(new Coordinate(x, grid.height() - 1), Direction.UP));
            if (scoreUp > max) max = scoreUp;
        }

        return max;
    }

    /*
     * Count the number of energised cells in the grid assuming a beam of
     * light starts from a specific `Coordinate` travelling in a specific
     * `Direction`.
     */
    private static int countEnergisedCells(final Grid grid, final Beam origin) {
        final SequencedSet<Beam> tips = new LinkedHashSet<>();
        tips.addFirst(origin);

        final byte[][] energisedCells = new byte[grid.height()][];
        for (int y = 0; y < grid.height(); y++) energisedCells[y] = new byte[grid.width()];

        while (!tips.isEmpty()) {
            final Beam beam = tips.removeFirst();

            if (!grid.contains(beam.coord)) continue;

            final byte marker = (byte) (1 << beam.direction.ordinal());
            if ((energisedCells[beam.coord.y()][beam.coord.x()] & marker) != 0) continue;

            final char type = (char) grid.get(beam.coord);
            tips.addAll(beam.progress(type));
            energisedCells[beam.coord.y()][beam.coord.x()] |= marker;
        }

        int sum = 0;
        for (int y = 0; y < grid.height(); y++)
            for (int x = 0; x < grid.width(); x++)
                sum += (energisedCells[y][x] > 0 ? 1 : 0);
        return sum;
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * Represents a mirror.
     */
    private enum Mirror {

        /*
         * A reflection along a line represented by a `/`.
         */
        NE_SW {
            @Override
            Direction reflect(final Direction direction) {
                return switch (direction) {
                    case UP -> Direction.RIGHT;
                    case RIGHT -> Direction.UP;
                    case DOWN -> Direction.LEFT;
                    case LEFT -> Direction.DOWN;
                };
            }
        },

        /*
         * A reflection along a line represented by a `\`.
         */
        NW_SE {
            @Override
            Direction reflect(final Direction direction) {
                return switch (direction) {
                    case UP -> Direction.LEFT;
                    case LEFT -> Direction.UP;
                    case DOWN -> Direction.RIGHT;
                    case RIGHT -> Direction.DOWN;
                };
            }
        }

        // End of constants
        ;

        // Mirror Methods

        /*
         * Calculate the new `Direction` of a beam of light that hits a reflector
         * in this orientation.
         */
        abstract Direction reflect(Direction direction);

    }

    /*
     * Represents the tip of a beam of light, travelling in a specific
     * `Direction`.
     */
    private record Beam(Coordinate coord, Direction direction) {

        // Beam Methods

        /*
         * Determine the next `Beam` or `Beam`s of light.
         */
        List<Beam> progress(final char type) {
            return switch (type) {
                case '.' -> next();
                case '|' -> isPointyEndOfSplitter(type, direction) ? next() : splitVertical();
                case '-' -> isPointyEndOfSplitter(type, direction) ? next() : splitHorizontal();
                case '/' -> reflect(Mirror.NE_SW);
                case '\\' -> reflect(Mirror.NW_SE);
                default -> throw new IllegalStateException("Could not successfully process a tile of type " + type);
            };
        }

        // Private Helper Methods

        /*
         * Progress this beam of light without interference.
         */
        private List<Beam> next() {
            return Collections.singletonList(new Beam(coord.adjustBy(direction), direction));
        }

        /*
         * Split this beam of light vertically.
         */
        private List<Beam> splitVertical() {
            return List.of(
                    new Beam(coord.adjustBy(Direction.UP), Direction.UP),
                    new Beam(coord.adjustBy(Direction.DOWN), Direction.DOWN)
            );
        }

        /*
         * Split this beam of light horizontally.
         */
        private List<Beam> splitHorizontal() {
            return List.of(
                    new Beam(coord.adjustBy(Direction.LEFT), Direction.LEFT),
                    new Beam(coord.adjustBy(Direction.RIGHT), Direction.RIGHT)
            );
        }

        /*
         * Reflect this beam of light.
         */
        private List<Beam> reflect(final Mirror mirror) {
            final Direction newDirection = mirror.reflect(direction);
            return Collections.singletonList(new Beam(coord.adjustBy(newDirection), newDirection));
        }

        /*
         * Check if this beam of light is entering the pointy end of a splitter.
         */
        private static boolean isPointyEndOfSplitter(final char type, final Direction direction) {
            if (type == '-') return direction == Direction.LEFT || direction == Direction.RIGHT;
            if (type == '|') return direction == Direction.UP || direction == Direction.DOWN;
            return false;
        }

        // Equals & Hash Code

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final Beam beam = (Beam) o;
            return Objects.equals(coord, beam.coord) && direction == beam.direction;
        }

        @Override
        public int hashCode() {
            return Objects.hash(coord, direction);
        }

    }

}

