package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.LongTuple;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 12: Hot Springs.
 */
@Solution(year = 2023, day = 12, title = "Hot Springs")
public class Day12 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You finally reach the hot springs!
     * You can see steam rising from secluded areas attached to the primary, ornate building.
     * <p>
     * As you turn to enter, the <a href="11">researcher</a> stops you.
     * "Wait - I thought you were looking for the hot springs, weren't you?"
     * You indicate that this definitely looks like hot springs to you.
     * <p>
     * "Oh, sorry, common mistake!
     * This is actually the <a href="https://en.wikipedia.org/wiki/Onsen" target="_blank">onsen</a>!
     * The hot springs are next door."
     * <p>
     * You look in the direction the researcher is pointing and suddenly notice the <span title="I love this joke. I'm not sorry.">massive metal helixes</span> towering overhead.
     * "This way!"
     * <p>
     * It only takes you a few more steps to reach the main gate of the massive fenced-off area containing the springs.
     * You go through the gate and into a small administrative building.
     * <p>
     * "Hello!
     * What brings you to the hot springs today?
     * Sorry they're not very hot right now; we're having a <em>lava shortage</em> at the moment."
     * You ask about the missing machine parts for Desert Island.
     * <p>
     * "Oh, all of Gear Island is currently offline!
     * Nothing is being manufactured at the moment, not until we get more lava to heat our forges.
     * And our springs.
     * The springs aren't very springy unless they're hot!"
     * <p>
     * "Say, could you go up and see why the lava stopped flowing?
     * The springs are too cold for normal operation, but we should be able to find one springy enough to launch <em>you</em> up there!"
     * <p>
     * There's just one problem - many of the springs have fallen into disrepair, so they're not actually sure which springs would even be <em>safe</em> to use!
     * Worse yet, their <em>condition records of which springs are damaged</em> (your puzzle input) are also damaged!
     * You'll need to help them repair the damaged records.
     * <p>
     * In the giant field just outside, the springs are arranged into <em>rows</em>.
     * For each row, the condition records show every spring and whether it is <em>operational</em> (<code>.</code>) or <em>damaged</em> (<code>#</code>).
     * This is the part of the condition records that is itself damaged; for some springs, it is simply <em>unknown</em> (<code>?</code>) whether the spring is operational or damaged.
     * <p>
     * However, the engineer that produced the condition records also duplicated some of this information in a different format!
     * After the list of springs for a given row, the size of each <em>contiguous group of damaged springs</em> is listed in the order those groups appear in the row.
     * This list always accounts for every damaged spring, and each number is the entire size of its contiguous group (that is, groups are always separated by at least one operational spring: <code>####</code> would always be <code>4</code>, never <code>2,2</code>).
     * <p>
     * So, condition records with no unknown spring conditions might look like this:
     * <pre>
     * #.#.### 1,1,3
     * .#...#....###. 1,1,3
     * .#.###.#.###### 1,3,1,6
     * ####.#...#... 4,1,1
     * #....######..#####. 1,6,5
     * .###.##....# 3,2,1
     * </pre>
     * <p>
     * However, the condition records are partially damaged; some of the springs' conditions are actually <em>unknown</em> (<code>?</code>).
     * For example:
     * <pre>
     * ???.### 1,1,3
     * .??..??...?##. 1,1,3
     * ?#?#?#?#?#?#?#? 1,3,1,6
     * ????.#...#... 4,1,1
     * ????.######..#####. 1,6,5
     * ?###???????? 3,2,1
     * </pre>
     * <p>
     * Equipped with this information, it is your job to figure out <em>how many different arrangements</em> of operational and broken springs fit the given criteria in each row.
     * <p>
     * In the first line (<code>???.### 1,1,3</code>), there is exactly <em>one</em> way separate groups of one, one, and three broken springs (in that order) can appear in that row: the first three unknown springs must be broken, then operational, then broken (<code>#.#</code>), making the whole row <code>#.#.###</code>.
     * <p>
     * The second line is more interesting: <code>.??..??...?##. 1,1,3</code> could be a total of <em>four</em> different arrangements
     * The last <code>?</code> must always be broken (to satisfy the final contiguous group of three broken springs), and each <code>??</code> must hide exactly one of the two broken springs.
     * (Neither <code>??</code> could be both broken springs or they would form a single contiguous group of two; if that were true, the numbers afterward would have been <code>2,3</code> instead.)
     * Since each <code>??</code> can either be <code>#.</code> or <code>.#</code>, there are four possible arrangements of springs.
     * <p>
     * The last line is actually consistent with <em>ten</em> different arrangements!
     * Because the first number is <code>3</code>, the first and second <code>?</code> must both be <code>.</code> (if either were <code>#</code>, the first number would have to be <code>4</code> or higher).
     * However, the remaining run of unknown spring conditions have many different ways they could hold groups of two and one broken springs:
     * <pre>
     * ?###???????? 3,2,1
     * .###.##.#...
     * .###.##..#..
     * .###.##...#.
     * .###.##....#
     * .###..##.#..
     * .###..##..#.
     * .###..##...#
     * .###...##.#.
     * .###...##..#
     * .###....##.#
     * </pre>
     * <p>
     * In this example, the number of possible arrangements for each row is:
     * <ul>
     * <li><code>???.### 1,1,3</code> - <code><em>1</em></code> arrangement</li>
     * <li><code>.??..??...?##. 1,1,3</code> - <code><em>4</em></code> arrangements</li>
     * <li><code>?#?#?#?#?#?#?#? 1,3,1,6</code> - <code><em>1</em></code> arrangement</li>
     * <li><code>????.#...#... 4,1,1</code> - <code><em>1</em></code> arrangement</li>
     * <li><code>????.######..#####. 1,6,5</code> - <code><em>4</em></code> arrangements</li>
     * <li><code>?###???????? 3,2,1</code> - <code><em>10</em></code> arrangements</li>
     * </ul>
     * <p>
     * Adding all of the possible arrangement counts together produces a total of <code><em>21</em></code> arrangements.
     * <p>
     * For each row, count all of the different arrangements of operational and broken springs that meet the given criteria.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of those counts?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .map(line -> line.split("\\s+", 2))
                .mapToLong(Day12::process)
                .sum();
    }

    /**
     * As you look out at the field of springs, you feel like there are way more springs than the condition records list.
     * When you examine the records, you discover that they were actually <em>folded up</em> this whole time!
     * <p>
     * To <em>unfold the records</em>, on each row, replace the list of spring conditions with five copies of itself (separated by <code>?</code>) and replace the list of contiguous groups of damaged springs with five copies of itself (separated by <code>,</code>).
     * <p>
     * So, this row:
     * <pre>
     * .# 1
     * </pre>
     * <p>
     * Would become:
     * <pre>
     * .#?.#?.#?.#?.# 1,1,1,1,1
     * </pre>
     * <p>
     * The first line of the above example would become:
     * <pre>
     * ???.###????.###????.###????.###????.### 1,1,3,1,1,3,1,1,3,1,1,3,1,1,3
     * </pre>
     * <p>
     * In the above example, after unfolding, the number of possible arrangements for some rows is now much larger:
     * <ul>
     * <li><code>???.### 1,1,3</code> - <code><em>1</em></code> arrangement</li>
     * <li><code>.??..??...?##. 1,1,3</code> - <code><em>16384</em></code> arrangements</li>
     * <li><code>?#?#?#?#?#?#?#? 1,3,1,6</code> - <code><em>1</em></code> arrangement</li>
     * <li><code>????.#...#... 4,1,1</code> - <code><em>16</em></code> arrangements</li>
     * <li><code>????.######..#####. 1,6,5</code> - <code><em>2500</em></code> arrangements</li>
     * <li><code>?###???????? 3,2,1</code> - <code><em>506250</em></code> arrangements</li>
     * </ul>
     * <p>
     * After unfolding, adding all of the possible arrangement counts together produces <code><em>525152</em></code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Unfold your condition records; what is the new sum of possible arrangement counts?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return context.stream()
                .map(line -> line.split("\\s+", 2))
                .map(Day12::unfoldLine)
                .mapToLong(Day12::process)
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        return context.stream()
                .map(line -> line.split("\\s+", 2))
                .reduce(
                        LongTuple.NULL,
                        (tup, line) -> tup.add(
                                process(line),
                                process(unfoldLine(line))
                        ),
                        LongTuple::add
                );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Unfold an input line by repeating each side 5 times, joined by a `?`
     * on the left-hand side and a `,` on the right.
     */
    static String[] unfoldLine(final String[] parts) {
        final String unfoldedRecord = "%1$s?%1$s?%1$s?%1$s?%1$s" .formatted(parts[0]);
        final String unfoldedArrangement = "%1$s,%1$s,%1$s,%1$s,%1$s" .formatted(parts[1]);
        return new String[]{unfoldedRecord, unfoldedArrangement};
    }

    /*
     * Process an input line.
     */
    static long process(final String[] parts) {
        final String record = parts[0];
        final int[] arrangement = Arrays.stream(parts[1].split(",\\s*"))
                .mapToInt(Integer::parseInt)
                .toArray();

        final int totalArrangement = IntStream.of(arrangement).sum();
        return process(record, arrangement, 0, totalArrangement, Cache.forArrangements(arrangement));
    }

    /*
     * Calculate the viable interpretations of a given record based on the
     * specified arrangements.
     */
    private static long process(
            final String record,
            final int[] arrangement,
            final int currentArrangement,
            final int totalRemainingArrangement,
            final Cache cache
    ) {
        // Some early exists
        final boolean noMoreArrangements = currentArrangement >= arrangement.length;
        if (record.matches("^[.?]*$") && noMoreArrangements) return 1;
        if (noMoreArrangements) return 0;
        if (totalRemainingArrangement > record.length()) return 0;

        // Cache, because otherwise ugh...
        if (cache.contains(currentArrangement, record)) return cache.get(currentArrangement, record);

        long sum = 0;
        final int nextArrangementLength = arrangement[currentArrangement];
        if (canMakeArrangement(record, nextArrangementLength)) {
            sum += process(
                    popBrokenSprings(record, nextArrangementLength),
                    arrangement,
                    currentArrangement + 1,
                    totalRemainingArrangement - nextArrangementLength,
                    cache
            );
        }

        if (record.charAt(0) != '#')
            sum += process(record.substring(1), arrangement, currentArrangement, totalRemainingArrangement, cache);

        assert sum >= 0;
        cache.set(currentArrangement, record, sum);
        return sum;
    }

    /*
     * Check if the first arrangement can be made from this record.
     */
    private static boolean canMakeArrangement(final String record, final int groupLength) {
        if (record.charAt(0) == '.') return false;
        if (record.length() < groupLength) return false;

        final boolean isGroup = record.substring(0, groupLength).indexOf('.') < 0;
        final boolean isTerminated = record.length() == groupLength || record.charAt(groupLength) != '#';
        return isGroup && isTerminated;
    }

    /*
     * Pop a number of broken springs from the start of a record.
     */
    private static String popBrokenSprings(final String record, final int groupLength) {
        if (record.length() - groupLength == 0) return "";

        final char[] newRecord = new char[record.length() - groupLength];
        System.arraycopy(record.toCharArray(), groupLength, newRecord, 0, newRecord.length);
        assert newRecord[0] != '#';
        newRecord[0] = '.';

        return new String(newRecord);
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A somewhat optimised cache for results.
     */
    private record Cache(Map<String, Long>[] caches) {

        // Helper Methods

        /*
         * Check if this cache contains a result.
         */
        boolean contains(final int currentArrangement, final String record) {
            final Map<String, Long> cache = caches[currentArrangement];
            return cache != null && cache.containsKey(record);
        }

        /*
         * Get a result.
         */
        long get(final int currentArrangement, final String record) {
            final Map<String, Long> cache = caches[currentArrangement];
            return cache.get(record);
        }

        /*
         * Set a result.
         */
        void set(final int currentArrangement, final String record, final long result) {
            Map<String, Long> cache = caches[currentArrangement];
            if (cache == null)
                cache = (caches[currentArrangement] = new HashMap<>());
            cache.put(record, result);
        }

        // Static Helper Methods

        @SuppressWarnings("unchecked")
        static Cache forArrangements(final int[] arrangements) {
            return new Cache(new Map[arrangements.length]);
        }
    }

}

