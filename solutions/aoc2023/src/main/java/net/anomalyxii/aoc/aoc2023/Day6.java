package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.Arrays.stream;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 6: Wait For It.
 */
@Solution(year = 2023, day = 6, title = "Wait For It")
public class Day6 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * The ferry quickly brings you across Island Island.
     * After asking around, you discover that there is indeed normally a large pile of sand somewhere near here, but you don't see anything besides lots of water and the small island where the ferry has docked.
     * <p>
     * As you try to figure out what to do next, you notice a poster on a wall near the ferry dock.
     * "Boat races! Open to the public! Grand prize is an all-expenses-paid trip to <em>Desert Island</em>!"
     * That must be where the sand comes from!
     * Best of all, the boat races are starting in just a few minutes.
     * <p>
     * You manage to sign up as a competitor in the boat races just in time.
     * The organizer explains that it's not really a traditional race - instead, you will get a fixed amount of time during which your boat has to travel as far as it can, and you win if your boat goes the farthest.
     * <p>
     * As part of signing up, you get a sheet of paper (your puzzle input) that lists the <em>time</em> allowed for each race and also the best <em>distance</em> ever recorded in that race.
     * To guarantee you win the grand prize, you need to make sure you <em>go farther in each race</em> than the current record holder.
     * <p>
     * The organizer brings you over to the area where the boat races are held.
     * The boats are much smaller than you expected - they're actually <em>toy boats</em>, each with a big button on top.
     * Holding down the button <em>charges the boat</em>, and releasing the button <em>allows the boat to move</em>.
     * Boats move faster if their button was held longer, but time spent holding the button counts against the total race time.
     * You can only hold the button at the start of the race, and boats don't move until the button is released.
     * <p>
     * For example:
     * <pre>
     * Time:      7  15   30
     * Distance:  9  40  200
     * </pre>
     * <p>
     * This document describes three races:
     * <ul>
     * <li>
     * The first race lasts 7 milliseconds.
     * The record distance in this race is 9 millimeters.
     * </li>
     * <li>
     * The second race lasts 15 milliseconds.
     * The record distance in this race is 40 millimeters.
     * </li>
     * <li>
     * The third race lasts 30 milliseconds.
     * The record distance in this race is 200 millimeters.
     * </li>
     * </ul>
     * <p>
     * Your toy boat has a starting speed of <em>zero millimeters per millisecond</em>.
     * For each whole millisecond you spend at the beginning of the race holding down the button, the boat's speed increases by <em>one millimeter per millisecond</em>.
     * <p>
     * So, because the first race lasts 7 milliseconds, you only have a few options:
     * <ul>
     * <li>
     * Don't hold the button at all (that is, hold it for <em><code>0</code> milliseconds</em>) at the start of the race.
     * The boat won't move; it will have traveled <em><code>0</code> millimeters</em> by the end of the race.
     * </li>
     * <li>
     * Hold the button for <em><code>1</code> millisecond</em> at the start of the race.
     * Then, the boat will travel at a speed of <code>1</code> millimeter per millisecond for 6 milliseconds, reaching a total distance traveled of <em><code>6</code> millimeters</em>.
     * </li>
     * <li>
     * Hold the button for <em><code>2</code> milliseconds</em>, giving the boat a speed of <code>2</code> millimeters per millisecond.
     * It will then get 5 milliseconds to move, reaching a total distance of <em><code>10</code> millimeters</em>.
     * </li>
     * <li>
     * Hold the button for <em><code>3</code> milliseconds</em>.
     * After its remaining 4 milliseconds of travel time, the boat will have gone <em><code>12</code> millimeters</em>.
     * </li>
     * <li>
     * Hold the button for <em><code>4</code> milliseconds</em>.
     * After its remaining 3 milliseconds of travel time, the boat will have gone <em><code>12</code> millimeters</em>.
     * </li>
     * <li>Hold the button for <em><code>5</code> milliseconds</em>, causing the boat to travel a total of <em><code>10</code> millimeters</em>.</li>
     * <li>Hold the button for <em><code>6</code> milliseconds</em>, causing the boat to travel a total of <em><code>6</code> millimeters</em>.</li>
     * <li>
     * Hold the button for <em><code>7</code> milliseconds</em>.
     * That's the entire duration of the race.
     * You never let go of the button.
     * The boat can't move until you let go of the button.
     * Please make sure you let go of the button so the boat gets to move.
     * <em><code>0</code> millimeters</em>.
     * </li>
     * </ul>
     * <p>
     * Since the current record for this race is <code>9</code> millimeters, there are actually <code><em>4</em></code> different ways you could win: you could hold the button for <code>2</code>, <code>3</code>, <code>4</code>, or <code>5</code> milliseconds at the start of the race.
     * <p>
     * In the second race, you could hold the button for at least <code>4</code> milliseconds and at most <code>11</code> milliseconds and beat the record, a total of <code><em>8</em></code> different ways to win.
     * <p>
     * In the third race, you could hold the button for at least <code>11</code> milliseconds and no more than <code>19</code> milliseconds and still beat the record, a total of <code><em>9</em></code> ways you could win.
     * <p>
     * To see how much margin of error you have, determine the <em>number of ways you can beat the record</em> in each race; in this example, if you multiply these values together, you get <code><em>288</em></code> (<code>4</code> * <code>8</code> * <code>9</code>).
     * <p>
     * Determine the number of ways you could beat the record in each race.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you multiply these numbers together?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final List<String> lines = context.read();
        final long[] durations = stream(lines.getFirst().substring(5).split("\\s+"))
                .filter(s -> !s.isEmpty())
                .mapToLong(Long::parseLong)
                .toArray();
        final long[] distances = stream(lines.getLast().substring(9).split("\\s+"))
                .filter(s -> !s.isEmpty())
                .mapToLong(Long::parseLong)
                .toArray();

        return IntStream.range(0, durations.length)
                .map(r -> calculateNumberOfWaysToWinTheRace(durations[r], distances[r]))
                .reduce((a, b) -> a * b)
                .orElse(0);
    }

    /**
     * As the race is about to start, you realize the piece of paper with race times and record distances you got earlier actually just has <span title="Keming!">very bad</span> <a href="https://en.wikipedia.org/wiki/Kerning" target="_blank">kerning</a>.
     * There's really <em>only one race</em> - ignore the spaces between the numbers on each line.
     * <p>
     * So, the example from before:
     * <pre>
     * Time:      7  15   30
     * Distance:  9  40  200
     * </pre>
     * <p>
     * ...now instead means this:
     * <pre>
     * Time:      71530
     * Distance:  940200
     * </pre>
     * <p>
     * Now, you have to figure out how many ways there are to win this single race.
     * In this example, the race lasts for <em><code>71530</code> milliseconds</em> and the record distance you need to beat is <em><code>940200</code> millimeters</em>.
     * You could hold the button anywhere from <code>14</code> to <code>71516</code> milliseconds and beat the record, a total of <code><em>71503</em></code> ways!
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many ways can you beat the record in this one much longer race?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final List<String> lines = context.read();
        final long duration = Long.parseLong(lines.getFirst().substring(5).replaceAll("\\s+", ""));
        final long distance = Long.parseLong(lines.getLast().substring(9).replaceAll("\\s+", ""));

        return calculateNumberOfWaysToWinTheRace(duration, distance);
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final List<String> lines = context.read();

        final long[] durations = stream(lines.getFirst().substring(5).split("\\s+"))
                .filter(s -> !s.isEmpty())
                .mapToLong(Long::parseLong)
                .toArray();
        final long[] distances = stream(lines.getLast().substring(9).split("\\s+"))
                .filter(s -> !s.isEmpty())
                .mapToLong(Long::parseLong)
                .toArray();

        final long longDuration = Long.parseLong(lines.getFirst().substring(5).replaceAll("\\s+", ""));
        final long longDistance = Long.parseLong(lines.getLast().substring(9).replaceAll("\\s+", ""));

        return new IntTuple(
                IntStream.range(0, durations.length)
                        .map(r -> calculateNumberOfWaysToWinTheRace(durations[r], distances[r]))
                        .reduce((a, b) -> a * b)
                        .orElse(0),
                calculateNumberOfWaysToWinTheRace(longDuration, longDistance)
        );
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Calculate the number of ways there are to win a race.
     */
    private static int calculateNumberOfWaysToWinTheRace(final long duration, final long distance) {
        // y = x * (duration - x)
        // :. y = -1 * x^2 + duration * x
        // :. solve for y > distance
        // :. distance = -1 * x^2 + duration * x
        // :. 0 = -1 * x^2 + duration * x - distance
        // :. x = (-duration +- sqrt(duration^2 - 4*-1*-distance) / (2 * -1)
        final double coefficient = Math.sqrt(duration * duration - 4 * (distance + 0.01)); // We want to be _better_ than the distance, so add a little bit
        final int first = (int) Math.floor(0.5 * (duration - coefficient));
        final int second = (int) Math.floor(0.5 * (duration + coefficient));

        return second - first;
    }

}

