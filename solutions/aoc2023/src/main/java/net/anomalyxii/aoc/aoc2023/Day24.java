package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.Arrays;
import java.util.List;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;
import static net.anomalyxii.aoc.utils.maths.Factors.highestCommonFactor;

/**
 * Day 24: Never Tell Me The Odds.
 */
@Solution(year = 2023, day = 24, title = "Never Tell Me The Odds")
public class Day24 {

    private static final long DEFAULT_LOWER_BOUND = 200000000000000L;
    private static final long DEFAULT_UPPER_BOUND = 400000000000000L;

    // ****************************************
    // Private Members
    // ****************************************

    private final long min;
    private final long max;

    // ****************************************
    // Constructors
    // ****************************************

    public Day24() {
        this(DEFAULT_LOWER_BOUND, DEFAULT_UPPER_BOUND);
    }

    Day24(final long min, final long max) {
        this.min = min;
        this.max = max;
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * It seems like something is going wrong with the snow-making process.
     * Instead of forming snow, the water that's been absorbed into the air seems to be forming <a href="https://en.wikipedia.org/wiki/Hail" target="_blank">hail</a>!
     * <p>
     * Maybe there's something you can do to break up the hailstones?
     * <p>
     * Due to strong, probably-magical winds, the hailstones are all flying through the air in perfectly linear trajectories.
     * You make a note of each hailstone's <em>position</em> and <em>velocity</em> (your puzzle input).
     * For example:
     * <pre>
     * 19, 13, 30 @ -2,  1, -2
     * 18, 19, 22 @ -1, -1, -2
     * 20, 25, 34 @ -2, -2, -4
     * 12, 31, 28 @ -1, -2, -1
     * 20, 19, 15 @  1, -5, -3
     * </pre>
     * <p>
     * Each line of text corresponds to the position and velocity of a single hailstone.
     * The positions indicate where the hailstones are <em>right now</em> (at time <code>0</code>).
     * The velocities are constant and indicate exactly how far each hailstone will move in <em>one nanosecond</em>.
     * <p>
     * Each line of text uses the format <code>px py pz @ vx vy vz</code>.
     * For instance, the hailstone specified by <code>20, 19, 15 @  1, -5, -3</code> has initial X position <code>20</code>, Y position <code>19</code>, Z position <code>15</code>, X velocity <code>1</code>, Y velocity <code>-5</code>, and Z velocity <code>-3</code>.
     * After one nanosecond, the hailstone would be at <code>21, 14, 12</code>.
     * <p>
     * Perhaps you won't have to do anything.
     * How likely are the hailstones to collide with each other and smash into tiny ice crystals?
     * <p>
     * To estimate this, consider only the X and Y axes; <em>ignore the Z axis</em>.
     * Looking <em>forward in time</em>, how many of the hailstones' <em>paths</em> will intersect within a test area?
     * (The hailstones themselves don't have to collide, just test for intersections between the paths they will trace.)
     * <p>
     * In this example, look for intersections that happen with an X and Y position each at least <code>7</code> and at most <code>27</code>; in your actual data, you'll need to check a much larger test area.
     * Comparing all pairs of hailstones' future paths produces the following results:
     * <pre>
     * Hailstone A: 19, 13, 30 @ -2, 1, -2
     * Hailstone B: 18, 19, 22 @ -1, -1, -2
     * Hailstones' paths will cross <em>inside</em> the test area (at x=14.333, y=15.333).
     *
     * Hailstone A: 19, 13, 30 @ -2, 1, -2
     * Hailstone B: 20, 25, 34 @ -2, -2, -4
     * Hailstones' paths will cross <em>inside</em> the test area (at x=11.667, y=16.667).
     *
     * Hailstone A: 19, 13, 30 @ -2, 1, -2
     * Hailstone B: 12, 31, 28 @ -1, -2, -1
     * Hailstones' paths will cross outside the test area (at x=6.2, y=19.4).
     *
     * Hailstone A: 19, 13, 30 @ -2, 1, -2
     * Hailstone B: 20, 19, 15 @ 1, -5, -3
     * Hailstones' paths crossed in the past for hailstone A.
     *
     * Hailstone A: 18, 19, 22 @ -1, -1, -2
     * Hailstone B: 20, 25, 34 @ -2, -2, -4
     * Hailstones' paths are parallel; they never intersect.
     *
     * Hailstone A: 18, 19, 22 @ -1, -1, -2
     * Hailstone B: 12, 31, 28 @ -1, -2, -1
     * Hailstones' paths will cross outside the test area (at x=-6, y=-5).
     *
     * Hailstone A: 18, 19, 22 @ -1, -1, -2
     * Hailstone B: 20, 19, 15 @ 1, -5, -3
     * Hailstones' paths crossed in the past for both hailstones.
     *
     * Hailstone A: 20, 25, 34 @ -2, -2, -4
     * Hailstone B: 12, 31, 28 @ -1, -2, -1
     * Hailstones' paths will cross outside the test area (at x=-2, y=3).
     *
     * Hailstone A: 20, 25, 34 @ -2, -2, -4
     * Hailstone B: 20, 19, 15 @ 1, -5, -3
     * Hailstones' paths crossed in the past for hailstone B.
     *
     * Hailstone A: 12, 31, 28 @ -1, -2, -1
     * Hailstone B: 20, 19, 15 @ 1, -5, -3
     * Hailstones' paths crossed in the past for both hailstones.
     * </pre>
     * <p>
     * So, in this example, <code><em>2</em></code> hailstones' future paths cross inside the boundaries of the test area.
     * <p>
     * However, you'll need to search a much larger test area if you want to see if any hailstones might collide.
     * Look for intersections that happen with an X and Y position each at least <code>200000000000000</code> and at most <code>400000000000000</code>.
     * Disregard the Z axis entirely.
     * <p>
     * Considering only the X and Y axes, check all pairs of hailstones' future paths for intersections.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many of these intersections occur within the test area?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        final List<Hailstone> hailstones = context.stream()
                .map(Hailstone::parse)
                .toList();

        int count = 0;
        for (int h1 = 0; h1 < hailstones.size(); h1++)
            for (int h2 = h1 + 1; h2 < hailstones.size(); h2++)
                if (hailstones.get(h1).intersectXY(hailstones.get(h2)).inBoundsXY(min, max))
                    ++count;

        return count;
    }

    /**
     * Upon further analysis, it doesn't seem like <em>any</em> hailstones will naturally collide.
     * It's up to you to fix that!
     * <p>
     * You find a rock on the ground nearby.
     * While it seems extremely unlikely, if you throw it just right, you should be able to <em>hit every hailstone in a single throw</em>!
     * <p>
     * You can use the probably-magical winds to reach <em>any integer position</em> you like and to propel the rock at <em>any integer velocity</em>.
     * Now <em>including the Z axis</em> in your calculations, if you throw the rock at time <code>0</code>, where do you need to be so that the rock <em>perfectly collides with every hailstone</em>?
     * Due to <span title="What, you've never studied probably-magical physics?">probably-magical inertia</span>, the rock won't slow down or change direction when it collides with a hailstone.
     * <p>
     * In the example above, you can achieve this by moving to position <code>24, 13, 10</code> and throwing the rock at velocity <code>-3, 1, 2</code>.
     * If you do this, you will hit every hailstone as follows:
     * <pre>
     * Hailstone: 19, 13, 30 @ -2, 1, -2
     * Collision time: 5
     * Collision position: 9, 18, 20
     *
     * Hailstone: 18, 19, 22 @ -1, -1, -2
     * Collision time: 3
     * Collision position: 15, 16, 16
     *
     * Hailstone: 20, 25, 34 @ -2, -2, -4
     * Collision time: 4
     * Collision position: 12, 17, 18
     *
     * Hailstone: 12, 31, 28 @ -1, -2, -1
     * Collision time: 6
     * Collision position: 6, 19, 22
     *
     * Hailstone: 20, 19, 15 @ 1, -5, -3
     * Collision time: 1
     * Collision position: 21, 14, 12
     * </pre>
     * <p>
     * Above, each hailstone is identified by its initial position and its velocity.
     * Then, the time and position of that hailstone's collision with your rock are given.
     * <p>
     * After 1 nanosecond, the rock has <em>exactly the same position</em> as one of the hailstones, obliterating it into ice dust!
     * Another hailstone is smashed to bits two nanoseconds after that.
     * After a total of 6 nanoseconds, all of the hailstones have been destroyed.
     * <p>
     * So, at time <code>0</code>, the rock needs to be at X position <code>24</code>, Y position <code>13</code>, and Z position <code>10</code>.
     * Adding these three coordinates together produces <code><em>47</em></code>.
     * (Don't add any coordinates from the rock's velocity.)
     * <p>
     * Determine the exact position and velocity the rock needs to have at time <code>0</code> so that it perfectly collides with every hailstone.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What do you get if you add up the X, Y, and Z coordinates of that initial position?
     * @throws IllegalStateException if no solution is found
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        final List<Hailstone> hailstones = context.stream()
                .map(Hailstone::parse)
                .toList();

        final Hailstone rock = calculateRockToThrow(hailstones);
        return rock.pos.x + rock.pos.y + rock.pos.z;
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Brute-force (ew) likely velocities to find one that will pass through
     * every `Hailstone`.
     */
    private static Hailstone calculateRockToThrow(final List<Hailstone> hailstones) {
        assert hailstones.size() > 2;

        final Hailstone h1 = hailstones.get(0);
        final Hailstone h2 = hailstones.get(1);
        final Hailstone h3 = hailstones.get(2);

        final Hailstone h2x = h2.sub(h1);
        final Hailstone h3x = h3.sub(h1);

        final LongVector h2nv = h2x.findNormalVector();
        final LongVector h3nv = h3x.findNormalVector();
        final LongVector s = h2nv.crossMultiply(h3nv).scale();

        final long h2t = (h2x.pos.y * s.x - h2x.pos.x * s.y) / (h2x.vel.x * s.y - h2x.vel.y * s.x);
        final long h3t = (h3x.pos.y * s.x - h3x.pos.x * s.y) / (h3x.vel.x * s.y - h3x.vel.y * s.x);
        assert h2t != h3t;

        final long h2ix = h2.pos.x + (h2.vel.x * h2t); // h2 intersect x
        final long h3ix = h3.pos.x + (h3.vel.x * h3t);
        final long h2iy = h2.pos.y + (h2.vel.y * h2t); // h2 intersect y
        final long h3iy = h3.pos.y + (h3.vel.y * h3t);
        final long h2iz = h2.pos.z + (h2.vel.z * h2t); // h2 intersect z
        final long h3iz = h3.pos.z + (h3.vel.z * h3t);

        final long dx = (h3ix - h2ix) / (h3t - h2t); // Rock velocity x
        final long dy = (h3iy - h2iy) / (h3t - h2t); // Rock velocity y
        final long dz = (h3iz - h2iz) / (h3t - h2t); // Rock velocity z

        return new Hailstone(h2ix - (dx * h2t), h2iy - (dy * h2t), h2iz - (dz * h2t), dx, dy, dz);
    }

    // ****************************************
    // Helper Classes
    // ****************************************

    /*
     * Hold a vector of 3 `long`s.
     */
    record LongVector(long x, long y, long z) {

        // Helper Methods

        /*
         * Subtract a given `LongVector` from this one.
         */
        LongVector sub(final LongVector o) {
            return new LongVector(
                    x - o.x,
                    y - o.y,
                    z - o.z
            );
        }

        /*
         * Cross-multiply with a given `LongVector`.
         */
        LongVector crossMultiply(final LongVector other) {
            final long x = this.y * other.z - this.z * other.y;
            final long y = this.z * other.x - this.x * other.z;
            final long z = this.x * other.y - this.y * other.x;

            return new LongVector(x, y, z);
        }

        /*
         * Reduce this `LongVector` by calculating the highest common factor of
         * the `x`, `y` and `z` components and scaling them back accordingly.
         */
        LongVector scale() {
            final long hcf = highestCommonFactor(highestCommonFactor(x, y), z);

            final long x = this.x / hcf;
            final long y = this.y / hcf;
            final long z = this.z / hcf;
            return new LongVector(x, y, z);
        }
    }

    /*
     * A Hailstone.
     */
    record Hailstone(LongVector pos, LongVector vel) {

        // Helper Constructor

        Hailstone(final long px, final long py, final long pz, final long vx, final long vy, final long vz) {
            this(new LongVector(px, py, pz), new LongVector(vx, vy, vz));
        }

        // Helper Methods

        /*
         * Find the "normal vector" to this line.
         */
        LongVector findNormalVector() {
            return vel.crossMultiply(pos).scale();
        }

        /*
         * Subtract the position and velocity of a given `Hailstone` from this
         * one.
         */
        Hailstone sub(final Hailstone o) {
            return new Hailstone(pos.sub(o.pos), vel.sub(o.vel));
        }

        /*
         * Check if the path of this `Hailstone` intersects with the path of the
         * given other `Hailstone` considering only the XY trajectory.
         */
        IntersectionPoint intersectXY(final Hailstone other) {
            final long a = vel.x * (other.pos.y - pos.y);
            final long b = vel.y * (other.pos.x - pos.x);
            final long c = vel.y * (other.vel.x);
            final long d = vel.x * (other.vel.y);

            if (c == d) return IntersectionPoint.NULL;

            final double ot = (a - b) / (double) (c - d);
            if (ot < 0) return IntersectionPoint.NULL;

            final double tt = (other.pos.x - pos.x + ot * other.vel.x) / vel.x;
            if (tt < 0) return IntersectionPoint.NULL;

            return IntersectionPoint.of(pos.x + tt * vel.x, pos.y + tt * vel.y, pos.z + tt * vel.z);
        }

        // Static Helper Methods

        /*
         * Parse a `Hailstone` from the given line.
         */
        private static Hailstone parse(final String line) {
            final String[] parts = line.split("\\s*@\\s*", 2);

            final long[] cs = Arrays.stream(parts[0].split(",\\s*")).mapToLong(Long::parseLong).toArray();
            final long[] vs = Arrays.stream(parts[1].split(",\\s*")).mapToLong(Long::parseLong).toArray();

            return new Hailstone(new LongVector(cs[0], cs[1], cs[2]), new LongVector(vs[0], vs[1], vs[2]));
        }
    }

    /*
     * Return the coordinates at which the path of two `Hailstone`s
     * intersects.
     */
    record IntersectionPoint(double x0, double y0, double z0) {

        /*
         * Represents that no `IntersectionPoint` was found.
         */
        static final IntersectionPoint NULL = new IntersectionPoint(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);

        // Helper Methods

        /*
         * Check if this `IntersectionPoint` is within the given boundary area.
         */
        boolean inBoundsXY(final long min, final long max) {
            return x0 >= min && x0 <= max && y0 >= min && y0 <= max;
        }

        // Static Helper Methods

        /*
         * Return a new `IntersectionPoint` from the given co-ordinates.
         */
        static IntersectionPoint of(final double x0, final double y0, final double z0) {
            if (!Double.isFinite(x0)) return NULL;
            if (!Double.isFinite(y0)) return NULL;
            if (!Double.isFinite(z0)) return NULL;

            return new IntersectionPoint(round(x0), round(y0), round(z0));
        }

        /*
         * Round to a reasonable precision.
         *
         * This makes things easier to test and doesn't _seem_ to affect the
         * results.
         */
        private static double round(final double val) {
            return Math.round(val * 100) / 100d;
        }

    }

}