package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;

import java.util.Map;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 01: Trebuchet?!.
 */
@Solution(year = 2023, day = 1, title = "Trebuchet?!")
public class Day1 {

    private static final Pattern NUMBERS = Pattern.compile("([0-9]|zero|one|two|three|four|five|six|seven|eight|nine)");
    private static final Map<String, String> WORDS_TO_NUMBERS = Map.of(
            "one", "1",
            "two", "2",
            "three", "3",
            "four", "4",
            "five", "5",
            "six", "6",
            "seven", "7",
            "eight", "8",
            "nine", "9",
            "zero", "0"
    );

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Something is wrong with global snow production, and you've been selected to take a look.
     * The Elves have even given you a map; on it, they've used stars to mark the top fifty locations that are likely to be having problems.
     * <p>
     * You've been doing this long enough to know that to restore snow operations, you need to check all <em class="star">fifty stars</em> by December 25th.
     * <p>
     * Collect stars by solving puzzles.
     * Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first.
     * Each puzzle grants <em class="star">one star</em>.
     * Good luck!
     * <p>
     * You try to ask why they can't just use a <a href="/2015/day/1">weather machine</a> ("not powerful enough") and where they're even sending you ("the sky") and why your map looks mostly blank ("you sure ask a lot of questions") <span title="My hope is that this abomination of a run-on sentence somehow conveys the chaos of being hastily loaded into a trebuchet.">and</span> hang on did you just say the sky ("of course, where do you think snow comes from") when you realize that the Elves are already loading you into a <a href="https://en.wikipedia.org/wiki/Trebuchet" target="_blank">trebuchet</a> ("please hold still, we need to strap you in").
     * <p>
     * As they're making the final adjustments, they discover that their calibration document (your puzzle input) has been <em>amended</em> by a very young Elf who was apparently just excited to show off her art skills.
     * Consequently, the Elves are having trouble reading the values on the document.
     * <p>
     * The newly-improved calibration document consists of lines of text; each line originally contained a specific <em>calibration value</em> that the Elves now need to recover.
     * On each line, the calibration value can be found by combining the <em>first digit</em> and the <em>last digit</em> (in that order) to form a single <em>two-digit number</em>.
     * <p>
     * For example:
     * <pre>
     * 1abc2
     * pqr3stu8vwx
     * a1b2c3d4e5f
     * treb7uchet
     * </pre>
     * <p>
     * In this example, the calibration values of these four lines are <code>12</code>, <code>38</code>, <code>15</code>, and <code>77</code>.
     * Adding these together produces <code><em>142</em></code>.
     * <p>
     * Consider your entire calibration document.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of all of the calibration values?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .map(line -> NUMBERS.matcher(line).results()
                        .map(MatchResult::group)
                        .filter(mr -> mr.length() == 1)
                        .collect(Collectors.toList()))
                .map(matches -> matches.getFirst() + matches.getLast())
                .mapToInt(Integer::parseInt)
                .sum();
    }

    /**
     * Your calculation isn't quite right.
     * It looks like some of the digits are actually <em>spelled out with letters</em>: <code>one</code>, <code>two</code>, <code>three</code>, <code>four</code>, <code>five</code>, <code>six</code>, <code>seven</code>, <code>eight</code>, and <code>nine</code> <em>also</em> count as valid "digits".
     * <p>
     * Equipped with this new information, you now need to find the real first and last digit on each line.
     * For example:
     * <pre>
     * two1nine
     * eightwothree
     * abcone2threexyz
     * xtwone3four
     * 4nineeightseven2
     * zoneight234
     * 7pqrstsixteen
     * </pre>
     * <p>
     * In this example, the calibration values are <code>29</code>, <code>83</code>, <code>13</code>, <code>24</code>, <code>42</code>, <code>14</code>, and <code>76</code>.
     * Adding these together produces <code><em>281</em></code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of all of the calibration values?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return context.stream()
                .map(line -> Stream.iterate(NUMBERS.matcher(line), m -> m.find(m.hasMatch() ? m.toMatchResult().start() + 1 : 0), m -> m)
                        .map(Matcher::toMatchResult)
                        .distinct()
                        .map(MatchResult::group)
                        .map(str -> WORDS_TO_NUMBERS.getOrDefault(str, str))
                        .collect(Collectors.toList()))
                .map(matches -> matches.getFirst() + matches.getLast())
                .mapToInt(Integer::parseInt)
                .sum();
    }

}

