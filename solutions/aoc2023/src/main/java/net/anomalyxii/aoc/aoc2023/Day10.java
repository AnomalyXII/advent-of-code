package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.algorithms.AreaCalculator;
import net.anomalyxii.aoc.utils.algorithms.ShoelaceCalculator;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Direction;

import java.util.ArrayList;
import java.util.List;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 10: Pipe Maze.
 */
@Solution(year = 2023, day = 10, title = "Pipe Maze")
public class Day10 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You use the hang glider to ride the hot air from Desert Island all the way up to the floating metal island.
     * This island is surprisingly cold and there definitely aren't any thermals to glide on, so you leave your hang glider behind.
     * <p>
     * You wander around for a while, but you don't find any people or animals.
     * However, you do occasionally find signposts labeled "<a href="https://en.wikipedia.org/wiki/Hot_spring" target="_blank">Hot Springs</a>" pointing in a seemingly consistent direction; maybe you can find someone at the hot springs and ask them where the desert-machine parts are made.
     * <p>
     * The landscape here is alien; even the flowers and trees are made of metal.
     * As you stop to admire some metal grass, you notice something metallic scurry away in your peripheral vision and jump into a big pipe!
     * It didn't look like any animal you've ever seen; if you want a better look, you'll need to get ahead of it.
     * <p>
     * Scanning the area, you discover that the entire field you're standing on is <span title="Manufactured by Hamilton and Hilbert Pipe Company">densely packed with pipes</span>; it was hard to tell at first because they're the same metallic silver color as the "ground".
     * You make a quick sketch of all of the surface pipes you can see (your puzzle input).
     * <p>
     * The pipes are arranged in a two-dimensional grid of <em>tiles</em>:
     * <ul>
     * <li><code>|</code> is a <em>vertical pipe</em> connecting north and south.</li>
     * <li><code>-</code> is a <em>horizontal pipe</em> connecting east and west.</li>
     * <li><code>L</code> is a <em>90-degree bend</em> connecting north and east.</li>
     * <li><code>J</code> is a <em>90-degree bend</em> connecting north and west.</li>
     * <li><code>7</code> is a <em>90-degree bend</em> connecting south and west.</li>
     * <li><code>F</code> is a <em>90-degree bend</em> connecting south and east.</li>
     * <li><code>.</code> is <em>ground</em>; there is no pipe in this tile.</li>
     * <li><code>S</code> is the <em>starting position</em> of the animal; there is a pipe on this tile, but your sketch doesn't show what shape the pipe has.</li>
     * </ul>
     * <p>
     * Based on the acoustics of the animal's scurrying, you're confident the pipe that contains the animal is <em>one large, continuous loop</em>.
     * <p>
     * For example, here is a square loop of pipe:
     * <pre>
     * .....
     * .F-7.
     * .|.|.
     * .L-J.
     * .....
     * </pre>
     * <p>
     * If the animal had entered this loop in the northwest corner, the sketch would instead look like this:
     * <pre>
     * .....
     * .<em>S</em>-7.
     * .|.|.
     * .L-J.
     * .....
     * </pre>
     * <p>
     * In the above diagram, the <code>S</code> tile is still a 90-degree <code>F</code> bend: you can tell because of how the adjacent pipes connect to it.
     * <p>
     * Unfortunately, there are also many pipes that <em>aren't connected to the loop</em>!
     * This sketch shows the same loop as above:
     * <pre>
     * -L|F7
     * 7S-7|
     * L|7||
     * -L-J|
     * L|-JF
     * </pre>
     * <p>
     * In the above diagram, you can still figure out which pipes form the main loop: they're the ones connected to <code>S</code>, pipes those pipes connect to, pipes <em>those</em> pipes connect to, and so on.
     * Every pipe in the main loop connects to its two neighbors (including <code>S</code>, which will have exactly two pipes connecting to it, and which is assumed to connect back to those two pipes).
     * <p>
     * Here is a sketch that contains a slightly more complex main loop:
     * <pre>
     * ..F7.
     * .FJ|.
     * SJ.L7
     * |F--J
     * LJ...
     * </pre>
     * <p>
     * Here's the same example sketch with the extra, non-main-loop pipe tiles also shown:
     * <pre>
     * 7-F7-
     * .FJ|7
     * SJLL7
     * |F--J
     * LJ.LJ
     * </pre>
     * <p>
     * If you want to <em>get out ahead of the animal</em>, you should find the tile in the loop that is <em>farthest</em> from the starting position.
     * Because the animal is in the pipe, it doesn't make sense to measure this by direct distance.
     * Instead, you need to find the tile that would take the longest number of steps <em>along the loop</em> to reach from the starting point - regardless of which way around the loop the animal went.
     * <p>
     * In the first example with the square loop:
     * <pre>
     * .....
     * .S-7.
     * .|.|.
     * .L-J.
     * .....
     * </pre>
     * <p>
     * You can count the distance each tile in the loop is from the starting point like this:
     * <pre>
     * .....
     * .012.
     * .1.3.
     * .23<em>4</em>.
     * .....
     * </pre>
     * <p>
     * In this example, the farthest point from the start is <code><em>4</em></code> steps away.
     * <p>
     * Here's the more complex loop again:
     * <pre>
     * ..F7.
     * .FJ|.
     * SJ.L7
     * |F--J
     * LJ...
     * </pre>
     * <p>
     * Here are the distances for each tile on that loop:
     * <pre>
     * ..45.
     * .236.
     * 01.7<em>8</em>
     * 14567
     * 23...
     * </pre>
     * <p>
     * Find the single giant loop starting at <code>S</code>.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many steps along the loop does it take to get from the starting position to the point farthest from the starting position?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final Pipe pipe = Pipe.read(context.read());
        return pipe.length / 2;
    }

    /**
     * You quickly reach the farthest point of the loop, but the animal never emerges.
     * Maybe its nest is <em>within the area enclosed by the loop</em>?
     * <p>
     * To determine whether it's even worth taking the time to search for such a nest, you should calculate how many tiles are contained within the loop.
     * For example:
     * <pre>
     * ...........
     * .S-------7.
     * .|F-----7|.
     * .||.....||.
     * .||.....||.
     * .|L-7.F-J|.
     * .|..|.|..|.
     * .L--J.L--J.
     * ...........
     * </pre>
     * <p>
     * The above loop encloses merely <em>four tiles</em> - the two pairs of <code>.</code> in the southwest and southeast (marked <code>I</code> below).
     * The middle <code>.</code> tiles (marked <code>O</code> below) are <em>not</em> in the loop.
     * Here is the same loop again with those regions marked:
     * <pre>
     * ...........
     * .S-------7.
     * .|F-----7|.
     * .||<em>OOOOO</em>||.
     * .||<em>OOOOO</em>||.
     * .|L-7<em>O</em>F-J|.
     * .|<em>II</em>|<em>O</em>|<em>II</em>|.
     * .L--J<em>O</em>L--J.
     * .....<em>O</em>.....
     * </pre>
     * <p>
     * In fact, there doesn't even need to be a full tile path to the outside for tiles to count as outside the loop - squeezing between pipes is also allowed!
     * Here, <code>I</code> is still within the loop and <code>O</code> is still outside the loop:
     * <pre>
     * ..........
     * .S------7.
     * .|F----7|.
     * .||<em>OOOO</em>||.
     * .||<em>OOOO</em>||.
     * .|L-7F-J|.
     * .|<em>II</em>||<em>II</em>|.
     * .L--JL--J.
     * ..........
     * </pre>
     * <p>
     * In both of the above examples, <code><em>4</em></code> tiles are enclosed by the loop.
     * <p>
     * Here's a larger example:
     * <pre>
     * .F----7F7F7F7F-7....
     * .|F--7||||||||FJ....
     * .||.FJ||||||||L7....
     * FJL7L7LJLJ||LJ.L-7..
     * L--J.L7...LJS7F-7L7.
     * ....F-J..F7FJ|L7L7L7
     * ....L7.F7||L7|.L7L7|
     * .....|FJLJ|FJ|F7|.LJ
     * ....FJL-7.||.||||...
     * ....L---J.LJ.LJLJ...
     * </pre>
     * <p>
     * The above sketch has many random bits of ground, some of which are in the loop (<code>I</code>) and some of which are outside it (<code>O</code>):
     * <pre>
     * <em>O</em>F----7F7F7F7F-7<em>OOOO</em>
     * <em>O</em>|F--7||||||||FJ<em>OOOO</em>
     * <em>O</em>||<em>O</em>FJ||||||||L7<em>OOOO</em>
     * FJL7L7LJLJ||LJ<em>I</em>L-7<em>OO</em>
     * L--J<em>O</em>L7<em>III</em>LJS7F-7L7<em>O</em>
     * <em>OOOO</em>F-J<em>II</em>F7FJ|L7L7L7
     * <em>OOOO</em>L7<em>I</em>F7||L7|<em>I</em>L7L7|
     * <em>OOOOO</em>|FJLJ|FJ|F7|<em>O</em>LJ
     * <em>OOOO</em>FJL-7<em>O</em>||<em>O</em>||||<em>OOO</em>
     * <em>OOOO</em>L---J<em>O</em>LJ<em>O</em>LJLJ<em>OOO</em>
     * </pre>
     * <p>
     * In this larger example, <code><em>8</em></code> tiles are enclosed by the loop.
     * <p>
     * Any tile that isn't part of the main loop can count as being enclosed by the loop.
     * Here's another example with many bits of junk pipe lying around that aren't connected to the main loop at all:
     * <pre>
     * FF7FSF7F7F7F7F7F---7
     * L|LJ||||||||||||F--J
     * FL-7LJLJ||||||LJL-77
     * F--JF--7||LJLJ7F7FJ-
     * L---JF-JLJ.||-FJLJJ7
     * |F|F-JF---7F7-L7L|7|
     * |FFJF7L7F-JF7|JL---7
     * 7-L-JL7||F7|L7F-7F7|
     * L.L7LFJ|||||FJL7||LJ
     * L7JLJL-JLJLJL--JLJ.L
     * </pre>
     * <p>
     * Here are just the tiles that are <em>enclosed by the loop</em> marked with <code>I</code>:
     * <pre>
     * FF7FSF7F7F7F7F7F---7
     * L|LJ||||||||||||F--J
     * FL-7LJLJ||||||LJL-77
     * F--JF--7||LJLJ<em>I</em>F7FJ-
     * L---JF-JLJ<em>IIII</em>FJLJJ7
     * |F|F-JF---7<em>III</em>L7L|7|
     * |FFJF7L7F-JF7<em>II</em>L---7
     * 7-L-JL7||F7|L7F-7F7|
     * L.L7LFJ|||||FJL7||LJ
     * L7JLJL-JLJLJL--JLJ.L
     * </pre>
     * <p>
     * In this last example, <code><em>10</em></code> tiles are enclosed by the loop.
     * <p>
     * Figure out whether you have time to search for the nest by calculating the area within the loop.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return How many tiles are enclosed by the loop?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Pipe pipe = Pipe.read(context.read());
        return pipe.countInsideTiles();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final Pipe pipe = Pipe.read(context.read());
        final int count = pipe.countInsideTiles();
        return new IntTuple(pipe.length / 2, count);
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A closed-loop pipe, made up of segments.
     */
    private record Pipe(int length, List<Coordinate> turns) {

        // Helper Methods

        /*
         * Calculate the number of tiles inside the pipe area.
         */
        int countInsideTiles() {
            // Pick's theorem is used to calculate the area of an irregular shape,
            // based on the perimeter and number of internal points.
            // It is defined as: a = i + 0.5 * (p - 1), where:
            //   a => area
            //   i => internal points
            //   p => perimeter points

            // And we can calculate the area...
            final AreaCalculator calculator = new ShoelaceCalculator();
            final long area = calculator.area(turns);

            // ... and the perimeter...
            final int perimeter = length - 1;

            // ... and thus we can rewrite Pick's theorem as:
            //   a - 0.5 * (p - 1) = i
            // This should allow us to calculate the number of internal points!
            final long internalPoints = (long) (area - (0.5 * perimeter));

            // I'm not _exactly_ sure why we need to add 1 to the result...
            //    ... but hey, it works! :X
            return (int) internalPoints + 1;
        }

        // Static Helper Methods

        /*
         * Extract a `Pipe` from a given `Grid`, starting at the position marked
         * with an `S`.
         */
        public static Pipe read(final List<String> lines) {
            int length = 0;
            final List<Coordinate> turns = new ArrayList<>();

            final Coordinate start = findStartingCoordinate(lines);
            final Direction forwards = startForward(start, lines);
            final Direction backwards = startBackward(start, lines);

            Direction direction = backwards;
            Coordinate current = start;
            char type = resolveSegmentType(forwards, backwards);
            while (current != null) {
                ++length;
                if (type == 'F' || type == 'J' || type == 'L' || type == '7')
                    turns.add(current);

                final Coordinate next = current.adjustBy(direction);
                type = charInGrid(lines, next);
                if (type == 'S') {
                    current = null;
                    direction = null;
                } else {
                    direction = continueThroughPipeSegment(direction, type);
                    current = next;
                }
            }

            return new Pipe(length, turns);
        }

        /*
         * Find the starting `Coordinate`.
         */
        private static Coordinate findStartingCoordinate(final List<String> grid) {
            for (int y = 0; y < grid.size(); y++) {
                final String line = grid.get(y);
                final int idx = line.indexOf('S');
                if (idx >= 0) return new Coordinate(idx, y);
            }
            throw new IllegalStateException("No starting point found!");
        }

        /*
         * Resolve a `Segment` type based on the directions that attach to it.
         */
        private static char resolveSegmentType(final Direction forwards, final Direction backwards) {
            final char type;
            if (forwards == Direction.UP && backwards == Direction.DOWN) type = '|';
            else if (forwards == Direction.RIGHT && backwards == Direction.LEFT) type = '-';
            else if (forwards == Direction.UP && backwards == Direction.LEFT) type = 'J';
            else if (forwards == Direction.UP && backwards == Direction.RIGHT) type = 'L';
            else if (forwards == Direction.LEFT && backwards == Direction.DOWN) type = '7';
            else if (forwards == Direction.RIGHT && backwards == Direction.DOWN) type = 'F';
            else throw new IllegalStateException("Invalid starting combination: [" + forwards + "] [" + backwards + "]");
            return type;
        }

        /*
         * Continue through a pipe `Segment`, resolving the outgoing direction as
         * a `Direction`.
         */
        private static Direction continueThroughPipeSegment(final Direction incoming, final char pipe) {
            if (pipe == '|')
                if (incoming == Direction.UP) return Direction.UP;
                else if (incoming == Direction.DOWN) return Direction.DOWN;
                else throw invalidPipeAndDirectionCombination(pipe, incoming);

            if (pipe == '-')
                if (incoming == Direction.LEFT) return Direction.LEFT;
                else if (incoming == Direction.RIGHT) return Direction.RIGHT;
                else throw invalidPipeAndDirectionCombination(pipe, incoming);

            if (pipe == 'F')
                if (incoming == Direction.UP) return Direction.RIGHT;
                else if (incoming == Direction.LEFT) return Direction.DOWN;
                else throw invalidPipeAndDirectionCombination(pipe, incoming);

            if (pipe == '7')
                if (incoming == Direction.RIGHT) return Direction.DOWN;
                else if (incoming == Direction.UP) return Direction.LEFT;
                else throw invalidPipeAndDirectionCombination(pipe, incoming);

            if (pipe == 'J')
                if (incoming == Direction.RIGHT) return Direction.UP;
                else if (incoming == Direction.DOWN) return Direction.LEFT;
                else throw invalidPipeAndDirectionCombination(pipe, incoming);

            if (pipe == 'L')
                if (incoming == Direction.LEFT) return Direction.UP;
                else if (incoming == Direction.DOWN) return Direction.RIGHT;
                else throw invalidPipeAndDirectionCombination(pipe, incoming);

            throw new IllegalStateException("Cannot continue along a pipe of type [" + pipe + "] in coming from direction " + incoming);
        }

        /*
         * Pick an appropriate direction to start travelling "forward" along the
         * pipe.
         */
        private static Direction startForward(final Coordinate coord, final List<String> grid) {
            final Coordinate up = coord.adjustBy(Direction.UP);
            if (isInGrid(grid, up)) {
                final char pipe = charInGrid(grid, up);
                if (pipe == '|' || pipe == 'F' || pipe == '7') return Direction.UP;
            }

            final Coordinate right = coord.adjustBy(Direction.RIGHT);
            if (isInGrid(grid, right)) {
                final char pipe = charInGrid(grid, right);
                if (pipe == '-' || pipe == '7' || pipe == 'J') return Direction.RIGHT;
            }

            final Coordinate left = coord.adjustBy(Direction.LEFT);
            if (isInGrid(grid, left)) {
                final char pipe = charInGrid(grid, left);
                if (pipe == '-' || pipe == 'L' || pipe == 'F') return Direction.LEFT;
            }

            throw new IllegalStateException("Could not work out how to start going clockwise from " + coord);
        }

        /*
         * Pick an appropriate direction to start travelling "backward" along the
         * pipe.
         */
        private static Direction startBackward(final Coordinate coord, final List<String> grid) {
            final Coordinate down = coord.adjustBy(Direction.DOWN);
            if (isInGrid(grid, down)) {
                final char pipe = charInGrid(grid, down);
                if (pipe == '|' || pipe == 'L' || pipe == 'J') return Direction.DOWN;
            }

            final Coordinate left = coord.adjustBy(Direction.LEFT);
            if (isInGrid(grid, left)) {
                final char pipe = charInGrid(grid, left);
                if (pipe == '-' || pipe == 'F' || pipe == 'L') return Direction.LEFT;
            }

            final Coordinate right = coord.adjustBy(Direction.RIGHT);
            if (isInGrid(grid, right)) {
                final char pipe = charInGrid(grid, right);
                if (pipe == '-' || pipe == '7' || pipe == 'F') return Direction.RIGHT;
            }

            throw new IllegalStateException("Could not work out how to start going anticlockwise from " + coord);
        }

        /*
         * Check if a given `Coordinate` is inside the grid.
         */
        private static boolean isInGrid(final List<String> grid, final Coordinate coord) {
            return coord.y() >= 0 && coord.x() >= 0
                    && coord.y() < grid.size() && coord.x() < grid.get(coord.y()).length();
        }

        /*
         * Get the character at a given `Coordinate` in the grid.
         */
        private static char charInGrid(final List<String> grid, final Coordinate coord) {
            return grid.get(coord.y()).charAt(coord.x());
        }

        /*
         * Throw an error if the intended direction is incompatible with the
         * current type of pipe.
         */
        private static IllegalArgumentException invalidPipeAndDirectionCombination(final int pipe, final Direction velocity) {
            throw new IllegalArgumentException("Cannot continue with a " + velocity + " along a pipe of type " + (char) pipe);
        }

    }

}
