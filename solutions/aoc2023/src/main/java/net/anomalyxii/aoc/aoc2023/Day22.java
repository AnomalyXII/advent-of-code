package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Area;
import net.anomalyxii.aoc.utils.geometry.Bounds;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 22: Sand Slabs.
 */
@Solution(year = 2023, day = 22, title = "Sand Slabs")
public class Day22 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Enough sand has fallen; it can finally filter water for Snow Island.
     * <p>
     * Well, <em>almost</em>.
     * <p>
     * The sand has been falling as large compacted <em>bricks</em> of sand, piling up to form an impressive stack here near the edge of Island Island.
     * In order to make use of the sand to filter water, some of the bricks will need to be broken apart - nay, <em><span title="Disintegrate - X,R&#10;Sorcery&#10;Destroy X target bricks of sand. They cannot be regenerated. Create 32768 0/1 colorless Sand artifact creature tokens for each brick of sand destroyed in this way.">disintegrated</span></em> - back into freely flowing sand.
     * <p>
     * The stack is tall enough that you'll have to be careful about choosing which bricks to disintegrate; if you disintegrate the wrong brick, large portions of the stack could topple, which sounds pretty dangerous.
     * <p>
     * The Elves responsible for water filtering operations took a <em>snapshot of the bricks while they were still falling</em> (your puzzle input) which should let you work out which bricks are safe to disintegrate.
     * For example:
     * <pre>
     * 1,0,1~1,2,1
     * 0,0,2~2,0,2
     * 0,2,3~2,2,3
     * 0,0,4~0,2,4
     * 2,0,5~2,2,5
     * 0,1,6~2,1,6
     * 1,1,8~1,1,9
     * </pre>
     * <p>
     * Each line of text in the snapshot represents the position of a single brick at the time the snapshot was taken.
     * The position is given as two <code>x,y,z</code> coordinates - one for each end of the brick - separated by a tilde (<code>~</code>).
     * Each brick is made up of a single straight line of cubes, and the Elves were even careful to choose a time for the snapshot that had all of the free-falling bricks at <em>integer positions above the ground</em>, so the whole snapshot is aligned to a three-dimensional cube grid.
     * <p>
     * A line like <code>2,2,2~2,2,2</code> means that both ends of the brick are at the same coordinate - in other words, that the brick is a single cube.
     * <p>
     * Lines like <code>0,0,10~1,0,10</code> or <code>0,0,10~0,1,10</code> both represent bricks that are <em>two cubes</em> in volume, both oriented horizontally.
     * The first brick extends in the <code>x</code> direction, while the second brick extends in the <code>y</code> direction.
     * <p>
     * A line like <code>0,0,1~0,0,10</code> represents a <em>ten-cube brick</em> which is oriented <em>vertically</em>.
     * One end of the brick is the cube located at <code>0,0,1</code>, while the other end of the brick is located directly above it at <code>0,0,10</code>.
     * <p>
     * The ground is at <code>z=0</code> and is perfectly flat; the lowest <code>z</code> value a brick can have is therefore <code>1</code>.
     * So, <code>5,5,1~5,6,1</code> and <code>0,2,1~0,2,5</code> are both resting on the ground, but <code>3,3,2~3,3,3</code> was above the ground at the time of the snapshot.
     * <p>
     * Because the snapshot was taken while the bricks were still falling, some bricks will <em>still be in the air</em>; you'll need to start by figuring out where they will end up.
     * Bricks are magically stabilized, so they <em>never rotate</em>, even in weird situations like where a long horizontal brick is only supported on one end.
     * Two bricks cannot occupy the same position, so a falling brick will come to rest upon the first other brick it encounters.
     * <p>
     * Here is the same example again, this time with each brick given a letter so it can be marked in diagrams:
     * <pre>
     * 1,0,1~1,2,1   &lt;- A
     * 0,0,2~2,0,2   &lt;- B
     * 0,2,3~2,2,3   &lt;- C
     * 0,0,4~0,2,4   &lt;- D
     * 2,0,5~2,2,5   &lt;- E
     * 0,1,6~2,1,6   &lt;- F
     * 1,1,8~1,1,9   &lt;- G
     * </pre>
     * <p>
     * At the time of the snapshot, from the side so the <code>x</code> axis goes left to right, these bricks are arranged like this:
     * <pre>
     *  x
     * 012
     * .G. 9
     * .G. 8
     * ... 7
     * FFF 6
     * ..E 5 z
     * D.. 4
     * CCC 3
     * BBB 2
     * .A. 1
     * --- 0
     * </pre>
     * <p>
     * Rotating the perspective 90 degrees so the <code>y</code> axis now goes left to right, the same bricks are arranged like this:
     * <pre>
     *  y
     * 012
     * .G. 9
     * .G. 8
     * ... 7
     * .F. 6
     * EEE 5 z
     * DDD 4
     * ..C 3
     * B.. 2
     * AAA 1
     * --- 0
     * </pre>
     * <p>
     * Once all of the bricks fall downward as far as they can go, the stack looks like this, where <code>?</code> means bricks are hidden behind other bricks at that location:
     * <pre>
     *  x
     * 012
     * .G. 6
     * .G. 5
     * FFF 4
     * D.E 3 z
     * ??? 2
     * .A. 1
     * --- 0
     * </pre>
     * <p>
     * Again from the side:
     * <pre>
     *  y
     * 012
     * .G. 6
     * .G. 5
     * .F. 4
     * ??? 3 z
     * B.C 2
     * AAA 1
     * --- 0
     * </pre>
     * <p>
     * Now that all of the bricks have settled, it becomes easier to tell which bricks are supporting which other bricks:
     * <ul>
     * <li>Brick <code>A</code> is the only brick supporting bricks <code>B</code> and <code>C</code>.</li>
     * <li>Brick <code>B</code> is one of two bricks supporting brick <code>D</code> and brick <code>E</code>.</li>
     * <li>Brick <code>C</code> is the other brick supporting brick <code>D</code> and brick <code>E</code>.</li>
     * <li>Brick <code>D</code> supports brick <code>F</code>.</li>
     * <li>Brick <code>E</code> also supports brick <code>F</code>.</li>
     * <li>Brick <code>F</code> supports brick <code>G</code>.</li>
     * <li>Brick <code>G</code> isn't supporting any bricks.</li>
     * </ul>
     * <p>
     * Your first task is to figure out <em>which bricks are safe to disintegrate</em>.
     * A brick can be safely disintegrated if, after removing it, <em>no other bricks</em> would fall further directly downward.
     * Don't actually disintegrate any bricks - just determine what would happen if, for each brick, only that brick were disintegrated.
     * Bricks can be disintegrated even if they're completely surrounded by other bricks; you can squeeze between bricks if you need to.
     * <p>
     * In this example, the bricks can be disintegrated as follows:
     * <ul>
     * <li>Brick <code>A</code> cannot be disintegrated safely; if it were disintegrated, bricks <code>B</code> and <code>C</code> would both fall.</li>
     * <li>Brick <code>B</code> <em>can</em> be disintegrated; the bricks above it (<code>D</code> and <code>E</code>) would still be supported by brick <code>C</code>.</li>
     * <li>Brick <code>C</code> <em>can</em> be disintegrated; the bricks above it (<code>D</code> and <code>E</code>) would still be supported by brick <code>B</code>.</li>
     * <li>Brick <code>D</code> <em>can</em> be disintegrated; the brick above it (<code>F</code>) would still be supported by brick <code>E</code>.</li>
     * <li>Brick <code>E</code> <em>can</em> be disintegrated; the brick above it (<code>F</code>) would still be supported by brick <code>D</code>.</li>
     * <li>Brick <code>F</code> cannot be disintegrated; the brick above it (<code>G</code>) would fall.</li>
     * <li>Brick <code>G</code> <em>can</em> be disintegrated; it does not support any other bricks.</li>
     * </ul>
     * <p>
     * So, in this example, <code><em>5</em></code> bricks can be safely disintegrated.
     * <p>
     * Figure how the blocks will settle based on the snapshot.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Once they've settled, consider disintegrating a single brick; how many bricks could be safely chosen as the one to get disintegrated?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final Set<Brick> bricks = Brick.simulateFalling(context);
        return bricks.stream()
                .filter(brick -> brick.countDependents() == 0)
                .mapToInt(brick -> 1)
                .sum();
    }

    /**
     * Disintegrating bricks one at a time isn't going to be fast enough.
     * While it might sound dangerous, what you really need is a <em>chain reaction</em>.
     * <p>
     * You'll need to figure out the best brick to disintegrate.
     * For each brick, determine how many <em>other bricks would fall</em> if that brick were disintegrated.
     * <p>
     * Using the same example as above:
     * <ul>
     * <li>Disintegrating brick <code>A</code> would cause all <code><em>6</em></code> other bricks to fall.</li>
     * <li>Disintegrating brick <code>F</code> would cause only <code><em>1</em></code> other brick, <code>G</code>, to fall.</li>
     * </ul>
     * <p>
     * Disintegrating any other brick would cause <em>no other bricks</em> to fall.
     * So, in this example, the sum of <em>the number of other bricks that would fall</em> as a result of disintegrating each brick is <code><em>7</em></code>.
     * <p>
     * For each brick, determine how many <em>other bricks</em> would fall if that brick were disintegrated.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of the number of other bricks that would fall?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Set<Brick> bricks = Brick.simulateFalling(context);
        return bricks.stream()
                .mapToInt(Brick::countChainReaction)
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final Set<Brick> bricks = Brick.simulateFalling(context);
        return bricks.stream()
                .reduce(
                        IntTuple.NULL,
                        (answer, brick) -> {
                            final int mod1 = brick.countDependents() == 0 ? 1 : 0;
                            final int mod2 = brick.countChainReaction();

                            return answer.add(mod1, mod2);
                        },
                        IntTuple::add
                );
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * A sand brick.
     */
    private static final class Brick implements Comparable<Brick> {

        /*
         * Not _really_ a `Brick`, but represents the ground level.
         */
        private static final Brick GROUND = new Brick(Area.INFINITY, Bounds.ZERO);

        /*
         * How to compare and thus sort `Brick`s.
         */
        private static final Comparator<Brick> COMPARATOR = Comparator.comparing((Brick r) -> r.depth)
                .thenComparing((Brick r) -> r.surface.w())
                .thenComparing((Brick r) -> r.surface.h());

        // Private Members

        private final Area surface;
        private final Set<Brick> supporting = new HashSet<>();
        private final Set<Brick> supportedBy = new HashSet<>();

        private Bounds depth;

        // Constructors

        private Brick(final Area surface, final Bounds depth) {
            this.surface = surface;
            this.depth = depth;
        }

        // Helper Methods

        /*
         * Register that this `Brick` is supporting the given other `Brick`.
         */
        void registerSupporting(final Brick other) {
            this.supporting.add(other);
        }

        /*
         * Count the number of supported `Brick`s that would fall is this `Brick`
         * was disintegrated.
         */
        int countDependents() {
            return supporting.stream()
                    .filter(next -> next.supportedBy.size() == 1)
                    .mapToInt(i -> 1)
                    .sum();
        }

        /*
         * Count the total number of `Brick`s that would fall if this `Brick` was
         * disintegrated.
         */
        int countChainReaction() {
            final Set<Brick> fallen = new HashSet<>();

            Set<Brick> wouldFall = Set.of(this);
            do {
                fallen.addAll(wouldFall);
                wouldFall = wouldFall.stream()
                        .flatMap(brick -> brick.supporting.stream())
                        .filter(next -> next.wouldFall(fallen))
                        .collect(Collectors.toSet());

            } while (!wouldFall.isEmpty());

            return fallen.size() - 1;
        }

        /*
         * Simulate this `Brick` falling one unit.
         */
        void fall(final SortedSet<Brick> fallenBricks) {
            final SortedSet<Brick> overlapping = fallenBricks.stream()
                    .filter(other -> !surface.intersect(other.surface).isNull())
                    .reduce(
                            new TreeSet<>(Set.of(GROUND)),
                            (set, other) -> {
                                final Brick first = set.getFirst();
                                if (other.depth.max() > first.depth.max())
                                    return new TreeSet<>(Set.of(other));

                                if (other.depth.max() == first.depth.max())
                                    set.add(other);
                                return set;
                            },
                            (a, b) -> {
                                throw new IllegalStateException("Should not need to merge!");
                            }
                    );

            final Brick first = overlapping.first();
            final int adjust = depth.min() - (first.depth.max() + 1);
            if (adjust != 0)
                depth = Bounds.of(depth.min() - adjust, depth.max() - adjust);

            supportedBy.addAll(overlapping);
            overlapping.forEach(support -> support.registerSupporting(this));
        }

        /*
         * Check if this `Brick` would fall down if all of the given other
         * `Brick`s had either fallen or been disintegrated.
         */
        private boolean wouldFall(final Set<Brick> others) {
            final HashSet<Brick> hypothetical = new HashSet<>(supportedBy);
            hypothetical.removeAll(others);
            return hypothetical.isEmpty();
        }

        // Comparable Methods

        @Override
        public int compareTo(final Brick o) {
            return COMPARATOR.compare(this, o);
        }

        // Equals & Hash Code

        @Override
        public boolean equals(final Object obj) {
            if (obj == this) return true;
            if (obj == null || obj.getClass() != this.getClass()) return false;
            final Brick that = (Brick) obj;
            return Objects.equals(this.surface, that.surface)
                    && Objects.equals(this.depth, that.depth);
        }

        @Override
        public int hashCode() {
            return Objects.hash(surface, depth);
        }

        // Static Helper Methods

        /*
         * Simulate the falling of `Brick`s.
         */
        static SortedSet<Brick> simulateFalling(final SolutionContext context) {
            final SortedSet<Brick> bricks = context.stream()
                    .map(Brick::parse)
                    .collect(Collectors.toCollection(TreeSet::new));

            final SortedSet<Brick> fallenBricks = new TreeSet<>();
            for (final Brick brick : bricks) {
                brick.fall(fallenBricks);
                fallenBricks.add(brick);
            }

            return fallenBricks;
        }

        /*
         * Parse a rock.
         */
        private static Brick parse(final String line) {
            final String[] parts = line.split("~");
            final int[] front = stream(parts[0].split(","))
                    .mapToInt(Integer::parseInt)
                    .toArray();
            final int[] back = stream(parts[1].split(","))
                    .mapToInt(Integer::parseInt)
                    .toArray();

            return new Brick(
                    Area.of(Bounds.of(front[0], back[0]), Bounds.of(front[1], back[1])),
                    Bounds.of(front[2], back[2])
            );
        }

    }


}

