package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 7: Camel Cards.
 */
@Solution(year = 2023, day = 7, title = "Camel Cards")
public class Day7 {

    /*
     * Index of the Jokers.
     */
    private static final int JARED_LETOS = 0;

    /*
     * The relative score of various cards:
     */
    private static final int VALUE_JOKER = 0;
    private static final int VALUE_2 = 1;
    private static final int VALUE_3 = 2;
    private static final int VALUE_4 = 3;
    private static final int VALUE_5 = 4;
    private static final int VALUE_6 = 5;
    private static final int VALUE_7 = 6;
    private static final int VALUE_8 = 7;
    private static final int VALUE_9 = 8;
    private static final int VALUE_10 = 9;
    private static final int VALUE_JACK = 10;
    private static final int VALUE_QUEEN = 11;
    private static final int VALUE_KING = 12;
    private static final int VALUE_ACE = 13;

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * Your all-expenses-paid trip turns out to be a one-way, five-minute ride in an <a href="https://en.wikipedia.org/wiki/Airship" target="_blank">airship</a>.
     * (At least it's a <span title="Please only read this sentence while listening to 'The Airship Blackjack' from the Final Fantasy 6 soundtrack."><em>cool</em> airship</span>!)
     * It drops you off at the edge of a vast desert and descends back to Island Island.
     * <p>
     * "Did you bring the parts?"
     * <p>
     * You turn around to see an Elf completely covered in white clothing, wearing goggles, and riding a large <a href="https://en.wikipedia.org/wiki/Dromedary" target="_blank">camel</a>.
     * <p>
     * "Did you bring the parts?" she asks again, louder this time.
     * You aren't sure what parts she's looking for; you're here to figure out why the sand stopped.
     * <p>
     * "The parts! For the sand, yes! Come with me; I will show you."
     * She beckons you onto the camel.
     * <p>
     * After riding a bit across the sands of Desert Island, you can see what look like very large rocks covering half of the horizon.
     * The Elf explains that the rocks are all along the part of Desert Island that is directly above Island Island, making it hard to even get there.
     * Normally, they use big machines to move the rocks and filter the sand, but the machines have broken down because Desert Island recently stopped receiving the <em>parts</em> they need to fix the machines.
     * <p>
     * You've already assumed it'll be your job to figure out why the parts stopped when she asks if you can help.
     * You agree automatically.
     * <p>
     * Because the journey will take a few days, she offers to teach you the game of <em>Camel Cards</em>.
     * Camel Cards is sort of similar to <a href="https://en.wikipedia.org/wiki/List_of_poker_hands" target="_blank">poker</a> except it's designed to be easier to play while riding a camel.
     * <p>
     * In Camel Cards, you get a list of <em>hands</em>, and your goal is to order them based on the <em>strength</em> of each hand.
     * A hand consists of <em>five cards</em> labeled one of <code>A</code>, <code>K</code>, <code>Q</code>, <code>J</code>, <code>T</code>, <code>9</code>, <code>8</code>, <code>7</code>, <code>6</code>, <code>5</code>, <code>4</code>, <code>3</code>, or <code>2</code>.
     * The relative strength of each card follows this order, where <code>A</code> is the highest and <code>2</code> is the lowest.
     * <p>
     * Every hand is exactly one <em>type</em>.
     * From strongest to weakest, they are:
     * <ul>
     * <li><em>Five of a kind</em>, where all five cards have the same label: <code>AAAAA</code></li>
     * <li><em>Four of a kind</em>, where four cards have the same label and one card has a different label: <code>AA8AA</code></li>
     * <li><em>Full house</em>, where three cards have the same label, and the remaining two cards share a different label: <code>23332</code></li>
     * <li><em>Three of a kind</em>, where three cards have the same label, and the remaining two cards are each different from any other card in the hand: <code>TTT98</code></li>
     * <li><em>Two pair</em>, where two cards share one label, two other cards share a second label, and the remaining card has a third label: <code>23432</code></li>
     * <li><em>One pair</em>, where two cards share one label, and the other three cards have a different label from the pair and each other: <code>A23A4</code></li>
     * <li><em>High card</em>, where all cards' labels are distinct: <code>23456</code></li>
     * </ul>
     * <p>
     * Hands are primarily ordered based on type; for example, every <em>full house</em> is stronger than any <em>three of a kind</em>.
     * <p>
     * If two hands have the same type, a second ordering rule takes effect.
     * Start by comparing the <em>first card in each hand</em>.
     * If these cards are different, the hand with the stronger first card is considered stronger.
     * If the first card in each hand have the <em>same label</em>, however, then move on to considering the <em>second card in each hand</em>.
     * If they differ, the hand with the higher second card wins; otherwise, continue with the third card in each hand, then the fourth, then the fifth.
     * <p>
     * So, <code>33332</code> and <code>2AAAA</code> are both <em>four of a kind</em> hands, but <code>33332</code> is stronger because its first card is stronger.
     * Similarly, <code>77888</code> and <code>77788</code> are both a <em>full house</em>, but <code>77888</code> is stronger because its third card is stronger (and both hands have the same first and second card).
     * <p>
     * To play Camel Cards, you are given a list of hands and their corresponding <em>bid</em> (your puzzle input).
     * For example:
     * <pre>
     * 32T3K 765
     * T55J5 684
     * KK677 28
     * KTJJT 220
     * QQQJA 483
     * </pre>
     * <p>
     * This example shows five hands; each hand is followed by its <em>bid</em> amount.
     * Each hand wins an amount equal to its bid multiplied by its <em>rank</em>, where the weakest hand gets rank 1, the second-weakest hand gets rank 2, and so on up to the strongest hand.
     * Because there are five hands in this example, the strongest hand will have rank 5 and its bid will be multiplied by 5.
     * <p>
     * So, the first step is to put the hands in order of strength:
     * <ul>
     * <li><code>32T3K</code> is the only <em>one pair</em> and the other hands are all a stronger type, so it gets rank <em>1</em>.</li>
     * <li><code>KK677</code> and <code>KTJJT</code> are both <em>two pair</em>.
     * Their first cards both have the same label, but the second card of <code>KK677</code> is stronger (<code>K</code> vs <code>T</code>), so <code>KTJJT</code> gets rank <em>2</em> and <code>KK677</code> gets rank <em>3</em>.</li>
     * <li><code>T55J5</code> and <code>QQQJA</code> are both <em>three of a kind</em>.
     * <code>QQQJA</code> has a stronger first card, so it gets rank <em>5</em> and <code>T55J5</code> gets rank <em>4</em>.</li>
     * </ul>
     * <p>
     * Now, you can determine the total winnings of this set of hands by adding up the result of multiplying each hand's bid with its rank (<code>765</code> * 1 + <code>220</code> * 2 + <code>28</code> * 3 + <code>684</code> * 4 + <code>483</code> * 5).
     * So the <em>total winnings</em> in this example are <code><em>6440</em></code>.
     * <p>
     * Find the rank of every hand in your set.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What are the total winnings?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final Hand[] hands = context.stream()
                .map(line -> Hand.parse(line, VALUE_JACK))
                .sorted()
                .toArray(Hand[]::new);
        return IntStream.range(0, hands.length)
                .map(pos -> (hands.length - pos) * hands[pos].wager)
                .sum();
    }

    /**
     * To make things a little more interesting, the Elf introduces one additional rule.
     * Now, <code>J</code> cards are <a href="https://en.wikipedia.org/wiki/Joker_(playing_card)" target="_blank">jokers</a> - wildcards that can act like whatever card would make the hand the strongest type possible.
     * <p>
     * To balance this, <em><code>J</code> cards are now the weakest</em> individual cards, weaker even than <code>2</code>.
     * The other cards stay in the same order: <code>A</code>, <code>K</code>, <code>Q</code>, <code>T</code>, <code>9</code>, <code>8</code>, <code>7</code>, <code>6</code>, <code>5</code>, <code>4</code>, <code>3</code>, <code>2</code>, <code>J</code>.
     * <p>
     * <code>J</code> cards can pretend to be whatever card is best for the purpose of determining hand type; for example, <code>QJJQ2</code> is now considered <em>four of a kind</em>.
     * However, for the purpose of breaking ties between two hands of the same type, <code>J</code> is always treated as <code>J</code>, not the card it's pretending to be: <code>JKKK2</code> is weaker than <code>QQQQ2</code> because <code>J</code> is weaker than <code>Q</code>.
     * <p>
     * Now, the above example goes very differently:
     * <pre>
     * 32T3K 765
     * T55J5 684
     * KK677 28
     * KTJJT 220
     * QQQJA 483
     * </pre>
     * <ul>
     * <li><code>32T3K</code> is still the only <em>one pair</em>; it doesn't contain any jokers, so its strength doesn't increase.</li>
     * <li><code>KK677</code> is now the only <em>two pair</em>, making it the second-weakest hand.</li>
     * <li>
     * <code>T55J5</code>, <code>KTJJT</code>, and <code>QQQJA</code> are now all <em>four of a kind</em>!
     * <code>T55J5</code> gets rank 3, <code>QQQJA</code> gets rank 4, and <code>KTJJT</code> gets rank 5.
     * </li>
     * </ul>
     * <p>
     * With the new joker rule, the total winnings in this example are <code><em>5905</em></code>.
     * <p>
     * Using the new joker rule, find the rank of every hand in your set.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What are the new total winnings?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Hand[] hands = context.stream()
                .map(line -> Hand.parse(line, VALUE_JOKER))
                .sorted()
                .toArray(Hand[]::new);
        return IntStream.range(0, hands.length)
                .map(pos -> (hands.length - pos) * hands[pos].wager)
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        int count = 0;
        final List<Hand> part1 = new ArrayList<>();
        final List<Hand> part2 = new ArrayList<>();
        for (final String line : context.read()) {
            ++count;
            part1.add(Hand.parse(line, VALUE_JACK));
            part2.add(Hand.parse(line, VALUE_JOKER));
        }

        part1.sort(Hand::compareTo);
        part2.sort(Hand::compareTo);

        int answer1 = 0;
        int answer2 = 0;
        for (int pos = 0; pos < count; pos++) {
            answer1 += (part1.size() - pos) * part1.get(pos).wager;
            answer2 += (part2.size() - pos) * part2.get(pos).wager;
        }

        return new IntTuple(answer1, answer2);
    }

    // ****************************************
    // Private Helper Classes
    // ****************************************

    /*
     * The kind of hand being held.
     */
    private enum Kind {

        /*
         * 5 cards of the same value.
         */
        FIVE_OF_A_KIND {
            @Override
            protected Kind combine(final Kind other) {
                throw throwIllegalCombination(other);
            }

            @Override
            protected Kind improve(final int numJokers) {
                throw throwIllegalImprovement(numJokers);
            }
        },

        /*
         * 4 cards of the same value.
         */
        FOUR_OF_A_KIND {
            @Override
            protected Kind combine(final Kind other) {
                if (other == HIGH_CARD) return FOUR_OF_A_KIND;
                throw throwIllegalCombination(other);
            }

            @Override
            protected Kind improve(final int numJokers) {
                if (numJokers == 1) return FIVE_OF_A_KIND;
                throw throwIllegalImprovement(numJokers);
            }
        },

        /*
         * 1 pair and 1 3-of-a-kind.
         */
        FULL_HOUSE {
            @Override
            protected Kind combine(final Kind other) {
                throw throwIllegalCombination(other);
            }

            @Override
            protected Kind improve(final int numJokers) {
                if (numJokers == 1) return FOUR_OF_A_KIND;
                throw throwIllegalImprovement(numJokers);
            }
        },

        /*
         * 3 cards of the same value.
         */
        THREE_OF_A_KIND {
            @Override
            protected Kind combine(final Kind other) {
                if (other == ONE_PAIR) return FULL_HOUSE;
                if (other == HIGH_CARD) return THREE_OF_A_KIND;
                throw throwIllegalCombination(other);
            }

            @Override
            protected Kind improve(final int numJokers) {
                if (numJokers == 1) return FOUR_OF_A_KIND;
                if (numJokers == 2) return FIVE_OF_A_KIND;
                throw throwIllegalImprovement(numJokers);
            }
        },

        /*
         * 2 pairs (what else is there to say?).
         */
        TWO_PAIR {
            @Override
            protected Kind combine(final Kind other) {
                if (other == HIGH_CARD) return TWO_PAIR;
                throw throwIllegalCombination(other);
            }

            @Override
            protected Kind improve(final int numJokers) {
                if (numJokers == 1) return FULL_HOUSE;
                throw throwIllegalImprovement(numJokers);
            }
        },

        /*
         * 2 cards of the same value.
         */
        ONE_PAIR {
            @Override
            protected Kind combine(final Kind other) {
                if (other == ONE_PAIR) return TWO_PAIR;
                if (other == THREE_OF_A_KIND) return FULL_HOUSE;
                if (other == HIGH_CARD) return ONE_PAIR;
                throw throwIllegalCombination(other);
            }

            @Override
            protected Kind improve(final int numJokers) {
                if (numJokers == 1) return THREE_OF_A_KIND;
                if (numJokers == 2) return FOUR_OF_A_KIND;
                if (numJokers == 3) return FIVE_OF_A_KIND;
                throw throwIllegalImprovement(numJokers);
            }
        },

        /*
         * Any single card.
         */
        HIGH_CARD {
            @Override
            protected Kind combine(final Kind other) {
                return other;
            }

            @Override
            protected Kind improve(final int numJokers) {
                if (numJokers == 1) return ONE_PAIR;
                if (numJokers == 2) return THREE_OF_A_KIND;
                if (numJokers == 3) return FOUR_OF_A_KIND;
                if (numJokers == 4) return FIVE_OF_A_KIND;
                throw throwIllegalImprovement(numJokers);
            }
        },

        /*
         * I guess maybe this is what happens when you play Camel Cards with a
         * crocodile.
         */
        NONE {
            @Override
            protected Kind combine(final Kind other) {
                return other;
            }

            @Override
            protected Kind improve(final int numJokers) {
                if (numJokers == 1) return HIGH_CARD;
                if (numJokers == 2) return TWO_PAIR;
                if (numJokers == 3) return THREE_OF_A_KIND;
                if (numJokers == 4) return FOUR_OF_A_KIND;
                if (numJokers == 5) return FIVE_OF_A_KIND;
                throw throwIllegalImprovement(numJokers);
            }
        },

        // End of constants
        ;

        // Helper Methods

        /*
         * Possibly upgrade the current hand based on a new set of cards.
         */
        protected abstract Kind combine(Kind other);

        /*
         * Possibly upgrade the current hand based on a number of jokers.
         */
        protected abstract Kind improve(int numJokers);

        /*
         * Throw an error if the current hand is incompatible with the suggested
         * other hand. This would normally happen if the number of cards in the
         * combined hand would exceed 5.
         */
        protected IllegalStateException throwIllegalCombination(final Kind other) {
            return new IllegalStateException("Cannot combine " + this + " and " + other);
        }

        /*
         * Throw an error if the current hand is incompatible with the provided
         * number of jokers. This would normally happen if the number of cards
         * in the improved hand would exceed 5.
         */
        protected IllegalStateException throwIllegalImprovement(final int numJokers) {
            return new IllegalStateException("Cannot improve " + this + " with " + numJokers);
        }

        // Static Helper Methods

        /*
         * Detect what kind of hand is being held.
         */
        private static Kind detect(final int[] cards) {
            final int[] count = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            for (final int card : cards) ++count[card];

            Kind best = NONE;
            for (int k = 1; k <= 13; k++) {
                best = switch (count[k]) {
                    case 5 -> best.combine(FIVE_OF_A_KIND);
                    case 4 -> best.combine(FOUR_OF_A_KIND);
                    case 3 -> best.combine(THREE_OF_A_KIND);
                    case 2 -> best.combine(ONE_PAIR);
                    case 1 -> best.combine(HIGH_CARD);
                    default -> best;
                };
            }

            return count[JARED_LETOS] > 0 ? best.improve(count[0]) : best;
        }

    }

    /*
     * Represents one hand in a game of cards, plus the associated wager.
     */
    private record Hand(int[] cards, int wager) implements Comparable<Hand> {

        // Comparable Methods

        @Override
        public int compareTo(final Hand o) {
            final Kind myKind = Kind.detect(cards);
            final Kind otherKind = Kind.detect(o.cards);
            final int cmp = myKind.compareTo(otherKind);
            if (cmp != 0)
                return cmp;

            // final int[] myCards = IntStream.of(cards).sorted().toArray();
            final int[] myCards = cards;
            // final int[] otherCards = IntStream.of(o.cards).sorted().toArray();
            final int[] otherCards = o.cards;
            for (int c = 0; c < myCards.length; c++)
                if (myCards[c] > otherCards[c]) return -1;
                else if (myCards[c] < otherCards[c]) return 1;

            return 0;
        }

        // Static Helper Methods

        /*
         * Parse a `Hand` from a given line.
         */
        static Hand parse(final String line, final int valueJ) {
            final String[] parts = line.split("\\s+", 2);
            return new Hand(
                    parts[0].chars()
                            .map(chr -> toCardValue(chr, valueJ))
                            .toArray(),
                    Integer.parseInt(parts[1])
            );
        }

        /*
         * Convert a single card into a comparable value.
         */
        private static int toCardValue(final int chr, final int valueJ) {
            return switch (chr) {
                case 'A' -> VALUE_ACE;
                case 'K' -> VALUE_KING;
                case 'Q' -> VALUE_QUEEN;
                case 'J' -> valueJ;
                case 'T' -> VALUE_10;
                case '9' -> VALUE_9;
                case '8' -> VALUE_8;
                case '7' -> VALUE_7;
                case '6' -> VALUE_6;
                case '5' -> VALUE_5;
                case '4' -> VALUE_4;
                case '3' -> VALUE_3;
                case '2' -> VALUE_2;
                default -> throw new IllegalArgumentException("Invalid card: " + chr);
            };
        }

    }

}

