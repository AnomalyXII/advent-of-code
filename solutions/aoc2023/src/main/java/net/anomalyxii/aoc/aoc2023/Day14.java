package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Direction;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.HashMap;
import java.util.Map;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 14: Parabolic Reflector Dish.
 */
@Solution(year = 2023, day = 14, title = "Parabolic Reflector Dish")
public class Day14 {

    /*
     * Number of iterations of tilting.
     */
    private static final int NUMBER_OF_ITERATIONS = 1000000000;

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You reach the place where all of the mirrors were pointing: a massive <a href="https://en.wikipedia.org/wiki/Parabolic_reflector" target="_blank">parabolic reflector dish</a> <span title="Why, where do you attach YOUR massive parabolic reflector dishes?">attached</span> to the side of another large mountain.
     * <p>
     * The dish is made up of many small mirrors, but while the mirrors themselves are roughly in the shape of a parabolic reflector dish, each individual mirror seems to be pointing in slightly the wrong direction.
     * If the dish is meant to focus light, all it's doing right now is sending it in a vague direction.
     * <p>
     * This system must be what provides the energy for the lava!
     * If you focus the reflector dish, maybe you can go where it's pointing and use the light to fix the lava production.
     * <p>
     * Upon closer inspection, the individual mirrors each appear to be connected via an elaborate system of ropes and pulleys to a large metal platform below the dish.
     * The platform is covered in large rocks of various shapes.
     * Depending on their position, the weight of the rocks deforms the platform, and the shape of the platform controls which ropes move and ultimately the focus of the dish.
     * <p>
     * In short: if you move the rocks, you can focus the dish.
     * The platform even has a control panel on the side that lets you <em>tilt</em> it in one of four directions!
     * The rounded rocks (<code>O</code>) will roll when the platform is tilted, while the cube-shaped rocks (<code>#</code>) will stay in place.
     * You note the positions of all of the empty spaces (<code>.</code>) and rocks (your puzzle input).
     * For example:
     * <pre>
     * O....#....
     * O.OO#....#
     * .....##...
     * OO.#O....O
     * .O.....O#.
     * O.#..O.#.#
     * ..O..#O..O
     * .......O..
     * #....###..
     * #OO..#....
     * </pre>
     * <p>
     * Start by tilting the lever so all of the rocks will slide <em>north</em> as far as they will go:
     * <pre>
     * OOOO.#.O..
     * OO..#....#
     * OO..O##..O
     * O..#.OO...
     * ........#.
     * ..#....#.#
     * ..O..#.O.O
     * ..O.......
     * #....###..
     * #....#....
     * </pre>
     * <p>
     * You notice that the support beams along the north side of the platform are <em>damaged</em>; to ensure the platform doesn't collapse, you should calculate the <em>total load</em> on the north support beams.
     * <p>
     * The amount of load caused by a single rounded rock (<code>O</code>) is equal to the number of rows from the rock to the south edge of the platform, including the row the rock is on.
     * (Cube-shaped rocks (<code>#</code>) don't contribute to load.)
     * So, the amount of load caused by each rock in each row is as follows:
     * <pre>
     * OOOO.#.O..
     * 10
     * OO..#....#  9
     * OO..O##..O  8
     * O..#.OO...
     * 7
     * ........#.
     * 6
     * ..#....#.#  5
     * ..O..#.O.O  4
     * ..O.......
     * 3
     * #....###..
     * 2
     * #....#....
     * 1
     * </pre>
     * <p>
     * The total load is the sum of the load caused by all of the <em>rounded rocks</em>.
     * In this example, the total load is <code><em>136</em></code>.
     * <p>
     * Tilt the platform so that the rounded rocks all roll north.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Afterward, what is the total load on the north support beams?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        final Grid.MutableGrid grid = context.readMutableGrid();
        tilt(grid, Direction.UP);

        return grid.stream()
                .filter(coord -> grid.get(coord) == 'O')
                .mapToInt(coord -> (grid.height() - coord.y()))
                .sum();
    }

    /**
     * The parabolic reflector dish deforms, but not in a way that focuses the beam.
     * To do that, you'll need to move the rocks to the edges of the platform.
     * Fortunately, a button on the side of the control panel labeled "<em>spin cycle</em>" attempts to do just that!
     * <p>
     * Each <em>cycle</em> tilts the platform four times so that the rounded rocks roll <em>north</em>, then <em>west</em>, then <em>south</em>, then <em>east</em>.
     * After each tilt, the rounded rocks roll as far as they can before the platform tilts in the next direction.
     * After one cycle, the platform will have finished rolling the rounded rocks in those four directions in that order.
     * <p>
     * Here's what happens in the example above after each of the first few cycles:
     * <pre>
     * After 1 cycle:
     * .....#....
     * ....#...O#
     * ...OO##...
     * .OO#......
     * .....OOO#.
     * .O#...O#.#
     * ....O#....
     * ......OOOO
     * #...O###..
     * #..OO#....
     *
     * After 2 cycles:
     * .....#....
     * ....#...O#
     * .....##...
     * ..O#......
     * .....OOO#.
     * .O#...O#.#
     * ....O#...O
     * .......OOO
     * #..OO###..
     * #.OOO#...O
     *
     * After 3 cycles:
     * .....#....
     * ....#...O#
     * .....##...
     * ..O#......
     * .....OOO#.
     * .O#...O#.#
     * ....O#...O
     * .......OOO
     * #...O###.O
     * #.OOO#...O
     * </pre>
     * <p>
     * This process should work if you leave it running long enough, but you're still worried about the north support beams.
     * To make sure they'll survive for a while, you need to calculate the <em>total load</em> on the north support beams after <code>1000000000</code> cycles.
     * <p>
     * In the above example, after <code>1000000000</code> cycles, the total load on the north support beams is <code><em>64</em></code>.
     * <p>
     * Run the spin cycle for <code>1000000000</code> cycles.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return Afterward, what is the total load on the north support beams?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        final Grid.MutableGrid grid = context.readMutableGrid();

        final Map<Integer, Integer> loopDetection = new HashMap<>();
        for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
            tilt(grid, Direction.NORTH);
            tilt(grid, Direction.WEST);
            tilt(grid, Direction.SOUTH);
            tilt(grid, Direction.EAST);

            final int hash = grid.calculateHash();
            if (loopDetection.containsKey(hash)) {
                final int startOfLoop = loopDetection.get(hash);
                final int lengthOfLoop = i - startOfLoop;
                final int delta = (NUMBER_OF_ITERATIONS - i) % lengthOfLoop;
                i = NUMBER_OF_ITERATIONS - (delta == 0 ? lengthOfLoop : delta);
            } else {
                loopDetection.put(hash, i);
            }
        }

        return grid.stream()
                .filter(coord -> grid.get(coord) == 'O')
                .mapToInt(coord -> (grid.height() - coord.y()))
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        final Grid.MutableGrid grid = context.readMutableGrid();

        tilt(grid, Direction.NORTH);

        final int answer1 = grid.stream()
                .filter(coord -> grid.get(coord) == 'O')
                .mapToInt(coord -> (grid.height() - coord.y()))
                .sum();

        tilt(grid, Direction.WEST);
        tilt(grid, Direction.SOUTH);
        tilt(grid, Direction.EAST);

        final Map<Integer, Integer> loopDetection = new HashMap<>();
        for (int i = 1; i < NUMBER_OF_ITERATIONS; i++) {
            tilt(grid, Direction.NORTH);
            tilt(grid, Direction.WEST);
            tilt(grid, Direction.SOUTH);
            tilt(grid, Direction.EAST);

            final int hash = grid.calculateHash();
            if (loopDetection.containsKey(hash)) {
                final int startOfLoop = loopDetection.get(hash);
                final int lengthOfLoop = i - startOfLoop;
                final int delta = (NUMBER_OF_ITERATIONS - i) % lengthOfLoop;
                i = NUMBER_OF_ITERATIONS - (delta == 0 ? lengthOfLoop : delta);
            } else {
                loopDetection.put(hash, i);
            }
        }

        final int answer2 = grid.stream()
                .filter(coord -> grid.get(coord) == 'O')
                .mapToInt(coord -> (grid.height() - coord.y()))
                .sum();

        return new IntTuple(answer1, answer2);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Tilt the dish in a specific direction.
     */
    private static void tilt(final Grid.MutableGrid grid, final Direction direction) {
        switch (direction) {
            case UP, LEFT -> tiltForwards(grid, direction);
            case DOWN, RIGHT -> tiltBackwards(grid, direction);
            default -> throw new IllegalArgumentException("Invalid direction:: " + direction);
        }

    }

    /*
     * Locate all the rocks on the dish.
     */

    /*
     * Collect the positions of rocks from top-left to bottom-right.
     */
    private static void tiltForwards(final Grid.MutableGrid grid, final Direction direction) {
        for (int y = 0; y < grid.height(); y++) {
            for (int x = 0; x < grid.width(); x++) {
                final Coordinate rock = new Coordinate(x, y);
                if (grid.get(rock) != 'O') continue;

                rollRock(grid, direction, rock);
            }
        }
    }

    /*
     * Collect the positions of rocks from bottom-right to top-left.
     */
    private static void tiltBackwards(final Grid.MutableGrid grid, final Direction direction) {
        for (int y = grid.height() - 1; y >= 0; y--) {
            for (int x = grid.width() - 1; x >= 0; x--) {
                final Coordinate rock = new Coordinate(x, y);
                if (grid.get(rock) != 'O') continue;
                rollRock(grid, direction, rock);
            }
        }
    }

    /*
     * Roll a rock in a given direction.
     */
    private static void rollRock(final Grid.MutableGrid grid, final Direction direction, final Coordinate rock) {
        Coordinate current = rock;
        Coordinate adjusted = current.adjustBy(direction);
        while (grid.contains(adjusted) && grid.get(adjusted) == '.') {
            current = adjusted;
            adjusted = current.adjustBy(direction);
        }

        grid.set(rock, '.');
        grid.set(current, 'O');
    }

}

