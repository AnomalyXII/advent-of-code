package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;

import static java.util.Arrays.stream;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 9: Mirage Maintenance.
 */
@Solution(year = 2023, day = 9, title = "Mirage Maintenance")
public class Day9 {

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You ride the camel through the sandstorm and stop where the ghost's maps told you to stop.
     * <span title="The sound of a sandstorm slowly settling.">The sandstorm subsequently subsides, somehow seeing you standing at an <em>oasis</em>!</span>
     * <p>
     * The camel goes to get some water and you stretch your neck.
     * As you look up, you discover what must be yet another giant floating island, this one made of metal!
     * That must be where the <em>parts to fix the sand machines</em> come from.
     * <p>
     * There's even a <a href="https://en.wikipedia.org/wiki/Hang_gliding" target="_blank">hang glider</a> partially buried in the sand here; once the sun rises and heats up the sand, you might be able to use the glider and the hot air to get all the way up to the metal island!
     * <p>
     * While you wait for the sun to rise, you admire the oasis hidden here in the middle of Desert Island.
     * It must have a delicate ecosystem; you might as well take some ecological readings while you wait.
     * Maybe you can report any environmental instabilities you find to someone so the oasis can be around for the next sandstorm-worn traveler.
     * <p>
     * You pull out your handy <em>Oasis And Sand Instability Sensor</em> and analyze your surroundings.
     * The OASIS produces a report of many values and how they are changing over time (your puzzle input).
     * Each line in the report contains the <em>history</em> of a single value.
     * For example:
     * <pre>
     * 0 3 6 9 12 15
     * 1 3 6 10 15 21
     * 10 13 16 21 30 45
     * </pre>
     * <p>
     * To best protect the oasis, your environmental report should include a <em>prediction of the next value</em> in each history.
     * To do this, start by making a new sequence from the <em>difference at each step</em> of your history.
     * If that sequence is <em>not</em> all zeroes, repeat this process, using the sequence you just generated as the input sequence.
     * Once all of the values in your latest sequence are zeroes, you can extrapolate what the next value of the original history should be.
     * <p>
     * In the above dataset, the first history is <code>0 3 6 9 12 15</code>.
     * Because the values increase by <code>3</code> each step, the first sequence of differences that you generate will be <code>3 3 3 3 3</code>.
     * Note that this sequence has one fewer value than the input sequence because at each step it considers two numbers from the input.
     * Since these values aren't <em>all zero</em>, repeat the process: the values differ by <code>0</code> at each step, so the next sequence is <code>0 0 0 0</code>.
     * This means you have enough information to extrapolate the history!
     * Visually, these sequences can be arranged like this:
     * <pre>
     * 0   3   6   9  12  15
     *   3   3   3   3   3
     *     0   0   0   0
     * </pre>
     * <p>
     * To extrapolate, start by adding a new zero to the end of your list of zeroes; because the zeroes represent differences between the two values above them, this also means there is now a placeholder in every sequence above it:
     *
     * <pre>
     * 0   3   6   9  12  15   <em>B</em>
     *   3   3   3   3   3   <em>A</em>
     *     0   0   0   0   <em>0</em>
     * </pre>
     * <p>
     * You can then start filling in placeholders from the bottom up.
     * <code>A</code> needs to be the result of increasing <code>3</code> (the value to its left) by <code>0</code> (the value below it); this means <code>A</code> must be <code><em>3</em></code>:
     * <pre>
     * 0   3   6   9  12  15   B
     *   3   3   3   3   <em>3</em>   <em>3</em>
     *     0   0   0   0   <em>0</em>
     * </pre>
     * <p>
     * Finally, you can fill in <code>B</code>, which needs to be the result of increasing <code>15</code> (the value to its left) by <code>3</code> (the value below it), or <code><em>18</em></code>:
     * <pre>
     * 0   3   6   9  12  <em>15</em>  <em>18</em>
     *   3   3   3   3   3   <em>3</em>
     *     0   0   0   0   0
     * </pre>
     * <p>
     * So, the next value of the first history is <code><em>18</em></code>.
     * <p>
     * Finding all-zero differences for the second history requires an additional sequence:
     * <pre>
     * 1   3   6  10  15  21
     *   2   3   4   5   6
     *     1   1   1   1
     *       0   0   0
     * </pre>
     * <p>
     * Then, following the same process as before, work out the next value in each sequence from the bottom up:
     * <pre>
     * 1   3   6  10  15  21  <em>28</em>
     *   2   3   4   5   6   <em>7</em>
     *     1   1   1   1   <em>1</em>
     *       0   0   0   <em>0</em>
     * </pre>
     * <p>
     * So, the next value of the second history is <code><em>28</em></code>.
     * <p>
     * The third history requires even more sequences, but its next value can be found the same way:
     * <pre>
     * 10  13  16  21  30  45  <em>68</em>
     *    3   3   5   9  15  <em>23</em>
     *      0   2   4   6   <em>8</em>
     *        2   2   2   <em>2</em>
     *          0   0   <em>0</em>
     * </pre>
     * <p>
     * So, the next value of the third history is <code><em>68</em></code>.
     * <p>
     * If you find the next value for each history in this example and add them together, you get <code><em>114</em></code>.
     * <p>
     * Analyze your OASIS report and extrapolate the next value for each history.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of these extrapolated values?
     */
    @Part(part = I)
    public int calculateAnswerForPart1(final SolutionContext context) {
        return context.stream()
                .mapToInt(Day9::calculateNext)
                .sum();
    }

    /**
     * Of course, it would be nice to have <em>even more history</em> included in your report.
     * Surely it's safe to just <em>extrapolate backwards</em> as well, right?
     * <p>
     * For each history, repeat the process of finding differences until the sequence of differences is entirely zero.
     * Then, rather than adding a zero to the end and filling in the next values of each previous sequence, you should instead add a zero to the <em>beginning</em> of your sequence of zeroes, then fill in new <em>first</em> values for each previous sequence.
     * <p>
     * In particular, here is what the third example history looks like when extrapolating back in time:
     * <pre>
     * <em>5</em>  10  13  16  21  30  45
     *   <em>5</em>   3   3   5   9  15
     *    <em>-2</em>   0   2   4   6
     *       <em>2</em>   2   2   2
     *         <em>0</em>   0   0
     * </pre>
     * <p>
     * Adding the new values on the left side of each sequence from bottom to top eventually reveals the new left-most history value: <code><em>5</em></code>.
     * <p>
     * Doing this for the remaining example data above results in previous values of <code><em>-3</em></code> for the first history and <code><em>0</em></code> for the second history.
     * Adding all three new values together produces <code><em>2</em></code>.
     * <p>
     * Analyze your OASIS report again, this time extrapolating the <em>previous</em> value for each history.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of these extrapolated values?
     */
    @Part(part = II)
    public int calculateAnswerForPart2(final SolutionContext context) {
        return context.stream()
                .mapToInt(Day9::calculatePrev)
                .sum();
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return an {@link IntTuple} containing the answers for both parts
     */
    @Optimised
    public IntTuple calculateAnswers(final SolutionContext context) {
        return context.stream()
                .map(line -> stream(line.split("\\s+"))
                        .mapToInt(Integer::parseInt)
                        .toArray())
                .reduce(
                        IntTuple.NULL,
                        (tup, seq) -> tup.add(
                                calculateNext(seq),
                                calculatePrev(seq)
                        ),
                        IntTuple::add
                );
    }

    // ****************************************
    // Static Helper Methods
    // ****************************************

    /*
     * Parse a line of numbers and extrapolate the next number in the
     * sequence.
     */
    static int calculateNext(final String input) {
        return calculateNext(
                stream(input.split("\\s+"))
                        .mapToInt(Integer::parseInt)
                        .toArray()
        );
    }

    /*
     * Parse a line of numbers and extrapolate the previous number in the
     * sequence.
     */
    static int calculatePrev(final String input) {
        return calculatePrev(
                stream(input.split("\\s+"))
                        .mapToInt(Integer::parseInt)
                        .toArray()
        );
    }

    /*
     * Extrapolate the next number in the sequence.
     */
    private static int calculateNext(final int[] input) {
        final int[] deltas = new int[input.length - 1];
        final boolean allZeros = fillInDeltas(input, deltas);

        if (allZeros) return input[0];
        else return input[input.length - 1] + calculateNext(deltas);
    }

    /*
     * Extrapolate the previous number in the sequence.
     */
    private static int calculatePrev(final int[] input) {
        final int[] deltas = new int[input.length - 1];
        final boolean allZeros = fillInDeltas(input, deltas);

        if (allZeros) return input[0];
        else return input[0] - calculatePrev(deltas);
    }

    /*
     * Calculate the differences between each pair of input numbers.
     */
    private static boolean fillInDeltas(final int[] input, final int[] deltas) {
        assert input.length > 1 : "Need at least 2 points to extrapolate from!";

        boolean allZeros = true;
        for (int i = 1; i < input.length; i++)
            allZeros &= (deltas[i - 1] = input[i] - input[i - 1]) == 0;
        return allZeros;
    }

}

