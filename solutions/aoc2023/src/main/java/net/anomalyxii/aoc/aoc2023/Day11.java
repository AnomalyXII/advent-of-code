package net.anomalyxii.aoc.aoc2023;

import net.anomalyxii.aoc.annotations.Optimised;
import net.anomalyxii.aoc.annotations.Part;
import net.anomalyxii.aoc.annotations.Solution;
import net.anomalyxii.aoc.context.SolutionContext;
import net.anomalyxii.aoc.result.IntTuple;
import net.anomalyxii.aoc.result.LongTuple;
import net.anomalyxii.aoc.utils.geometry.Coordinate;
import net.anomalyxii.aoc.utils.geometry.Grid;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static net.anomalyxii.aoc.annotations.Part.PartNumber.I;
import static net.anomalyxii.aoc.annotations.Part.PartNumber.II;

/**
 * Day 11: Cosmic Expansion.
 */
@Solution(year = 2023, day = 11, title = "Cosmic Expansion")
public class Day11 {

    // ****************************************
    // Private Members
    // ****************************************

    private final int scaleFactor;

    // ****************************************
    // Constructors
    // ****************************************

    public Day11() {
        this(1000000);
    }

    Day11(final int scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    // ****************************************
    // Challenge Methods
    // ****************************************

    /**
     * You continue following signs for "Hot Springs" and eventually come across an <a href="https://en.wikipedia.org/wiki/Observatory" target="_blank">observatory</a>.
     * The Elf within turns out to be a researcher studying cosmic expansion using the giant telescope here.
     * <p>
     * He doesn't know anything about the missing machine parts; he's only visiting for this research project.
     * However, he confirms that the hot springs are the next-closest area likely to have people; he'll even take you straight there once he's done with today's observation analysis.
     * <p>
     * Maybe you can help him with the analysis to speed things up?
     * <p>
     * The researcher has collected a bunch of data and compiled the data into a single giant <em>image</em> (your puzzle input).
     * The image includes <em>empty space</em> (<code>.</code>) and <em>galaxies</em> (<code>#</code>).
     * For example:
     * <pre>
     * ...#......
     * .......#..
     * #.........
     * ..........
     * ......#...
     * .#........
     * .........#
     * ..........
     * .......#..
     * #...#.....
     * </pre>
     * <p>
     * The researcher is trying to figure out the sum of the lengths of the <em>shortest path between every pair of galaxies</em>.
     * However, there's a catch: the universe expanded in the time it took the light from those galaxies to reach the observatory.
     * <p>
     * Due to something involving gravitational effects, <em>only some space expands</em>.
     * In fact, the result is that <em>any rows or columns that contain no galaxies</em> should all actually be twice as big.
     * <p>
     * In the above example, three columns and two rows contain no galaxies:
     * <pre>
     * v  v  v
     *  ...#......
     *  .......#..
     *  #.........
     * &gt;..........&lt;
     *  ......#...
     *  .#........
     *  .........#
     * &gt;..........&lt;
     *  .......#..
     *  #...#.....
     *    ^  ^  ^
     * </pre>
     * <p>
     * These rows and columns need to be <em>twice as big</em>; the result of cosmic expansion therefore looks like this:
     * <pre>
     * ....#........
     * .........#...
     * #............
     * .............
     * .............
     * ........#....
     * .#...........
     * ............#
     * .............
     * .............
     * .........#...
     * #....#.......
     * </pre>
     * <p>
     * Equipped with this expanded universe, the shortest path between every pair of galaxies can be found.
     * It can help to assign every galaxy a unique number:
     * <pre>
     * ....1........
     * .........2...
     * 3............
     * .............
     * .............
     * ........4....
     * .5...........
     * ............6
     * .............
     * .............
     * .........7...
     * 8....9.......
     * </pre>
     * <p>
     * In these 9 galaxies, there are <em>36 pairs</em>.
     * Only count each pair once; order within the pair doesn't matter.
     * For each pair, find any shortest path between the two galaxies using only steps that move up, down, left, or right exactly one <code>.</code> or <code>#</code> at a time.
     * (The shortest path between two galaxies is allowed to pass through another galaxy.)
     * <p>
     * For example, here is one of the shortest paths between galaxies <code>5</code> and <code>9</code>:
     * <pre>
     * ....1........
     * .........2...
     * 3............
     * .............
     * .............
     * ........4....
     * .5...........
     * .##.........6
     * ..##.........
     * ...##........
     * ....##...7...
     * 8....9.......
     * </pre>
     * <p>
     * This path has length <code><em>9</em></code> because it takes a minimum of <em>nine steps</em> to get from galaxy <code>5</code> to galaxy <code>9</code> (the eight locations marked <code>#</code> plus the step onto galaxy <code>9</code> itself).
     * Here are some other example shortest path lengths:
     * <ul>
     * <li>Between galaxy <code>1</code> and galaxy <code>7</code>: 15</li>
     * <li>Between galaxy <code>3</code> and galaxy <code>6</code>: 17</li>
     * <li>Between galaxy <code>8</code> and galaxy <code>9</code>: 5</li>
     * </ul>
     * <p>
     * In this example, after expanding the universe, the sum of the shortest path between all 36 pairs of galaxies is <code><em>374</em></code>.
     * <p>
     * Expand the universe, then find the length of the shortest path between every pair of galaxies.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of these lengths?
     */
    @Part(part = I)
    public long calculateAnswerForPart1(final SolutionContext context) {
        return solve(context, 1);
    }

    /**
     * The galaxies are much <em>older</em> (and thus much <em>farther apart</em>) than the researcher initially estimated.
     * <p>
     * Now, instead of the expansion you did before, make each empty row or column <em><span title="And you have to have your pinky near your mouth when you do it.">one million</span> times</em> larger.
     * That is, each empty row should be replaced with <code>1000000</code> empty rows, and each empty column should be replaced with <code>1000000</code> empty columns.
     * <p>
     * (In the example above, if each empty row or column were merely <code>10</code> times larger, the sum of the shortest paths between every pair of galaxies would be <code><em>1030</em></code>.
     * If each empty row or column were merely <code>100</code> times larger, the sum of the shortest paths between every pair of galaxies would be <code><em>8410</em></code>.
     * However, your universe will need to expand far beyond these values.)
     * <p>
     * Starting with the same initial image, expand the universe according to these new rules, then find the length of the shortest path between every pair of galaxies.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return What is the sum of these lengths?
     */
    @Part(part = II)
    public long calculateAnswerForPart2(final SolutionContext context) {
        return solve(context, scaleFactor - 1);
    }

    // ****************************************
    // Optimised Challenge Methods
    // ****************************************

    /**
     * An optimised solution for parts 1 and 2.
     *
     * @param context the {@link SolutionContext} to solve against
     * @return a {@link LongTuple} containing the answers for both parts
     */
    @Optimised
    public LongTuple calculateAnswers(final SolutionContext context) {
        final Set<Coordinate> galaxies = parseGalaxies(context);

        final int[] rowsWithGalaxies = galaxies.stream()
                .mapToInt(Coordinate::y)
                .sorted()
                .distinct()
                .toArray();

        final int[] columnsWithGalaxies = galaxies.stream()
                .mapToInt(Coordinate::x)
                .sorted()
                .distinct()
                .toArray();

        final List<Coordinate> resolvedGalaxies1 = new ArrayList<>();
        final List<Coordinate> resolvedGalaxies2 = new ArrayList<>();
        for (final Coordinate coordinate : galaxies) {
            int ry = 0;
            IntTuple newY = new IntTuple(coordinate.y(), coordinate.y());
            for (int y = 0; y < coordinate.y(); y++) {
                if (ry < rowsWithGalaxies.length && y < rowsWithGalaxies[ry])
                    newY = newY.add(1, scaleFactor - 1);
                else
                    ++ry;
            }

            int rx = 0;
            IntTuple newX = new IntTuple(coordinate.x(), coordinate.x());
            for (int x = 0; x < coordinate.x(); x++) {
                if (rx < columnsWithGalaxies.length && x < columnsWithGalaxies[rx])
                    newX = newX.add(1, scaleFactor - 1);
                else
                    ++rx;
            }

            resolvedGalaxies1.add(new Coordinate(newX.answer1(), newY.answer1()));
            resolvedGalaxies2.add(new Coordinate(newX.answer2(), newY.answer2()));
        }

        long part1 = 0;
        long part2 = 0;
        for (int f = 0; f < galaxies.size(); f++) {
            for (int s = f + 1; s < galaxies.size(); s++) {
                part1 += distance(resolvedGalaxies1.get(f), resolvedGalaxies1.get(s));
                part2 += distance(resolvedGalaxies2.get(f), resolvedGalaxies2.get(s));
            }
        }

        return new LongTuple(part1, part2);
    }

    // ****************************************
    // Private Helper Methods
    // ****************************************

    /*
     * Expand the given input universe and calculate the sum of the distance
     * between every galaxy.
     */
    private long solve(final SolutionContext context, final int scaleFactor) {
        final Set<Coordinate> galaxies = parseGalaxies(context);

        final int[] rowsWithGalaxies = galaxies.stream()
                .mapToInt(Coordinate::y)
                .sorted()
                .distinct()
                .toArray();

        final int[] columnsWithGalaxies = galaxies.stream()
                .mapToInt(Coordinate::x)
                .sorted()
                .distinct()
                .toArray();

        final List<Coordinate> resolvedGalaxies = galaxies.stream()
                .map(galaxy -> {
                    int ry = 0;
                    int newY = galaxy.y();
                    for (int y = 0; y < galaxy.y(); y++) {
                        if (ry < rowsWithGalaxies.length && y < rowsWithGalaxies[ry])
                            newY += scaleFactor;
                        else
                            ++ry;
                    }

                    int rx = 0;
                    int newX = galaxy.x();
                    for (int x = 0; x < galaxy.x(); x++) {
                        if (rx < columnsWithGalaxies.length && x < columnsWithGalaxies[rx])
                            newX += scaleFactor;
                        else
                            ++rx;
                    }

                    return new Coordinate(newX, newY);
                })
                .toList();

        long sum = 0;
        for (int f = 0; f < galaxies.size(); f++)
            for (int s = f + 1; s < galaxies.size(); s++)
                sum += distance(resolvedGalaxies.get(f), resolvedGalaxies.get(s));

        return sum;
    }

    /*
     * Parse the `Coordinate`s of each galaxy.
     */
    private static Set<Coordinate> parseGalaxies(final SolutionContext context) {
        final Grid galaxyMap = context.readGrid();
        final Set<Coordinate> galaxies = new HashSet<>();
        galaxyMap.forEach(coordinate -> {
            final int reading = galaxyMap.get(coordinate);
            if (reading == '#') galaxies.add(coordinate);
        });
        return galaxies;
    }

    /*
     * Calculate the shortest distance between two galaxies.
     */
    private static int distance(final Coordinate start, final Coordinate end) {
        return Math.abs(start.x() - end.x()) + Math.abs(start.y() - end.y());
    }

}

